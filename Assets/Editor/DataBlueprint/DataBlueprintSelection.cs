﻿////////////////////////////////////////////
// 
// DataBlueprintSelection.cs
//
// Created: 25/08/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System.Collections.Generic;
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;
using System;
using System.Collections;

namespace DataBlueprints
{

	public class DataBlueprintSelection : IReadOnlyList<DataBlueprint>, IList<DataBlueprint>
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[DataBlueprintSelection] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//

		//
		// MEMBERS 
		//

		private List<WeakReference>	_selected = new List<WeakReference>();

		//
		// PROPERTIES
		//

		public DataBlueprint this[int index]
		{
			get
			{
				return BlueprintFromWeakRef( _selected[ index ] );
			}

			set
			{
				_selected[ index ].Target = value;
			}
		}

		public int Count
		{
			get
			{
				Purge();
				return _selected.Count;
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//

		private static DataBlueprint BlueprintFromWeakRef( WeakReference wref )
		{
			return wref.IsAlive ? wref.Target as DataBlueprint : null;
		}

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public void Add( DataBlueprint item )
		{
			_selected.Add( new WeakReference( item ) );
		}

		public void Clear()
		{
			_selected.Clear();
		}

		public bool Contains( DataBlueprint item )
		{
			Purge();
			return _selected.Find( wref => wref.IsAlive && wref.Target == item ) != null;
		}

		public void CopyTo( DataBlueprint[] array, int arrayIndex )
		{
			Purge();
			for ( int i = 0; i < _selected.Count; ++i )
			{
				if ( arrayIndex + i >= array.Length ) throw new ArgumentException( string.Format( "array not big enough" ) );
				array[ arrayIndex + i ] = _selected[i].IsAlive ? _selected[i].Target as DataBlueprint : null;
			}
		}

		public IEnumerator<DataBlueprint> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		public int IndexOf( DataBlueprint item )
		{
			Purge();
			return _selected.FindIndex( wref => wref.IsAlive && wref.Target == item );
		}

		public void Insert( int index, DataBlueprint item )
		{
			_selected.Insert( index, new WeakReference( item ) );
		}

		public bool Remove( DataBlueprint item )
		{
			int	index = IndexOf( item );
			if ( index < 0 ) return false;
			RemoveAt( index );
			return true;
		}

		public void RemoveAt( int index )
		{
			_selected.RemoveAt( index );
		}

		public void RemoveRange( int index, int length )
		{
			for ( int i = index + length - 1; i >= index; --i )
			{
				if ( i < _selected.Count )
				{
					_selected.RemoveAt( index );
				}
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			throw new NotImplementedException();
		}

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private void Purge()
		{
			for ( int i = 0; i < _selected.Count; ++i )
			{
				if ( _selected[i] == null || !_selected[i].IsAlive )
				{
					_selected.RemoveAt( i-- );
				}
			}
		}

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}

}