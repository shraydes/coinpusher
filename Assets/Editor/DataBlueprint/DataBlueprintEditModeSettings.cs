﻿using System.Collections.Generic;


public class DataBlueprintEditModeSettings
{
	public static readonly Dictionary<EditorWindow_DataBlueprints.EEditMode, DataBlueprintEditModeSettings> _presets = new Dictionary<EditorWindow_DataBlueprints.EEditMode, DataBlueprintEditModeSettings>
	{
		{
			EditorWindow_DataBlueprints.EEditMode.Designer,
			new DataBlueprintEditModeSettings(
				canAddRemoveBlueprints:			false,
				canAddRemoveProperties:			false,
				canEditBlueprintName:			false,
				canEditBlueprintParent:			false,
				canEditPropertyName:			false,
				canEditObjectMemberName:		false,
				canEditPropertyValueType:		false,
				canEditPropertyValue:			true,
				canEditPropertyDescription:		false,
				canEditPropertyOverride:		true,
				unsavedChangesMessage:			"Make sure to save your changes (from the Blueprints window) before quitting Unity or running the game"

			)
		},
		{
			EditorWindow_DataBlueprints.EEditMode.Developer,
			new DataBlueprintEditModeSettings(
				canAddRemoveBlueprints:			true,
				canAddRemoveProperties:			true,
				canEditBlueprintName:			true,
				canEditBlueprintParent:			true,
				canEditPropertyName:			true,
				canEditObjectMemberName:		true,
				canEditPropertyValueType:		true,
				canEditPropertyValue:			true,
				canEditPropertyDescription:		true,
				canEditPropertyOverride:		true,
				unsavedChangesMessage:			"Make sure to save your changes (from the Blueprints window) before quitting Unity, running the game or reloading the assembly (recompiling)"
			)
		},
		{
			EditorWindow_DataBlueprints.EEditMode.Producer,
			new DataBlueprintEditModeSettings(
				canAddRemoveBlueprints:			false,
				canAddRemoveProperties:			false,
				canEditBlueprintName:			false,
				canEditBlueprintParent:			false,
				canEditPropertyName:			false,
				canEditObjectMemberName:		false,
				canEditPropertyValueType:		false,
				canEditPropertyValue:			false,
				canEditPropertyDescription:		false,
				canEditPropertyOverride:		false,
				unsavedChangesMessage:			null
			)
		}
	};



	public readonly bool _canAddRemoveBlueprints;
	public readonly bool _canAddRemoveProperties;
	public readonly bool _canEditBlueprintName;
	public readonly bool _canEditBlueprintParent;
	public readonly bool _canEditPropertyName;
	public readonly bool _canEditObjectMemberName;
	public readonly bool _canEditPropertyValueType;
	public readonly bool _canEditPropertyValue;
	public readonly bool _canEditPropertyDescription;
	public readonly bool _canEditPropertyOverride;
	public readonly string _unsavedChangesMessage;


	public DataBlueprintEditModeSettings(bool canAddRemoveBlueprints, bool canAddRemoveProperties, bool canEditBlueprintName, bool canEditBlueprintParent, bool canEditPropertyName, bool canEditObjectMemberName, bool canEditPropertyValueType, bool canEditPropertyValue, bool canEditPropertyDescription, bool canEditPropertyOverride, string unsavedChangesMessage)
	{
		_canAddRemoveBlueprints =		canAddRemoveBlueprints;
		_canAddRemoveProperties =		canAddRemoveProperties;
		_canEditBlueprintName =			canEditBlueprintName;
		_canEditBlueprintParent =		canEditBlueprintParent;
		_canEditPropertyName =			canEditPropertyName;
		_canEditObjectMemberName =		canEditObjectMemberName;
		_canEditPropertyValueType =		canEditPropertyValueType;
		_canEditPropertyValue =			canEditPropertyValue;
		_canEditPropertyDescription =	canEditPropertyDescription;
		_canEditPropertyOverride =		canEditPropertyOverride;
		_unsavedChangesMessage =		unsavedChangesMessage;
	}
}
