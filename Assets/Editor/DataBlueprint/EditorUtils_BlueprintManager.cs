﻿////////////////////////////////////////////
// 
// EditorUtils_BlueprintManager.cs
//
// Created: 24/06/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using System.IO;
using UnityEditor;
using AmuzoEngine;
using LitJson;
using LoggerAPI = UnityEngine.Debug;

namespace DataBlueprints
{

	public static class EditorUtils_BlueprintManager
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[EditorUtils_BlueprintManager] ";

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//
	
		//
		// PROPERTIES
		//
	
		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//
	
		public static void ExportBlueprints( string path )
		{
			JsonData	jsonData = BlueprintManager.ToJsonData();

			// ensure directory exists
			Directory.CreateDirectory( Path.GetDirectoryName( path ) );

			// write to file
			File.WriteAllText( path, jsonData.ToJson( true, true ) );
		}

		// ** PROTECTED
		//

		// ** PRIVATE
		//
	}
}

