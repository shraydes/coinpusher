﻿using System.Collections;
using System.Collections.Generic;
using DataBlueprints;
using LitJson;
using UnityEditor;
using UnityEngine;
using AmuzoEngine;


public static class EditorUtils_DataBlueprint
{

	public delegate void DOnChange(string message, DataBlueprint blueprint = null);
	public delegate void DBlueprintAction(DataBlueprint blueprint);
	public delegate bool DBlueprintMenuItemEnabledFunc(DataBlueprint blueprint);
	public delegate string DBlueprintNameFormatFunc(DataBlueprint blueprint);

	public const float DEFAULT_TREE_WIDTH = 200;
	private const int TREE_TAB_SIZE = 21;
	private const float FIELD_WIDTH_VALUE = 100;
	private const float FIELD_WIDTH_OBJECT = 150;
	private const float FIELD_WIDTH_VALUE_BLUEPRINT_FOLDOUT = 20;
	private const float FIELD_WIDTH_VALUE_BLUEPRINT_FOLDOUT_SPACING = 5;
	private const float FIELD_WIDTH_VALUE_BLUEPRINT = FIELD_WIDTH_VALUE - FIELD_WIDTH_VALUE_BLUEPRINT_FOLDOUT - FIELD_WIDTH_VALUE_BLUEPRINT_FOLDOUT_SPACING;
	private const float EXPAND_COLLAPSE_BUTTON_WIDTH = 16;

	private const string VALUE_BLUEPRINT_NAME_PREFIX = "@~#";
	private static System.Random	_sValueBlueprintNameRNG = null;
	private static string GenerateRandomValueBlueprintName()
	{
		if ( _sValueBlueprintNameRNG == null )
		{
			_sValueBlueprintNameRNG = new System.Random( System.DateTime.Now.Millisecond );
		}

		return VALUE_BLUEPRINT_NAME_PREFIX + _sValueBlueprintNameRNG.Next();
	}

	private static DataBlueprint _blueprintDragging;
	private static Rect _blueprintDraggingStartRect;
	private static Vector2 _blueprintDraggingStartMousePos;
	private static Dictionary<DataBlueprint, Rect> _prevRects = new Dictionary<DataBlueprint, Rect>();
	
	private static int	_propertyIndentLevel = 0;
	public static int _pPropertyIndentLevel
	{
		get
		{
			return _propertyIndentLevel;
		}
		set
		{
			_propertyIndentLevel = value;
			EditorGUI.indentLevel = _propertyIndentLevel;
		}
	}
	


	public static bool Tree(
		IReadOnlyList<DataBlueprint> blueprints,
		IList<DataBlueprint> selectedList,
		bool allowMultiSelect = false,
		bool showRightClick = false,
		bool allowDragging = false,
		DBlueprintAction onAdd = null,
		DBlueprintAction onDelete = null,
		DOnChange onChange = null,
		DBlueprintNameFormatFunc getLabel = null,
		DBlueprintNameFormatFunc getTooltip = null,
		string searchString = ""
	)
	{
		bool	hasSelectionChanged = false;

		// ensure search string is non-null and lower case
		searchString = (searchString ?? "").ToLower();
		

		// cache event properties (properties of Event.current may change)
		DataBlueprint blueprintOver = null;
		bool wasMouseReleased = Event.current.rawType == EventType.MouseUp;
		bool wasMouseReleasedOutside = wasMouseReleased && Event.current.type == EventType.Ignore;


		// get blueprints that match search string
		IReadOnlyList<DataBlueprint> matchingBlueprints = new ReadOnlyListWrapper<DataBlueprint>( 
			blueprints.FindAll(
				blueprint => 
					blueprint._pName.ToLower().Contains( searchString )
					&& !blueprint.HasCollapsedAncestor() 
			)
		);
		
		List<DataBlueprint> toShow = new List<DataBlueprint>(matchingBlueprints);
		DataBlueprint currBlueprint = toShow.Count > 0 ? toShow[0] : null;


		while (toShow.Count > 0)
		{
			// draw parent blueprints first
			while (currBlueprint.GetParent() != null
				&& toShow.Contains(currBlueprint.GetParent())
			) {
				currBlueprint = currBlueprint.GetParent();
			}

			toShow.Remove(currBlueprint);

			// draw blueprint
			bool isMouseOver;
			int mouseButtonClicked;
			Layout_BlueprintTreeItem(matchingBlueprints, selectedList != null && selectedList.Contains( currBlueprint ), allowDragging, currBlueprint, onAdd, onDelete, out isMouseOver, out mouseButtonClicked, currBlueprint.HasChildren(), getLabel, getTooltip);

			if (isMouseOver) {
				blueprintOver = currBlueprint;
			}
			
			// handle mouse button clicked
			switch (mouseButtonClicked)
			{
				case 0 :
					if ( selectedList != null )
					{
						if ( Event.current.control && allowMultiSelect )
						{
							if ( selectedList.Contains( currBlueprint ) )
							{
								selectedList.Remove( currBlueprint );
							}
							else
							{
								selectedList.Add( currBlueprint );
							}
							hasSelectionChanged = true;
						}
						else
						{
							if ( selectedList.Count != 1 || selectedList[0] != currBlueprint )
							{
								selectedList.Clear();
								selectedList.Add( currBlueprint );
								hasSelectionChanged = true;
							}
						}
					}
					break;

				case 1 :
					if (showRightClick) {
						ShowRightClickMenu(blueprints, currBlueprint, onAdd, onDelete, onChange);
					}
					break;

				case 2 :
					currBlueprint._pIsExpandedInTreeView = !currBlueprint._pIsExpandedInTreeView;
					break;
			}

			
			// no more to draw
			if (toShow.Count == 0) {
				break;
			}

			// set next blueprint...

			// ...to child blueprint, if there is one
			bool foundChildBlueprint = false;
			foreach (DataBlueprint blueprint in toShow) {
				if (blueprint._parentBlueprintName == currBlueprint._pName) {
					currBlueprint = blueprint;
					foundChildBlueprint = true;
					break;
				}
			}
			if (foundChildBlueprint) {
				continue;
			}

			// ...to next sibling blueprint, if there is one
			while (currBlueprint != null
				&& !toShow.Contains(currBlueprint)
			) {
				currBlueprint = currBlueprint.GetNextSibling() ?? currBlueprint.GetParent();
			}

			// ...otherwise, next blueprint in toShow
			if (currBlueprint == null) {
				currBlueprint = toShow[0];
			}
		}

		
		// handle mouse released
		if (wasMouseReleased)
		{
			// release blueprint
			if (!wasMouseReleasedOutside &&
				_blueprintDragging != null &&
				_blueprintDragging != blueprintOver &&
				blueprints.Contains(_blueprintDragging)
			) {
				// prevent circular inheritance
				if (blueprintOver != null && blueprintOver.HasBlueprintAsAncestor(_blueprintDragging)) {
					blueprintOver.SetParent(null);
				}

				// parent the dropped blueprint
				if (_blueprintDragging.GetParent() != blueprintOver)
				{
					if (onChange != null) {
						onChange("Change Blueprint Parent", _blueprintDragging);
					}

					_blueprintDragging.SetParent(blueprintOver);
				}
			}

			if (blueprints.Contains(_blueprintDragging)) {
				_blueprintDragging = null;
			}
		}
		

		// draw dragging blueprint
		if (_blueprintDragging != null &&
			blueprints.Contains(_blueprintDragging)
		) {
			GUI.Box(
				_blueprintDraggingStartRect.Translate(Event.current.mousePosition - _blueprintDraggingStartMousePos),
				_blueprintDragging._pName,
				_pStyleTreeButton
			);
		}

		return hasSelectionChanged;
	}



	private static void Layout_BlueprintTreeItem(
		IReadOnlyList<DataBlueprint> blueprints,
		bool selected,
		bool allowDragging,
		DataBlueprint blueprint,
		DBlueprintAction onAdd,
		DBlueprintAction onDelete,
		out bool isMouseOver,
		out int mouseButtonClicked,
		bool hasChildren,
		DBlueprintNameFormatFunc getLabel = null,
		DBlueprintNameFormatFunc geTooltip = null
	)
	{
		EditorGUILayout.BeginHorizontal();
		{
			// icons
			foreach (Texture2D icon in GetIcons(blueprints, blueprint))
			{
				Rect texRect = GUILayoutUtility.GetRect(icon.width, icon.height, GUILayout.Width(icon.width), GUILayout.Height(icon.height));
				texRect.y += 3;
				GUI.DrawTexture(
					texRect,
					icon,
					ScaleMode.ScaleAndCrop,
					true
				);
			}


			// button properties
			string buttonLabel =
				(hasChildren ? blueprint._pIsExpandedInTreeView ? "◢ " : "▶ " : "") +
				(selected ? "<b>" : "") +
				(getLabel != null ? getLabel(blueprint) : blueprint._pName) +
				(selected ? "</b>" : "");

			string buttonTooltip = geTooltip != null ? geTooltip(blueprint) : null;
			GUIContent buttonContent = new GUIContent(buttonLabel, buttonTooltip);
			GUIStyle buttonStyle = selected ? _pStyleTreeButtonSelected : _pStyleTreeButton;
			Rect buttonRect = GUILayoutUtility.GetRect(buttonContent, buttonStyle);


			// expand/collapse button
			if (hasChildren)
			{
				Rect expandCollapseButtonRect = new Rect(buttonRect.xMin, buttonRect.yMin, EXPAND_COLLAPSE_BUTTON_WIDTH, buttonRect.height);

				if (GUI.Button(expandCollapseButtonRect, "", _pStyleTreeButtonExpandCollapse)) {
					blueprint._pIsExpandedInTreeView = !blueprint._pIsExpandedInTreeView;
				}
			}


			// ensure buttonRect is valid
			if (buttonRect.width == 1 && buttonRect.height == 1)
			{
				if (_prevRects.ContainsKey(blueprint)) {
					buttonRect = _prevRects[blueprint];
				}
			}
			else {
				if (_prevRects.ContainsKey(blueprint)) {
					_prevRects[blueprint] = buttonRect;
				}
				else {
					_prevRects.Add(blueprint, buttonRect);
				}
			}


			isMouseOver = buttonRect.Contains(Event.current.mousePosition);


			// start dragging
			if (allowDragging &&
				isMouseOver &&
				_blueprintDragging == null &&
				Event.current.type == EventType.MouseDrag
			) {
				_blueprintDragging = blueprint;
				_blueprintDraggingStartRect = buttonRect;
				_blueprintDraggingStartMousePos = Event.current.mousePosition;
			}


			// dragging over
			if (allowDragging &&
				isMouseOver &&
				_blueprintDragging != null &&
				_blueprintDragging != blueprint
			) {
				EditorGUIUtility.AddCursorRect(buttonRect, MouseCursor.ArrowPlus);
				buttonStyle = _pStyleTreeButtonSelected;
			}


			// button
			if (GUI.Button(buttonRect, buttonContent, buttonStyle)) {
				mouseButtonClicked = Event.current.button;
			}
			else {
				mouseButtonClicked = -1;
			}
		}
		EditorGUILayout.EndHorizontal();
	}


	public static void Layout_BlueprintPropertyList(
		IReadOnlyList<DataBlueprint> blueprints,
		DataBlueprint blueprint,
		DataBlueprintEditModeSettings editModeSettings,
		DOnChange onChange,
		GUIStyle subSectionHeaderStyle,
		bool showHeaders,
		bool showOurs
	)
	{
		blueprint._pHasRenderedProperties = true;

		DataBlueprint prevOwner = null;
		bool hasNonOverrideProperties = false;

		List<string> shownPropertyNames = new List<string>();

		foreach( DataBlueprintProperty property in blueprint.GetAllProperties( true ) )
		{
			// don't a show property more than once
			if( shownPropertyNames.Contains( property._name ) )
			{
				continue;
			}
			shownPropertyNames.Add( property._name );

			// get root owner of property (either self or ancestor)
			DataBlueprint rootOwner = blueprint.GetRootPropertyOwner( property._name );
			bool	isOurs = rootOwner == blueprint;

			if( isOurs )
			{
				hasNonOverrideProperties = true;
				if ( !showOurs )
				{
					continue;
				}
			}

			// owner of property
			if( rootOwner != prevOwner && showHeaders && ( showOurs || !isOurs ) )
			{
				_pPropertyIndentLevel++;
				EditorGUILayout.LabelField(
					( isOurs ? "Ours" : "Inherited from " + rootOwner._pName ) + ":",
					subSectionHeaderStyle
				);
				_pPropertyIndentLevel--;
			}

			// layout
			EditorUtils_DataBlueprint.Layout_BlueprintProperty(
				BlueprintManager._pBlueprintsReadOnly,
				blueprint,
				rootOwner.GetProperty( property._name ),
				blueprint.GetProperty( property._name, true ),
				editModeSettings,
				onChange, 
				subSectionHeaderStyle
			);

			prevOwner = rootOwner;
		}

		if ( showOurs )
		{
			// ensure "Ours" label is shown
			if( showHeaders && !hasNonOverrideProperties && editModeSettings._canAddRemoveProperties )
			{
				_pPropertyIndentLevel++;
				EditorGUILayout.LabelField( "Ours:", subSectionHeaderStyle );
				_pPropertyIndentLevel--;
			}


			// add property
			if( editModeSettings._canAddRemoveProperties )
			{
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Space( _pPropertyIndentLevel * 15 + 35 );

					if( GUILayout.Button( "+ Add Property", GUILayout.Width( 120 ) ) )
					{
						onChange( "Add Blueprint Property", blueprint );
						blueprint.NewProperty();
					}
				}
				EditorGUILayout.EndHorizontal();
			}
		}
	}



	public static void Layout_BlueprintProperty(
		IReadOnlyList<DataBlueprint> blueprints,
		DataBlueprint blueprint,
		DataBlueprintProperty rootProperty,
		DataBlueprintProperty valueProperty,
		DataBlueprintEditModeSettings editModeSettings,
		DOnChange onChange,
		GUIStyle subSectionHeaderStyle
	)
	{
		bool ownsRoot = blueprint._pPropertiesReadOnly.Contains(rootProperty);
		bool ownsValue = blueprint._pPropertiesReadOnly.Contains(valueProperty);
		bool isValueOverride = ownsValue && !ownsRoot;
		bool isShowSubsection = true;

		Rect	valueRect;

		_pPropertyIndentLevel += 2;
		EditorGUILayout.BeginHorizontal();
		{
			// name
			if (editModeSettings._canEditPropertyName)
			{
				EditorGUI.BeginDisabledGroup(!ownsRoot);
				{
					string newName = EditorGUILayout.TextField(rootProperty._name, GUILayout.Width(150));

					// only set if not already used in self or ancestors
					if (blueprint.GetProperty(newName, true) == null && !string.IsNullOrEmpty(newName)) {
						if (onChange != null) onChange("Change Blueprint Property Name", blueprint);
						blueprint.SetNameOfPropertyInChildren(rootProperty._name, newName);
						rootProperty._name = newName;
					}
				}
				EditorGUI.EndDisabledGroup();
			}
			else
			{
				EditorGUILayout.LabelField( new GUIContent( rootProperty._name, rootProperty._name ), GUILayout.Width(150));
			}
			EditorGUI.indentLevel = 0;


			// object member name
			if (editModeSettings._canEditObjectMemberName)
			{
				EditorGUI.BeginDisabledGroup(!ownsRoot);
				{
					string newName = EditorUtils.TextFieldWithEmptyMessage(null, rootProperty._objectMemberName, "_memberName", GUILayout.Width(100));

					// only set if not already used in self or ancestors
					if (newName != rootProperty._objectMemberName && blueprint.GetPropertyByObjectMemberName(newName, true) == null) {
						if (onChange != null) onChange("Change Blueprint Property Object Member Name", blueprint);
						rootProperty._objectMemberName = newName;
						blueprint.UpdateChildrensOverridingProperties();
					}
				}
				EditorGUI.EndDisabledGroup();
			}
			else
			{
				EditorGUILayout.LabelField("", GUILayout.Width(100));
			}


			// value type
			{
				string	typeName = rootProperty._pValueType.ToString();
				if ( rootProperty._pIsArray )
				{
					typeName += "[]";
				}
				if (editModeSettings._canEditPropertyValueType)
				{
					EditorGUI.BeginDisabledGroup(!ownsRoot);
					{
						if ( GUILayout.Button( typeName, EditorStyles.popup, GUILayout.Width(FIELD_WIDTH_VALUE) ) )
						{
							ShowValueTypesPopup( 
								typeCurr: rootProperty._pValueType, 
								isArrayCurr: rootProperty._pIsArray, 
								onTypeSelected: t => {
									if (t != rootProperty._pValueType) {
										if (onChange != null) onChange("Change Blueprint Property Value Type", blueprint);
										rootProperty._pValueType = t;
										blueprint.UpdateChildrensOverridingProperties();
									}
								},
								onArrayToggled: () => {
									if (onChange != null) onChange("Change Blueprint Property Is Array", blueprint);
									rootProperty._pIsArray = !rootProperty._pIsArray;
									blueprint.UpdateChildrensOverridingProperties();
								} );
						}
					}
					EditorGUI.EndDisabledGroup();
				}
				else
				{
					EditorGUILayout.LabelField(typeName, GUILayout.Width(FIELD_WIDTH_VALUE));
				}
			}


			// value
			{
				valueRect = EditorGUILayout.GetControlRect( false, GUILayout.Width(FIELD_WIDTH_VALUE) );

				if (isValueOverride && GUI.enabled) GUI.backgroundColor = Color.green;

				if ( rootProperty._pIsArray )
				{
					Layout_BlueprintPropertyValueArrayHeader( 
						blueprint,
						valueProperty,
						size:valueProperty._pValues.Count,
						setSize:newSize => {
							if ( newSize < 0 ) return;
							valueProperty._pValues.SetCount( newSize );
						},
						editModeSettings: editModeSettings,
						onChange: onChange,
						rect: valueRect );
				}
				else
				{
					Layout_BlueprintPropertyValue( blueprints, blueprint, rootProperty, valueProperty, valueProperty._pValue, newValue => {
						valueProperty._pValue = newValue;
					}, valueProperty._pIsValueExpandedInEditor, newExpanded => {
						valueProperty._pIsValueExpandedInEditor = newExpanded;
					}, editModeSettings, onChange, subSectionHeaderStyle, valueRect );
				}

				GUI.backgroundColor = Color.white;
			}


			// description
			if (editModeSettings._canEditPropertyDescription)
			{
				EditorGUI.BeginDisabledGroup(!ownsRoot);
				{
					string newDesc = EditorUtils.TextFieldWithEmptyMessage(null, rootProperty._description, "Description");

					if (newDesc != rootProperty._description) {
						if (onChange != null) onChange("Change Blueprint Property Description", blueprint);
						rootProperty._description = newDesc;
						blueprint.UpdateChildrensOverridingProperties();
					}
				}
				EditorGUI.EndDisabledGroup();
			}
			else
			{
				EditorGUILayout.LabelField(rootProperty._description);
			}


			// override
			if (!ownsRoot && editModeSettings._canEditPropertyOverride)
			{
				float prevLabelWidth = EditorGUIUtility.labelWidth;
				EditorGUIUtility.labelWidth = 65;
				
				// un-checked
				if (!isValueOverride)
				{
					if (EditorGUILayout.Toggle("Override?", false, GUILayout.Width(81)))
					{
						if (onChange != null) onChange("Blueprint Property Override", blueprint);
						blueprint.NewProperty(
							rootProperty._name,
							rootProperty._description,
							rootProperty._objectMemberName,
							rootProperty._pValueType,
							rootProperty._pIsArray,
							rootProperty._pIsArray ? valueProperty._pValues : valueProperty._pValue
						);
						isShowSubsection = false;
					}
				}
				// checked
				else
				{
					if (!EditorGUILayout.Toggle("Override?", true, GUILayout.Width(81)))
					{
						if (onChange != null) onChange("Blueprint Property Override", blueprint);
						blueprint.DeleteProperty( valueProperty, EDeletePropertyType.UntilOverride );
						isShowSubsection = false;
					}
				}

				EditorGUIUtility.labelWidth = prevLabelWidth;
			}


			// delete
			if (ownsRoot && editModeSettings._canAddRemoveProperties)
			{
				if (GUILayout.Button("X", GUILayout.Width(18), GUILayout.Height(16)))
				{
					if (blueprint.GetChildren().Length == 0) {
						DeleteProperty(blueprint, rootProperty, EDeletePropertyType.UntilOverride, onChange);
						isShowSubsection = false;
					}
					else
					{
						ShowDeletePropertyMenu(blueprint, rootProperty, onChange);
					}
				}
			}

			GUILayout.Space(3);
		}
		EditorGUILayout.EndHorizontal();

		// array values and property sub-section
		if ( isShowSubsection )
		{
			if ( rootProperty._pIsArray )
			{
				if (isValueOverride && GUI.enabled) GUI.backgroundColor = Color.green;

				Rect	valueRect_i;

				for ( int i = 0; i < valueProperty._pValues.Count; ++i )
				{
					int	iCap = i;

					valueRect_i = EditorGUILayout.GetControlRect( false, GUILayout.Width(FIELD_WIDTH_VALUE) );
					valueRect_i.xMin = valueRect.xMin;
					valueRect_i.xMax = valueRect.xMax;

					Layout_BlueprintPropertyValue( blueprints, blueprint, rootProperty, valueProperty, valueProperty._pValues[i], newValue => {
						valueProperty._pValues[iCap] = newValue;
					}, valueProperty._pAreValuesExpandedInEditor[i], newExpanded => {
						valueProperty._pAreValuesExpandedInEditor[iCap] = newExpanded;
					}, editModeSettings, onChange, subSectionHeaderStyle, valueRect_i );

					Layout_BlueprintPropertySubsection( 
						blueprints,
						blueprint,
						rootProperty,
						valueProperty, 
						valueProperty._pValues[i],
						valueProperty._pAreValuesExpandedInEditor[i],
						editModeSettings,
						onChange,
						subSectionHeaderStyle );
				}

				GUI.backgroundColor = Color.white;
			}
			else
			{
				Layout_BlueprintPropertySubsection( 
					blueprints,
					blueprint,
					rootProperty,
					valueProperty, 
					valueProperty._pValue,
					valueProperty._pIsValueExpandedInEditor,
					editModeSettings,
					onChange,
					subSectionHeaderStyle );
			}
		}

		_pPropertyIndentLevel -= 2;
	}


	private static void Layout_BlueprintPropertySubsection(
		IReadOnlyList<DataBlueprint> blueprints,
		DataBlueprint blueprint,
		DataBlueprintProperty rootProperty,
		DataBlueprintProperty valueProperty,
		object propertyValue,
		bool isExpanded,
		DataBlueprintEditModeSettings editModeSettings,
		DOnChange onChange,
		GUIStyle subSectionHeaderStyle
	)
	{
		if ( rootProperty._pValueType == DataBlueprintProperty.ValueType.Blueprint )
		{
			DataBlueprint	valueBlueprint = BlueprintManager.GetBlueprint( (string)propertyValue );
			if ( isExpanded && valueBlueprint != null && !valueBlueprint._pHasRenderedProperties )//stop recursive infinite rendering loop
			{
				DataBlueprintEditModeSettings	noEditMode = DataBlueprintEditModeSettings._presets[ EditorWindow_DataBlueprints.EEditMode.ReadOnly ];
				Layout_BlueprintPropertyList( blueprints, valueBlueprint, noEditMode, onChange, subSectionHeaderStyle, showHeaders:false, showOurs:true );
			}
		}
	}

	private static void Layout_BlueprintPropertyValue(
		IReadOnlyList<DataBlueprint> blueprints,
		DataBlueprint blueprint,
		DataBlueprintProperty rootProperty,
		DataBlueprintProperty valueProperty,
		object oldValue,
		System.Action<object> setValue,
		bool isExpanded,
		System.Action<bool> setExpanded,
		DataBlueprintEditModeSettings editModeSettings,
		DOnChange onChange,
		GUIStyle subSectionHeaderStyle,
		Rect? rect
	)
	{
		if ( editModeSettings._canEditPropertyValue )
		{
			object newValue = oldValue;

			bool ownsValue = blueprint._pPropertiesReadOnly.Contains(valueProperty);
			EditorGUI.BeginDisabledGroup(!ownsValue);
			{
				switch (rootProperty._pValueType)
				{
					case DataBlueprintProperty.ValueType.String:
						newValue = EditorUtils.TextFieldWithEmptyMessage(rect, (string)( oldValue ?? "" ), "Value", GUILayout.Width(FIELD_WIDTH_VALUE));
						break;

					case DataBlueprintProperty.ValueType.Int:
						if ( rect.HasValue )
						{
							newValue = EditorGUI.IntField(rect.Value, (int)( oldValue ?? 0 ));
						}
						else
						{
							newValue = EditorGUILayout.IntField((int)( oldValue ?? 0 ), GUILayout.Width(FIELD_WIDTH_VALUE));
						}
						break;

					case DataBlueprintProperty.ValueType.Float:
						if ( rect.HasValue )
						{
							newValue = EditorGUI.FloatField(rect.Value, (float)( oldValue ?? 0f ));
						}
						else
						{
							newValue = EditorGUILayout.FloatField((float)( oldValue ?? 0f ), GUILayout.Width(FIELD_WIDTH_VALUE));
						}
						break;

					case DataBlueprintProperty.ValueType.Bool:
						if ( rect.HasValue )
						{
							newValue = EditorGUI.Toggle(rect.Value, (bool)( oldValue ?? false ));
						}
						else
						{
							newValue = EditorGUILayout.Toggle((bool)( oldValue ?? false ), GUILayout.Width(FIELD_WIDTH_VALUE));
						}
						break;

					case DataBlueprintProperty.ValueType.Prefab:
						if ( rect.HasValue )
						{
							newValue = EditorGUI.ObjectField(rect.Value, (GameObject)oldValue, typeof(GameObject), false);
						}
						else
						{
							newValue = EditorGUILayout.ObjectField((GameObject)oldValue, typeof(GameObject), false, GUILayout.Width(FIELD_WIDTH_OBJECT));
						}
						break;

					case DataBlueprintProperty.ValueType.Texture:
						if ( rect.HasValue )
						{
							newValue = EditorGUI.ObjectField(rect.Value, (Texture2D)oldValue, typeof(Texture2D), false);
						}
						else
						{
							newValue = EditorGUILayout.ObjectField((Texture2D)oldValue, typeof(Texture2D), false, GUILayout.Width(FIELD_WIDTH_OBJECT));
						}
						break;

					case DataBlueprintProperty.ValueType.AudioClip:
						if ( rect.HasValue )
						{
							newValue = EditorGUI.ObjectField(rect.Value, (AudioClip)oldValue, typeof(AudioClip), false);
						}
						else
						{
							newValue = EditorGUILayout.ObjectField((AudioClip)oldValue, typeof(AudioClip), false, GUILayout.Width(FIELD_WIDTH_OBJECT));
						}
						break;

					case DataBlueprintProperty.ValueType.Blueprint:
						newValue = Layout_BlueprintPropertyValue_Blueprint( blueprint, valueProperty, oldValue, setValue, isExpanded, setExpanded, editModeSettings, onChange, rect );
						break;

					default :
						Debug.LogError(string.Format("Unhandled ValueType \"{0}\"", rootProperty._pValueType));
						break;
				}
			}
			EditorGUI.EndDisabledGroup();

			// update if changed
			if (!Equals(oldValue, newValue)) {
				if (onChange != null) onChange("Change Blueprint Property Value", blueprint);
				setValue( newValue );
			}
		}
		else
		{
			switch (rootProperty._pValueType)
			{
			case DataBlueprintProperty.ValueType.String:
			case DataBlueprintProperty.ValueType.Int:
			case DataBlueprintProperty.ValueType.Float:
			case DataBlueprintProperty.ValueType.Bool:
				{
					string	valueStr = oldValue != null ? oldValue.ToString() : "";
					if ( rect.HasValue )
					{
						EditorGUI.LabelField( rect.Value, new GUIContent( valueStr, valueStr ) );
					}
					else
					{
						EditorGUILayout.LabelField( new GUIContent( valueStr, valueStr ), GUILayout.Width(FIELD_WIDTH_VALUE) );
					}
				}
				break;

			case DataBlueprintProperty.ValueType.Prefab:
				if ( rect.HasValue )
				{
					EditorGUI.ObjectField(rect.Value, (GameObject)oldValue, typeof(GameObject), false);
				}
				else
				{
					EditorGUILayout.ObjectField((GameObject)oldValue, typeof(GameObject), false, GUILayout.Width(FIELD_WIDTH_OBJECT));
				}
				break;

			case DataBlueprintProperty.ValueType.Texture:
				if ( rect.HasValue )
				{
					EditorGUI.ObjectField(rect.Value, (Texture2D)oldValue, typeof(Texture2D), false);
				}
				else
				{
					EditorGUILayout.ObjectField((Texture2D)oldValue, typeof(Texture2D), false, GUILayout.Width(FIELD_WIDTH_OBJECT));
				}
				break;

			case DataBlueprintProperty.ValueType.AudioClip:
				if ( rect.HasValue )
				{
					EditorGUI.ObjectField(rect.Value, (AudioClip)oldValue, typeof(AudioClip), false);
				}
				else
				{
					EditorGUILayout.ObjectField((AudioClip)oldValue, typeof(AudioClip), false, GUILayout.Width(FIELD_WIDTH_OBJECT));
				}
				break;

			case DataBlueprintProperty.ValueType.Blueprint:
				Layout_BlueprintPropertyValue_Blueprint( blueprint, valueProperty, oldValue, setValue, isExpanded, setExpanded, editModeSettings, onChange, rect );
				break;
			}
		}
	}


	private static int Layout_BlueprintPropertyValueArrayHeader( 
		DataBlueprint blueprint, 
		DataBlueprintProperty valueProperty,
		int size, 
		System.Action<int> setSize, 
		DataBlueprintEditModeSettings editModeSettings,
		DOnChange onChange, 
		Rect rect )
	{
		if ( editModeSettings._canEditPropertyValue )
		{
			bool ownsValue = blueprint._pPropertiesReadOnly.Contains(valueProperty);
			EditorGUI.BeginDisabledGroup(!ownsValue);
			{
				EditorGUIUtility.labelWidth = 0.5f * rect.width;
				int	newSize = EditorGUI.IntField( rect, "Size", size );

				if ( newSize != size )
				{
					if (onChange != null) onChange("Change Blueprint Property Value Array Size", blueprint);
					setSize( newSize );
					size = newSize;
				}
			}
			EditorGUI.EndDisabledGroup();
		}
		else
		{
			EditorGUI.LabelField( rect, "Size", size.ToString() );
		}

		return size;
	}


	private static object Layout_BlueprintPropertyValue_Blueprint( 
		DataBlueprint blueprint,
		DataBlueprintProperty valueProperty,
		object oldValue,
		System.Action<object> setValue,
		bool isExpanded,
		System.Action<bool> setExpanded,
		DataBlueprintEditModeSettings editModeSettings,
		DOnChange onChange,
		Rect? rect
	)
	{
		DataBlueprint	current = BlueprintManager.GetBlueprint( (string)oldValue );
		bool			canFoldout = current != null && !current._pHasRenderedProperties;
		Rect			buttonRect = rect.HasValue ? rect.Value : EditorGUILayout.GetControlRect( false, GUILayout.Width( FIELD_WIDTH_VALUE ) );
		Rect			foldoutRect = buttonRect;

		buttonRect.width = canFoldout ? FIELD_WIDTH_VALUE_BLUEPRINT : FIELD_WIDTH_VALUE;
		foldoutRect.xMin = foldoutRect.xMax - FIELD_WIDTH_VALUE_BLUEPRINT_FOLDOUT;

		string	name = DataBlueprint.NameOrNone( current );

		if ( editModeSettings._canEditPropertyValue )
		{
			bool ownsValue = blueprint._pPropertiesReadOnly.Contains(valueProperty);
			EditorGUI.BeginDisabledGroup(!ownsValue || !editModeSettings._canEditPropertyValue);
			{
				if ( GUI.Button( buttonRect, new GUIContent( name, name ), EditorStyles.popup ) )
				{
					ShowBlueprintsPopup(
						blueprints: blueprint._pBlueprintList,
						onSelected: selected => {
							if ( selected != current )
							{
								setValue( selected != null ? selected._pName : "" );
								if ( onChange != null )
								{
									onChange( "Change Blueprint Property Value", blueprint );
								}
							}
						},
						current: current,
						enabledFunc: bp => true,
						nameFormatFunc: bp => {
							return bp.GetInheritancePath();
						}
					);
				}
			}
			EditorGUI.EndDisabledGroup();
		}
		else
		{
			EditorGUI.LabelField( buttonRect, new GUIContent( name, name ) );
		}

		if ( canFoldout )
		{
			if ( GUI.Button( foldoutRect, isExpanded ? "\u25B2" : "\u25BC" ) )
			{
				setExpanded( !isExpanded );
			}
		}

		return oldValue;
	}


	private static void Layout_ArrayList( string name, bool isExpanded, System.Action<bool> setExpanded, int size, System.Action<int> setSize, System.Action<int> drawElement )
	{
		EditorGUILayout.BeginVertical();

		bool	isExpandedNew = EditorGUILayout.Foldout( isExpanded, name );

		if ( isExpandedNew != isExpanded )
		{
			setExpanded( isExpandedNew );
		}

		if ( isExpandedNew )
		{
			int	newSize = EditorGUILayout.IntField( "Size", size );

			if ( newSize != size )
			{
				setSize( newSize );
			}

			for ( int i = 0; i < size; ++i )
			{
				drawElement( i );
			}
		}

		EditorGUILayout.EndVertical();
	}


	private static Texture2D[] GetIcons(IReadOnlyList<DataBlueprint> blueprints, DataBlueprint blueprint, bool isParent = false)
	{
		List<Texture2D> ret = new List<Texture2D>();

		if (blueprint.GetParent() != null)
		{
			ret.Add(GetIcons(blueprints, blueprint.GetParent(), true));
			ret.Add(
				blueprint.GetNextSibling() != null
					? (isParent ? _pIconLine : _pIconFork)
					: (isParent ? _pIconBlank : _pIconArrow)
			);
		}
		else {
			ret.Add(_pIconNone);
		}

		return ret.ToArray();
	}





	private static void ShowDeletePropertyMenu(DataBlueprint blueprint, DataBlueprintProperty property, DOnChange onChange)
	{
		GenericMenu menu = new GenericMenu();

		// title
		menu.AddDisabledItem(
			new GUIContent("Delete and...")
		);

		menu.AddSeparator("");

		// all
		menu.AddItem(
			new GUIContent("Delete overrides in descendants"),
			false,
			() => DeleteProperty(blueprint, property, EDeletePropertyType.All, onChange)
		);

		// until override
		menu.AddItem(
			new GUIContent("Keep overrides in descendants"),
			false,
			() => DeleteProperty(blueprint, property, EDeletePropertyType.UntilOverride, onChange)
		);

		// move to children
		menu.AddItem(
			new GUIContent("Move to children (if not overridden)"),
			false,
			() => DeleteProperty(blueprint, property, EDeletePropertyType.MoveToChildren, onChange)
		);

		menu.ShowAsContext();
	}





	private static void DeleteProperty(
		DataBlueprint fromBlueprint,
		DataBlueprintProperty property,
		EDeletePropertyType deleteType,
		DOnChange onChange
	)
	{
		if (onChange != null) {
			onChange("Delete Blueprint Property", fromBlueprint);
		}

		fromBlueprint.DeleteProperty(property, deleteType);
	}





	private static void ShowRightClickMenu(
		IReadOnlyList<DataBlueprint> blueprints,
		DataBlueprint blueprint,
		DBlueprintAction onAdd,
		DBlueprintAction onDelete,
		DOnChange onChange
	)
	{
		GenericMenu rightClickMenu = new GenericMenu();

		// delete
		rightClickMenu.AddItem(
			new GUIContent("Delete"),
			false,
			() => {
				if (onChange != null) onChange("Delete Blueprint");
				BlueprintManager.DeleteBlueprint(
					blueprint,
					blueprint.GetChildren().Length > 0 && EditorUtility.DisplayDialog("Delete Children?", "Delete child blueprints too?", "Delete Children", "Keep Children")
				);
				if (onDelete != null) onDelete(blueprint);
			}
		);

		// add
		rightClickMenu.AddItem(
			new GUIContent("Add Child"),
			false,
			() => {
				if (onChange != null) onChange("Add Child Blueprint");
				DataBlueprint newBlueprint = BlueprintManager.AddBlueprint(BlueprintManager._pNextNewBlueprintName, blueprint._pName);
				if (onAdd != null) onAdd(newBlueprint);
			}
		);

		rightClickMenu.ShowAsContext(); 
	}





	public static void ShowOptions(
		int	optionCount,
		System.Predicate<int> isOptionVisible,
		System.Predicate<int> isOptionEnabled,
		System.Predicate<int> isOptionSelected,
		System.Action<int, bool, bool> showOption
	)
	{
		if ( showOption == null ) return;

		for ( int i = 0; i < optionCount; ++i )
		{
			if ( isOptionVisible != null && isOptionVisible( i ) == false ) continue;

			showOption( i, isOptionEnabled == null || isOptionEnabled( i ) == true, isOptionSelected != null && isOptionSelected( i ) == true );
		}
	}


	public static void ShowMenuOptions(
		GenericMenu menu,
		int	optionCount,
		System.Predicate<int> isOptionVisible,
		System.Predicate<int> isOptionEnabled,
		System.Predicate<int> isOptionSelected,
		System.Func<int, GUIContent> getOptionContent,
		System.Action<int> selectOption
	)
	{
		if ( getOptionContent == null ) return;

		ShowOptions( optionCount, isOptionVisible, isOptionEnabled, isOptionSelected, showOption:( i, isEnabled, isSelected ) => {
			if ( isEnabled )
			{
				menu.AddItem( getOptionContent( i ),
					isSelected,
					i2 => {
						if ( selectOption != null )
						{
							selectOption( (int)i2 );
						}
					},
					i );
			}
			else
			{
				menu.AddDisabledItem( getOptionContent( i ) );
			}
		} );
	}


	public static void ShowEnumMenuOptions<EnumType>(
		GenericMenu menu,
		System.Predicate<EnumType> isOptionVisible,
		System.Predicate<EnumType> isOptionEnabled,
		System.Predicate<EnumType> isOptionSelected,
		System.Func<EnumType, GUIContent> getOptionContent,
		System.Action<EnumType> selectOption
	) where EnumType : struct, System.IConvertible
	{
		System.Type	enumType = typeof( EnumType );
		string[]	enumNames = System.Enum.GetNames( enumType );
		EnumType[]	enumValues = System.Enum.GetValues( enumType ) as EnumType[];

		ShowMenuOptions( menu, 
			optionCount:enumValues.Length,
			isOptionVisible:i => isOptionVisible == null || isOptionVisible( enumValues[i] ) == true,
			isOptionEnabled:i => isOptionEnabled == null || isOptionEnabled( enumValues[i] ) == true,
			isOptionSelected:i => isOptionSelected != null && isOptionSelected( enumValues[i] ) == true,
			getOptionContent:i => getOptionContent != null ? getOptionContent( enumValues[i] ) : new GUIContent( enumNames[i] ),
			selectOption:i => {
				if ( selectOption != null )
				{
					selectOption( enumValues[i] );
				}
			} );
	}


	public static void ShowValueTypesPopup(
		DataBlueprintProperty.ValueType typeCurr,
		bool isArrayCurr,
		System.Action<DataBlueprintProperty.ValueType> onTypeSelected,
		System.Action onArrayToggled
	)
	{
		GenericMenu menu = new GenericMenu();

		// value types
		ShowEnumMenuOptions<DataBlueprintProperty.ValueType>( menu,
			isOptionVisible: v => true,
			isOptionEnabled: v => true,
			isOptionSelected: v => v == typeCurr,
			getOptionContent: null,
			selectOption: v => {
				if ( onTypeSelected != null )
				{
					onTypeSelected( v );
				}
			} );

		menu.AddSeparator("");

		// is array?
		menu.AddItem( new GUIContent( "Array" ),
			isArrayCurr,
			() => {
				if ( onArrayToggled != null )
				{
					onArrayToggled();
				}
			}
		);

		// display
		menu.ShowAsContext();
	}


	public static void ShowBlueprintsPopup(
		IReadOnlyList<DataBlueprint> blueprints,
		DBlueprintAction onSelected,
		DataBlueprint current,
		DBlueprintMenuItemEnabledFunc enabledFunc = null,
		DBlueprintNameFormatFunc nameFormatFunc = null
	)
	{
		GenericMenu menu = new GenericMenu();

		// none
		menu.AddItem(
			new GUIContent("None"),
			current == null,
			() => onSelected(null)
		);

		menu.AddSeparator("");

		int	selectedIndex = blueprints.IndexOf( current );

		// blueprints
		ShowMenuOptions( menu, 
			optionCount:blueprints.Count,
			isOptionVisible:i => true,
			isOptionEnabled:i => enabledFunc == null || enabledFunc( blueprints[i] ) == true,
			isOptionSelected:i => i == selectedIndex,
			getOptionContent:i => new GUIContent(nameFormatFunc != null ? nameFormatFunc(blueprints[i]) : blueprints[i]._pName),
			selectOption:i => {
				if ( onSelected != null )
				{
					onSelected( blueprints[i] );
				}
			} );

		// display
		menu.ShowAsContext();
	}


	// ------------------------------------------------ ICONS ------------------------------------------------

	private static Texture2D _iconBlueprints;
	public static Texture2D _pIconBlueprints{
		get { return _iconBlueprints != null ? _iconBlueprints : (_iconBlueprints = Resources.Load<Texture2D>("BlueprintsIcon")); }
	}

	private static Texture2D _iconNone;
	public static Texture2D _pIconNone {
		get { return _iconNone != null ? _iconNone : (_iconNone = new Texture2D(0, TREE_TAB_SIZE)); }
	}

	private static Texture2D _iconBlank;
	public static Texture2D _pIconBlank {
		get { return _iconBlank != null ? _iconBlank : (_iconBlank = Utils.GetSolidTexture(Color.clear, TREE_TAB_SIZE, TREE_TAB_SIZE)); }
	}

	private static Texture2D _iconLine;
	public static Texture2D _pIconLine {
		get { return _iconLine != null ? _iconLine : (_iconLine = Resources.Load<Texture2D>("TreeIconLine")); }
	}

	private static Texture2D _iconArrow;
	public static Texture2D _pIconArrow {
		get { return _iconArrow != null ? _iconArrow : (_iconArrow = Resources.Load<Texture2D>("TreeIconArrowCurved")); }
	}

	private static Texture2D _iconFork;
	public static Texture2D _pIconFork {
		get { return _iconFork != null ? _iconFork : (_iconFork = Resources.Load<Texture2D>("TreeIconFork")); }
	}

	private static Texture2D _iconWarning;
	public static Texture2D _pIconWarning{
		get { return _iconWarning != null ? _iconWarning : (_iconWarning = Resources.Load<Texture2D>("WarningIcon")); }
	}

	private static Texture2D _iconInfo;
	public static Texture2D _pIconInfo{
		get { return _iconInfo != null ? _iconInfo : (_iconInfo = Resources.Load<Texture2D>("InfoIcon")); }
	}





	// ------------------------------------------------ STYLES ------------------------------------------------

	private static GUIStyle _styleTreeButton;
	private static GUIStyle _pStyleTreeButton
	{
		get
		{
			if (_styleTreeButton == null || _styleTreeButton.normal.background == null)
			{
				_styleTreeButton = new GUIStyle("Button");
				_styleTreeButton.alignment = TextAnchor.MiddleLeft;
				_styleTreeButton.normal.background = Utils.GetSolidTexture(new Color(1, 1, 1, 0.1f));
				_styleTreeButton.active.background = Utils.GetSolidTexture(new Color(1, 1, 1, 0.2f));
				_styleTreeButton.richText = true;
			}
			
			return _styleTreeButton;
		}
	}

	private static GUIStyle _styleTreeButtonSelected;
	private static GUIStyle _pStyleTreeButtonSelected
	{
		get
		{
			if (_styleTreeButtonSelected == null || _styleTreeButtonSelected.normal.background == null)
			{
				_styleTreeButtonSelected = new GUIStyle("Button");
				_styleTreeButtonSelected.alignment = TextAnchor.MiddleLeft;
				_styleTreeButtonSelected.normal.background = Utils.GetSolidTexture(new Color(1, 1, 1, 0.2f));
				_styleTreeButtonSelected.active.background = Utils.GetSolidTexture(new Color(1, 1, 1, 0.2f));
				_styleTreeButtonSelected.richText = true;
			}

			return _styleTreeButtonSelected;
		}
	}

	private static GUIStyle _styleTreeButtonExpandCollapse;
	private static GUIStyle _pStyleTreeButtonExpandCollapse
	{
		get
		{
			if (_styleTreeButtonExpandCollapse == null || _styleTreeButtonExpandCollapse.active.background == null)
			{
				_styleTreeButtonExpandCollapse = new GUIStyle("Button");
				_styleTreeButtonExpandCollapse.normal.background = null;
				_styleTreeButtonExpandCollapse.active.background = null;
			}

			return _styleTreeButtonExpandCollapse;
		}
	}

	
}
