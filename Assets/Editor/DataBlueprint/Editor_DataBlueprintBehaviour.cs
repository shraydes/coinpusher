﻿using System.Collections.Generic;
using DataBlueprints;
using AmuzoEngine;
using UnityEditor;
using UnityEngine;


/// <summary>
/// This editor is applied to all subclasses of DataBlueprintBehaviour
/// </summary>

[CustomEditor(typeof(DataBlueprintBehaviour), true)]

public class Editor_DataBlueprintBehaviour : Editor
{

	private bool _blueprintDetailsFoldout = true;
	private DataBlueprintBehaviour _dataBlueprintBehaviour;
	private DataBlueprintBehaviour _pDataBlueprintBehaviour {
		get { return _dataBlueprintBehaviour ?? (_dataBlueprintBehaviour = (DataBlueprintBehaviour)target); }
	}



	public override void OnInspectorGUI()
	{
		// blueprint header
		Layout_BlueprintHeader();


		// blueprint details
		if (_pDataBlueprintBehaviour._pBlueprint != null) {
			Layout_BlueprintDetails(_pDataBlueprintBehaviour._pBlueprint);
		}
		

		EditorGUILayout.Space();

		
		// default inspector
		EditorGUILayout.LabelField("Default Inspector", EditorStyles.boldLabel);

		EditorGUI.BeginChangeCheck();
		{
			DrawDefaultInspector();
		}
		// revert blueprint values if any values were modified
		if (EditorGUI.EndChangeCheck() &&
			_pDataBlueprintBehaviour._initialisationType != EInitializeType.MANUAL
		) {
			SetValuesFromBlueprint_WithWarning(_pDataBlueprintBehaviour._pBlueprint);
		}
	}


	public static void DrawBlueprintSelectionProperty( Rect position, SerializedProperty property, GUIContent label )
	{
		float	totalWidth = position.width;
		float	editWidth = 50f;
		float	labelPopupWidthRatio = 0.5f;
		float	labelWidth = labelPopupWidthRatio * ( totalWidth - editWidth );
		float	popupWidth = ( 1f - labelPopupWidthRatio ) * ( totalWidth - editWidth );

		Rect	labelRect = position;
		labelRect.width = labelWidth;

		Rect	popupRect = position;
		popupRect.xMin = labelRect.xMax;
		popupRect.width = popupWidth;

		Rect	editRect = position;
		editRect.xMin = popupRect.xMax;
		editRect.width = editWidth;

		// label
		EditorGUI.LabelField( labelRect, label );

		System.Func<DataBlueprint>	getBlueprint = () => {
			if ( string.IsNullOrEmpty( property.stringValue ) ) return null;
			return BlueprintManager.GetBlueprint( property.stringValue );
		};
			
		// popup
		if ( GUI.Button( popupRect, DataBlueprint.NameOrNone( getBlueprint() ), EditorStyles.popup) )
		{
			EditorUtils_DataBlueprint.DBlueprintAction	onBlueprintSelected = bp => {
				property.stringValue = bp._pName;
				property.serializedObject.ApplyModifiedProperties();
			};

			EditorUtils_DataBlueprint.ShowBlueprintsPopup(
				BlueprintManager._pBlueprintsReadOnly,
				onBlueprintSelected,
				getBlueprint(),
				nameFormatFunc: blueprint => blueprint.GetInheritancePath()
			);
		}

		// edit
		EditorGUI.BeginDisabledGroup(getBlueprint() == null);
		{
			if ( GUI.Button( editRect, "Edit", EditorStyles.miniButton ) )
			{
				EditorWindow_DataBlueprints.ShowWindow()._pSelectedBlueprint = getBlueprint();
			}
		}
		EditorGUI.EndDisabledGroup();
	}


	private void Layout_BlueprintHeader()
	{
		EditorGUILayout.BeginHorizontal();
		{
			// label
			EditorGUILayout.LabelField("Blueprint", EditorStyles.boldLabel, GUILayout.Width(45 * (Screen.width * 0.01f - 1)), GUILayout.MinWidth(117));
			
			// popup
			if (GUILayout.Button(DataBlueprint.NameOrNone(_pDataBlueprintBehaviour._pBlueprint), EditorStyles.popup))
			{
				EditorUtils_DataBlueprint.ShowBlueprintsPopup(
					BlueprintManager._pBlueprintsReadOnly,
					OnBlueprintSelected,
					_pDataBlueprintBehaviour._pBlueprint,
					nameFormatFunc: blueprint => blueprint.GetInheritancePath()
				);
			}

			// edit
			EditorGUI.BeginDisabledGroup(_pDataBlueprintBehaviour._pBlueprint == null);
			{
				if (GUILayout.Button("Edit", EditorStyles.miniButton, GUILayout.Width(50))) {
					EditorWindow_DataBlueprints.ShowWindow()._pSelectedBlueprint = _pDataBlueprintBehaviour._pBlueprint;
				}
			}
			EditorGUI.EndDisabledGroup();
		}
		EditorGUILayout.EndHorizontal();
	}



	private void Layout_BlueprintDetails(DataBlueprint blueprint)
	{
		if (blueprint == null) {
			return;
		}
		
		bool hasMatchingProperties = _pDataBlueprintBehaviour.HasPropertiesThatMatchBlueprint(blueprint);

		
		EditorGUI.BeginDisabledGroup(!hasMatchingProperties);
		{
			EditorGUI.BeginChangeCheck();
			{
				// initialisation stage
				_pDataBlueprintBehaviour._initialisationType =
					(EInitializeType)EditorGUILayout.EnumPopup(
						new GUIContent(
							"Set Values On:",
							!hasMatchingProperties ? "This blueprint has no properties that map to a field or property of this DataBlueprintBehaviour" : null
						),
						_pDataBlueprintBehaviour._initialisationType
					);
			}
			// pull values from blueprint if set to Awake or Start
			if (EditorGUI.EndChangeCheck() &&
				_pDataBlueprintBehaviour._initialisationType != EInitializeType.MANUAL
			) {
				_pDataBlueprintBehaviour.SetValuesFromBlueprint(blueprint);
			}
		}
		EditorGUI.EndDisabledGroup();


		// warning box if initialisation type is set to manual
		if (hasMatchingProperties &&
			_pDataBlueprintBehaviour._initialisationType == EInitializeType.MANUAL
		) {
			EditorGUILayout.HelpBox(
				"Values will not be set automatically. SetValuesFromBlueprint() must be called manually on this DataBlueprintBehaviour.",
				MessageType.Warning
			);
		}


		// details
		if (_blueprintDetailsFoldout = EditorGUILayout.Foldout(_blueprintDetailsFoldout, "Properties"))
		{
			// properties
			foreach (DataBlueprintProperty property in blueprint.GetAllProperties())
			{
				bool hasProperty = _pDataBlueprintBehaviour.HasDataBlueprintProperty(property);
				bool hasPropertyIgnoreType = _pDataBlueprintBehaviour.HasDataBlueprintProperty(property, true);


				// get icon and tooltip
				Texture2D icon;
				string tooltip;
					
				if (!hasProperty && hasPropertyIgnoreType)
				{
					icon = EditorUtils_DataBlueprint._pIconWarning;
					tooltip = string.Format(
						"This property matches this DataBlueprintBehaviour's field or property \"{0}\", but the types do not match.\n\nBlueprint type: {1}\nBehaviour type: {2}",
						property._objectMemberName,
						property._pSystemType,
						_pDataBlueprintBehaviour.GetFieldOrPropertyType(property._objectMemberName)
					);
				}
				else if (hasProperty)
				{
					icon = EditorUtils_DataBlueprint._pIconInfo;
					tooltip = string.Format(
						"The field or property \"{0}\" on this DataBlueprintBehaviour will be set to {1} {2}",
						property._objectMemberName,
						property._pIsArray ? property._pValues : property._pValue,
						_pDataBlueprintBehaviour._initialisationType != EInitializeType.MANUAL
							? "on " + _pDataBlueprintBehaviour._initialisationType.ToString().ToLower()
							: "when SetValuesFromBlueprint() is called"
					);
				}
				else
				{
					icon = EditorUtils_DataBlueprint._pIconBlank;
					tooltip = null;
				}
					

				GUIContent label = new GUIContent(" " + property._name, icon, tooltip);


				switch (property._pValueType)
				{
					case DataBlueprintProperty.ValueType.String:
						Layout_BlueprintProperty( property, label, 
							showValueGUI: () => {
								EditorGUILayout.TextField(label, (string)property._pValue);
							},
							showArrayValueGUI: i => {
								EditorGUILayout.TextField("Element " + i, (string)property._pValues[i]);
							} );
						break;

					case DataBlueprintProperty.ValueType.Int:
						Layout_BlueprintProperty( property, label, 
							showValueGUI: () => {
								EditorGUILayout.IntField(label, (int)property._pValue);
							},
							showArrayValueGUI: i => {
								EditorGUILayout.IntField("Element " + i, (int)property._pValues[i]);
							} );
						break;

					case DataBlueprintProperty.ValueType.Float:
						Layout_BlueprintProperty( property, label, 
							showValueGUI: () => {
								EditorGUILayout.FloatField(label, (float)property._pValue);
							},
							showArrayValueGUI: i => {
								EditorGUILayout.FloatField("Element " + i, (float)property._pValues[i]);
							} );
						break;

					case DataBlueprintProperty.ValueType.Bool:
						Layout_BlueprintProperty( property, label, 
							showValueGUI: () => {
								EditorGUILayout.Toggle(label, (bool)property._pValue);
							},
							showArrayValueGUI: i => {
								EditorGUILayout.Toggle("Element " + i, (bool)property._pValues[i]);
							} );
						break;

					case DataBlueprintProperty.ValueType.Prefab:
						Layout_BlueprintProperty( property, label, 
							showValueGUI: () => {
								EditorGUI.ObjectField(EditorGUILayout.GetControlRect(), label, (GameObject)property._pValue, typeof(GameObject), false);
							},
							showArrayValueGUI: i => {
								EditorGUI.ObjectField(EditorGUILayout.GetControlRect(), "Element " + i, (GameObject)property._pValues[i], typeof(GameObject), false);
							} );
						break;

					case DataBlueprintProperty.ValueType.Texture:
						Layout_BlueprintProperty( property, label, 
							showValueGUI: () => {
								EditorGUI.ObjectField(EditorGUILayout.GetControlRect(), label, (Texture2D)property._pValue, typeof(Texture2D), false);
							},
							showArrayValueGUI: i => {
								EditorGUI.ObjectField(EditorGUILayout.GetControlRect(), "Element " + i, (Texture2D)property._pValues[i], typeof(Texture2D), false);
							} );
						break;
						
					case DataBlueprintProperty.ValueType.AudioClip:
						Layout_BlueprintProperty( property, label, 
							showValueGUI: () => {
								EditorGUI.ObjectField(EditorGUILayout.GetControlRect(), label, (AudioClip)property._pValue, typeof(AudioClip), false);
							},
							showArrayValueGUI: i => {
								EditorGUI.ObjectField(EditorGUILayout.GetControlRect(), "Element " + i, (AudioClip)property._pValues[i], typeof(AudioClip), false);
							} );
						break;

					case DataBlueprintProperty.ValueType.Blueprint:
						Layout_BlueprintProperty( property, label, 
							showValueGUI: () => {
								EditorGUILayout.TextField(label, (string)property._pValue );
							},
							showArrayValueGUI: i => {
								EditorGUILayout.TextField("Element " + i, (string)property._pValues[i] );
							} );
						break;

					default:
						Debug.LogError(string.Format("Unhandled ValueType \"{0}\"", property._pValueType));
						break;
				}
			}
		}
	}


	private void Layout_BlueprintProperty( DataBlueprintProperty property, GUIContent label, System.Action showValueGUI, System.Action<int> showArrayValueGUI )
	{
		if ( property._pIsArray )
		{
			property._pIsArrayExpandedInEditor = EditorGUILayout.Foldout( property._pIsArrayExpandedInEditor, label );
			if ( property._pIsArrayExpandedInEditor )
			{
				EditorGUI.indentLevel++;
				for ( int i = 0; i < property._pValues.Count; ++i )
				{
					showArrayValueGUI( i );
				}
				EditorGUI.indentLevel--;
			}
		}
		else
		{
			showValueGUI();
		}
	}

	/// <summary>
	/// Shows a warning dialog if values were changed that will be overwritten by the blueprint
	/// </summary>
	private void SetValuesFromBlueprint_WithWarning(DataBlueprint blueprint)
	{
		if (blueprint == null) {
			return;
		}
		
		
		// get matching properties
		DataBlueprintProperty[] matchingProps = _pDataBlueprintBehaviour.GetPropertiesThatMatchBlueprint(blueprint);

		
		// store previous values
		List<object> prevValues = new List<object>();

		for (int i = 0; i < matchingProps.Length; ++i)
		{
			object value = _pDataBlueprintBehaviour.GetFieldOrProperty(matchingProps[i]._objectMemberName);

			if (value != null && value.ToString() == "null") {
				value = null;
			}
			prevValues.Add(value);
		}


		// set values from blueprint
		_pDataBlueprintBehaviour.SetValuesFromBlueprint(blueprint);
		

		// check if any properties were reverted
		for (int i = 0; i < matchingProps.Length; ++i)
		{
			bool	hasChanged = false;
			object	currValue = _pDataBlueprintBehaviour.GetFieldOrProperty(matchingProps[i]._objectMemberName);
			if (currValue != null && currValue.ToString() == "null") {
				currValue = null;
			}
			if ( currValue != null && currValue.IsArrayOrList() )
			{
				if ( prevValues[i] != null && prevValues[i].IsArrayOrList() )
				{
					int	currSize = currValue.GetArrayOrListSize();

					if ( currSize != prevValues[i].GetArrayOrListSize() )
					{
						hasChanged = true;
					}
					else
					{
						for ( int j = 0; j < currSize && !hasChanged; ++j )
						{
							hasChanged = !Equals(currValue.GetArrayOrListElement(j), prevValues[i].GetArrayOrListElement(j));
						}
					}
				}
				else
				{
					hasChanged = true;
				}
			}
			else if ( prevValues[i] != null && prevValues[i].IsArrayOrList() )
			{
				hasChanged = true;
			}
			else
			{
				hasChanged = !Equals(currValue, prevValues[i]);
			}

			if (hasChanged)
			{
				GUI.FocusControl(null);

				// show dialog if a property was reverted
				EditorUtility.DisplayDialog(
					"Warning",
					string.Format(
						"Modifying {0} in the inspector is disabled, as it will be set automatically by the blueprint's property \"{1}\" on {2}.\n\nChange \"Set Values On\" to {3} to change this value in the inspector.",
						matchingProps[i]._objectMemberName,
						matchingProps[i]._name,
						_pDataBlueprintBehaviour._initialisationType,
						EInitializeType.MANUAL
					),
					"OK"
				);
			}
		}
	}



	private void OnBlueprintSelected(DataBlueprint blueprint)
	{
		_pDataBlueprintBehaviour._pBlueprint = blueprint;

		// warn if _pDataBlueprintBehaviour has properties that will be overridden by this blueprint
		if (blueprint != null && _pDataBlueprintBehaviour.HasPropertiesThatMatchBlueprint(blueprint))
		{
			EditorUtility.DisplayDialog(
				"Warning",
				"This blueprint has properties that will overwrite members of this DataBlueprintBehaviour.\n\nThese properties will be indicated by white info icons when you close this dialog.",
				"OK"
			);

			_blueprintDetailsFoldout = true;
		}
	}

}
