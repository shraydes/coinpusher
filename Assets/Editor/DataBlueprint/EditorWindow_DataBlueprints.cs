﻿#if !UNITY_2018_3_OR_NEWER
#define HAS_UNITY_WWW
#endif

using DataBlueprints;
using LitJson;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using AmuzoEngine;

#if HAS_UNITY_WWW
using UnityWebRequestAPI = UnityEngine.WWW;
#else
using UnityWebRequestType = UnityEngine.Networking.UnityWebRequest;
#endif

public class EditorWindow_DataBlueprints : EditorWindow
{

	public enum EEditMode
	{
		Designer,
		Developer,
		Producer,
		ReadOnly = Producer
	}

	public enum EBlueprintListId
	{
		MAIN = 0,
		IMPORT
	}


	private const string EDIT_MODE_PREFS_STRING = "DataBlueprintsEditMode";
	private const float TOP_TAB_HEIGHT = 25;
	private const float SEPARATOR_WIDTH = 5;
	private const float TOP_BAR_HEIGHT = 21;
	private const float HEADING1_HEIGHT = 17;
	private const string TITLE = "Blueprints";
	private const float TREE_AREA_WIDTH = EditorUtils_DataBlueprint.DEFAULT_TREE_WIDTH;


	private Vector2 _scrollPosTree = Vector2.zero;
	private Vector2 _scrollPosEditor = Vector2.zero;
	private DataBlueprintSelection _selectedBlueprints = new DataBlueprintSelection();
	private EEditMode _editMode;
	private DataBlueprintEditModeSettings _currEditModeSettings;
	private string _searchString = "";
	private Object _test;



	[MenuItem( ( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Blueprints &B" ) )]
	static public EditorWindow_DataBlueprints ShowWindow()
	{
		// get window
		EditorWindow_DataBlueprints w =
			EditorWindow.GetWindow<EditorWindow_DataBlueprints>(
				TITLE,
				typeof( SceneView ),
				System.Type.GetType( "UnityEditor.GameView,UnityEditor" )
			);

		w.UpdateTitle();
		w.Show( true );
		return w;
	}



	void OnEnable()
	{
		// get edit mode
		int _prefsEditMode = EditorPrefs.GetInt( EDIT_MODE_PREFS_STRING, -1 );

		if( _prefsEditMode == -1 )
		{
			_pEditMode =
				( EEditMode )EditorUtility.DisplayDialogComplex(
					"Hello!", "What are you?                       ",
					Utils.FormatLikeInspectorVariable( ( EEditMode )0 ),
					Utils.FormatLikeInspectorVariable( ( EEditMode )1 ),
					Utils.FormatLikeInspectorVariable( ( EEditMode )2 )
				);
		}
		else
		{
			_pEditMode = ( EEditMode )_prefsEditMode;
		}
	}



	private void OnGUI()
	{
		// toolbar
		GUILayout.BeginArea( new Rect( 0, 0, Screen.width, TOP_BAR_HEIGHT ) );
		{
			Area_Toolbar();
		}
		GUILayout.EndArea();


        float   treeHeight = Screen.height - TOP_TAB_HEIGHT - TOP_BAR_HEIGHT;

        if ( BlueprintManager._pIsImporting )
        {
			treeHeight = 0.5f * treeHeight - TOP_BAR_HEIGHT;
        }

		// tree
		GUILayout.BeginArea( new Rect( 0, TOP_BAR_HEIGHT, TREE_AREA_WIDTH, treeHeight ) );
		{
			_scrollPosTree = GUILayout.BeginScrollView( _scrollPosTree, false, true );
			{
				Area_Tree( EBlueprintListId.MAIN, _currEditModeSettings );
			}
			GUILayout.EndScrollView();
		}
		GUILayout.EndArea();

 		DataBlueprintEditModeSettings	importListEditModeSettings = DataBlueprintEditModeSettings._presets[ EEditMode.ReadOnly ];

		if ( BlueprintManager._pIsImporting )
		{
			// import toolbar
			GUILayout.BeginArea( new Rect( 0, TOP_BAR_HEIGHT + treeHeight, TREE_AREA_WIDTH, TOP_BAR_HEIGHT ) );
			{
				Area_ImportToolbar();
			}
			GUILayout.EndArea();

			if ( BlueprintManager._pIsImporting )//import might have been ended by the import toolbar
			{
				// import tree
				GUILayout.BeginArea( new Rect( 0, TOP_BAR_HEIGHT + treeHeight + TOP_BAR_HEIGHT, TREE_AREA_WIDTH, treeHeight ) );
				{
					_scrollPosTree = GUILayout.BeginScrollView( _scrollPosTree, false, true );
					{
						Area_Tree( EBlueprintListId.IMPORT, importListEditModeSettings );
					}
					GUILayout.EndScrollView();
				}
				GUILayout.EndArea();
			}
		}

		// editor
		GUILayout.BeginArea( new Rect( TREE_AREA_WIDTH + SEPARATOR_WIDTH, TOP_BAR_HEIGHT, Screen.width - TREE_AREA_WIDTH - SEPARATOR_WIDTH, Screen.height - TOP_TAB_HEIGHT - TOP_BAR_HEIGHT ) );
		{
			Area_Editor( _pSelectedBlueprintListId == EBlueprintListId.IMPORT ? importListEditModeSettings : _currEditModeSettings );
		}
		GUILayout.EndArea();


		// repaint on undo
		if( Event.current.type == EventType.ValidateCommand &&
			Event.current.commandName == "UndoRedoPerformed"
		)
		{
			Repaint();
		}

		UpdateTitle();
	}


	private IDataBlueprintList GetBlueprintList( EBlueprintListId listId )
	{
		switch ( listId )
		{
		case EBlueprintListId.MAIN: return BlueprintManager._pBlueprintsReadOnly;
		case EBlueprintListId.IMPORT: return BlueprintManager._pImportList;
		default: return null;
		}
	}


	// ---------------------------------------- AREAS ----------------------------------------

	private void Area_Toolbar()
	{
		EditorGUILayout.BeginHorizontal();
		{
			// export
			if( GUILayout.Button( new GUIContent( "Export", "Export blueprints to JSON" ), EditorStyles.toolbarButton, GUILayout.Width( 50 ) ) )
			{
				string path = EditorUtility.SaveFilePanel( "Select Blueprints JSON File", "", "DataBlueprints.txt", "txt" );
				if ( !string.IsNullOrEmpty( path ) )
				{
					EditorUtils_BlueprintManager.ExportBlueprints( path );
				}
			}

			// import
			if( GUILayout.Button( new GUIContent( "Import", "Import blueprints from a json-formatted text file" ), EditorStyles.toolbarButton, GUILayout.Width( 50 ) ) )
			{
				string path = EditorUtility.OpenFilePanel( "Select Blueprints JSON File", "", "txt" );
				if( !string.IsNullOrEmpty( path ) )
				{
					ImportBlueprints( path );
				}
			}

			// new
			if( GUILayout.Button( new GUIContent( "Clear", "Remove all blueprints from this list" ), EditorStyles.toolbarButton, GUILayout.Width( 50 ) ) )
			{
				RegisterChange( "Clear Blueprints" );
				BlueprintManager.DeleteAllBlueprints();
			}

			// edit mode
			_pEditMode = ( EEditMode )EditorGUILayout.EnumPopup( _pEditMode, EditorStyles.toolbarDropDown, GUILayout.Width( 70 ) );

			// fill up bar
			GUILayout.Box( new GUIContent(), EditorStyles.toolbar, GUILayout.ExpandWidth( true ) );
		}
		EditorGUILayout.EndHorizontal();
	}



	private void Area_Tree( EBlueprintListId listId, DataBlueprintEditModeSettings editModeSettings )
	{
		// search bar
		EditorGUILayout.BeginHorizontal();
		{
			_searchString = GUILayout.TextField( _searchString, GUI.skin.FindStyle( "ToolbarSeachTextField" ) );

			if( GUILayout.Button( "", GUI.skin.FindStyle( string.IsNullOrEmpty( _searchString ) ? "ToolbarSeachCancelButtonEmpty" : "ToolbarSeachCancelButton" ) ) )
			{
				_searchString = "";
			}
		};
		EditorGUILayout.EndHorizontal();

		IReadOnlyList<DataBlueprint>	blueprints = GetBlueprintList( listId );

		bool	allowMultiSelect = listId == EBlueprintListId.IMPORT;

		// tree
		bool	hasSelectionChanged = EditorUtils_DataBlueprint.Tree(
			blueprints: blueprints,
			selectedList: _selectedBlueprints,
			allowMultiSelect: allowMultiSelect,
			showRightClick: editModeSettings._canAddRemoveBlueprints,
			allowDragging: editModeSettings._canEditBlueprintParent,
			onAdd: blueprint => _pSelectedBlueprint = blueprint,
			onDelete: blueprint => {
				if ( _pSelectedBlueprint == blueprint )
				{
					_pSelectedBlueprint = null;
				}
			},
			onChange: RegisterChange,
			searchString: _searchString
		);

		if ( hasSelectionChanged )
		{
			OnSelectionChanged();
		}

        if ( !BlueprintManager._pIsImporting )
        {
		    EditorGUILayout.Space();


		    // add blueprint
		    if( editModeSettings._canAddRemoveBlueprints )
		    {
			    if( GUILayout.Button( "+ Add Blueprint" ) )
			    {
				    RegisterChange( "Add Blueprint" );
				    _pSelectedBlueprint = BlueprintManager.AddBlueprint();
			    }
		    }
        }
	}


	private void Area_ImportToolbar()
	{
		EditorGUILayout.BeginHorizontal();
		{
			// import
			if( GUILayout.Button( new GUIContent( "Add", "Add selected blueprints" ), EditorStyles.toolbarButton, GUILayout.Width( 50 ) ) )
			{
				if ( _pSelectedBlueprintListId == EBlueprintListId.IMPORT && _selectedBlueprints.Count > 0 )
				{
					bool	hasMissingDependencies = SelectImportDependencies( _selectedBlueprints, dryRun:true );

					if ( hasMissingDependencies )
					{
						bool	isSelectDependencies = EditorUtility.DisplayDialog( 
							"Missing Dependencies", 
							"Do you want to also select the missing blueprint dependencies, or just add the current selection?", 
							"Select", 
							"Add"
						);

						if ( isSelectDependencies )
						{
							SelectImportDependencies( _selectedBlueprints, dryRun:false );
						}
						else
						{
							DoImport( _selectedBlueprints );
						}
					}
					else
					{
						DoImport( _selectedBlueprints );
					}
				}
			}

			// close
			if( GUILayout.Button( new GUIContent( "Close" ), EditorStyles.toolbarButton, GUILayout.Width( 50 ) ) )
			{
				BlueprintManager.EndImport();
			}

			// fill up bar
			GUILayout.Box( new GUIContent(), EditorStyles.toolbar, GUILayout.ExpandWidth( true ) );
		}
		EditorGUILayout.EndHorizontal();
	}


	private void Area_Editor( DataBlueprintEditModeSettings editModeSettings )
	{
		// check if there is a blueprint to edit
		if( _pSelectedBlueprint == null )
		{
			EditorGUILayout.LabelField( "<- Select blueprint to edit" );
			return;
		}


		// name
		EditorGUILayout.BeginHorizontal();
		{
			if( editModeSettings._canEditBlueprintName )
			{
				// label
				EditorGUILayout.LabelField( "Name", EditorStyles.boldLabel, GUILayout.Width( 50 ), GUILayout.Height( HEADING1_HEIGHT ) );

				// field
				string newName = GUILayout.TextField( _pSelectedBlueprint._pName, GUILayout.Width( 120 ) );

				// if changed, and not already in use..
				if( GetBlueprint( newName ) == null && !string.IsNullOrEmpty( newName ) )
				//if (newName != _pSelectedBlueprint._name)
				{
					RegisterChange( "Change Blueprint Name", _pSelectedBlueprint );

					_pSelectedBlueprint.SetName( newName );
				};
			}
			else
			{
				EditorGUILayout.LabelField(
					( editModeSettings._canEditPropertyValue ? "Editing: " : "Viewing: " ) + _pSelectedBlueprint._pName,
					EditorStyles.boldLabel,
					GUILayout.Height( HEADING1_HEIGHT )
				);
			}
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();


		// parent
		if( editModeSettings._canEditBlueprintParent )
		{
			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.LabelField( "Parent", EditorStyles.boldLabel, GUILayout.Width( 50 ), GUILayout.Height( HEADING1_HEIGHT ) );

				if(
					GUILayout.Button(
						DataBlueprint.NameOrNone( _pSelectedBlueprint.GetParent() ),
						EditorStyles.popup,
						GUILayout.Width( 120 )
					)
				)
				{
					EditorUtils_DataBlueprint.ShowBlueprintsPopup(
						_pSelectedBlueprintList,
						blueprint =>
						{
							if( _pSelectedBlueprint.GetParent() != blueprint )
							{
								RegisterChange( "Change Blueprint Parent", _pSelectedBlueprint );
								_pSelectedBlueprint.SetParent( blueprint );
							}
						},
						_pSelectedBlueprint.GetParent(),
						blueprint =>
						{
							return !blueprint.HasBlueprintAsAncestor( _pSelectedBlueprint );
						}, 
						nameFormatFunc:blueprint => {
							return blueprint.GetInheritancePath();
						}
					);
				}
			}
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Space();
		}


		// properties
		EditorGUILayout.LabelField( "Properties", EditorStyles.boldLabel, GUILayout.Height( HEADING1_HEIGHT ) );

		EditorUtils_DataBlueprint._pPropertyIndentLevel = 0;

		if ( _pSelectedBlueprintList != null )
		{
			for ( int i = 0; i < _pSelectedBlueprintList.Count; ++i )
			{
				_pSelectedBlueprintList[i]._pHasRenderedProperties = false;
			}
		}

		_scrollPosEditor = GUILayout.BeginScrollView( _scrollPosEditor );

		EditorUtils_DataBlueprint.Layout_BlueprintPropertyList( 
			_pSelectedBlueprintList,
			_pSelectedBlueprint,
			editModeSettings,
			RegisterChange,
			_pStylePropertiesSubSectionHeader, 
			showHeaders: true,
			showOurs: true
		);

		GUILayout.EndScrollView();
	}





	// -------------------------------------------------------------------------------------------------

	private void RegisterChange( string name, DataBlueprint blueprint = null )
	{
		//Debug.LogError( "Blueprint change: " + name );
		Undo.RecordObject( BlueprintManager._pInstance, name );
		BlueprintManager.SetDirty();
	}



	private void ImportBlueprints( string filePath )
	{
		if ( BlueprintManager._pIsImporting )
		{
			BlueprintManager.EndImport();
		}

		// load text
		UnityWebRequestType www = new UnityWebRequestType( FileHelper.AddLocalFileUrlScheme( filePath ) );
		while( !www.isDone )
			;

		// parse text
		#if HAS_UNITY_WWW
		string	jsonText = www.text;
		#else
		string	jsonText = www.downloadHandler.text;
		#endif

		JsonData json = JsonMapper.ToObject( new JsonReader( jsonText ) );

		BlueprintManager.BeginImport( json );

		//EditorWindow_ImportBlueprints.ShowWindow( json );
	}

	private bool SelectImportDependencies( IList<DataBlueprint> selection, bool dryRun )
	{
		int	addCount = 0;

		System.Action<DataBlueprint>	recursiveAddBlueprint = null;

		recursiveAddBlueprint = bp => {
			if ( bp == null ) return;
			if ( !selection.Contains( bp ) )
			{
				if ( !dryRun )
				{
					selection.Add( bp );
				}
				++addCount;
				DataBlueprint	bpp = bp.GetParent();
				if ( bpp != null )
				{
					recursiveAddBlueprint( bpp );
				}
			}
		};

		DataBlueprint	blueprint;
		DataBlueprintProperty	property;

		for ( int i = 0; i < selection.Count; ++i )
		{
			blueprint = selection[i];

			for ( int j = 0; j < blueprint._pPropertiesReadOnly.Count; ++j )
			{
				property = blueprint._pPropertiesReadOnly[j];

				if ( property._pValueType == DataBlueprintProperty.ValueType.Blueprint )
				{
					if ( property._pIsArray )
					{
						for ( int k = 0; k < property._pValues.Count; ++k )
						{
							recursiveAddBlueprint( BlueprintManager.GetBlueprint( (string)property._pValues[k] ) );
						}
					}
					else
					{
						recursiveAddBlueprint( BlueprintManager.GetBlueprint( (string)property._pValue ) );
					}
				}
			}
		}

		return addCount > 0;
	}

	private static void DoRename( IReadOnlyList<DataBlueprint> importList )
	{
		DataBlueprint	importBlueprint;

		for ( int i = 0; i < importList.Count; ++i )
		{
			importBlueprint = importList[i];

			if ( importBlueprint == null ) continue;

			DataBlueprint	existingBlueprint = BlueprintManager.GetBlueprint( importBlueprint._pName );

			if ( existingBlueprint != null )
			{
				bool	isRename = EditorUtility.DisplayDialog(
					"Name Conflict",
					string.Format( "There is already a blueprint named \"{0}\" in this project.  Do you wish to rename this imported blueprint?", importBlueprint._pName ),
					"Yes",
					"No"
				);
			
				if ( isRename )
				{
					string	newName = BlueprintManager.GetNextDuplicatedBlueprintName( 
						importBlueprint._pName, 
						new IReadOnlyList<DataBlueprint>[] { BlueprintManager._pBlueprintsReadOnly, importList }
					);
					importBlueprint.SetName( newName );
				}
			}
		}
	}

	private static void DoImport( IReadOnlyList<DataBlueprint> importList )
	{
		DoRename( importList );

		BlueprintManager.BeginChanges();

		DataBlueprint	importBlueprint;

		for ( int i = 0; i < importList.Count; ++i )
		{
			importBlueprint = importList[i];

			if ( importBlueprint == null ) continue;

			DataBlueprint	existingBlueprint = BlueprintManager.GetBlueprint( importBlueprint._pName );

			if ( existingBlueprint != null )
			{
				bool	isReplace = EditorUtility.DisplayDialog(
					"Name Conflict",
					string.Format( "There is already a blueprint named \"{0}\" in this project.  Do you wish to replace it?", importBlueprint._pName ),
					"Yes",
					"No"
				);
			
				if ( isReplace )
				{
					DataBlueprint	newBlueprint = BlueprintManager.NewBlueprint( importBlueprint );
					BlueprintManager.ReplaceBlueprint( existingBlueprint, newBlueprint );
				}
			}
			else
			{
				BlueprintManager.CopyBlueprint( importBlueprint );
			}
		}

		BlueprintManager.EndChanges();
	}

	private void OnDestroy()
	{
	}



	private void UpdateTitle()
	{
		this.SetTitleContent(
			TITLE,
			EditorUtils_DataBlueprint._pIconBlueprints
		);
	}





	// ---------------------------------------------- PROPERTIES ----------------------------------------------

	private EEditMode _pEditMode
	{
		get
		{
			return _editMode;
		}

		set
		{
			_currEditModeSettings = DataBlueprintEditModeSettings._presets[value];
			_editMode = value;
			EditorPrefs.SetInt( EDIT_MODE_PREFS_STRING, ( int )value );
		}
	}

	private EBlueprintListId _pSelectedBlueprintListId { get; set; }

	private void OnSelectionChanged()
	{
		IDataBlueprintListReadOnly	selectedList = ValidateSelectedBlueprints();

		if ( selectedList != null )
		{
			if ( selectedList == BlueprintManager._pBlueprintsReadOnly )
			{
				_pSelectedBlueprintListId = EBlueprintListId.MAIN;
			}
			else if ( selectedList == BlueprintManager._pImportList )
			{
				_pSelectedBlueprintListId = EBlueprintListId.IMPORT;
			}
			else
			{
				throw new System.Exception( "Unrecognized blueprint list" );
			}
		}

		GUI.FocusControl( null );
	}

	private IDataBlueprintList ValidateSelectedBlueprints()
	{
		IDataBlueprintList	list = null;

		for ( int i = 0; i < _selectedBlueprints.Count; ++i )
		{
			if ( list == null )
			{
				list = _selectedBlueprints[i]._pBlueprintList;
			}
			else if ( list != _selectedBlueprints[i]._pBlueprintList )
			{
				_selectedBlueprints.RemoveAt(i--);
			}
		}

		return list;
	}

	private IDataBlueprintList _pSelectedBlueprintList
	{
		get
		{
			return GetBlueprintList( _pSelectedBlueprintListId );
		}
	}

	public DataBlueprint _pSelectedBlueprint
	{
		get
		{
			return _selectedBlueprints.Count == 1 ? _selectedBlueprints[0] : null;
		}
		set
		{
			bool	hasChanged = false;
			if ( value == null )
			{
				if ( _selectedBlueprints.Count > 0 )
				{
					_selectedBlueprints.Clear();
					hasChanged = true;
				}
			}
			else if ( _selectedBlueprints.Count == 0 )
			{
				_selectedBlueprints.Add( value );
				hasChanged = true;
			}
			else
			{
				if ( _selectedBlueprints.Count > 1 )
				{
					_selectedBlueprints.RemoveRange( 1, _selectedBlueprints.Count - 1 );
					hasChanged = true;
				}
				if ( _selectedBlueprints[0] != value )
				{
					_selectedBlueprints[0] = value;
					hasChanged = true;
				}
			}
			if ( hasChanged )
			{
				OnSelectionChanged();
			}
		}
	}

	private DataBlueprint GetBlueprint( string name )
	{
		IDataBlueprintListReadOnly	blueprintList = _pSelectedBlueprintList;
		return blueprintList != null ? blueprintList.GetBlueprint( name ) : BlueprintManager.GetBlueprint( name );
	}


	private static GUIStyle _stylePropertiesSubSectionHeader;
	public static GUIStyle _pStylePropertiesSubSectionHeader
	{
		get
		{
			if( _stylePropertiesSubSectionHeader == null )
			{
				_stylePropertiesSubSectionHeader = new GUIStyle( "Label" );
				_stylePropertiesSubSectionHeader.fontStyle = FontStyle.Italic;
				_stylePropertiesSubSectionHeader.normal.textColor = Color.grey;
			}

			return _stylePropertiesSubSectionHeader;
		}
	}


}
