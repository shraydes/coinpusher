﻿using System.Reflection;
using UnityEditor;
using UnityEngine;


public static class EditorExtensions
{

	private static readonly PropertyInfo _windowContentPropertyInfo =
		typeof(EditorWindow).GetProperty(
			"cachedTitleContent",
			BindingFlags.Instance | BindingFlags.NonPublic
		);



	public static void SetTitleContent(this EditorWindow window, string text, Texture icon)
	{
		if (_windowContentPropertyInfo == null)
			return;
		if (window == null)
			return;

		GUIContent titleContent = (GUIContent)_windowContentPropertyInfo.GetValue(window, null);
		
		// set text
		if (titleContent.text != text) {
			titleContent.text = text;
		}

		// set icon
		if (titleContent.image != icon) {
			titleContent.image = icon;
		}
	}

	
}
