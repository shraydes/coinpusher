﻿using UnityEngine;
using UnityEditor;


namespace UnityEditor
{

	public class EditorUtils
	{

		/// <summary>
		/// Draws a text field, but shows emptyMessage if text is empty
		/// </summary>
		public static string TextFieldWithEmptyMessage(Rect? rect, string text, string emptyMessage, params GUILayoutOption[] options)
		{
			Rect textFieldRect = rect.HasValue ? rect.Value : EditorGUILayout.GetControlRect(false, options);

			text = EditorGUI.TextField(textFieldRect, text);

			if (string.IsNullOrEmpty(text)) {
				EditorGUI.LabelField(textFieldRect, emptyMessage, _pStyleNoValueLabel);
			}

			return text;
		}





		// ------------------------------------------------ STYLES ------------------------------------------------

		private static GUIStyle _styleEmptyValueBox;
		public static GUIStyle _pStyleNoValueLabel
		{
			get
			{
				if (_styleEmptyValueBox == null)
				{
					_styleEmptyValueBox = new GUIStyle("textField");
					_styleEmptyValueBox.fontStyle = FontStyle.Italic;
					_styleEmptyValueBox.normal.textColor = Color.gray;
					_styleEmptyValueBox.normal.background = null;
				}

				return _styleEmptyValueBox;
			}
		}
	
	}
}