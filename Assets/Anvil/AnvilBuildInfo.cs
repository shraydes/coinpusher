using UnityEngine;

using System.Collections.Generic;

public static class AnvilBuildInfo
{
	private static List<string> keys;
	private static List<string> values;
	private static string data;
	private static int _width = -1;
	private static int _height = -1;

	public static void Initialize(string data)
	{
		string[] lines = data.Split(new string[]{"\r\n", "\n"}, System.StringSplitOptions.None);
		keys = new List<string>();
		values = new List<string>();
		AnvilBuildInfo.data = data;

		for(int i = 0; i < lines.Length; i++)
		{
			string[] keyVal = lines[i].Split('=');

			if(keyVal.Length < 1) continue;
			keys.Add(keyVal[0]);

			Debug.Log("Anvil added key: " + keyVal[0]);

			if(keyVal.Length < 2)
			{
				values.Add("");
			}
			else
			{
				values.Add(keyVal[1]);
				Debug.Log("Anvil added value: " + keyVal[1]);
			}
		}
	}

	private static void Initialize()
	{
		if(keys != null)
		{
			return;
		}

		TextAsset te = Resources.Load("BuildInfo") as TextAsset;

		if(te == null)
		{
			Initialize("");
		}
		else
		{
			Initialize(te.text);
		}
	}

	public static string GetVar(string key, string defaultValue)
	{
    Initialize();

		for(int i = 0; i < keys.Count; i++)
		{
			if(keys[i] == key)
			{
				if(values[i] == "") return defaultValue;
				return values[i];
			}
		}

    return defaultValue;
	}

	public static string GetDefine(string partialKey, string defaultValue)
	{
		Initialize();

		string definesVar = GetVar("DEFINES", "");
		string[] defines = definesVar.Split(' ');

		for(int i = 0; i < defines.Length; i++)
		{
			if(defines[i].Contains(partialKey) == true)
			{
				return defines[i].Substring(partialKey.Length);
			}
		}

		return "";
	}

	public static string GetData()
	{
		return data;
	}

	// **************  Convinience getter properties *****************

	public static string _pBuildDate
	{
		get
		{
			return AnvilBuildInfo.GetVar( "BUILD_DATE", "1985-08-26 01:20:00" );
		}
	}

	public static string _pBambooVersion
	{
		get
		{
			return AnvilBuildInfo.GetVar( "BAMBOO_VERSION", "0" );
		}
	}

	public static string _pProject
	{
		get
		{
			return AnvilBuildInfo.GetVar( "PROJECT", "OURTEC" );
		}
	}

	public static string _pProjectCode
	{
		get
		{
			return AnvilBuildInfo.GetVar( "PROJECT_CODE", "OURTEC" );
		}
	}

	public static string _pBuildId
	{
		get
		{
			return AnvilBuildInfo.GetVar( "BUILD_ID", "Internal" );
		}
	}

	public static string _pTitle
	{
		get
		{
			return AnvilBuildInfo.GetVar( "TITLE", "Project Title" );
		}
	}

	public static string _pPlatform
	{
		get
		{
			return AnvilBuildInfo.GetVar( "PLATFORM", "Web" );
		}
	}

	public static string _pOsVersion
	{
		get
		{
			return AnvilBuildInfo.GetVar( "OS_VERSION", "" );
		}
	}

	public static string _pVersion
	{
		get
		{
			return AnvilBuildInfo.GetVar( "VERSION", "0" );
		}
	}

	public static string _pBundleVersion
	{
		get
		{
			return AnvilBuildInfo.GetVar( "BUNDLE_VERSION", "0" );
		}
	}

	public static string _pDeviceCapabilities
	{
		get
		{
			return AnvilBuildInfo.GetVar( "DEVICE_CAPS", "" );
		}
	}

	public static int _pWidth
	{
		get
		{
			if( _width != -1 )
			{
				return _width;
			}

			string temp = AnvilBuildInfo.GetVar( "WIDTH", Screen.width.ToString() );

			_width = Screen.width;

			int.TryParse( temp, out _width );
			
			return _width;
		}
	}

	public static int _pHeight
	{
		get
		{
			if( _height != -1 )
			{
				return _height;
			}
			
			string temp = AnvilBuildInfo.GetVar( "HEIGHT", Screen.height.ToString() );

			_height = Screen.height;

			int.TryParse( temp, out _height );
			
			return _height;
		}
	}

	public static string _pColour
	{
		get
		{
			return AnvilBuildInfo.GetVar( "COLOR", "#000" );
		}
	}

	public static string _pBundleId
	{
		get
		{
			return AnvilBuildInfo.GetVar( "BUNDLE_ID", "" );
		}
	}

	public static string _pCertificate
	{
		get
		{
			return AnvilBuildInfo.GetVar( "CERTIFICATE", "" );
		}
	}

	public static string _pAppName
	{
		get
		{
			return AnvilBuildInfo.GetVar( "APP_NAME", "OURTEC" );
		}
	}

	public static bool _pSplitApk
	{
		get
		{
			bool returnBool = false;

			bool.TryParse( AnvilBuildInfo.GetVar( "SPLIT_APK", "false" ), out returnBool );

			return returnBool;
		}
	}

	public static string _pDefines
	{
		get
		{
			return AnvilBuildInfo.GetVar( "DEFINES", "" );
		}
	}
}