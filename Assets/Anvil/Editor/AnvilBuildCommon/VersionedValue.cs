﻿using System;
using System.Collections.Generic;

namespace AnvilBuildCommon
{
	public class VersionedValue
	{
		private struct VersionValuePair
		{
			public string	_version;
			public string	_value;
		}

		private List<VersionValuePair>	_versionValuePairs = new List<VersionValuePair>();

		public string Get( Predicate<string> matcher, out string gotVersion )
		{
			if ( matcher == null ) throw new ArgumentNullException( "matcher" );

			gotVersion = null;

			for ( int i = 0; i < _versionValuePairs.Count; ++i )
			{
				if ( string.IsNullOrEmpty( _versionValuePairs[i]._version ) ) continue;

				if ( matcher( _versionValuePairs[i]._version ) )
				{
					gotVersion = _versionValuePairs[i]._version;
					return _versionValuePairs[i]._value;
				}
			}

			return null;
		}

		public string Get( string wantVersion, out string gotVersion )
		{
			return Get( version => version.StartsWith( wantVersion ), out gotVersion );
		}

		public void Set( string version, string value )
		{
			VersionValuePair	newPair = new VersionValuePair();
			newPair._version = version;
			newPair._value = value;

			_versionValuePairs.Add( newPair );
		}
	}
}

