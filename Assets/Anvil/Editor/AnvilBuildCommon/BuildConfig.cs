﻿using System;
using System.Text;
using System.Collections.Generic;
using LitJson;

namespace AnvilBuildCommon
{
	public class BuildConfig
	{
		public const string	ENVIRONMENT_VARIABLE = "UNITY_BUILD_CONFIG";


		public class BuildOptions
		{
			public bool?	_pIsDevelopment	{ get; set; }

			public bool?	_pIsAllowDebugging	{ get; set; }
		}

		public class WebPlayerOptions
		{
			public string	_pDirectory	{ get; set; }
		}

		public class WebGLOptions
		{
			public string	_pDirectory	{ get; set; }
		}

		public class AndroidOptions
		{
			public string	_pKeystore	{ get; set; }

			private List<string>	_blockPermissions = new List<string>();
			public List<string>		_pBlockPermissions	{ get { return _blockPermissions; } }

			public bool?	_pIsOutputGradleProject { get; set; }

			public int?		_pRebuildApk	{ get; set; }

			public string	_pApktoolPath	{ get; set; }

			public string	_pBuildToolsVersion	{ get; set; }

			public string	_pBuildToolsPath	{ get; set; }
		}

		public class JdkOptions
		{
			public string	_pVersion	{ get; set; }

			public string	_pPath	{ get; set; }
		}

		public class IosOptions
		{
			public string	_pXcodeVersion	{ get; set; }

			public string	_pXcodeAppPath	{ get; set; }

			public int?		_pXcodeProjectAction	{ get; set; }

			public bool?	_pIsManualXcodeBuild	{ get; set; }

			public string	_pDistributionMethod	{ get; set; }

			public string	_pProvisioningProfile	{ get; set; }

			public string	_pKeychainPassword	{ get; set; }

			public bool?	_pIsEnableBitcode	{ get; set; }

			public bool?	_pIsEnableSwift	{ get; set; }

			public string	_pSwiftVersion	{ get; set; }

			public bool?	_pIsEnableOnDemandResources	{ get; set; }

			public bool?	_pIsEmbedOnDemandResources	{ get; set; }

			public string	_pOnDemandResourcesBaseUrl { get; set; }
		}

		public class UwpOptions
		{
			public string	_pMSBuildVersion	{ get; set; }

			public string	_pMSBuildPath	{ get; set; }

			public string	_pWindowsSdkVersion	{ get; set; }

			public string	_pWindowsSdkToolsPath	{ get; set; }

			public int?		_pVsProjectAction	{ get; set; }

			public string	_pBuildConfiguration { get; set; }

			public string	_pBuildPlatforms { get; set; }

			public bool?	_pIsSaveMSBuildLog { get; set; }

			public string	_pCertificate { get; set; }

			public bool?	_pIsCreateAppxBundle { get; set; }

			public bool?	_pIsStoreUpload { get; set; }

			public int?		_pDefaultAssetScale { get; set; }
		}

		public class SwitchOptions
		{
			public bool?	_pIsCreateRom { get; set; }
		}


		private const string	DEFAULT_ANVIL_BUILD_METHOD = "AnvilBuild.Build";

		public const string		WEB_DEMO_DIR_ALIAS = "*DEMO_DIR";

		public string	_pUnityVersion	{ get; set; }

		public string	_pUnityExe	{ get; set; }

		public string	_pBuildTarget	{ get; set; }

		public string	_pProductCode	{ get; set; }

		public string	_pBuildId	{ get; set; }

		public string	_pBuildVersion	{ get; set; }

		public string	_pExecuteMethod	{ get; set; }

		public string	_pOutputName	{ get; set; }

		public string	_pBundleId	{ get; set; }

		public string	_pDisplayName	{ get; set; }

		public int?		_pAssetBundleAction	{ get; set; }

		public bool?	_pIsDeleteLastBuild	{ get; set; }

		private BuildOptions	_buildOptions = new BuildOptions();
		public BuildOptions		_pBuildOptions	{ get { return _buildOptions; } }

		private WebPlayerOptions	_webPlayerOptions = new WebPlayerOptions();
		public WebPlayerOptions		_pWebPlayerOptions	{ get { return _webPlayerOptions; } }

		private WebGLOptions	_webGLOptions = new WebGLOptions();
		public WebGLOptions		_pWebGLOptions	{ get { return _webGLOptions; } }

		private AndroidOptions	_androidOptions = new AndroidOptions();
		public AndroidOptions	_pAndroidOptions	{ get { return _androidOptions; } }

		private JdkOptions	_jdkOptions = new JdkOptions();
		public JdkOptions	_pJdkOptions	{ get { return _jdkOptions; } }

		private IosOptions	_iosOptions = new IosOptions();
		public IosOptions	_pIosOptions	{ get { return _iosOptions; } }

		private UwpOptions	_uwpOptions = new UwpOptions();
		public UwpOptions	_pUwpOptions	{ get { return _uwpOptions; } }

		private SwitchOptions	_switchOptions = new SwitchOptions();
		public SwitchOptions	_pSwitchOptions	{ get { return _switchOptions; } }

		private List<string>	_defines = new List<string>();
		public List<string>		_pDefines	{ get { return _defines; } }

		public bool	_pIsValid
		{
			get
			{
				if ( string.IsNullOrEmpty( _pBuildTarget ) ) return false;
				if ( string.IsNullOrEmpty( _pProductCode ) ) return false;
				if ( string.IsNullOrEmpty( _pBuildId ) ) return false;
				return true;
			}
		}


		public BuildConfig()
		{
			_pUnityVersion = null;
			_pUnityExe = null;
			_pBuildTarget = null;
			_pProductCode = null;
			_pBuildId = null;
			_pBuildVersion = null;
			_pExecuteMethod = DEFAULT_ANVIL_BUILD_METHOD;
			_pOutputName = null;
			_pBundleId = null;
			_pDisplayName = null;
			_pAssetBundleAction = null;
			_pIsDeleteLastBuild = null;
			_buildOptions._pIsDevelopment = null;
			_buildOptions._pIsAllowDebugging = null;
			_webPlayerOptions._pDirectory = null;
			_webGLOptions._pDirectory = null;
			_androidOptions._pKeystore = null;
			_androidOptions._pBlockPermissions.Clear();
			_androidOptions._pIsOutputGradleProject = null;
			_androidOptions._pApktoolPath = null;
			_androidOptions._pBuildToolsVersion = null;
			_androidOptions._pBuildToolsPath = null;
			_jdkOptions._pVersion = null;
			_jdkOptions._pPath = null;
			_iosOptions._pXcodeVersion = null;
			_iosOptions._pXcodeAppPath = null;
			_iosOptions._pXcodeProjectAction = null;
			_iosOptions._pIsManualXcodeBuild = null;
			_iosOptions._pDistributionMethod = null;
			_iosOptions._pProvisioningProfile = null;
			_iosOptions._pIsEnableOnDemandResources = null;
			_iosOptions._pIsEmbedOnDemandResources = null;
			_iosOptions._pOnDemandResourcesBaseUrl = null;
			_iosOptions._pKeychainPassword = null;
			_iosOptions._pIsEnableBitcode = null;
			_iosOptions._pIsEnableSwift = null;
			_iosOptions._pSwiftVersion = null;
			_uwpOptions._pMSBuildVersion = null;
			_uwpOptions._pMSBuildPath = null;
			_uwpOptions._pWindowsSdkVersion = null;
			_uwpOptions._pWindowsSdkToolsPath = null;
			_uwpOptions._pVsProjectAction = null;
			_uwpOptions._pBuildConfiguration = null;
			_uwpOptions._pBuildPlatforms = null;
			_uwpOptions._pIsSaveMSBuildLog = null;
			_uwpOptions._pCertificate = null;
			_uwpOptions._pIsCreateAppxBundle = null;
			_uwpOptions._pIsStoreUpload = null;
			_uwpOptions._pDefaultAssetScale = null;
			_switchOptions._pIsCreateRom = null;
		}

		public string ToJson( string unityVersion )
		{
			StringBuilder	sb = new StringBuilder();
			JsonWriter		jw = new JsonWriter( sb );

			jw.PrettyPrint = true;
			jw.WriteObjectStart();

			if ( !string.IsNullOrEmpty( _pProductCode ) )
			{
				jw.WritePropertyName( "productCode" );
				jw.Write( _pProductCode );
			}

			if ( !string.IsNullOrEmpty( _pBuildId ) )
			{
				jw.WritePropertyName( "buildId" );
				jw.Write( _pBuildId );
			}

			if ( !string.IsNullOrEmpty( _pBuildVersion ) )
			{
				jw.WritePropertyName( "buildVersion" );
				jw.Write( _pBuildVersion );
			}

			if ( !string.IsNullOrEmpty( _pOutputName ) )
			{
				jw.WritePropertyName( "outputName" );
				jw.Write( _pOutputName );
			}

			if ( !string.IsNullOrEmpty( _pBundleId ) )
			{
				jw.WritePropertyName( "bundleId" );
				jw.Write( _pBundleId );
			}

			if ( !string.IsNullOrEmpty( _pDisplayName ) )
			{
				jw.WritePropertyName( "displayName" );
				jw.Write( _pDisplayName );
			}

			if ( _pAssetBundleAction.HasValue )
			{
				jw.WritePropertyName( "assetBundleAction" );
				jw.Write( _pAssetBundleAction.Value );
			}

			if ( _pIsDeleteLastBuild.HasValue )
			{
				jw.WritePropertyName( "deleteLastBuild" );
				jw.Write( _pIsDeleteLastBuild.Value );
			}

			if ( _pDefines.Count > 0 )
			{
				jw.WritePropertyName( "defines" );
				jw.WriteArrayStart();

				for ( int i = 0; i < _pDefines.Count; ++i )
				{
					jw.Write( _pDefines[i] );
				}

				jw.WriteArrayEnd();
			}

			if ( _pBuildOptions._pIsDevelopment.HasValue || _pBuildOptions._pIsAllowDebugging.HasValue )
			{
				jw.WritePropertyName( "buildOpts" );
				jw.WriteObjectStart();

				if ( _pBuildOptions._pIsDevelopment.HasValue )
				{
					jw.WritePropertyName( "development" );
					jw.Write( _pBuildOptions._pIsDevelopment.Value );
				}

				if ( _pBuildOptions._pIsAllowDebugging.HasValue )
				{
					jw.WritePropertyName( "allowDebugging" );
					jw.Write( _pBuildOptions._pIsAllowDebugging.Value );
				}

				jw.WriteObjectEnd();
			}

			if ( !string.IsNullOrEmpty( _pWebPlayerOptions._pDirectory ) )
			{
				jw.WritePropertyName( "webPlayer" );
				jw.WriteObjectStart();

				if ( !string.IsNullOrEmpty( _pWebPlayerOptions._pDirectory ) )
				{
					jw.WritePropertyName( "dir" );
					jw.Write( _pWebPlayerOptions._pDirectory );
				}

				jw.WriteObjectEnd();
			}

			if ( !string.IsNullOrEmpty( _pWebGLOptions._pDirectory ) )
			{
				jw.WritePropertyName( "webGL" );
				jw.WriteObjectStart();

				if ( !string.IsNullOrEmpty( _pWebGLOptions._pDirectory ) )
				{
					jw.WritePropertyName( "dir" );
					jw.Write( _pWebGLOptions._pDirectory );
				}

				jw.WriteObjectEnd();
			}

			if ( !string.IsNullOrEmpty( _pAndroidOptions._pKeystore ) || _pAndroidOptions._pBlockPermissions.Count > 0 || _pAndroidOptions._pIsOutputGradleProject.HasValue || _pAndroidOptions._pRebuildApk.HasValue || !string.IsNullOrEmpty( _pAndroidOptions._pApktoolPath ) || !string.IsNullOrEmpty( _pAndroidOptions._pBuildToolsVersion ) || !string.IsNullOrEmpty( _pAndroidOptions._pBuildToolsPath ) )
			{
				jw.WritePropertyName( "android" );
				jw.WriteObjectStart();

				if ( !string.IsNullOrEmpty( _pAndroidOptions._pKeystore ) )
				{
					jw.WritePropertyName( "keystore" );
					jw.Write( _pAndroidOptions._pKeystore );
				}

				if ( _pAndroidOptions._pBlockPermissions.Count > 0 )
				{
					jw.WritePropertyName( "blockPermissions" );
					jw.WriteArrayStart();
					for ( int i = 0; i < _pAndroidOptions._pBlockPermissions.Count; ++i )
					{
						if ( !string.IsNullOrEmpty( _pAndroidOptions._pBlockPermissions[i] ) )
						{
							jw.Write( _pAndroidOptions._pBlockPermissions[i] );
						}
					}
					jw.WriteArrayEnd();
				}

				if ( _pAndroidOptions._pIsOutputGradleProject.HasValue )
				{
					jw.WritePropertyName( "outputGradleProject" );
					jw.Write( _pAndroidOptions._pIsOutputGradleProject.Value );
				}

				if ( _pAndroidOptions._pRebuildApk.HasValue )
				{
					jw.WritePropertyName( "rebuildApk" );
					jw.Write( _pAndroidOptions._pRebuildApk.Value );
				}

				if ( !string.IsNullOrEmpty( _pAndroidOptions._pApktoolPath ) )
				{
					jw.WritePropertyName( "apktoolPath" );
					jw.Write( _pAndroidOptions._pApktoolPath );
				}

				if ( !string.IsNullOrEmpty( _pAndroidOptions._pBuildToolsPath ) )
				{
					jw.WritePropertyName( "buildToolsPath" );
					jw.Write( _pAndroidOptions._pBuildToolsPath );
				}

				if ( !string.IsNullOrEmpty( _pAndroidOptions._pBuildToolsVersion ) )
				{
					jw.WritePropertyName( "buildToolsVersion" );
					jw.Write( _pAndroidOptions._pBuildToolsVersion );
				}

				jw.WriteObjectEnd();
			}

			if ( !string.IsNullOrEmpty( _pJdkOptions._pVersion ) || !string.IsNullOrEmpty( _pJdkOptions._pPath ) )
			{
				jw.WritePropertyName( "jdk" );
				jw.WriteObjectStart();

				if ( !string.IsNullOrEmpty( _pJdkOptions._pPath ) )
				{
					jw.WritePropertyName( "path" );
					jw.Write( _pJdkOptions._pPath );
				}

				if ( !string.IsNullOrEmpty( _pJdkOptions._pVersion ) )
				{
					jw.WritePropertyName( "version" );
					jw.Write( _pJdkOptions._pVersion );
				}

				jw.WriteObjectEnd();
			}

			if ( !string.IsNullOrEmpty( _pIosOptions._pXcodeVersion ) || !string.IsNullOrEmpty( _pIosOptions._pXcodeAppPath ) || _pIosOptions._pXcodeProjectAction.HasValue || _pIosOptions._pIsManualXcodeBuild.HasValue || !string.IsNullOrEmpty(_pIosOptions._pDistributionMethod ) || !string.IsNullOrEmpty( _pIosOptions._pProvisioningProfile ) || _pIosOptions._pIsEnableOnDemandResources.HasValue || _pIosOptions._pIsEmbedOnDemandResources.HasValue || !string.IsNullOrEmpty( _pIosOptions._pOnDemandResourcesBaseUrl ) || !string.IsNullOrEmpty( _pIosOptions._pKeychainPassword ) || _pIosOptions._pIsEnableBitcode.HasValue || _pIosOptions._pIsEnableSwift.HasValue || !string.IsNullOrEmpty( _pIosOptions._pSwiftVersion ) )
			{
				jw.WritePropertyName( "iOS" );
				jw.WriteObjectStart();

				if ( !string.IsNullOrEmpty( _pIosOptions._pXcodeAppPath ) )
				{
					jw.WritePropertyName( "xcodeAppPath" );
					jw.Write( _pIosOptions._pXcodeAppPath );
				}

				if ( !string.IsNullOrEmpty( _pIosOptions._pXcodeVersion ) )
				{
					jw.WritePropertyName( "xcodeVersion" );
					jw.Write( _pIosOptions._pXcodeVersion );
				}

				if ( _pIosOptions._pXcodeProjectAction.HasValue )
				{
					jw.WritePropertyName( "xcodeProjectAction" );
					jw.Write( _pIosOptions._pXcodeProjectAction.Value );
				}

				if ( _pIosOptions._pIsManualXcodeBuild.HasValue )
				{
					jw.WritePropertyName( "manualXcodeBuild" );
					jw.Write( _pIosOptions._pIsManualXcodeBuild.Value );
				}

				if ( !string.IsNullOrEmpty( _pIosOptions._pDistributionMethod ) )
				{
					jw.WritePropertyName( "distributionMethod" );
					jw.Write( _pIosOptions._pDistributionMethod );
#if ENABLE_BC_PRE_1_11
					jw.WritePropertyName( "useEnterpriseAccount" );
					jw.Write( string.Equals( _pIosOptions._pDistributionMethod, "enterprise", StringComparison.OrdinalIgnoreCase ) );
#endif
				}

				if ( !string.IsNullOrEmpty( _pIosOptions._pProvisioningProfile ) )
				{
					jw.WritePropertyName( "provisioningProfile" );
					jw.Write( _pIosOptions._pProvisioningProfile );
				}

				if ( _pIosOptions._pIsEnableOnDemandResources.HasValue )
				{
					jw.WritePropertyName( "enableOnDemandResources" );
					jw.Write( _pIosOptions._pIsEnableOnDemandResources.Value );
				}

				if ( _pIosOptions._pIsEmbedOnDemandResources.HasValue )
				{
					jw.WritePropertyName( "embedOnDemandResources" );
					jw.Write( _pIosOptions._pIsEmbedOnDemandResources.Value );
				}

				if ( !string.IsNullOrEmpty( _pIosOptions._pOnDemandResourcesBaseUrl ) )
				{
					jw.WritePropertyName( "onDemandResourcesBaseUrl" );
					jw.Write( _pIosOptions._pOnDemandResourcesBaseUrl );
				}

				if ( !string.IsNullOrEmpty( _pIosOptions._pKeychainPassword ) )
				{
					jw.WritePropertyName( "keychain" );
					jw.WriteObjectStart();
					jw.WritePropertyName( "password" );
					jw.Write( _pIosOptions._pKeychainPassword );
					jw.WriteObjectEnd();
				}

				if ( _pIosOptions._pIsEnableBitcode.HasValue )
				{
					jw.WritePropertyName( "enableBitcode" );
					jw.Write( _pIosOptions._pIsEnableBitcode.Value );
				}

				if ( _pIosOptions._pIsEnableSwift.HasValue )
				{
					jw.WritePropertyName( "enableSwift" );
					jw.Write( _pIosOptions._pIsEnableSwift.Value );
				}

				if ( !string.IsNullOrEmpty( _pIosOptions._pSwiftVersion ) )
				{
					jw.WritePropertyName( "swiftVersion" );
					jw.Write( _pIosOptions._pSwiftVersion );
				}

				jw.WriteObjectEnd();
			}

			if ( !string.IsNullOrEmpty( _pUwpOptions._pMSBuildVersion ) || !string.IsNullOrEmpty( _pUwpOptions._pMSBuildPath ) || !string.IsNullOrEmpty( _pUwpOptions._pWindowsSdkVersion ) || !string.IsNullOrEmpty( _pUwpOptions._pWindowsSdkToolsPath ) || _pUwpOptions._pVsProjectAction.HasValue || !string.IsNullOrEmpty( _pUwpOptions._pBuildConfiguration ) || !string.IsNullOrEmpty( _pUwpOptions._pBuildPlatforms ) || _pUwpOptions._pIsSaveMSBuildLog.HasValue || !string.IsNullOrEmpty( _pUwpOptions._pCertificate ) || _pUwpOptions._pIsCreateAppxBundle.HasValue  || _pUwpOptions._pIsStoreUpload.HasValue || _pUwpOptions._pDefaultAssetScale.HasValue )
			{
				jw.WritePropertyName( "UWP" );
				jw.WriteObjectStart();

				if ( !string.IsNullOrEmpty( _pUwpOptions._pMSBuildPath ) )
				{
					jw.WritePropertyName( "msbuildPath" );
					jw.Write( _pUwpOptions._pMSBuildPath );
				}

				if ( !string.IsNullOrEmpty( _pUwpOptions._pMSBuildVersion ) )
				{
					jw.WritePropertyName( "msbuildVersion" );
					jw.Write( _pUwpOptions._pMSBuildVersion );
				}

				if ( !string.IsNullOrEmpty( _pUwpOptions._pWindowsSdkToolsPath ) )
				{
					jw.WritePropertyName( "windowsSdkToolsPath" );
					jw.Write( _pUwpOptions._pWindowsSdkToolsPath );
				}

				if ( !string.IsNullOrEmpty( _pUwpOptions._pWindowsSdkVersion ) )
				{
					jw.WritePropertyName( "windowsSdkVersion" );
					jw.Write( _pUwpOptions._pWindowsSdkVersion );
				}

				if ( _pUwpOptions._pVsProjectAction.HasValue )
				{
					jw.WritePropertyName( "vsProjectAction" );
					jw.Write( _pUwpOptions._pVsProjectAction.Value );
				}

				if ( !string.IsNullOrEmpty( _pUwpOptions._pBuildConfiguration ) )
				{
					jw.WritePropertyName( "buildConfiguration" );
					jw.Write( _pUwpOptions._pBuildConfiguration );
				}

				if ( !string.IsNullOrEmpty( _pUwpOptions._pBuildPlatforms ) )
				{
					jw.WritePropertyName( "buildPlatforms" );
					jw.Write( _pUwpOptions._pBuildPlatforms );
				}

				if ( _pUwpOptions._pIsSaveMSBuildLog.HasValue )
				{
					jw.WritePropertyName( "saveMSBuildLog" );
					jw.Write( _pUwpOptions._pIsSaveMSBuildLog.Value );
				}

				if ( !string.IsNullOrEmpty( _pUwpOptions._pCertificate ) )
				{
					jw.WritePropertyName( "certificate" );
					jw.Write( _pUwpOptions._pCertificate );
				}

				if ( _pUwpOptions._pIsCreateAppxBundle.HasValue )
				{
					jw.WritePropertyName( "createAppxBundle" );
					jw.Write( _pUwpOptions._pIsCreateAppxBundle.Value );
				}

				if ( _pUwpOptions._pIsStoreUpload.HasValue )
				{
					jw.WritePropertyName( "storeUpload" );
					jw.Write( _pUwpOptions._pIsStoreUpload.Value );
				}

				if ( _pUwpOptions._pDefaultAssetScale.HasValue )
				{
					jw.WritePropertyName( "defaultAssetScale" );
					jw.Write( _pUwpOptions._pDefaultAssetScale.Value );
				}

				jw.WriteObjectEnd();
			}

			if ( _pSwitchOptions._pIsCreateRom.HasValue )
			{
				jw.WritePropertyName( "switch" );
				jw.WriteObjectStart();

				if ( _pSwitchOptions._pIsCreateRom.HasValue )
				{
					jw.WritePropertyName( "createRom" );
					jw.Write( _pSwitchOptions._pIsCreateRom.Value );
				}

				jw.WriteObjectEnd();
			}

			if ( !string.IsNullOrEmpty( unityVersion ) )
			{
				jw.WritePropertyName( "unityVersion" );
				jw.Write( unityVersion );
			}

			jw.WriteObjectEnd();

			return sb.ToString();
		}

		public bool FromJson( string configString )
		{
			if ( configString == null ) return false;

			JsonData	configJson = Extensions.LoadJson( configString );

			if ( configJson == null || !configJson.IsObject ) return false;

			_pProductCode = configJson.TryGetString( "productCode", null );
			_pBuildId = configJson.TryGetString( "buildId", null );
			_pBuildVersion = configJson.TryGetString( "buildVersion", null );
			_pOutputName = configJson.TryGetString( "outputName", null );
			_pBundleId = configJson.TryGetString( "bundleId", null );
			_pDisplayName = configJson.TryGetString( "displayName", null );

			JsonData	j1, j2;

			j1 = configJson.TryGet( "assetBundleAction", JsonType.Int );
			if ( j1 != null )
			{
				_pAssetBundleAction = (int)j1;
			}
			else
			{
				_pAssetBundleAction = null;
			}

			j1 = configJson.TryGet( "deleteLastBuild", JsonType.Boolean );
			if ( j1 != null )
			{
				_pIsDeleteLastBuild = (bool)j1;
			}
			else
			{
				_pIsDeleteLastBuild = null;
			}

			_pDefines.Clear();
			j1 = configJson.TryGet( "defines", JsonType.Array );
			if ( j1 != null )
			{
				for ( int i = 0; i < j1.Count; ++i )
				{
					if ( j1[i].IsString )
					{
						_pDefines.Add( (string)(j1[i]) );
					}
				}
			}

			j1 = configJson.TryGet( "buildOpts", JsonType.Object );
			if ( j1 != null )
			{
				j2 = j1.TryGet( "development", JsonType.Boolean );
				if ( j2 != null )
				{
					_pBuildOptions._pIsDevelopment = (bool)j2;
				}
				else
				{
					_pBuildOptions._pIsDevelopment = null;
				}

				j2 = j1.TryGet( "allowDebugging", JsonType.Boolean );
				if ( j2 != null )
				{
					_pBuildOptions._pIsAllowDebugging = (bool)j2;
				}
				else
				{
					_pBuildOptions._pIsAllowDebugging = null;
				}
			}

			j1 = configJson.TryGet( "webPlayer", JsonType.Object );
			if ( j1 != null )
			{
				j2 = j1.TryGet( "dir", JsonType.String );
				if ( j2 != null )
				{
					_pWebPlayerOptions._pDirectory = (string)j2;
					if ( _pWebPlayerOptions._pDirectory.StartsWith( "*" ) )
					{
						_pWebPlayerOptions._pDirectory = WEB_DEMO_DIR_ALIAS;
					}
				}
				else
				{
					_pWebPlayerOptions._pDirectory = null;
				}
			}

			j1 = configJson.TryGet( "webGL", JsonType.Object );
			if ( j1 != null )
			{
				j2 = j1.TryGet( "dir", JsonType.String );
				if ( j2 != null )
				{
					_pWebGLOptions._pDirectory = (string)j2;
					if ( _pWebGLOptions._pDirectory.StartsWith( "*" ) )
					{
						_pWebGLOptions._pDirectory = WEB_DEMO_DIR_ALIAS;
					}
				}
				else
				{
					_pWebGLOptions._pDirectory = null;
				}
			}

			j1 = configJson.TryGet( "android", JsonType.Object );
			if ( j1 != null )
			{
				_pAndroidOptions._pKeystore = j1.TryGetString( "keystore", null );
				_pAndroidOptions._pBlockPermissions.Clear();
				j2 = j1.TryGet( "blockPermissions", JsonType.Array );
				if ( j2 != null )
				{
					for ( int i = 0; i < j2.Count; ++i )
					{
						if ( j2[i].GetJsonType() == JsonType.String )
						{
							_pAndroidOptions._pBlockPermissions.Add( ( string )( j2[i] ) );
						}
					}
				}
				j2 = j1.TryGet( "outputGradleProject", JsonType.Boolean );
				if ( j2 != null )
				{
					_pAndroidOptions._pIsOutputGradleProject = (bool)j2;
				}
				else
				{
					_pAndroidOptions._pIsOutputGradleProject = null;
				}

				j2 = j1.TryGet( "rebuildApk", JsonType.Int );
				if ( j2 != null )
				{
					_pAndroidOptions._pRebuildApk = (int)j2;
				}
				else
				{
					_pAndroidOptions._pRebuildApk = null;
				}

				_pAndroidOptions._pApktoolPath = j1.TryGetString( "apktoolPath", null );
			}

			j1 = configJson.TryGet( "jdk", JsonType.Object );
			if ( j1 != null )
			{
				_pJdkOptions._pPath = j1.TryGetString( "path", null );
				_pJdkOptions._pVersion = j1.TryGetString( "version", null );
			}

			j1 = configJson.TryGet( "iOS", JsonType.Object );
			if ( j1 != null )
			{
				_pIosOptions._pXcodeAppPath = j1.TryGetString( "xcodeAppPath", null );
				_pIosOptions._pXcodeVersion = j1.TryGetString( "xcodeVersion", null );

				j2 = j1.TryGet( "xcodeProjectAction", JsonType.Int );
				if ( j2 != null )
				{
					_pIosOptions._pXcodeProjectAction = (int)j2;
				}
				else
				{
					_pIosOptions._pXcodeProjectAction = null;
				}

				j2 = j1.TryGet( "manualXcodeBuild", JsonType.Boolean );
				if ( j2 != null )
				{
					_pIosOptions._pIsManualXcodeBuild = (bool)j2;
				}
				else
				{
					_pIosOptions._pIsManualXcodeBuild = null;
				}

				_pIosOptions._pDistributionMethod = j1.TryGetString( "distributionMethod", null );
				_pIosOptions._pProvisioningProfile = j1.TryGetString( "provisioningProfile", null );

				j2 = j1.TryGet( "enableOnDemandResources", JsonType.Boolean );
				if ( j2 != null )
				{
					_pIosOptions._pIsEnableOnDemandResources = (bool)j2;
				}
				else
				{
					_pIosOptions._pIsEnableOnDemandResources = null;
				}

				j2 = j1.TryGet( "embedOnDemandResources", JsonType.Boolean );
				if ( j2 != null )
				{
					_pIosOptions._pIsEmbedOnDemandResources = (bool)j2;
				}
				else
				{
					_pIosOptions._pIsEmbedOnDemandResources = null;
				}

				_pIosOptions._pOnDemandResourcesBaseUrl = j1.TryGetString( "onDemandResourcesBaseUrl", null );

				j2 = j1.TryGet( "keychain", JsonType.Object );
				if ( j2 != null )
				{
					_pIosOptions._pKeychainPassword = j2.TryGetString( "password", null );
				}

				j2 = j1.TryGet( "enableBitcode", JsonType.Boolean );
				if ( j2 != null )
				{
					_pIosOptions._pIsEnableBitcode = (bool)j2;
				}
				else
				{
					_pIosOptions._pIsEnableBitcode = null;
				}

				j2 = j1.TryGet( "enableSwift", JsonType.Boolean );
				if ( j2 != null )
				{
					_pIosOptions._pIsEnableSwift = (bool)j2;
				}
				else
				{
					_pIosOptions._pIsEnableSwift = null;
				}

				_pIosOptions._pSwiftVersion = j1.TryGetString( "swiftVersion", null );
			}

			j1 = configJson.TryGet( "UWP", JsonType.Object );
			if ( j1 != null )
			{
				_pUwpOptions._pMSBuildPath = j1.TryGetString( "msbuildPath", null );
				_pUwpOptions._pMSBuildVersion = j1.TryGetString( "msbuildVersion", null );
				_pUwpOptions._pWindowsSdkToolsPath = j1.TryGetString( "windowsSdkToolsPath", null );
				_pUwpOptions._pWindowsSdkVersion = j1.TryGetString( "windowsSdkVersion", null );

				j2 = j1.TryGet( "vsProjectAction", JsonType.Int );
				if ( j2 != null )
				{
					_pUwpOptions._pVsProjectAction = (int)j2;
				}
				else
				{
					_pUwpOptions._pVsProjectAction = null;
				}

				_pUwpOptions._pBuildConfiguration = j1.TryGetString( "buildConfiguration", null );
				_pUwpOptions._pBuildPlatforms = j1.TryGetString( "buildPlatforms", null );

				j2 = j1.TryGet( "saveMSBuildLog", JsonType.Boolean );
				if ( j2 != null )
				{
					_pUwpOptions._pIsSaveMSBuildLog = (bool)j2;
				}
				else
				{
					_pUwpOptions._pIsSaveMSBuildLog = null;
				}

				_pUwpOptions._pCertificate = j1.TryGetString( "certificate", null );

				j2 = j1.TryGet( "createAppxBundle", JsonType.Boolean );
				if ( j2 != null )
				{
					_pUwpOptions._pIsCreateAppxBundle = (bool)j2;
				}
				else
				{
					_pUwpOptions._pIsCreateAppxBundle = null;
				}

				j2 = j1.TryGet( "storeUpload", JsonType.Boolean );
				if ( j2 != null )
				{
					_pUwpOptions._pIsStoreUpload = (bool)j2;
				}
				else
				{
					_pUwpOptions._pIsStoreUpload = null;
				}

				j2 = j1.TryGet( "defaultAssetScale", JsonType.Int );
				if ( j2 != null )
				{
					_pUwpOptions._pDefaultAssetScale = (int)j2;
				}
				else
				{
					_pUwpOptions._pDefaultAssetScale = null;
				}
			}

			j1 = configJson.TryGet( "switch", JsonType.Object );
			if ( j1 != null )
			{
				j2 = j1.TryGet( "createRom", JsonType.Boolean );
				if ( j2 != null )
				{
					_pSwitchOptions._pIsCreateRom = (bool)j2;
				}
				else
				{
					_pSwitchOptions._pIsCreateRom = null;
				}
			}

			return true;
		}
	}
}

