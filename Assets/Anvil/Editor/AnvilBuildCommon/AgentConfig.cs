﻿using System;
using System.IO;
using LitJson;

namespace AnvilBuildCommon
{
	public class AgentConfig
	{
		public const string		ENVIRONMENT_VARIABLE = "ANVIL_AGENT_CONFIG";

		private const string	NOT_UNITY_PATCH = "!p";

		public class UnityData
		{
			public VersionedValue	_versionedExe;
		}

		public class AndroidData
		{
			public string	_apktoolPath;
			public VersionedValue	_versionedBuildToolsPath;
		}

		public class JdkData
		{
			public VersionedValue	_versionedPath;
		}

		public class XcodeData
		{
			public class KeychainData
			{
				public string	_password;
			}

			public VersionedValue	_versionedPath = null;
			public KeychainData	_keychain = new KeychainData();
		}

		public class MSBuildData
		{
			public VersionedValue	_versionedPath = null;
		}

		public class WindowsSdkToolsData
		{
			public VersionedValue	_versionedPath = null;
		}

		private UnityData	_unityData = new UnityData();
		public UnityData	_pUnityData	{ get { return _unityData; } }

		private AndroidData	_androidData = new AndroidData();
		public AndroidData	_pAndroidData { get { return _androidData; } }

		private JdkData	_jdkData = new JdkData();
		public JdkData	_pJdkData { get { return _jdkData; } }

		private XcodeData	_xcodeData = new XcodeData();
		public XcodeData	_pXcodeData	{ get { return _xcodeData; } }

		public string	_pKeychainPassword	{ get { return _xcodeData._keychain._password; } }

		private MSBuildData	_msbuildData = new MSBuildData();
		public MSBuildData	_pMSBuildData	{ get { return _msbuildData; } }

		private WindowsSdkToolsData	_windowsSdkToolsData = new WindowsSdkToolsData();
		public WindowsSdkToolsData	_pWindowsSdkToolsData	{ get { return _windowsSdkToolsData; } }

		public AgentConfig( string configPath, System.Action onFileNotFound )
		{
			ReadConfig( configPath, onFileNotFound );
		}

		public AgentConfig( string configStr )
		{
			ReadConfig( configStr );
		}

		public AgentConfig( JsonData configJson )
		{
			ReadConfig( configJson );
		}

		public string GetUnityExe( string wantVersion, out string gotVersion )
		{
			bool	allowPatch = true;

			if ( wantVersion.EndsWith( NOT_UNITY_PATCH ) )
			{
				allowPatch = false;
				wantVersion = wantVersion.Substring( 0, wantVersion.Length - NOT_UNITY_PATCH.Length );
			}

			return _unityData._versionedExe.Get( version => version.StartsWith( wantVersion ) && ( allowPatch || !( version.Contains( "p" ) || version.Contains( "P" ) ) ), out gotVersion );
		}

		public string GetAndroidBuildToolsPath( string wantVersion, out string gotVersion )
		{
			return _androidData._versionedBuildToolsPath.Get( wantVersion, out gotVersion );
		}

		public string GetJdkPath( string wantVersion, out string gotVersion )
		{
			return _jdkData._versionedPath.Get( wantVersion, out gotVersion );
		}

		public string GetXcodePath( string wantVersion, out string gotVersion )
		{
			return _xcodeData._versionedPath.Get( wantVersion, out gotVersion );
		}

		public string GetMSBuildPath( string wantVersion, out string gotVersion )
		{
			return _msbuildData._versionedPath.Get( wantVersion, out gotVersion );
		}

		public string GetWindowsSdkToolsPath( string wantVersion, out string gotVersion )
		{
			return _windowsSdkToolsData._versionedPath.Get( wantVersion, out gotVersion );
		}

		private void ReadConfig( string configPath, System.Action onFileNotFound )
		{
			StreamReader	sr = null;

			try
			{
				sr = new StreamReader( configPath );
			}
			catch ( IOException e )
			{
				if ( e is FileNotFoundException || e is DirectoryNotFoundException )
				{
					if ( onFileNotFound != null )
					{
						onFileNotFound();
					}
					return;
				}
				else
				{
					throw;
				}
			}

			ReadConfig( sr.ReadToEnd() );
		}

		private void ReadConfig( string configStr )
		{
			ReadConfig( Extensions.LoadJson( configStr ) );
		}

		private void ReadConfig( JsonData configJson )
		{
			if ( configJson == null ) return;
			if ( !configJson.IsObject ) return;

			JsonData	unityJson = configJson.TryGet( "unity", JsonType.Object );

			if ( unityJson != null )
			{
				ReadVersionedPath( ref _unityData._versionedExe, unityJson.TryGet( "versions", JsonType.Array ) );
			}

			JsonData	androidJson = configJson.TryGet( "android", JsonType.Object );

			if ( androidJson != null )
			{
				_androidData._apktoolPath = androidJson.TryGetString( "apktoolPath", null );

				ReadVersionedPath( ref _androidData._versionedBuildToolsPath, androidJson.TryGet( "buildToolsVersions", JsonType.Array ) );
			}

			JsonData	jdkJson = configJson.TryGet( "jdk", JsonType.Object );

			if ( jdkJson != null )
			{
				ReadVersionedPath( ref _jdkData._versionedPath, jdkJson.TryGet( "versions", JsonType.Array ) );
			}

			JsonData	xcodeJson = configJson.TryGet( "xcode", JsonType.Object );

			if ( xcodeJson != null )
			{
				ReadVersionedPath( ref _xcodeData._versionedPath, xcodeJson.TryGet( "versions", JsonType.Array ) );

				JsonData	keychainJson = xcodeJson.TryGet( "keychain", JsonType.Object );

				if ( keychainJson != null )
				{
					_xcodeData._keychain._password = keychainJson.TryGetString( "password", null );
				}
			}

			JsonData	msbuildJson = configJson.TryGet( "msbuild", JsonType.Object );

			if ( msbuildJson != null )
			{
				ReadVersionedPath( ref _msbuildData._versionedPath, msbuildJson.TryGet( "versions", JsonType.Array ) );
			}

			JsonData	windowsSdkToolsJson = configJson.TryGet( "windowsSdkTools", JsonType.Object );

			if ( windowsSdkToolsJson != null )
			{
				ReadVersionedPath( ref _windowsSdkToolsData._versionedPath, windowsSdkToolsJson.TryGet( "versions", JsonType.Array ) );
			}
		}

		private void ReadVersionedPath( ref VersionedValue versionedPath, JsonData versionsJson )
		{
			JsonData	vj;

			if ( versionsJson != null && versionsJson.IsArray )
			{
				versionedPath = new VersionedValue();

				string	version, path;

				for ( int i = 0; i < versionsJson.Count; ++i )
				{
					vj = versionsJson[i];

					if ( vj != null && vj.IsObject )
					{
						version = vj.TryGetString( "version", null );
						if ( string.IsNullOrEmpty( version ) ) continue;

						path = vj.TryGetString( "path", null );
						if ( string.IsNullOrEmpty( path ) ) continue;

						versionedPath.Set( version, path );
					}
				}
			}
			else
			{
				versionedPath = null;
			}
		}
	}
}

