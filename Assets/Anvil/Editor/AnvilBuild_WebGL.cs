﻿////////////////////////////////////////////
// 
// AnvilBuild_WebGL.cs
//
// Created: 20/06/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if UNITY_4_6
#define UNITY_4
#endif

#if !UNITY_4
#define SUPPORT_WEBGL
#endif

#if SUPPORT_WEBGL && !UNITY_5_0 && !UNITY_5_1
#define ENABLE_FALLBACK_FONT_OVERRIDE
#endif

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

#if SUPPORT_WEBGL
public static partial class AnvilBuild
{
	//
	// CONSTS / DEFINES / ENUMS
	//

	//
	// NESTED CLASSES / STRUCTS
	//
#if ENABLE_FALLBACK_FONT_OVERRIDE
	private class FontImporterData
	{
		private TrueTypeFontImporter	_fontImporter;

		private List<string>	_fontNames;

		private List<Font>	_fontReferences;

		public FontImporterData( TrueTypeFontImporter fontImporter )
		{
			_fontImporter = fontImporter;
			ReadFromImporter();
		}

		public FontImporterData( FontImporterData src )
		{
			_fontImporter = src._fontImporter;
			_fontNames = new List<string>( src._fontNames );
			_fontReferences = new List<Font>( src._fontReferences );
		}

		public void ReadFromImporter()
		{
			_fontNames = new List<string>();
			_fontReferences = new List<Font>();

			for ( int i = 0; i < _fontImporter.fontNames.Length; ++i )
			{
				_fontNames.Add( _fontImporter.fontNames[i] );
			}

			for ( int i = 0; i < _fontImporter.fontReferences.Length; ++i )
			{
				_fontReferences.Add( _fontImporter.fontReferences[i] );
			}
		}

		public void WriteToImporter()
		{
			if ( _fontNames != null )
			{
				_fontImporter.fontNames = _fontNames.ToArray();
			}
			else
			{
				_fontImporter.fontNames = new string[0];
			}

			if ( _fontReferences != null )
			{
				_fontImporter.fontReferences = _fontReferences.ToArray();
			}
			else
			{
				_fontImporter.fontReferences = new Font[0];
			}

			EditorUtility.SetDirty( _fontImporter );

			_fontImporter.SaveAndReimport();
		}

		public void AddFont( string fontName, Font font )
		{
			if ( _fontNames != null && !_fontNames.Contains( fontName ) )
			{
				if ( _fontNames.Count == 0 && fontName != _fontImporter.fontTTFName )
				{
					_fontNames.Add( _fontImporter.fontTTFName );
				}

				_fontNames.Add( fontName );
			}

			if ( _fontReferences != null && !_fontReferences.Contains( font ) )
			{
				_fontReferences.Add( font );
			}
		}
	}
#endif			
	//
	// MEMBERS 
	//

#if ENABLE_FALLBACK_FONT_OVERRIDE
	private static List<FontImporterData>	_webGLFontImporterDataOriginal;
#endif	
	//
	// PROPERTIES
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static void InitWebGL()
	{
#if ENABLE_FALLBACK_FONT_OVERRIDE
		SetWebGLFallbackFonts();
#else
		if ( AnvilProjectConfig._pWebGLFallbackFont != null )
		{
			Debug.LogWarning( DEBUG_LOG_PREFIX + "Fallback font will be ignored!" );
		}
#endif	
	}

	private static void CleanUpWebGL()
	{
#if ENABLE_FALLBACK_FONT_OVERRIDE
		RestoreWebGLFontData();
#endif	
	}

#if ENABLE_FALLBACK_FONT_OVERRIDE
	private static void SetWebGLFallbackFonts()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Setting WebGL fallback fonts" );

		_webGLFontImporterDataOriginal = new List<FontImporterData>();

		string	fallbackFontName = AnvilProjectConfig._pWebGLFallbackFontName;

		if ( string.IsNullOrEmpty( fallbackFontName ) ) return;

		Font	fallbackFont = AnvilProjectConfig._pWebGLFallbackFont;

		if ( fallbackFont == null ) return;

		Font[]	targetFonts = AnvilProjectConfig._pWebGLFallbackFontTargets;

		if ( targetFonts == null ) return;

		string	fontPath;
		TrueTypeFontImporter	fontImporter;
		FontImporterData	fontImporterDataOld;
		FontImporterData	fontImporterDataNew;

		for ( int i = 0; i < targetFonts.Length; ++i )
		{
			if ( targetFonts[i] == null ) continue;

			fontPath = AssetDatabase.GetAssetPath( targetFonts[i] );

			if ( string.IsNullOrEmpty( fontPath ) )
			{
				ForceFailBuild( "No asset path for font: " + targetFonts[i] );
				continue;
			}

			fontImporter = AssetImporter.GetAtPath( fontPath ) as TrueTypeFontImporter;

			if ( fontImporter == null )
			{
				ForceFailBuild( "No font importer for asset: " + fontPath );
				continue;
			}

			Debug.Log( DEBUG_LOG_PREFIX + "Setting " + fontImporter.fontTTFName + " fallback to " + fallbackFontName );

			fontImporterDataOld = new FontImporterData( fontImporter );

			_webGLFontImporterDataOriginal.Add( fontImporterDataOld );

			fontImporterDataNew = new FontImporterData( fontImporterDataOld );
			fontImporterDataNew.AddFont( fallbackFontName, fallbackFont );
			fontImporterDataNew.WriteToImporter();
		}
	}

	private static void RestoreWebGLFontData()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Restoring WebGL font data" );

		if ( _webGLFontImporterDataOriginal == null ) return;

		for ( int i = 0; i < _webGLFontImporterDataOriginal.Count; ++i )
		{
			if ( _webGLFontImporterDataOriginal[i] == null ) continue;

			_webGLFontImporterDataOriginal[i].WriteToImporter();
		}
	}
#endif	
}
#endif	
