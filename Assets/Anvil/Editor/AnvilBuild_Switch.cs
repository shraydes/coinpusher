﻿#if UNITY_5_6_OR_NEWER
#define SUPPORT_SWITCH
#endif

using UnityEngine;
using UnityEditor;

public static partial class AnvilBuild
{
	private static bool		_isCreateSwitchRom = false; 

	private static void InitSwitch()
	{
		if ( _pBuildConfig._pSwitchOptions._pIsCreateRom.HasValue )
		{
			_isCreateSwitchRom = _pBuildConfig._pSwitchOptions._pIsCreateRom.Value;
		}
		else
		{
			_isCreateSwitchRom = true;
		}

		if ( _isCreateSwitchRom )
		{
			_outputPath += ".nsp";
		}
		else
		{
			_outputPath += ".nspd";
		}

		#if SUPPORT_SWITCH
		EditorUserBuildSettings.switchCreateRomFile = _isCreateSwitchRom;
		#endif
	}
}
