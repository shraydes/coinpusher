﻿////////////////////////////////////////////
// 
// AnvilBuild_AndroidGradle.cs
//
// Created: 17/10/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_AnvilBuild_AndroidGradle
#endif

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using AmuzoEngine;

public static partial class AnvilBuild
{
	//
	// CONSTS / DEFINES / ENUMS
	//

	#if UNITY_2018_3_OR_NEWER
	private const string GRADLE_VERSION = "3.2.0";
	#elif UNITY_2017_4_OR_NEWER
	private const string GRADLE_VERSION = "2.3.0";
	#else
	private const string GRADLE_VERSION = "2.1.0";
	#endif

	#if UNITY_2018_3_OR_NEWER
	private const string JAVA_VERSION = "1.8";
	#else
	private const string JAVA_VERSION = "1.7";
	#endif

	public enum EAndroidGradleRepositoryModType
	{
		NULL = 0,

		JCENTER = 1,
		MAVEN = 2,
	}

	public enum EAndroidGradleDependencyModType
	{
		NULL = 0,

		COMPILE = 1,
	}

	public enum EAndroidGradlePackagingModType
	{
		NULL = 0,

		EXCLUDE = 1,
	}

	//
	// NESTED CLASSES / STRUCTS
	//

	[System.Serializable]
	public class AndroidGradleRepositoryMod
	{
		public EAndroidGradleRepositoryModType	_modType;

		public string	_mavenUrl;

		public void CopyFrom( AndroidGradleRepositoryMod src )
		{
			_modType = src._modType;
			_mavenUrl = src._mavenUrl;
		}
	}

	[System.Serializable]
	public class AndroidGradleDependencyMod
	{
		public EAndroidGradleDependencyModType	_modType;

		public string	_compileModule;

		public string[]	_closureLines = new string[0];

		public void CopyFrom( AndroidGradleDependencyMod src )
		{
			_modType = src._modType;
			_compileModule = src._compileModule;

			if ( src._closureLines == null )
			{
				_closureLines = null;
			}
			else
			{
				_closureLines = new string[ src._closureLines.Length ];
				System.Array.Copy( src._closureLines, _closureLines, src._closureLines.Length );
			}
		}
	}

	[System.Serializable]
	public class AndroidGradlePackagingMod
	{
		public EAndroidGradlePackagingModType	_modType;

		public string	_filePath;

		public void CopyFrom( AndroidGradlePackagingMod src )
		{
			_modType = src._modType;
			_filePath = src._filePath;
		}
	}

	[System.Serializable]
	public class AndroidGradleSettings
	{
		public List<AndroidGradleRepositoryMod>	_repoMods = new List<AndroidGradleRepositoryMod>();

		public List<AndroidGradleDependencyMod>	_depMods = new List<AndroidGradleDependencyMod>();

		public bool	_multiDexEnabled;

		public List<AndroidGradlePackagingMod>	_packMods = new List<AndroidGradlePackagingMod>();

		public void CopyFrom( AndroidGradleSettings src )
		{
			_repoMods = new List<AndroidGradleRepositoryMod>( src._repoMods.Count );

			for ( int i = 0; i < src._repoMods.Count; ++i )
			{
				AndroidGradleRepositoryMod	newMod = new AndroidGradleRepositoryMod();
				newMod.CopyFrom( src._repoMods[i] );
				_repoMods.Add( newMod );
			}

			_depMods = new List<AndroidGradleDependencyMod>( src._depMods.Count );

			for ( int i = 0; i < src._depMods.Count; ++i )
			{
				AndroidGradleDependencyMod	newMod = new AndroidGradleDependencyMod();
				newMod.CopyFrom( src._depMods[i] );
				_depMods.Add( newMod );
			}

			_multiDexEnabled = src._multiDexEnabled;

			_packMods = new List<AndroidGradlePackagingMod>( src._packMods.Count );

			for ( int i = 0; i < src._packMods.Count; ++i )
			{
				AndroidGradlePackagingMod	newMod = new AndroidGradlePackagingMod();
				newMod.CopyFrom( src._packMods[i] );
				_packMods.Add( newMod );
			}
		}
	}
			
	//
	// MEMBERS 
	//

	private static AndroidGradleSettings	_androidGradleSettings;
	
	//
	// PROPERTIES
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	// ** INTERNAL
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static void InitAndroidGradleSettings()
	{
		_androidGradleSettings = new AndroidGradleSettings();

		if ( AnvilProjectConfig._pGradleSettings != null )
		{
			_androidGradleSettings.CopyFrom( AnvilProjectConfig._pGradleSettings );
		}
	}

	private static void ModifyAndroidGradleSettings()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Plugins modifying Android Gradle settings" );
		ForEachBuildPlugin( p => {
			p.OnModifyAndroidGradleSettings( _androidGradleSettings );
		}, true );
	}

	private static void OutputAndroidGradleBuildScript( string outputFilepath )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "OutputAndroidGradleBuildScript: " + outputFilepath );

		List<string>	lines;
		
		try
		{
			lines = new List<string>( System.IO.File.ReadAllLines( _projectPath + "/" + _dirFilesAndroid + "MainGradleTemplate.gradle" ) );
		}
		catch ( System.Exception )
		{
			Debug.LogError( "Failed to read MainGradleTemplate" );
			throw;
		}

		int	varStartIdx, varNameStartIdx, varNameEndIx, varEndIdx;
		string	varName;

		for ( int i = 0; i < lines.Count; ++i )
		{
			varStartIdx = lines[i].IndexOf( "**ANVIL_" );

			if ( varStartIdx < 0 ) continue;

			varNameStartIdx = varStartIdx + 2;

			varNameEndIx = lines[i].IndexOf( "**", varNameStartIdx );

			if ( varNameEndIx < 0 ) ForceFailBuild( "Invalid Anvil var in gradle template!\n\t" + lines[i] );

			varEndIdx = varNameEndIx + 2;

			varName = lines[i].Substring( varNameStartIdx, varNameEndIx - varNameStartIdx );

			switch ( varName )
			{
			case "ANVIL_GRADLE_VERSION":
				ReplaceText( lines, i, varStartIdx, varEndIdx, GRADLE_VERSION );
				break;
			case "ANVIL_JAVA_VERSION":
				ReplaceText( lines, i, varStartIdx, varEndIdx, "JavaVersion.VERSION_" + JAVA_VERSION.Replace('.', '_') );
				break;
			case "ANVIL_REPOS":
				lines.RemoveAt( i );
				InsertRepoModsLines( lines, i );
				break;
			case "ANVIL_DEPS":
				lines.RemoveAt( i );
				InsertDepModsLines( lines, i );
				break;
			case "ANVIL_MULTI_DEX":
				ReplaceText( lines, i, varStartIdx, varEndIdx, _androidGradleSettings._multiDexEnabled.ToString().ToLower() );
				break;
			case "ANVIL_MINIFY_DEBUG":
				ReplaceText( lines, i, varStartIdx, varEndIdx, GetMinifyEnabled( debug:true ).ToString().ToLower() );
				break;
			case "ANVIL_PROGUARD_DEBUG":
				ReplaceText( lines, i, varStartIdx, varEndIdx, GetUseProguard( debug:true ).ToString().ToLower() );
				break;
			case "ANVIL_MINIFY_RELEASE":
				ReplaceText( lines, i, varStartIdx, varEndIdx, GetMinifyEnabled( debug:false ).ToString().ToLower() );
				break;
			case "ANVIL_PROGUARD_RELEASE":
				ReplaceText( lines, i, varStartIdx, varEndIdx, GetUseProguard( debug:false ).ToString().ToLower() );
				break;
			case "ANVIL_PACKAGING_OPTIONS":
				lines.RemoveAt( i );
				InsertPackModsLines( lines, i );
				break;
			default:
				if ( varName.StartsWith( "ANVIL_" ) )
				{
					ForceFailBuild( "Unexpected Anvil var in gradle template: " + varName );
				}
				break;
			}
		}

		System.IO.Directory.CreateDirectory( System.IO.Path.GetDirectoryName( outputFilepath ) );
		System.IO.File.WriteAllLines( outputFilepath, lines.ToArray(), System.Text.Encoding.UTF8 );
	}

	private static void InsertRepoModsLines( List<string> lines, int insertIdx )
	{
		for ( int i = 0; i < _androidGradleSettings._repoMods.Count; ++i )
		{
			switch ( _androidGradleSettings._repoMods[i]._modType )
			{
			case EAndroidGradleRepositoryModType.JCENTER:
				lines.Insert( insertIdx++, "\tjcenter()" );
				break;
			case EAndroidGradleRepositoryModType.MAVEN:
				lines.Insert( insertIdx++, "\tmaven {" );
				lines.Insert( insertIdx++, "\t\turl '" + _androidGradleSettings._repoMods[i]._mavenUrl + "'" );
				lines.Insert( insertIdx++, "\t}" );
				break;
			}
		}
	}

	private static void InsertDepModsLines( List<string> lines, int insertIdx )
	{
		for ( int i = 0; i < _androidGradleSettings._depMods.Count; ++i )
		{
			switch ( _androidGradleSettings._depMods[i]._modType )
			{
			case EAndroidGradleDependencyModType.COMPILE:
				lines.Insert( insertIdx++, "\tcompile( '" + _androidGradleSettings._depMods[i]._compileModule + "' )" );
				if ( _androidGradleSettings._depMods[i]._closureLines.Length > 0 )
				{
					lines.Insert( insertIdx++, "\t{" );
					for ( int j = 0; j < _androidGradleSettings._depMods[i]._closureLines.Length; ++j )
					{
						lines.Insert( insertIdx++, "\t\t" + _androidGradleSettings._depMods[i]._closureLines[j] );
					}
					lines.Insert( insertIdx++, "\t}" );
				}
				break;
			}
		}
	}

	private static void InsertPackModsLines( List<string> lines, int insertIdx )
	{
		for ( int i = 0; i < _androidGradleSettings._packMods.Count; ++i )
		{
			switch ( _androidGradleSettings._packMods[i]._modType )
			{
			case EAndroidGradlePackagingModType.EXCLUDE:
				lines.Insert( insertIdx++, "\t\texclude( '" + _androidGradleSettings._packMods[i]._filePath + "' )" );
				break;
			}
		}
	}

	private static bool GetMinifyEnabled( bool debug )
	{
		if ( _androidGradleSettings._multiDexEnabled ) return false;
		return debug ? EditorUserBuildSettings.androidDebugMinification != AndroidMinification.None : EditorUserBuildSettings.androidReleaseMinification != AndroidMinification.None;
	}

	private static bool GetUseProguard( bool debug )
	{
		if ( _androidGradleSettings._multiDexEnabled ) return true;
		return debug ? EditorUserBuildSettings.androidDebugMinification == AndroidMinification.Proguard : EditorUserBuildSettings.androidReleaseMinification == AndroidMinification.Proguard;
	}

	private static void ReplaceText( List<string> lines, int lineIdx, int startIdx, int endIdx, string newText )
	{
		lines[ lineIdx ] = lines[ lineIdx ].Substring( 0, startIdx ) + newText + lines[ lineIdx ].Substring( endIdx );
	}
}

