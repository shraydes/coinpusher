////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// AnvilBuild.cs
//
// Created: 28/11/2014 chope
// Contributors: karsten, barry, and just about everyone else. 
// 
// Intention:
// this is the c# exposed part out our automated build process. 
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if UNITY_4_6
#define UNITY_4
#endif

#if !UNITY_4
#define SUPPORT_WEBGL
#endif

#if UNITY_5_4_OR_NEWER
#define SUPPORT_TVOS
#endif

#if !UNITY_5_4_OR_NEWER
#define SUPPORT_WEBPLAYER
#endif

#if !UNITY_5_5_OR_NEWER
#define USE_ANDROID_NATIVE_ACTIVITY
#endif

#if (UNITY_IOS || UNITY_TVOS) && UNITY_5_4_OR_NEWER
#define SUPPORT_ODR
#endif

#if NET_4_6 || ANVIL_NET_4_6
#define USE_DOT_NET_4_6
#endif

#if UNITY_5_6_OR_NEWER
#define SUPPORT_SWITCH
#endif

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using LitJson;
using AmuzoEngine;
using AnvilBuildCommon;
using AmuzoCoreBuildTools;

#if UNITY_2018_3_OR_NEWER
using BuildPlayerResult = UnityEditor.Build.Reporting.BuildReport;
#else
using BuildPlayerResult = System.String;
#endif

//----------------------------------------------------------------------------------------------------------------------
public static partial class AnvilBuild
{
	//------------------------------------------------------------------------------------------------------------------
	// CONSTS / DEFINES
	private const string	LOG_TAG = "[AnvilBuild] ";
	public const string ANVIL_HOME = "Assets/Anvil";
	private const string NAME_WEBPLAYER_OUTPUT = "WebPlayer";
	private const string NAME_WEBGL_OUTPUT = "WebGLPlayer";
	private const string DEBUG_LOG_PREFIX = "ANVIL_BUILD - ";
	private const string URL_PLATFORM_DEFINES = "http://4ord.4t2cloud.com/getReleaseText.php?";
	private const string EXPORT_FOLDER_NAME = "Export";
	private const string IOS_BUILD_FOLDER_NAME = "iosBuild";
	private const string TVOS_BUILD_FOLDER_NAME = "tvosBuild";
	private const string UWP_BUILD_FOLDER_NAME = "uwpBuild";
	private const string SWITCH_BUILD_FOLDER_NAME = "switchBuild";
	private const string DEFAULT_OUTPUT_NAME = "BuildOutput";
	private const string OUTPUT_TO_DEMO_DIR = "*DEMO_DIR";
#if USE_ANDROID_NATIVE_ACTIVITY
	private const string DEFAULT_ANDROID_MAIN_ACTIVITY_NAME = "com.unity3d.player.UnityPlayerNativeActivity";
#else
	private const string DEFAULT_ANDROID_MAIN_ACTIVITY_NAME = "com.unity3d.player.UnityPlayerActivity";
#endif
	private const string WEBGL_BUILD_OUTPUT_FOLDER = "Build";
	private const string DEFAULT_ANVIL_PROJECT_CONFIG_PATH = "Assets/Anvil/Editor/AnvilProjectConfig.asset";
	
	//------------------------------------------------------------------------------------------------------------------
	// MEMBERS
	public static string _dirFilesWeb = "Assets/Anvil/Resources/FilesWeb/";
	public static string _dirFilesWebGL = "Assets/Anvil/Resources/FilesWebGL/";
	public static string _dirFilesAndroid = "Assets/Anvil/Resources/FilesAndroid/";
	private static string _dirFilesIos = "Assets/Anvil/Resources/FilesIos/";
#if SUPPORT_TVOS
	private static string _dirFilesTvos = "Assets/Anvil/Resources/FilesTvos/";
	public static string _pDirFilesIosTvos { get { return _buildTarget == BuildTarget.iOS ? _dirFilesIos : _buildTarget == BuildTarget.tvOS ? _dirFilesTvos : null; } }
#else
	public static string _pDirFilesIosTvos { get { return _dirFilesIos; } }
#endif
	public static string _dirFilesUwp = "Assets/Anvil/Resources/FilesUwp/";
	private static string _dirAdditionalFilesIos = "Assets/Anvil/Editor/iosfiles/";

	private static string	_agentConfigPath;
	private static AgentConfig	_agentConfig;
	public static AgentConfig	_pAgentConfig { get { return _agentConfig; } }

	private static string _projectPath;
	public static string _pProjectPath	{ get { return _projectPath; } }
	private static string _exportPath;
	public static string _pExportPath	{ get { return _exportPath; } }
	private static string _dataPath;
	private static string _productCode;
	private static string _defaultOutputName;
	private static string _outputName;
	public static string _pOutputName	{ get { return _outputName; } }
	private static string _pAssetBundlesOutputPath { get { return CombinePaths( _exportPath, _outputName + "_AssetBundles" ); } }
	private static String _buildConfigJson;
	private static BuildConfig _buildConfig;
	public static BuildConfig _pBuildConfig	{ get { return _buildConfig; } }
	private static string _productName;
	private static string _cachedProductName;
	private static string _cachedCompanyName;
	private static string _buildInfoPathAndName;
	private static string _buildInfo;
	private static string _outputPath;
	public static string _pOutputPath	{ get { return _outputPath; } }
	private static string _demoDirectory;
	private static string _bundleIdentifier;
	public static string _pBundleIdentifier { get { return _bundleIdentifier; } }
	private static BuildTarget _buildTarget;
	private static BuildTargetGroup _buildTargetGroup;
	private static string _cachedDefines = null;

	private static bool	_isDevelopment = false;
	private static bool	_isAllowDebugging = false;
	private static bool	_isSubmission = false;

	private static bool _pIsWantBuildApp
	{
		get
		{
			return !_buildConfig._pAssetBundleAction.HasValue || _buildConfig._pAssetBundleAction.Value != 1;
		}
	}

	private static bool _pIsWantBuildAssetBundles
	{
		get
		{
			return !_buildConfig._pAssetBundleAction.HasValue || _buildConfig._pAssetBundleAction.Value > 0;
		}
	}
	
	private static string	_androidManifestNamespaceURI;
	public static string	_pAndroidManifestNamespaceURI
	{
		get
		{
			return _androidManifestNamespaceURI;
		}
	}

	private static bool		_isAndroidLeanbackSupported = false;

	private static string	_androidMainActivityName;
	public static string	_pAndroidMainActivityName
	{
		get
		{
			return _androidMainActivityName;
		}
	}

	private static string	_javaPath;

	private static string	_jarsignerPath;

	private static string	_jarPath;

	
	//------------------------------------------------------------------------------------------------------------------
	// DELEGATES / EVENTS
	public static event DOnPreBuild _onPreBuild;
	public delegate bool DOnPreBuild();

	public static event DOnBuild _onBuild;
	public delegate bool DOnBuild();

	public static event DOnPostBuild _onPostBuild;
	public delegate bool DOnPostBuild();

	//------------------------------------------------------------------------------------------------------------------
	public static void Build()
	{
		_agentConfig = null;
		_buildConfig = null;
		_buildInfo = null;

		_TryBuild();
	}

	//------------------------------------------------------------------------------------------------------------------
	public static void BuildInternal( AgentConfig agentConfig, BuildConfig buildConfig, string buildInfo )
	{
		_agentConfig = agentConfig;
		_buildConfig = buildConfig;
		_buildInfo = buildInfo;

		_TryBuild();
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void _TryBuild()
	{
		try
		{
			_Build();
		}
		catch ( System.Exception e )
		{
			Debug.LogError( e.Message );

			CleanUp();

			throw;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void _Build()
	{
#if PUMA_DEFINE_PUMA
		Puma.PackageManager.RefreshDatabase();
		Puma.PackageInfo	pi = Puma.PackageManager.GetPackageInfo( "Amuzo", "Anvil" );
		if ( pi != null )
		{
			System.Text.StringBuilder	sb = new System.Text.StringBuilder();

			sb.AppendLine( DEBUG_LOG_PREFIX + "Core Build Tools Version: " + BuildToolsVersion.ID.ToString() );
			sb.AppendLine( DEBUG_LOG_PREFIX + "Anvil Version: " + pi._pPackageVersion );

			pi.DetectChanges( ( string filePath, string fileHashA, string fileHashB ) => {
				sb.AppendLine( "\tAnvil package file changed: " + filePath );
			} );

			Debug.Log( sb.ToString() );
		}
#endif
		Debug.Log( DEBUG_LOG_PREFIX + "Building Unity Bamboo/4ORD-Release-configured build" );

		if ( _agentConfig == null )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Getting agent config" );
			_agentConfigPath = Environment.GetEnvironmentVariable( AgentConfig.ENVIRONMENT_VARIABLE );
			if ( !string.IsNullOrEmpty( _agentConfigPath ) )
			{
				_agentConfig = new AgentConfig( _agentConfigPath, () => {
					ForceFailBuild( "Failed to load Anvil agent config at: " + _agentConfigPath );
				} );
			}
		}

		Debug.Log( DEBUG_LOG_PREFIX + "Active plugins:" );
		ForEachBuildPlugin( p => {
			Debug.Log( "\t" + p._pPluginName );
		} );

		if ( AnvilProjectConfig.Load( DEFAULT_ANVIL_PROJECT_CONFIG_PATH ) == false )
		{
			Debug.LogWarning( DEBUG_LOG_PREFIX + "Failed to load Anvil Project Config at: " + DEFAULT_ANVIL_PROJECT_CONFIG_PATH );
		}

		// the 4ord plan lookup is done by the project code set as the product name in the editor. 
		// but this can change from data gathered from 4ord, so hold it in its own string to re-set after build. 
		_cachedProductName = PlayerSettings.productName;
		Debug.Log( DEBUG_LOG_PREFIX + "Product Name, BEFORE setting 4ord data is: " + _cachedProductName.ToString() );

		_cachedCompanyName = PlayerSettings.companyName;
		Debug.Log( DEBUG_LOG_PREFIX + "Company Name, BEFORE build is: " + _cachedCompanyName.ToString() );

		_buildTarget = EditorUserBuildSettings.activeBuildTarget;
		Debug.Log( DEBUG_LOG_PREFIX + "Build Target, retrieved from Unity's EditorUserBuildSettings class is: " + _buildTarget.ToString() );

		SetBuildTargetGroupFromBuildTarget();
		Debug.Log( DEBUG_LOG_PREFIX + "Build Target Group is: " + _buildTargetGroup.ToString() );

		_cachedDefines = ScriptingDefineSymbolsFacade.GetPlayerSettingsSymbols( _buildTargetGroup );
		Debug.Log( DEBUG_LOG_PREFIX + "Cached defines: " + _cachedDefines );

		if ( _buildConfig == null )
		{
			_buildConfigJson = Environment.GetEnvironmentVariable( BuildConfig.ENVIRONMENT_VARIABLE );
			Debug.Log( DEBUG_LOG_PREFIX + "Config String, defined as Unity internal Environment variables are: " + _buildConfigJson.ToString() );
		
			_buildConfig = new BuildConfig();
			_buildConfig.FromJson( _buildConfigJson );
		}

		Debug.Log( DEBUG_LOG_PREFIX + "Config Data: " + _buildConfig );
		
		_dataPath = Application.dataPath;
		Debug.Log( DEBUG_LOG_PREFIX + "Project Path, retrieved from Unity's Application class is: " + _dataPath );

		_projectPath = Directory.GetParent( _dataPath ).FullName;
		Debug.Log( DEBUG_LOG_PREFIX + "Project Path, retrieved as Parent Directory of data path is: " + _projectPath );
		
		_exportPath = CombinePaths( _projectPath, EXPORT_FOLDER_NAME );
		Debug.Log( DEBUG_LOG_PREFIX + "Export path is: " + _exportPath );

		if ( string.IsNullOrEmpty( _buildConfig._pProductCode ) )
		{
			_productCode = AnvilProjectConfig._pProjectCode;
		}
		else
		{
			_productCode = _buildConfig._pProductCode;
		}
		Debug.Log( DEBUG_LOG_PREFIX + "Product code is: " + _productCode );
		
		WorkOutOutputName();
		Debug.Log( DEBUG_LOG_PREFIX + "Output name is: " + _outputName );
		
		EmbedPlatformDefines();
		
		if ( _buildConfig != null && !string.IsNullOrEmpty( _buildConfig._pBundleId ) )
		{
			_bundleIdentifier = _buildConfig._pBundleId;
			Debug.Log( DEBUG_LOG_PREFIX + "Bundle Identifier, from config data is: " + _bundleIdentifier );
		}
		else
		{
			_bundleIdentifier = AnvilBuildInfo.GetVar( "BUNDLE_ID", "com.amuzo.ourtec" );
			Debug.Log( DEBUG_LOG_PREFIX + "Bundle Identifier, looked up from 4ord data is: " + _bundleIdentifier );
		}

		if ( _buildConfig != null && !string.IsNullOrEmpty( _buildConfig._pDisplayName ) )
		{
			_productName = _buildConfig._pDisplayName;
			Debug.Log( DEBUG_LOG_PREFIX + "Product display name, from config data is: " + _productName );
		}
		else
		{
			_productName = AnvilBuildInfo.GetVar( "APP_NAME", "Amuzo Framework" );
			Debug.Log( DEBUG_LOG_PREFIX + "Product display name, looked up from 4ord data is: " + _productName );
		}

		_isDevelopment = _buildConfig != null && _buildConfig._pBuildOptions._pIsDevelopment.HasValue && _buildConfig._pBuildOptions._pIsDevelopment.Value == true;
		_isAllowDebugging = _buildConfig != null && _buildConfig._pBuildOptions._pIsAllowDebugging.HasValue && _buildConfig._pBuildOptions._pIsAllowDebugging.Value == true;

		if ( _buildConfig != null && _buildConfig._pDefines.Contains( "SUBMISSION" ) )
		{
			_isSubmission = true;
		}
		else
		{
			bool.TryParse( AnvilBuildInfo.GetVar( "SUBMISSION", "false" ), out _isSubmission );

			if ( _isSubmission )
			{
				Debug.Log( LOG_TAG + "Adding symbol: SUBMISSION" );
				ScriptingDefineSymbolsFacade.AddSymbol( "SUBMISSION", _buildTargetGroup );
			}
		}

		Debug.Log( LOG_TAG + "Submission build: " + _isSubmission );

		bool deleteLastBuild = _buildConfig._pIsDeleteLastBuild.HasValue ? _buildConfig._pIsDeleteLastBuild.Value : true;
		if( !deleteLastBuild )
		{
		    Debug.Log( DEBUG_LOG_PREFIX + "NOT deleting previous build by request" );
		}
		else
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Deleting previous build by request (or by default)" );
			
			SafeDeleteDirectory( _exportPath );
			SafeDeleteFile( _exportPath + ".zip" );
		}

#if UNITY_2017_1_OR_NEWER
		PlayerSettings.applicationIdentifier = _bundleIdentifier;
#else
		PlayerSettings.bundleIdentifier = _bundleIdentifier;
#endif
		PlayerSettings.bundleVersion = AnvilBuildInfo.GetVar( "VERSION", "1" );
		#if UNITY_4
		PlayerSettings.shortBundleVersion = AnvilBuildInfo.GetVar( "BUNDLE_VERSION", "1.0.0" );
		#endif
		PlayerSettings.productName = _productName;
		PlayerSettings.companyName = AnvilBuildInfo.GetVar( "COMPANY_NAME", "Amuzo" );

		_demoDirectory = AnvilBuildInfo.GetVar( "DEMO", "" );
		Debug.Log( DEBUG_LOG_PREFIX + "Demos Dir, AFTER setting 4ord data is: " + _demoDirectory.ToString() );

		WorkOutOutputDirectoryOfBuild();

		InitJdk();
		
		switch( _buildTarget )
		{
			#if UNITY_4
			case BuildTarget.iPhone:
			#else
			case BuildTarget.iOS:
			#endif
			{
				PlayerSettings.iOS.targetDevice = iOSTargetDevice.iPhoneAndiPad;

				string	minOSVersion = AnvilBuildInfo.GetVar( "OS_VERSION", "6+" );

				minOSVersion = minOSVersion.TrimEnd( '+' );

				Debug.Log( DEBUG_LOG_PREFIX + "iOS minOSVersion: " + minOSVersion );

				if ( !minOSVersion.Contains( "." ) )
				{
					minOSVersion = minOSVersion + ".0";
					Debug.Log( DEBUG_LOG_PREFIX + "iOS minOSVersion: " + minOSVersion );
				}

#if UNITY_5_5_OR_NEWER
				PlayerSettings.iOS.targetOSVersionString = minOSVersion;

				Debug.Log( DEBUG_LOG_PREFIX + "iOS minOSVersion set to " + PlayerSettings.iOS.targetOSVersionString );
#else
				string[] parts = minOSVersion.Split( '.' );

				minOSVersion = string.Format( "iOS_{0}_{1}", parts[0], parts[1] );

				Debug.Log( DEBUG_LOG_PREFIX + "iOS minOSVersion: " + minOSVersion );

				EnumUtils.FromStringMap<iOSTargetOSVersion>	map = new EnumUtils.FromStringMap<iOSTargetOSVersion>( iOSTargetOSVersion.Unknown );

				PlayerSettings.iOS.targetOSVersion = map.Lookup( minOSVersion );

				if ( PlayerSettings.iOS.targetOSVersion == iOSTargetOSVersion.Unknown )
				{
					ForceFailBuild( "Failed to find matching iOSTargetOSVersion enum value for '" + minOSVersion + "'" );
				}

				Debug.Log( DEBUG_LOG_PREFIX + "iOS minOSVersion set to " + PlayerSettings.iOS.targetOSVersion );
#endif

#if UNITY_2017_1_OR_NEWER
				PlayerSettings.SetApplicationIdentifier( BuildTargetGroup.iOS, _bundleIdentifier );
#else
				PlayerSettings.iPhoneBundleIdentifier = _bundleIdentifier;
#endif
				PlayerSettings.bundleVersion = AnvilBuildInfo.GetVar( "BUNDLE_VERSION", "1.0.0" );
				PlayerSettings.iOS.buildNumber = AnvilBuildInfo.GetVar( "VERSION", "1" );
				PlayerSettings.iOS.applicationDisplayName = _productName;
				PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
				PreBuild_iOS_tvOS();
				break;
			}
#if SUPPORT_TVOS
			case BuildTarget.tvOS:
			{
				string	minOSVersion = AnvilBuildInfo.GetVar( "OS_VERSION", "9+" );

				minOSVersion = minOSVersion.TrimEnd( '+' );

				Debug.Log( DEBUG_LOG_PREFIX + "tvOS minOSVersion: " + minOSVersion );

				if ( !minOSVersion.Contains( "." ) )
				{
					minOSVersion = minOSVersion + ".0";
					Debug.Log( DEBUG_LOG_PREFIX + "tvOS minOSVersion: " + minOSVersion );
				}

				PlayerSettings.tvOS.targetOSVersionString = minOSVersion;

				Debug.Log( DEBUG_LOG_PREFIX + "tvOS minOSVersion set to " + PlayerSettings.tvOS.targetOSVersionString );
#if UNITY_2017_1_OR_NEWER
				PlayerSettings.SetApplicationIdentifier( BuildTargetGroup.tvOS, _bundleIdentifier );
#else
				PlayerSettings.iPhoneBundleIdentifier = _bundleIdentifier;
#endif
				PlayerSettings.bundleVersion = AnvilBuildInfo.GetVar( "BUNDLE_VERSION", "1.0.0" );
				PlayerSettings.iOS.buildNumber = AnvilBuildInfo.GetVar( "VERSION", "1" );
				PlayerSettings.iOS.applicationDisplayName = _productName;
				PlayerSettings.tvOS.sdkVersion = tvOSSdkVersion.Device;
				PreBuild_iOS_tvOS();
				break;
			}
#endif//SUPPORT_TVOS
			case BuildTarget.Android:
			{
				InitAndroid();

				// this is not the bamboo version, which is Bamboo.BAMBOO_VERSION
				PlayerSettings.Android.bundleVersionCode = 
					int.Parse( AnvilBuildInfo.GetVar( "VERSION", "1" ) ); 
				
				// for android the bundle version is the 3 digit number.
				PlayerSettings.bundleVersion = AnvilBuildInfo.GetVar( "BUNDLE_VERSION", "1.0.0" );

				bool makeSplitApk = Convert.ToBoolean( AnvilBuildInfo.GetVar( "SPLIT_APK", "false" ) );
				if( makeSplitApk )
				{
					string defineSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup( BuildTargetGroup.Android );
					PlayerSettings.SetScriptingDefineSymbolsForGroup( BuildTargetGroup.Android, defineSymbols + " SPLIT_APK" );

					Debug.Log( DEBUG_LOG_PREFIX + 
						"Split APK defined, setting PlayerSettings.Android.useAPKExpansionFiles to true" );
				}
				else
				{
					Debug.Log( DEBUG_LOG_PREFIX + 
						"Split APK not defined" );
				}
				PlayerSettings.Android.useAPKExpansionFiles = makeSplitApk;

				OutputAndroidManifest( _projectPath + "/Assets/Plugins/Android/AndroidManifest.xml" );

				break;
			}
			case BuildTarget.StandaloneWindows:
				break;
#if SUPPORT_WEBPLAYER
			case BuildTarget.WebPlayer:
				break;
#endif
#if SUPPORT_WEBGL
			case BuildTarget.WebGL:
			{
				InitWebGL();
				break;
			}
#endif
#if UNITY_4
			case BuildTarget.MetroPlayer:
			{
				break;
			}
#else
			case BuildTarget.WSAPlayer:
			{
				InitUWP();
				break;
			}
#endif
#if SUPPORT_SWITCH
			case BuildTarget.Switch:
			{
				InitSwitch();
				break;
			}
#endif
			default:
			{
				throw new System.Exception( string.Format( "Build target {0} not supported! ", _buildTarget ) );
			}
		}

		DoPreBuild();
		DoBuild( _outputPath );

		switch( _buildTarget )
		{
#if UNITY_4
		case BuildTarget.iPhone:
#else
		case BuildTarget.iOS:
#endif
#if SUPPORT_TVOS
		case BuildTarget.tvOS:
#endif
		{
			OutputIosPlist( CombinePaths( _outputPath, "Info.plist" ) );
			
			break;
		}
#if SUPPORT_WEBPLAYER
		case BuildTarget.WebPlayer:
		{
			string	tempOutputName = Path.GetFileName( _outputPath );
			
			// delete the 'out of the box' html file (it'll be replaced by project's version)
			Debug.Log( DEBUG_LOG_PREFIX + "Deleting the Unity html file: " + CombinePaths( _outputPath, tempOutputName + ".html" ) );
			
			SafeDeleteFile( CombinePaths( _outputPath, tempOutputName + ".html" ) );
			
			// rename the .unity File to be what our custom html file looks for. 
			SafeMoveFile( CombinePaths( _outputPath, tempOutputName + ".unity3d" ), CombinePaths( _outputPath, NAME_WEBPLAYER_OUTPUT + ".unity3d" ) );
			
			string	subdir = "";
			
			if ( !string.IsNullOrEmpty( _buildConfig._pWebPlayerOptions._pDirectory ) )
			{
				if ( _buildConfig._pWebPlayerOptions._pDirectory == BuildConfig.WEB_DEMO_DIR_ALIAS )
				{
					subdir = _demoDirectory;
				}
				else
				{
					subdir = _buildConfig._pWebPlayerOptions._pDirectory;
				}
			}
			
			if ( !string.IsNullOrEmpty( subdir ) )
			{
				// move the .unity file into the sub-directory
				SafeMoveFile( CombinePaths( _outputPath, NAME_WEBPLAYER_OUTPUT + ".unity3d" ), CombinePaths( _outputPath, subdir, NAME_WEBPLAYER_OUTPUT + ".unity3d" ) );
			}
			
			// copy in our project specific webplayer files. 
			string	webFilesPath = CombinePaths( _projectPath, _dirFilesWeb );
			
			if ( !string.IsNullOrEmpty( _demoDirectory ) )
			{
				string	demoPath = CombinePaths( webFilesPath, _demoDirectory );
			
				if ( System.IO.Directory.Exists( demoPath ) )
				{
					webFilesPath = demoPath;
				}
			}
			
			DirectoryCopy( webFilesPath, CombinePaths( _outputPath, subdir ), true, RenameHiddenJavascript );
			
			break;
		}
#endif
#if SUPPORT_WEBGL
		case BuildTarget.WebGL:
		{
			// rename the export folder
			Debug.Log( DEBUG_LOG_PREFIX + "Renaming export folder" );
			SafeMoveDirectoryContents( _outputPath, _exportPath );
			SafeDeleteDirectory( _outputPath );
			_outputPath = _exportPath;
			
			// delete the 'out of the box' webgl files (they'll be replaced by project's versions)
			Debug.Log( DEBUG_LOG_PREFIX + "Deleting the default Unity webgl files" );
			
			SafeDeleteFile( CombinePaths( _outputPath, "index.html" ) );
			SafeDeleteDirectory( CombinePaths( _outputPath, "TemplateData" ) );
			
			string	subdir = "";
			
			if ( !string.IsNullOrEmpty( _buildConfig._pWebGLOptions._pDirectory ) )
			{
				if ( _buildConfig._pWebGLOptions._pDirectory == BuildConfig.WEB_DEMO_DIR_ALIAS )
				{
					subdir = _demoDirectory;
				}
				else
				{
					subdir = _buildConfig._pWebGLOptions._pDirectory;
				}
			}
			
			if ( !string.IsNullOrEmpty( subdir ) )
			{
				SafeMoveDirectory( CombinePaths( _outputPath, WEBGL_BUILD_OUTPUT_FOLDER ), CombinePaths( _outputPath, subdir, WEBGL_BUILD_OUTPUT_FOLDER ) );
			}
			
			// copy in our project specific webgl files. 
			string	webGLFilesPath = CombinePaths( _projectPath, _dirFilesWebGL );
			
			if ( !string.IsNullOrEmpty( _demoDirectory ) )
			{
				string	demoPath = CombinePaths( webGLFilesPath, _demoDirectory );
				
				if ( System.IO.Directory.Exists( demoPath ) )
				{
					webGLFilesPath = demoPath;
				}
			}
			
			DirectoryCopy( webGLFilesPath, CombinePaths( _outputPath, subdir ), true, RenameHiddenJavascript );
			
			break;
		}
#endif
		case BuildTarget.Android:
		if ( !_pIsOutputGradleProject )
		{
			bool makeSplitApk = Convert.ToBoolean( AnvilBuildInfo.GetVar( "SPLIT_APK", "false" ) );
			if( makeSplitApk )
			{
				string oldName = CombinePaths( _exportPath, _outputName + ".main.obb" );
				string newName = CombinePaths( _exportPath, "main." + AnvilBuildInfo._pVersion.ToString() + "." + AnvilBuildInfo._pBundleId + ".obb" );
				Debug.Log( DEBUG_LOG_PREFIX + "Renaming OBB file FROM: " + oldName + "  TO: " + newName );
				FileUtil.CopyFileOrDirectory( oldName, newName );
				FileUtil.DeleteFileOrDirectory( oldName );
			}

			if ( _rebuildApkEndStage > ERebuildApkStage.NONE )
			{
				Debug.Log( DEBUG_LOG_PREFIX + "Begin APK Mods" );

				if ( _rebuildApkEndStage >= ERebuildApkStage.DECODE )
				{
					string	decodedPath = CombinePaths( _exportPath, _outputName );
					DecodeApk( _outputPath, decodedPath );
					if ( _androidBlockPerms.Count > 0 )
					{
						RemoveAndroidPermissions( CombinePaths( decodedPath, "AndroidManifest.xml" ) );
					}
					if ( _rebuildApkEndStage >= ERebuildApkStage.REBUILD )
					{
						RebuildApk( _outputPath, decodedPath );
						if ( _isUnsignedApk )
						{
							Debug.Log( DEBUG_LOG_PREFIX + "Not signing APK, as requested" );
						}
						else
						{
							SignApk( _outputPath, PlayerSettings.Android.keystoreName, PlayerSettings.Android.keyaliasName, PlayerSettings.Android.keystorePass, PlayerSettings.Android.keyaliasPass );
							AlignApk( _outputPath );
						}
						if ( _rebuildApkEndStage >= ERebuildApkStage.CLEAN )
						{
							SafeDeleteDirectory( decodedPath );
						}
					}
				}
				Debug.Log( DEBUG_LOG_PREFIX + "End APK Mods" );
			}
		}
		break;
		case BuildTarget.StandaloneWindows:
		case BuildTarget.StandaloneWindows64:
		#if UNITY_2017_3_OR_NEWER
		case BuildTarget.StandaloneOSX:
		#else
		case BuildTarget.StandaloneOSXIntel:
		case BuildTarget.StandaloneOSXIntel64:
		case BuildTarget.StandaloneOSXUniversal:
		#endif
			{
				string tempDir = CombinePaths(_exportPath, "ToZip");

				FileHelper.ForEachFileInDirectory(_exportPath, action: (fi) => {
					SafeMoveFile(fi.FullName, CombinePaths(tempDir, fi.Name));
				}, match: (fi) => !fi.Extension.Equals("zip", StringComparison.OrdinalIgnoreCase));

				FileHelper.ForEachSubDirectory(_exportPath, action: (di) => {
					SafeMoveDirectory(di.FullName, CombinePaths(tempDir, di.Name));
				});

				string zipName = _pOutputName + ".zip";
				string zipPath = CombinePaths(_exportPath, zipName);

				ZipDirectory(zipPath, tempDir);

				SafeDeleteDirectory( tempDir );
			}
			break;
		default:
		{
			break;
		}
		}
		
		WorkOutBuildInfoNameAndLocation();

		OutputBuildData( _buildInfoPathAndName );

		DoPostBuild();
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void DoPreBuild()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Starting PREBUILD steps..." );

		Debug.Log( DEBUG_LOG_PREFIX + "Plugins pre-build" );
		ForEachBuildPlugin( p => {
			p.OnPreBuild();
		}, true );
		
		if( _onPreBuild != null )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "On Prebuild Events defined, Calling them now..." );
			if( !_onPreBuild() ) return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void DoBuild( string outPath )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Starting BUILD steps..." );

		if( _onBuild != null )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "On Build Events defined, Calling them now..." );
			if( !_onBuild() ) return;
		}

		bool	isWantBuild = _pIsWantBuildApp;
		bool	isWantBundles = _pIsWantBuildAssetBundles;

		if ( isWantBundles )
		{
			//CC20161012 - Asset bundles support
			string	assetBundlesOutputPath = _pAssetBundlesOutputPath;

			Directory.CreateDirectory( assetBundlesOutputPath );

			Debug.Log( DEBUG_LOG_PREFIX + "Building asset bundles" );

			AssetBundleManifest	assetBundleManifest = BuildPipeline.BuildAssetBundles( assetBundlesOutputPath, BuildAssetBundleOptions.ForceRebuildAssetBundle, _buildTarget );

			int	assetBundleCount = 0;

			if ( assetBundleManifest != null )
			{
				string[]	assetBundleNames = assetBundleManifest.GetAllAssetBundles();

				if ( assetBundleNames != null )
				{
					assetBundleCount = assetBundleNames.Length;
#if SUPPORT_ODR
					if ( IsEnableAppleOnDemandResources() )
					{
						UnityEditor.iOS.Resource[]	resources = new UnityEditor.iOS.Resource[ assetBundleCount ];

						for ( int i = 0; i < assetBundleCount; ++i )
						{
							resources[i] = new UnityEditor.iOS.Resource( assetBundleNames[i], CombinePaths( assetBundlesOutputPath, assetBundleNames[i] ) ).AddOnDemandResourceTags( assetBundleNames[i] + "-odr" );
						}

						UnityEditor.iOS.BuildPipeline.collectResources += () => resources;
					}
#endif
				}
			}

			Debug.Log( DEBUG_LOG_PREFIX + string.Format( "{0} asset bundles created", assetBundleCount ) );

			if ( assetBundleCount == 0 )
			{
				Directory.Delete( assetBundlesOutputPath );
			}
			// END - Asset bundles support
		}

		if ( isWantBuild )
		{
			string[] levels = null;
			int num = 0;
			int i = 0;
#if UNITY_4
			bool mobile = _buildTarget == BuildTarget.Android || _buildTarget == BuildTarget.iPhone;
#else
			bool mobile = _buildTarget == BuildTarget.Android || _buildTarget == BuildTarget.iOS;
#endif

			foreach( EditorBuildSettingsScene scene in EditorBuildSettings.scenes )
			{
				if( scene.enabled || mobile )
				{
					++num;
				}
			}

			if ( num == 0 )
			{
				ForceFailBuild( "No scenes in build settings scene list" );
			}

			levels = new string[num];

			foreach( EditorBuildSettingsScene scene in EditorBuildSettings.scenes )
			{
				if( scene.enabled || mobile )
				{
					levels[i++] = scene.path;
				}
			}
#if UNITY_2017_1_OR_NEWER
			EditorUserBuildSettings.SwitchActiveBuildTarget( _buildTargetGroup, _buildTarget );
#else
			EditorUserBuildSettings.SwitchActiveBuildTarget( _buildTarget );
#endif
#if SUPPORT_WEBPLAYER
			EditorUserBuildSettings.webPlayerStreamed = true;
			EditorUserBuildSettings.webPlayerOfflineDeployment = false;
#endif

			BuildOptions options = BuildOptions.None;

			if ( _isDevelopment )
			{
				options |= BuildOptions.Development;
			}
		
			if ( _isAllowDebugging )
			{
				options |= BuildOptions.AllowDebugging;
			}

			string levelsStr = "";

			foreach( string level in levels )
			{
				levelsStr += "\n\t" + level;
			}

			Debug.Log( DEBUG_LOG_PREFIX + "Building with Levels: " + levelsStr );

			BuildPlayerResult	result = BuildPipeline.BuildPlayer( levels, outPath, _buildTarget, options );

			#if UNITY_2018_3_OR_NEWER
			if( result.summary.result != UnityEditor.Build.Reporting.BuildResult.Succeeded )
			{
				throw new System.Exception( "Failed to build game: " + result.summary.result );
			}
			#else
			if( result.Length > 0 )
			{
				throw new System.Exception( "Failed to build game: " + result );
			}
			#endif
		}

		if ( isWantBuild )
		{
#if UNITY_4
			if( _buildTarget == BuildTarget.iPhone )
#else
#if SUPPORT_TVOS
			if( _buildTarget == BuildTarget.iOS || _buildTarget == BuildTarget.tvOS )
#else
			if( _buildTarget == BuildTarget.iOS )
#endif
#endif
			{
				OutputIosPlist( CombinePaths( _outputPath, "Info.plist" ) );
			
				Debug.Log( "iOS/tvOS Build: Copying additional files..." );
			
				DirectoryCopy( _dirAdditionalFilesIos, _outputPath );
			
				bool	isBuildXcodeProject = true;
			
				if ( _buildConfig._pIosOptions._pXcodeProjectAction.HasValue )
				{
					isBuildXcodeProject = _buildConfig._pIosOptions._pXcodeProjectAction.Value > 0;
				}
			
				if ( isBuildXcodeProject )
				{
					DoXcodeBuild();
				}
				else
				{
					Debug.LogWarning( DEBUG_LOG_PREFIX + "iOS/tvOS Build: Not doing Xcode build!" );
				}
			}

			if( _buildTarget == BuildTarget.WSAPlayer )
			{
				//Debug.Log( "UWP Build: Copying additional files..." );
			
				//DirectoryCopy( _dirAdditionalFilesUwp, _outputPath );
			
				bool	isPreBuildProject = true;
				bool	isBuildProject = true;
			
				if ( _buildConfig._pUwpOptions._pVsProjectAction.HasValue )
				{
					isPreBuildProject = _buildConfig._pUwpOptions._pVsProjectAction.Value > 0;
					isBuildProject = _buildConfig._pUwpOptions._pVsProjectAction.Value > 1;
				}
			
				if ( isPreBuildProject )
				{
					DoVisualStudioPreBuild();

					if ( isBuildProject )
					{
						DoVisualStudioBuild();
					}
					else
					{
						Debug.LogWarning( DEBUG_LOG_PREFIX + "UWP Build: Not doing VS build, only pre-build steps." );
					}
				}
				else
				{
					Debug.LogWarning( DEBUG_LOG_PREFIX + "UWP Build: Not doing VS build or pre-build steps." );
				}
			}
		}

		if ( !isWantBuild ) throw new Exception( "No build" );
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void DoPostBuild()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Starting POSTBUILD steps..." );

		CleanUp();

#if UNITY_4
		if( _buildTarget == BuildTarget.iPhone )
#else
#if SUPPORT_TVOS
		if( _buildTarget == BuildTarget.iOS || _buildTarget == BuildTarget.tvOS )
#else
		if( _buildTarget == BuildTarget.iOS )
#endif
#endif
		{
			bool	isDeleteXcodeProject = true;
			
			if ( _buildConfig._pIosOptions._pXcodeProjectAction.HasValue )
			{
				isDeleteXcodeProject = _buildConfig._pIosOptions._pXcodeProjectAction.Value == 2;
			}
			
			if ( isDeleteXcodeProject )
			{
				SafeDeleteDirectory( _outputPath );
			}
			else
			{
				Debug.LogWarning( DEBUG_LOG_PREFIX + "Not deleting Xcode project!" );
			}

#if SUPPORT_ODR
			bool	isDeleteAssetBundles = IsEnableAppleOnDemandResources();
#else
			bool	isDeleteAssetBundles = false;
#endif
			if ( isDeleteAssetBundles )
			{
				string	assetBundlesOutputPath = _pAssetBundlesOutputPath;

				if ( Directory.Exists( assetBundlesOutputPath ) )
				{
					Debug.LogWarning( DEBUG_LOG_PREFIX + "Deleting asset bundles folder: " + assetBundlesOutputPath );

					SafeDeleteDirectory( assetBundlesOutputPath );
				}
			}
		}

		if( _buildTarget == BuildTarget.WSAPlayer )
		{
			bool	isDeleteProject = true;
			
			if ( _buildConfig._pUwpOptions._pVsProjectAction.HasValue )
			{
				isDeleteProject = _buildConfig._pUwpOptions._pVsProjectAction.Value == 3;
			}
			
			if ( isDeleteProject )
			{
				SafeDeleteDirectory( _outputPath );
			}
			else
			{
				Debug.LogWarning( DEBUG_LOG_PREFIX + "Not deleting VS project!" );
			}
		}

		if( _onPostBuild != null )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "On PostBuild Events defined, Calling them now..." );
			if( !_onPostBuild() ) return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void ForceFailBuild( string message )
	{
		Debug.LogError( DEBUG_LOG_PREFIX + message );

		//CleanUp(); now done in exception handling

		throw new System.Exception( DEBUG_LOG_PREFIX + "Failed build: " + message );
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void CleanUp()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Cleaning up!" );

		if ( _cachedProductName != null )
		{
			// set the product name back to what it was before so we can use it for ford look up next time. 
			PlayerSettings.productName = _cachedProductName;
			Debug.Log( DEBUG_LOG_PREFIX + "Product Name Reset to value at start of build: " + _cachedProductName );
		}

		if ( _cachedCompanyName != null )
		{
			// set the company name back to what it was before
			PlayerSettings.companyName = _cachedCompanyName;
			Debug.Log( DEBUG_LOG_PREFIX + "Company Name Reset to value at start of build: " + _cachedProductName );
		}

		if ( _buildTargetGroup != BuildTargetGroup.Unknown && _cachedDefines != null )
		{
			ScriptingDefineSymbolsFacade.SetPlayerSettingsSymbols( _buildTargetGroup, _cachedDefines );
			Debug.Log( DEBUG_LOG_PREFIX + "Define symbols reset to value at start of build: " + _cachedDefines );
		}

#if SUPPORT_WEBGL
		CleanUpWebGL();
#endif
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void WorkOutOutputName()
	{
		_defaultOutputName = DEFAULT_OUTPUT_NAME;
		
		if ( _buildConfig != null )
		{
			List<string>	parts = new List<string>(3);
			
			if ( !string.IsNullOrEmpty( _productCode ) )
			{
				parts.Add( _productCode );
			}
			
			if ( !string.IsNullOrEmpty( _buildConfig._pBuildVersion ) )
			{
				parts.Add( _buildConfig._pBuildVersion );
			}
			
			if ( !string.IsNullOrEmpty( _buildConfig._pBuildId ) )
			{
				parts.Add( _buildConfig._pBuildId );
			}
			
			if ( parts.Count > 0 )
			{
				_defaultOutputName = string.Join( "_", parts.ToArray() );
			}
		}
			
		if ( _buildConfig != null && !string.IsNullOrEmpty( _buildConfig._pOutputName ) )
		{
			_outputName = _buildConfig._pOutputName;
		}
		else
		{
			_outputName = _defaultOutputName;
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static void WorkOutBuildInfoNameAndLocation()
	{
		_buildInfoPathAndName = BuildUtils.GetBuildInfoPath( _exportPath, _defaultOutputName );

		Debug.Log( DEBUG_LOG_PREFIX + "Build Info Text file path and name has been worked out to be: " + _buildInfoPathAndName );
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void WorkOutOutputDirectoryOfBuild()
	{
		switch( _buildTarget )
		{
#if UNITY_4
		case BuildTarget.iPhone:
#else
		case BuildTarget.iOS:
#endif
		{
			_outputPath = CombinePaths( _exportPath, IOS_BUILD_FOLDER_NAME );
			System.IO.Directory.CreateDirectory( _exportPath );
			break;
		}
#if SUPPORT_TVOS
		case BuildTarget.tvOS:
		{
			_outputPath = CombinePaths( _exportPath, TVOS_BUILD_FOLDER_NAME );
			System.IO.Directory.CreateDirectory( _exportPath );
			break;
		}
#endif
		case BuildTarget.Android:
		{
			_outputPath = CombinePaths( _exportPath, _outputName );
			if ( !_pIsOutputGradleProject )
			{
				_outputPath += ".apk";
			}
			System.IO.Directory.CreateDirectory( _exportPath );
			break;
		}
		case BuildTarget.StandaloneWindows:
		{
			_outputPath = CombinePaths( _exportPath, _outputName + ".exe" );
			System.IO.Directory.CreateDirectory( _exportPath );
			break;
		}
#if SUPPORT_WEBPLAYER
		case BuildTarget.WebPlayer:
		{
			_outputPath = _exportPath;
			System.IO.Directory.CreateDirectory( _outputPath );
			break;
		}
#endif
#if SUPPORT_WEBGL
		case BuildTarget.WebGL:
		{
			//rename output folder to "Export" (for example) later.
			_outputPath = CombinePaths( _projectPath, NAME_WEBGL_OUTPUT );
			System.IO.Directory.CreateDirectory( _outputPath );
			break;
		}
#endif
#if UNITY_4
		case BuildTarget.MetroPlayer:
		{
			_outputPath = CombinePaths( _exportPath, _outputName );
			System.IO.Directory.CreateDirectory( _outputPath );
			break;
		}
#else
		case BuildTarget.WSAPlayer:
		{
			_outputPath = CombinePaths( _exportPath, UWP_BUILD_FOLDER_NAME );
			System.IO.Directory.CreateDirectory( _outputPath );
			break;
		}
#endif
#if SUPPORT_SWITCH
		case BuildTarget.Switch:
		{
			_outputPath = CombinePaths( _exportPath, _outputName );
			break;
		}
#endif
		default:
		{
			throw new Exception( "Platform target has no switch case in WorkOutOutputDirectoryOfBuild: " + 
			                    _buildTarget.ToString() );
		}
		}
		
		Debug.Log( DEBUG_LOG_PREFIX + "Output Path for this build target platform has been worked out to be: " + _outputPath );
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void InitJdk()
	{
		string	jdkPath = GetBuildOrAgentVersionedPath( 
			getBuildPath:() => _pBuildConfig._pJdkOptions._pPath,
			getBuildVersion:() => _pBuildConfig._pJdkOptions._pVersion, 
			getAgentPath:( string wantVersion, out string gotVersion ) => _pAgentConfig.GetJdkPath( wantVersion, out gotVersion ), 
			isValidPath:( string path ) => !string.IsNullOrEmpty( path ),
			defaultPath:null );
		
		Debug.Log( LOG_TAG + "JDK: " + jdkPath );

		if ( string.IsNullOrEmpty( jdkPath ) ) return;

		_jarPath = FindFile( CombinePaths( jdkPath, "bin", "jar" ), "", "exe" );
		_javaPath = FindFile( CombinePaths( jdkPath, "bin", "java" ), "", "exe" );
		_jarsignerPath = FindFile( CombinePaths( jdkPath, "bin", "jarsigner" ), "", "exe" );
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void OutputBuildData( string outputFilepath )
	{
		File.WriteAllText( outputFilepath, AnvilBuildInfo.GetData() );
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static void OutputAndroidManifest( string outputFilepath )
	{		
		Debug.Log( DEBUG_LOG_PREFIX + "OutputAndroidManifest: " + outputFilepath );

		XmlDocument manifest = new XmlDocument();
		manifest.Load( _projectPath + "/" + _dirFilesAndroid + "AndroidManifestTemplate.xml" );

		XmlElement rootElem = manifest.DocumentElement as XmlElement;
		
		_androidManifestNamespaceURI = rootElem.GetNamespaceOfPrefix( "android" );

		rootElem.SetAttribute( "versionCode", _androidManifestNamespaceURI, AnvilBuildInfo.GetVar( "VERSION", "1" ) );
		rootElem.SetAttribute( "versionName", _androidManifestNamespaceURI, AnvilBuildInfo.GetVar( "BUNDLE_VERSION", "1.0.0" ) );

		XmlUtils.SetXmlChild( rootElem, "uses-sdk", usesSdkElem => {
			usesSdkElem.SetAttribute( "minSdkVersion", _androidManifestNamespaceURI, AnvilBuildInfo.GetVar( "OS_VERSION", "9+").TrimEnd( '+' ) );
		} );

		if ( AnvilProjectConfig._pDoAndroidManifestTemplateMods )
		{
			XmlUtils.SetXmlChild( rootElem, "application", applicationElem => {
				applicationElem.SetAttribute( "debuggable", _androidManifestNamespaceURI, ( _isAllowDebugging ? "true" : "false" ) );
			} );

			if ( _isAndroidLeanbackSupported )
			{
				XmlUtils.SetXmlChild( rootElem, "application", applicationElem => {
					applicationElem.SetAttribute( "banner", _androidManifestNamespaceURI, "@drawable/app_banner" );
				} );
			}

			_androidMainActivityName = DEFAULT_ANDROID_MAIN_ACTIVITY_NAME;

			Debug.Log( DEBUG_LOG_PREFIX + "Plugins modifying Android main activity" );
			bool	isModified = false;
			ForEachBuildPlugin( p => {
				if ( p.OnSetAndroidMainActivity( ref _androidMainActivityName ) )
				{
					if ( isModified )
					{
						Debug.LogWarning( DEBUG_LOG_PREFIX + "Android main activity modified multiple times" );
					}
					else
					{
						isModified = true;
					}
				}
			}, true );

			//<intent-filter>
			//  <action android:name="android.intent.action.MAIN" />
			//  <category android:name="android.intent.category.LAUNCHER" />
			//  <category android:name="android.intent.category.LEANBACK_LAUNCHER" />
			//</intent-filter>
			//<meta-data android:name="unityplayer.ForwardNativeEventsToDalvik" android:value="true" />
			XmlUtils.SetXmlChild( rootElem, "application", applicationElem => {
				SetAndroidManifestElement( manifest, applicationElem, "activity", "name", _androidMainActivityName, activityElem => {
					activityElem.SetAttribute( "screenOrientation", _androidManifestNamespaceURI, "sensorLandscape" );
					activityElem.SetAttribute( "launchMode", _androidManifestNamespaceURI, "singleTask" );
					XmlUtils.SetXmlChild( activityElem, "intent-filter", intentElem => {
						SetAndroidManifestElement( manifest, intentElem, "action", "name", "android.intent.action.MAIN", null );
						SetAndroidManifestElement( manifest, intentElem, "category", "name", "android.intent.category.LAUNCHER", null );
						if ( _isAndroidLeanbackSupported )
						{
							SetAndroidManifestElement( manifest, intentElem, "category", "name", "android.intent.category.LEANBACK_LAUNCHER", null );
						}
					} );
					SetAndroidManifestElement( manifest, activityElem, "meta-data", "name", "unityplayer.UnityActivity", unityActivityElem => {
						unityActivityElem.SetAttribute( "value", _androidManifestNamespaceURI, "true" );
					} );
					string	customUrlScheme = AnvilBuildInfo.GetVar( "CUSTOM_URL", null );
					if ( !string.IsNullOrEmpty( customUrlScheme ) )
					{
						XmlUtils.AddXmlChild( activityElem, "intent-filter", intentElem => {
							SetAndroidManifestElement( manifest, intentElem, "data", "scheme", customUrlScheme, null );
							SetAndroidManifestElement( manifest, intentElem, "action", "name", "android.intent.action.VIEW", null );
							SetAndroidManifestElement( manifest, intentElem, "category", "name", "android.intent.category.DEFAULT", null );
							SetAndroidManifestElement( manifest, intentElem, "category", "name", "android.intent.category.BROWSABLE", null );
						} );
					}
					SetAndroidManifestElement( manifest, activityElem, "meta-data", "name", "unityplayer.ForwardNativeEventsToDalvik", forwardEventsElem => {
						forwardEventsElem.SetAttribute( "value", _androidManifestNamespaceURI, "true" );
					} );
				} );
			} );

			Debug.Log( DEBUG_LOG_PREFIX + "Plugins modifying Android manifest" );
			ForEachBuildPlugin( p => {
				p.OnSetAndroidManifestEntries( manifest, rootElem );
			}, true );

			// if this is a split apk build we need to have the write external storage permission
			if( PlayerSettings.Android.useAPKExpansionFiles )
			{
				SetAndroidPermission( manifest, "android.permission.WRITE_EXTERNAL_STORAGE" );
			}
		}
		else
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Skipping Android manifest template mods" );
		}

		//CC20180504 - ensure target dir exists
		Directory.CreateDirectory( Path.GetDirectoryName( outputFilepath ) );

		manifest.Save( outputFilepath );
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void OutputIosPlist( string outputFilepath )
	{
		XmlDocument plist = new XmlDocument();
		plist.Load( CombinePaths( _projectPath, _pDirFilesIosTvos, "Template.plist" ) );

		XmlElement dict = plist.DocumentElement.GetElementsByTagName("dict")[0] as XmlElement;
		bool key = true;
		string keyName = "";
		string[] devcaps = AnvilBuildInfo._pDeviceCapabilities.Split('|');
		string	appName = _productName;

		foreach(XmlNode node in dict.ChildNodes)
		{
			XmlElement e = node as XmlElement;
			if(e == null) throw new Exception("Unknown plist structure");

			if(key)
			{
				keyName = e.InnerText;
			}
			else
			{
				switch(keyName)
				{
					case "CFBundleDisplayName":
						e.InnerText = appName;
						break;
					case "CFBundleIdentifier":
						e.InnerText = _bundleIdentifier;
						break;
					case "CFBundleVersion":
						e.InnerText = AnvilBuildInfo.GetVar("VERSION", "1");
						break;
					case "CFBundleShortVersionString":
						e.InnerText = AnvilBuildInfo.GetVar("BUNDLE_VERSION", "1.0.0");
						break;
					case "UIRequiredDeviceCapabilities":
						e.RemoveAll();

						foreach(string cap in devcaps)
						{
							XmlElement elem = plist.CreateElement("string");
							XmlText text = plist.CreateTextNode(cap);
							e.AppendChild(elem);
							e.LastChild.AppendChild(text);
						}
						break;
				}
			}

			key = !key;
		}

		SetLanguagesInIosPlist( plist, dict );
		SetOrientationInIosPlist( plist, dict );

		string	customUrlScheme = AnvilBuildInfo.GetVar( "CUSTOM_URL", null );
		if ( !string.IsNullOrEmpty( customUrlScheme ) )
		{
			SetIosPlistDictValue( plist, dict, "CFBundleURLTypes", "array", urlTypesElem => {
				XmlUtils.AddXmlChild( urlTypesElem, "dict", newUrlTypeElem => {
					SetIosPlistDictValue( plist, newUrlTypeElem, "CFBundleURLName", "string", urlNameElem => {
						urlNameElem.InnerText = "CUSTOM_URL";
					} );
					SetIosPlistDictValue( plist, newUrlTypeElem, "CFBundleURLSchemes", "array", urlSchemesElem => {
						XmlUtils.AddXmlChild( urlSchemesElem, "string", newUrlSchemeElem => {
							newUrlSchemeElem.InnerText = customUrlScheme;
						} );
					} );
				} );
			} );
		}
		
		Debug.Log( DEBUG_LOG_PREFIX + "Plugins modifying iOS plist" );
		ForEachBuildPlugin( p => {
			p.OnSetIosPlistEntries( plist, dict );
		}, true );

		SaveIosPlist( plist, outputFilepath );
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public static void SetAndroidManifestElement( XmlDocument manifest, XmlElement parentElem, string elemType, string idKey, string idValue, System.Action<XmlElement> setAction )
	{
		XmlUtils.SetXmlChild( parentElem, e => ( e.Name == elemType && e.HasAttribute( idKey, _androidManifestNamespaceURI ) && e.GetAttribute( idKey, _androidManifestNamespaceURI ) == idValue ), () => {
			XmlElement	e = manifest.CreateElement( elemType );
			if ( e == null ) return null;
			e.SetAttribute( idKey, _androidManifestNamespaceURI, idValue );
			return e;
		}, setAction );
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public static void RemoveAndroidManifestElement( XmlDocument manifest, XmlElement parentElem, string elemType, string idKey, string idValue )
	{
		XmlUtils.RemoveXmlChild( parentElem, e => ( e.Name == elemType && e.HasAttribute( idKey, _androidManifestNamespaceURI ) && e.GetAttribute( idKey, _androidManifestNamespaceURI ) == idValue ) );
	}
	
	//------------------------------------------------------------------------------------------------------------------
	// Added 2016-4-21
	//  <uses-permission android:name="android.permission.INTERNET" />
	public static void SetAndroidPermission( XmlDocument manifest, string permission )
	{
		XmlElement rootElem = manifest.DocumentElement as XmlElement;

		SetAndroidManifestElement( manifest, rootElem, "uses-permission", "name", permission, null );
	}

	//------------------------------------------------------------------------------------------------------------------
	// Added 2016-6-10
	//  <uses-permission android:name="android.permission.INTERNET" />
	public static void RemoveAndroidPermission( XmlDocument manifest, string permission )
	{
		XmlElement rootElem = manifest.DocumentElement as XmlElement;

		RemoveAndroidManifestElement( manifest, rootElem, "uses-permission", "name", permission );
	}

	//------------------------------------------------------------------------------------------------------------------
	// Added 2016-4-21
	// relies on sdk version being set correctly above 1, then this permission will be blocked if added by processes 
	// beyond our control, eg 3rd party plugins.
	//  <uses-permission android:name="android.permission.INTERNET" android:maxSdkVersion="1" />
	public static void SetAndroidPermissionBlocked( XmlDocument manifest, string permission )
	{
		XmlElement rootElem = manifest.DocumentElement as XmlElement;

		SetAndroidManifestElement( manifest, rootElem, "uses-permission", "name", permission, e => {
			e.SetAttribute( "maxSdkVersion", _androidManifestNamespaceURI, "1" );
		} );
	}

	//------------------------------------------------------------------------------------------------------------------
	public static void SetIosPlistDictValue( XmlDocument plist, XmlElement dict, string key, string valueType, System.Action<XmlElement> setAction )
	{
		XmlElement	valueElem = FindIosPlistDictValueElement( dict, key, valueType );
		
		if ( valueElem != null )
		{
			if ( setAction != null )
			{
				setAction( valueElem );
			}
		}
		else
		{
			XmlUtils.AddXmlChild( dict, "key", keyElem => {
				keyElem.InnerText = key;
			} );
			
			XmlUtils.AddXmlChild( dict, valueType, setAction );
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public static void SetIosPlistDictBooleanValue( XmlDocument plist, XmlElement dict, string key, bool value )
	{
		string	valueStr = value.ToString().ToLower();

		SetIosPlistDictValue( plist, dict, key, valueStr, e => {
			if ( e.Name != valueStr )
			{
				dict.ReplaceChild( plist.CreateElement( valueStr ), e );
			}
		} );
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public static bool HasIosPlistDictEntry( XmlElement dict, string key, string valueType, System.Predicate<XmlElement> valueMatch )
	{
		XmlElement	valueElem = FindIosPlistDictValueElement( dict, key, valueType );

		return valueElem != null && ( valueMatch == null || valueMatch( valueElem ) == true );
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static XmlElement FindIosPlistDictValueElement( XmlElement dict, string key, string valueType, System.Action<XmlElement> onFoundKey = null )
	{
		XmlElement	keyElem = null;
		XmlElement	valueElem = null;

		bool	isFindKey = true;
		bool	isFoundKey = false;
		
		foreach ( XmlNode node in dict.ChildNodes )
		{
			XmlElement	elem = node as XmlElement;
			
			if ( elem == null ) continue;
			
			if ( isFindKey )
			{
				if ( elem.InnerText == key )
				{
					isFoundKey = true;
					keyElem = elem;
				}
			}
			else if ( isFoundKey )
			{
				if ( valueType != null && elem.Name != valueType )
				{
					Debug.LogWarning( "[FindIosPlistValueElement] Found element name '" + elem.Name + "' does not match expected value type '" + valueType + "'" );
				}
				valueElem = elem;
				break;
			}
			
			isFindKey = !isFindKey;
		}

		if ( onFoundKey != null )
		{
			onFoundKey( keyElem );
		}
		
		return valueElem;
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void RemoveIosPlistDictionaryEntry( XmlElement dict, string key, string valueType )
	{
		XmlElement	valueElem = FindIosPlistDictValueElement( dict, key, valueType, onFoundKey:( XmlElement keyElem ) => {
			dict.RemoveChild( keyElem );
		} );

		if ( valueElem != null )
		{
			dict.RemoveChild( valueElem );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public static void SetIosPlistApplicationQueriesScheme( XmlDocument plist, XmlElement applicationQueriesSchemesElem, string scheme )
	{
		XmlUtils.SetXmlChild( applicationQueriesSchemesElem, e => e.Name == "string" && e.InnerText == scheme, () => plist.CreateElement( "string" ), e => {
			e.InnerText = scheme;
		} );
	}

	//------------------------------------------------------------------------------------------------------------------
	public static void SetIosPlistExceptionDomain( XmlDocument plist, XmlElement exceptionDomainsElem, string domain, bool isIncludeSubdomains = true, bool isRequiresForwardSecrecy = false )
	{
		SetIosPlistDictValue( plist, exceptionDomainsElem, domain, "dict", e => {
			SetIosPlistDictBooleanValue( plist, e, "NSIncludesSubdomains", isIncludeSubdomains );
			SetIosPlistDictBooleanValue( plist, e, "NSThirdPartyExceptionRequiresForwardSecrecy", isRequiresForwardSecrecy );
		} );
	}

	private sealed class Utf8StringWriter : StringWriter
	{
		public override System.Text.Encoding Encoding
		{
			get
			{
				return System.Text.Encoding.UTF8;
			}
		}
	}

	// remove the pesky empty brackets in the doctype, which are standards compliant, but not Apple Plist Tool compliant!
	private static void SaveIosPlist( XmlDocument plist, string filePath )
	{
		//CC20170922 - ensure utf-8 encoding, System.Xml in C# v4.6 (in Unity 2017) seems to force utf-16
		StringWriter sw = new Utf8StringWriter();

		plist.Save( sw );

		string	plistText = sw.ToString();
		int		docTypeIdx = plistText.IndexOf( "<!DOCTYPE" );

		if ( docTypeIdx >= 0 )
		{
			int	endOfLineIdx = plistText.IndexOfAny( new char[] { '\n', '\r', '\0' }, docTypeIdx );
			if ( endOfLineIdx >= 0 )
			{
				int	bracketIdx = plistText.IndexOf( "[]", docTypeIdx, endOfLineIdx - docTypeIdx );
				if ( bracketIdx >= 0 )
				{
					Debug.Log( DEBUG_LOG_PREFIX + "Removing \"[]\" from plist DOCTYPE tag." );
					plistText = plistText.Remove( bracketIdx, 2 );
				}
			}
		}
		
		File.WriteAllText( filePath, plistText );
	}
	
	private static void SetBuildTargetGroupFromBuildTarget()
	{
		_buildTargetGroup = BuildTargetUtils.GetBuildTargetGroup( _buildTarget );

		if ( _buildTargetGroup == BuildTargetGroup.Unknown )
		{
			throw new Exception( "Unexpected build target: " + _buildTarget.ToString() );
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static void EmbedPlatformDefines()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Embedding Platform Defines" );
		
		string	buildId = ( _buildConfig != null && !String.IsNullOrEmpty( _buildConfig._pBuildId ) ) ? _buildConfig._pBuildId : "Internal";
		string	buildVersion = ( _buildConfig != null && !String.IsNullOrEmpty( _buildConfig._pBuildVersion ) ) ? _buildConfig._pBuildVersion : null;

		if ( ScriptingDefineSymbolsFacade.BeginChanges( _buildTargetGroup ) == true )
		{
			ScriptingDefineSymbolsFacade.AddSymbol( buildId, _buildTargetGroup );
		
			if ( _buildConfig != null )
			{
				for ( int i = 0; i < _buildConfig._pDefines.Count; ++i )
				{
					ScriptingDefineSymbolsFacade.AddSymbol( _buildConfig._pDefines[i], _buildTargetGroup );
				}
			}

			ScriptingDefineSymbolsFacade.EndChanges();
		}
		
		string	definesString = ScriptingDefineSymbolsFacade.GetPlayerSettingsSymbols( _buildTargetGroup );

#if PUMA_DEFINE_PUMA
		Puma.PackageManager.RefreshDatabase();
		Puma.PackageManager.ApplyPackageSymbols();
#endif

		string	productCode = string.IsNullOrEmpty( _productCode ) ? _cachedProductName : _productCode;
		string	localAssetPath = Path.Combine( ANVIL_HOME, "Resources/BuildInfo.txt" );
		string	fullAssetPath = Path.Combine( _projectPath, localAssetPath );

		if ( string.IsNullOrEmpty( _buildInfo ) )
		{
			BuildUtils.DownloadBuildInfo( 
				productCode, 
				_buildTarget.ToString(), 
				buildId, 
				_outputName, 
				definesString, 
				buildVersion, 
				onUrlFormed: url => {
					Debug.Log( DEBUG_LOG_PREFIX + "4ORD URL: " + url );
				},
				onResult: ( isResult, contents ) => {
					if ( !isResult )
					{
						UnityEngine.Debug.LogWarning( "4ord returned no results" );
					}
					_buildInfo = contents;
				}, 
				outputPath: fullAssetPath
			);
		}
		else
		{
			using ( StreamWriter writer = new StreamWriter( path:fullAssetPath, append:false ) )
			{
				writer.Write( _buildInfo );
			}
		}

		Debug.Log( DEBUG_LOG_PREFIX + "Platform Defines\n------------------\n" + _buildInfo + "\n------------------------" );

		AssetDatabase.ImportAsset( localAssetPath, ImportAssetOptions.ForceUpdate );

		AnvilBuildInfo.Initialize( _buildInfo );
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static ValueT GetBuildOrAgentValue<ValueT>( System.Func<ValueT> getBuildValue, System.Func<ValueT> getAgentValue, System.Predicate<ValueT> isValidValue, ValueT defaultValue )
	{
		ValueT	value;

		if ( _buildConfig == null || isValidValue( ( value = getBuildValue() ) ) == false )
		{
			if ( _agentConfig == null || isValidValue( ( value = getAgentValue() ) ) == false )
			{
				value = defaultValue;
			}
		}

		return value;
	}

	//------------------------------------------------------------------------------------------------------------------
	private delegate string DGetAgentVersionedPath( string wantVersion, out string gotVersion );
	private static string GetBuildOrAgentVersionedPath( System.Func<string> getBuildPath, System.Func<string> getBuildVersion, DGetAgentVersionedPath getAgentPath, System.Predicate<string> isValidPath, string defaultPath )
	{
		return GetBuildOrAgentValue( 
			getBuildValue:() => getBuildPath(), 
			getAgentValue:() => {
				string	path, gotVersion;
				string	buildVersion = getBuildVersion();
				if ( _pBuildConfig != null && !string.IsNullOrEmpty( buildVersion ) )
				{
					path = getAgentPath( buildVersion, out gotVersion );
				}
				else
				{
					path = getAgentPath( "", out gotVersion );
				}
				return path;
			},
			isValidValue:( string value ) => isValidPath( value ),
			defaultValue:defaultPath );
	}

	//------------------------------------------------------------------------------------------------------------------
	private static string FindFile( string pathBase, params string[] extensions )
	{
		foreach ( string ext in extensions )
		{
			string	path = pathBase;

			if ( !string.IsNullOrEmpty( ext ) )
			{
				path += "." + ext;
			}

			FileInfo	fi = new FileInfo( path );

			if ( fi.Exists ) return path;
		}

		return null;
	}

	//------------------------------------------------------------------------------------------------------------------
	private static string CombinePaths( params string[] paths )
	{
		string	combined = "";
		
		for ( int i = 0; i < paths.Length; ++i )
		{
			combined = Path.Combine( combined, paths[i] );
		}
		
		return combined;
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void SafeDeleteFile( string file )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Deleting file: " + file );
		try
		{
			System.IO.File.Delete( file );
		}
		catch( System.Exception e )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Failed to delete! " + e.Message );
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static void SafeDeleteDirectory( string dir )
	{
		dir = SafePath( dir );

		Debug.Log( DEBUG_LOG_PREFIX + "Deleting directory: " + dir );

		try
		{
			System.IO.Directory.Delete( dir, true );
		}
		catch( System.Exception e )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Failed to delete! " + e.Message );
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public static void SafeMoveFile( string fromFile, string toFile )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Moving file: " + fromFile + " --> " + toFile );
		try
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Ensuring parent directories of " + toFile );
			System.IO.Directory.CreateDirectory( Path.GetDirectoryName( toFile ) );
			Debug.Log( DEBUG_LOG_PREFIX + "Moving file: " + fromFile + " --> " + toFile );
			System.IO.File.Move( fromFile, toFile );
		}
		catch( System.Exception e )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Failed to move! " + e.Message);
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static void SafeMoveDirectory( string fromDir, string toDir )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Moving directory: " + fromDir + " --> " + toDir );
		try
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Ensuring parent directories of " + toDir );
			System.IO.Directory.CreateDirectory( toDir );
			SafeDeleteDirectory( toDir );
			Debug.Log( DEBUG_LOG_PREFIX + "Moving directory: " + fromDir + " --> " + toDir );
			System.IO.Directory.Move( fromDir, toDir );
		}
		catch( System.Exception e )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Failed to move! " + e.Message );
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static void SafeMoveDirectoryContents( string sourceDirName, string destDirName )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Moving contents of: " + sourceDirName + " into: " + destDirName );
		
		// Get the subdirectories for the specified directory.
		DirectoryInfo dir = new DirectoryInfo( sourceDirName );
		
		if( !dir.Exists )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Source directory not found, no files to copy." );
			return;
		}
		
		// If the destination directory doesn't exist, create it. 
		if( !Directory.Exists( destDirName ) )
		{
			Directory.CreateDirectory( destDirName );
			Debug.Log( DEBUG_LOG_PREFIX + "Destination directory not found, creating it now." );
		}
		
		FileInfo[] files = dir.GetFiles();
		foreach( FileInfo file in files )
		{
			SafeMoveFile( CombinePaths( sourceDirName, file.Name ), CombinePaths( destDirName, file.Name ) );
		}
		
		DirectoryInfo[] dirs = dir.GetDirectories();
		foreach( DirectoryInfo subdir in dirs )
		{
			SafeMoveDirectory( CombinePaths( sourceDirName, subdir.Name ), CombinePaths( destDirName, subdir.Name ) );
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private delegate void DFileRenamer( ref string fileName );
	
	//------------------------------------------------------------------------------------------------------------------
	private static void RenameHiddenJavascript( ref string fileName )
	{
		if ( fileName.EndsWith( ".js.txt", StringComparison.OrdinalIgnoreCase ) )
		{
			fileName = fileName.Substring( 0, fileName.Length - 4 );
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private static void DirectoryCopy( string sourceDirName, string destDirName, bool copySubDirs = true, DFileRenamer fileRenamer = null )
    {
		Debug.Log( DEBUG_LOG_PREFIX + "Copying contents of: " + sourceDirName + 
			" into: " + destDirName + ", CopyRecursive: " + copySubDirs.ToString() );

        // Get the subdirectories for the specified directory.
        DirectoryInfo dir = new DirectoryInfo( sourceDirName );
 
        if( !dir.Exists )
        {
            Debug.Log( DEBUG_LOG_PREFIX + "Source directory not found, no files to copy." );
			return;
        }

        // If the destination directory doesn't exist, create it. 
        if( !Directory.Exists( destDirName ) )
        {
            Directory.CreateDirectory( destDirName );
			Debug.Log( DEBUG_LOG_PREFIX + "Destination directory not found, creating it now." );
        }

        // Get the files in the directory and copy them to the new location. this will also overwrite. 
        FileInfo[] files = dir.GetFiles();
        foreach( FileInfo file in files )
        {
			string	fileName = file.Name;
			
			// ignore meta files. 
			if( fileName.EndsWith( "meta" ) ) continue;

			if ( fileRenamer != null )
			{
				fileRenamer( ref fileName );
			}
			
			string temppath = Path.Combine( destDirName, fileName );
            file.CopyTo( temppath, true );
        }

        // If copying subdirectories, copy them and their contents to new location. 
        if( copySubDirs )
        {
			DirectoryInfo[] dirs = dir.GetDirectories();
			foreach( DirectoryInfo subdir in dirs )
            {
                string temppath = Path.Combine( destDirName, subdir.Name );
				DirectoryCopy( subdir.FullName, temppath, copySubDirs, fileRenamer );
            }
        }
    }

	//------------------------------------------------------------------------------------------------------------------
	private static void ZipDirectory( string zipPath, string dirPath, params string[] files )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Zipping directory: " + dirPath + " --> " + zipPath );

		if ( string.IsNullOrEmpty( _jarPath ) )
		{
			ForceFailBuild( "Must have jar executable to zip directory" );
		}

		List<string>	args = new List<string>();

		args.Add( "cMf \"" + zipPath + "\"" );

		if ( files.Length > 0 )
		{
			foreach ( string file in files )
			{
				args.Add( "\"" + file + "\"" );
			}
		}
		else
		{
			args.Add( "." );
		}

		Execute( _jarPath, args.ToArray(), workingDir:dirPath );
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void UnzipArchive( string zipPath )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Unzipping archive: " + zipPath );

		if ( string.IsNullOrEmpty( _jarPath ) )
		{
			ForceFailBuild( "Must have jar executable to unzip archive" );
		}

		string	zipDir = Path.GetDirectoryName( zipPath );
		string	zipFile = Path.GetFileName( zipPath );

		Execute( _jarPath, new string[] { "xf \"" + zipFile + "\"" }, workingDir:zipDir );
	}

	//------------------------------------------------------------------------------------------------------------------
	// Add '\\?\' to absolute Windows paths to get around the 260 char MAX_PATH limitation.  See:
	//   https://msdn.microsoft.com/en-us/library/aa365247.aspx#maxpath
	private static string SafePath( string path )
	{
#if USE_DOT_NET_4_6
		return path;
#else
		if ( path == null ) return null;
		
		Regex	re = new Regex( "[a-zA-Z]:\\\\" );

		if ( !re.IsMatch( path ) ) return path;

		return "\\\\?\\" + path;
#endif
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void CompressDirectory(string sInDir, string sOutFile)
	{
		string[] sFiles = Directory.GetFiles(sInDir, "*.*", SearchOption.AllDirectories);
		int iDirLen = sInDir[sInDir.Length - 1] == Path.DirectorySeparatorChar ? sInDir.Length : sInDir.Length + 1;

		using(FileStream outFile = new FileStream(sOutFile, FileMode.Create, FileAccess.Write, FileShare.None))
		using(GZipStream str = new GZipStream(outFile, CompressionMode.Compress))
			foreach(string sFilePath in sFiles)
			{
				string sRelativePath = sFilePath.Substring(iDirLen);
				Debug.Log("Compressing: " + sRelativePath);
				CompressFile(sInDir, sRelativePath, str);
			}
	}

	//------------------------------------------------------------------------------------------------------------------
	private static void CompressFile(string sDir, string sRelativePath, GZipStream zipStream)
	{
		//Compress file name
		char[] chars = sRelativePath.ToCharArray();
		zipStream.Write(BitConverter.GetBytes(chars.Length), 0, sizeof(int));
		foreach(char c in chars)
			zipStream.Write(BitConverter.GetBytes(c), 0, sizeof(char));

		//Compress file content
		byte[] bytes = File.ReadAllBytes(Path.Combine(sDir, sRelativePath));
		zipStream.Write(BitConverter.GetBytes(bytes.Length), 0, sizeof(int));
		zipStream.Write(bytes, 0, bytes.Length);
	}

	//------------------------------------------------------------------------------------------------------------------
	public static void Compress(DirectoryInfo directorySelected)
	{
		foreach(FileInfo fileToCompress in directorySelected.GetFiles())
		{
			using(FileStream originalFileStream = fileToCompress.OpenRead())
			{
				if((File.GetAttributes(fileToCompress.FullName) &
					 FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
				{
					using(FileStream compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
					{
						using(GZipStream compressionStream = new GZipStream(compressedFileStream,
							 CompressionMode.Compress))
						{
							originalFileStream.CopyTo(compressionStream);
						}
					}
				}
			}
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public static void Execute( string program, string[] arguments, string workingDir = null, string username = null, string password = null, System.Action<string> stdoutHandler = null, System.Action<string> stderrHandler = null )
	{
		string	argumentsStr = string.Join( " ", arguments );

		UnityEngine.Debug.Log("Executing: " + program + " " + argumentsStr);
		System.Diagnostics.Process proc = new System.Diagnostics.Process();

		proc.StartInfo.FileName = program;
		proc.StartInfo.Arguments = argumentsStr;
		proc.StartInfo.UseShellExecute = false;
		proc.StartInfo.RedirectStandardOutput = true;
		proc.StartInfo.RedirectStandardError = true;
		proc.StartInfo.CreateNoWindow = true;
		if ( !string.IsNullOrEmpty( workingDir ) )
		{
			proc.StartInfo.WorkingDirectory = workingDir;
		}

		if ( !string.IsNullOrEmpty( username ) )
		{
			proc.StartInfo.UserName = username;

			if ( !string.IsNullOrEmpty( password ) )
			{
				System.Security.SecureString	securePassword = new System.Security.SecureString();
				foreach ( char c in password )
				{
					securePassword.AppendChar(c);
				}
				securePassword.MakeReadOnly();
				proc.StartInfo.Password = securePassword;
			}
		}

		proc.Start();
		
		System.Text.StringBuilder	stdoutText = new System.Text.StringBuilder("");
		System.Text.StringBuilder	stderrText = new System.Text.StringBuilder("");

		string	line;

		while(proc.StandardOutput.EndOfStream == false)
		{
			line = proc.StandardOutput.ReadLine();

			stdoutText.AppendLine( line );

			if ( stdoutHandler != null )
			{
				stdoutHandler( line );
			}
		}

		while(proc.StandardError.EndOfStream == false)
		{
			line = proc.StandardError.ReadLine();

			stderrText.AppendLine( line );

			if ( stderrHandler != null )
			{
				stderrHandler( line );
			}
		}

		proc.WaitForExit();

		if(proc.ExitCode != 0)
		{
			UnityEngine.Debug.Log( "Process [" + program + "] failed with exit code [" + proc.ExitCode + "]");
			UnityEngine.Debug.Log( "Process stdout:\n" + stdoutText.ToString() );
			UnityEngine.Debug.Log( "Process stderr:\n" + stderrText.ToString() );
			throw new System.Exception("Process [" + program + "] failed with exit code [" + proc.ExitCode + "]");
		}
	}

}

//----------------------------------------------------------------------------------------------------------------------
public static class StreamExtensions
{
	public static void CopyTo(this Stream input, Stream output)
	{
		byte[] buffer = new byte[16 * 1024];
		int bytesRead;
		while((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
		{
			output.Write(buffer, 0, bytesRead);
		}
	}
}
