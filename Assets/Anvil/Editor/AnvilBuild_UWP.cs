﻿////////////////////////////////////////////
// 
// AnvilBuild_WSA.cs
//
// Created: 25/10/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using UnityEngine;
using UnityEditor;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public static partial class AnvilBuild
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG_WSA = "[AnvilBuild_WSA] ";

	private const string	DIR_PACKAGE = "AppPackages";

	private const string	UWP_VSPROJ_ADDITIONS_DIR = "Assets/Plugins/OURTEC/UWP/VsProjectAdditions";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//

	private class UwpPackageNameParts
	{
		public string	_pPrefix { get; private set; }
		public string	_pVersion { get; private set; }
		public string	_pPlatform { get; private set; }
		public string	_pConfiguration { get; private set; }
		public bool		_pHasConfiguration { get { return !string.IsNullOrEmpty( _pConfiguration ); } }
		public UwpPackageNameParts( string dirName, bool isExpectConfigPart )
		{
			int	expectedCount = isExpectConfigPart ? 5 : 4;
			string[]	parts = dirName.Split('_');
			if ( parts.Length < expectedCount ) throw new Exception( "Unexpected directory found: " + dirName );
			int	partIdx = parts.Length - 2;
			if ( isExpectConfigPart )
			{
				_pConfiguration = parts[ partIdx-- ];
			}
			_pPlatform = parts[ partIdx-- ];
			_pVersion = parts[ partIdx-- ];
			_pPrefix = string.Join( "_", parts, 0, partIdx + 1 );
		}
	}
			
	//
	// MEMBERS 
	//

	private static string	_msbuildPath;

	private static string	_msbuildVersionString;
	
	private static int[]	_msbuildVersionParts;

	private static string	_makeAppxPath;

	private static string	_signToolPath;

	private static string	_uwpBuildConfiguration = "Release";

	private static string[]	_uwpBuildPlatforms = new string[] { "x86", "x64", "ARM" };

	private static string	_uwpCertificatePath;

	private static string	_uwpCertificatePassword;

	private static string	_uwpCertificateThumbprint;

	private static bool		_uwpIsCreateAppxBundle = false; 

	private static bool		_uwpIsStoreUpload = false; 

	private static string	_uwpVersionString;

	//
	// PROPERTIES
	//

	private static string _pPackagesDir
	{
		get
		{
			return CombinePaths( _pOutputPath, DIR_PACKAGE, PlayerSettings.productName );
		}
	}
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static void InitUWP()
	{
		_msbuildPath = GetMSBuildPath();
		
		if ( !string.IsNullOrEmpty( _msbuildPath ) )
		{
			UnityEngine.Debug.Log( LOG_TAG_WSA + "MSBuild path: " + _msbuildPath );
		}
		else
		{
			ForceFailBuild( "No MSBuild path!" );
		}

		//Figure out MSBuild version
		_msbuildVersionString = null;
		_msbuildVersionParts = null;
		Regex	versionPattern = new Regex( "^[0-9]+(\\.[0-9]+)*$" );
		Execute( _msbuildPath, new string[] { "/Version" }, stdoutHandler:( string line ) => {
			if ( versionPattern.IsMatch( line ) )
			{
				_msbuildVersionString = line;
				Debug.Log( LOG_TAG_WSA + "MSBuild version: " + _msbuildVersionString );
				string[]	parts = line.Split('.');
				_msbuildVersionParts = new int[ parts.Length ];
				for ( int i = 0; i < parts.Length; ++i )
				{
					_msbuildVersionParts[i] = int.Parse( parts[i] );
				}
			}
		} );

		if ( _msbuildVersionParts == null || _msbuildVersionParts.Length == 0 )
		{
			ForceFailBuild( "Failed to determine MSBuild version!" );
		}

		EditorUserBuildSettings.wsaUWPBuildType = WSAUWPBuildType.XAML;

		string	windowsSdkToolsPath = GetWindowsSdkToolsPath();

		if ( !string.IsNullOrEmpty( windowsSdkToolsPath ) )
		{
			UnityEngine.Debug.Log( LOG_TAG_WSA + "Windows SDK Tools path: " + windowsSdkToolsPath );
		}
		else
		{
			ForceFailBuild( "No Windows SDK Tools path!" );
		}

		_makeAppxPath = CombinePaths( windowsSdkToolsPath, "MakeAppx.exe" );
		_signToolPath = CombinePaths( windowsSdkToolsPath, "SignTool.exe" );

		if ( _pBuildConfig._pUwpOptions._pIsStoreUpload.HasValue )
		{
			_uwpIsStoreUpload = _pBuildConfig._pUwpOptions._pIsStoreUpload.Value;
		}
		else
		{
			_uwpIsStoreUpload = _isSubmission;
		}

		if ( !string.IsNullOrEmpty( _pBuildConfig._pUwpOptions._pBuildConfiguration ) )
		{
			_uwpBuildConfiguration = _pBuildConfig._pUwpOptions._pBuildConfiguration;
		}
		else
		{
			//CC20171004 - primarily due to bug in "Release" config in Unity 5.5.2, but also probably best to have Internal match Submission in this way, and we're not losing much
			//_uwpBuildConfiguration = _uwpIsStoreUpload ? "Master" : _isDevelopment ? "Debug" : "Release";
			_uwpBuildConfiguration = _isAllowDebugging ? "Debug" : _isDevelopment ? "Release" : "Master";
		}

		if ( !string.IsNullOrEmpty( _pBuildConfig._pUwpOptions._pBuildPlatforms ) )
		{
			_uwpBuildPlatforms = _pBuildConfig._pUwpOptions._pBuildPlatforms.Split( '|', ',', ';' );
		}
		else
		{
			_uwpBuildPlatforms = new string[] { "x86", "x64", "ARM" };
		}

		string[] certSpec;

		if ( !string.IsNullOrEmpty( _pBuildConfig._pUwpOptions._pCertificate ) )
		{
			certSpec = _pBuildConfig._pUwpOptions._pCertificate.Split( '|' );
		}
		else
		{
			certSpec = AnvilBuildInfo.GetVar( "CERTIFICATE", "" ).Split( '|' );
		}

		if ( certSpec.Length >= 1 && !string.IsNullOrEmpty( certSpec[0] ) )
		{
			_uwpCertificatePath = CombinePaths( _projectPath, _dirFilesUwp, certSpec[0] );
			if ( !_uwpCertificatePath.ToLower().EndsWith( ".pfx" ) )
			{
				_uwpCertificatePath += ".pfx";
			}
		}

		if ( certSpec.Length >= 2 && !string.IsNullOrEmpty( certSpec[1] ) )
		{
			_uwpCertificatePassword = certSpec[1];
		}

		PlayerSettings.WSA.SetCertificate( _uwpCertificatePath, _uwpCertificatePassword );

		if ( !string.IsNullOrEmpty( _uwpCertificatePath ) && !string.IsNullOrEmpty( _uwpCertificatePassword ) )
		{
			System.Security.Cryptography.X509Certificates.X509Certificate2	cert = new System.Security.Cryptography.X509Certificates.X509Certificate2( _uwpCertificatePath, _uwpCertificatePassword );

			_uwpCertificateThumbprint = cert.Thumbprint;
		}

		if ( _pBuildConfig._pUwpOptions._pIsCreateAppxBundle.HasValue )
		{
			_uwpIsCreateAppxBundle = _pBuildConfig._pUwpOptions._pIsCreateAppxBundle.Value;
		}
		else
		{
			//bool.TryParse( AnvilBuildInfo.GetVar( "COMPRESSED", "false" ), out _uwpIsCreateAppxBundle );
			_uwpIsCreateAppxBundle = true;//need to bundle by default because 4ORD needs one file to download
		}

		if ( string.IsNullOrEmpty( _jarPath ) )
		{
			ForceFailBuild( "jar exe is required to zip test packages files" );
		}

		//unity names folder and vsproj with <PlayerSettings.productName>, so best to use product code instead.
		PlayerSettings.productName = _productCode;

		string	publisherName = AnvilBuildInfo.GetVar( "PUBLISHER_NAME", null );

		if ( !string.IsNullOrEmpty( publisherName ) )
		{
			PlayerSettings.companyName = publisherName;
		}

		if ( !string.IsNullOrEmpty( _bundleIdentifier ) )
		{
			PlayerSettings.WSA.packageName = _bundleIdentifier;
		}
		else
		{
			PlayerSettings.WSA.packageName = _productCode;
		}

		_uwpVersionString = AnvilBuildInfo.GetVar( "BUNDLE_VERSION", "1.0.0" );
		//Debug.Log( LOG_TAG_WSA + "_uwpVersionString:" + _uwpVersionString );
		List<string>	versionParts = new List<string>( _uwpVersionString.Split( '.' ) );
		while ( versionParts.Count < 4 )
		{
			versionParts.Add( "0" );
		}
		while ( versionParts.Count > 4 )
		{
			versionParts.RemoveAt( versionParts.Count - 1 );
		}
		_uwpVersionString = String.Join( ".", versionParts.ToArray() );
		//Debug.Log( LOG_TAG_WSA + "_uwpVersionString:" + _uwpVersionString );
		Version	version = new Version( _uwpVersionString );
		//Debug.Log( LOG_TAG_WSA + "version:" + version );
		PlayerSettings.WSA.packageVersion = version;
		Debug.Log( LOG_TAG_WSA + "packageVersion:" + PlayerSettings.WSA.packageVersion );
		PlayerSettings.WSA.applicationDescription = _productName;
		PlayerSettings.WSA.tileShortName = _productName;

		string[]	caps = AnvilBuildInfo._pDeviceCapabilities.Split('|');
		EnumUtils.FromStringMap<PlayerSettings.WSACapability>	nameToEnumMap = new EnumUtils.FromStringMap<PlayerSettings.WSACapability>( isCaseSensitive:false );

		foreach ( string capName in caps )
		{
			PlayerSettings.WSACapability	capEnum;
			
			if ( !nameToEnumMap.TryLookup( capName, out capEnum ) )
			{
				LoggerAPI.LogWarning( LOG_TAG_WSA + "Unexpected capability in build info: " + capName );
				continue;
			}

			PlayerSettings.WSA.SetCapability( capEnum, true );
		}
	}

	private static void DoVisualStudioPreBuild()
	{
		DoVsProjectMods();
		DoUwpManifestMods();
	}

	private static void DoVisualStudioBuild()
	{
		if ( _uwpIsCreateAppxBundle )
		{
			MSBuildAndPackageAndBundle();
		}
		else
		{
			foreach ( string p in _uwpBuildPlatforms )
			{
				MSBuildAndPackage( p );
			}
		}

		CopyPackages();
	}
	
	private static string GetMSBuildPath()
	{
		return GetBuildOrAgentVersionedPath( 
			getBuildPath:() => _pBuildConfig._pUwpOptions._pMSBuildPath,
			getBuildVersion:() => _pBuildConfig._pUwpOptions._pMSBuildVersion, 
			getAgentPath:( string wantVersion, out string gotVersion ) => _pAgentConfig.GetMSBuildPath( wantVersion, out gotVersion ), 
			isValidPath:( string path ) => !string.IsNullOrEmpty( path ),
			defaultPath:null );
	}
	
	private static string GetWindowsSdkToolsPath()
	{
		return GetBuildOrAgentVersionedPath( 
			getBuildPath:() => _pBuildConfig._pUwpOptions._pWindowsSdkToolsPath,
			getBuildVersion:() => _pBuildConfig._pUwpOptions._pWindowsSdkVersion, 
			getAgentPath:( string wantVersion, out string gotVersion ) => _pAgentConfig.GetWindowsSdkToolsPath( wantVersion, out gotVersion ), 
			isValidPath:( string path ) => !string.IsNullOrEmpty( path ),
			defaultPath:null );
	}

	private static void DoVsProjectMods()
	{
		string	additionsDir = CombinePaths( _pProjectPath, UWP_VSPROJ_ADDITIONS_DIR );
		string	projectDir = CombinePaths( _pOutputPath, PlayerSettings.productName );

		DirectoryCopy( additionsDir, projectDir );

		string	projectPath = CombinePaths( projectDir, PlayerSettings.productName + ".vcxproj" );

		XmlDocument	projectXml = new XmlDocument();

		projectXml.Load( projectPath );
		projectXml.Save( projectPath + ".orig" );

		XmlElement projectElement = projectXml.DocumentElement as XmlElement;

		XmlUtils.AddXmlChild( projectElement, "ItemGroup", itemGroupElem => {
			//itemGroupElem.SetAttribute( "Label", "AmuzoItems" );
			FileHelper.ForEachFileInDirectory( additionsDir, action:fi => {
				string	itemType;
				switch ( fi.Extension.ToLower() )
				{
				case ".cpp":
					itemType = "ClCompile";
					break;
				case ".h":
					itemType = "ClInclude";
					break;
				default:
					itemType = "None";
					break;
				}
				XmlUtils.AddXmlChild( itemGroupElem, itemType, compileElem => {
					compileElem.SetAttribute( "Include", fi.Name );
				} );
			} );
		} );

		projectXml.Save( projectPath );
	}

	private static void DoUwpManifestMods()
	{
		string	manifestPath = CombinePaths( _pOutputPath, PlayerSettings.productName, "Package.appxmanifest" );

		XmlDocument	manifestXml = new XmlDocument();

		manifestXml.Load( manifestPath );
		//manifestXml.Save( manifestPath + ".orig" );

		XmlElement packageElement = manifestXml.DocumentElement as XmlElement;

		XmlUtils.SetXmlChild( packageElement, "Properties", propertiesElement => {
			XmlUtils.SetXmlChild( propertiesElement, "DisplayName", displayNameElement => {
				displayNameElement.InnerText = _productName;
			} );
		} );

		XmlUtils.SetXmlChild( packageElement, "Applications", applicationsElement => {
			XmlUtils.SetXmlChild( applicationsElement, findElem => findElem.Name == "Application" && findElem.HasAttribute( "Id" ) && findElem.GetAttribute( "Id" ) == "App", null, applicationElement => {
				XmlUtils.SetXmlChild( applicationElement, "uap:VisualElements", visualElementsElement => {
					visualElementsElement.SetAttribute( "DisplayName", _productName );
				} );
			} );
		} );

		manifestXml.Save( manifestPath );
	}

	private static void MSBuildAndPackageAndBundle()
	{
		//CC20170224 - this way doesn't work, it seems to place builds in the wrong folders, and then fail later on when it can't find them.  Use manual command line method instead.
		//List<string>	buildArgs = new List<string>();

		//AddCommonBuildArgs( buildArgs );

		//buildArgs.Add( "/p:AppxBundle=Always" );
		//buildArgs.Add( "/p:AppxBundlePlatforms=\"" + string.Join( "|", _uwpBuildPlatforms ) + "\"" );

		//Execute( _msbuildPath, buildArgs.ToArray(), _pOutputPath );

		//CC20170224 - manual command line method:

		//Build each package individually
		foreach ( string p in _uwpBuildPlatforms )
		{
			MSBuildAndPackage( p );
		}

		string	packagesDir = _pPackagesDir;

		//Delete existing appxupload packages as these will be rebuilt from _Test dirs
		FileHelper.ForEachFileInDirectory( packagesDir, match:fi => fi.Extension.Equals( ".appxupload", StringComparison.OrdinalIgnoreCase ), action:fi => SafeDeleteFile( fi.FullName ) );

		List<string>	packagePlatforms = new List<string>();

		UwpPackageNameParts	packageNameParts = null;

		//Find package name and platforms from existing _Test dirs
		FileHelper.ForEachSubDirectory( packagesDir, action:di => {
			//not sure if it was ever actually the case that non-store-upload build did not have the 
			//configuration in the file name, but it's seems like it's not the case now - CC20171220
			UwpPackageNameParts	dirNameParts = new UwpPackageNameParts( di.Name, isExpectConfigPart:true/*_uwpIsStoreUpload*/ );
			if ( packageNameParts == null )
			{
				packageNameParts = dirNameParts;
			}
			packagePlatforms.Add( dirNameParts._pPlatform );
		} );

		string	bundlePackageFolderName = packageNameParts._pHasConfiguration ? 
											packageNameParts._pPrefix + "_" + packageNameParts._pVersion + "_" + packageNameParts._pConfiguration + "_Test" :
											packageNameParts._pPrefix + "_" + packageNameParts._pVersion + "_Test";

		string	bundlePackageFolderPath = CombinePaths( packagesDir, bundlePackageFolderName );

		string	bundlePackageName = packageNameParts._pHasConfiguration ? 
										packageNameParts._pPrefix + "_" + packageNameParts._pVersion + "_" + string.Join( "_", packagePlatforms.ToArray() ) + "_" + packageNameParts._pConfiguration : 
										packageNameParts._pPrefix + "_" + packageNameParts._pVersion + "_" + string.Join( "_", packagePlatforms.ToArray() );

		bool	hasDoneFullCopy = false;

		//Copy and rename required files from existing _Test dirs to new _Test dir
		FileHelper.ForEachSubDirectory( packagesDir, action:di => {
			if ( !hasDoneFullCopy )
			{
				SafeMoveDirectoryContents( di.FullName, bundlePackageFolderPath );
				FileHelper.ForEachFileInDirectory( bundlePackageFolderPath, match:fi => fi.Extension.Equals( ".cer", StringComparison.OrdinalIgnoreCase ), action:fi => {
					SafeMoveFile( fi.FullName, CombinePaths( bundlePackageFolderPath, bundlePackageName + ".cer" ) );
				} );
				hasDoneFullCopy = true;
			}
			else
			{
				FileHelper.ForEachFileInDirectory( di, match:fi => fi.Extension.Equals( ".appx", StringComparison.OrdinalIgnoreCase ) || fi.Extension.Equals( ".appxsym", StringComparison.OrdinalIgnoreCase ), action:fi => {
					SafeMoveFile( fi.FullName, CombinePaths( bundlePackageFolderPath, fi.Name ) );
				} );
			}
			//Delete original _Test dir
			SafeDeleteDirectory( di.FullName );
		} );

		//Move appx files into temporary sub-dir to be bundled
		FileHelper.ForEachFileInDirectory( bundlePackageFolderPath, match:fi => fi.Extension.Equals( ".appx", StringComparison.OrdinalIgnoreCase ), action:fi => {
			SafeMoveFile( fi.FullName, CombinePaths( bundlePackageFolderPath, "ToBundle", fi.Name ) );
		} );

		//Bundle the temporary sub-dir
		Execute( _makeAppxPath, new string[] {
			"bundle", 
			"/d ToBundle",
			"/p " + bundlePackageName + ".appxbundle"
		}, workingDir:bundlePackageFolderPath );

		//Delete the temporary sub-dir
		SafeDeleteDirectory( CombinePaths( bundlePackageFolderPath, "ToBundle" ) );

		//Find the bundle file
		string	bundlePackageFileName = null;
		FileHelper.ForEachFileInDirectory( bundlePackageFolderPath, match:fi => fi.Extension.Equals( ".appxbundle", StringComparison.OrdinalIgnoreCase ), action:fi => {
			if ( bundlePackageFileName != null )
			{
				ForceFailBuild( LOG_TAG_WSA + "Expected to find one bundle only" );
			}
			bundlePackageFileName = fi.Name;
		} );

		//Sign the bundle
		SignWindowsPackage( CombinePaths( bundlePackageFolderPath, bundlePackageFileName ), _uwpCertificatePath, _uwpCertificatePassword );

		//If it's a store build, create the appxupload file
		if ( _uwpIsStoreUpload )
		{
			List<string>	filesToZip = new List<string>();

			filesToZip.Add( bundlePackageFileName );

			FileHelper.ForEachFileInDirectory( bundlePackageFolderPath, match:fi => fi.Extension.Equals( ".appxsym", StringComparison.OrdinalIgnoreCase ), action:fi => {
				filesToZip.Add( fi.Name );
			} );

			if ( filesToZip.Count == 0 )
			{
				ForceFailBuild( "No files to zip" );
			}

			ZipDirectory( CombinePaths( packagesDir, bundlePackageName + ".appxupload" ), bundlePackageFolderPath, filesToZip.ToArray() );
		}
	}

	private static void SignWindowsPackage( string packageFilePath, string certPath, string certPass )
	{
		List<string>	signToolArgs = new List<string>();

		signToolArgs.Add( "sign");
		signToolArgs.Add( "/fd SHA256");
		signToolArgs.Add( "/a");
		signToolArgs.Add( "/f \"" + certPath + "\"");

		if ( !string.IsNullOrEmpty( certPass ) )
		{
			signToolArgs.Add( "/p \"" + certPass + "\"");
		}

		signToolArgs.Add( "\"" + packageFilePath + "\"" );

		//Sign the bundle
		Execute( _signToolPath, signToolArgs.ToArray() );
	}

	private static void MSBuildAndPackage( string platform )
	{
		List<string>	buildArgs = new List<string>();

		AddCommonBuildArgs( buildArgs );

		buildArgs.Add( "/p:Platform=" + platform );
		buildArgs.Add( "/p:AppxBundle=Never" );

		if ( _pBuildConfig._pUwpOptions._pIsSaveMSBuildLog.HasValue && _pBuildConfig._pUwpOptions._pIsSaveMSBuildLog.Value == true )
		{
			buildArgs.Add( "/fl1" );
			buildArgs.Add( "/flp1:logfile=log_" + platform + ".txt" );
		}

		Execute( _msbuildPath, buildArgs.ToArray(), _pOutputPath );
	}

	private static void AddCommonBuildArgs( List<string> buildArgs )
	{
		buildArgs.Add( "\"" + PlayerSettings.productName + ".sln\"" );
		//buildArgs.Add( "/p:KeyFile=\"" + _uwpCertificatePath + "\";Password=" + _uwpCertificatePassword );
		buildArgs.Add( "/p:Configuration=" + _uwpBuildConfiguration );

		if ( !string.IsNullOrEmpty( _uwpCertificateThumbprint ) )
		{
			buildArgs.Add( "/p:PackageCertificateThumbprint=" + _uwpCertificateThumbprint );
		}
		if ( _uwpIsStoreUpload )
		{
			buildArgs.Add( "/p:UapAppxPackageBuildMode=StoreUpload" );
			//buildArgs.Add( "/p:AppxPackageIsForStore=true" );//old (see https://blogs.msdn.microsoft.com/wsdevsol/2016/01/08/windows-store-app-projects-stopped-generating-the-appxupload-file-after-installing-vs-2015-update-1/)
			//buildArgs.Add( "/p:UseDotNetNativeToolChain=true" );
		}
		//else
		//{
		//	buildArgs.Add( "/p:UapAppxPackageBuildMode=CI" );
		//	buildArgs.Add( "/p:BuildAppxUploadPackageForUap=false" );//old (see https://blogs.msdn.microsoft.com/wsdevsol/2016/01/08/windows-store-app-projects-stopped-generating-the-appxupload-file-after-installing-vs-2015-update-1/)
		//}

		if ( _pBuildConfig._pUwpOptions._pDefaultAssetScale.HasValue )
		{
			buildArgs.Add( "/p:UapDefaultAssetScale=" + _pBuildConfig._pUwpOptions._pDefaultAssetScale.Value.ToString() );
		}
	}

	private static void CopyPackages()
	{
		string	packagesDir = _pPackagesDir;

		int	appxUploadCount = 0;
		FileHelper.ForEachFileInDirectory( packagesDir, match:fi => fi.Extension.Equals( ".appxupload", StringComparison.OrdinalIgnoreCase ), action:fi => ++appxUploadCount );

		FileHelper.ForEachFileInDirectory( packagesDir, match:fi => fi.Extension.Equals( ".appxupload", StringComparison.OrdinalIgnoreCase ), action:fi => {
			string	destFile;
			if ( appxUploadCount == 1 )
			{
				destFile = CombinePaths( _pExportPath, _pOutputName + ".appxupload" );
			}
			else
			{
				string		name = fi.Name;//includes .appxupload extension
				string[]	parts = name.Split('_');
				string		suffix = string.Join( "_", parts, 2, parts.Length - 2 );
				destFile = CombinePaths( _pExportPath, _pOutputName + "_" + suffix );
			}
			File.Copy( fi.FullName, destFile );
		} );

		if ( appxUploadCount == 0 )
		{
			int	subDirCount = 0;
			FileHelper.ForEachSubDirectory( packagesDir, action:di => ++subDirCount );

			FileHelper.ForEachSubDirectory( packagesDir, action:di => {
				string	destFile;
				if ( subDirCount == 1 )
				{
					destFile = CombinePaths( _pExportPath, _pOutputName + ".zip" );
				}
				else
				{
					string[]	parts = di.Name.Split('_');
					string		suffix = string.Join( "_", parts, 2, parts.Length - 2 );
					destFile = CombinePaths( _pExportPath, _pOutputName + "_" + suffix + ".zip" );
				}
				ZipDirectory( destFile, di.FullName );
			} );
		}
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

