﻿////////////////////////////////////////////
// 
// AnvilProjectConfig.cs
//
// Created: 20/06/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !UNITY_2018_3_OR_NEWER
#define SUPPORT_ANDROID_INTERNAL_BUILD_SYSTEM
#endif

using UnityEngine;
using UnityEditor;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

[CreateAssetMenu( fileName = "AnvilProjectConfig", order = 1000 )]
public class AnvilProjectConfig : ScriptableObject
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[AnvilProjectConfig] ";

	private const string	PROJECT_CODE_DEFAULT = "";

	private const bool		DO_ANDROID_MANIFEST_TEMPLATE_MODS_DEFAULT = true;

	private const bool		DO_SKIP_UNITY_ANDROID_PERMISSIONS_DIALOG_DEFAULT = false;

	#if SUPPORT_ANDROID_INTERNAL_BUILD_SYSTEM
	private const bool		USE_GRADLE_ANDROID_BUILD_SYSTEM_DEFAULT = false;
	#endif

	private const bool		IS_ENABLE_IOS_BITCODE_DEFAULT = false;

	private const bool		IS_ENABLE_IOS_SWIFT_DEFAULT = false;

	private static readonly IosTvosConfig	IOS_CONFIG_DEFAULT = new IosTvosConfig( isEnableBitcode:IS_ENABLE_IOS_BITCODE_DEFAULT, isEnableSwift:IS_ENABLE_IOS_SWIFT_DEFAULT );

	private const bool		IS_ENABLE_TVOS_BITCODE_DEFAULT = true;

	private const bool		IS_ENABLE_TVOS_SWIFT_DEFAULT = false;

	private static readonly IosTvosConfig	TVOS_CONFIG_DEFAULT = new IosTvosConfig( isEnableBitcode:IS_ENABLE_TVOS_BITCODE_DEFAULT, isEnableSwift:IS_ENABLE_TVOS_SWIFT_DEFAULT );

	//
	// SINGLETON DECLARATION
	//

	private static AnvilProjectConfig	_instance;

	//
	// NESTED CLASSES / STRUCTS
	//

	[System.Serializable]
	public struct IosTvosConfig
	{
		[SerializeField]
		private bool		_isEnableBitcode;

		[SerializeField]
		private bool		_isEnableSwift;

		[SerializeField]
		private string	_swiftVersion;

		[SerializeField]
		private bool	_isRemovePushNotificationCapability;

		[SerializeField]
		private bool	_isForceLegacyBuildSystem;

		public bool _pIsEnableBitcode
		{
			get
			{
				return _isEnableBitcode;
			}
		}

		public bool _pIsEnableSwift
		{
			get
			{
				return _isEnableSwift;
			}
		}

		public string _pSwiftVersion
		{
			get
			{
				return _swiftVersion;
			}
		}

		public bool _pIsRemovePushNotificationCapability
		{
			get
			{
				return _isRemovePushNotificationCapability;
			}
		}

		public bool _pIsForceLegacyBuildSystem
		{
			get
			{
				return _isForceLegacyBuildSystem;
			}
		}

		public IosTvosConfig( bool isEnableBitcode, bool isEnableSwift, string swiftVersion = null )
		{
			_isEnableBitcode = isEnableBitcode;
			_isEnableSwift = isEnableSwift;
			_swiftVersion = swiftVersion;
			_isRemovePushNotificationCapability = false;
			_isForceLegacyBuildSystem = false;
		}
	}
			
	//
	// MEMBERS 
	//

	[SerializeField]
	private string	_projectCode = PROJECT_CODE_DEFAULT;

	[SerializeField]
	private bool	_doAndroidManifestTemplateMods = DO_ANDROID_MANIFEST_TEMPLATE_MODS_DEFAULT;

	[SerializeField]
	private bool	_doSkipUnityAndroidPermissionsDialog = DO_SKIP_UNITY_ANDROID_PERMISSIONS_DIALOG_DEFAULT;

	#if SUPPORT_ANDROID_INTERNAL_BUILD_SYSTEM
	[SerializeField]
	private bool	_useGradleAndroidBuildSystem;
	#endif

	[SerializeField]
	private AnvilBuild.AndroidGradleSettings	_gradleSettings;
	
	[SerializeField]
	private IosTvosConfig	_iosConfig = IOS_CONFIG_DEFAULT;

	[SerializeField]
	private IosTvosConfig	_tvosConfig = TVOS_CONFIG_DEFAULT;

	[SerializeField]
	private string	_webGLFallbackFontName;

	[SerializeField]
	private Font	_webGLFallbackFont;

	[SerializeField]
	private Font[]	_webGLFallbackFontTargets;
	
	//
	// PROPERTIES
	//

	public static string _pProjectCode
	{
		get
		{
			return _instance != null ? _instance._projectCode : PROJECT_CODE_DEFAULT;
		}
	}

	public static bool _pDoAndroidManifestTemplateMods
	{
		get
		{
			return _instance != null ? _instance._doAndroidManifestTemplateMods : DO_ANDROID_MANIFEST_TEMPLATE_MODS_DEFAULT;
		}
	}

	public static bool _pDoSkipUnityAndroidPermissionsDialog
	{
		get
		{
			return _instance != null ? _instance._doSkipUnityAndroidPermissionsDialog : DO_SKIP_UNITY_ANDROID_PERMISSIONS_DIALOG_DEFAULT;
		}
	}

	#if SUPPORT_ANDROID_INTERNAL_BUILD_SYSTEM
	public static bool _pUseGradleAndroidBuildSystem
	{
		get
		{
			return _instance != null ? _instance._useGradleAndroidBuildSystem : USE_GRADLE_ANDROID_BUILD_SYSTEM_DEFAULT;
		}
	}
	#endif

	public static AnvilBuild.AndroidGradleSettings _pGradleSettings
	{
		get
		{
			return _instance != null ? _instance._gradleSettings : null;
		}
	}
	
	public static IosTvosConfig _pIosConfig
	{
		get
		{
			return _instance != null ? _instance._iosConfig : IOS_CONFIG_DEFAULT;
		}
	}

	public static IosTvosConfig _pTvosConfig
	{
		get
		{
			return _instance != null ? _instance._tvosConfig : TVOS_CONFIG_DEFAULT;
		}
	}

	public static string _pWebGLFallbackFontName
	{
		get
		{
			return _instance != null ? _instance._webGLFallbackFontName : null;
		}
	}

	public static Font _pWebGLFallbackFont
	{
		get
		{
			return _instance != null ? _instance._webGLFallbackFont : null;
		}
	}
	
	public static Font[] _pWebGLFallbackFontTargets
	{
		get
		{
			return _instance != null ? _instance._webGLFallbackFontTargets : null;
		}
	}
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	// ** PUBLIC
	//

	public static bool Load( string configPath )
	{
		_instance = AssetDatabase.LoadAssetAtPath<AnvilProjectConfig>( configPath );
		return _instance != null;
	}

	public static IosTvosConfig GetIosTvosConfig( BuildTarget buildTarget )
	{
		switch ( buildTarget )
		{
		case BuildTarget.iOS:
			return _pIosConfig;
		case BuildTarget.tvOS:
			return _pTvosConfig;
		}
		throw new System.ArgumentException( "Invalid build target: " + buildTarget, "buildTarget" );
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

