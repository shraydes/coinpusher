﻿////////////////////////////////////////////
// 
// AnvilBuild_Plugins.cs
//
// Created: 15/02/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEditor;

public static partial class AnvilBuild
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	//
	// NESTED CLASSES / STRUCTS
	//

	public interface IBuildPlugin
	{
		string	_pPluginName	{ get; }

		bool	_pIsRequired	{ get; }

		void	OnPreBuild();

		bool	OnSetAndroidMainActivity( ref string mainActivityName );

		void	OnSetAndroidManifestEntries( XmlDocument manifest, XmlElement rootElem );

		void	OnModifyAndroidGradleSettings( AnvilBuild.AndroidGradleSettings gradleSettings );

		void	OnSetIosPlistEntries( XmlDocument plist, XmlElement rootDict );

		void	OnSetIosExportOptions( XmlDocument plist, XmlElement rootDict );
	}
			
	//
	// MEMBERS 
	//
	
	private static List<IBuildPlugin>	_buildPlugins;

	//
	// PROPERTIES
	//
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public static void RegisterBuildPlugin( IBuildPlugin plugin )
	{
#if !DISABLE_ANVIL_PLUGINS
		if ( plugin == null ) return;
		
		if ( _buildPlugins == null )
		{
			_buildPlugins = new List<IBuildPlugin>();
		}

		if ( !_buildPlugins.Contains( plugin ) )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Registering plugin: " + plugin._pPluginName );

			_buildPlugins.Add( plugin );
		}
#endif
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static void ForEachBuildPlugin( System.Action<IBuildPlugin> pluginAction, bool isRequiredPluginsOnly = false )
	{
		if ( pluginAction == null ) return;
		if ( _buildPlugins == null ) return;

		for ( int i = 0; i < _buildPlugins.Count; ++i )
		{
			if ( _buildPlugins[i] == null ) continue;
			if ( isRequiredPluginsOnly && !_buildPlugins[i]._pIsRequired ) continue;
			pluginAction( _buildPlugins[i] );
		}
	}
}

