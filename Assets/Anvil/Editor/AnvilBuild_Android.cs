﻿////////////////////////////////////////////
// 
// AnvilBuild_Android.cs
//
// Created: 25/05/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !UNITY_2018_3_OR_NEWER
#define SUPPORT_ANDROID_INTERNAL_BUILD_SYSTEM
#endif

using System.IO;
using System.Xml;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using AmuzoEngine;
using AnvilBuildCommon;
using LoggerAPI = UnityEngine.Debug;

public static partial class AnvilBuild
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG_ANDROID = "[AnvilBuild_Android] ";

	private const string	JAVA_EXE_WIN = "java.exe";

	private const string	JAVA_EXE_MAC = "java";

	private const string	APKTOOL_JAR = "apktool.jar";

	private const string	JARSIGNER_EXE_WIN = "jarsigner.exe";

	private const string	JARSIGNER_EXE_MAC = "jarsigner";

	private const string	ZIPALIGN_EXE_WIN = "zipalign.exe";

	private const string	ZIPALIGN_EXE_MAC = "zipalign";

	private const string	FILE_NAME_UNALIGNED_APK = "unaligned.apk";

	private enum ERebuildApkStage
	{
		NONE = 0,
		DECODE = 1,
		REBUILD = 2,
		CLEAN = 3,
		FINAL = CLEAN
	}

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	private static bool		_isUnsignedApk;

	private static List<string>	_androidBlockPerms = new List<string>();

	private static ERebuildApkStage		_rebuildApkEndStage;

	private static string	_apktoolPath;

	private static string	_zipalignPath;
	
	//
	// PROPERTIES
	//

	private static bool _pIsOutputGradleProject
	{
		get
		{
			return _pBuildConfig._pAndroidOptions._pIsOutputGradleProject.HasValue && _pBuildConfig._pAndroidOptions._pIsOutputGradleProject.Value == true;
		}
	}
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static void InitAndroid()
	{
		string[] cert;

		if ( _buildConfig != null && _buildConfig._pAndroidOptions != null && !string.IsNullOrEmpty( _buildConfig._pAndroidOptions._pKeystore ) )
		{
			cert = _buildConfig._pAndroidOptions._pKeystore.Split( '|' );
		}
		else
		{
			cert = AnvilBuildInfo.GetVar( "CERTIFICATE", "" ).Split( '|' );
		}

		if ( cert.Length == 1 && cert[0].ToLower() == "unsigned" )
		{
			PlayerSettings.Android.keyaliasName = "temporary";
			PlayerSettings.Android.keyaliasPass = "marvin";
			PlayerSettings.Android.keystoreName = CombinePaths( _projectPath, _dirFilesAndroid, "Default.keystore" );
			PlayerSettings.Android.keystorePass = "marvin";

			_isUnsignedApk = true;
		}
		else if( cert.Length < 3 )
		{
			Debug.LogWarning( DEBUG_LOG_PREFIX + "Certificate not added because data in wrong format" );
		}
		else
		{
			PlayerSettings.Android.keyaliasName = cert[1];
			PlayerSettings.Android.keyaliasPass = cert.Length > 3 ? cert[3] : cert[2];
			PlayerSettings.Android.keystoreName = _projectPath + "/" + _dirFilesAndroid + cert[0];
			PlayerSettings.Android.keystorePass = cert[2];
		}

		PlayerSettings.Android.keystoreName = PlayerSettings.Android.keystoreName.Replace( '\\', '/' );

		_androidBlockPerms.Clear();

		string[]	buildInfoBlockPerms = AnvilBuildInfo.GetVar( "BLOCK_PERMS", "" ).Split( ',', ';', ' ' );

		if ( buildInfoBlockPerms != null )
		{
			for ( int i = 0; i < buildInfoBlockPerms.Length; ++i )
			{
				if ( !string.IsNullOrEmpty( buildInfoBlockPerms[i] ) )
				{
					_androidBlockPerms.Add( buildInfoBlockPerms[i] );
				}
			}
		}

		for ( int i = 0; i < _buildConfig._pAndroidOptions._pBlockPermissions.Count; ++i )
		{
			if ( !string.IsNullOrEmpty( _buildConfig._pAndroidOptions._pBlockPermissions[i] ) )
			{
				_androidBlockPerms.Add( _buildConfig._pAndroidOptions._pBlockPermissions[i] );
			}
		}

		ValidateBlockPerms();


		_rebuildApkEndStage = ERebuildApkStage.NONE;
		if ( _buildConfig._pAndroidOptions._pRebuildApk.HasValue )
		{
			_rebuildApkEndStage = (ERebuildApkStage)( _buildConfig._pAndroidOptions._pRebuildApk.Value );
		}
		else if ( _isUnsignedApk || _androidBlockPerms.Count > 0 )
		{
			_rebuildApkEndStage = ERebuildApkStage.FINAL;
		}


		bool	isRequiresJava, isRequiresApktool, isRequiresJarsigner, isRequiresZipalign;

		isRequiresJava = isRequiresApktool = _rebuildApkEndStage >= ERebuildApkStage.DECODE;
		isRequiresJarsigner = isRequiresZipalign = _rebuildApkEndStage >= ERebuildApkStage.REBUILD && !_isUnsignedApk;

		if ( isRequiresJava )
		{
			if ( string.IsNullOrEmpty( _javaPath ) )
			{
				ForceFailBuild( "Must have java executable to rebuild APK!" );
			}
		}

		if ( isRequiresApktool )
		{
			_apktoolPath = GetBuildOrAgentValue( 
				getBuildValue:() => _buildConfig._pAndroidOptions._pApktoolPath, 
				getAgentValue:() => _agentConfig._pAndroidData._apktoolPath,
				isValidValue:( string value ) => !string.IsNullOrEmpty( value ) && value.ToLower().EndsWith( APKTOOL_JAR ), 
				defaultValue:null );

			if ( string.IsNullOrEmpty( _apktoolPath ) )
			{
				ForceFailBuild( "Must specify valid path to apktool jar to rebuild APK!" );
			}
		}

		if ( isRequiresJarsigner )
		{
			if ( string.IsNullOrEmpty( _jarsignerPath ) )
			{
				ForceFailBuild( "Must have jarsigner executable to rebuild APK!" );
			}
		}

		if ( isRequiresZipalign )
		{
			string	buildToolsPath = GetBuildOrAgentVersionedPath( 
				getBuildPath:() => _pBuildConfig._pAndroidOptions._pBuildToolsPath,
				getBuildVersion:() => _pBuildConfig._pAndroidOptions._pBuildToolsVersion, 
				getAgentPath:( string wantVersion, out string gotVersion ) => _pAgentConfig.GetAndroidBuildToolsPath( wantVersion, out gotVersion ), 
				isValidPath:( string path ) => !string.IsNullOrEmpty( path ),
				defaultPath:null );

			Debug.Log( LOG_TAG_ANDROID + "Android build tools: " + buildToolsPath );
		
			if ( string.IsNullOrEmpty( buildToolsPath ) )
			{
				ForceFailBuild( "Must specify valid path to Android build tools to rebuild APK!" );
			}

			_zipalignPath = FindFile( CombinePaths( buildToolsPath, "zipalign" ), "", "exe" );

			if ( string.IsNullOrEmpty( _zipalignPath ) )
			{
				ForceFailBuild( "Must have zipalign executable to rebuild APK!" );
			}
		}

		if ( PlayerSettings.defaultInterfaceOrientation == UIOrientation.Portrait || PlayerSettings.defaultInterfaceOrientation == UIOrientation.PortraitUpsideDown )
		{
			PlayerSettings.Android.androidTVCompatibility = false;
		}

		#if SUPPORT_ANDROID_INTERNAL_BUILD_SYSTEM
		if ( !AnvilProjectConfig._pUseGradleAndroidBuildSystem )
		{
			EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;
		}
		else
		#endif
		{
			EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

			InitAndroidGradleSettings();

			ModifyAndroidGradleSettings();

			OutputAndroidGradleBuildScript( _projectPath + "/Assets/Plugins/Android/mainTemplate.gradle" );
		}

		EditorUserBuildSettings.exportAsGoogleAndroidProject = _pIsOutputGradleProject;
	}

	private static void ValidateBlockPerms()
	{
		for ( int i = 0; i < _androidBlockPerms.Count; ++i )
		{
			if ( _androidBlockPerms[i] == null )
			{
				_androidBlockPerms.RemoveAt( i-- );
				continue;
			}

			_androidBlockPerms[i] = _androidBlockPerms[i].Trim();

			if ( _androidBlockPerms[i].Length == 0 )
			{
				_androidBlockPerms.RemoveAt( i-- );
				continue;
			}

			for ( int j = 0; j < i; ++j )//remove dupes
			{
				if ( _androidBlockPerms[j] == _androidBlockPerms[i] )
				{
					_androidBlockPerms.RemoveAt( i-- );
					continue;
				}
			}

			if ( !_androidBlockPerms[i].Contains( "." ) )//add default prefix
			{
				_androidBlockPerms[i] = "android.permission." + _androidBlockPerms[i];
			}
		}
	}

	private static void DecodeApk( string apkPath, string decodedPath )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Decoding APK: " + apkPath );
		Execute( _javaPath, new string[] { 
			"-jar", 
			"\"" + _apktoolPath + "\"", 
			"d", 
			"\"" + apkPath + "\"", 
			"-o \"" + decodedPath + "\"" }, _exportPath );

		HackCompressionMetadata( decodedPath );
	}

	private static void HackCompressionMetadata( string decodedPath )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Hacking apktool compression metadata" );

		List<string>	apktoolMetadataLines;
		string			apktoolMetadataPath = CombinePaths( decodedPath, "apktool.yml" );

		try
		{
			apktoolMetadataLines = new List<string>( File.ReadAllLines( apktoolMetadataPath ) );
		}
		catch ( System.Exception )
		{
			Debug.LogError( DEBUG_LOG_PREFIX + "Failed to read apktool metadata file at: " + apktoolMetadataPath );
			return;
		}

		//CC20160603 - should probably use a C# Yaml lib (such as YamlDotNet for Unity, in asset store), but this is a quick and dirty way

		bool	isRemovingDoNotCompressEntries = false;

		for ( int i = 0; i < apktoolMetadataLines.Count; ++i )
		{
			if ( apktoolMetadataLines[i].StartsWith( "compressionType" ) )
			{
				apktoolMetadataLines[i] = apktoolMetadataLines[i].Replace( "false", "true" );
			}
			else if ( apktoolMetadataLines[i].StartsWith( "doNotCompress" ) )
			{
				apktoolMetadataLines[i] += " {}";
				isRemovingDoNotCompressEntries = true;
			}
			else if ( isRemovingDoNotCompressEntries )
			{
				if ( apktoolMetadataLines[i].StartsWith( "-" ) )
				{
					apktoolMetadataLines.RemoveAt( i-- );
					continue;
				}
				else
				{
					isRemovingDoNotCompressEntries = false;
				}
			}
		}

		try
		{
			File.WriteAllLines( apktoolMetadataPath, apktoolMetadataLines.ToArray() );
		}
		catch ( System.Exception )
		{
			Debug.LogError( DEBUG_LOG_PREFIX + "Failed to write hacked apktool metadata file to: " + apktoolMetadataPath );
			return;
		}
	}

	private static void RemoveAndroidPermissions( string manifestPath )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Removing Android permissions from: " + manifestPath );

		XmlDocument manifest = new XmlDocument();
		manifest.Load( manifestPath );

		for ( int i = 0; i < _androidBlockPerms.Count; ++i )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Removing Android permission: " + _androidBlockPerms[i] );
			RemoveAndroidPermission( manifest, _androidBlockPerms[i] );
		}

		manifest.Save( manifestPath );
	}

	private static void RebuildApk( string apkPath, string decodedPath )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Rebuilding APK: " + apkPath );
		Execute( _javaPath, new string[] { 
			"-jar", 
			"\"" + _apktoolPath + "\"", 
			"b", 
			"\"" + decodedPath + "\"", 
			"-o \"" + apkPath + "\"" }, _exportPath );
	}

	private static void SignApk( string apkPath, string keystore, string keyalias, string storepass, string keypass )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Signing APK: " + apkPath );
		Execute( _jarsignerPath, new string[] { 
			"-verbose", 
			"-sigalg SHA1withRSA", 
			"-digestalg SHA1", 
			"-keystore " + keystore, 
			"-storepass " + storepass, 
			"-keypass " + keypass,
			"\"" + apkPath + "\"",
			keyalias }, _exportPath );
	}

	private static void AlignApk( string apkPath )
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Aligning APK: " + apkPath );
		string	apkDir = Path.GetDirectoryName( apkPath );
		string	unalignedApkPath = Path.Combine( apkDir, FILE_NAME_UNALIGNED_APK );

		SafeMoveFile( apkPath, unalignedApkPath );

		Execute( _zipalignPath, new string[] {
			"-f", 
			"-v 4", 
			"\"" + unalignedApkPath + "\"",
			"\"" + apkPath + "\"" }, _exportPath );
		
		SafeDeleteFile( unalignedApkPath );
	}
}

