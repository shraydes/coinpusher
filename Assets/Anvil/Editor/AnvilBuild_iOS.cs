#if UNITY_5_4_OR_NEWER
#define SUPPORT_TVOS
#endif

#if ( UNITY_IOS || UNITY_TVOS ) && UNITY_5_4_OR_NEWER
#define SUPPORT_ODR
#endif

#if UNITY_5_4_4 || UNITY_5_5_OR_NEWER
#define USE_UNITY_MANUAL_SIGNING_API
#endif

#if UNITY_5_6_OR_NEWER
#define SUPPORT_XCODE_9
#endif

using UnityEngine;
using UnityEditor;

using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using AmuzoEngine;
using AnvilBuildCommon;

public static partial class AnvilBuild
{
	private const string	APPLE_SIGNING_IDENTITY_PREFIX = "iPhone Distribution: ";
	private const string	APPLE_SIGNING_IDENTITY_STANDARD = "iPhone Distribution: Amuzo Games Ltd";
	private const string	APPLE_SIGNING_IDENTITY_ENTERPRISE = "iPhone Distribution: Amuzo Games Ltd (Ent)";
	public const string		PROVISIONING_PROFILE_EXT = ".mobileprovision";
	
	private const string	DEFAULT_XCODE_APP_PATH = "/Applications/Xcode.app";
	private const string	XCODE_RELATIVE_PATH = "Contents/MacOS/Xcode";
	private const string	XCODEBUILD_RELATIVE_PATH = "Contents/Developer/usr/bin/xcodebuild";
	private const string	XCRUN_RELATIVE_PATH = "Contents/Developer/usr/bin/xcrun";
	private const string	DEFAULT_MAC_KEYCHAIN_PASSWORD = "bamboo";
	
	private static string	_xcodeExePath = null;
	private static string	_xcodeBuildPath = null;
	private static string	_xcrunPath = null;
	private static string	_provisionPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "/Library/MobileDevice/Provisioning Profiles";

	private const string	XCODE_CONFIGURATION_NAME = "Release";
	private const string	XCODE_TARGET_NAME = "Unity-iPhone";
	private const string	XCODE_SCHEME_NAME = "Unity-iPhone";
	private const string	XCODE_TARGET_SDK_NAME_IOS = "iphoneos";
	private const string	XCODE_TARGET_SDK_NAME_TVOS = "appletvos";
	private const string	XCODE_ARCHIVE_NAME = "iOS.xcarchive";
	private const string	XCODE_EXPORT_OPTIONS_NAME = "ExportOptions.plist";
	private const string	XCODE_ODR_EXPORT_FOLDER_NAME = "OnDemandResources";
	private const string	XCODE_ENTITLEMENTS_FILE_EXT = ".entitlements";
	private const string	XCODE_PROJECT_FILE_NAME = "project.pbxproj";

	public const string		IOS_DISTRIBUTION_METHOD_APP_STORE = "app-store";
	public const string		IOS_DISTRIBUTION_METHOD_ENTERPRISE = "enterprise";
	public const string		IOS_DISTRIBUTION_METHOD_AD_HOC = "ad-hoc";
	public const string		IOS_DISTRIBUTION_METHOD_DEVELOPMENT = "development";

	private static string	_macKeychainPassword = null;

	private const string	XCODE_VERSION_PREFIX = "Xcode ";
	private static string	_xcodeVersionString;
	private static int[]	_xcodeVersionParts;

	private struct IosVars
	{
		public bool		_isBuildingOnMac;
		public bool		_isXcodeManualBuild;
		public bool		_useEnterpriseIdentity;
		public string	_distributionMethod;
		public string	_provisionName;
		public bool		_isEmbedOnDemandResources;
		public string	_onDemandResourcesBaseUrl;
		public string	_identityToUse;
		public string	_provisioningProfileUuid;
		public string	_teamName;
	}

	private static IosVars	_iosVars = new IosVars();

	private static string _pXcodeTargetSdk
	{
		get
		{
#if SUPPORT_TVOS
			return _buildTarget == BuildTarget.iOS ? XCODE_TARGET_SDK_NAME_IOS : _buildTarget == BuildTarget.tvOS ? XCODE_TARGET_SDK_NAME_TVOS : null;
#else
			return _buildTarget == BuildTarget.iOS ? XCODE_TARGET_SDK_NAME_IOS : null;
#endif
		}
	}

#if SUPPORT_ODR
	public static bool IsEnableAppleOnDemandResources( BuildConfig buildConfigOverride = null )
	{
		BuildConfig	buildConfig = buildConfigOverride ?? _pBuildConfig;

		if ( buildConfig != null && buildConfig._pIosOptions._pIsEnableOnDemandResources.HasValue )
		{
			return buildConfig._pIosOptions._pIsEnableOnDemandResources.Value;
		}

		if ( EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS || EditorUserBuildSettings.activeBuildTarget == BuildTarget.tvOS )
		{
			return PlayerSettings.iOS.useOnDemandResources;
		}

		return false;
	}
#endif

	private static void PreBuild_iOS_tvOS()
	{
		if ( !_pIsWantBuildApp ) return;

		_iosVars._isBuildingOnMac = Application.platform == RuntimePlatform.OSXEditor;

		GetIosSigningAndExportParameters( out _iosVars._useEnterpriseIdentity, out _iosVars._distributionMethod, out _iosVars._provisionName, out _iosVars._isEmbedOnDemandResources, out _iosVars._onDemandResourcesBaseUrl );

		if ( string.IsNullOrEmpty( _iosVars._provisionName ) )
		{
			_iosVars._provisionName = "Default";
		}
		
		UnityEngine.Debug.Log("Provision Name: " + _iosVars._provisionName);
		
		if ( _iosVars._isBuildingOnMac )
		{
			SetMacHostSpecificData();
		}

		if ( _pBuildConfig._pIosOptions._pIsManualXcodeBuild.HasValue )
		{
			_iosVars._isXcodeManualBuild = _pBuildConfig._pIosOptions._pIsManualXcodeBuild.Value;
		}

		PrepareProvisioningProfile( _pDirFilesIosTvos + _iosVars._provisionName + PROVISIONING_PROFILE_EXT );

		if ( string.IsNullOrEmpty( _iosVars._teamName ) )
		{
			UnityEngine.Debug.LogWarning("ANVIL_BUILD_IOS: Failed to get team name from provisioning profile. Using default code signing identity" );

			_iosVars._identityToUse = _iosVars._useEnterpriseIdentity ? APPLE_SIGNING_IDENTITY_ENTERPRISE : APPLE_SIGNING_IDENTITY_STANDARD;
		}
		else
		{
			_iosVars._identityToUse = APPLE_SIGNING_IDENTITY_PREFIX + _iosVars._teamName;
		}
		
		UnityEngine.Debug.Log("ANVIL_BUILD_IOS: identity to use: " + _iosVars._identityToUse );
		
#if USE_UNITY_MANUAL_SIGNING_API
		PlayerSettings.iOS.appleEnableAutomaticSigning = false;
		switch ( _buildTarget )
		{
		case BuildTarget.iOS:
			PlayerSettings.iOS.iOSManualProvisioningProfileID = _iosVars._provisioningProfileUuid;
			break;
		case BuildTarget.tvOS:
			PlayerSettings.iOS.tvOSManualProvisioningProfileID = _iosVars._provisioningProfileUuid;
			break;
		default:
			ForceFailBuild( string.Format( "Unexpected build target: {0}", _buildTarget ) );
			break;
		}
#endif
	}

	public static void DoXcodeBuild()
	{
		List<string>	securityArgs = new List<string>();
		securityArgs.Add( "unlock-keychain" );
		securityArgs.Add( "-p " + _macKeychainPassword );
		securityArgs.Add( CombinePaths( System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Library/Keychains/login.keychain" ) );

		Execute( "security", securityArgs.ToArray(), _pOutputPath );

		CheckRemovePushNotificationCapability();

		string	tempPackagePath;

		if ( _iosVars._isXcodeManualBuild )
		{
			tempPackagePath = LaunchXcode();
		}
		else
		{
			if ( _xcodeVersionParts[0] < 7 )
			{
				tempPackagePath = XcodeBuildAndPackage_Xcode6( _iosVars._identityToUse, _iosVars._provisioningProfileUuid );
			}
			else if ( _xcodeVersionParts[0] == 7 )
			{
				tempPackagePath = XcodeBuildAndPackage_Xcode7( _iosVars._identityToUse, _iosVars._provisioningProfileUuid, _iosVars._distributionMethod, _iosVars._isEmbedOnDemandResources, _iosVars._onDemandResourcesBaseUrl );
			}
			else
			{
				tempPackagePath = XcodeBuildAndPackage_Xcode8( _iosVars._identityToUse, _iosVars._provisioningProfileUuid, _iosVars._distributionMethod, _iosVars._isEmbedOnDemandResources, _iosVars._onDemandResourcesBaseUrl );
			}
		}

		FileInfo	tempPackageFileInfo = new FileInfo( tempPackagePath );

		string	outputName = _pOutputName;

		if ( tempPackageFileInfo.Exists && !string.IsNullOrEmpty( outputName ) )
		{
			string	outputPackagePath = CombinePaths( _pExportPath, outputName + ".ipa" );
			SafeMoveFile( tempPackagePath, outputPackagePath );

			string	tempOdrFolder = CombinePaths( tempPackageFileInfo.DirectoryName, XCODE_ODR_EXPORT_FOLDER_NAME );

			if ( Directory.Exists( tempOdrFolder ) )
			{
				string	outputOdrFolder = CombinePaths( _pExportPath, outputName + "_ODR" );
				SafeMoveDirectory( tempOdrFolder, outputOdrFolder );
			}
		}
	}

	private static string LaunchXcode()
	{
		string	expectedPackagePath = CombinePaths( _pExportPath, XCODE_TARGET_NAME + ".ipa" );

		List<string>	xcodeArgs = new List<string>();

		xcodeArgs.Add( XCODE_TARGET_NAME + ".xcodeproj" );

		Execute( _xcodeExePath, xcodeArgs.ToArray(), _pOutputPath );

		return expectedPackagePath;
	}

	//return ipa package path
	private static string XcodeBuildAndPackage_Xcode6( string identity, string provisioningProfile )
	{
		List<string>	buildArgs = new List<string>();

		buildArgs.Add( "-target " + XCODE_TARGET_NAME );

		AddCommonXcodeBuildOptions( buildArgs );
		AddCommonXcodeBuildSettings( buildArgs, identity, provisioningProfile );
	
		Execute( _xcodeBuildPath, buildArgs.ToArray(), _pOutputPath );
		

		string	tempPackagePath = CombinePaths( _pExportPath, "iOS.ipa" );
		
		List<string>	packageArgs = new List<string>();
		packageArgs.Add( "-sdk "+ _pXcodeTargetSdk );
		packageArgs.Add( "PackageApplication" );
		packageArgs.Add( "-v \"" + GetIosAppFilePath() + "\"" );
		packageArgs.Add( "-o \"" + tempPackagePath + "\"" );

		Execute( _xcrunPath, packageArgs.ToArray(), _pOutputPath );

		return tempPackagePath;
	}

	//return ipa package path
	private static string XcodeBuildAndPackage_Xcode7( string identity, string provisioningProfile, string distributionMethod, bool isEmbedOnDemandResources, string onDemandResourcesBaseUrl )
	{
		List<string>	buildArgs = new List<string>();

		buildArgs.Add( "-scheme " + XCODE_SCHEME_NAME );
		buildArgs.Add( "-archivePath \"" + CombinePaths( _pOutputPath, XCODE_ARCHIVE_NAME ) + "\"" );

		AddCommonXcodeBuildOptions( buildArgs );
		AddCommonXcodeBuildSettings( buildArgs, identity, provisioningProfile );

#if SUPPORT_ODR
		if ( IsEnableAppleOnDemandResources() )
		{
			buildArgs.Add( "ENABLE_ON_DEMAND_RESOURCES=YES" );
		}
		else
		{
			buildArgs.Add( "ENABLE_ON_DEMAND_RESOURCES=NO" );
		}
#endif

		if ( !string.IsNullOrEmpty( onDemandResourcesBaseUrl ) )
		{
			if ( !onDemandResourcesBaseUrl.EndsWith( "/" ) )
			{
				onDemandResourcesBaseUrl += "/";
			}

			buildArgs.Add( "ASSET_PACK_MANIFEST_URL_PREFIX=\"" + onDemandResourcesBaseUrl + "\"" );
		}

		buildArgs.Add( "archive" );
	
		Execute( _xcodeBuildPath, buildArgs.ToArray(), _pOutputPath );
		

		List<string>	exportArgs = new List<string>();
		exportArgs.Add( "-exportArchive" );
		exportArgs.Add( "-archivePath \"" + CombinePaths( _pOutputPath, XCODE_ARCHIVE_NAME ) + "\"" );
		exportArgs.Add( "-exportPath \"" + _pExportPath + "\"" );

		string	exportOptionsPath = CombinePaths( _pOutputPath, XCODE_EXPORT_OPTIONS_NAME );

		GenerateIpaExportOptions( exportOptionsPath, provisioningProfile, distributionMethod, isEmbedOnDemandResources, onDemandResourcesBaseUrl );

		exportArgs.Add( "-exportOptionsPlist \"" + exportOptionsPath + "\"" );

		Execute( _xcodeBuildPath, exportArgs.ToArray(), _pOutputPath );

		return CombinePaths( _pExportPath, XCODE_TARGET_NAME + ".ipa" );
	}
		
	//return ipa package path
	private static string XcodeBuildAndPackage_Xcode8( string identity, string provisioningProfile, string distributionMethod, bool isEmbedOnDemandResources, string onDemandResourcesBaseUrl )
	{
		return XcodeBuildAndPackage_Xcode7( identity, provisioningProfile, distributionMethod, isEmbedOnDemandResources, onDemandResourcesBaseUrl );
	}

	private static void AddCommonXcodeBuildOptions( List<string> buildArgs )
	{
		buildArgs.Add( "-configuration " + XCODE_CONFIGURATION_NAME );
		buildArgs.Add( "-sdk " + _pXcodeTargetSdk );

		bool	isForceLegacyBuildSystem = AnvilProjectConfig.GetIosTvosConfig( _buildTarget )._pIsForceLegacyBuildSystem;

		if ( isForceLegacyBuildSystem )
		{
			if ( _xcodeVersionParts[0] >= 10 )
			{
				buildArgs.Add( "-UseModernBuildSystem=NO" );
			}
			else
			{
				UnityEngine.Debug.LogWarning( LOG_TAG + "Invalid options: isForceLegacyBuildSystem=true with XcodeVersion=" + _xcodeVersionParts[0] );
			}
		}
	}

	private static void AddCommonXcodeBuildSettings( List<string> buildArgs, string identity, string provisioningProfile )
	{
		buildArgs.Add( "CODE_SIGN_IDENTITY=\"" + identity + "\"" );

#if !USE_UNITY_MANUAL_SIGNING_API
		buildArgs.Add( "PROVISIONING_PROFILE=\"" + provisioningProfile + "\"" );
#endif

		bool	isEnableBitcode = AnvilProjectConfig.GetIosTvosConfig( _buildTarget )._pIsEnableBitcode;

		if ( _pBuildConfig._pIosOptions._pIsEnableBitcode.HasValue )
		{
			isEnableBitcode = _pBuildConfig._pIosOptions._pIsEnableBitcode.Value;
		}

		if ( isEnableBitcode )
		{
			buildArgs.Add( "ENABLE_BITCODE=YES" );
		}
		else
		{
			buildArgs.Add( "ENABLE_BITCODE=NO" );
		}

		bool	isEnableSwift = AnvilProjectConfig.GetIosTvosConfig( _buildTarget )._pIsEnableSwift;

		if ( _pBuildConfig._pIosOptions._pIsEnableSwift.HasValue )
		{
			isEnableSwift = _pBuildConfig._pIosOptions._pIsEnableSwift.Value;
		}

		if ( isEnableSwift )
		{
			buildArgs.Add( "EMBEDDED_CONTENT_CONTAINS_SWIFT=YES" );

			string	swiftVersion = AnvilProjectConfig.GetIosTvosConfig( _buildTarget )._pSwiftVersion;

			if ( !string.IsNullOrEmpty( _pBuildConfig._pIosOptions._pSwiftVersion ) )
			{
				swiftVersion = _pBuildConfig._pIosOptions._pSwiftVersion;
			}

			if ( !string.IsNullOrEmpty( swiftVersion ) )
			{
				buildArgs.Add( "SWIFT_VERSION=" + swiftVersion );
			}
		}
	}
	
	private static void GetIosSigningAndExportParameters( out bool useEnterpriseIdentity, out string distributionMethod, out string provisionName, out bool isEmbedOnDemandResources, out string onDemandResourcesBaseUrl )
	{
		useEnterpriseIdentity = false;
		distributionMethod = IOS_DISTRIBUTION_METHOD_APP_STORE;
		provisionName = "Default";
		
		string[]	cert = AnvilBuildInfo.GetVar( "CERTIFICATE", "" ).Split( '|' );
		
		if ( cert.Length >= 1 )
		{
			provisionName = cert[0];
			
			if ( provisionName.ToLower().EndsWith( PROVISIONING_PROFILE_EXT ) )
			{
				provisionName = provisionName.Substring( 0, provisionName.Length - PROVISIONING_PROFILE_EXT.Length );
			}
			
			if ( cert.Length >= 2 )
			{
				distributionMethod = GetDistributionMethodFromShortString( cert[1] );
			}
		}
		
		if ( !string.IsNullOrEmpty( _pBuildConfig._pIosOptions._pDistributionMethod ) )
		{
			distributionMethod = _pBuildConfig._pIosOptions._pDistributionMethod;
		}

		useEnterpriseIdentity = string.Equals( distributionMethod, IOS_DISTRIBUTION_METHOD_ENTERPRISE, System.StringComparison.OrdinalIgnoreCase );

		if ( !string.IsNullOrEmpty( _pBuildConfig._pIosOptions._pProvisioningProfile ) )
		{
			provisionName = _pBuildConfig._pIosOptions._pProvisioningProfile;
		}

		bool	isSplit;
		bool.TryParse( AnvilBuildInfo.GetVar( "SPLIT_APK", "false" ), out isSplit );

		isEmbedOnDemandResources = !isSplit;

		if ( _pBuildConfig._pIosOptions._pIsEmbedOnDemandResources.HasValue )
		{
			isEmbedOnDemandResources = _pBuildConfig._pIosOptions._pIsEmbedOnDemandResources.Value;
		}

		onDemandResourcesBaseUrl = AnvilBuildInfo.GetVar( "ODR_URL", null );

		if ( !string.IsNullOrEmpty( _pBuildConfig._pIosOptions._pOnDemandResourcesBaseUrl ) )
		{
			onDemandResourcesBaseUrl = _pBuildConfig._pIosOptions._pOnDemandResourcesBaseUrl;
		}
	}

	private static string GetDistributionMethodFromShortString( string distShort )
	{
		if ( distShort.StartsWith( "app", System.StringComparison.OrdinalIgnoreCase ) ) return IOS_DISTRIBUTION_METHOD_APP_STORE;
		if ( distShort.StartsWith( "ent", System.StringComparison.OrdinalIgnoreCase ) ) return IOS_DISTRIBUTION_METHOD_ENTERPRISE;
		if ( distShort.StartsWith( "ad", System.StringComparison.OrdinalIgnoreCase ) ) return IOS_DISTRIBUTION_METHOD_AD_HOC;
		if ( distShort.StartsWith( "dev", System.StringComparison.OrdinalIgnoreCase ) ) return IOS_DISTRIBUTION_METHOD_DEVELOPMENT;
		return null;
	}
	
	private static void SetMacHostSpecificData()
	{
		string	xcodeAppPath = GetXcodeAppPath();
		
		if ( !string.IsNullOrEmpty( xcodeAppPath ) )
		{
			UnityEngine.Debug.Log( "ANVIL_BUILD_IOS: Xcode path: " + xcodeAppPath );
		}
		else
		{
			ForceFailBuild( "No Xcode app path!" );
		}

		_xcodeExePath = CombinePaths( xcodeAppPath, XCODE_RELATIVE_PATH );
		_xcodeBuildPath = CombinePaths( xcodeAppPath, XCODEBUILD_RELATIVE_PATH );
		_xcrunPath = CombinePaths( xcodeAppPath, XCRUN_RELATIVE_PATH );

		//Figure out Xcode version
		_xcodeVersionString = null;
		Execute( _xcodeBuildPath, new string[] { "-version" }, stdoutHandler:( string line ) => {
			if ( line.StartsWith( XCODE_VERSION_PREFIX ) && string.IsNullOrEmpty( _xcodeVersionString ) )
			{
				_xcodeVersionString = line.Substring( XCODE_VERSION_PREFIX.Length );
				if ( ParseXcodeVersionString() == false )
				{
					UnityEngine.Debug.LogWarning( "ANVIL_BUILD_IOS: Invalid Xcode version string: " + _xcodeVersionString );
					_xcodeVersionString = null;
				}
			}
		} );

		if ( _xcodeVersionParts == null || _xcodeVersionParts.Length == 0 )
		{
			ForceFailBuild( "Failed to determine Xcode version!" );
		}
		
		_macKeychainPassword = GetBuildOrAgentValue( getBuildValue:() => _pBuildConfig._pIosOptions._pKeychainPassword,
			getAgentValue:() => _pAgentConfig._pKeychainPassword,
			isValidValue:( string value ) => !string.IsNullOrEmpty( value ),
			defaultValue:DEFAULT_MAC_KEYCHAIN_PASSWORD );
		
		if ( string.IsNullOrEmpty( _macKeychainPassword ) )
		{
			ForceFailBuild( "No keychain password!" );
		}
	}

	private static string GetXcodeAppPath()
	{
		return GetBuildOrAgentVersionedPath( 
			getBuildPath:() => _pBuildConfig._pIosOptions._pXcodeAppPath,
			getBuildVersion:() => _pBuildConfig._pIosOptions._pXcodeVersion, 
			getAgentPath:( string wantVersion, out string gotVersion ) => _pAgentConfig.GetXcodePath( wantVersion, out gotVersion ), 
			isValidPath:( string path ) => !string.IsNullOrEmpty( path ),
			defaultPath:DEFAULT_XCODE_APP_PATH );
	}

	private static bool ParseXcodeVersionString()
	{
		_xcodeVersionParts = null;

		if ( string.IsNullOrEmpty( _xcodeVersionString ) ) return false;

		string[]	parts = _xcodeVersionString.Split( '.' );
		List<int>	intParts = new List<int>( parts.Length );
		int			intPart;

		for ( int i = 0; i < parts.Length; ++i )
		{
			if ( int.TryParse( parts[i], out intPart ) )
			{
				intParts.Add( intPart );
			}
			else
			{
				return false;
			}
		}

		_xcodeVersionParts = intParts.ToArray();

		return true;
	}

	private static void GenerateIpaExportOptions( string exportOptionsPath, string provisioningProfileUuid, string distributionMethod, bool isEmbedOnDemandResources, string onDemandResourcesBaseUrl )
	{
		XmlDocument	plist = new XmlDocument();

		plist.Load( CombinePaths( _pProjectPath, _pDirFilesIosTvos, "Empty.plist" ) );

		XmlElement	rootDict = plist.DocumentElement.GetElementsByTagName( "dict" )[0] as XmlElement;

#if SUPPORT_XCODE_9
		SetIosPlistDictValue( plist, rootDict, "provisioningProfiles", "dict", setAction:profileDict => {
			SetIosPlistDictValue( plist, profileDict, _pBundleIdentifier, "string", setAction:profileElem => {
				profileElem.InnerText = provisioningProfileUuid;
			} );
		} );
#endif

		SetIosPlistDictValue( plist, rootDict, "method", "string", e => {
			e.InnerText = distributionMethod;
		} );

	    SetIosPlistDictBooleanValue( plist, rootDict, "embedOnDemandResourcesAssetPacksInBundle", isEmbedOnDemandResources );

        if ( !string.IsNullOrEmpty( onDemandResourcesBaseUrl ) )
		{
			if ( !onDemandResourcesBaseUrl.EndsWith( "/" ) )
			{
				onDemandResourcesBaseUrl += "/";
			}

			SetIosPlistDictValue( plist, rootDict, "onDemandResourcesAssetPacksBaseURL", "string", setAction:e => {
				e.InnerText = onDemandResourcesBaseUrl;
			} );
		}

		UnityEngine.Debug.Log( DEBUG_LOG_PREFIX + "Plugins modifying iOS export options" );
		ForEachBuildPlugin( p => {
			p.OnSetIosExportOptions( plist, rootDict );
		}, true );

		SaveIosPlist( plist, exportOptionsPath );
	}

	private static string GetIosAppFilePath()
	{
		string		buildSubdir = CombinePaths( "build", XCODE_CONFIGURATION_NAME + "-" + _pXcodeTargetSdk );
		string[]	fileEntries = System.IO.Directory.GetDirectories( CombinePaths( _pOutputPath, buildSubdir ) );

		for(int i = 0; i < fileEntries.Length; i++)
		{
			string	fileEntry = Path.GetFileName( fileEntries[i] );

			if ( fileEntry.EndsWith( ".app" ) )
			{
				return CombinePaths( buildSubdir, fileEntry );
			}
		}

		throw new System.Exception("Failed to locate generated app");
	}

	private static void PrepareProvisioningProfile( string path )
	{
		PrepareProvisioningProfile( path, out _iosVars._provisioningProfileUuid, out _iosVars._teamName );
	}

	public static void PrepareProvisioningProfile( string path, out string uuid, out string teamName )
	{
		UnityEngine.Debug.Log("Preparing Provisioning Profile: " + path);

		if ( _iosVars._isBuildingOnMac )
		{
			ReadProvisioningProfile_Mac( path, out uuid, out teamName );
		}
		else
		{
			ReadProvisioningProfile_Windows( path, out uuid, out teamName );
		}

		UnityEngine.Debug.Log("Provision UUID: " + uuid );
		UnityEngine.Debug.Log("Provision TeamName: " + teamName );

		if ( _iosVars._isBuildingOnMac )
		{
			try
			{
				System.IO.Directory.CreateDirectory(_provisionPath);
			}
			catch(System.Exception) { }

			string	installedProvisionPath = CombinePaths( _provisionPath, uuid + PROVISIONING_PROFILE_EXT );
			UnityEngine.Debug.Log( "Installed Provision Path: " + installedProvisionPath );

			try
			{
				FileUtil.ReplaceFile( path, installedProvisionPath );
			}
			catch(System.Exception) { }
		}
	}

	private static void ReadProvisioningProfile_Windows( string path, out string uuid, out string teamName )
	{
		System.IO.StreamReader sr = new System.IO.StreamReader(path);
		string contents = sr.ReadToEnd();

		UnityEngine.Debug.Log("Provisioning Profile Contents: " + contents);

		int startIndex = -1;

		for(int i = 0; i < contents.Length - 24; i++)
		{
			if(contents[i] == '-' && contents[i + 5] == '-' && contents[i + 5 + 5] == '-' && contents[i + 5 + 5 + 5] == '-')
			{
				startIndex = i - 8;
				break;
			}
		}

		if(startIndex == -1)
		{
			throw new System.Exception("Failed to extract UUID");
		}

		uuid = contents.Substring(startIndex, 36);
		teamName = null;
	}

	private static void ReadProvisioningProfile_Mac( string path, out string uuid, out string teamName )
	{
		List<string>	securityArgs = new List<string>();
		securityArgs.Add( "cms" );
		securityArgs.Add( "-D" );
		securityArgs.Add( "-i \"" + path + "\"" );

		string	tempXmlPath = CombinePaths( _pExportPath, _iosVars._provisionName + ".xml" );

		securityArgs.Add( "-o \"" + tempXmlPath + "\"" );

		Execute( "security", securityArgs.ToArray(), _pProjectPath );

		try
		{
			XmlDocument	profileXml = new XmlDocument();

			profileXml.Load( tempXmlPath );

			XmlElement	rootDict = profileXml.DocumentElement.GetElementsByTagName( "dict" )[0] as XmlElement;

			if ( rootDict == null )
			{
				ForceFailBuild( "No root dict in plist: " + tempXmlPath );
			}

			XmlElement	elem = FindIosPlistDictValueElement( rootDict, "UUID", "string" );

			if ( elem == null )
			{
				ForceFailBuild( "No UUID element in plist: " + tempXmlPath );
			}

			uuid = elem.InnerText;

			elem = FindIosPlistDictValueElement( rootDict, "TeamName", "string" );

			if ( elem == null )
			{
				ForceFailBuild( "No TeamName element in plist: " + tempXmlPath );
			}

			teamName = elem.InnerText;
		}
		finally
		{
			SafeDeleteFile( tempXmlPath );
		}
	}

	private static void SetLanguagesInIosPlist( XmlDocument plist, XmlElement rootDict )
	{
		string[]	langs = AnvilBuildInfo.GetVar( "LANGS", "" ).Split( '|' );

		if ( langs == null || langs.Length == 0 || ( langs.Length == 1 && string.IsNullOrEmpty( langs[0] ) ) )
		{
			langs = new string[] { "EN" };
		}

		SetIosPlistDictValue( plist, rootDict, "CFBundleLocalizations", "array", arrayElem => {
			XmlElement	newArrayElem = plist.CreateElement( "array" );

			for ( int i = 0; i < langs.Length; ++i )
			{
				XmlUtils.AddXmlChild( newArrayElem, "string", langElem => {
					langElem.InnerText = langs[i].ToLower().Trim();
				} );
			}

			rootDict.ReplaceChild( newArrayElem, arrayElem );
		} );
	}

	private static void SetOrientationInIosPlist( XmlDocument plist, XmlElement rootDict )
	{
		UIOrientation	defaultOri = PlayerSettings.defaultInterfaceOrientation;

		string	defaultOriStr = null;

		switch ( defaultOri )
		{
		case UIOrientation.Portrait:
			defaultOriStr = "UIInterfaceOrientationPortrait";
			break;
		case UIOrientation.PortraitUpsideDown:
			defaultOriStr = "UIInterfaceOrientationPortraitUpsideDown";
			break;
		case UIOrientation.LandscapeLeft:
		case UIOrientation.AutoRotation:
			defaultOriStr = "UIInterfaceOrientationLandscapeLeft";
			break;
		case UIOrientation.LandscapeRight:
			defaultOriStr = "UIInterfaceOrientationLandscapeRight";
			break;
		}

		if ( string.IsNullOrEmpty( defaultOriStr ) )
		{
			ForceFailBuild( "Unexpected defaultOrientation: " + defaultOri );
		}

		SetIosPlistDictValue( plist, rootDict, "UIInterfaceOrientation", "string", valueElem => {
			valueElem.InnerText = defaultOriStr;
		} );

		if ( defaultOri == UIOrientation.AutoRotation )
		{
			List<string>	supportOriStrs = new List<string>();

			if ( PlayerSettings.allowedAutorotateToPortrait ) supportOriStrs.Add( "UIInterfaceOrientationPortrait" );
			if ( PlayerSettings.allowedAutorotateToPortraitUpsideDown ) supportOriStrs.Add( "UIInterfaceOrientationPortraitUpsideDown" );
			if ( PlayerSettings.allowedAutorotateToLandscapeLeft ) supportOriStrs.Add( "UIInterfaceOrientationLandscapeLeft" );
			if ( PlayerSettings.allowedAutorotateToLandscapeRight ) supportOriStrs.Add( "UIInterfaceOrientationLandscapeRight" );

			SetIosPlistDictValue( plist, rootDict, "UISupportedInterfaceOrientations", "array", arrayElem => {
				XmlElement	newArrayElem = plist.CreateElement( "array" );
				for ( int i = 0; i < supportOriStrs.Count; ++i )
				{
					XmlUtils.AddXmlChild( newArrayElem, "string", langElem => {
						langElem.InnerText = supportOriStrs[i];
					} );
				}
				rootDict.ReplaceChild( newArrayElem, arrayElem );
			} );
		}
		else
		{
			RemoveIosPlistDictionaryEntry( rootDict, "UISupportedInterfaceOrientations", "array" );
		}
	}

	private static void CheckRemovePushNotificationCapability()
	{
		if ( !AnvilProjectConfig.GetIosTvosConfig( _buildTarget )._pIsRemovePushNotificationCapability ) return;
		
		string	entitlementsFilePath = null;

		DirectoryInfo	dirInfo = new DirectoryInfo( _pOutputPath );

		FileHelper.ForEachFileInDirectory( dirInfo, action:( FileInfo fi ) => {
			if ( string.IsNullOrEmpty( entitlementsFilePath ) )
			{
				entitlementsFilePath = fi.FullName;
			}
			else
			{
				ForceFailBuild( "Unexpected: Multiple entitlements files found: " + entitlementsFilePath + ", " + fi.FullName );
			}
		}, match:( FileInfo fi ) => fi.Extension == XCODE_ENTITLEMENTS_FILE_EXT );

		if ( !string.IsNullOrEmpty( entitlementsFilePath ) )
		{
			XmlDocument	plist = new XmlDocument();
			plist.Load( entitlementsFilePath );

			XmlElement	rootDict = plist.DocumentElement.GetElementsByTagName("dict")[0] as XmlElement;

			RemoveIosPlistDictionaryEntry( rootDict, "aps-environment", "string" );

			plist.Save( entitlementsFilePath );
		}

		string	projectFilePath = CombinePaths( _pOutputPath, XCODE_TARGET_NAME + ".xcodeproj", XCODE_PROJECT_FILE_NAME );

		FileHelper.ForEachLine( projectFilePath, lineAction:( List<string> lines, int i ) => {
			if ( lines[i].Contains( "com.apple.Push" ) )
			{
				UnityEngine.Debug.LogWarning( LOG_TAG + "Removing com.apple.Push capability from Xcode project file: " + projectFilePath );
				lines.RemoveRange( i, 3 );
				return -3;
			}
			return 0;
		}, saveFilePath:projectFilePath );
	}
}
