﻿#if UNITY_4_6
#define UNITY_4
#endif

#if !UNITY_4
#define SUPPORT_WEBGL
#endif

#if UNITY_5_4_OR_NEWER
#define SUPPORT_TVOS
#endif

#if !UNITY_5_4_OR_NEWER
#define SUPPORT_WEBPLAYER
#endif

#if ( UNITY_IOS || UNITY_TVOS ) && UNITY_5_4_OR_NEWER
#define SUPPORT_ODR
#endif

using System.Collections.Generic;
using System.Text;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using LitJson;
using AnvilBuildCommon;

public class AnvilWindow : EditorWindow
{
	public enum EIosDistributionMethod
	{
		DEFAULT,
		APP_STORE,
		ENTERPRISE,
		AD_HOC,
		DEVELOPMENT
	}

	private const string DEFAULT_BUILD_METHOD = "AnvilBuild.Build";

	private static string _agentConfigPath = "";

	private static string _agentConfigJson = "";

	private static string _buildConfigJson = "";

	private static BuildConfig _buildConfig = CreateBuildConfig();

	private static string _definesString = "";

	private static string _androidBlockPermsString = "";

	private static EIosDistributionMethod	_iosDistributionMethod = EIosDistributionMethod.DEFAULT;

	private static string _overrideBuildMethod = null;

	private bool	_isShowAgentConfig = false;

	[MenuItem( ( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Anvil" ) )]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		AnvilWindow window = ( AnvilWindow )EditorWindow.GetWindow( typeof( AnvilWindow ) );
#if UNITY_4
		window.title = "Anvil (" + EditorUserBuildSettings.activeBuildTarget + ")";
#else	
		window.titleContent = new GUIContent( "Anvil (" + EditorUserBuildSettings.activeBuildTarget + ")" );
#endif
		window.GetAgentConfigEnv();
		window.GetBuildConfigEnv();
	}

	void OnEnable()
	{
		GetAgentConfigEnv();
		GetBuildConfigEnv();
	}

	void OnGUI()
	{
		EditorGUILayout.BeginHorizontal();

		_agentConfigPath = EditorGUILayout.TextField( "Agent Config", _agentConfigPath );

		if ( string.IsNullOrEmpty( _agentConfigPath ) )
		{
			if ( GUILayout.Button( "Find" ) )
			{
				_agentConfigPath = EditorUtility.OpenFilePanel( "Anvil Agent Config", "", "txt" );

				if ( !string.IsNullOrEmpty( _agentConfigPath ) )
				{
					SetAgentConfigEnv();
				}
			}
		}
		else
		{
			if ( GUILayout.Button( "Set" ) )
			{
				SetAgentConfigEnv();
			}
		}

		EditorGUILayout.EndHorizontal();

		if ( !string.IsNullOrEmpty( _agentConfigJson ) )
		{
			_isShowAgentConfig = EditorGUILayout.Foldout( _isShowAgentConfig, "Agent Config JSON" );

			if ( _isShowAgentConfig )
			{
				EditorGUILayout.TextArea( _agentConfigJson );
			}
		}

		_buildConfig._pProductCode = EditorGUILayout.TextField( "Product Code", _buildConfig._pProductCode );
		_buildConfig._pBuildId = EditorGUILayout.TextField( "Build ID", _buildConfig._pBuildId );
		_buildConfig._pBuildVersion = EditorGUILayout.TextField( "Build Version", _buildConfig._pBuildVersion );
		_buildConfig._pOutputName = EditorGUILayout.TextField( "Output Name", _buildConfig._pOutputName );

		ShowString( "Bundle ID", _buildConfig._pBundleId, value => { _buildConfig._pBundleId = value; } );
		ShowString( "Display Name", _buildConfig._pDisplayName, value => { _buildConfig._pDisplayName = value; } );
		ShowNullable( "Asset Bundles", _buildConfig._pAssetBundleAction, value => { _buildConfig._pAssetBundleAction = value; }, 2, ( label, value ) => EditorGUILayout.IntField( label, value ) );
		ShowNullable( "Delete Last Build", _buildConfig._pIsDeleteLastBuild, value => { _buildConfig._pIsDeleteLastBuild = value; }, true, ( label, value ) => EditorGUILayout.Toggle( label, value ) );

		_definesString = EditorGUILayout.TextField( "Defines", _definesString );

		ShowNullable( "Development", _buildConfig._pBuildOptions._pIsDevelopment, value => { _buildConfig._pBuildOptions._pIsDevelopment = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
		ShowNullable( "Allow Debugging", _buildConfig._pBuildOptions._pIsAllowDebugging, value => { _buildConfig._pBuildOptions._pIsAllowDebugging = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );

		switch( EditorUserBuildSettings.activeBuildTarget )
		{
#if SUPPORT_WEBPLAYER
			case BuildTarget.WebPlayer:
				ShowString( "Directory", _buildConfig._pWebPlayerOptions._pDirectory, value => { _buildConfig._pWebPlayerOptions._pDirectory = value; } );
				break;
#endif
#if SUPPORT_WEBGL
			case BuildTarget.WebGL:
				ShowString( "Directory", _buildConfig._pWebGLOptions._pDirectory, value => { _buildConfig._pWebGLOptions._pDirectory = value; } );
				break;
#endif
			case BuildTarget.Android:
				ShowString( "Keystore", _buildConfig._pAndroidOptions._pKeystore, value => { _buildConfig._pAndroidOptions._pKeystore = value; } );
				_androidBlockPermsString = EditorGUILayout.TextField( "Block Permissions", _androidBlockPermsString );
				ShowNullable( "Output Gradle Project", _buildConfig._pAndroidOptions._pIsOutputGradleProject, value => { _buildConfig._pAndroidOptions._pIsOutputGradleProject = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				ShowNullable( "Rebuild APK", _buildConfig._pAndroidOptions._pRebuildApk, value => { _buildConfig._pAndroidOptions._pRebuildApk = value; }, 3, ( label, value ) => EditorGUILayout.IntField( label, value ) );
				ShowString( "JDK Version", _buildConfig._pJdkOptions._pVersion, value => { _buildConfig._pJdkOptions._pVersion = value; } );
				ShowString( "JDK Path", _buildConfig._pJdkOptions._pPath, value => { _buildConfig._pJdkOptions._pPath = value; } );
				ShowString( "apktool Path", _buildConfig._pAndroidOptions._pApktoolPath, value => { _buildConfig._pAndroidOptions._pApktoolPath = value; } );
				ShowString( "Build Tools Version", _buildConfig._pAndroidOptions._pBuildToolsVersion, value => { _buildConfig._pAndroidOptions._pBuildToolsVersion = value; } );
				ShowString( "Build Tools Path", _buildConfig._pAndroidOptions._pBuildToolsPath, value => { _buildConfig._pAndroidOptions._pBuildToolsPath = value; } );
				break;
#if UNITY_4				
			case BuildTarget.iPhone:
#else
			case BuildTarget.iOS:
#endif
#if SUPPORT_TVOS
			case BuildTarget.tvOS:
#endif
				ShowString( "Xcode Version", _buildConfig._pIosOptions._pXcodeVersion, value => { _buildConfig._pIosOptions._pXcodeVersion = value; } );
				ShowString( "Xcode App Path", _buildConfig._pIosOptions._pXcodeAppPath, value => { _buildConfig._pIosOptions._pXcodeAppPath = value; } );
				ShowNullable( "Xcode Project Action", _buildConfig._pIosOptions._pXcodeProjectAction, value => { _buildConfig._pIosOptions._pXcodeProjectAction = value; }, 2, ( label, value ) => EditorGUILayout.IntField( label, value ) );
				ShowNullable( "Manual Xcode Build", _buildConfig._pIosOptions._pIsManualXcodeBuild, value => { _buildConfig._pIosOptions._pIsManualXcodeBuild = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				_iosDistributionMethod = (EIosDistributionMethod)EditorGUILayout.EnumPopup( "Distribution Method", _iosDistributionMethod );
				ShowString( "Provisioning Profile", _buildConfig._pIosOptions._pProvisioningProfile, value => { _buildConfig._pIosOptions._pProvisioningProfile = value; } );
#if SUPPORT_ODR
				ShowNullable( "Enable ODR", _buildConfig._pIosOptions._pIsEnableOnDemandResources, value => { _buildConfig._pIosOptions._pIsEnableOnDemandResources = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				if ( AnvilBuild.IsEnableAppleOnDemandResources( _buildConfig ) )
				{
					ShowNullable( "Embed ODR", _buildConfig._pIosOptions._pIsEmbedOnDemandResources, value => { _buildConfig._pIosOptions._pIsEmbedOnDemandResources = value; }, true, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
					ShowString( "ODR URL", _buildConfig._pIosOptions._pOnDemandResourcesBaseUrl, value => { _buildConfig._pIosOptions._pOnDemandResourcesBaseUrl = value; } );
				}
#endif
				ShowString( "Keychain Password", _buildConfig._pIosOptions._pKeychainPassword, value => { _buildConfig._pIosOptions._pKeychainPassword = value; } );
				ShowNullable( "Enable Bitcode", _buildConfig._pIosOptions._pIsEnableBitcode, value => { _buildConfig._pIosOptions._pIsEnableBitcode = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				ShowNullable( "Enable Swift", _buildConfig._pIosOptions._pIsEnableSwift, value => { _buildConfig._pIosOptions._pIsEnableSwift = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				ShowString( "Swift Version", _buildConfig._pIosOptions._pSwiftVersion, value => { 
					_buildConfig._pIosOptions._pSwiftVersion = value;
					if ( !string.IsNullOrEmpty( _buildConfig._pIosOptions._pSwiftVersion ) )
					{
						_buildConfig._pIosOptions._pIsEnableSwift = true;
					}
				} );
				break;
			case BuildTarget.WSAPlayer:
				ShowString( "MSBuild Version", _buildConfig._pUwpOptions._pMSBuildVersion, value => { _buildConfig._pUwpOptions._pMSBuildVersion = value; } );
				ShowString( "MSBuild Path", _buildConfig._pUwpOptions._pMSBuildPath, value => { _buildConfig._pUwpOptions._pMSBuildPath = value; } );
				ShowString( "Windows SDK Version", _buildConfig._pUwpOptions._pWindowsSdkVersion, value => { _buildConfig._pUwpOptions._pWindowsSdkVersion = value; } );
				ShowString( "Windows SDK Tools Path", _buildConfig._pUwpOptions._pWindowsSdkToolsPath, value => { _buildConfig._pUwpOptions._pWindowsSdkToolsPath = value; } );
				ShowNullable( "VS Project Action", _buildConfig._pUwpOptions._pVsProjectAction, value => { _buildConfig._pUwpOptions._pVsProjectAction = value; }, 3, ( label, value ) => EditorGUILayout.IntField( label, value ) );
				ShowString( "Build Configuration", _buildConfig._pUwpOptions._pBuildConfiguration, value => { _buildConfig._pUwpOptions._pBuildConfiguration = value; } );
				ShowString( "Build Platforms", _buildConfig._pUwpOptions._pBuildPlatforms, value => { _buildConfig._pUwpOptions._pBuildPlatforms = value; } );
				ShowNullable( "Save MSBuild Log", _buildConfig._pUwpOptions._pIsSaveMSBuildLog, value => { _buildConfig._pUwpOptions._pIsSaveMSBuildLog = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				ShowString( "Certificate", _buildConfig._pUwpOptions._pCertificate, value => { _buildConfig._pUwpOptions._pCertificate = value; } );
				ShowNullable( "Create Appx Bundle", _buildConfig._pUwpOptions._pIsCreateAppxBundle, value => { _buildConfig._pUwpOptions._pIsCreateAppxBundle = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				ShowNullable( "Store Upload Package", _buildConfig._pUwpOptions._pIsStoreUpload, value => { _buildConfig._pUwpOptions._pIsStoreUpload = value; }, false, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				ShowNullable( "Default Asset Scale", _buildConfig._pUwpOptions._pDefaultAssetScale, value => { _buildConfig._pUwpOptions._pDefaultAssetScale = value; }, 100, ( label, value ) => EditorGUILayout.IntField( label, value ) );
				ShowString( "JDK Version", _buildConfig._pJdkOptions._pVersion, value => { _buildConfig._pJdkOptions._pVersion = value; } );
				ShowString( "JDK Path", _buildConfig._pJdkOptions._pPath, value => { _buildConfig._pJdkOptions._pPath = value; } );
				break;
			case BuildTarget.Switch:
				ShowNullable( "Create ROM", _buildConfig._pSwitchOptions._pIsCreateRom, value => { _buildConfig._pSwitchOptions._pIsCreateRom = value; }, true, ( label, value ) => EditorGUILayout.Toggle( label, value ) );
				break;
		}

		if( GUILayout.Button( "Set Env" ) )
		{
			SetBuildConfigEnv();
		}

		EditorGUILayout.TextArea( _buildConfigJson );

		ShowString( "Build Method", _overrideBuildMethod, value => { _overrideBuildMethod = value; } );

		if( GUILayout.Button( "Build" ) )
		{
			SetBuildConfigEnv();
			if ( TryCallBuildMethod() == true )
			{
				TryIncrementBuildVersion();
				SetBuildConfigEnv();
			}
		}
	}

	private void SetAgentConfigEnv()
	{
		System.Environment.SetEnvironmentVariable( AgentConfig.ENVIRONMENT_VARIABLE, _agentConfigPath );
		ReadAgentConfigJson();
	}

	private void GetAgentConfigEnv()
	{
		_agentConfigPath = System.Environment.GetEnvironmentVariable( AgentConfig.ENVIRONMENT_VARIABLE );
		ReadAgentConfigJson();
	}

	private void ReadAgentConfigJson()
	{
		if ( string.IsNullOrEmpty( _agentConfigPath ) )
		{
			_agentConfigJson = null;
		}

		try
		{
			_agentConfigJson = System.IO.File.ReadAllText( _agentConfigPath );
		}
		catch( System.Exception )
		{
			Debug.LogError( "Failed to read: " + _agentConfigPath );
			_agentConfigJson = null;
		}
	}

	private void SetBuildConfigEnv()
	{
		SetConfigDefines();
		SetConfigAndroidBlockPerms();
		SetIosDistributionMethod();
		_buildConfigJson = _buildConfig.ToJson( null );
		System.Environment.SetEnvironmentVariable( BuildConfig.ENVIRONMENT_VARIABLE, _buildConfigJson );
	}

	private void SetConfigDefines()
	{
		SetConfigListFromCsv( _buildConfig._pDefines, _definesString );
	}

	private void SetConfigAndroidBlockPerms()
	{
		SetConfigListFromCsv( _buildConfig._pAndroidOptions._pBlockPermissions, _androidBlockPermsString );
	}

	private void SetIosDistributionMethod()
	{
		switch ( _iosDistributionMethod )
		{
		case EIosDistributionMethod.DEFAULT:
			_buildConfig._pIosOptions._pDistributionMethod = null;
			break;
		case EIosDistributionMethod.APP_STORE:
			_buildConfig._pIosOptions._pDistributionMethod = AnvilBuild.IOS_DISTRIBUTION_METHOD_APP_STORE;
			break;
		case EIosDistributionMethod.ENTERPRISE:
			_buildConfig._pIosOptions._pDistributionMethod = AnvilBuild.IOS_DISTRIBUTION_METHOD_ENTERPRISE;
			break;
		case EIosDistributionMethod.AD_HOC:
			_buildConfig._pIosOptions._pDistributionMethod = AnvilBuild.IOS_DISTRIBUTION_METHOD_AD_HOC;
			break;
		case EIosDistributionMethod.DEVELOPMENT:
			_buildConfig._pIosOptions._pDistributionMethod = AnvilBuild.IOS_DISTRIBUTION_METHOD_DEVELOPMENT;
			break;
		}
	}

	private void GetBuildConfigEnv()
	{
		_buildConfigJson = System.Environment.GetEnvironmentVariable( BuildConfig.ENVIRONMENT_VARIABLE );
		if ( !string.IsNullOrEmpty( _buildConfigJson ) )
		{
			_buildConfig.FromJson( _buildConfigJson );
		}
		SetConfigDefinesString();
		SetConfigAndroidBlockPermsString();
		SetIosDistributionMethodProxy();
	}

	private void SetConfigDefinesString()
	{
		SetCsvFromConfigList( ref _definesString, _buildConfig._pDefines );
	}

	private void SetConfigAndroidBlockPermsString()
	{
		SetCsvFromConfigList( ref _androidBlockPermsString, _buildConfig._pAndroidOptions._pBlockPermissions );
	}

	private void SetIosDistributionMethodProxy()
	{
		if ( string.IsNullOrEmpty( _buildConfig._pIosOptions._pDistributionMethod ) )
		{
			_iosDistributionMethod = EIosDistributionMethod.DEFAULT;
		}
		else if ( string.Equals( _buildConfig._pIosOptions._pDistributionMethod, AnvilBuild.IOS_DISTRIBUTION_METHOD_APP_STORE, System.StringComparison.OrdinalIgnoreCase ) )
		{
			_iosDistributionMethod = EIosDistributionMethod.APP_STORE;
		}
		else if ( string.Equals( _buildConfig._pIosOptions._pDistributionMethod, AnvilBuild.IOS_DISTRIBUTION_METHOD_ENTERPRISE, System.StringComparison.OrdinalIgnoreCase ) )
		{
			_iosDistributionMethod = EIosDistributionMethod.ENTERPRISE;
		}
		else if ( string.Equals( _buildConfig._pIosOptions._pDistributionMethod, AnvilBuild.IOS_DISTRIBUTION_METHOD_AD_HOC, System.StringComparison.OrdinalIgnoreCase ) )
		{
			_iosDistributionMethod = EIosDistributionMethod.AD_HOC;
		}
		else if ( string.Equals( _buildConfig._pIosOptions._pDistributionMethod, AnvilBuild.IOS_DISTRIBUTION_METHOD_DEVELOPMENT, System.StringComparison.OrdinalIgnoreCase ) )
		{
			_iosDistributionMethod = EIosDistributionMethod.DEVELOPMENT;
		}
		else
		{
			Debug.LogWarning( "Unrecognised distribution method: " + _buildConfig._pIosOptions._pDistributionMethod );
			_iosDistributionMethod = EIosDistributionMethod.DEFAULT;
		}
	}

	private bool TryCallBuildMethod()
	{
		try
		{
			return CallBuildMethod();
		}
		catch ( System.Exception )
		{
			return true;
		}
	}

	private bool CallBuildMethod()
	{
		string method = DEFAULT_BUILD_METHOD;

		if( !string.IsNullOrEmpty( _overrideBuildMethod ) )
		{
			method = _overrideBuildMethod;
		}

		string[] parts = method.Split( '.' );

		System.Type type = null;
		System.Reflection.MethodInfo methodInfo;

		for( int i = 0; i < parts.Length; ++i )
		{
			if( i == parts.Length - 1 )
			{
				methodInfo = type.GetMethod( parts[i] );

				if( methodInfo != null )
				{
					methodInfo.Invoke( null, null );
					return true;
				}
				else
				{
					Debug.LogError( "[AnvilEditor] Failed to find build method: " + method );
					return false;
				}
			}
			else if( i == 0 )
			{
				type = System.Type.GetType( parts[i] );
			}
			else
			{
				type = type.GetNestedType( parts[i] );
			}

			if( type == null )
			{
				Debug.LogError( "[AnvilEditor] Failed to find build method: " + method );
				return false;
			}
		}

		return false;
	}

	private static void TryIncrementBuildVersion()
	{
		if ( _buildConfig != null )
		{
			int	versionNumber;

			if ( int.TryParse( _buildConfig._pBuildVersion, out versionNumber ) && versionNumber >= 0 )
			{
				_buildConfig._pBuildVersion = (versionNumber + 1).ToString();
			}
		}
	}

	private static BuildConfig CreateBuildConfig()
	{
		BuildConfig bc = new BuildConfig();

		bc._pProductCode = "";
		bc._pBuildId = "Internal";
		bc._pBuildVersion = "1";
		bc._pOutputName = "";

		return bc;
	}

	private void ShowNullable<valueT>( string name, valueT? value, System.Action<valueT?> setValue, valueT defaultValue, System.Func<string, valueT, valueT> editorFunc ) where valueT : struct
	{
		if( EditorGUILayout.Toggle( "Set " + name, value.HasValue ) )
		{
			value = editorFunc( name, value.HasValue ? value.Value : defaultValue );
		}
		else
		{
			value = null;
		}

		setValue( value );
	}

	private void ShowString( string name, string value, System.Action<string> setValue )
	{
		if( EditorGUILayout.Toggle( "Set " + name, value != null ) )
		{
			value = EditorGUILayout.TextField( name, value != null ? value : "" );
		}
		else
		{
			value = null;
		}

		setValue( value );
	}

	private static void SetConfigListFromCsv( List<string> list, string csv )
	{
		if ( list == null )
		{
			Debug.LogError( "Null list" );
			return;
		}

		if ( string.IsNullOrEmpty( csv ) )
		{
			list.Clear();
		}
		else
		{
			string[] values = csv.Split( ',', ';', ' ' );

			int i = 0;

			for( i = 0; i < values.Length && i < list.Count; ++i )
			{
				list[i] = values[i];
			}

			for( ; i < values.Length; ++i )
			{
				list.Add( values[i] );
			}

			if( list.Count > i )
			{
				list.RemoveRange( i, list.Count - i );
			}
		}
	}

	private static void SetCsvFromConfigList( ref string csv, List<string> list )
	{
		csv = string.Join( ";", list.ToArray() );
	}

}
