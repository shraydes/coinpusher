﻿////////////////////////////////////////////
// 
// JobManager.cs
//
// Created: 23/05/2014 ccuthbert
// Contributors: 
// 
// Intention:
// Manages a set of jobs.  
// Starts each job taking into account its 
// dependencies.
// Signals on complete of all jobs.
//
// Note: 
// Initially implemented to extend 
// functionality of FlowSceneLoader, and to 
// improve InitialisationFacade.
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using System.Collections.Generic;
using Logger = NoDebugLogger;

public class JobManager
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[JobManager] ";
	
	private enum EJobState
	{
		NULL = 0,
		WAITING_TO_START,
		RUNNING,
		WAITING_TO_FINISH,
		FINISHED
	}
	
	//
	// NESTED CLASSES / STRUCTS
	//
	
	public interface IOwner
	{
		string		_pName			{ get; }
		bool		_pCanStartJobs	{ get; }
	}
	
	public interface IJob
	{
		string		_pName			{ get; }
		bool		Start();
		void		Update();
		bool		_pIsFinished	{ get; }
	}
	
	private class JobData
	{
		public EJobState	_state;
		
		public List<JobData>	_dependencies;
		
		public JobData()
		{
			_state = EJobState.NULL;
			_dependencies = new List<JobData>();
		}
		
		public bool _pHasUnfinishedDependencies
		{
			get
			{
				return _dependencies != null && _dependencies.Count > 0 && _dependencies.Exists( d => d._state != EJobState.FINISHED );
			}
		}
	}
	
	//
	// MEMBERS 
	//
	
	private IOwner	_owner;
	
	private List<IJob>	_jobs;
	
	private Dictionary<IJob, JobData>	_jobDataDict;
	
	private System.Action<IJob>	_onJobStarting;
	
	private System.Action<IJob>	_onJobStarted;
	
	private System.Action<IJob>	_onJobFinished;
	
	private System.Action	_onAllJobsFinished;
	
	private int	_numUnfinishedJobs;
	
	//
	// PROPERTIES
	//
	
	private string	_pOwnerName	{ get { return _owner != null ? _owner._pName : "[no owner]"; } }
	
	private string	_pLogTag	{ get { return "[JobManager:" + _pOwnerName + "] "; } }
	
	public event System.Action<IJob>	_pOnJobStarting	{ add { _onJobStarting += value; }	remove { _onJobStarting -= value; } }
	
	public event System.Action<IJob>	_pOnJobStarted	{ add { _onJobStarted += value; }	remove { _onJobStarted -= value; } }
	
	public event System.Action<IJob>	_pOnJobFinished	{ add { _onJobFinished += value; }	remove { _onJobFinished -= value; } }
	
	public event System.Action	_pOnAllJobsFinished	{ add { _onAllJobsFinished += value; }	remove { _onAllJobsFinished -= value; } }
	
	public bool	_pHasAllJobsFinished
	{
		get
		{
			return _numUnfinishedJobs == 0;
		}
	}
	
	//
	// ABSTRACTS / VIRTUALS
	//
	
	//
	// STATIC FUNCTIONS
	//
	
	//
	// FUNCTIONS 
	//
	
	// ** PUBLIC
	//
	
	public JobManager( IOwner owner )
	{
		_owner = owner;
		_jobs = new List<IJob>();
		_jobDataDict = new Dictionary<IJob, JobData>();
		_onJobStarting = null;
		_onJobStarted = null;
		_onJobFinished = null;
		_onAllJobsFinished = null;
		_numUnfinishedJobs = 0;
	}
	
	public void RegisterJob( IJob job, bool enable )
	{
		if ( _jobs == null )
		{
			Logger.LogError( "No jobs list" );
			return;
		}
		
		if ( job == null )
		{
			Logger.LogError( "Null job" );
			return;
		}
		
		if ( !_jobs.Contains( job ) )
		{
			_jobs.Add( job );
			_jobDataDict[ job ] = new JobData();
		}
		
		if ( enable )
		{
			EnableJob( job );
		}
	}
	
	public void UnregisterJob( IJob job )
	{
		if ( job == null ) return;
		
		RemoveDependency( job );
		
		if ( _jobs != null && _jobs.Contains( job ) )
		{
			_jobs.Remove( job );
		}
		
		if ( _jobDataDict != null && _jobDataDict.ContainsKey( job ) )
		{
			_jobDataDict.Remove( job );
		}
	}
	
	public void AddDependency( IJob job, IJob dep )
	{
		if ( job == null )
		{
			Logger.LogError( "Null job" );
			return;
		}
		
		if ( dep == null )
		{
			Logger.LogError( "Null dependency" );
			return;
		}
		
		JobData	jd = FindJobData( job );
		
		if ( jd == null )
		{
			Logger.LogError( "Unregistered job" );
			return;
		}
		
		JobData	jd2 = FindJobData( dep );
		
		if ( jd2 == null )
		{
			RegisterJob( dep, false );
			jd2 = FindJobData( dep );
		}
		
		if ( jd2 == null )
		{
			Logger.LogError( "Unregistered dependency" );
			return;
		}
		
		if ( jd._dependencies.Contains( jd2 ) )
		{
			Logger.LogError( "Already added dependency" );
			return;
		}
		
		jd._dependencies.Add( jd2 );
	}
	
	public void RemoveDependency( IJob job, IJob dep )
	{
		if ( job == null )
		{
			Logger.LogError( "Null job" );
			return;
		}
		
		if ( dep == null )
		{
			Logger.LogError( "Null dependency" );
			return;
		}
		
		JobData	jd = FindJobData( job );
		
		if ( jd == null )
		{
			Logger.LogError( "Unregistered job" );
			return;
		}
		
		JobData	jd2 = FindJobData( dep );
		
		if ( jd2 == null )
		{
			Logger.LogError( "Unregistered dependency" );
			return;
		}
		
		if ( !jd._dependencies.Contains( jd2 ) ) return;
		
		jd._dependencies.Remove( jd2 );
	}
	
	public void EnableJob( IJob job )
	{
		if ( job == null )
		{
			Logger.LogError( "Null job" );
			return;
		}
		
		JobData	jd = FindJobData( job );
		
		if ( jd == null )
		{
			Logger.LogError( "Unregistered job '" + job._pName + "'" );
			return;
		}
		
		if ( jd._state != EJobState.NULL )
		{
			Logger.LogError( "Job '" + job._pName + "' already enabled" );
			return;
		}
		
		jd._state = EJobState.WAITING_TO_START;
		
		//CheckStartJob( job, ref jd );
	}
	
	public void Update()
	{
		UpdateJobs();
	}
	
	// ** PROTECTED
	//
	
	// ** PRIVATE
	//
	
	private JobManager()
	{
	}
	
	private JobData FindJobData( IJob job )
	{
		return _jobDataDict != null && _jobDataDict.ContainsKey( job ) ? _jobDataDict[ job ] : null;
	}
	
	private void RemoveDependency( IJob dep )
	{
		if ( _jobs == null ) return;
		
		for ( int i = 0; i < _jobs.Count; ++i )
		{
			if ( _jobs[i] != null && _jobs[i] != dep )
			{
				RemoveDependency( _jobs[i], dep );
			}
		}
	}
	
	private void CheckStartJob( IJob job, ref JobData jd )
	{
		if ( _owner != null && !_owner._pCanStartJobs ) return;
		
		if ( jd == null )
		{
			jd = FindJobData( job );
			if ( jd == null ) return;
		}
		
		if ( CanJobStart( job, ref jd ) == false ) return;
		
		if ( _onJobStarting != null )
		{
			_onJobStarting( job );
		}
		
		if ( job.Start() )
		{
			jd._state = EJobState.RUNNING;
			
			++_numUnfinishedJobs;
			
			if ( _onJobStarted != null )
			{
				_onJobStarted( job );
			}
		}
	}

	private void CheckFinishJob( IJob job, ref JobData jd )
	{
		if ( jd == null )
		{
			jd = FindJobData( job );
			if ( jd == null ) return;
		}
		
		if ( CanJobFinish( job, ref jd ) )
		{
			jd._state = EJobState.FINISHED;
			
			if ( _onJobFinished != null )
			{
				_onJobFinished( job );
			}
		}
		else if ( jd._state != EJobState.WAITING_TO_FINISH )
		{
			jd._state = EJobState.WAITING_TO_FINISH;
		}
	}
	
	private bool CanJobStart( IJob job, ref JobData jd )
	{
		if ( jd == null )
		{
			jd = FindJobData( job );
			if ( jd == null ) return false;
		}
		
		if ( jd._state != EJobState.WAITING_TO_START ) return false;
		
		return !jd._pHasUnfinishedDependencies;
	}
	
	private bool CanJobFinish( IJob job, ref JobData jd )
	{
		if ( jd == null )
		{
			jd = FindJobData( job );
			if ( jd == null ) return false;
		}
		
		if ( jd._state != EJobState.RUNNING && jd._state != EJobState.WAITING_TO_FINISH ) return false;
		
		return !jd._pHasUnfinishedDependencies;
	}
	
	private void UpdateJob( IJob job, ref JobData jd )
	{
		if ( jd == null )
		{
			jd = FindJobData( job );
			if ( jd == null ) return;
		}

		CheckStartJob( job, ref jd );

		if ( jd._state == EJobState.WAITING_TO_START ) return;
		if ( jd._state == EJobState.FINISHED ) return;

		if ( jd._state == EJobState.RUNNING )
		{
			job.Update();
		
			if ( job._pIsFinished )
			{
				CheckFinishJob( job, ref jd );
			}
		}
		else if ( jd._state == EJobState.WAITING_TO_FINISH )
		{
			CheckFinishJob( job, ref jd );
		}
	}
	
	private void UpdateJobs()
	{

		int	oldUnfinishedJobs = _numUnfinishedJobs;
		int	newUnfinishedJobs = 0;
		JobData	jd;

		//PersonalLogs.BobLog( "Num unfinished jobs [" + oldUnfinishedJobs + "]" );

		for ( int i = 0; i < _jobs.Count; ++i )
		{
			jd = FindJobData( _jobs[i] );

			if ( jd == null ) continue;
			if ( jd._state == EJobState.FINISHED ) continue;

	//		Profiler.BeginSample("UpdateJob" + _jobs[i]._pName);

			UpdateJob( _jobs[i], ref jd );

	//		Profiler.EndSample();
			
			if ( jd._state != EJobState.FINISHED )
			{
				++newUnfinishedJobs;
			}
		}
		
		_numUnfinishedJobs = newUnfinishedJobs;
		
		if ( newUnfinishedJobs == 0 && oldUnfinishedJobs != 0 )
		{
			if ( _onAllJobsFinished != null )
			{
				_onAllJobsFinished();
			}
		}

	}
	
	
	//
	// OPERATOR OVERLOADS
	//
	
	
}


