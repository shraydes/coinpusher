﻿using UnityEngine;
using System.Collections.Generic;

public enum InitialisationState
{
	WAITING_TO_START,
	INITIALISING,
	FINISHED
}

public class InitialisationObject : MonoBehaviour, JobManager.IJob
{
	// Objects are initaliased in ascending order.
	public List<InitialisationObject>	_initDepends;
	
	protected InitialisationState _currentState = InitialisationState.WAITING_TO_START;
	
	public InitialisationState	_pInitialisationState	{ get { return _currentState; } }

//	public float _pFinishedTime { get { return _finishedTime; } set { _finishedTime = value;  }  }
//	float _finishedTime;

	protected virtual void Awake()
	{	
		InitialisationFacade._pInstance.addToQueue( this );
		_currentState = InitialisationState.WAITING_TO_START;
	}
	
	protected virtual void OnDestroy()
	{
		_currentState = InitialisationState.FINISHED;
		if ( InitialisationFacade._pExists )
		{
			InitialisationFacade._pInstance.removeFromQueue( this );
		}
	}
	
	
	public virtual void startInitialising() {}

	public virtual InitialisationState updateInitialisation() { return _currentState; }

	public void AddInitialisationDependency( InitialisationObject newDep )
	{
		if ( _currentState == InitialisationState.FINISHED )
		{
			Debug.LogWarning( "[InitialisationObject] Adding dependency: New dependency will have no effect because initialisaion has already finished" );
		}

		if ( !_initDepends.Contains( newDep ) )
		{
			_initDepends.Add( newDep );
		}
	}

#if DEBUG_INITIALISATION_QUEUE
	public void DebugForceFinishInitialisation()
	{
		_currentState = InitialisationState.FINISHED;
	}
#endif
	
	
	string JobManager.IJob._pName	{ get { return this.name; } }
	
	bool JobManager.IJob.Start()
	{
		startInitialising();
		return _currentState != InitialisationState.WAITING_TO_START;
	}
	
	void JobManager.IJob.Update()
	{
		_currentState = updateInitialisation();
	}
	
	bool JobManager.IJob._pIsFinished	{ get { return _currentState == InitialisationState.FINISHED; } }
}
