﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(InitialisationObject), true)]
public class InitialisationObjectInspector : Editor
{
	private InitialisationObject	_object;
	
	private InitialisationObject[]	_dependents;
	
	private bool	_isDependentsFoldOut = false;
	
	protected virtual void OnEnable()
	{
		_object = target as InitialisationObject;
		RefreshDependents();
	}
	
	public override void OnInspectorGUI()
	{
		if ( _object == null ) return;
		
		DrawDefaultInspector();
		
		EditorGUILayout.Separator();
		EditorGUILayout.LabelField( "--------Initialisation Info--------" );
		
		bool	newFoldOut = EditorGUILayout.Foldout( _isDependentsFoldOut, "Dependents" );
		
		if ( newFoldOut )
		{
			if ( !_isDependentsFoldOut )
			{
				RefreshDependents();
			}
			
			foreach ( InitialisationObject d in _dependents )
			{
				EditorGUILayout.ObjectField( d, d.GetType(), true );
			}
		}
		
		_isDependentsFoldOut = newFoldOut;
		
		if ( Application.isPlaying )
		{
			Color	oldColor = GUI.color;
			
			switch ( _object._pInitialisationState )
			{
			case InitialisationState.WAITING_TO_START:
				GUI.color = Color.red;
				EditorGUILayout.LabelField( "Waiting to start" );
				break;
			case InitialisationState.INITIALISING:
				GUI.color = new Color( 1f, 0.5f, 0f );
				EditorGUILayout.LabelField( "Initialising" );
				break;
			case InitialisationState.FINISHED:
				GUI.color = Color.green;
				EditorGUILayout.LabelField( "Finished" );
				break;
			}
			
			GUI.color = oldColor;
		}
		
		if ( GUI.changed )
		{
			EditorUtility.SetDirty( _object );
		}
	}
	
	private void RefreshDependents()
	{
		if ( _object == null )
		{
			_dependents = new InitialisationObject[0];
			return;
		}
		
		InitialisationObject[]	allObjs = Object.FindObjectsOfType<InitialisationObject>();
		List<InitialisationObject>	depObjs = new List<InitialisationObject>();
		
		foreach ( InitialisationObject o in allObjs )
		{
			if ( o == null ) continue;
			if ( o == _object ) continue;
			if ( o._initDepends == null ) continue;
			if ( o._initDepends.IndexOf(_object ) >= 0 )
			{
				depObjs.Add( o );
			}
		}
		
		_dependents = depObjs.ToArray();
	}
}
