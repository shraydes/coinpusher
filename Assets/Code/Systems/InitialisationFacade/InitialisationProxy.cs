﻿////////////////////////////////////////////
// 
// InitialisationProxy.cs
//
// Created: 21/07/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class InitialisationProxy : InitialisationObject
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[InitialisationProxy] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	public interface ITarget
	{
		InitialisationState	_pInitialisationState { get; set; }
		void				OnStartInitialising();
		InitialisationState	OnUpdateInitialising();
	}

	//
	// MEMBERS 
	//
	
	[SerializeField]
	private MonoBehaviour	_targetScript;

	private ITarget			_target;

	//
	// PROPERTIES
	//

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	protected override void Awake()
	{
		if ( _targetScript != null )
		{
			_target = _targetScript as ITarget;

			if ( _target == null )
			{
				Debug.LogError( LOG_TAG + "Target script does not implement ITarget interface" );
				return;
			}
		}

		if ( _target != null )
		{
			_target._pInitialisationState = InitialisationState.WAITING_TO_START;
		}

		base.Awake();
	}

	protected override void OnDestroy()
	{
		if ( _target != null )
		{
			_target._pInitialisationState = InitialisationState.FINISHED;
		}

		base.OnDestroy();
	}

	// ** PUBLIC
	//

	public override void startInitialising()
	{
		if ( _target != null )
		{
			_target.OnStartInitialising();
			_currentState = _target._pInitialisationState;
		}
	}

	public override InitialisationState updateInitialisation()
	{
		if ( _target != null )
		{
			_currentState = _target.OnUpdateInitialising();
		}
		return _currentState;
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

