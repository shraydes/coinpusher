﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InitialisationFacade : AmuzoMonoSingleton<InitialisationFacade>, JobManager.IOwner
{
	[SerializeField]
	private bool	_orderByNumberPrefix = false;

	#if WANT_DEBUG_LOGS
	const bool DO_DEBUG = true;
	#else
	const bool DO_DEBUG = false;
	#endif

	float _queueStartTime = 0f;

	private JobManager	_jobManager;
	
	private List<InitialisationObject>	_objects;
	
	private System.Action	_onFinished;
	
	public event System.Action	_pOnFinished
	{
		add
		{
			_onFinished += value;
		}
		remove
		{
			_onFinished -= value;
		}
	}
	
	public bool	_pHasFinished
	{
		get
		{
			return _jobManager == null || _jobManager._pHasAllJobsFinished;
		}
	}
	
	protected override void OnInitialise()
	{
		base.OnInitialise();

		init();
	}

	void Start () 
	{
		_queueStartTime = Time.realtimeSinceStartup;

		Log( "Checking for un-assigned children.");
		InitialisationObject childScript;
		for( int x = 0; x < transform.childCount; x++ )
		{
			childScript = transform.GetChild( x ).gameObject.GetComponent<InitialisationObject>();

			if( childScript == null )
			{
				continue;
			}

			addToQueue( childScript );
		}
	}

	void Update () 
	{
		if ( _jobManager != null )
		{
			_jobManager.Update();
		}
	}

	public void addToQueue( InitialisationObject obj )
	{
		if ( obj == null ) return;
		
		if ( _objects != null && !_objects.Contains( obj ) )
		{
			_objects.Add( obj );

			if ( _orderByNumberPrefix )
			{
				DoOrderByNumberPrefix();
			}
		}
		
		if ( _jobManager != null )
		{
			Log( "Added new init job [" + obj.name + "]");
			_jobManager.RegisterJob( obj, false );
			
			if ( obj._initDepends != null )
			{
				foreach ( var dep in obj._initDepends )
				{
					if ( dep != null )
					{
						_jobManager.AddDependency( obj, dep );
					}
				}
			}
			
			_jobManager.EnableJob( obj );
		}
	}
	
	public void removeFromQueue( InitialisationObject obj )
	{
		if ( obj == null ) return;
		
		if ( _jobManager != null )
		{
			_jobManager.UnregisterJob( obj );
		}
		
		if ( _objects != null && _objects.Contains( obj ) )
		{
			_objects.Remove( obj );
		}
	}

	void init()
	{
		_jobManager = new JobManager( this );
		_jobManager._pOnJobStarting += job => {
			Log( "Starting job '" + job._pName + "'" );
		};
		_jobManager._pOnJobStarted += job => {
			Log( "Job '" + job._pName + "' started" );
		};
		_jobManager._pOnJobFinished += job => {
			Log( "Job '" + job._pName + "' finished" );
		};
		_jobManager._pOnAllJobsFinished += () => {
			Log( "All jobs finished. Time taken: [" + ( Time.realtimeSinceStartup - _queueStartTime ) + "] secs." );
			if ( _onFinished != null )
			{
				_onFinished();
				_onFinished = null;
			}
		};
		_objects = new List<InitialisationObject>();
	}

	private void DoOrderByNumberPrefix()
	{
		if ( _objects == null ) return;

		//sort the object list with non-numerics first
		_objects.Sort( ( a, b ) => {
			string	nameA = a.gameObject.name;
			string	nameB = b.gameObject.name;
			int		nameLenA = nameA.Length;
			int		nameLenB = nameB.Length;
			bool	isNumericA = nameLenA > 0 && nameA[0] >= '0' && nameA[0] <= '9';
			bool	isNumericB = nameLenB > 0 && nameB[0] >= '0' && nameB[0] <= '9';
			if ( !isNumericA )
			{
				if ( isNumericB )
				{
					return -1;
				}
				else
				{
					return 0;
				}
			}
			else if( !isNumericB )
			{
				return 1;
			}
			int	numA, numB;
			int	charIdxA = 0;
			int	charIdxB = 0;
			while( true )
			{
				numA = 0;
				for ( ; charIdxA < nameLenA && nameA[ charIdxA ] >= '0' && nameA[ charIdxA ] <= '9'; ++charIdxA )
				{
					numA *= 10;
					numA += nameA[ charIdxA ] - '0';
				}
				numB = 0;
				for ( ; charIdxB < nameLenB && nameB[ charIdxB ] >= '0' && nameB[ charIdxB ] <= '9'; ++charIdxB )
				{
					numB *= 10;
					numB += nameB[ charIdxB ] - '0';
				}
				if ( numA != numB )
				{
					return numA.CompareTo( numB );
				}
				if ( charIdxA < nameLenA && nameA[ charIdxA ] == '.' )
				{
					++charIdxA;
					if ( charIdxB < nameLenB && nameB[ charIdxB ] == '.' )
					{
						++charIdxB;
					}
				}
				else
				{
					if ( charIdxB < nameLenB && nameB[ charIdxB ] == '.' )
					{
						++charIdxB;
					}
					else
					{
						return 0;
					}
				}
			}
		} );

		//iterate the sorted object list, setting dependencies on previous numeric-prefixed object
		bool	isAddingDependencies = false;
		string	name;
		for ( int i = 0; i < _objects.Count; ++i )
		{
			if ( isAddingDependencies )
			{
				if ( !_objects[i]._initDepends.Contains( _objects[i-1] ) )
				{
					_objects[i]._initDepends.Add( _objects[i-1] );
				}
			}
			else
			{
				name = _objects[i].gameObject.name;
				if ( name.Length > 0 && name[0] >= '0' && name[0] <= '9' )
				{
					isAddingDependencies = true;
				}
			}
		}
	}

	string JobManager.IOwner._pName	{ get { return "InitialisationFacade"; } }
	
	bool JobManager.IOwner._pCanStartJobs	{ get { return _queueStartTime > 0f; } }
	
	
	// ********************* Debugging *************************
	
	public static void Log(string message, UnityEngine.Object o = null)
	{
		if( !DO_DEBUG )
		{
			#pragma warning disable 0162		
			return;
			#pragma warning restore 0162
		}

		#pragma warning disable 0162		
#if UNITY_EDITOR
		Debug.Log ("<color=cyan><b>InitFacade: </b></color>"+message, o);
#else
		Debug.Log ("InitFacade: "+message, o);
#endif
		#pragma warning restore 0162
	}
}
