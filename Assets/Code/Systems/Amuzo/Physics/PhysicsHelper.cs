using System;
using System.Collections.Generic;
using UnityEngine;

namespace AmuzoPhysics
{

public static class PhysicsHelper 
{
	public static float CalculateRequiredImpulse( float invMassA, float invMassB, float velA, float velB, float wantSpeedDelta )
	{
		return ( wantSpeedDelta - (velA - velB) ) / (invMassA + invMassB);
	}

	public static float CalculateCollisionImpulse( float invMassA, float invMassB, float velA, float velB, float bounce )
	{
		return CalculateRequiredImpulse( invMassA, invMassB, velA, velB, -bounce * (velA - velB) );
	}
	
	public static float CalculateBrakingImpulse( float invMassA, float invMassB, float velA, float velB, float maxImpulse )
	{
		float	impulse = CalculateRequiredImpulse( invMassA, invMassB, velA, velB, 0.0f );
		
		return Mathf.Clamp( impulse, -maxImpulse, maxImpulse );
	}
	
	public static float CalculateFrictionImpulse( float invMassA, float invMassB, float velA, float velB, float normalImpulse, float frictionCoeff )
	{
		return CalculateBrakingImpulse( invMassA, invMassB, velA, velB, Mathf.Abs( frictionCoeff * normalImpulse ) );
	}

	public static bool DoBoundsCollisionTest( Bounds boundsA, Bounds boundsB, out Vector3 normal, out float penetration )
	{
		var	minA = boundsA.min;
		var	maxA = boundsA.max;
		var	minB = boundsB.min;
		var	maxB = boundsB.max;
		
		var	upPen = maxB.y - minA.y;
		var	downPen = maxA.y - minB.y;
		var	leftPen = maxA.x - minB.x;
		var	rightPen = maxB.x - minA.x;
		var	forwardPen = maxB.z - minA.z;
		var	backwardPen = maxA.z - minB.z;
		
		normal = Vector3.zero;
		penetration = Mathf.Min( upPen, downPen, leftPen, rightPen, forwardPen, backwardPen );
		
		if ( penetration == upPen )
		{
			normal = Vector3.up;
		}
		else
		if ( penetration == downPen )
		{
			normal = -Vector3.up;
		}
		else
		if ( penetration == leftPen )
		{
			normal = -Vector3.right;
		}
		else
		if ( penetration == rightPen )
		{
			normal = Vector3.right;
		}
		else
		if ( penetration == forwardPen )
		{
			normal = Vector3.forward;
		}
		else
		if ( penetration == backwardPen )
		{
			normal = -Vector3.forward;
		}
		
		return penetration >= 0.0f;
	}

	public static bool DoRayCircleTest( Vector2 rayStart, Vector2 rayDir, Vector2 circlePos, float circleRadius, out float rayHitDist )
	{
		Vector2	relPos = rayStart - circlePos;

		float	r1, r2;

		bool	hasSolution = MathHelper.SolveQuadratic( 
			rayDir.sqrMagnitude, 
			2f * Vector2.Dot( relPos, rayDir ),
			relPos.sqrMagnitude - circleRadius * circleRadius, 
			out r1, out r2 );

		if ( !hasSolution )
		{
			rayHitDist = 0f;
			return false;
		}

		rayHitDist = Mathf.Min( r1, r2 );

		if ( rayHitDist < -circleRadius ) return false;
		if ( rayHitDist < 0f ) rayHitDist = 0f;

		return true;
	}

		

	public static void GetJointsAndTheirRigidbodies(out List<Joint> joints, out List<Rigidbody> rigidbodies, int layerMask = ~0)
	{
		joints = new List<Joint>(UnityEngine.Object.FindObjectsOfType<Joint>());
		rigidbodies = new List<Rigidbody>();
		rigidbodies.SetCount(joints.Count);
		
		for (int i = joints.Count - 1; i >= 0; --i)
		{
			// Remove any joints not in the layer mask
			if ((1 << joints[i].gameObject.layer & layerMask) == 0) {
				joints.RemoveAt(i);
				rigidbodies.RemoveAt(i);
				continue;
			}

			rigidbodies[i] = joints[i].GetComponent<Rigidbody>();
		}
	}
}

}
