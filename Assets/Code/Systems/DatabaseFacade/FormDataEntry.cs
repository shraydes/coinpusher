﻿using UnityEngine;
using System.Collections;

public class FormDataEntry
{
	public string
		key,
		value;
	
	public bool 
		doEncrypt = false;
	
	public FormDataEntry( string key, string value, bool doEncrypt = true )
	{
		this.key = key;
		this.value = value;
		this.doEncrypt = doEncrypt;
		
		if( value == null )
		{
			Debug.LogError( "FormDataEntry created with NULL value. Key [" + key + "]");
		}
	}
}
