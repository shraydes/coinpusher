﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[System.Reflection.Obfuscation( Exclude = true, ApplyToMembers = true )]
public class BasicEncryption : MonoBehaviour
{
	private static string keyDefault = "emjrwtbadozhykng";
	private static string bases = "0123456789ABCDEF";

	public delegate void ReturnFunction( string returnString );
	ReturnFunction returnOnEncryptionComplete;
	ReturnFunction returnOnDecryptionComplete;

	static BasicEncryption instance;

	public static int
		CHARACTER_THRESHOLD = 200;

	private bool	_isBusy = false;
	
	public static BasicEncryption Instance
	{
		get
		{
			if( instance == null )
			{
				GameObject newObject = new GameObject( "BasicEncryption" );
				DontDestroyOnLoad( newObject );
				instance = newObject.AddComponent<BasicEncryption>();
			}

			return instance;
		}
	}

	public static string EncryptImmediateFast( string input, string keyOverride = null )
	{
		//Console.WriteLine("Encrypting");
//		float	t = Time.realtimeSinceStartup;
		
		// Pass 1 - Make a new string of hex values (padded to 4 digits each) for each character in string
		char[]	buffer1 = new char[ input.Length << 2 ];
		int		charCount1 = 0;
		string	s;

		for( int idx = 0; idx < input.Length; idx++ )
		{
            s = GetEncryptedHexString(input, idx);
			s.CopyTo( 0, buffer1, charCount1, 4 );
			charCount1 += 4;
		}

		//Console.WriteLine("1: " + eS);

		// Pass 2 - Add a random hex character every 3 characters
		System.Random random = new System.Random();
		char[]	buffer2 = new char[ (buffer1.Length << 2) / 3 ];
		int		charCount2 = 0;
		for (int idx = 0, c = 0; idx < charCount1; idx++)
		{
            if (c >= 2)
			{
				buffer2[ charCount2++ ] = bases[ (int)random.Next(16) ];
				buffer2[ charCount2++ ] = buffer1[ idx ];
				c = 0;
			}
			else
			{
				buffer2[ charCount2++ ] = buffer1[ idx ];
				c++;
			}
		}

		//Console.WriteLine("2: " + eS2);

		// Pass 3 - Reverse everything		
		int		halfCharCount = charCount2 >> 1;
		char	swap;
		for (int idx = 0; idx < halfCharCount; idx++)
		{
			swap = buffer2[ idx ];
			buffer2[ idx ] = buffer2[ charCount2 - idx - 1];
			buffer2[ charCount2 - idx - 1] = swap;
		}

		//Console.WriteLine("3: " + eS3);

		string	keyUsed = keyOverride ?? keyDefault;

		// Pass 4 - Lock chars using private key
		for (int idx = 0; idx < charCount2; idx++)
		{
			for( int y = 0; y < bases.Length; y++ )
			{
				if (bases[y] == buffer2[idx])
				{
					buffer2[idx] = keyUsed[y];
				}
				}
			}

//		Debug.Log( "[BasicEncryption] EncryptImmediate took " + ((Time.realtimeSinceStartup - t)*1000f).ToString( "F0" ) + "ms to encrypt " + input.Length + " chars" );

		return new string( buffer2, 0, charCount2 );
	}

	public string encryptImmediate( string input )
	{
		//Console.WriteLine("Encrypting");
		// Pass 1 - Make a new string of hex values (padded to 4 digits each) for each character in string
		string eS = "";

		for( int idx = 0; idx < input.Length; idx++ )
		{
			eS += GetEncryptedHexString( input, idx );
		}

		//Console.WriteLine("1: " + eS);

		// Pass 2 - Add a random hex character every 3 characters
		System.Random random = new System.Random();
		string eS2 = "";
		int c = 0;

		for( int idx = 0; idx < eS.Length; idx++ )
		{
			c++;
			if( c++ > 3 )
			{
				eS2 += ( ( int )random.Next( 16 ) ).ToString( "X" );
				eS2 += eS[idx];
				c = 0;
			}
			else
			{
				eS2 += eS[idx];
			}
		}

		//Console.WriteLine("2: " + eS2);

		// Pass 3 - Reverse everything		
		string eS3 = "";

		for( int idx = 0; idx < eS2.Length; idx++ )
		{
			eS3 += eS2[eS2.Length - idx - 1];

		}

		//Console.WriteLine("3: " + eS3);

		// Pass 4 - Lock chars using private key
		string eS4 = "";

		for( int idx = 0; idx < eS3.Length; idx++ )
		{
			for( int y = 0; y < bases.Length; y++ )
			{
				if( bases[y] == eS3[idx] )
				{
					eS4 += keyDefault[y];
				}
			}

		}

		//Console.WriteLine("4: " + eS4);
		return eS4;
	}

	private static IEnumerator Encrypt( string input )
	{
		//Console.WriteLine("Encrypting");
		// Pass 1 - Make a new string of hex values (padded to 4 digits each) for each character in string
		string eS = "";

		for( int idx = 0; idx < input.Length; idx++ )
		{
			eS += GetEncryptedHexString( input, idx );
			if( idx % CHARACTER_THRESHOLD == 0 )
				yield return new WaitForSeconds( 0 );
		}

		//Console.WriteLine("1: " + eS);

		// Pass 2 - Add a random hex character every 3 characters
		System.Random random = new System.Random();
		string eS2 = "";
		int c = 0;

		for( int idx = 0; idx < eS.Length; idx++ )
		{
			c++;
			if( c++ > 3 )
			{
				eS2 += ( ( int )random.Next( 16 ) ).ToString( "X" );
				eS2 += eS[idx];
				c = 0;
			}
			else
			{
				eS2 += eS[idx];
			}

			if( idx % CHARACTER_THRESHOLD == 0 )
				yield return new WaitForSeconds( 0 );
		}

		//Console.WriteLine("2: " + eS2);

		// Pass 3 - Reverse everything		
		string eS3 = "";

		for( int idx = 0; idx < eS2.Length; idx++ )
		{
			eS3 += eS2[eS2.Length - idx - 1];

			if( idx % CHARACTER_THRESHOLD == 0 )
				yield return new WaitForSeconds( 0 );
		}

		//Console.WriteLine("3: " + eS3);

		// Pass 4 - Lock chars using private key
		string eS4 = "";

		for( int idx = 0; idx < eS3.Length; idx++ )
		{
			for( int y = 0; y < bases.Length; y++ )
			{
				if( bases[y] == eS3[idx] )
				{
					eS4 += keyDefault[y];
				}
			}

			if( idx % CHARACTER_THRESHOLD == 0 )
				yield return new WaitForSeconds( 0 );
		}

		//Console.WriteLine("4: " + eS4);

		BasicEncryption.Instance.encrpytionComplete( eS4 );
	}

	void encrpytionComplete( string returnString )
	{
		if( returnOnEncryptionComplete != null )
			returnOnEncryptionComplete( returnString );
	}

	public static string DecryptImmediateFast( string input, string keyOverride = null )
	{
//		float	t = Time.realtimeSinceStartup;
		
		//Debug.Log("Decrypting [" + input + "]");
		// Pass 4 - Unlock chars using private key
		char[]	buffer1 = input.ToCharArray();
		int		charCount1 = buffer1.Length;
		
		string	keyUsed = keyOverride ?? keyDefault;

		for (int idx = 0; idx < charCount1; idx++)
		{
			for (int y = 0; y < keyUsed.Length; y++)
			{
				if (keyUsed[y] == buffer1[idx])
				{
					buffer1[idx] = bases[y];
				}
			}
		}
		
		// Pass 3 - Unreverse everything
		int		halfCharCount = charCount1 >> 1;
		char	swap;
		for (int idx = 0; idx < halfCharCount; idx++)
		{
			swap = buffer1[ idx ];
			buffer1[ idx ] = buffer1[ charCount1 - idx - 1];
			buffer1[ charCount1 - idx - 1] = swap;
		}
		
		// Pass 2 - Remove random hex character every 3 characters
		// Note: (The first random hex character is removed after the second character instead)
		char[]	buffer2 = new char[ charCount1 ];
		int		charCount2 = 0;
		for (int idx = 0, c = 1; idx < charCount1; idx++)
		{
			if (c >= 3)
			{
				c = 0;
			}
			else
			{
				buffer2[ charCount2++ ] = buffer1[ idx ];
				c++;
			}
		}
		
		// Pass 1 - Make a new string of hex values (padded to 4 digits each) for each character in string
		charCount1 = 0;
		for (int idx = 0; idx < charCount2; idx += 4)
		{
			buffer1[ charCount1++ ] = GetDecryptedHexString2( buffer2, charCount2, idx );
		}
		
//		Debug.Log( "[BasicEncryption] DecryptImmediate took " + ((Time.realtimeSinceStartup - t)*1000f).ToString( "F0" ) + "ms to decrypt " + input.Length + " chars" );
		
		return new string( buffer1, 0, charCount1 );
	}
	
	private static IEnumerator Decrypt( string input )
	{
		//Debug.Log("Decrypting [" + input + "]");
		// Pass 4 - Unlock chars using private key
		string eS4 = "";

		for( int idx = 0; idx < input.Length; idx++ )
		{
			for( int y = 0; y < keyDefault.Length; y++ )
			{
				if( keyDefault[y] == input[idx] )
				{
					eS4 += bases[y];
				}
			}

			if( idx % CHARACTER_THRESHOLD == 0 )
				yield return new WaitForSeconds( 0 );
		}

		//Console.WriteLine("4: " + eS4);

		// Pass 3 - Unreverse everything	
		string eS3 = "";

		for( int idx = 0; idx < eS4.Length; idx++ )
		{
			eS3 += eS4[eS4.Length - idx - 1];

			if( idx % CHARACTER_THRESHOLD == 0 )
				yield return new WaitForSeconds( 0 );
		}

		//Console.WriteLine("3: " + eS3);

		// Pass 2 - Remove random hex character every 3 characters
		// Note: (The first random hex character is removed after the second character instead)
		string eS2 = "";
		int c = 1;

		for( int idx = 0; idx < eS3.Length; idx++ )
		{
			c++;
			if( c > 3 )
			{
				c = 0;
			}
			else
			{
				eS2 += eS3[idx];
			}

			if( idx % CHARACTER_THRESHOLD == 0 )
				yield return new WaitForSeconds( 0 );
		}

		//Console.WriteLine("2: " + eS2);

		// Pass 1 - Make a new string of hex values (padded to 4 digits each) for each character in string
		string eS = "";
		string batch = "";
		//List<byte> bytes = new List<byte>();

		for( int idx = 0; idx < eS2.Length; idx += 4 )
		{
			batch = eS2.Substring( idx, 4 );
			eS = GetDecryptedHexString( batch, idx ) + eS;

			if( idx % CHARACTER_THRESHOLD == 0 )
				yield return new WaitForSeconds( 0 );
		}

		//Console.WriteLine("1: " + eS);

		BasicEncryption.Instance.decrpytionComplete( eS );
	}

	void decrpytionComplete( string returnString )
	{
		if( returnOnDecryptionComplete != null )
			returnOnDecryptionComplete( returnString );
	}

	private static string GetEncryptedHexString( string input, int idx )
	{
		return ( ( int )input[input.Length - idx - 1] + idx + 476 ).ToString( "X4" );
	}

	private static string GetDecryptedHexString( string input, int idx )
	{
		return "" + ( char )( int.Parse( input, System.Globalization.NumberStyles.HexNumber ) - ( idx / 4 ) - 476 );
	}

	private static char GetDecryptedHexString2(char[] buffer, int charCount, int idx)
	{
		idx = charCount - idx - 4;
		return (char)(int.Parse(new string(buffer, idx, 4), System.Globalization.NumberStyles.HexNumber) - (idx / 4) - 476);
	}
	
	public void encrypt( string input, ReturnFunction returnOnEncryptionComplete )
	{
		if ( _isBusy )
		{
			Debug.LogError( "[BasicEncryption] Can not encrypt because busy!" );
			returnOnDecryptionComplete( input );
			return;
		}
		
		_isBusy = true;
		this.returnOnEncryptionComplete = result => {
			_isBusy = false;
			returnOnEncryptionComplete( result );
		};
		StartCoroutine( Encrypt( input ) );
	}

	public void decrypt( string input, ReturnFunction returnOnDecryptionComplete )
	{
		if ( _isBusy )
		{
			Debug.LogError( "[BasicEncryption] Can not decrypt because busy!" );
			returnOnDecryptionComplete( input );
			return;
		}
		
		_isBusy = true;
		this.returnOnDecryptionComplete = result => {
			_isBusy = false;
			returnOnDecryptionComplete( result );
		};
		StartCoroutine( Decrypt( input ) );
	}

	//16 unique, lower case, alpha chars
	public static bool IsEncryptionKeyValid( string key )
	{
		if ( string.IsNullOrEmpty( key ) ) return false;
		if ( key.Length != 16 ) return false;

		for ( int i = 0; i < 16; ++i )
		{
			if ( key[i] < 'a' || key[i] > 'z' ) return false;

			for ( int j = 0; j < i; ++j )
			{
				if ( key[j] == key[i] ) return false;
			}
		}

		return true;
	}

}
