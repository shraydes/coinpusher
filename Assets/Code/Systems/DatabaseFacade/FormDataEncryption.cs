﻿#if !UNITY_2018_3_OR_NEWER
#define HAS_UNITY_WWW
#endif

using UnityEngine;
using System.Text;
using System.Collections.Generic;

#if HAS_UNITY_WWW
using UnityWebRequestAPI = UnityEngine.WWW;
#else
using UnityWebRequestAPI = UnityEngine.Networking.UnityWebRequest;
#endif

public class FormDataEncryption  
{
	DownloadRequest request;
	System.Action<DownloadRequest> _onComplete;
	int _changeIndex = 0;

	private static SimpleDataSerializer	_queryStringParser = new SimpleDataSerializer( pairSeparator:'&', keyValueSeparator:'=', nullValue:"" );
	
	public FormDataEncryption( DownloadRequest request, System.Action<DownloadRequest> onComplete )
	{
		this.request = request;
		this._onComplete = onComplete;

		modifyKey( null );
	}
	
	// modifyKey and modifyValue form a two stage loop. It iterates through all form data modifying keys and values.
	private void modifyKey( string modifiedValue )
	{
		// Check to see if this is a repeat, ignoring the first call.
		if( !string.IsNullOrEmpty( modifiedValue ) )
		{
			request.formData[ _changeIndex ].value = modifiedValue;
			_changeIndex++;
			
			// Skip entries that are not to be encrypted.
			while( _changeIndex < request.formData.Count && !request.formData[ _changeIndex ].doEncrypt )
			{
				_changeIndex++;
			}
			
			// Check if we have finished all form data.
			if( _changeIndex == request.formData.Count )
			{
				_onComplete( request );
				return;
			}
		}
		
		// Kick off the key encryption.
		if ( request.formData[ _changeIndex ].key.Length > BasicEncryption.CHARACTER_THRESHOLD)
		{
			BasicEncryption.Instance.encrypt ( request.formData[ _changeIndex ].key, modifyValue );
		}
		else
		{
			string encryptedString = BasicEncryption.Instance.encryptImmediate ( request.formData[ _changeIndex ].key );
			
			modifyValue (encryptedString);
		}
	}
	
	private void modifyValue( string modifiedKey )
	{
		if( string.IsNullOrEmpty( modifiedKey ) )
		{
			// Oh dear. This should never happen!
			Debug.LogError("Uh oh. Encryption failed!");
			return;
		}
		
		request.formData[ _changeIndex ].key = modifiedKey;
		
		// Kick off the value encryption.
		if ( request.formData[ _changeIndex ].value.Length > BasicEncryption.CHARACTER_THRESHOLD)
		{
			BasicEncryption.Instance.encrypt ( request.formData[ _changeIndex ].value, modifyKey );
		}
		else
		{
			string encryptedString = BasicEncryption.Instance.encryptImmediate ( request.formData[ _changeIndex ].value );
			modifyKey (encryptedString);
		}
	}

	public static void EncryptImmediate( IList<FormDataEntry> formData, string encryptionKeyOverride = null, string singleEncryptedEntryKey = null )
	{
		if ( string.IsNullOrEmpty( singleEncryptedEntryKey ) )
		{
			//encrypt keys and values individually and preserve each entry
			for ( int i = 0; i < formData.Count; ++i )
			{
				if ( formData[i].doEncrypt )
				{
					formData[i].key = BasicEncryption.EncryptImmediateFast( formData[i].key, encryptionKeyOverride );
					formData[i].value = BasicEncryption.EncryptImmediateFast( formData[i].value, encryptionKeyOverride );
				}
			}
		}
		else
		{
			//create query string, encrypt in entirity, add new entry for encrypted query string, and remove individual now-encrypted entries
			StringBuilder	queryStringBuilder = new StringBuilder();

			for ( int i = 0; i < formData.Count; ++i )
			{
				if ( formData[i].doEncrypt )
				{
					queryStringBuilder.Append( formData[i].key );
					queryStringBuilder.Append( '=' );
					queryStringBuilder.Append( UnityWebRequestAPI.EscapeURL( formData[i].value ) );
					queryStringBuilder.Append( '&' );//query string will end with an ampersand, which is requred as a workaround for a bug in the encryption

					formData.RemoveAt( i-- );
				}
			}

			if ( queryStringBuilder.Length > 0 )
			{
				string	encrypted = BasicEncryption.EncryptImmediateFast( queryStringBuilder.ToString(), encryptionKeyOverride );

				formData.Add( new FormDataEntry( singleEncryptedEntryKey, encrypted ) );
			}
		}
	}

	public static void DecryptImmediate( IList<FormDataEntry> formData, string encryptionKeyOverride = null, string singleEncryptedEntryKey = null )
	{
		if ( string.IsNullOrEmpty( singleEncryptedEntryKey ) )
		{
			for ( int i = 0; i < formData.Count; ++i )
			{
				if ( formData[i].doEncrypt )
				{
					formData[i].key = BasicEncryption.DecryptImmediateFast( formData[i].key, encryptionKeyOverride );
					formData[i].value = BasicEncryption.DecryptImmediateFast( formData[i].value, encryptionKeyOverride );
				}
			}
		}
		else
		{
			int	encrytpedEntryIdx;

			for ( encrytpedEntryIdx = 0; encrytpedEntryIdx < formData.Count && formData[ encrytpedEntryIdx ].key != singleEncryptedEntryKey; ++encrytpedEntryIdx ) {}

			if ( encrytpedEntryIdx < formData.Count )
			{
				string	encrypted = formData[ encrytpedEntryIdx ].value;

				formData.RemoveAt( encrytpedEntryIdx );

				string	queryString = BasicEncryption.DecryptImmediateFast( encrypted, encryptionKeyOverride );

				_queryStringParser.DeserializePairs( queryString, setPairCount:( int pairCount ) => {
				}, setPair:( int pairIdx, string key, string value ) => {
					formData.Add( new FormDataEntry( key, UnityWebRequestAPI.UnEscapeURL( value ) ) );
				} );
			}
		}
	}
	
}
