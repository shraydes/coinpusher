﻿using UnityEngine;
using System;

public class UpdateForwarder : MonoBehaviour 
{
	public Action OnUpdateFunction;

	void Awake()
	{
		DontDestroyOnLoad( gameObject );
	}

	void Update () 
	{
		if( OnUpdateFunction != null )
		{
			OnUpdateFunction();
		}
	}
}