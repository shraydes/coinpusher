﻿#if UNITY_5_4_OR_NEWER
#define USE_UNITY_WEBREQUEST
#endif
#if !UNITY_2018_3_OR_NEWER
#define HAS_UNITY_WWW
#endif
#if PUMA_DEFINE_UNIWEB && !UNITY_WSA
#define HAS_UNIWEB
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
#if USE_UNITY_WEBREQUEST
using UnityEngine.Networking;
#endif
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
using System.IO;
#endif

// Changes:
// CC20160922 - move DLC specific stuff from DownloadRequest to DLCAsset

public enum DownloadStatus
{
	NOT_DOWNLOADING,
	DOWNLOADING,
	FAILED,         // suggests the program can carry on anyway. 
	SUCCEEDED,
	ERROR           // fatal error, must be handled and app stopped. 
}

public enum DownloadLocation
{
	REMOTE,
	LOCAL_PERSISTANT,
	LOCAL_STREAMING
}

public enum EDownloadLibrary
{
	UNITY,
	UNIWEB,
	UNITY_WEBREQUEST
}

public enum EDownloadAssetType
{
	NONE = 0,
	TEXT,
	TEXTURE,
	ASSET_BUNDLE
}

public interface IWebRequest
{
	void	Send();

	event Action	_pOnResult;
}

public class DownloadRequest : IWebRequest
{
	protected delegate void DOnParseUrl( ref string url );

	const char RETURN = '\r';
	const char NEW_L = '\n';
	static string NEW_LINE = "" + RETURN + NEW_L;
	const char TAB = '\t';
	const char QUOTE = '\"';

	const string DATE_KEY = "DATE";
	const string CACHE_CONTROL_KEY = "CACHE-CONTROL";
	const string MAX_AGE_KEY = "max-age";

	const string ASSET_BUNDLE_DEFAULT_FILETYPE = ".unity3d";
	const string ASSET_BUNDLE_MOBILE_FILETYPE = ".assetbundle";

	const int MAX_RETRYS = 3;

	const float DEFAULT_TIMEOUT = 2.5f;

	private static string[] URL_FILENAME_PARTS_TO_REPLACE = new string[]
	{
		"://",
		".",
		"/",
		"?",
		"&",
		"="
	};
	private const string URL_FILENAME_REPLACEMENT = "_";

	private static readonly string[] IMAGE_FILE_EXTENSIONS = new string[]
	{
		"png",
		"jpeg",
		"jpg",
		"bmp",
		"gif",
		"tiff",
		"ppm",
		"pgm",
		"pbm",
		"pnm"
	}; 

	public string
		fileName,
		requestName,
		localFilePath,
		streamingFilePath;

	public List<FormDataEntry> formData = new List<FormDataEntry>();
	public string _postData;

	public Dictionary<string, string> formHeaders = new Dictionary<string, string>();

	public bool doDebug = false;
	public bool doVerboseDebug = false;

	public float _requestTimeout = DEFAULT_TIMEOUT;

#if HAS_UNITY_WWW
	WWW _www = null;
	WWW _localHeaderInfoDownload;
#endif

#if USE_UNITY_WEBREQUEST
	UnityWebRequest _unityWebRequest;
	UnityWebRequest _localHeaderInfoDownload_UnityWebRequest;
#endif

#if HAS_UNIWEB
	HTTP.Request _request = null;
#endif

	public bool doRemoteDownload = true;
	public bool doLocalDownload = true;
	public bool doLocalHeaderInfo = true;
	public bool doCacheDownload = true;
	public bool doCacheHeaderInfo = true;

	public bool doUseCacheTimeout = true;

	public bool doFail8x8Textures = false;

	public int downloadSize = 0;
	public int numAttempts = 0;
	public int maxAttempts = MAX_RETRYS;

	public event Action<DownloadRequest> onSuccessCallBack;
	public event Action<DownloadRequest> onFailCallback;

	protected event DOnParseUrl	_pOnParseUrl;

#if UNITY_ANDROID
	bool hasTriedStreamingAssets = false;
#endif

	DownloadLocation _downloadLocation = DownloadLocation.REMOTE;

	Dictionary<string, string> _responseHeaders;

	GameObject _updateObject;

	DateTime _downloadTime;
	TimeSpan _cacheTimeout;

	string _dataPath;
#pragma warning disable 0414
	string _streamingPath;
#pragma warning restore 0414
	string _rawURL;
	string _requestURL;
#pragma warning disable 0649
	string _persistantHeaderInfoFilePath;
	string _streamingHeaderInfoFilePath;
#pragma warning restore 0649

	int _lastBytes = 0;

	float _timeoutTimer = 0f;

#if DEBUG_DOWNLOAD_REQUEST
	float	_debugDownloadTimer = 0f;
	float	_debugTimeoutMax = 0f;
	float	_debugTimeoutAcc = 0f;
	int		_debugTimeoutCount = 0;
#endif

	bool _hasBeenDownloaded = false;

#if !UNITY_WEBPLAYER && !UNITY_WEBGL
	bool _hasEncounteredFatalError = false;
#endif
	bool _hasPreprocessed = false;
	bool _hasCheckedForLocalHeaderInfo = false;
	bool _hasDLDateBeenSet = false;
	bool _hasCacheTimeoutBeenSet = false;

	DownloadStatus _currentStatus = DownloadStatus.NOT_DOWNLOADING;

	EDownloadLibrary _downloadLibrary;
	EDownloadLibrary _downloadLibraryUsed;

#pragma warning disable 0414
	private EDownloadAssetType	_assetTypeExpected = EDownloadAssetType.NONE;
#pragma warning restore 0414

	private AssetBundle	_assetBundleFromUnityWebRequest;

#if !UNITY_WEBPLAYER && !UNITY_WEBGL
	public bool _pHasEncounteredFatalError
	{
		get
		{
			return _hasEncounteredFatalError;
		}
	}
#endif

	public bool _pHasBeenDownloaded
	{
		get
		{
			return _hasBeenDownloaded;
		}
	}

	public DownloadLocation _pDownloadLocation
	{
		get
		{
			return _downloadLocation;
		}
	}

	public DownloadStatus _pCurrentStatus
	{
		get
		{
			return _currentStatus;
		}
	}

	public string _pRequestURLWithFormData
	{
		get
		{
			if( formData == null || formData.Count == 0 )
			{
				return _requestURL;
			}

			string returnString = _requestURL + "?";

			for( int x = 0; x < formData.Count; x++ )
			{
				returnString += formData[x].key + "=" + formData[x].value + "&";
			}

			return returnString.Substring( 0, returnString.Length - 1 );
		}

	}

	public bool hasLocalFile
	{
		get
		{
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
			return File.Exists( localFilePath );
#else
			return false;
#endif
		}
	}

	public bool hasLocalHeaderFile
	{
		get
		{
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
			return File.Exists( _persistantHeaderInfoFilePath );
#else
			return false;
#endif
		}
	}

	public bool hasLocalStreamingFile
	{
		get
		{
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
			return File.Exists( streamingFilePath );
#else
			return false;
#endif
		}
	}

	public bool hasLocalStreamingHeaderFile
	{
		get
		{
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
			return File.Exists( _streamingHeaderInfoFilePath );
#else
			return false;
#endif
		}
	}

	public string dataPath
	{
		get
		{
			return _dataPath;
		}
		set
		{
			if( _dataPath != value )
			{
				_dataPath = value;
				parseURL();
			}
		}
	}

	public string requestUrl
	{
		get
		{
			return _requestURL;
		}
		set
		{
			if( _rawURL != value )
			{
				_rawURL = value;
				parseURL();
			}
		}
	}

	public Texture _pResponseTexture
	{
		get
		{
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY )
			{
#if HAS_UNITY_WWW
				if ( _www != null )
				{
					return _www.texture;
				}
#endif
			}

			if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
			{
#if HAS_UNIWEB
				if ( _request != null )
				{
					return null;//_request.response.Texture;
				}
#endif
			}

#if USE_UNITY_WEBREQUEST
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST && _unityWebRequest != null && _unityWebRequest.isDone )
			{
				DownloadHandlerTexture dlhTex = _unityWebRequest.downloadHandler as DownloadHandlerTexture;
				return dlhTex != null && dlhTex.isDone? dlhTex.texture : null;
			}
#endif

			return null;
		}
	}

	public string _pResponseString
	{
		get
		{
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY )
			{
#if HAS_UNITY_WWW
				if ( _www != null )
				{
					return _www.isDone && string.IsNullOrEmpty(_www.error) ? _www.text : "";
				}
#endif
			}

			if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
			{
#if HAS_UNIWEB
				if ( _request != null )
				{
					return _request.response.Text;
				}
#endif
			}

#if USE_UNITY_WEBREQUEST
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST && _unityWebRequest != null )
			{
				return _unityWebRequest.isDone && 
						_unityWebRequest.downloadHandler != null && 
						string.IsNullOrEmpty( _unityWebRequest.error )? _unityWebRequest.downloadHandler.text : "";
			}
#endif

			return null;
		}
	}

	public string _pErrorString
	{
		get
		{
			if( _requestTimeout >= 0f && _timeoutTimer > _requestTimeout )
			{
				return "Download request timed out";
			}

			if( _downloadLibraryUsed == EDownloadLibrary.UNITY )
			{
#if HAS_UNITY_WWW
				if ( _www != null )
				{
					return _www.error;
				}
#endif
			}

			if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
			{
#if HAS_UNIWEB
				if ( _request != null )
				{
					if( _request.exception != null )
					{
						return _request.exception.Message;
					}
					else
					{
						return null;
					}
				}
#endif
			}

#if USE_UNITY_WEBREQUEST
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST && _unityWebRequest != null )
			{
				return _unityWebRequest.error;
			}
#endif

			return null;
		}
	}

	public AssetBundle _pAssetBundle
	{
		get
		{
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY )
			{
#if HAS_UNITY_WWW
				if ( _www != null )
				{
					return _www.assetBundle;
				}
#endif
			}

			if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
			{
#if HAS_UNIWEB
				if ( _request != null )
				{
					// TODO: Impliment this.
					return null;
				}
#endif
			}

#if USE_UNITY_WEBREQUEST
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST && _unityWebRequest != null )
			{
				//if( _unityWebRequest.isDone && _unityWebRequest.downloadHandler is DownloadHandlerAssetBundle )
				//{
				//	DownloadHandlerAssetBundle assetDLH = (DownloadHandlerAssetBundle)_unityWebRequest.downloadHandler;
				//	return assetDLH.assetBundle;
				//}
				return _assetBundleFromUnityWebRequest;
			}
#endif

			return null;
		}
	}

	public bool _pHasResponse
	{
		get
		{
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY )
			{
#if HAS_UNITY_WWW
				if ( _www != null )
				{
					return true;
				}
#endif
			}

			if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
			{
#if HAS_UNIWEB
				if ( _request != null )
				{
					return true;
				}
#endif
			}

#if USE_UNITY_WEBREQUEST
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST && _unityWebRequest != null )
			{
				return true;
			}
#endif
			return false;
		}
	}

	public float _pDecimalProgress
	{
		get
		{
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY )
			{
#if HAS_UNITY_WWW
				if ( _www != null )
				{
					return _www.progress;
				}
#endif
			}

			if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
			{
#if HAS_UNIWEB
				if ( _request != null )
				{
					return _request.Progress;
				}
#endif
			}

#if USE_UNITY_WEBREQUEST
			if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST && _unityWebRequest != null )
			{
				return _unityWebRequest.downloadProgress;
			}
#endif

			return 0f;
		}
	}

	public DateTime _pDownloadDate
	{
		get
		{
			if( _hasDLDateBeenSet )
			{
				return _downloadTime;
			}

			parseDownloadTime();

			return _downloadTime;
		}
	}

	public TimeSpan _pCacheTimeout
	{
		get
		{
			if( _hasCacheTimeoutBeenSet )
			{
				return _cacheTimeout;
			}

			parseDownloadTime();

			return _cacheTimeout;
		}
	}

	public bool _pIsCacheInDate
	{
		get
		{
			if( _pCacheTimeout.TotalSeconds <= 0 )
			{
				return false;
			}
			else
			{
				return DateTime.Now < _pDownloadDate.Add( _pCacheTimeout );
			}
		}
	}

	public event System.Action	_pOnUpdateDownload;

	public DownloadRequest()
	{
#if USE_UNITY_WEBREQUEST
		_downloadLibrary = EDownloadLibrary.UNITY_WEBREQUEST;
#else
		//#if UNITY_EDITOR || UNITY_WEBPLAYER
		_downloadLibrary = EDownloadLibrary.UNITY;
		//#else
		//		_downloadLibrary = EDownloadLibrary.UNIWEB;
		//#endif
#endif
		_dataPath = Application.persistentDataPath;
		if( _dataPath != null && _dataPath.Length > 0 && _dataPath[_dataPath.Length - 1] != '/' )
		{
			_dataPath += '/';
		}
#if UNITY_ANDROID
		_streamingPath = Application.dataPath + "!/assets/";
#elif UNITY_IOS
		_streamingPath = Application.dataPath + "/Raw/";
#else
		_streamingPath = Application.dataPath + "/StreamingAssets/";
#endif

#if DEBUG_MESSAGES
		doDebug = true;
#endif
	}

	public void ClearDownload()
	{
		destroyUpdateObject();
#if HAS_UNITY_WWW
		if( _www != null )
		{
			_www.Dispose();
		}
		_www = null;
		if( _localHeaderInfoDownload != null )
		{
			_localHeaderInfoDownload.Dispose();
		}
		_localHeaderInfoDownload = null;
#endif
#if HAS_UNIWEB
		_request = null;
#endif
#if UNITY_ANDROID
		hasTriedStreamingAssets = false;
#endif
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
		_hasEncounteredFatalError = false;
#endif
		_hasPreprocessed = false;
		_hasCacheTimeoutBeenSet = false;
		_hasDLDateBeenSet = false;
		_hasBeenDownloaded = false;
	}

	public void success()
	{
		if( doLocalHeaderInfo && !_hasCheckedForLocalHeaderInfo && _downloadLocation != DownloadLocation.REMOTE )
		{
#if !UNITY_ANDROID
			if( hasLocalHeaderFile || hasLocalStreamingHeaderFile )
			{
#endif
				fetchLocalHeaderInfo();
				return;
#if !UNITY_ANDROID
			}
#endif
		}

		parseDownloadTime();

		//Log( "Download time [" + _pDownloadDate.ToShortDateString() + "]. Cache timeout [" + _pCacheTimeout.TotalSeconds + "]" );
		//Log ( "Is cache in date? [" + _pIsCacheInDate + "]" );

#if UNITY_ANDROID
		if( _downloadLocation == DownloadLocation.LOCAL_STREAMING && doUseCacheTimeout && !_pIsCacheInDate )
		{
			Log ( "Streaming assets copy was out of date. Checking persistant location" );
			ClearDownload();
			hasTriedStreamingAssets = true;
			download();
			return;
		}
#endif

		if( _downloadLocation != DownloadLocation.REMOTE && doUseCacheTimeout && !_pIsCacheInDate )
		{
			Log( "Locally cached copy was out of date. Downloading remotely" );
			ClearDownload();
			doLocalDownload = false;
			doLocalHeaderInfo = false;
			download();
			return;
		}

		destroyUpdateObject();

		if( onSuccessCallBack != null )
		{
			onSuccessCallBack( this );
		}
	}

	public void fail()
	{
#if UNITY_ANDROID
		if( !hasTriedStreamingAssets && _downloadLocation == DownloadLocation.LOCAL_STREAMING )
		{
			hasTriedStreamingAssets = true;
			download();
			return;
		}
#endif

		Log( "Request failed [" + _pErrorString + "]" );

		destroyUpdateObject();

		if( onFailCallback != null )
		{
			onFailCallback( this );
		}
	}

	public void onPreProcessComplete()
	{
		//Log("PreProcess complete.");
		_hasPreprocessed = true;
		download();
	}

	public void onPostProcessComplete()
	{
		success();
	}

	public void download()
	{
		if( !_hasPreprocessed )
		{
			preProcessRequest();
			return;
		}

		numAttempts = 0;
		Log( "Starting download..." );

		if( hasLocalFile && doLocalDownload )
		{
			Log( "Downloading local file." );
			startLocalDownload();
			return;
		}

#if UNITY_ANDROID
		if( !hasTriedStreamingAssets && doLocalDownload )
		{
			Log( "Downloading local file from streaming assets");
			startLocalDownload();
			return;
		}
#else
		if( hasLocalStreamingFile && doLocalDownload )
		{
			Log( "Downloading local file from streaming assets" );
			startLocalDownload();
			return;
		}
#endif

		if( !doRemoteDownload )
		{
			if( _downloadLocation == DownloadLocation.REMOTE )
			{
				// Just in case we didn't actually attempt any downloads.
				_downloadLocation = DownloadLocation.LOCAL_PERSISTANT;
			}

			Log( "Unable to download local file." );
			fail();
			return;
		}

		if( doLocalDownload )
		{
			Log( "File locations [" + localFilePath + "] and [" + streamingFilePath + "] were checked for existing asset and none were found." );

			Log( "No local copy of asset [" + requestName + "] exists. Starting download from [" + requestUrl + "]" );
		}
		else
		{
			Log( "Forcing new download from [" + requestUrl + "]" );
		}

		startRemoteDownload();
	}

	public void FailedExternalVarification()
	{
		Log( "Download has failed external verification.");

		if( numAttempts < maxAttempts )
		{
			retry();
			return;
		}

		_currentStatus = DownloadStatus.FAILED;
		fail();
	}

	// this is not static to allow debug logs to be turned on on an individual download basis.
	public void Log( string message, UnityEngine.Object o = null )
	{
		if( !doDebug && !doVerboseDebug )
		{
			return;
		}

#if UNITY_EDITOR
		Debug.Log( "<color=green><b>DownloadRequest: </b></color>" + message, o );
#else
		Debug.Log ("DownloadRequest: "+message, o);
#endif
	}

	protected void parseURL()
	{
		_requestURL = _rawURL;

		if ( _pOnParseUrl != null )
		{
			_pOnParseUrl( ref _requestURL );
		}

#if UNITY_IPHONE || UNITY_ANDROID
		if( _requestURL.Contains( ASSET_BUNDLE_DEFAULT_FILETYPE ) )
		{
			_requestURL = _requestURL.Replace( ASSET_BUNDLE_DEFAULT_FILETYPE, ASSET_BUNDLE_MOBILE_FILETYPE );
		}
#endif
		fileName = GenerateFileNameFromURL( _requestURL );
		
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
		localFilePath = dataPath + fileName;
		streamingFilePath = _streamingPath + fileName;

		_persistantHeaderInfoFilePath = makeLocalHeaderFilePath( localFilePath );
		_streamingHeaderInfoFilePath = makeLocalHeaderFilePath( streamingFilePath );
#endif
		
		InferAssetType();
	}

	public static string GenerateFileNameFromURL( string url )
	{
		string uniqueName = url;

		for( int x = 0; x < URL_FILENAME_PARTS_TO_REPLACE.Length; x++ )
		{
			uniqueName = uniqueName.Replace( URL_FILENAME_PARTS_TO_REPLACE[x], URL_FILENAME_REPLACEMENT );
		}
		
		return uniqueName;
	}

	string makeLocalHeaderFilePath( string downloadPath )
	{
		int lastfolderIndex = downloadPath.LastIndexOf( "/" );
		int fileExtensionIndex = downloadPath.LastIndexOf( "." );
		if( fileExtensionIndex != -1 && fileExtensionIndex > lastfolderIndex )
		{
			downloadPath = downloadPath.Substring( 0, fileExtensionIndex );
		}
		downloadPath += "_headerInfo.txt";
		return downloadPath;
	}

	void startLocalDownload()
	{
		_currentStatus = DownloadStatus.DOWNLOADING;
		_lastBytes = 0;
		_timeoutTimer = 0f;
		_downloadLocation = hasLocalFile ? DownloadLocation.LOCAL_PERSISTANT : DownloadLocation.LOCAL_STREAMING;

		ensureUpdateObject();

		string localDownloadUrl = FileHelper.AddLocalFileUrlScheme( hasLocalFile ? localFilePath : streamingFilePath );

#if UNITY_ANDROID
		if( _downloadLocation == DownloadLocation.LOCAL_STREAMING )
		{
			localDownloadUrl = "jar:" + localDownloadUrl;
		}
#endif

		Log( "Downloading from Local file path [" + localDownloadUrl + "]" );

		_downloadLibraryUsed = _downloadLibrary;

		if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
		{
#if HAS_UNIWEB
			_request = new HTTP.Request( "GET", localDownloadUrl );
			_request.Send();
#else
			Debug.LogError( "DownloadRequest: No UniWeb support" );
#endif
		}
		else if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST )
		{
#if USE_UNITY_WEBREQUEST
			_unityWebRequest = CreateAndSendUnityWebRequest( localDownloadUrl, _assetTypeExpected, doCacheDownload, null, null, null, null );
#endif
		}
		else
		{
#if HAS_UNITY_WWW
			_www = new WWW( localDownloadUrl );
#endif
		}
	}

	void startRemoteDownload()
	{
		_currentStatus = DownloadStatus.DOWNLOADING;
		_lastBytes = 0;
		_timeoutTimer = 0f;
		_downloadLocation = DownloadLocation.REMOTE;

		ensureUpdateObject();

		_downloadLibraryUsed = _downloadLibrary;

		if( formData != null && formData.Count > 0 )
		{
			WWWForm newForm = new WWWForm();
			for( int x = 0; x < formData.Count; x++ )
			{
				newForm.AddField( formData[x].key, formData[x].value );
			}

			if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
			{
#if HAS_UNIWEB
				_request = new HTTP.Request( requestUrl, newForm );
				foreach( KeyValuePair<string, string> formHeader in formHeaders )
				{
					_request.headers.Set( formHeader.Key, formHeader.Value );
				}
				_request.Send();
#else
				Debug.LogError( "DownloadRequest: No UniWeb support" );
#endif
			}
			else if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST )
			{
#if USE_UNITY_WEBREQUEST
				_unityWebRequest = CreateAndSendUnityWebRequest( requestUrl, _assetTypeExpected, doCacheDownload, newForm, null, null, formHeaders );
#endif
			}
			else
			{
#if HAS_UNITY_WWW
				_www = new WWW( requestUrl, newForm.data, formHeaders );
#endif
			}
		}
		else if( !string.IsNullOrEmpty( _postData ) )
		{
			if( _downloadLibrary == EDownloadLibrary.UNIWEB )
			{
				Debug.LogError( "DownloadRequest: No UniWeb support using only post data string yet" );
			}
			else if( _downloadLibrary == EDownloadLibrary.UNITY_WEBREQUEST )
			{
#if USE_UNITY_WEBREQUEST
				_unityWebRequest = CreateAndSendUnityWebRequest( requestUrl, _assetTypeExpected, doCacheDownload, null, _postData, "application/json", formHeaders );
#endif
			}
			else
			{
#if HAS_UNITY_WWW
				_www = new WWW( requestUrl, System.Text.Encoding.ASCII.GetBytes( _postData.ToCharArray() ), formHeaders );
#endif
			}
		}
		else
		{
			if( _downloadLibraryUsed == EDownloadLibrary.UNIWEB )
			{
#if HAS_UNIWEB
				_request = new HTTP.Request( "GET", requestUrl );

				foreach( KeyValuePair<string, string> formHeader in formHeaders )
				{
					_request.headers.Set( formHeader.Key, formHeader.Value );
				}
				_request.Send();
#else
				Debug.LogError( "DownloadRequest: No UniWeb support" );
#endif
			}
			else if( _downloadLibraryUsed == EDownloadLibrary.UNITY_WEBREQUEST )
			{
#if USE_UNITY_WEBREQUEST
				_unityWebRequest = CreateAndSendUnityWebRequest( requestUrl, _assetTypeExpected, doCacheDownload, null, null, null, formHeaders );
#endif
			}
			else
			{
#if HAS_UNITY_WWW
				_www = new WWW( requestUrl, null, formHeaders );
#endif
			}
		}

		Log( "Request sent [" + requestUrl + "]" );
	}

#if USE_UNITY_WEBREQUEST
	private static UnityWebRequest CreateAndSendUnityWebRequest( string url, EDownloadAssetType assetTypeExpected, bool cacheDownload, WWWForm formData, string postData, string postDataContentType, IDictionary<string, string> requestHeaders )
	{
		UnityWebRequest	newRequest = CreateUnityWebRequest( url, formData, postData, postDataContentType );

		SendUnityWebRequest( newRequest, assetTypeExpected, cacheDownload, requestHeaders );

		return newRequest;
	}
#endif

#if USE_UNITY_WEBREQUEST
	private static UnityWebRequest CreateUnityWebRequest( string url, WWWForm formData, string postData, string postDataContentType )
	{
		UnityWebRequest	newRequest;

		if ( formData != null )
		{
			newRequest = UnityWebRequest.Post( url, formData );
		}
		else if ( !string.IsNullOrEmpty( postData ) )
		{
			newRequest = UnityWebRequest.Post( url, new WWWForm() );

			byte[]	data = System.Text.Encoding.UTF8.GetBytes( postData );
 
			// add the data into the upload handler. 
			UploadHandlerRaw upHandler = new UploadHandlerRaw( data );

			if ( !string.IsNullOrEmpty( postDataContentType ) )
			{
				upHandler.contentType = postDataContentType;
			}

			newRequest.uploadHandler = upHandler;
		}
		else
		{
			newRequest = new UnityWebRequest( url );
		}

		return newRequest;
	}
#endif

#if USE_UNITY_WEBREQUEST
	private static void SendUnityWebRequest( UnityWebRequest webRequest, EDownloadAssetType assetTypeExpected, bool cacheDownload, IDictionary<string, string> requestHeaders )
	{
		if ( requestHeaders != null )
		{
			foreach( KeyValuePair<string, string> header in requestHeaders )
			{
				webRequest.SetRequestHeader( header.Key, header.Value );
			}
		}

		CreateDownloadHandler( webRequest, assetTypeExpected, cacheDownload );

		#if UNITY_2017_2_OR_NEWER
		webRequest.SendWebRequest();
		#else
		webRequest.Send();
		#endif
	}
#endif

	void ensureUpdateObject()
	{
		if( _updateObject != null )
		{
			_updateObject.name = "Download [" + fileName + "]";
			return;
		}

		_updateObject = new GameObject( "Download [" + fileName + "]" );
		UpdateForwarder forwarder = _updateObject.AddComponent<UpdateForwarder>();
		forwarder.OnUpdateFunction = Update;
	}

	void destroyUpdateObject()
	{
		if( _updateObject == null )
		{
			return;
		}

		if( Application.isPlaying )
		{
			GameObject.Destroy( _updateObject );
		}
		else
		{
			GameObject.DestroyImmediate( _updateObject );
		}

		_updateObject = null;
	}

	void Update()
	{
		if( doVerboseDebug )
			Log( "Updating [" + _downloadLocation.ToString() + "] download using [" + _downloadLibraryUsed + "]. Current status [" + _currentStatus + "]" );

		updateLocalHeaderDownload();

		if( _currentStatus != DownloadStatus.DOWNLOADING )
		{
			return;
		}

		UpdateUnityDownload();

		UpdateUniWebDownload();

		UpdateUnityWebRequestDownload();

		if ( _pOnUpdateDownload != null )
		{
			_pOnUpdateDownload();
		}
	}

	//NB There is probably some redundant code that will never be called in this function
	// because it used to be used to handle remote downloads as well as local ones, and it is
	// now only called to handle local downloads (on mobile), so the conditions relating to remote
	// downloads will never be true (unless we're on webplayer)

	// For safety's sake this code has been left unaltered for now so that no new bugs are
	// introduced by attempting to delete the (possibly) redundant code.

	void UpdateUnityDownload()
	{
#if HAS_UNITY_WWW
		if( _downloadLibraryUsed != EDownloadLibrary.UNITY )
		{
			return;
		}

		if( _www == null )
		{
			if( _currentStatus == DownloadStatus.DOWNLOADING )
			{
				_currentStatus = DownloadStatus.FAILED;
				Log( "Download became null." );
				fail();
				return;
			}

			_currentStatus = DownloadStatus.NOT_DOWNLOADING;
			return;
		}

		bool hasTimedOut = updateTimeout( Mathf.FloorToInt( _www.progress * 100000f ) );

		if( !hasTimedOut && !_www.isDone && _www.error == null )
		{
			_currentStatus = DownloadStatus.DOWNLOADING;
			return;
		}

		if( _www.error == null && !hasTimedOut )
		{
			int responseCode = getResponseCode( _www );

			_hasBeenDownloaded = true;
#if DEBUG_DOWNLOAD_REQUEST
			Log( "Download complete: T=" + _debugDownloadTimer.ToString() + "s, tm=" + _debugTimeoutMax.ToString() + "s, ta=" + (_debugTimeoutAcc / _debugTimeoutCount).ToString() + "s. Response code [" + responseCode + "]" );
#else
			Log( "Download complete. Response code [" + responseCode + "]" );
#endif
			processResponseHeaders( _www );

			if( !cacheDownload( _www.bytes ) || !cacheHeaderInfo() )
			{
				// Fatal error occured while caching - probably ran out of disk space
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
				_hasEncounteredFatalError = true;
#endif
				_currentStatus = DownloadStatus.ERROR;
				fail();
				return;
			}
			else if( doFail8x8Textures && _www.texture != null && _www.texture.width == 8 && _www.texture.height == 8 )
			{
				// The unsupported image file ? is 8 x 8 pixels. Can result from a corrupted download.
				Log( "Download had 8x8 pixel texture. Assuming failed/corrupt download." );

				if( numAttempts < maxAttempts )
				{
					retry();
					return;
				}

				_currentStatus = DownloadStatus.FAILED;
				fail();
				return;
			}
			else if( responseCode == 206 )
			{
				// Response code 206 ( partial content ) is not really acceptable.
				Log( "Download had response code 206 ( partial content )" );

				if( numAttempts < maxAttempts )
				{
					retry();
					return;
				}

				_currentStatus = DownloadStatus.FAILED;
				fail();
				return;
			}
			else
			{
				_currentStatus = DownloadStatus.SUCCEEDED;
				postProcessResponse();
			}
			return;
		}
		else
		{
			Log( hasTimedOut ? ( "Timed out after [" + _requestTimeout + "] seconds." ) : ( "Error [" + _www.error + "]" ) );

			if( numAttempts < maxAttempts )
			{
				retry();
				return;
			}

			_currentStatus = DownloadStatus.FAILED;
			fail();
			return;
		}
#endif
	}



	//
	//
	//

	// NB This code has been refactored from the UpdateUnityDownload function (previously known as
	// the Update() function) to handle remote downloads with UniWeb's HTTP.Request class instead
	// of the WWW class. Likewise to the comments above, this function will have redundant code
	// relating to local downloads, which has been left in (despite conditions meaning it will
	// never be called) in the interest of not introducing new bugs.

	void UpdateUniWebDownload()
	{
		if( _downloadLibraryUsed != EDownloadLibrary.UNIWEB )
		{
			return;
		}

#if HAS_UNIWEB
		if( _request == null )
		{
			if( _currentStatus == DownloadStatus.DOWNLOADING )
			{
				_currentStatus = DownloadStatus.FAILED;
				Log( "Download became null." );
				fail();
				return;
			}

			_currentStatus = DownloadStatus.NOT_DOWNLOADING;
			return;
		}

		bool hasTimedOut = updateTimeout( Mathf.FloorToInt( _request.Progress * 100000f ) );

		if( !hasTimedOut && !_request.isDone && _request.exception == null )
		{
			_currentStatus = DownloadStatus.DOWNLOADING;
			return;
		}

		if( !hasTimedOut && _request.exception == null )
		{
			_hasBeenDownloaded = true;

			Log( "Download complete." );

			processResponseHeaders( _request );

			if( !cacheDownload( _request.response.Bytes ) || !cacheHeaderInfo() )
			{
				// Fatal error occured while caching - probably ran out of disk space
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
				_hasEncounteredFatalError = true;
#endif
				_currentStatus = DownloadStatus.ERROR;
				fail();
				return;
			}
			else
			{
				_currentStatus = DownloadStatus.SUCCEEDED;
				postProcessResponse();
			}
			return;
		}
		else
		{
			Log( "Download Failed." );
			Log( hasTimedOut ? "Timed out after [" + _requestTimeout + "] seconds." : _request.exception.Message );

			if( numAttempts < maxAttempts )
			{
				retry();
				return;
			}

			_currentStatus = DownloadStatus.FAILED;
			fail();
			return;
		}
#endif//HAS_UNIWEB
	}

	void UpdateUnityWebRequestDownload()
	{
#if USE_UNITY_WEBREQUEST
		if( _downloadLibraryUsed != EDownloadLibrary.UNITY_WEBREQUEST )
		{
			return;
		}

		if( _unityWebRequest == null )
		{
			if( _currentStatus == DownloadStatus.DOWNLOADING )
			{
				_currentStatus = DownloadStatus.FAILED;
				Log( "Download became null." );
				fail();
				return;
			}

			_currentStatus = DownloadStatus.NOT_DOWNLOADING;
			return;
		}

		bool hasTimedOut = updateTimeout( Mathf.FloorToInt( _unityWebRequest.downloadProgress * 100000f ) );
 
		if( !hasTimedOut && !_unityWebRequest.isDone && string.IsNullOrEmpty( _unityWebRequest.error ) )
		{
			_currentStatus = DownloadStatus.DOWNLOADING;
			return;
		}

		if( string.IsNullOrEmpty( _unityWebRequest.error ) && !hasTimedOut )
		{
			if( _unityWebRequest.downloadHandler != null && !_unityWebRequest.downloadHandler.isDone )
			{
				_currentStatus = DownloadStatus.DOWNLOADING;
				return;
			}

			DownloadHandlerTexture dlhTex = _unityWebRequest.downloadHandler as DownloadHandlerTexture;
			int responseCode = (int)_unityWebRequest.responseCode;

			_hasBeenDownloaded = true;
#if DEBUG_DOWNLOAD_REQUEST
			Log( "Download complete: T=" + _debugDownloadTimer.ToString() + "s, tm=" + _debugTimeoutMax.ToString() + "s, ta=" + (_debugTimeoutAcc / _debugTimeoutCount).ToString() + "s. Response code [" + responseCode + "]" );
#else
			Log( "Download complete. Response code [" + responseCode + "]" );
#endif
			processResponseHeaders( _unityWebRequest );

			if ( _assetTypeExpected == EDownloadAssetType.ASSET_BUNDLE )
			{
				GetAssetBundleFromUnityWebRequest();
			}

			if( ( !( _unityWebRequest.downloadHandler is DownloadHandlerAssetBundle ) && !cacheDownload( _unityWebRequest.downloadHandler.data ) ) || !cacheHeaderInfo() )
			{
				// Fatal error occured while caching - probably ran out of disk space
#if !UNITY_WEBPLAYER && !UNITY_WEBGL
				_hasEncounteredFatalError = true;
#endif
				_currentStatus = DownloadStatus.ERROR;
				fail();
				return;
			}
			else if( responseCode == 206 )
			{
				// Response code 206 ( partial content ) is not really acceptable.
				Log( "Download had response code 206 ( partial content )" );

				if( numAttempts < maxAttempts )
				{
					retry();
					return;
				}

				_currentStatus = DownloadStatus.FAILED;
				fail();
				return;
			}
			else if( doFail8x8Textures && dlhTex != null && dlhTex.texture != null && 
					dlhTex.texture.width == 8 && dlhTex.texture.height == 8)
			{
				// The unsupported image file ? is 8 x 8 pixels. Can result from a corrupted download.
				Log( "Download had 8x8 pixel texture. Assuming failed/corrupt download." );

				if( numAttempts < maxAttempts )
				{
					retry();
					return;
				}

				_currentStatus = DownloadStatus.FAILED;
				fail();
				return;
			}
			else
			{ 
				_currentStatus = DownloadStatus.SUCCEEDED;
				postProcessResponse();
			}
			return;
		}
		else
		{
			Log( hasTimedOut ? ( "Timed out after [" + _requestTimeout + "] seconds." ) : ( "Error [" + _unityWebRequest.error + "]" ) );

			if( numAttempts < maxAttempts )
			{
				retry();
				return;
			}

			_currentStatus = DownloadStatus.FAILED;
			fail();
			return;
		}
#endif
	}

	protected virtual void preProcessRequest()
	{
		onPreProcessComplete();
	}

	protected virtual void postProcessResponse()
	{
		onPostProcessComplete();
	}

	void retry()
	{
		Log( "Retrying download..." );

		numAttempts++;

		switch( _downloadLocation )
		{
			case ( DownloadLocation.REMOTE ):
				startRemoteDownload();
				break;
			case ( DownloadLocation.LOCAL_STREAMING ):
			case ( DownloadLocation.LOCAL_PERSISTANT ):
				startLocalDownload();
				break;
		}
	}

	// Returns true when timeout has been exceeded.
	bool updateTimeout( int currentProgress )
	{
		//float dt = RealTime.deltaTime;
		float dt = Time.unscaledDeltaTime;

		if( _lastBytes == currentProgress )
		{
			_timeoutTimer += dt;
			if( doVerboseDebug )
				Log( "Updating timeout [" + ( Mathf.FloorToInt( _timeoutTimer * 10 ) * 0.1f ) + "/" + _requestTimeout + "]" );
		}
		else
		{
#if DEBUG_DOWNLOAD_REQUEST
			if ( _timeoutTimer > _debugTimeoutMax )
			{
				_debugTimeoutMax = _timeoutTimer;
			}
			_debugTimeoutAcc += _timeoutTimer;
			_debugTimeoutCount++;
#endif
			_timeoutTimer = 0f;
			if( doVerboseDebug )
				Log( "Updating progress [" + ( currentProgress / 1000f ) + "%]" );
		}

		_lastBytes = currentProgress;

#if DEBUG_DOWNLOAD_REQUEST
		if ( _lastBytes > 0 )
		{
			_debugDownloadTimer += dt;
		}
#endif
		return _requestTimeout >= 0f && _timeoutTimer > _requestTimeout;
	}

	// Returns false if it enountered a fatal error.
	bool cacheDownload( byte[] bytes )
	{
		if( !doCacheDownload || _downloadLocation != DownloadLocation.REMOTE )
		{
			return true;
		}

#if UNITY_WEBPLAYER || UNITY_WEBGL
		return true;
#else
		Log( "Download complete. Writing download to file [" + localFilePath + "]" );

		if( !Directory.Exists( dataPath ) )
		{
			Log( "Directory [" + dataPath + "] did not exist, attempting to Create." );

			try
			{
				Directory.CreateDirectory( dataPath );
			}
			catch( System.Exception e )
			{
				Debug.LogError( "Failed to Create directory, EXEPTION: " + e.ToString() );
				return false;
			}
		}

		try
		{
			File.WriteAllBytes( localFilePath, bytes );
		}
		catch( System.Exception e )
		{
			Debug.LogError( "Failed to write download [" + requestName + "] to file! Error [" + e.Message + "]" );
			return false;
		}

		return true;
#endif
	}

#if USE_UNITY_WEBREQUEST
	void processResponseHeaders( UnityWebRequest unityWebRequest )
	{
		if ( _unityWebRequest.GetResponseHeaders() == null )
		{
			OnNoResponseHeaders();
			return;
		}

		List<string> keys = new List<string>();
		List<string> values = new List<string>();

		foreach( KeyValuePair<string, string> pair in unityWebRequest.GetResponseHeaders() )
		{
			keys.Add( pair.Key );
			values.Add( pair.Value );
		}

		processResponseHeaders( keys.ToArray(), values.ToArray() );
	}
#endif

#if HAS_UNITY_WWW
	void processResponseHeaders( WWW www )
	{
		if ( www.responseHeaders == null )
		{
			OnNoResponseHeaders();
			return;
		}

		List<string> keys = new List<string>();
		List<string> values = new List<string>();

		foreach( KeyValuePair<string, string> pair in www.responseHeaders )
		{
			keys.Add( pair.Key );
			values.Add( pair.Value );
		}

		processResponseHeaders( keys.ToArray(), values.ToArray() );
	}
#endif

#if HAS_UNIWEB
	void processResponseHeaders( HTTP.Request request )
	
		if ( request.headers == null )
		{
			OnNoResponseHeaders();
			return;
		}

		List<string> keys = request.headers.Keys;
		List<string> values = new List<string>();

		string valueString;
		for( int x = 0, max = keys.Count; x < max; x++ )
		{
			List<string> valueArray = request.headers.GetAll( keys[x] );
			valueString = "";
			for( int y = 0, ymax = valueArray.Count; y < ymax; y++ )
			{
				valueString += valueArray[y] + ",";
			}
			if( valueString.Length > 0 )
			{
				valueString = valueString.Substring( 0, valueString.Length - 1 );
			}
			values.Add( valueString );
		}

		processResponseHeaders( keys.ToArray(), values.ToArray() );
	}
#endif

	void processResponseHeaders( string[] keys, string[] values )
	{
		_responseHeaders = new Dictionary<string, string>();

		if( keys.Length != values.Length )
		{
			Log( "Header key-value pair mismatch." );
			return;
		}

		for( int x = 0, max = keys.Length; x < max; x++ )
		{
			if( _responseHeaders.ContainsKey( keys[x] ) )
			{
				continue;
			}

			_responseHeaders.Add( keys[x], values[x] );
		}
	}

	private void OnNoResponseHeaders()
	{
		_responseHeaders = new Dictionary<string, string>();
	}

	// Returns false if it enountered a fatal error.
	bool cacheHeaderInfo()
	{
		if( !doCacheHeaderInfo || _downloadLocation != DownloadLocation.REMOTE )
		{
			return true;
		}

#if UNITY_WEBPLAYER || UNITY_WEBGL
		return true;
#else
		string jsonString = "{";

		foreach( KeyValuePair<string, string> kvp in _responseHeaders )
		{
			jsonString += NEW_LINE;
			jsonString += TAB + "" + QUOTE + kvp.Key + QUOTE + ":" + QUOTE + kvp.Value + QUOTE + ",";
		}

		jsonString = jsonString.Substring( 0, jsonString.Length - 1 );
		jsonString += NEW_LINE + "}";

		Log( "Header info save location [" + _persistantHeaderInfoFilePath + "]" );

		if( !Directory.Exists( dataPath ) )
		{
			Log( "Directory [" + dataPath + "] did not exist, attempting to Create." );

			try
			{
				Directory.CreateDirectory( dataPath );
			}
			catch( System.Exception e )
			{
				Debug.LogError( "Failed to Create directory, EXEPTION: " + e.ToString() );
				return false;
			}
		}

		try
		{
			File.WriteAllBytes( _persistantHeaderInfoFilePath, System.Text.Encoding.UTF8.GetBytes( jsonString ) );
		}
		catch( System.Exception e )
		{
			Debug.LogError( "Failed to write header info to file! Error [" + e.Message + "]" );
			return false;
		}

		return true;
#endif
	}

	void fetchLocalHeaderInfo()
	{
		_hasCheckedForLocalHeaderInfo = true;
		_lastBytes = 0;
		_timeoutTimer = 0f;
		ensureUpdateObject();

		bool isStreaming = _downloadLocation == DownloadLocation.LOCAL_STREAMING;

		string localDownloadUrl = FileHelper.AddLocalFileUrlScheme( isStreaming ? _streamingHeaderInfoFilePath : _persistantHeaderInfoFilePath );

#if UNITY_ANDROID
		if( isStreaming )
		{
			localDownloadUrl = "jar:" + localDownloadUrl;
		}
#endif

		Log( "Fetching local header info from [" + localDownloadUrl + "]" );

		switch ( _downloadLibrary )
		{
		case EDownloadLibrary.UNITY:
#if HAS_UNITY_WWW
			_localHeaderInfoDownload = new WWW( localDownloadUrl );
#endif
			break;
		case EDownloadLibrary.UNITY_WEBREQUEST:
#if USE_UNITY_WEBREQUEST
			_localHeaderInfoDownload_UnityWebRequest = CreateAndSendUnityWebRequest( localDownloadUrl, EDownloadAssetType.NONE, false, null, null, null, null );
#endif
			break;
		}
	}

	void updateLocalHeaderDownload()
	{
		switch ( _downloadLibrary )
		{
		case EDownloadLibrary.UNITY:
			updateLocalHeaderDownload_UnityWWW();
			break;
		case EDownloadLibrary.UNITY_WEBREQUEST:
			updateLocalHeaderDownload_UnityWebRequest();
			break;
		}
	}

	void updateLocalHeaderDownload_UnityWWW()
	{
#if HAS_UNITY_WWW
		if( _localHeaderInfoDownload == null )
		{
			return;
		}

		if( !string.IsNullOrEmpty( _localHeaderInfoDownload.error ) )
		{
			Log( "local header file download failed. Error [" + _localHeaderInfoDownload.error + "]" );
			localHeaderInfoDownloadFail();
			return;
		}

		if( !_localHeaderInfoDownload.isDone && updateTimeout( Mathf.FloorToInt( _localHeaderInfoDownload.progress * 100000f ) ) )
		{
			Log( "local header file download failed. Timed out." );
			localHeaderInfoDownloadFail();
			return;
		}

		if( !_localHeaderInfoDownload.isDone )
		{
			return;
		}

		localHeaderInfoDownloadSuccess();
#endif
	}

	void updateLocalHeaderDownload_UnityWebRequest()
	{
#if USE_UNITY_WEBREQUEST
		if( _localHeaderInfoDownload_UnityWebRequest == null )
		{
			return;
		}

		if( !string.IsNullOrEmpty( _localHeaderInfoDownload_UnityWebRequest.error ) )
		{
			Log( "local header file download failed. Error [" + _localHeaderInfoDownload_UnityWebRequest.error + "]" );
			localHeaderInfoDownloadFail();
			return;
		}

		if( !_localHeaderInfoDownload_UnityWebRequest.isDone && updateTimeout( Mathf.FloorToInt( _localHeaderInfoDownload_UnityWebRequest.downloadProgress * 100000f ) ) )
		{
			Log( "local header file download failed. Timed out." );
			localHeaderInfoDownloadFail();
			return;
		}

		if( !_localHeaderInfoDownload_UnityWebRequest.isDone )
		{
			return;
		}

		localHeaderInfoDownloadSuccess();
#endif
	}

	void localHeaderInfoDownloadFail()
	{
#if HAS_UNITY_WWW
		if( _localHeaderInfoDownload != null )
		{
			_localHeaderInfoDownload.Dispose();
		}

		_localHeaderInfoDownload = null;
#endif

#if USE_UNITY_WEBREQUEST
		if( _localHeaderInfoDownload_UnityWebRequest != null )
		{
			_localHeaderInfoDownload_UnityWebRequest.Dispose();
		}

		_localHeaderInfoDownload_UnityWebRequest = null;
#endif

		destroyUpdateObject();

		// We failed to download the local header info ( which is highly unlikely ), but overall the request succeeded.
		success();
	}

	void localHeaderInfoDownloadSuccess()
	{
		string	headerText = null;

		switch ( _downloadLibrary )
		{
		case EDownloadLibrary.UNITY:
#if HAS_UNITY_WWW
			headerText = _localHeaderInfoDownload.text;
#endif
			break;
		case EDownloadLibrary.UNITY_WEBREQUEST:
#if USE_UNITY_WEBREQUEST
			headerText = _localHeaderInfoDownload_UnityWebRequest.downloadHandler.text;
#endif
			break;
		}

		if( string.IsNullOrEmpty( headerText ) )
		{
			Log( "local header file download succeeded but was empty!" );
			localHeaderInfoDownloadFail();
		}

		string jsonString = headerText.Replace( NEW_LINE + "", "" ).Replace( TAB + "", "" ).Replace( "{", "" ).Replace( "}", "" );//.Replace(QUOTE +"", "");

		Log( "Local header file downloaded. [" + jsonString + "]" );

		int nextQuote = jsonString.IndexOf( QUOTE );
		int currentStart = -1;
		int currentEnd = -1;
		string currentKey = "";
		string currentValue = "";

		while( nextQuote != -1 )
		{
			if( currentStart == -1 )
			{
				currentStart = nextQuote;
			}
			else if( currentEnd == -1 )
			{
				currentEnd = nextQuote - 1;
			}

			if( currentStart != -1 && currentEnd != -1 )
			{
				if( currentKey == "" )
				{
					currentKey = jsonString.Substring( currentStart + 1, currentEnd - currentStart );
				}
				else if( currentValue == "" )
				{
					currentValue = jsonString.Substring( currentStart + 1, currentEnd - currentStart );

					//Log( "Key [" + currentKey + "]. Value [" + currentValue + "]" );

					if( !_responseHeaders.ContainsKey( currentKey ) )
					{
						_responseHeaders.Add( currentKey, currentValue );
					}

					currentKey = "";
					currentValue = "";
				}

				currentStart = -1;
				currentEnd = -1;
			}

			nextQuote = jsonString.IndexOf( QUOTE, nextQuote + 1 );
		}

		destroyUpdateObject();

		success();
	}

	void parseDownloadTime()
	{
		if( _currentStatus != DownloadStatus.SUCCEEDED )
		{
			return;
		}

		//Log( "Has header date? [" + _responseHeaders.ContainsKey( DATE_KEY ) + "]." + ( _responseHeaders.ContainsKey( DATE_KEY )? ( "Date [" + _responseHeaders[ DATE_KEY ] + "]" ) : "" ) );

		if( _responseHeaders == null || !_responseHeaders.ContainsKey( DATE_KEY ) )
		{
			_downloadTime = DateTime.Now;
		}
		else
		{
			_downloadTime = DateTime.Now;

			DateTime.TryParse( _responseHeaders[DATE_KEY], out _downloadTime );
		}

		_hasDLDateBeenSet = true;

		//Log( "Has cache control? [" + _responseHeaders.ContainsKey( CACHE_CONTROL_KEY ) + "]." + ( _responseHeaders.ContainsKey( CACHE_CONTROL_KEY )? ( "Cache control [" + _responseHeaders[ CACHE_CONTROL_KEY ] + "]" ) : "" ) );

		if( _responseHeaders != null && _responseHeaders.ContainsKey( CACHE_CONTROL_KEY ) )
		{
			string[] cacheInfoCSVArray = _responseHeaders[CACHE_CONTROL_KEY].Split( ',' );

			for( int x = 0, max = cacheInfoCSVArray.Length; x < max; x++ )
			{
				if( cacheInfoCSVArray[x].Contains( MAX_AGE_KEY ) )
				{
					cacheInfoCSVArray = cacheInfoCSVArray[x].Split( '=' );

					if( cacheInfoCSVArray.Length != 2 )
					{
						continue;
					}

					int seconds = 0;
					int.TryParse( cacheInfoCSVArray[1], out seconds );

					if( seconds == 0 )
					{
						seconds = -1;
					}

					_cacheTimeout = new TimeSpan( 0, 0, seconds );

					//Log( "Cache timeout total seconds set to [" + _cacheTimeout.TotalSeconds +"]");

					_hasCacheTimeoutBeenSet = true;
				}
				else if( cacheInfoCSVArray[x].Contains( "no-cache" ) || cacheInfoCSVArray[x].Contains( "no-store" ) )
				{
					_cacheTimeout = new TimeSpan( 0, 0, -1 );
					_hasCacheTimeoutBeenSet = true;
				}
				else
				{
					continue;
				}
			}

			// This means cache control was defined but it had no max age or other time defining variables
			if( !_hasCacheTimeoutBeenSet )
			{
				_cacheTimeout = new TimeSpan( 0, 0, -1 );
				_hasCacheTimeoutBeenSet = true;
			}
		}

		// No cache-control data at all. Ignore cache rules?
		if( !_hasCacheTimeoutBeenSet )
		{
			_cacheTimeout = new TimeSpan();
			doUseCacheTimeout = false;
			_hasCacheTimeoutBeenSet = true;
		}
	}

	public string FindFormData( string key, string defaultValue )
	{
		if( formData == null )
			return defaultValue;

		for( int i = 0; i < formData.Count; ++i )
		{
			if( formData[i] == null )
				continue;
			if( formData[i].key != key )
				continue;
			return formData[i].value;
		}

		return defaultValue;
	}

#if HAS_UNITY_WWW
	public int getResponseCode( WWW request )
	{
		int ret = 0;
		if (request.responseHeaders == null )
		{
			Log("no response headers.");
		}
		else
		{
			if( !request.responseHeaders.ContainsKey("STATUS") )
			{
				Log("response headers has no STATUS.");
			}
			else
			{
				ret = parseResponseCode(request.responseHeaders["STATUS"]);
			}
		}	
		return ret;
	}
#endif

	public int parseResponseCode(string statusLine)
	{
		int ret = 0;

		string[] components = statusLine.Split(' ');
		if (components.Length < 3)
		{
			Log("invalid response status: " + statusLine);
		}
		else
		{
			if (!int.TryParse(components[1], out ret))
			{
				Log("invalid response code: " + components[1]);
			}
		}

		return ret;
	}

	private void InferAssetType()
	{
		string	fileExtension = System.IO.Path.GetExtension( this.fileName ).ToLower();
		bool	hasImageFileExtension = IMAGE_FILE_EXTENSIONS.Contains( fileExtension );

		if ( hasImageFileExtension )
		{
			SetAssetType( EDownloadAssetType.TEXTURE );
		}
	}

	public void SetAssetType( EDownloadAssetType type )
	{
		Debug.Log( "[DownloadRequest] Asset type expected: " + type );
		_assetTypeExpected = type;
	}

	void IWebRequest.Send()
	{
		download();
	}

	event Action IWebRequest._pOnResult
	{
		add
		{
			onSuccessCallBack += rq => value();
			onFailCallback += rq => value();
		}
		remove
		{
			throw new NotImplementedException();
		}
	}

#if USE_UNITY_WEBREQUEST
	private static void CreateDownloadHandler( UnityWebRequest request, EDownloadAssetType assetTypeExpected, bool cacheDownload )
	{
		switch ( assetTypeExpected )
		{
		case EDownloadAssetType.TEXTURE:
			request.downloadHandler = new DownloadHandlerTexture();
			break;
		case EDownloadAssetType.ASSET_BUNDLE:
			if ( cacheDownload )
			{
				request.downloadHandler = new DownloadHandlerBuffer();
			}
			else
			{
				request.downloadHandler = new DownloadHandlerAssetBundle( request.url, 0 );
			}
			break;
		default:
			request.downloadHandler = new DownloadHandlerBuffer();
			break;
		}
	}

	private void CreateDownloadHandler()
	{
		if ( _downloadLibraryUsed != EDownloadLibrary.UNITY_WEBREQUEST )
		{
			Debug.LogError( "[DownloadRequest] Download library '" + _downloadLibraryUsed + "' does not use download handlers" );
			return;
		}

		if ( _unityWebRequest == null )
		{
			Debug.LogError( "[DownloadRequest] _unityWebRequest is null" );
			return;
		}

		//if ( _assetTypeExpected == EDownloadAssetType.NONE )
		//{
		//	Debug.LogWarning( "[DownloadRequest] Have not set expected asset type" );
		//}

		CreateDownloadHandler( _unityWebRequest, _assetTypeExpected, doCacheDownload );
	}

	private void GetAssetBundleFromUnityWebRequest()
	{
		if ( _unityWebRequest.downloadHandler is DownloadHandlerAssetBundle )
		{
			_assetBundleFromUnityWebRequest = ( _unityWebRequest.downloadHandler as DownloadHandlerAssetBundle ).assetBundle;
		}
		else
		{
			_assetBundleFromUnityWebRequest = AssetBundle.LoadFromMemory( _unityWebRequest.downloadHandler.data );
		}
	}
#endif//USE_UNITY_WEBREQUEST
}
