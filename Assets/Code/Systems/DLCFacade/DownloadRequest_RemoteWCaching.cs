﻿using UnityEngine;
using System.Collections;

public class DownloadRequest_RemoteWCaching  : DownloadRequest 
{
	public DownloadRequest_RemoteWCaching() : base()
	{
		doRemoteDownload = true;

		doCacheDownload = true;
		doCacheHeaderInfo = true;

		doLocalDownload = false;
		doLocalHeaderInfo = false;
	}
}
