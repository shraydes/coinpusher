﻿using UnityEngine;
using System.Collections;

public class DownloadRequest_RemoteOnly : DownloadRequest 
{
	public DownloadRequest_RemoteOnly() : base()
	{
		doRemoteDownload = true;

		doCacheDownload = false;
		doCacheHeaderInfo = false;

		doLocalDownload = false;
		doLocalHeaderInfo = false;
	}
}
