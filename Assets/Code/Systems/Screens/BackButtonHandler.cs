﻿////////////////////////////////////////////
// 
// BackButtonHandler.cs
//
// Created: 20/01/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System.Collections.Generic;
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

using UnityEngine.EventSystems;
using WidgetType = UnityEngine.UI.Graphic;
using ButtonType = UnityEngine.UI.Button;

public class BackButtonHandler : MonoBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[BackButtonHandler] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	public delegate void DOnDeviceBackButtonPressed( ref bool isHandled );

	//
	// MEMBERS 
	//

	private BooleanStateRequests	_backButtonDisableRequests;

	private bool	_isBackButtonDisabled;
	
	//
	// PROPERTIES
	//

	public static event DOnDeviceBackButtonPressed	_pOnDeviceBackButtonPressed;

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	private void Awake()
	{
		_backButtonDisableRequests = new BooleanStateRequests( newState => {
			_isBackButtonDisabled = newState;
		}, () => _isBackButtonDisabled, false );
	}

	private void Update()
	{
		HandleDeviceBackButton();
	}

	// ** PUBLIC
	//

	public void BlockBackButton( bool wantBlocked, object source )
	{
		if ( _backButtonDisableRequests == null ) return;

		Debug.Log( "[ScreenFacade] BlockBackButton: " + wantBlocked + ", " + source );

		_backButtonDisableRequests.RequestState( wantBlocked, source, 0, true );
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private void HandleDeviceBackButton()
	{
		if ( _isBackButtonDisabled ) return;

		bool	isBackPressed = false;
		
#if UNITY_EDITOR
		isBackPressed = Input.GetKeyDown( KeyCode.Escape );
#elif UNITY_ANDROID
		isBackPressed = Input.GetKeyDown( KeyCode.Escape );
#elif UNITY_TVOS
		isBackPressed = AppleTVFacade.GetButtonDown( AppleTVFacade.EButtonId.MENU_BACK );
#endif
		
		if ( isBackPressed )
		{
			OnDeviceBackButtonPressed();
		}
	}
	
	private void OnDeviceBackButtonPressed()
	{
		bool	isBackButtonPressHandled = false;

		if ( _pOnDeviceBackButtonPressed != null )
		{
			_pOnDeviceBackButtonPressed( ref isBackButtonPressHandled );
		}
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

