﻿using UnityEngine;
using UnityEngine.Serialization;
using AmuzoEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class ScreenFacade : MonoBehaviour 
{
	const bool DO_DEBUG = true;

	public static event System.Action<ScreenBase>	_pOnRegisterScreen;
	
	private static Comparison<ScreenFlow>	_sCompareScreenFlowDepth = ( ScreenFlow f1, ScreenFlow f2 ) =>
	{
		ScreenBase	s1 = f1._pCurrentScreen;
		ScreenBase	s2 = f2._pCurrentScreen;

		return s1 == null ? s2 == null ? 0 : 1 : s2 == null ? -1 : s1._pDepth.CompareTo( s2._pDepth );
	};

	private List<ScreenBase>	_screens;

	private Dictionary<System.Type, ScreenBase>	_screenSingletons;

	[SerializeField]
	[FormerlySerializedAs("_uguiCanvas")]
	private Canvas	_uguiMainCanvas;
	
	[SerializeField]
	private Camera	_uguiUICamera;
	public Camera _pMainUICamera
	{
		get
		{
			return _uguiUICamera;
		}
	}
		
	private JobCounter	_screenRegistrationJobs;
	

	[SerializeField]
	private int	_screenFlowCountMax = 4;

	private ObjectPool<ScreenFlow>	_screenFlowPool = null;

	private ScreenFlow	_mainScreenFlow = null;

	private List<ScreenFlow>	_activeScreenFlows = null;

	private ScreenBase	_currentScreen;

	private StringBuilder	_currentLocationBuilder;

	private string	_currentLocation;

	private string	_previousLocation;

	// -----------------------------------------------------------------------------------------------
	public ScreenBase _pCurrentScreen
	{
		get
		{
			return _currentScreen;
		}
	}
	
	public string _pCurrentLocation
	{
		get
		{
			return _currentLocation;
		}
	}
	
	public string _pPreviousLocation
	{
		get
		{
			return _previousLocation;
		}
	}
	
	public ScreenFlow _pMainScreenFlow
	{
		get
		{
			return _mainScreenFlow;
		}
	}

	public bool _pMoreThanOneFlowActive
	{
		get
		{
			if( _activeScreenFlows == null )
				return false;

			return ( _activeScreenFlows.Count > 1 );
		}
	}

	public bool _pIsAnyScreenTweening
	{
		get
		{
			if ( _activeScreenFlows == null ) return false;
			for ( int i = 0; i < _activeScreenFlows.Count; ++i )
			{
				if ( _activeScreenFlows[i] != null && _activeScreenFlows[i]._pIsTransitioning ) return true;
			}
			return false;
		}
	}

	public static event System.Action	_pOnCurrentScreenChanged;

	public static event System.Action	_pOnCurrentLocationChanged;

	// -----------------------------------------------------------------------------------------------
	public void Awake()
	{
		Facades<ScreenFacade>.Register( this );

		ScreenBase._pOnScreenDepthChanged += ( ScreenBase s ) => CheckRefreshScreenOrder();
		BackButtonHandler._pOnDeviceBackButtonPressed += OnDeviceBackButtonPressed;
		
		_screens = new List<ScreenBase>();
		_screenSingletons = new Dictionary<System.Type, ScreenBase>();
		_currentLocationBuilder = new StringBuilder();
		_screenRegistrationJobs = new JobCounter( OnEndScreenRegistrationJobs );
		
		InitScreenFlows();
		
		DontDestroyOnLoad( gameObject );
	}

	// -----------------------------------------------------------------------------------------------
	public void Start()
	{
		ScreenBase[]	childScreens = gameObject.GetComponentsInChildren<ScreenBase>( includeInactive:true );

		BeginScreenRegistrationJob();

		foreach ( ScreenBase s in childScreens )
		{
			RegisterScreen( s );
		}

		EndScreenRegistrationJob();
	}

	// -----------------------------------------------------------------------------------------------
	public void BeginRegisteringScreens()
	{
		BeginScreenRegistrationJob();
	}
	
	// -----------------------------------------------------------------------------------------------
	public void EndRegisteringScreens()
	{
		EndScreenRegistrationJob();
	}
	
	// -----------------------------------------------------------------------------------------------
	private void BeginScreenRegistrationJob()
	{
		_screenRegistrationJobs.BeginJob();
	}
	
	// -----------------------------------------------------------------------------------------------
	private void EndScreenRegistrationJob()
	{
		_screenRegistrationJobs.EndJob();
	}
	
	// -----------------------------------------------------------------------------------------------
	private void OnEndScreenRegistrationJobs()
	{
		CheckRefreshScreenOrder();

		for ( int i = 0; i < _screens.Count; ++i )
		{
			if ( _screens[i] != null )
			{
				_screens[i].OnNewScreensRegistered();
			}
		}
	}

	// -----------------------------------------------------------------------------------------------
#if DEBUG_SHOW_SCREEN_FLOW
	void OnGUI()
	{
		DebugRender();
	}
#endif

	public ScreenBase LoadScreenPrefab( ScreenBase prefab )
	{
		GameObject	screenRoot = gameObject;

		if ( _uguiMainCanvas != null )
		{
			screenRoot = _uguiMainCanvas.gameObject;
		}

		ScreenBase screen = GameObject.Instantiate( prefab, screenRoot.transform ) as ScreenBase;
				
		screen.gameObject.name = prefab.name;
		screen.transform.localScale = Vector3.one;

		RegisterScreen( screen );

		screen.gameObject.SetActive( false );

		return screen;
	}

	// -----------------------------------------------------------------------------------------------
	private void RegisterScreen(ScreenBase screen)
	{
		BeginScreenRegistrationJob();

		if ( !_screens.Contains( screen ) )
		{
			_screens.Add( screen );
		}

		System.Type	screenType = screen.GetType();

		if ( !_screenSingletons.ContainsKey( screenType ) )
		{
			_screenSingletons.Add( screenType, screen );
		}
		else
		{
			if ( _screenSingletons[ screenType ] != screen )
			{
				Debug.LogError( "[ScreenFacade] Different " + screenType + " singleton already registered!" );
			}
		}

		screen.RegisterScreen();
		
		if ( _pOnRegisterScreen != null )
		{
			_pOnRegisterScreen( screen );
		}

		EndScreenRegistrationJob();

		Log( "Screen Registered [" + screenType + "]" );
	}
	
	// -----------------------------------------------------------------------------------------------
	public void ShowScreen( string screenName, bool doClearHistory = false, bool doAddCurrentScreenToHistory = true, System.Action onComplete = null )
	{
		if ( string.IsNullOrEmpty( screenName ) ) return;

		ScreenBase	screen = FindScreen( screenName );

		if ( screen == null )
		{
			Debug.LogError( "ScreenFacade: Failed to find screen: " + screenName );
			return;
		}

		ShowScreen( screen, doClearHistory, doAddCurrentScreenToHistory, onComplete );
	}

	// -----------------------------------------------------------------------------------------------
	public void ShowScreen( ScreenBase screen, bool doClearHistory = false, bool doAddCurrentScreenToHistory = true, System.Action onComplete = null )
	{
		if ( _mainScreenFlow == null )
		{
			Debug.LogError( "ScreenFacade: ShowScreen: Null _mainScreenFlow" );
			return;
		}

		_mainScreenFlow.ShowScreen( screen, doClearHistory, doAddCurrentScreenToHistory, onComplete );
	}

	// -----------------------------------------------------------------------------------------------
	public void ShowPreviousScreen( System.Action onComplete = null )
	{
		if ( _mainScreenFlow == null )
		{
			Debug.LogError( "ScreenFacade: ShowPreviousScreen: Null _mainScreenFlow" );
			return;
		}

		_mainScreenFlow.ShowPreviousScreen( onComplete );
	}

	// -----------------------------------------------------------------------------------------------
	public void ShowNoScreen( bool doClearHistory = false, bool doAddCurrentScreenToHistory = true, System.Action onComplete = null )
	{
		if ( _mainScreenFlow == null )
		{
			Debug.LogError( "ScreenFacade: ShowNoScreen: Null _mainScreenFlow" );
			return;
		}

		_mainScreenFlow.ShowNoScreen( doClearHistory, doAddCurrentScreenToHistory, onComplete );
	}

	// -----------------------------------------------------------------------------------------------
	public void ShowPopupOverlay( ScreenBase screen, System.Action onShowComplete = null, System.Action onFlowEnd = null )
	{
		ScreenFlow	newFlow = null;
		
		newFlow = CreateScreenFlow( () => {
			if ( onFlowEnd != null )
			{
				onFlowEnd();
			}
			DestroyScreenFlow( newFlow );
		} );

		if ( newFlow == null )
		{
			Debug.LogError( "ScreenFacade: ShowScreenInNewFlow: Failed to create new flow" );
			return;
		}

		newFlow.ShowScreen( screen, onComplete:onShowComplete );
	}

	// -----------------------------------------------------------------------------------------------
	public bool IsScreenOnTop( ScreenBase screen )
	{
		return screen._pScreenFlow == _activeScreenFlows[ _activeScreenFlows.Count - 1 ];
	}

	// -----------------------------------------------------------------------------------------------
	public void ForEachActiveScreen( System.Action<ScreenBase> action )
	{
		if ( _activeScreenFlows == null ) return;
		if ( action == null ) return;
		
		//TODO: lock active screen flows
		
		for ( int i = 0; i < _activeScreenFlows.Count; ++i )
		{
			if ( _activeScreenFlows[i] == null ) continue;
			if ( _activeScreenFlows[i]._pCurrentScreen == null ) continue;
			_activeScreenFlows[i]._pCurrentScreen.ForEachLayer( true, action );
		}
		
		//TODO: unlock active screen flows
	}
	
	// -----------------------------------------------------------------------------------------------
	public ScreenBase FindActiveScreen( System.Predicate<ScreenBase> pred )
	{
		if ( _activeScreenFlows == null ) return null;
		if ( pred == null ) return null;
		
		//TODO: lock active screen flows
		
		ScreenBase	foundScreen = null;
		
		for ( int i = 0; i < _activeScreenFlows.Count; ++i )
		{
			if ( _activeScreenFlows[i] == null ) continue;
			if ( _activeScreenFlows[i]._pCurrentScreen == null ) continue;
			foundScreen = _activeScreenFlows[i]._pCurrentScreen.FindLayer( true, pred );
			if ( foundScreen != null ) break;
		}
		
		//TODO: unlock active screen flows
		
		return foundScreen;
	}
	
	// -----------------------------------------------------------------------------------------------
	public T FindActiveScreenOfType<T>() where T : class
	{
		return FindActiveScreen( s => (s is T) ) as T;
	}
	
	// -----------------------------------------------------------------------------------------------

	public ScreenBase FindScreen( string screenName )
	{
		if ( _screens == null ) return null;
		return _screens.Find( s => s.gameObject.name == screenName );
	}
	
	public T GetScreenInstance<T>() where T : ScreenBase
	{
		if ( _screenSingletons == null ) return null;

		System.Type	screenType = typeof( T );

		if ( !_screenSingletons.ContainsKey( screenType ) ) return null;

		return _screenSingletons[ screenType ] as T;
	}

	public bool ScreenToCanvasWorldPoint( Vector2 screenPoint, out Vector3 worldPoint )
	{
		return ScreenToCanvasWorldPoint( screenPoint, _uguiMainCanvas, out worldPoint );
	}

	public bool ScreenToCanvasWorldPoint( Vector2 screenPoint, Canvas screenCanvas, out Vector3 worldPoint )
	{
		worldPoint = Vector3.zero;

		if ( screenCanvas == null ) return false;
		if ( _uguiUICamera == null ) return false;

		return RectTransformUtility.ScreenPointToWorldPointInRectangle( screenCanvas.transform as RectTransform, screenPoint, _uguiUICamera, out worldPoint );
	}
	
	// -----------------------------------------------------------------------------------------------

	private void InitScreenFlows()
	{
		_screenFlowPool = new ObjectPool<ScreenFlow>( size:_screenFlowCountMax, internalAllocFn:() => {
			ScreenFlow	newFlow = new ScreenFlow();
			newFlow._pOnScreenChanged += () => {
				OnScreenChanged( newFlow );
			};
			return newFlow;
		}, internalFreeFn:null );

		_mainScreenFlow = _screenFlowPool.Allocate();

		_activeScreenFlows = new List<ScreenFlow>( _screenFlowCountMax );
		_activeScreenFlows.Add( _mainScreenFlow );
	}

	private ScreenFlow CreateScreenFlow( System.Action onFlowEnd )
	{
		if ( _screenFlowPool == null ) return null;

		ScreenFlow	flow = _screenFlowPool.Allocate();

		if ( flow == null ) return null;

		flow.Reset();
		flow._pOnEnd += onFlowEnd;

		if ( _activeScreenFlows != null && !_activeScreenFlows.Contains( flow ) )
		{
			_activeScreenFlows.Add( flow );
		}

		return flow;
	}

	private void DestroyScreenFlow( ScreenFlow flow )
	{
		if ( flow == null ) return;

		if ( _activeScreenFlows != null && _activeScreenFlows.Contains( flow ) )
		{
			_activeScreenFlows.Remove( flow );
		}

		if ( _screenFlowPool != null )
		{
			_screenFlowPool.Free( flow );
		}
	}

	private void OnScreenChanged( ScreenFlow screenFlow )
	{
		RefreshActiveScreenOrder();
		RefreshCurrentScreen();
		RefreshCurrentLocation();
	}

	private void RefreshActiveScreenOrder()
	{
		_activeScreenFlows.Sort( _sCompareScreenFlowDepth );
	}

	private void RefreshCurrentScreen()
	{
		ScreenBase	newCurrScreen = null;

		for ( int i = _activeScreenFlows.Count - 1; i >= 0 && newCurrScreen == null; --i )
		{
			newCurrScreen = _activeScreenFlows[i]._pCurrentScreen;
		}

		if ( _currentScreen != newCurrScreen )
		{
			_currentScreen = newCurrScreen;

			OnCurrentScreenChanged();
		}
	}
	
	private void RefreshCurrentLocation()
	{
		_currentLocationBuilder.Length = 0;

		for ( int i = 0; i < _activeScreenFlows.Count; ++i )
		{
			if ( _activeScreenFlows[i]._pCurrentScreen == null ) continue;

			if ( i > 0 )
			{
				_currentLocationBuilder.Append( '.' );
			}

			_currentLocationBuilder.Append( _activeScreenFlows[i]._pCurrentScreen._pScreenName );
		}

		string	newCurrentLocation = _currentLocationBuilder.ToString();

		if ( _currentLocation != newCurrentLocation )
		{
			_previousLocation = _currentLocation;
			_currentLocation = newCurrentLocation;

			OnCurrentLocationChanged();
		}
	}

	// -----------------------------------------------------------------------------------------------
	
	private static List<ScreenBase>	_sSortedScreens = new List<ScreenBase>();
	public static void RefreshScreenOrder( IEnumerable<ScreenBase> screens )
	{
		_sSortedScreens.Clear();
		_sSortedScreens.AddRange( screens );
		_sSortedScreens.Sort( comparison:( s1, s2 ) => s1._pDepth - s2._pDepth );

		for ( int i = 0; i < _sSortedScreens.Count; ++i )
		{
			_sSortedScreens[i].gameObject.transform.SetSiblingIndex( i );
		}
	}

	private void CheckRefreshScreenOrder()
	{
		if ( !_screenRegistrationJobs._pHasJobs )
		{
			RefreshScreenOrder( _screens );
		}
	}

	// -----------------------------------------------------------------------------------------------
	
	private void OnCurrentScreenChanged()
	{
		if ( _pOnCurrentScreenChanged != null )
		{
			_pOnCurrentScreenChanged();
		}
	}

	private void OnCurrentLocationChanged()
	{
		if ( _pOnCurrentLocationChanged != null )
		{
			_pOnCurrentLocationChanged();
		}
	}

	// -----------------------------------------------------------------------------------------------

	private void OnDeviceBackButtonPressed( ref bool isHandled )
	{
		if ( isHandled ) return;

		if ( _currentScreen != null )
		{
			_currentScreen.NotifyDeviceBackButtonPressed( ref isHandled );
		}
	}

	// *** Debugging ***
	
	public void DebugRender()
	{
		const int	leftX = 25;
		const int	topY = 25;
		const int	columnWidth = 180;
		const int	lineHeight = 25;
		
		int currX = leftX;
		
		if ( _activeScreenFlows != null )
		{
			for ( int i = 0; i < _activeScreenFlows.Count; ++i )
			{
				if ( _activeScreenFlows[i] != null )
				{
					_activeScreenFlows[i].DebugRender( currX, topY, columnWidth, lineHeight );
					currX += columnWidth;
				}
			}
		}
	}
	
	public static void Log(string message, UnityEngine.Object o = null)
	{
		#pragma warning disable 0162
		if( !DO_DEBUG )
		{
			return;
		}

		#if DEBUG_MESSAGES
			#if UNITY_EDITOR
			Debug.Log ( "<color=#CCA329><b>ScreenFacade: </b></color>" + message, o);
			#else
			Debug.Log ( "ScreenFacade: " + message, o);
			#endif
		#endif
		#pragma warning restore 0162
	}
}
