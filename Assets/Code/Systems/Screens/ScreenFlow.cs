﻿////////////////////////////////////////////
// 
// ScreenFlow.cs
//
// Created: dd/mm/yyyy ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class ScreenFlow
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ScreenFlow] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	private ScreenBase	_currentScreen;

	private List<ScreenBase>	_screenHistory;

	private ScreenTransition	_screenTransition;

	//
	// PROPERTIES
	//

	public event System.Action	_pOnScreenChanged;

	public event System.Action	_pOnEnd;

	public ScreenBase _pCurrentScreen
	{
		get
		{
			return _currentScreen ?? _screenTransition._pCurrentScreen;
		}
	}

	public bool _pIsTransitioning
	{
		get
		{
			return _screenTransition._pCurrentStage != ScreenTransition.EStage.NULL;
		}
	}

	public bool _pIsTransitioningIn
	{
		get
		{
			return _screenTransition._pCurrentStage == ScreenTransition.EStage.IN;
		}
	}

	public bool _pIsTransitioningOut
	{
		get
		{
			return _screenTransition._pCurrentStage == ScreenTransition.EStage.OUT;
		}
	}

	public IScreenTransitionManager _pTransitionManager
	{
		get
		{
			return _screenTransition;
		}
	}

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public ScreenFlow()
	{
		_screenHistory = new List<ScreenBase>();
		_screenTransition = new ScreenTransition();

		Reset();
	}

	public void Reset()
	{
		_pOnEnd = null;
		_currentScreen = null;
		_screenHistory.Clear();
		_screenTransition.Reset();
	}

	public void ShowScreen( ScreenBase nextScreen, bool doClearHistory = false, bool doAddCurrentScreenToHistory = true, System.Action onComplete = null )
	{
#if DEBUG_MESSAGES
		LoggerAPI.Log( LOG_TAG + "ShowScreen: nextScreen:" + nextScreen.name + ", doClearHistory:" + doClearHistory );
#endif
		if ( nextScreen == null )
		{
			LoggerAPI.LogError( LOG_TAG + "Null nextScreen" );
			return;
		}

		DoScreenTransition( doClearHistory, doAddCurrentScreenToHistory && !doClearHistory, nextScreen, onComplete );
	}

	public void ShowPreviousScreen( System.Action onComplete = null )
	{
#if DEBUG_MESSAGES
		LoggerAPI.Log( LOG_TAG + "ShowPreviousScreen" );
#endif
		if ( _currentScreen == null || _screenHistory == null || _screenHistory.Count == 0 )
		{
			if ( onComplete != null )
			{
				onComplete();
			}
		}

		ScreenBase	prevScreen = _screenHistory[ _screenHistory.Count - 1 ];

		_screenHistory.RemoveAt( _screenHistory.Count - 1 );

		if ( prevScreen == null )
		{
			LoggerAPI.LogError( LOG_TAG + "Null prevScreen" );
			return;
		}

		DoScreenTransition( doClearHistory:false, doAddCurrScreenToHistory:false, nextScreen:prevScreen, onComplete:onComplete );
	}

	public void ShowNoScreen( bool doClearHistory = false, bool doAddCurrentScreenToHistory = true, System.Action onComplete = null )
	{
#if DEBUG_MESSAGES
		LoggerAPI.Log( LOG_TAG + "ShowNoScreen: doClearHistory:" + doClearHistory );
#endif
		DoScreenTransition( doClearHistory, doAddCurrentScreenToHistory && !doClearHistory, nextScreen:null, onComplete:onComplete );
	}

	public void End( System.Action onComplete = null )
	{
#if DEBUG_MESSAGES
		LoggerAPI.Log( LOG_TAG + "End" );
#endif
		DoScreenTransition( doClearHistory:false, doAddCurrScreenToHistory:true, nextScreen:null, onComplete:() => {
			if ( _pOnEnd != null )
			{
				_pOnEnd();
			}
			if ( onComplete != null )
			{
				onComplete();
			}
		} );
	}
		
	public void DebugRender( int leftX, int topY, int columnWidth, int lineHeight )
	{
		Rect	box = new Rect( leftX, topY, columnWidth, lineHeight );
		
		if ( _screenTransition != null && _screenTransition._pCurrentStage != ScreenTransition.EStage.NULL )
		{
			box.y = lineHeight >> 1;

			if ( _screenTransition._pScreenIn != null )
			{
				GUI.Box( box, _screenTransition._pScreenIn.name );
			}
			else
			{
				GUI.Box( box, "" );
			}
			box.y += lineHeight;

			if ( _screenTransition._pScreenOut != null )
			{
				GUI.Box( box, _screenTransition._pScreenOut.name );
			}
			else
			{
				GUI.Box( box, "" );
			}
			box.y += lineHeight;
		}
		else
		{
			box.y = lineHeight;

			if ( _currentScreen != null )
			{
				GUI.Box( box, _currentScreen.name );
			}
			else
			{
				GUI.Box( box, "" );
			}
			box.y += lineHeight;
		}

		box.y = 3 * lineHeight;

		if ( _screenHistory != null )
		{
			for ( int i = _screenHistory.Count - 1; i >= 0; --i )
			{
				if ( _screenHistory[i] != null )
				{
					GUI.Box( box, _screenHistory[i].name );
				}
				else
				{
					GUI.Box( box, "null" );
				}
				box.y += lineHeight;
			}
		}
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private void DoScreenTransition( bool doClearHistory, bool doAddCurrScreenToHistory, ScreenBase nextScreen, System.Action onComplete )
	{
		if ( doClearHistory )
		{
#if DEBUG_MESSAGES
			LoggerAPI.Log( LOG_TAG + "DoScreenTransition: Clearing history" );
#endif
			_screenHistory.Clear();
		}

		System.Action	onComplete2 = () => {
			_currentScreen = nextScreen;
			if ( _pOnScreenChanged != null )
			{
				_pOnScreenChanged();
			}
			if ( onComplete != null )
			{
				onComplete();
			}
		};

		if ( _screenTransition._pCurrentStage != ScreenTransition.EStage.NULL )
		{
#if DEBUG_MESSAGES
			LoggerAPI.Log( LOG_TAG + "DoScreenTransition: Screen transition already in progress" );
#endif
			if ( _screenTransition._pCurrentStage == ScreenTransition.EStage.OUT )
			{
#if DEBUG_MESSAGES
				LoggerAPI.Log( LOG_TAG + "DoScreenTransition: Modifying current screen transition to " + nextScreen );
#endif
				_screenTransition.Modify( nextScreen, onComplete2 );
				return;
			}
			else if ( _screenTransition._pCurrentStage == ScreenTransition.EStage.IN )
			{
#if DEBUG_MESSAGES
				LoggerAPI.Log( LOG_TAG + "DoScreenTransition: Cancelling current screen transition" );
#endif
				_screenTransition.Cancel();
			}
		}
		
		if ( _currentScreen != null )
		{
			if ( doAddCurrScreenToHistory )
			{
				_screenHistory.Add( _currentScreen );
			}

			ScreenBase	screenOut = _currentScreen;

			_currentScreen = null;

#if DEBUG_MESSAGES
			LoggerAPI.Log( LOG_TAG + "Starting new screen transition from " + screenOut + " to " + nextScreen );
#endif
			_screenTransition.Init( screenOut, nextScreen, onComplete2 );
		}
		else
		{
#if DEBUG_MESSAGES
			LoggerAPI.Log( LOG_TAG + "Starting new screen transition to " + nextScreen );
#endif
			_screenTransition.Init( this, nextScreen, onComplete2 );
		}
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

