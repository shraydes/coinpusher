﻿////////////////////////////////////////////
// 
// ScreenFlowInit.cs
//
// Created: 10/05/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class ScreenFlowInit : InitialisationObject
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ScreenFlowInit] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//

	//
	// MEMBERS 
	//

	[SerializeField]
	private string	_screenName;

	[SerializeField]
	private ScreenBase	_screenPrefabEditorOnly;

	[SerializeField]
	private bool	_doClearHistory;

	//
	// PROPERTIES
	//

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	// ** PUBLIC
	//

	public override void startInitialising()
	{
		_currentState = InitialisationState.INITIALISING;

		if ( Facades<ScreenFacade>.Instance != null )
		{
			Facades<ScreenFacade>.Instance.ShowScreen( _screenName, _doClearHistory );
			_currentState = InitialisationState.FINISHED;
		}
#if UNITY_EDITOR
		else if ( _screenPrefabEditorOnly != null )
		{
			ScreenFacade	screenFacade = Resources.Load<ScreenFacade>( "ScreenFacade" );

			if ( screenFacade != null )
			{
				screenFacade = GameObject.Instantiate( screenFacade );

				ScreenBase	screen = screenFacade.LoadScreenPrefab( _screenPrefabEditorOnly );

				screenFacade.ShowScreen( screen );
			}
		}
#endif
		else
		{
			LoggerAPI.LogError( LOG_TAG + "Screen Facade not found!" );
		}
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

