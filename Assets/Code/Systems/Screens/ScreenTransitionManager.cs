﻿////////////////////////////////////////////
// 
// ScreenTransitionManager.cs
//
// Created: 04/12/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_ScreenTransitionManager
#endif

public interface IScreenTransitionManager
{
	void	BeginExitScreenJob();

	void	EndExitScreenJob();

	void	BeginBlockNextScreenJob();

	void	EndBlockNextScreenJob();
}

