﻿////////////////////////////////////////////
// 
// ScreenTransition.cs
//
// Created: 06/05/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic; 
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class ScreenTransition : IScreenTransitionManager
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ScreenTransition] ";

	public enum EStage
	{
		NULL = 0,
		OUT,
		IN
	}

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	private ScreenFlow	_screenFlow;

	private ScreenBase	_screenOut;

	private ScreenBase	_screenIn;

	private List<ScreenBase>	_keepLayers;

	private System.Action	_onComplete;

	private EStage	_currStage;

	private JobCounter	_currJobs;
	
	//
	// PROPERTIES
	//

	public EStage _pCurrentStage
	{
		get
		{
			return _currStage;
		}
	}

	public ScreenBase _pCurrentScreen
	{
		get
		{
			return _currStage == EStage.OUT ? _screenOut : _currStage == EStage.IN ? _screenIn : null;
		}
	}
	
	public ScreenBase _pScreenOut
	{
		get
		{
			return _screenOut;
		}
	}
	
	public ScreenBase _pScreenIn
	{
		get
		{
			return _screenIn;
		}
	}
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public ScreenTransition()
	{
		_keepLayers = new List<ScreenBase>();
	}

	public void Reset()
	{
		_screenFlow = null;
		_screenOut = null;
		_screenIn = null;
		_keepLayers.Clear();
		_onComplete = null;
		_currStage = EStage.NULL;
		_currJobs = null;
	}

	public void Init( ScreenFlow screenFlow, ScreenBase screenIn, System.Action onComplete )
	{
		Reset();

		_screenFlow = screenFlow;
		_screenIn = screenIn;
		_onComplete = onComplete;

		BeginStageIn();
	}
	
	public void Init( ScreenBase screenOut, ScreenBase screenIn, System.Action onComplete )
	{
		Reset();

		if ( screenOut == null )
		{
			LoggerAPI.LogError( LOG_TAG + "Null screenOut" );
			return;
		}

		_screenFlow = screenOut._pScreenFlow;
		_screenOut = screenOut;
		_screenIn = screenIn;

		FindPersistantLayers();

		_onComplete = onComplete;

		BeginStageOut();
	}
	
	public void Modify( ScreenBase screenIn, System.Action onComplete )
	{
		if ( _currStage != EStage.OUT )
		{
			LoggerAPI.LogError( LOG_TAG + "Can only modify transition in OUT stage!" );
			return;
		}

		ModifyScreenIn( screenIn );
		ModifyOnComplete( onComplete );
	}
	
	public void Modify( ScreenBase screenIn )
	{
		if ( _currStage != EStage.OUT )
		{
			LoggerAPI.LogError( LOG_TAG + "Can only modify transition in OUT stage!" );
			return;
		}

		ModifyScreenIn( screenIn );
	}
	
	public void Modify( System.Action onComplete )
	{
		ModifyOnComplete( onComplete );
	}

	public void Cancel()
	{
		if ( _currStage != EStage.IN )
		{
			LoggerAPI.LogError( LOG_TAG + "Can only cancel transition in IN stage!" );
			return;
		}

		CancelStageIn();
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private void BeginStageOut()
	{
#if DEBUG_MESSAGES
		LoggerAPI.Log( LOG_TAG + "BeginStageOut" );
#endif

		_currStage = EStage.OUT;

		if ( _screenOut == null )
		{
			EndStageOut();
			return;
		}

		_currJobs = new JobCounter( EndStageOut );
		_currJobs.BeginJob();

		_screenOut.ForEachLayer( true, action:( s ) => {
			s.ExitScreen( isScreenTransition:true );
		}, layerMatch:( s ) => _keepLayers == null || !_keepLayers.Contains( s ) );

		_currJobs.EndJob();
	}

	private void EndStageOut()
	{
#if DEBUG_MESSAGES
		LoggerAPI.Log( LOG_TAG + "EndStageOut" );
#endif

		_currStage = EStage.NULL;
		_currJobs = null;

		BeginStageIn();
	}
	
	private void BeginStageIn()
	{
#if DEBUG_MESSAGES
		LoggerAPI.Log( LOG_TAG + "BeginStageIn" );
#endif

		_currStage = EStage.IN;

		if ( _screenIn == null )
		{
			EndStageIn();
			return;
		}

		_currJobs = _screenIn.ForEachLayerAsync( true, ( s, oc ) => {
			s.ShowScreen( _screenFlow, true, oc );
		}, EndStageIn, s => _keepLayers == null || !_keepLayers.Contains( s ) );
	}

	private void EndStageIn()
	{
#if DEBUG_MESSAGES
		LoggerAPI.Log( LOG_TAG + "EndStageIn" );
#endif

		_currStage = EStage.NULL;
		_currJobs = null;

		if ( _onComplete != null )
		{
			_onComplete();
		}
	}

	private void CancelStageIn()
	{
		_screenIn.ForEachLayer( true, s => {
			s.CancelShowScreen();
		}, s => _keepLayers == null || !_keepLayers.Contains( s ) );

		_onComplete = null;

		_currStage = EStage.NULL;
		_currJobs = null;
	}

	private void ModifyScreenIn( ScreenBase screenIn )
	{
		_screenIn = screenIn;

		List<ScreenBase>	oldKeepLayers = new List<ScreenBase>( _keepLayers );

		FindPersistantLayers();

		for ( int i = 0; i < oldKeepLayers.Count; ++i )
		{
			if ( oldKeepLayers[i] != null && !_keepLayers.Contains( oldKeepLayers[i] ) )
			{
				oldKeepLayers[i].ExitScreen( isScreenTransition:true );
			}
		}
	}

	private void ModifyOnComplete( System.Action onComplete )
	{
		_onComplete = onComplete;
	}

	private void FindPersistantLayers()
	{
		_keepLayers.Clear();

		if ( _screenIn != null )
		{
			IList<ScreenBase>	layersOut = _screenOut._pLayers;
			IList<ScreenBase>	layersIn = _screenIn._pLayers;

			if ( layersOut != null && layersIn != null )
			{
				for ( int i = 0; i < layersOut.Count; ++i )
				{
					if ( layersOut[i] != null && layersIn.Contains( layersOut[i] ) )
					{
						_keepLayers.Add( layersOut[i] );
					}
				}
			}
		}
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//

	void IScreenTransitionManager.BeginExitScreenJob()
	{
	}

	void IScreenTransitionManager.EndExitScreenJob()
	{
	}

	void IScreenTransitionManager.BeginBlockNextScreenJob()
	{
		if ( _currStage != EStage.OUT )
		{
			LoggerAPI.LogError( LOG_TAG + "Unexpected stage: " + _currStage );
			return;
		}

		_currJobs.BeginJob();
	}

	void IScreenTransitionManager.EndBlockNextScreenJob()
	{
		if ( _currStage != EStage.OUT )
		{
			LoggerAPI.LogError( LOG_TAG + "Unexpected stage: " + _currStage );
			return;
		}

		_currJobs.EndJob();
	}
}

