﻿#if PUMA_DEFINE_ANTI_CHEAT
#define HAS_OBSCURED_PREFS
#endif
////////////////////////////////////////////
// 
// AmuzoPrefs.cs
//
// Created: 07/04/2015 rbateman
// Contributors: 
//	23/07/2015 ccuthbert - rewrite, adding obscured prefs support, saving of key data
//  14/12/2015 rbateman - Made into ScriptableSingleton and added editor menu options to some public functions.
// 
// Intention:
// This is a class to track the keys saved to player prefs.
// This is in order to be able to exract all player prefs and serialize them to file at any time.
//
// Note: 
// No notes
//
// Remark: <symbol>
// No remarks
//
////////////////////////////////////////////


using System.Collections;
using System.Collections.Generic;
#if !UNITY_WEBPLAYER
using System.IO;
#endif
using System;
using System.Text;
using UnityEngine;
using AmuzoEngine;
#if HAS_OBSCURED_PREFS
using CodeStage.AntiCheat.ObscuredTypes;
#endif
using LitJson;
using Logger = UnityEngine.Debug;

public class PlayerPrefsFacade : AmuzoScriptableSingleton<PlayerPrefsFacade>
{
	// CONSTS / DEFINES / ENUMS

	public enum EValueType
	{
		NULL = 0,
		STRING,
		FLOAT,
		INT,
		LONG
	}

	private const string LOG_TAG = "[PlayerPrefs] ";

	private const string KEY_DATA_KEY = "ppkd";

	private const string DUMP_FILE_NAME = "PlayerPrefsOutput.txt";

	// SINGLETON DECLARATION

	// NESTED CLASSES / STRUCTS

	// MEMBERS 

	[SerializeField]
	private bool _useEncryption = false;

#pragma warning disable 0414
	[SerializeField]
	private string _encryptionKey = "";
#pragma warning restore 0414

	[NonSerialized]
	private EnumUtils.FromStringMap<EValueType> _valueTypeFromStringMap = new EnumUtils.FromStringMap<EValueType>( EValueType.NULL );

	[NonSerialized]
	private Dictionary<string, EValueType> _keyData = null;

	//private static JobCounter	_keyDataChanges = null;

	[NonSerialized]
	private bool hasInitialised = false;

	// PROPERTIES

	// ABSTRACTS / VIRTUALS

	// STATIC FUNCTIONS

	// FUNCTIONS 

	// ** UNITY

	protected override void Initialise()
	{
		if( hasInitialised )
		{
			return;
		}

		if( _useEncryption )
		{
#if HAS_OBSCURED_PREFS
			if( string.IsNullOrEmpty( _encryptionKey ) )
			{
				ObscuredPrefs.SetNewCryptoKey( SystemInfo.deviceUniqueIdentifier );
			}
			else
			{
				ObscuredPrefs.SetNewCryptoKey( _encryptionKey );
			}
#else
			Logger.LogError( LOG_TAG + "Encryption disabled" );
			_useEncryption = false;
#endif
		}

		CheckInitializeKeyData();

#if PUMA_DEFINE_DEBUG_TOOLS
		AddDebugMenu();
#endif

		hasInitialised = true;
	}

	// ** PUBLIC

	public void DeleteKey( string key )
	{
#if HAS_OBSCURED_PREFS
		if( _useEncryption )
		{
			ObscuredPrefs.DeleteKey( key );
		}
		else
#endif
		{
			PlayerPrefs.DeleteKey( key );
		}

		OnDeleteKey( key );
	}

	public void DeleteAll()
	{
#if HAS_OBSCURED_PREFS
		if( _useEncryption )
		{
			ObscuredPrefs.DeleteAll();
		}
		else
#endif
		{
			PlayerPrefs.DeleteAll();
		}

		OnDeleteAllKeys();
	}

	public bool HasKey( string key )
	{
#if HAS_OBSCURED_PREFS
		return ( _useEncryption && ObscuredPrefs.HasKey( key ) ) || PlayerPrefs.HasKey( key );
#else
		return PlayerPrefs.HasKey( key );
#endif
	}

	public void Save()
	{
#if HAS_OBSCURED_PREFS
		if( _useEncryption )
		{
			ObscuredPrefs.Save();
		}
		else
#endif
		{
			PlayerPrefs.Save();
		}
	}

	public string GetString( string key )
	{
#if HAS_OBSCURED_PREFS
		return _useEncryption ? ObscuredPrefs.GetString( key ) : PlayerPrefs.GetString( key );
#else
		return PlayerPrefs.GetString( key );
#endif
	}

	public float GetFloat( string key )
	{
#if HAS_OBSCURED_PREFS
		return _useEncryption ? ObscuredPrefs.GetFloat( key ) : PlayerPrefs.GetFloat( key );
#else
		return PlayerPrefs.GetFloat( key );
#endif
	}

	public int GetInt( string key )
	{
#if HAS_OBSCURED_PREFS
		return _useEncryption ? ObscuredPrefs.GetInt( key ) : PlayerPrefs.GetInt( key );
#else
		return PlayerPrefs.GetInt( key );
#endif
	}

	public long GetLong( string key )
	{
#if HAS_OBSCURED_PREFS
		return _useEncryption ? ObscuredPrefs.GetLong( key ) : PlayerPrefs.GetInt( key );
#else
		return PlayerPrefs.GetInt( key );
#endif
	}

	public string GetString( string key, string defaultValue )
	{
#if HAS_OBSCURED_PREFS
		return _useEncryption ? ObscuredPrefs.GetString( key, defaultValue ) : PlayerPrefs.GetString( key, defaultValue );
#else
		return PlayerPrefs.GetString( key, defaultValue );
#endif
	}

	public float GetFloat( string key, float defaultValue )
	{
#if HAS_OBSCURED_PREFS
		return _useEncryption ? ObscuredPrefs.GetFloat( key, defaultValue ) : PlayerPrefs.GetFloat( key, defaultValue );
#else
		return PlayerPrefs.GetFloat( key, defaultValue );
#endif
	}

	public int GetInt( string key, int defaultValue )
	{
#if HAS_OBSCURED_PREFS
		return _useEncryption ? ObscuredPrefs.GetInt( key, defaultValue ) : PlayerPrefs.GetInt( key, defaultValue );
#else
		return PlayerPrefs.GetInt( key, defaultValue );
#endif
	}

	public long GetLong( string key, long defaultValue )
	{
#if HAS_OBSCURED_PREFS
		return _useEncryption ? ObscuredPrefs.GetLong( key, defaultValue ) : PlayerPrefs.GetInt( key, ( int )defaultValue );
#else
		return PlayerPrefs.GetInt( key, ( int )defaultValue );
#endif
	}

	public void SetString( string key, string value )
	{
#if HAS_OBSCURED_PREFS
		if( _useEncryption )
		{
			ObscuredPrefs.SetString( key, value );
		}
		else
#endif
		{
			PlayerPrefs.SetString( key, value );
		}

		OnSetKey( key, EValueType.STRING );
	}

	public void SetFloat( string key, float value )
	{
#if HAS_OBSCURED_PREFS
		if( _useEncryption )
		{
			ObscuredPrefs.SetFloat( key, value );
		}
		else
#endif
		{
			PlayerPrefs.SetFloat( key, value );
		}

		OnSetKey( key, EValueType.FLOAT );
	}

	public void SetInt( string key, int value )
	{
#if HAS_OBSCURED_PREFS
		if( _useEncryption )
		{
			ObscuredPrefs.SetInt( key, value );
		}
		else
#endif
		{
			PlayerPrefs.SetInt( key, value );
		}

		OnSetKey( key, EValueType.INT );
	}

	public void SetLong( string key, long value )
	{
#if HAS_OBSCURED_PREFS
		if( _useEncryption )
		{
			ObscuredPrefs.SetLong( key, value );
		}
		else
#endif
		{
			PlayerPrefs.SetInt( key, ( int )value );
		}

		OnSetKey( key, EValueType.LONG );
	}

	public void DebugLogPlayerPrefs()
	{
		Logger.Log( LOG_TAG + "Player prefs:" );
		Logger.Log( SerializePlayerPrefs( true ) );
	}

	public void DebugDumpPlayerPrefs()
	{
#if !UNITY_WEBPLAYER
		string prefsString = SerializePlayerPrefs( true );

		if( string.IsNullOrEmpty( prefsString ) )
		{
			Logger.LogError( LOG_TAG + "Can not dump player prefs: failed to serialize prefs" );
			return;
		}

		string dumpFilePath = Application.persistentDataPath;

		if( !Directory.Exists( dumpFilePath ) )
		{
			try
			{
				Directory.CreateDirectory( dumpFilePath );
			}
			catch( System.Exception e )
			{
				Logger.LogError( LOG_TAG + "Can not dump player prefs: failed to create directory '" + dumpFilePath + "': " + e.ToString() );
				return;
			}
		}

		string fullPath = Path.Combine( dumpFilePath, DUMP_FILE_NAME );

		Logger.Log( LOG_TAG + "Dumping player prefs to '" + fullPath + "'" );

		try
		{
			File.WriteAllBytes( fullPath, System.Text.Encoding.UTF8.GetBytes( prefsString ) );
		}
		catch( System.Exception e )
		{
			Debug.LogError( LOG_TAG + "Can not dump player prefs: failed to write file '" + fullPath + "': " + e.ToString() );
		}
#endif
	}

	// ** PROTECTED

	// ** PRIVATE

	private void CheckInitializeKeyData()
	{
		if( _keyData != null )
			return;

		_keyData = new Dictionary<string, EValueType>();
		//_keyDataChanges = new JobCounter( OnKeyDataChanged );

		string serData = GetString( KEY_DATA_KEY );

		if( !string.IsNullOrEmpty( serData ) )
		{
			DeserializeKeyData( serData );
		}

#if DEBUG_MESSAGES
		DebugLogPlayerPrefs();
#endif
	}

	private void OnSetKey( string key, EValueType valueType )
	{
		CheckInitializeKeyData();
		if( _keyData.ContainsKey( key ) )
		{
			if( _keyData[key] != valueType )
			{
				BeginKeyDataChange();
				_keyData[key] = valueType;
				EndKeyDataChange();
			}
		}
		else
		{
			BeginKeyDataChange();
			_keyData.Add( key, valueType );
			EndKeyDataChange();
		}
	}

	private void OnDeleteKey( string key )
	{
		CheckInitializeKeyData();
		if( _keyData.ContainsKey( key ) )
		{
			BeginKeyDataChange();
			_keyData.Remove( key );
			EndKeyDataChange();
		}
	}

	private void OnDeleteAllKeys()
	{
		CheckInitializeKeyData();
		if( _keyData == null || _keyData.Count > 0 )
		{
			BeginKeyDataChange();
			_keyData = new Dictionary<string, EValueType>();
			EndKeyDataChange();
		}
	}

	private void BeginKeyDataChange()
	{
		//_keyDataChanges.BeginJob();
	}

	private void EndKeyDataChange()
	{
		//_keyDataChanges.EndJob();
		OnKeyDataChanged();
	}

	private void OnKeyDataChanged()
	{
		string serData = SerializeKeyData();

		SetString( KEY_DATA_KEY, serData );
	}

	private string SerializeKeyData()
	{
		JsonWriter jw = Extensions.BeginJson();

		if( jw == null )
		{
			Logger.LogError( LOG_TAG + "Can not serialize key data: failed to begin json writing" );
			return null;
		}

		foreach( KeyValuePair<string, EValueType> kv in _keyData )
		{
			jw.WriteValue( kv.Key, kv.Value.ToString() );
		}

		return Extensions.EndJson();
	}

	private void DeserializeKeyData( string serData )
	{
		JsonData jd = Extensions.LoadJson( serData );

		if( jd == null )
		{
			Logger.LogError( LOG_TAG + "Can not deserialize key data: failed to load json string: " + serData );
			return;
		}

		if( !jd.IsObject )
		{
			Logger.LogError( LOG_TAG + "Can not deserialize key data: unexpected json type: type=" + jd.GetJsonType() + ", json=" + serData );
			return;
		}

		_keyData = new Dictionary<string, EValueType>();

		JsonData vd;

		foreach( DictionaryEntry kv in (IDictionary)jd )
		{
			vd = ( JsonData )kv.Value;

			if( !vd.IsString )
				continue;

			_keyData.Add( ( string )kv.Key, _valueTypeFromStringMap.Lookup( ( string )vd ) );
		}
	}

	private string SerializePlayerPrefs( bool pretty = false )
	{
		JsonWriter jw = Extensions.BeginJson();

		if( jw == null )
		{
			Logger.LogError( LOG_TAG + "Can not get player prefs string: failed to begin json writing" );
			return null;
		}

		jw.PrettyPrint = pretty;

		CheckInitializeKeyData();

		foreach( KeyValuePair<string, EValueType> kv in _keyData )
		{
			switch( kv.Value )
			{
				case EValueType.STRING:
					jw.WriteValue( kv.Key, GetString( kv.Key ) );
					break;
				case EValueType.FLOAT:
					jw.WriteValue( kv.Key, GetFloat( kv.Key ) );
					break;
				case EValueType.INT:
					jw.WriteValue( kv.Key, GetInt( kv.Key ) );
					break;
				case EValueType.LONG:
					jw.WriteValue( kv.Key, GetLong( kv.Key ) );
					break;
			}
		}

		return Extensions.EndJson();
	}

#if PUMA_DEFINE_DEBUG_TOOLS
	private void AddDebugMenu()
	{
		Func<string> debugInfoText = () =>
		{
			return SerializePlayerPrefs( true );
		};

		AmuzoDebugMenu playerPrefsFacadeMenu = new AmuzoDebugMenu( "PLAYER PREFS FACADE" );
		playerPrefsFacadeMenu.AddInfoTextFunction( debugInfoText );
		playerPrefsFacadeMenu.AddButton( new AmuzoDebugMenuButton( playerPrefsFacadeMenu, "DUMP TO FILE", () =>
		{
			DebugDumpPlayerPrefs();
		} ) );
		playerPrefsFacadeMenu.AddButton( new AmuzoDebugMenuButton( playerPrefsFacadeMenu, "DELETE ALL", () =>
		{
			DeleteAll();
		} ) );
		AmuzoDebugMenuManager.RegisterRootDebugMenu( playerPrefsFacadeMenu );
	}
#endif
	// OPERATOR OVERLOADS

}
