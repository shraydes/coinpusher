﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class PlayerPrefsFacadeEditor
{
	[MenuItem( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Player Prefs/Delete All Player Prefs" )]
	public static void StaticDeleteAll()
	{
		if( PlayerPrefsFacade._pInstance != null )
		{
			PlayerPrefsFacade._pInstance.DeleteAll();
		}
	}

	[MenuItem( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Player Prefs/Save All Player Prefs" )]
	public static void StaticSave()
	{
		if( PlayerPrefsFacade._pInstance != null )
		{
			PlayerPrefsFacade._pInstance.Save();
		}
	}

	[MenuItem( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Player Prefs/Log All Player Prefs" )]
	public static void StaticLogPlayerPrefs()
	{
		if( PlayerPrefsFacade._pInstance != null )
		{
			PlayerPrefsFacade._pInstance.DebugLogPlayerPrefs();
		}
	}

	[MenuItem( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Player Prefs/Dump All Player Prefs" )]
	public static void StaticDumpPlayerPrefs()
	{
		if( PlayerPrefsFacade._pInstance != null )
		{
			PlayerPrefsFacade._pInstance.DebugDumpPlayerPrefs();
		}
	}
}
