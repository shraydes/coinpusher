﻿////////////////////////////////////////////
// 
// ObjectSpawner.cs
//
// Created: 01/08/2014 ccuthbert
// Contributors:
// 
// Intention:
// Generic object spawner.
// Handles multiple ways of specifying spawn 
// locations.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using System.Collections.Generic;
using Logger = UnityEngine.Debug;

namespace AmuzoEngine
{

public class ObjectSpawner : MonoBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ObjectSpawner] ";
	
	//
	// SINGLETON DECLARATION
	//
	
	//
	// NESTED CLASSES / STRUCTS
	//
	
	public interface IOwner
	{
		void	OnSpawned( GameObject obj );
		void	OnDespawn( GameObject obj );
	}
	
	public interface ISpawnObject
	{
		GameObject	_pGameObject { get; }

		ObjectSpawner	_pSpawner { get; set; }

		void	OnSpawned();
		void	OnDespawn();
	}

	public interface ISpawnPoints
	{
		bool	ChooseSpawnPoint( out Vector3 position );
	}
	
	private struct ObjectData
	{
		public GameObject	_gameObject;
		public ISpawnObject	_spawnObject;
		public ObjectData( GameObject gameObject )
		{
			_gameObject = gameObject;
			_spawnObject = gameObject.GetComponent( typeof( ISpawnObject ) ) as ISpawnObject;
		}
		public void Clear()
		{
			_gameObject = null;
			_spawnObject = null;
		}
	}
	
	public class SimpleSpawnPoint : ISpawnPoints
	{
		private Vector3	_pos;
		private SimpleSpawnPoint()
		{
		}
		public SimpleSpawnPoint( Vector3 pos )
		{
			_pos = pos;
		}
		bool ISpawnPoints.ChooseSpawnPoint( out Vector3 position )
		{
			position = _pos;
			return true;
		}
	}
	
	//
	// MEMBERS 
	//
	
	public GameObject	_owner;
	
	private IOwner	_ownerInt;
	
	public GameObject	_objectInstantiator;
	
	private IObjectInstantiator	_objectInstantiatorInt;
	
	public GameObject	_objectContainer;
	
	public GameObject	_defaultSpawnPoints;
	
	private ISpawnPoints	_defaultSpawnPointsInt;
	
	private List<ObjectData>	_objects;
	
	public bool	_isFailedSpawnError;
	
	private bool	_isCheckRespawn;
	
	//
	// PROPERTIES
	//
	
	private bool	_pIsValid	{ get { return _objectInstantiatorInt != null && _objects != null; } }
	
	//
	// ABSTRACTS / VIRTUALS
	//
	
	//
	// STATIC FUNCTIONS
	//
	
	//
	// FUNCTIONS 
	//
	
	// ** UNITY
	//
	
	protected virtual void Awake()
	{
		Initialize();
	}
	
	protected virtual void Update()
	{
		UpdateObjects();
	}
	
	// ** PUBLIC
	//
	
#if UNITY_EDITOR
	// -----------------------------------------------------------------------------------------------
	[ContextMenu("Spawn")]
	public void EditorSpawn()
	{
		SpawnObject( gameObject.transform.position );
	}
#endif

	public GameObject SpawnObject( Vector3 spawnPos )
	{
		if ( !_pIsValid )
		{
			if ( _isFailedSpawnError )
			{
				Logger.LogWarning( LOG_TAG + "Cannot spawn objects", gameObject );
			}
			return null;
		}
		
		return SpawnObjectInt( spawnPos, null );
	}
	
	public void SpawnObjects( int count, Vector3 spawnPos, System.Action<GameObject> onSpawned = null )
	{
		if ( !_pIsValid )
		{
			if ( _isFailedSpawnError )
			{
				Logger.LogWarning( LOG_TAG + "Cannot spawn objects", gameObject );
			}
			return;
		}
		
		while ( count-- > 0 )
		{
			SpawnObjectInt( spawnPos, onSpawned );
		}
	}
	
	public void SpawnObjects( int count, ISpawnPoints spawnPoints = null, System.Action<GameObject> onSpawned = null )
	{
		if ( !_pIsValid )
		{
			if ( _isFailedSpawnError )
			{
				Logger.LogWarning( LOG_TAG + "Cannot spawn objects", gameObject );
			}
			return;
		}
		
		if ( spawnPoints == null )
		{
			spawnPoints = _defaultSpawnPointsInt;
		}
		
		Vector3	spawnPos;
		
		while ( count-- > 0 )
		{
			spawnPos = transform.position;
			
			if ( spawnPoints != null && !spawnPoints.ChooseSpawnPoint( out spawnPos ) )
			{
				if ( _isFailedSpawnError )
				{
					Logger.LogWarning( LOG_TAG + "Failed to choose spawn point", gameObject );
				}
				break;
			}
		
			SpawnObjectInt( spawnPos, onSpawned );
		}
	}
	
	public void DespawnObject( GameObject obj )
	{
		if ( !_pIsValid ) return;
		
		int	index = _objects.FindIndex( d => d._gameObject == obj );
		
		if ( index < 0 )
		{
			Logger.LogWarning( LOG_TAG + "Cannot despawn object", obj );
		}
		
		DespawnObject( index, true );
	}
	
#if UNITY_EDITOR
	// -----------------------------------------------------------------------------------------------
	[ContextMenu("Despawn All")]
	public void EditorDespawnAll()
	{
		DespawnAll();
	}
#endif

	public void DespawnAll()
	{
		if ( !_pIsValid ) return;
		
		for ( int i = 0; i < _objects.Count; ++i )
		{
			DespawnObject( i, false );
		}
		
		_objects.Clear();
	}
	
	// ** PROTECTED
	//
	
	// ** PRIVATE
	//
	
	private void Compose()
	{
		if ( _owner != null )
		{
			_ownerInt = _owner.GetComponent( typeof( IOwner ) ) as IOwner;
			if ( _ownerInt == null )
			{
				Logger.LogError( LOG_TAG + "Bad owner", gameObject );
			}
		}
		
		if ( _objectInstantiator != null )
		{
			_objectInstantiatorInt = _objectInstantiator.GetComponent( typeof( IObjectInstantiator ) ) as IObjectInstantiator;
			if ( _objectInstantiatorInt == null )
			{
				Logger.LogError( LOG_TAG + "Bad object instantiator", gameObject );
			}
		}
		else
		{
			Logger.LogError( LOG_TAG + "No object instantiator", gameObject );
		}
		
		if ( _defaultSpawnPoints != null )
		{
			_defaultSpawnPointsInt = _defaultSpawnPoints.GetComponent( typeof( ISpawnPoints ) ) as ISpawnPoints;
			if ( _defaultSpawnPointsInt == null )
			{
				Logger.LogError( LOG_TAG + "Bad spawn points", gameObject );
			}
		}
		
		_objects = new List<ObjectData>();
	}
	
	private void Initialize()
	{
		Compose();
		
		if ( _objectInstantiatorInt != null )
		{
			_isCheckRespawn = _objectInstantiatorInt._pCanReuseInstances;
		}
	}
	
	private GameObject SpawnObjectInt( Vector3 spawnPos, System.Action<GameObject> onSpawned )
	{
		GameObject	obj = _objectInstantiatorInt.CreateInstance() as GameObject;
		
		if ( obj == null )
		{
			if ( _isFailedSpawnError )
			{
				Logger.LogWarning( LOG_TAG + "Failed to instantiate object", gameObject );
			}
			return null;
		}
		
		ObjectData	data;
		int	index = _isCheckRespawn ? _objects.FindIndex( d => d._gameObject == obj ) : -1;
		
		if ( index < 0 )
		{
			data = new ObjectData( obj );
			
			_objects.Add( data );
		}
		else
		{
			data = _objects[ index ];
			
			NotifyDespawn( data );
		}
		
		if ( _objectContainer != null )
		{
			obj.transform.parent = _objectContainer.transform;
		}
		
		obj.transform.position = spawnPos;
		obj.SetActive( true );
		
		if ( _ownerInt != null )
		{
			_ownerInt.OnSpawned( obj );
		}
		
		if ( data._spawnObject != null )
		{
			data._spawnObject._pSpawner = this;
			data._spawnObject.OnSpawned();
		}
		
		if ( onSpawned != null )
		{
			onSpawned( obj );
		}
		
		return obj;
	}
	
	private void DespawnObject( int objIndex, bool isRemove )
	{
		ObjectData	data = _objects[ objIndex ];
		GameObject	obj = data._gameObject;
		
		NotifyDespawn( data );
		
		if ( isRemove )
		{
			_objects.RemoveAt( objIndex );
		}
		else
		{
			_objects[ objIndex ].Clear();
		}
		
		if ( obj != null )
		{
			_objectInstantiatorInt.DestroyInstance( obj );
		}
	}
	
	private void NotifyDespawn( ObjectData data )
	{
		GameObject	obj = data._gameObject;
		
		if ( obj != null )
		{
			if ( data._spawnObject != null )
			{
				data._spawnObject.OnDespawn();
				data._spawnObject._pSpawner = null;
			}
		
			if ( _ownerInt != null )
			{
				_ownerInt.OnDespawn( obj );
			}
		}
	}
	
	private void UpdateObjects()
	{
		//bool	isWantDespawn;
		
		for ( int i = 0; i < _objects.Count; ++i )
		{
			if ( _objects[i]._gameObject == null )
			{
				_objects.RemoveAt(i);
				--i;
				continue;
			}
			
			//isWantDespawn = ( _objects[i]._spawnObject != null && _objects[i]._spawnObject._pIsWantDespawn )
			//	|| ( _ownerInt != null && _ownerInt.IsWantDespawn( _objects[i]._gameObject ) );
			
			//if ( isWantDespawn )
			//{
			//	DespawnObject( i, false );
			//	_objects.RemoveAt(i);
			//	--i;
			//	continue;
			//}
		}
	}
	
	//
	// OPERATOR OVERLOADS
	//
}
	
public static class SpawnObjectUtils
{
	public static void Despawn( this ObjectSpawner.ISpawnObject spawnObj )
	{
		spawnObj._pSpawner.DespawnObject( spawnObj._pGameObject );
	}
}

}
