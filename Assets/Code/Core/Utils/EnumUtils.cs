﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EnumUtils {


	/**
	 * <summary>
	 * Returns true if successful, false if not.
	 * </summary>
	*/
	public static bool ToEnum<EnumType>(object input, ref EnumType enumerator)
	{
		try {
			enumerator = (EnumType)System.Enum.Parse(typeof(EnumType), input.ToString());
		}
		catch (ArgumentException) {
			return false;
		}
		return true;
	}



	public static bool EnumHasValue<EnumType>(object input) {
		return System.Enum.IsDefined(typeof(EnumType), input.ToString());
	}


	public static EnumType[] CreateValueArray<EnumType>( EnumType[] ignoreValues = null ) where EnumType : struct, System.IConvertible
	{
		System.Type	enumType = typeof( EnumType );
		EnumType[]	enumValues = System.Enum.GetValues( enumType ) as EnumType[];
		int			enumCount = enumValues.Length;

		List<EnumType>	valueList = new List<EnumType>( enumCount );

		for ( int i = 0; i < enumCount; ++i )
		{
			if ( !valueList.Contains( enumValues[i] ) && ( ignoreValues == null || !ignoreValues.Contains( enumValues[i] ) ) )
			{
				valueList.Add( enumValues[i] );
			}
		}

		return valueList.ToArray();
	}
	
	public class FromStringMap<EnumType> where EnumType : struct, System.IConvertible
	{
		private Dictionary<string, EnumType>	_dict;
		
		private EnumType	_defaultValue;
		
		private bool	_isCaseSensitive;
		
		private void Initialize()
		{
			
			System.Type	enumType = typeof( EnumType );
			string[]	enumNames = System.Enum.GetNames( enumType );
			EnumType[]	enumValues = System.Enum.GetValues( enumType ) as EnumType[];
			int			enumCount = enumNames.Length;
			
			_dict = new Dictionary<string, EnumType>( enumCount );
			
			for ( int i = 0; i < enumCount; ++i )
			{
//				Debug.Log( enumNames[i] + "-->" + enumValues[i].ToString() );
				if ( _isCaseSensitive )
				{
					_dict.Add( enumNames[i], enumValues[i] );
				}
				else
				{
					_dict.Add( enumNames[i].ToUpper(), enumValues[i] );
				}
			}
		}
		
		public FromStringMap()
		{
			_isCaseSensitive = true;
			Initialize();
			_defaultValue = default( EnumType );
		}
		
		public FromStringMap( bool isCaseSensitive )
		{
			_isCaseSensitive = isCaseSensitive;
			Initialize();
			_defaultValue = default( EnumType );
		}
		
		public FromStringMap( EnumType defaultValue )
		{
			_isCaseSensitive = true;
			Initialize();
			_defaultValue = defaultValue;
		}
		
		public FromStringMap( EnumType defaultValue, bool isCaseSensitive )
		{
			_isCaseSensitive = isCaseSensitive;
			Initialize();
			_defaultValue = defaultValue;
		}
		
		public EnumType Lookup( string name )
		{
			if ( _dict == null ) return _defaultValue;
			if ( !_isCaseSensitive )
			{
				name = name.ToUpper();
			}
			if ( !_dict.ContainsKey( name ) ) return _defaultValue;
			return _dict[ name ];
		}
		
		public bool TryLookup( string name, out EnumType value )
		{
			value = _defaultValue;
			if ( _dict == null ) return false;
			if ( !_isCaseSensitive )
			{
				name = name.ToUpper();
			}
			if ( !_dict.ContainsKey( name ) ) return false;
			value = _dict[ name ];
			return true;
		}
	}
}
