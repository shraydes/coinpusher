﻿////////////////////////////////////////////
// 
// ObjectInstantiator.cs
//
// Created: 01/08/2014 ccuthbert
// Contributors:
// 
// Intention:
// Common interface to instantiate objects.
// Intended to wrap object pool systems and 
// normal instantiation (via Object.Instantiate).
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using Logger = UnityEngine.Debug;

namespace AmuzoEngine
{

public interface IObjectInstantiator
{
	Object	CreateInstance();
	void	DestroyInstance( Object inst );
	bool	_pCanReuseInstances	{ get; }
}

}
