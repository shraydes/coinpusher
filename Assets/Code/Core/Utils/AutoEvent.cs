﻿////////////////////////////////////////////
// 
// AutoEvent.cs
//
// Created: 05/08/2014 ccuthbert
// Contributors:
// 
// Intention:
// Autonomous Event.  Work without an 'event 
// management' system.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
//
// CC20170921 - changes:
//	* _pCallbacks --> _pOnEvent
//	* added _pOnNextEvent
//
////////////////////////////////////////////
 
namespace AmuzoEngine
{

public class AutoEvent
{
	public event System.Action	_pOnEvent;
	public event System.Action	_pOnNextEvent;
	public void	OnEvent()
	{
		if ( _pOnEvent != null )
		{
			_pOnEvent();
		}
		if ( _pOnNextEvent != null )
		{
			System.Action	cachedAction = _pOnNextEvent;
			_pOnNextEvent = null;
			cachedAction();
		}
	}
}

public class AutoEvent<TArg>
{
	public event System.Action<TArg>	_pOnEvent;
	public event System.Action<TArg>	_pOnNextEvent;
	public void	OnEvent( TArg arg )
	{
		if ( _pOnEvent != null )
		{
			_pOnEvent( arg );
		}
		if ( _pOnNextEvent != null )
		{
			System.Action<TArg>	cachedAction = _pOnNextEvent;
			_pOnNextEvent = null;
			cachedAction( arg );
		}
	}
}

public class AutoEvent<TArg1,TArg2>
{
	public event System.Action<TArg1,TArg2>	_pOnEvent;
	public event System.Action<TArg1,TArg2>	_pOnNextEvent;
	public void	OnEvent( TArg1 arg1, TArg2 arg2 )
	{
		if ( _pOnEvent != null )
		{
			_pOnEvent( arg1, arg2 );
		}
		if ( _pOnNextEvent != null )
		{
			System.Action<TArg1,TArg2>	cachedAction = _pOnNextEvent;
			_pOnNextEvent = null;
			cachedAction( arg1, arg2 );
		}
	}
}

public class AutoEvent<TArg1,TArg2,TArg3>
{
	public event System.Action<TArg1,TArg2,TArg3>	_pOnEvent;
	public event System.Action<TArg1,TArg2,TArg3>	_pOnNextEvent;
	public void	OnEvent( TArg1 arg1, TArg2 arg2, TArg3 arg3 )
	{
		if ( _pOnEvent != null )
		{
			_pOnEvent( arg1, arg2, arg3 );
		}
		if ( _pOnNextEvent != null )
		{
			System.Action<TArg1,TArg2,TArg3>	cachedAction = _pOnNextEvent;
			_pOnNextEvent = null;
			cachedAction( arg1, arg2, arg3 );
		}
	}
}

public class AutoEvent<TArg1,TArg2,TArg3,TArg4>
{
	public event System.Action<TArg1,TArg2,TArg3,TArg4>	_pOnEvent;
	public event System.Action<TArg1,TArg2,TArg3,TArg4>	_pOnNextEvent;
	public void	OnEvent( TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4 )
	{
		if ( _pOnEvent != null )
		{
			_pOnEvent( arg1, arg2, arg3, arg4 );
		}
		if ( _pOnNextEvent != null )
		{
			System.Action<TArg1,TArg2,TArg3,TArg4>	cachedAction = _pOnNextEvent;
			_pOnNextEvent = null;
			cachedAction( arg1, arg2, arg3, arg4 );
		}
	}
}

}
