﻿////////////////////////////////////////////
// 
// SimpleStates.cs
//
// Created: 21/11/2014 ccuthbert
// Contributors:
// 
// Intention:
// Class to implement simple state system.
// Alternative to boiler-plate state code.
//
// Note: 
// EStateType is user-defined enum.
// States can have begin, end and update 
// event callbacks.
// Use _pOnBeginState, _pOnEndState and 
// _pOnUpdateState to set the event callbacks.
// Use _pState to set state, thus triggering 
// begin/end events.
// Call OnUpdate to trigger update events.
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using System.Collections.Generic;
using Logger = NoDebugLogger;

namespace AmuzoEngine
{

public class SimpleStates<EStateType> where EStateType : struct, System.IConvertible
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[SimpleStates] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
	
	public delegate void	DOnBeginState();
	public delegate void	DOnUpdateState( float dt );
	public delegate void	DOnEndState();
	public delegate void	DOnStateChanged( EStateType oldState, EStateType newState );
	
	public interface IStateEvents<DStateEventType>
	{
		DStateEventType	this[ EStateType state ]	{ set; }
	}
	
	private class StateEvents<DStateEventType> : IStateEvents<DStateEventType>
	{
		public delegate int	DGetStateIndex( EStateType state );
		private DStateEventType[]	_events;
		private DGetStateIndex	_getStateIndex;
		public StateEvents( int stateCount, DGetStateIndex getStateIndex )
		{
			_events = new DStateEventType[ stateCount ];
			_getStateIndex = getStateIndex;
		}
		public DStateEventType	this[ int stateIndex ]
		{
			get	{ return _events[ stateIndex ]; }
		}
		DStateEventType	IStateEvents<DStateEventType>.this[ EStateType state ]
		{
			set { _events[ _getStateIndex( state ) ] = value; }
		}
	}
	
	private class OnBeginStateEvents : StateEvents<DOnBeginState>
	{
		public OnBeginStateEvents( int stateCount, DGetStateIndex getStateIndex ) : base( stateCount, getStateIndex )
		{
		}
	}
			
	private class OnUpdateStateEvents : StateEvents<DOnUpdateState>
	{
		public OnUpdateStateEvents( int stateCount, DGetStateIndex getStateIndex ) : base( stateCount, getStateIndex )
		{
		}
	}
			
	private class OnEndStateEvents : StateEvents<DOnEndState>
	{
		public OnEndStateEvents( int stateCount, DGetStateIndex getStateIndex ) : base( stateCount, getStateIndex )
		{
		}
	}
			
	//
	// MEMBERS 
	//
	
	private EStateType[]	_stateValues;
	
//	private string[]	_stateNames;
	
	private int	_stateCount;
	
	private Dictionary<EStateType, int>	_stateIndices;
	
	private int	_defaultStateIndex;
	
	private int	_currStateIndex;
	
	private int	_prevStateIndex = -1;
	
	private OnBeginStateEvents	_onBeginStateEvents;
	
	private OnUpdateStateEvents	_onUpdateStateEvents;
	
	private OnEndStateEvents	_onEndStateEvents;
	
	private event DOnStateChanged	_onStateChanged;
	
	//
	// PROPERTIES
	//
	
	public EStateType	_pState
	{
		get { return _stateValues[ _currStateIndex ]; }
		set { OnSetState( _stateIndices[ value ], false ); }
	}
	
	public bool	_pHasPreviousState
	{
		get	{ return _prevStateIndex >= 0; }
	}
	
	public EStateType	_pPreviousState
	{
		get { return _stateValues[ _prevStateIndex ]; }
	}
	
	public IStateEvents<DOnBeginState>	_pOnBeginState
	{
		get { return _onBeginStateEvents; }
	}
	
	public IStateEvents<DOnUpdateState>	_pOnUpdateState
	{
		get { return _onUpdateStateEvents; }
	}
	
	public IStateEvents<DOnEndState>	_pOnEndState
	{
		get { return _onEndStateEvents; }
	}
	
	public event DOnStateChanged _pOnStateChanged
	{
		add		{ _onStateChanged += value; }
		remove	{ _onStateChanged -= value; }
	}
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	public SimpleStates( EStateType defaultState )
	{
		Initialize();
		
		_defaultStateIndex = _stateIndices[ defaultState ];
		
		ResetState();
	}
	
	public void ResetState()
	{
		OnSetState( _defaultStateIndex, true );
	}
	
	public void OnUpdate( float dt )
	{
		OnUpdateState( _currStateIndex, dt );
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private SimpleStates()
	{
	}
	
	private void Initialize()
	{
		System.Array	values = System.Enum.GetValues( typeof( EStateType ) );
		
		_stateCount = values.Length;
		_stateValues = new EStateType[ _stateCount ];
		_stateIndices = new Dictionary<EStateType, int>( _stateCount );
		
		for ( int i = 0; i < _stateCount; ++i )
		{
			_stateValues[i] = (EStateType)values.GetValue(i);
			_stateIndices.Add( _stateValues[i], i );
		}
		
//		_stateNames = System.Enum.GetNames( typeof( EStateType ) );

		_onBeginStateEvents = new OnBeginStateEvents( _stateCount, GetStateIndex );
		_onUpdateStateEvents = new OnUpdateStateEvents( _stateCount, GetStateIndex );
		_onEndStateEvents = new OnEndStateEvents( _stateCount, GetStateIndex );
	}
	
	private int GetStateIndex( EStateType state )
	{
		return _stateIndices[ state ];
	}
	
	private void OnSetState( int newStateIndex, bool isForce )
	{
		if ( _currStateIndex == newStateIndex && !isForce ) return;
		
		OnEndState( _currStateIndex );
		
		_prevStateIndex = _currStateIndex;
		_currStateIndex = newStateIndex;
		
		OnBeginState( _currStateIndex );
		
		if ( _onStateChanged != null )
		{
			_onStateChanged( _stateValues[ _prevStateIndex ], _stateValues[ newStateIndex ] );
		}
	}
	
	private void OnBeginState( int stateIndex )
	{
		DOnBeginState	onBeginState = _onBeginStateEvents[ stateIndex ];
		
		if ( onBeginState != null )
		{
			onBeginState();
		}
	}
	
	private void OnUpdateState( int stateIndex, float dt )
	{
		DOnUpdateState	onUpdateState = _onUpdateStateEvents[ stateIndex ];
		
		if ( onUpdateState != null )
		{
			onUpdateState( dt );
		}
	}
	
	private void OnEndState( int stateIndex )
	{
		DOnEndState	onEndState = _onEndStateEvents[ stateIndex ];
		
		if ( onEndState != null )
		{
			onEndState();
		}
	}
	
	//
	// OPERATOR OVERLOADS
	//
}

}
