﻿////////////////////////////////////////////
// 
// JobCounter.cs
//
// Created: 26/05/2015 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// Refactored from FullScreenMovieFacade
// CC20160322:
// - added 'onBegin'
// - renamed 'onComplete' -> 'onEnd'
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
namespace AmuzoEngine
{

public class JobCounter
{
	private int	_count;
	public bool _pHasJobs
	{
		get
		{
			return _count > 0;
		}
	}
	private System.Action	_onBegin;
	private System.Action	_onEnd;
	private System.Action	_onNextEnd;
	public JobCounter( System.Action onBegin, System.Action onEnd )
	{
		_count = 0;
		_onBegin = onBegin;
		_onEnd = onEnd;
		_onNextEnd = null;
	}
	public JobCounter( System.Action onEnd )
	{
		_count = 0;
		_onBegin = null;
		_onEnd = onEnd;
		_onNextEnd = null;
	}
	public void BeginJob( System.Action onEnd = null )
	{
		if ( _count == 0 )
		{
			_onNextEnd = null;

			if ( _onBegin != null )
			{
				_onBegin();
			}
		}
		if ( onEnd != null )
		{
			_onNextEnd += onEnd;
		}
		++_count;
	}
	public void EndJob()
	{
		if ( _count <= 0 ) return;
		--_count;
		if ( _count == 0 )
		{
			if ( _onNextEnd != null )
			{
				System.Action	onNextEnd = _onNextEnd;
				_onNextEnd = null;
				onNextEnd();
			}
			if ( _onEnd != null )
			{
				_onEnd();
			}
		}
	}
}

}
