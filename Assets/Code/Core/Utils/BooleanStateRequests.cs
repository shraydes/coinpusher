﻿////////////////////////////////////////////
// 
// BooleanStateRequests.cs
//
// Created: 10/04/2015 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using System.Collections.Generic;
using Logger = UnityEngine.Debug;
using Verbose = NoDebugLogger;

//namespace AmuzoEngine
//{

public class BooleanStateRequests
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[BooleanStateRequests] ";
	
	public const int	DEFAULT_PRIORITY = 0;
	
	[System.Flags]
	public enum ERequestField
	{
		NULL = 0,
		SOURCE = 1,
		PRIORITY = 2,
		STATE = 4,
		
		DEFAULT_ID_FIELDS = SOURCE
	}

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
	
	public delegate void	DSetState( bool value );
	
	public delegate bool	DGetState();
	
	public class Context
	{
		private string	_name;
		public Context()
		{
			_name = "Anon";
		}
		public Context( string name )
		{
			_name = name;
		}
		public override string ToString ()
		{
			return _name;
		}
	}
	
	private class Request
	{
		public object	_source;
		public int		_priority;
		public bool		_state;
		public Request( object source, int priority, bool state )
		{
			_source = source;
			_priority = priority;
			_state = state;
		}
	}
	
	//
	// MEMBERS 
	//
	
	private DSetState	_setState = null;
	
	private DGetState	_getState = null;
	
	private bool	_defaultState = false;
	
	private ERequestField	_requestIdFields = ERequestField.NULL;
	
	private LinkedList<Request>	_requests = null;
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	public BooleanStateRequests( DSetState setState, DGetState getState, bool defaultState, ERequestField requestIdFields = ERequestField.DEFAULT_ID_FIELDS )
	{
		_setState = setState;
		_getState = getState;
		_defaultState = defaultState;
		_requestIdFields = requestIdFields;
		
		ResetState();
	}

	public void SetDefaultState( bool defaultState )
	{
		_defaultState = defaultState;

		RefreshState();
	}
	
	public void RequestState( bool wantRequest, object source, int priority, bool wantState )
	{
		if ( wantRequest )
		{
			AddStateRequest( source, priority, wantState );
		}
		else
		{
			RemoveStateRequest( source, priority, wantState );
		}
	}

    public void Clear()
    {
        ResetState();
    }
	
	public void ForEachStateRequest( System.Action<object, int, bool> action )
	{
		if ( action == null ) return;
		
		LinkedListNode<Request>	node = _requests.First;
		LinkedListNode<Request>	nextNode;
		
		for ( ; node != null; node = nextNode )
		{
			nextNode = node.Next;
			
			action( node.Value._source, node.Value._priority, node.Value._state );
		}
	}
	
	#if DEBUG_FEATURES
	public void DebugCheckState( string context )
	{
		CheckState( wantState => {
			Logger.LogWarning( LOG_TAG + context + " has wrong state: " + (!wantState).ToString() );
		}, false );
	}
	#endif
	
	// ** PROTECTED
	//
	
	protected virtual bool ResolveConflict( int priority )
	{
		return _defaultState;
	}

	// ** PRIVATE
	//
	
	private BooleanStateRequests()
	{
	}
	
	private void ResetState()
	{
		_requests = new LinkedList<Request>();
		
		RefreshState();
	}
	
	private void AddStateRequest( object source, int priority, bool state )
	{
		if ( source == null )
		{
			Logger.LogError( LOG_TAG + "Can not add request with null source!" );
			return;
		}
		
		bool	isChanged = false;
		LinkedListNode<Request>	node = FindRequest( source, priority, state );
		
		if ( node != null )
		{
			Request	request = node.Value;
			
			if ( request._source != source )
			{
				request._source = source;
				isChanged = true;
			}
			
			if ( request._state != state )
			{
				request._state = state;
				isChanged = true;
			}
			
			if ( request._priority != priority )
			{
				request._priority = priority;
				isChanged = true;
			}
		}
		else
		{
			_requests.AddLast( new Request( source, priority, state ) );
			isChanged = true;
		}
		
		if ( isChanged )
		{
			RefreshState();
		}
	}
	
	private void RemoveStateRequest( object source, int priority, bool state )
	{
		if ( source == null )
		{
			Logger.LogWarning( LOG_TAG + "Removing request with null source!" );
		}
		
		LinkedListNode<Request>	node = FindRequest( source, priority, state );
		
		if ( node != null )
		{
			_requests.Remove( node );
			RefreshState();
		}
	}
	
	private LinkedListNode<Request> FindRequest( object source, int priority, bool state )
	{
		LinkedListNode<Request>	node = _requests.First;
		for ( ; node != null; node = node.Next )
		{
			if ( ( _requestIdFields & ERequestField.SOURCE ) != ERequestField.NULL && node.Value._source != source ) continue;
			if ( ( _requestIdFields & ERequestField.PRIORITY ) != ERequestField.NULL && node.Value._priority != priority ) continue;
			if ( ( _requestIdFields & ERequestField.STATE ) != ERequestField.NULL && node.Value._state != state ) continue;
			return node;
		}
		return null;
	}
	
	private bool GetDesiredState( bool isRefreshing )
	{
		if ( _requests == null || _requests.Count == 0 ) return _defaultState;
		
		int		priority;
		int		highestPriority = int.MinValue;
		int		highestPriorityCount = 0;
		bool	desiredState = _defaultState;
		bool	isConflict = false;
		bool	isChanged = false;
		
		LinkedListNode<Request>	node = _requests.First;
		LinkedListNode<Request>	nextNode;
		
		for ( ; node != null; node = nextNode )
		{
			nextNode = node.Next;
			
			if ( node.Value == null || node.Value._source == null )
			{
				_requests.Remove( node );
				isChanged = true;
				continue;
			}
			
			priority = node.Value._priority;
			
			if ( priority > highestPriority )
			{
				desiredState = node.Value._state;
				highestPriority = priority;
				highestPriorityCount = 1;
				isConflict = false;
			}
			else if ( priority == highestPriority )
			{
				++highestPriorityCount;
				
				if ( node.Value._state != desiredState )
				{
					isConflict = true;
				}
			}
		}
		
		if ( isConflict )
		{
			desiredState = ResolveConflict( highestPriority );
		}
		
		if ( isChanged && !isRefreshing )
		{
			RefreshState();
		}
		
		return desiredState;
	}
	
	private void CheckState( System.Action<bool> onWrongState, bool isRefreshing )
	{
		bool	wantState = GetDesiredState( isRefreshing );
		
		if ( wantState != _getState() )
		{
			onWrongState( wantState );
		}
	}
	
	private void RefreshState()
	{
		CheckState( wantState => {
			Verbose.Log( LOG_TAG + "Setting state: " + wantState.ToString() );
			_setState( wantState );
		}, true );
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

//}
