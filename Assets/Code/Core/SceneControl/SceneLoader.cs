#if PUMA_DEFINE_DLC
#define ENABLE_DOWNLOAD_SCENE
#endif

using UnityEngine;
#if UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using System.Collections;
using System.Collections.Generic;
#if !UNITY_WEBPLAYER
using System.IO;
#endif

public class SceneLoader : MonoBehaviour, InitialisationProxy.ITarget 
{
	static bool DO_DEBUG = true;
	const int DELAY_FRAMES = 2;

	public string levelName;
	public bool assetbundle = false;
	public bool forceLoadIfAlreadyLoaded = false;
	public bool loadAdditively = false;
	public bool	loadAsync = true;
	public bool	_isClearResourcesOnLoadComplete = false;

	AsyncOperation levelLoader;

	[SerializeField]
	private bool	_dontDestroyOnLoad = true;

	int _delayFrames = DELAY_FRAMES;
	
	public bool	isLoading	{ get { return levelLoader != null && !levelLoader.isDone; } }
	public float	_pLoadingProgress	{ get { return levelLoader != null ? levelLoader.progress : 0f; } }

	InitialisationState	_initialisationState;

	InitialisationState InitialisationProxy.ITarget._pInitialisationState
	{
		get
		{
			return _initialisationState;
		}

		set
		{
			_initialisationState = value;
		}
	}

	System.Action<bool>	_onLoadCompleteAction;

	private static List<string>	_sLoadedSceneNames = new List<string>();

	public static string _pLastLoadedSceneName
	{	
		get
		{
			return _sLoadedSceneNames.Count > 0 ? _sLoadedSceneNames[ _sLoadedSceneNames.Count - 1 ] : null;
		}
	}
	
	// we are now releasing bundles, so they will need to be reloaded ...
	//static List<string> loadedSceneAssetBundleNames = new List<string>();

	void Awake()
	{
		if ( _dontDestroyOnLoad )
		{
			DontDestroyOnLoad( gameObject );
		}
	}
	
	public bool AttemptLoadLevel( System.Action<bool> onLoadComplete = null )
	{
		Log("Attempting to load level [" + levelName +"]");
		
		_delayFrames = DELAY_FRAMES;
		
		SafeCallOnLoadCompleteAction( false );
			
		_onLoadCompleteAction = onLoadComplete;
			
		if ( IsSceneLoaded( levelName ) && !forceLoadIfAlreadyLoaded )
		{
			Log("Level loading not required.");
			SafeCallOnLoadCompleteAction( true );
			return false;
		}

		if(assetbundle)// && !loadedSceneAssetBundleNames.Contains(levelName))
		{
#if !UNITY_WEBPLAYER
			if( !File.Exists( streamingFilePath() ) && !File.Exists( persistantFilePath() ) )
			{
				Log("Unable to find scene bundle at path [" + streamingFilePath() + "]");
#if ENABLE_DOWNLOAD_SCENE					
				if( Facades<DLCFacade>.Instance != null )
				{
					Facades<DLCFacade>.Instance.downloadAsset( levelName, AssetType.LEVEL_BUNDLE, downloadComplete, downloadFailed ); 
				}
				else
#endif
				{
					Log("Could not load level bundle");
					return false;
				}
			}
			else
			{
				Log("Loading scene from file.");
				StartCoroutine(LoadSceneBundleAsync());
			}			
#else
			StartCoroutine(LoadSceneBundleAsync());
#endif
				
		}
		else
		{
			Log("Loading level.");
			LoadLevel(null);
		}
			
		return true;
	}
	
#if ENABLE_DOWNLOAD_SCENE
	void downloadComplete( DownloadRequest assetBundleRequest )
	{	
		if(assetBundleRequest._pHasResponse )
		{
			StartCoroutine( LoadLevelAsync( assetBundleRequest._pAssetBundle ) );
		}
		else
		{
			// This means the bundle has been downloaded and already been disposed of. So we need to retreive it from local storage again.
			LoadSceneBundleAsync();
		}
		
		assetBundleRequest.ClearDownload();
	}
	
	void downloadFailed( DownloadRequest assetBundleRequest )
	{
		if( assetBundleRequest._pHasResponse )
		{
			assetBundleRequest.ClearDownload();
		}
		
		Log("Could not load level bundle");
	}
#endif

	void LoadLevel( AssetBundle bundle )
	{
		if ( loadAsync )
		{
			StartCoroutine( LoadLevelAsync( null ) );
		}
		else
		{
			LoadLevelBlocking( bundle );
		}
	}
		
	void LoadLevelBlocking(AssetBundle bundle)
	{
		if(loadAdditively)
		{
			Log("Loading scene additively: " + levelName);
#if UNITY_5_3_OR_NEWER
			SceneManager.LoadScene( levelName, LoadSceneMode.Additive );
#else
			Application.LoadLevelAdditive(levelName);
#endif
		}
		else
		{
			Log("Loading scene now: " + levelName);
#if UNITY_5_3_OR_NEWER
			SceneManager.LoadScene( levelName, LoadSceneMode.Single );
#else
			Application.LoadLevel(levelName);
#endif
		}
		
		if(bundle != null)
		{
			bundle.Unload(false);
		}

		if ( _isClearResourcesOnLoadComplete )
		{
			Log( "Unloading unused assets" );

			AsyncOperation	unloadOp = Resources.UnloadUnusedAssets();

			while ( !unloadOp.isDone );
		}
		
		OnPostSceneLoad();
	}

	IEnumerator LoadLevelAsync(AssetBundle bundle)
	{
		while( _delayFrames > 0 )
		{
			_delayFrames--;
			yield return new WaitForEndOfFrame();
		}

		if(loadAdditively)
		{
			Log("Loading scene additively: " + levelName);
#if UNITY_5_3_OR_NEWER
			levelLoader = SceneManager.LoadSceneAsync( levelName, LoadSceneMode.Additive );
#else
			levelLoader = Application.LoadLevelAdditiveAsync(levelName);
#endif
		}
		else
		{
			Log("Loading scene now: " + levelName);
#if UNITY_5_3_OR_NEWER
			levelLoader = SceneManager.LoadSceneAsync( levelName, LoadSceneMode.Single );
#else
			levelLoader = Application.LoadLevelAsync(levelName);
#endif
		}
		
		yield return levelLoader;
		
		if(bundle != null)
		{
			yield return new WaitForEndOfFrame();
			bundle.Unload(false);
		}

		if ( _isClearResourcesOnLoadComplete )
		{
			Log( "Unloading unused assets" );

			yield return Resources.UnloadUnusedAssets();

			yield return new WaitForSeconds( 0.1f );
		}
		
		OnPostSceneLoad();

		levelLoader = null;
	}

	private void OnPostSceneLoad()
	{
		if ( loadAdditively )
		{
			AddLoadedScene( levelName );
		}
		else
		{
			SetLoadedScene( levelName );
		}

#if UNITY_5_3_OR_NEWER
		Log( "Scene loading complete. Current level name [" + SceneManager.GetActiveScene() +"]" );
#else
		Log( "Scene loading complete. Current level name [" + Application.loadedLevelName +"]" );
#endif

//		if(Facades<GUIRenderFacade>.Instance != null)
//		{
//			Facades<GUIRenderFacade>.Instance.EnsureNonGUICamerasAreNotRenderingGUILayer();
//		}

		SafeCallOnLoadCompleteAction( true );
	}
	
	private void SafeCallOnLoadCompleteAction( bool success )
	{
		if ( _onLoadCompleteAction == null ) return;
		
		System.Action<bool>	cachedAction = _onLoadCompleteAction;
		
		_onLoadCompleteAction = null;
		
		cachedAction( true );
	}
	
	void UpdateLoadingFeedback()
	{
		// ?
	}
	
	IEnumerator LoadSceneBundleAsync()
	{
		string path = streamingFilePath();
		
#if !UNITY_WEBPLAYER
		if( !File.Exists( streamingFilePath() ) )
		{
			path = persistantFilePath();
		}
		
		path = FileHelper.AddLocalFileUrlScheme( path );
#endif
		
		Log("Retrieving [" + path + "]");

		DownloadRequest	download = new DownloadRequest_LocalOnly();

		download.requestUrl = path;
		download.SetAssetType( EDownloadAssetType.ASSET_BUNDLE );

		int	downloadId = GlobalDownloadQueue._pInstance.Download( download );

		yield return new WaitUntil( () => download._pCurrentStatus == DownloadStatus.SUCCEEDED || download._pCurrentStatus == DownloadStatus.FAILED || download._pCurrentStatus == DownloadStatus.ERROR );
			
		if( download._pCurrentStatus == DownloadStatus.SUCCEEDED && download._pAssetBundle != null )
		{
			//loadedSceneAssetBundleNames.Add (levelName);
			AssetBundle bundle = download._pAssetBundle;
			
			if(bundle != null)
			{
				Log("SceneLoader: Level loaded from file. Loading scene");
				StartCoroutine(LoadLevelAsync(bundle));
			}
			else
			{
				Log("Could not load level bundle");
			}
		}
		else
		{
			Log("Unable to download: " + path);
		}	
	}
	
	string GetPlatformFolder()
	{
#if UNITY_IPHONE
		return "iPhone";
#elif UNITY_ANDROID
		return "Android";
#elif UNITY_METRO
		return "MetroPlayer";
#else
		return "WebPlayer";
#endif
	}
	
	string streamingFilePath()
	{
		return Application.streamingAssetsPath + "/" + GetPlatformFolder() + "/" + levelName + ".assetbundle";
	}
	
	string persistantFilePath()
	{
		return Application.persistentDataPath + "/" + GetPlatformFolder() + "/" + levelName + ".assetbundle";
	}

	private static void CheckInitLoadedScenes()
	{
		if ( _sLoadedSceneNames == null )
		{
			_sLoadedSceneNames = new List<string>(1);
		}
	}

	private static void SetLoadedScene( string sceneName )
	{
		CheckInitLoadedScenes();
		_sLoadedSceneNames.Clear();
		_sLoadedSceneNames.Add( sceneName );
	}

	private static void AddLoadedScene( string sceneName )
	{
		CheckInitLoadedScenes();
		int	idx = _sLoadedSceneNames.FindIndex( sn => sn == sceneName );

		if ( idx < 0 )
		{
			_sLoadedSceneNames.Add( sceneName );
		}
		else if ( idx < _sLoadedSceneNames.Count - 1 )
		{
			_sLoadedSceneNames.RemoveAt( idx );
			_sLoadedSceneNames.Add( sceneName );
		}
	}

	public static bool IsSceneLoaded( string sceneName )
	{
		return _sLoadedSceneNames != null && _sLoadedSceneNames.Contains( sceneName );
	}

	public static bool IsSceneLoadedLast( string sceneName )
	{
		return _sLoadedSceneNames != null && _sLoadedSceneNames.Count > 0 && _sLoadedSceneNames[ _sLoadedSceneNames.Count - 1 ] == sceneName;
	}

	// *** Debugging ***
	
	public static void Log(string message, UnityEngine.Object o = null)
	{
		if( !DO_DEBUG )
		{
			return;
		}
	#if DEBUG_MESSAGES
		#if UNITY_EDITOR
		Debug.Log ( "<color=#CC0000><b>SceneLoader[" + Time.realtimeSinceStartup.ToString( "F3" ) + "] : </b></color>" + message, o);
		#else
		Debug.Log ( "SceneLoader: " + message, o);
		#endif
	#endif
	}

	void InitialisationProxy.ITarget.OnStartInitialising()
	{
		_initialisationState = InitialisationState.INITIALISING;

		bool	isLoadingLevel = AttemptLoadLevel( success => {
			_initialisationState = InitialisationState.FINISHED;
		} );

		if ( !isLoadingLevel )
		{
			_initialisationState = InitialisationState.FINISHED;
		}
	}

	InitialisationState InitialisationProxy.ITarget.OnUpdateInitialising()
	{
		return _initialisationState;
	}
}
