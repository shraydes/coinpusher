﻿////////////////////////////////////////////
// 
// DefaultSceneLoader.cs
//
// Created: 10/05/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public static class DefaultSceneLoader
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[DefaultSceneLoader] ";

	//
	// SINGLETON DECLARATION
	//

	private static SceneLoader	_instance;

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public static bool LoadLevel( string sceneName, bool isAsync = true, bool isAdditive = false, bool isReload = false, System.Action onComplete = null, bool isWaitForSceneInit = false )
	{
		if ( string.IsNullOrEmpty( sceneName ) ) return false;

		CheckCreateInstance();

		_instance.levelName = sceneName;
		_instance.assetbundle = false;
		_instance.forceLoadIfAlreadyLoaded = isReload;
		_instance.loadAdditively = isAdditive;
		_instance.loadAsync = isAsync;

		return _instance.AttemptLoadLevel( onLoadComplete:( bool isSuccess ) => {
			if ( onComplete != null )
			{
				if ( isSuccess && isWaitForSceneInit && InitialisationFacade._pExists && !InitialisationFacade._pInstance._pHasFinished )
				{
					InitialisationFacade._pInstance._pOnFinished += () => {
						onComplete();
					};
				}
				else
				{
					onComplete();
				}
			}
		} );
	}

	public static bool ClearScene( bool isAsync = true, System.Action onComplete = null )
	{
		return LoadLevel( "Empty", isAsync:isAsync, onComplete:onComplete );
	}
	
	public static bool ReloadLevel( System.Action onComplete = null, bool isWaitForSceneInit = false )
	{
		return LoadLevel( SceneLoader._pLastLoadedSceneName, isReload:true, onComplete:onComplete, isWaitForSceneInit:isWaitForSceneInit );
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static void CheckCreateInstance()
	{
		if ( _instance != null ) return;
		GameObject	go = new GameObject( "DefaultSceneLoader" );
		Object.DontDestroyOnLoad( go );
		_instance = go.AddComponent<SceneLoader>();
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

