﻿////////////////////////////////////////////
// 
// ScreenAnimatorStateBehaviour.cs
//
// Created: 02/05/2019 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_ScreenAnimatorStateBehaviour
#endif

using UnityEngine;
using AmuzoEngine;

public class ScreenAnimatorStateBehaviour : StateMachineBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ScreenAnimatorStateBehaviour] ";

	public enum EAnimationType
	{
		NULL = 0,

		TWEEN_IN = 1,
		TWEEN_OUT = 2
	}

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//

	//
	// MEMBERS 
	//

	[SerializeField]
	private EAnimationType	_animationType;

	//
	// PROPERTIES
	//

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		base.OnStateExit(animator, stateInfo, layerIndex);

		ScreenAnimator	screenAnimator = animator.GetComponent<ScreenAnimator>();

		if ( screenAnimator == null )
		{
			Debug.LogError( LOG_TAG + "Expected ScreenAnimator component" );
			return;
		}

		switch ( _animationType )
		{
		case EAnimationType.TWEEN_IN:
			screenAnimator.ANIM_EVENT_EndTweenIn();
			break;
		case EAnimationType.TWEEN_OUT:
			screenAnimator.ANIM_EVENT_EndTweenOut();
			break;
		}
	}

	// ** PUBLIC
	//

	// ** INTERNAL
	//

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

