﻿#if PUMA_DEFINE_ASSET_BUNDLE_DOWNLOADER
#define ENABLE_DOWNLOADER
#endif

////////////////////////////////////////////
// 
// ScreenDownloader.cs
//
// Created: 21/11/2016 ccuthbert
// Contributors:
// 
// Intention:
// Script to manage downloading of screen 
// prefabs as asset bundle.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections;
using System.IO;
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

#if ENABLE_DOWNLOADER
using BaseBehaviour = AssetBundleDownloaderBehaviour;
#else
using BaseBehaviour = UnityEngine.MonoBehaviour;
#endif

public class ScreenDownloader : BaseBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ScreenDownloader] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//

	//
	// MEMBERS 
	//

	[SerializeField]
	private ScreenLoader	_loader;

	//
	// PROPERTIES
	//

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

#if ENABLE_DOWNLOADER
	protected override void Awake()
	{
		base.Awake();

		SetLoadAssetHandler( ( assetName, setProgress, onComplete ) => {
			GameObject	asset = _pAssetBundle.LoadAsset<GameObject>( assetName );
			if ( asset != null )
			{
				ScreenBase	screen = asset.GetComponent<ScreenBase>();
				if ( screen != null )
				{
					_loader.screensToLoad.Add( new ScreenLoaderInfo( screen ) );
				}
				else
				{
					LoggerAPI.LogError( LOG_TAG + string.Format( "Failed to find screen component on asset: {0}", assetName ) );
				}
			}
			else
			{
				LoggerAPI.LogError( LOG_TAG + string.Format( "Failed to load asset: {0}", assetName ) );
			}
			onComplete();
		} );
	}
#endif 

	// ** PUBLIC
	//

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

