﻿////////////////////////////////////////////
// 
// ScreenTweener.cs
//
// Created: 13/11/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

public interface IScreenTweener
{
	bool	_pIsAutoTweenIn { get; }

	bool	_pIsAutoTweenOut { get; }

	float	_pAutoTweenInDuration { get; }

	float	_pAutoTweenOutDuration { get; }

	bool	_pIsAutoTweenOutBlocking { get; }

	bool	_pIsTweeningIn { get; }

	bool	_pIsTweeningOut { get; }

	void	BeginAutoTweenIn();

	void	BeginAutoTweenOut();

	void	ForceEndTween();
}

