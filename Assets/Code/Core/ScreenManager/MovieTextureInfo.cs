using UnityEngine;
using System.Collections;

public class MovieTextureInfo{
	//transition_Intro_01
	public float progressDecimal { get { return currentMovieTime/duration; } }
	
#if !UNITY_ANDROID  &&  !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
	public bool hasMovie{ get { return texture != null; } }
#endif

	public bool IsPlaying { get { return isPlaying; } }
	
	public float 
		duration = 0f,
		currentMovieTime = 0f,
		stopTimeindex = -1f;

	public System.Action<AudioClip> _playMusicCallback = null;

	public int 
		frames;
	
#if !UNITY_ANDROID  &&  !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
	MovieTexture 
//		textureMask,
		texture;
#endif
	Vector2
		offset,
		frameSize;
	
	Texture2D
		staticMask;
	
	ReturnFunction 
		onMovieComplete;
	
	GameObject
		cameraObject,
		moviePlane;
	
//	Camera
//		movieCamera;
	
//	RenderTexture
//		movieRenderTexture;
	
//	Material
//		moviePlaneMaterial;
	
	bool 
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
		hasMovieMask = false,
		hasStaticMask = false,
#endif
		isPlaying = false;

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
	int 
		delayFrames = 0;
#endif
	
	public delegate void ReturnFunction();
	
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
	public MovieTextureInfo(string movieId, Vector2 frameSize, int frames, bool loop, MovieTexture explicitTexture ){		
		this.frames = frames;
		this.frameSize = frameSize;
		texture = explicitTexture != null? explicitTexture : (MovieTexture)Resources.Load(movieId);
		
		if(texture == null){
			if (Debug.isDebugBuild) Debug.LogWarning("Unable to find MovieTexture [" + movieId + "] in resources.");
			return;
		}
		duration = texture.duration;

		texture.loop = loop;
		
		stopMovie();
		if (Debug.isDebugBuild) Debug.Log("MovieTexture [" + movieId + "] created, with duration [" + duration +"] and num frames [" + frames + "], texture size [" + texture.width + "," + texture.height +"], frameSize [" +frameSize.x + "," + frameSize.y+"]");
		
//		Object maskResource = Resources.Load(movieId+"_Mask");
//		try{
//			staticMask = (Texture2D)maskResource;
//		}catch{};
//		try{
//			textureMask = (MovieTexture)maskResource;
//		}catch{};
//		
//		if(textureMask != null)
//			hasMovieMask = true;
//		else if(staticMask != null)
//			hasStaticMask = true;
		
		if(!hasMovieMask && !hasStaticMask){
			//Debug.LogWarning(" MovieTextureMask [" + movieId + "_Mask] is an incompatible or non-existant resource. Running without a mask.");
			return;
		}
		
//		cameraObject = new GameObject();
//		cameraObject.name = "Camera_" + movieId;
//		cameraObject.transform.parent = MovieTextureManager.cameraHolder.transform;
//		cameraObject.transform.localPosition = new Vector3(10f*MovieTextureManager.numCameras,0,0);
//		MovieTextureManager.numCameras++;
//		
//		movieCamera = cameraObject.AddComponent<Camera>();
//		GameObject.Destroy(cameraObject.GetComponent<AudioListener>());
//		GameObject.Destroy(cameraObject.GetComponent<AudioSource>());
//		movieCamera.orthographic = true;
//        movieCamera.clearFlags = CameraClearFlags.Color;
//		movieCamera.backgroundColor = new Color(0f,0f,0f,0f);
//		
//		
//		Vector2 ratio = new Vector2(frameSize.x/Screen.width,frameSize.y/Screen.height); 
//
//		
//		movieCamera.aspect = frameSize.x/frameSize.y;
//		movieCamera.nearClipPlane = 1f;
//		movieCamera.farClipPlane = 1.4f;
//		movieCamera.orthographicSize = frameSize.y/Screen.height;
		
//		moviePlaneMaterial = new Material(MovieTextureManager.movieTextureMaterial);
//		moviePlaneMaterial.SetTexture("_MainTex",texture);
//		if(hasMovieMask){
//			moviePlaneMaterial.SetTexture("_AlphaTex",textureMask);
//			textureMask.loop = loop;
//		}
//		if(hasStaticMask)
//			moviePlaneMaterial.SetTexture("_AlphaTex", staticMask);
//		
//		moviePlane = new GameObject();
//		moviePlane.name = "Movie plane " + movieId;
//		moviePlane.transform.parent = cameraObject.transform;
//		moviePlane.transform.localPosition = new Vector3(0,0,0);
//		
//		MovieTextureQuad quadScript = moviePlane.AddComponent<MovieTextureQuad>();
//		quadScript.newVertices = new Vector3[] { 
//			new Vector3(-1.5f*ratio.x,1f*ratio.y,1.1f),  new Vector3(1.5f*ratio.x,1f*ratio.y,1.1f), 
//			new Vector3(-1.5f*ratio.x,-1f*ratio.y,1.1f),  new Vector3(1.5f*ratio.x,-1f*ratio.y,1.1f)
//		};
//		
//		quadScript.init();
//		
//		
//		Renderer movieRenderer = moviePlane.GetComponent<Renderer>();
//		movieRenderer.material = moviePlaneMaterial;
//		
//		movieRenderTexture = new RenderTexture((int)frameSize.x,(int)frameSize.y,32);
//		movieRenderTexture.name = "MovieTexture_"+movieId;
//		movieCamera.targetTexture = movieRenderTexture;
	}
#endif
	
	//Overload
	public void playMovie(){
		playMovie(null,0);	
	}
	//Overload
	public void playMovie(ReturnFunction onMovieComplete){
		playMovie(onMovieComplete,0);	
	}
	//Overload
	public void playMovie(int delayFrames){
		playMovie(null,delayFrames);	
	}
	public void playMovie(ReturnFunction onMovieComplete, int delayFrames){
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
		this.onMovieComplete = onMovieComplete;
		this.delayFrames = delayFrames;
		stopMovie();
		texture.Play();
		if( texture.audioClip != null && _playMusicCallback != null )
		{
			_playMusicCallback( texture.audioClip );
		}
		
//		if(textureMask != null)
//			textureMask.Play();
//		if(movieCamera != null)
//			movieCamera.enabled = true;
		isPlaying = true;
//        if(movieRenderTexture != null)
//            RenderTexture.active = movieRenderTexture;
		if (Debug.isDebugBuild) Debug.LogWarning("Playing movie [" + texture.name + "]");
#endif
	}
	
	public void stopMovie(){
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
		texture.Stop();
//		if(textureMask != null)
//			textureMask.Stop();
		isPlaying = false;
		currentMovieTime = 0f;
		stopTimeindex = -1f;
#endif
	}
	
	public void pauseMovie(){
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
		texture.Pause();
//		if(textureMask != null)
//			textureMask.Pause();
#endif
	}
	
	public void goToAndStop(int frame){
		goToAndStop((float)frame/(float)frames);
	}
	
	public void goToAndStop(float timeIndex){
		stopTimeindex = timeIndex;
	}
	
	public void update(){
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
		if(delayFrames > 0)
			delayFrames--;
		
		
		if(!isPlaying && stopTimeindex > 0f && currentMovieTime < stopTimeindex){
			if(!texture.isPlaying){
				texture.Play();
//				if(textureMask != null)
//					textureMask.Play();
			}
			isPlaying = true;
		}
		
		if(!isPlaying)
			return;
		
		if(texture.isPlaying)
			currentMovieTime += Time.deltaTime;
		else{
			if(duration == -1)
				duration = currentMovieTime;
			if(onMovieComplete != null)
				onMovieComplete();
			if(!texture.loop)
				stopMovie();
		}
		if(stopTimeindex > 0f && currentMovieTime >= stopTimeindex){
			texture.Pause();
			isPlaying = false;
		}
#endif
	}
	
	public void onGUI(){
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
//		float yPos = 0;
//		if(delayFrames > 0){
//			yPos = -Screen.height; 
//		}
		
//		if(hasMovieMask || hasStaticMask) 
//			GUI.DrawTexture(new Rect( 0,yPos,frameSize.x,frameSize.y), movieRenderTexture);
//		else 
		float ratio = (float)texture.width / (float)texture.height;
		Rect displayRect = new Rect(); 
		displayRect.width = ( ratio > 1f )? frameSize.x : ratio * frameSize.y;
		displayRect.height = ( ratio <= 1f )? frameSize.y : (1f/ratio) * frameSize.x;
		displayRect.x = (frameSize.x/2) - (displayRect.width/2f);
		displayRect.y = (frameSize.y/2) - (displayRect.height/2f);

		GUI.Label( displayRect, texture );
#endif
	}
}
