﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScreenLoaderInfo 
{
	public ScreenBase screenBase;
	public bool immediatelyShowOnLoad = false;
	public ScreenLoaderInfo( ScreenBase screen )
	{
		screenBase = screen;
	}
}
