#if PUMA_DEFINE_LOCALISATION_FACADE
#define SUPPORT_LOCALISATION
#endif

using UnityEngine;
using AmuzoEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

using PanelType = UnityEngine.CanvasGroup;

public class ScreenBase : MonoBehaviour
{
	public enum ScreenTweenType
	{
		Idle,
		TweenIn,
		TweenOut,
	}

	private bool _isManualShow;

	public event System.Action	_pOnShowScreen;

	public event System.Action	_pOnShowComplete;

	public event System.Action	_pOnExitScreen;

	public event System.Action	_pOnExitComplete;

	public event System.Action<bool>	_pOnLayoutChanged;
	
	private System.Action	_onNextShowComplete;

	private System.Action	_onNextHardExitComplete;

	private System.Action	_onNextSoftExitComplete;

	private IScreenTweener[]	_tweeners;

	protected bool _isVisible = false;
	private bool _isShowing = false;
	private bool _initialised = false;
	private float _screenHardExitTime = 0;
	private float _screenSoftExitTime = 0f;
	private float _screenStartTime = 0;

	[SerializeField]
	private Canvas	_canvas;

	protected PanelType _panel;

	[SerializeField]
	private int	_uguiScreenDepth;

	private ScreenFlow	_screenFlow;

	private ScreenFlow	_screenFlowForNavigateEndFlow;

	private List<ScreenBase>	_layers;

	public static event System.Action<ScreenBase>	_pOnScreenDepthChanged;

	public bool _pIsShowing
	{
		get
		{
			return _isShowing;
		}
	}

	public bool _pIsManualShow
	{
		get
		{
			return _isManualShow;
		}
	}

	private ScreenTweenType _tweenType = ScreenTweenType.Idle;
	public ScreenTweenType _pCurrentTweenType
	{
		get
		{
			return _tweenType;
		}
	}
	public bool _pIsTweening
	{
		get
		{
			return _tweenType != ScreenTweenType.Idle;
		}
	}

	public Canvas _pCanvas
	{
		get
		{
			return _canvas;
		}
	}

	protected float _pAlpha
	{
		get
		{
			return _panel != null ? _panel.alpha : 1f;
		}
		set
		{
			if( _panel != null )
				_panel.alpha = value;
		}
	}

	public int _pDepth
	{
		get
		{
			return _uguiScreenDepth;
		}
		protected set
		{
			if ( _uguiScreenDepth != value )
			{
				_uguiScreenDepth = value;

				RefreshCanvasSortingOrder();
					
				if ( _pOnScreenDepthChanged != null )
				{
					_pOnScreenDepthChanged( this );
				}
			}
		}
	}

	public string _pScreenName
	{
		get
		{
			return gameObject.name;
		}
	}

	public ScreenFlow _pScreenFlow
	{
		get
		{
			return _screenFlow;
		}
	}

	public IList<ScreenBase> _pLayers
	{
		get
		{
			return _layers;
		}
	}

	protected virtual void Awake()
	{
		if ( _canvas == null )
		{
			_canvas = GetComponent<Canvas>();
			RefreshCanvasSortingOrder();
		}

		_panel = GetComponent<PanelType>();
	}

	[ContextMenu("Register")]
	public void RegisterScreen()
	{
		OnRegisterScreen();
	}

	public virtual void OnNewScreensRegistered()
	{
	}

	public void AddLayer( ScreenBase layer )
	{
		if ( layer == null ) return;

		if ( _layers == null )
		{
			_layers = new List<ScreenBase>();
		}

		if ( !_layers.Contains( layer ) )
		{
			_layers.Add( layer );
		}

		if ( _isShowing && !layer._isShowing )
		{
			layer.ShowScreen( _screenFlow, isScreenTransition:false );
		}
	}

	public void RemoveLayer( ScreenBase layer )
	{
		if ( layer == null ) return;

		if ( _layers != null && _layers.Contains( layer ) )
		{
			if ( _isShowing && layer._isShowing )
			{
				layer.ExitScreen( isScreenTransition:false );
			}

			_layers.Remove( layer );
		}
	}

	public void RemoveAllLayers()
	{
		if ( _layers != null )
		{
			if ( _isShowing )
			{
				for ( int i = 0; i < _layers.Count; ++i )
				{
					if ( _layers[i] != null && _layers[i]._isShowing )
					{
						_layers[i].ExitScreen( isScreenTransition:false );
					}
				}
			}

			_layers.Clear();
		}
	}

	public void SetLayers( params ScreenBase[] layers )
	{
		RemoveAllLayers();

		if ( layers == null ) return;

		for ( int i = 0; i < layers.Length; ++i )
		{
			AddLayer( layers[i] );
		}
	}

	public void ForEachLayer( bool includeSelf, System.Action<ScreenBase> action, System.Predicate<ScreenBase> layerMatch = null )
	{
		if ( action == null ) return;

		if ( includeSelf )
		{
			action( this );
		}

		IList<ScreenBase>	layers = _pLayers;

		if ( layers != null )
		{
			for ( int i = 0; i < layers.Count; ++i )
			{
				if ( layers[i] == null ) continue;
				if ( layerMatch != null && !layerMatch( layers[i] ) ) continue;

				action( layers[i] );
			}
		}
	}

	public JobCounter ForEachLayerAsync( bool includeSelf, System.Action<ScreenBase, System.Action> asyncAction, System.Action onComplete = null, System.Predicate<ScreenBase> layerMatch = null )
	{
		if ( asyncAction == null )
		{
			if ( onComplete != null )
			{
				onComplete();
			}
			return null;
		}

		JobCounter	actionJobs = new JobCounter( onComplete );

		System.Action<ScreenBase>	asyncActionWrapper = s => {
			actionJobs.BeginJob();
			asyncAction( s, () => {
				actionJobs.EndJob();
			} );
		};

		actionJobs.BeginJob();

		if ( includeSelf )
		{
			asyncActionWrapper( this );
		}

		IList<ScreenBase>	screenLayers = _pLayers;

		if ( screenLayers != null )
		{
			for ( int i = 0; i < screenLayers.Count; ++i )
			{
				if ( screenLayers[i] == null ) continue;
				if ( layerMatch != null && !layerMatch( screenLayers[i] ) ) continue;

				asyncActionWrapper( screenLayers[i] );
			}
		}

		actionJobs.EndJob();

		return actionJobs._pHasJobs ? actionJobs : null;
	}

	public ScreenBase FindLayer( bool includeSelf, System.Predicate<ScreenBase> pred )
	{
		IList<ScreenBase>	layers = _pLayers;

		if ( pred == null )
		{
			return includeSelf ? this : layers != null && layers.Count > 0 ? layers[0] : null;
		}

		if ( includeSelf && pred( this ) == true  ) return this;

		if ( layers != null )
		{
			for ( int i = 0; i < layers.Count; ++i )
			{
				if ( layers[i] == null ) continue;
				if ( pred( layers[i] ) == true  ) return layers[i];
			}
		}

		return null;
	}
	
	// -----------------------------------------------------------------------------------------------
	private void Initialise()
	{
		Component[]	tweenerComponents = gameObject.GetComponentsInChildren( typeof( IScreenTweener ), includeInactive:true );

		_tweeners = new IScreenTweener[ tweenerComponents.Length ];

		for ( int i = 0; i < tweenerComponents.Length; ++i )
		{
			_tweeners[i] = tweenerComponents[i] as IScreenTweener;
		}

		_initialised = true;

		#if SUPPORT_LOCALISATION
		AmuzoNGUILocalize[] localisationElements = gameObject.GetComponentsInChildren<AmuzoNGUILocalize>( true );
		int numElements = localisationElements.Length;

		for( int i = 0; i < numElements; i++ )
		{
			localisationElements[i].AssignTextFromKey();
		}
		#endif

		OnInitialise();
	}

	// -----------------------------------------------------------------------------------------------
	protected virtual void OnRegisterScreen()
	{
	}
	// -----------------------------------------------------------------------------------------------
	protected virtual void OnInitialise()
	{
	}
	// -----------------------------------------------------------------------------------------------
	protected virtual void OnShowScreen()
	{
		if ( _pOnShowScreen != null )
		{
			_pOnShowScreen();
		}
	}
	// -----------------------------------------------------------------------------------------------
	protected virtual void OnExitScreen()
	{
		if ( _pOnExitScreen != null )
		{
			_pOnExitScreen();
		}
	}
	// -----------------------------------------------------------------------------------------------
	// any override should call base
	protected virtual void OnScreenShowComplete()
	{
		ForceEndTransitionWidgetsAutoTweenIn();

		if( _onNextShowComplete != null )
		{
			_onNextShowComplete();
			_onNextShowComplete = null;
		}
		
		if ( _pOnShowComplete != null )
		{
			_pOnShowComplete();
		}
	}
	// -----------------------------------------------------------------------------------------------
	// any override should call base
	protected virtual void OnScreenExitComplete()
	{
		//CC20180621 - move to end of function to handle the case where this same screen is shown again in the callbacks triggered from this call
		//OnScreenSoftExitComplete();//with current implementation, ok to call twice (ie after already called earlier if soft exit distinct from hard exit) - CC20171128

		ForceEndTransitionWidgetsAutoTweenOut();

		_isVisible = false;
		_isShowing = false;
		_isManualShow = false;
		_screenFlow = null;
		_tweenType = ScreenTweenType.Idle;

		gameObject.SetActive( false );
		
		if ( _pOnExitComplete != null )
		{
			_pOnExitComplete();
		}

		if( _onNextHardExitComplete != null )
		{
			System.Action	cachedEvent = _onNextHardExitComplete;
			_onNextHardExitComplete = null;
			cachedEvent();
		}

		OnScreenSoftExitComplete();//with current implementation, ok to call twice (ie after already called earlier if soft exit distinct from hard exit) - CC20171128
	}

	// -----------------------------------------------------------------------------------------------
	// any override should call base
	protected virtual void OnScreenSoftExitComplete()
	{
		if ( _onNextSoftExitComplete != null )
		{
			System.Action	cachedEvent = _onNextSoftExitComplete;
			_onNextSoftExitComplete = null;
			cachedEvent();
		}
	}


	public bool IsReadyToExitTransition
	{
		get
		{
			return _isReadyToExitTransition;
		}
	}

	protected bool _isReadyToExitTransition = true;

	public static string formatCamelCaseForDebug( string camelCase )
	{
		string returnString = camelCase.Substring( 0, 1 ).ToUpper();
		for( int x = 1; x < camelCase.Length; x++ )
		{
			string charAsString = camelCase.Substring( x, 1 );

			if( charAsString == charAsString.ToUpper() )
			{
				returnString += " " + charAsString;
			}
			else
			{
				returnString += charAsString;
			}
		}
		return returnString;
	}

	// -----------------------------------------------------------------------------------------------
	public void Navigate( ScreenBase nextScreen, bool doClearHistory = false, bool doAddCurrentScreenToHistory = true, System.Action onComplete = null )
	{
		if ( _screenFlowForNavigateEndFlow == null )
		{
			Debug.LogError( this.name + ": Navigate: Null _screenFlowForNavigateEndFlow" );
			return;
		}

		_screenFlowForNavigateEndFlow.ShowScreen( nextScreen, doClearHistory, doAddCurrentScreenToHistory, onComplete );
	}

	// -----------------------------------------------------------------------------------------------
	public void NavigateBack( System.Action onComplete = null )
	{
		if ( _screenFlowForNavigateEndFlow == null )
		{
			Debug.LogError( this.name + ": NavigateBack: Null _screenFlowForNavigateEndFlow" );
			return;
		}

		_screenFlowForNavigateEndFlow.ShowPreviousScreen( onComplete );
	}

	// -----------------------------------------------------------------------------------------------
	public void EndFlow( System.Action onComplete = null )
	{
		if ( _screenFlowForNavigateEndFlow == null )
		{
			Debug.LogError( this.name + ": EndFlow: Null _screenFlowForNavigateEndFlow" );
			return;
		}

		_screenFlowForNavigateEndFlow.End( onComplete );
	}

	// -----------------------------------------------------------------------------------------------
	public void ShowPopupOverlay( ScreenBase popupOverlay, System.Action onShowComplete = null, System.Action onPopupOverlayEnd = null )
	{
		Facades<ScreenFacade>.Instance.ShowPopupOverlay( popupOverlay, onShowComplete, onPopupOverlayEnd );
	}

#if UNITY_EDITOR
	// -----------------------------------------------------------------------------------------------
	[ContextMenu("Show")]
	public void EditorShowScreen()
	{
		if ( Facades<ScreenFacade>.Instance != null )
		{
			Facades<ScreenFacade>.Instance.ShowScreen( this );
		}
		else
		{
			ShowScreen( screenFlow:null, isScreenTransition:false );
		}
	}

	// -----------------------------------------------------------------------------------------------
	[ContextMenu("ShowPopup")]
	public void EditorShowPopup()
	{
		if ( Facades<ScreenFacade>.Instance != null )
		{
			Facades<ScreenFacade>.Instance.ShowPopupOverlay( this );
		}
		else
		{
			ShowScreen( screenFlow:null, isScreenTransition:false );
		}
	}
#endif

	// -----------------------------------------------------------------------------------------------
	public void ShowScreen( ScreenFlow screenFlow, bool isScreenTransition, System.Action onComplete = null )
	{
		if( !_initialised )
		{
			Initialise();
		}

#if DEBUG_MESSAGES
		Debug.Log("Show Screen: " + name);
#endif

		if ( _screenFlow != null && _screenFlow != screenFlow )
		{
			Debug.LogError( "ShowScreen: '" + name + "' already has a different screen flow set, ie already shown in a different flow!" );
			return;
		}

		_screenFlow = screenFlow;
		_screenFlowForNavigateEndFlow = screenFlow;

		gameObject.SetActive( true );

		_isVisible = true;
		//_isManualShow = isManualShow;

		bool wasShowing = _isShowing;

		OnShowScreen();

		_isShowing = true;

		_onNextShowComplete += onComplete;

		if( wasShowing )
		{
			OnScreenShowComplete();
			return;
		}

		//	Debug.Log("Beginning Tween In: " + name + "(time=" + RealTime.time + ")");
		_screenStartTime = -1f;

		int	tweenCount = 0;

		// calculate when the screen will exit
		for( int i = 0; i < _tweeners.Length; i++ )
		{
			float tweenElementDuration = 0;
			if( _tweeners[i] == null )
				continue;

			// no auto screen transition for this element, don't include in the time calc
			if( !_tweeners[i]._pIsAutoTweenIn )
			{
				continue;
			}

			++tweenCount;

			tweenElementDuration = _tweeners[i]._pAutoTweenInDuration;

			_tweeners[i].BeginAutoTweenIn();

			if ( tweenElementDuration >= 0f )
			{
				// add to our screen exit time, if this tween widget takes longer
				_screenStartTime = Mathf.Max( _screenStartTime, tweenElementDuration );
			}
		}

		if ( _screenStartTime >= 0f )
		{
			_screenStartTime += Time.unscaledTime;
		}

		if ( tweenCount > 0 )
		{
			_tweenType = ScreenTweenType.TweenIn;
		}
		else
		{
			//	Debug.Log("Immediate show: " + name);
			OnScreenShowComplete();
		}
	}

	// -----------------------------------------------------------------------------------------------
	public void CancelShowScreen()
	{
		if ( !_isVisible ) return;

		_onNextShowComplete = null;

		ExitScreen( isScreenTransition:false, isForceExitImmediate:true );
	}

#if UNITY_EDITOR
	// -----------------------------------------------------------------------------------------------
	[ContextMenu("Exit")]
	public void EditorExitScreen()
	{
		ExitScreen( isScreenTransition:false, isForceExitImmediate:true );
	}

	// -----------------------------------------------------------------------------------------------
	[ContextMenu("EndFlow")]
	public void EditorEndFlow()
	{
		EndFlow();
	}
#endif

	// -----------------------------------------------------------------------------------------------
	public float ExitScreen( bool isScreenTransition, bool isForceExitImmediate = false )
	{
#if DEBUG_MESSAGES
		Debug.Log("ExitScreen: " + name);
#endif

		OnExitScreen();

		if ( _screenFlow != null && isScreenTransition )
		{
			_screenFlow._pTransitionManager.BeginExitScreenJob();
			_screenFlow._pTransitionManager.BeginBlockNextScreenJob();

			_onNextHardExitComplete += _screenFlow._pTransitionManager.EndExitScreenJob;
			_onNextSoftExitComplete += _screenFlow._pTransitionManager.EndBlockNextScreenJob;
		}

		_screenHardExitTime = -1f;
		_screenSoftExitTime = -1f;

		int	tweenCount = 0;
			
		// calculate when the screen will exit
		for( int i = 0; i < _tweeners.Length; i++ )
		{
			if ( isForceExitImmediate ) continue;

			float tweenElementDuration = 0;
			if( _tweeners[i] == null )
				continue;

			// no auto screen transition for this element, don't include in the time calc
			if( !_tweeners[i]._pIsAutoTweenOut )
			{
				continue;
			}

			++tweenCount;
			
			tweenElementDuration = _tweeners[i]._pAutoTweenOutDuration;

			_tweeners[i].BeginAutoTweenOut();

			if ( tweenElementDuration >= 0f )
			{
				// add to our screen exit time, if this tween widget takes longer
				_screenHardExitTime = Mathf.Max( _screenHardExitTime, tweenElementDuration );

				if ( _tweeners[i]._pIsAutoTweenOutBlocking )
				{
					_screenSoftExitTime = Mathf.Max( _screenSoftExitTime, tweenElementDuration );
				}
			}
		}

		//			Debug.Log("Screen has transition widgets, flow will proceed in " + (_screenExitTime+additionalDelay) + " seconds");

		float exitDuration = _screenHardExitTime;

		if ( _screenHardExitTime >= 0f )
		{
			_screenHardExitTime += Time.unscaledTime;

			if ( _screenSoftExitTime >= 0f )
			{
				_screenSoftExitTime += Time.unscaledTime;
			}
		}

		if ( tweenCount > 0 )
		{
			_tweenType = ScreenTweenType.TweenOut;
		}
		else
		{
			OnScreenExitComplete();
		}

		return exitDuration;
	}

	// -----------------------------------------------------------------------------------------------
	protected virtual void Update()
	{
		if( _tweenType == ScreenTweenType.Idle )
			return;

		if( _tweenType == ScreenTweenType.TweenOut )
		{
			if ( _screenHardExitTime >= 0f && Time.unscaledTime >= _screenHardExitTime )
			{
				EndTweenOut();
			}
			else if ( _screenSoftExitTime >= 0f && Time.unscaledTime >= _screenSoftExitTime )
			{
				OnScreenSoftExitComplete();
			}
		}
		else if( _tweenType == ScreenTweenType.TweenIn )
		{
			if ( _screenStartTime >= 0f && Time.unscaledTime >= _screenStartTime )
			{
				EndTweenIn();
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public void EndTweenIn()
	{
		if ( _tweenType != ScreenTweenType.TweenIn )
		{
			Debug.LogError( "[ScreenBase] EndTweenIn - Tween state = " + _tweenType );
			return;
		}

		_tweenType = ScreenTweenType.Idle;

		OnScreenShowComplete();
	}

	//------------------------------------------------------------------------------------------------------------------
	public void EndTweenOut()
	{
		if ( _tweenType != ScreenTweenType.TweenOut )
		{
			Debug.LogError( "[ScreenBase] EndTweenOut - Tween state = " + _tweenType );
			return;
		}

		_tweenType = ScreenTweenType.Idle;

		OnScreenExitComplete();
	}

	//------------------------------------------------------------------------------------------------------------------
	public static Color getColourFromArray( Color[] colours, float decimalProgress )
	{
		if( colours.Length < 2 )
		{
			Debug.Log( "ScreenBase: Must have at least 2 colours in order to use 'getColourFromArray'!" );
			return Color.white;
		}

		float
			progressPerColour = 1f / ( float )colours.Length,
			progressFromAToB = ( decimalProgress % progressPerColour ) / progressPerColour,
			temp = 0f;

		Color
			colourA = Color.white,
			colourB = Color.white;

		for( int x = 0; x < colours.Length; x++ )
		{
			temp += progressPerColour;

			if( temp > decimalProgress )
			{
				colourA = colours[x];
				colourB = colours[( x + 1 ) % colours.Length];
				return Color.Lerp( colourA, colourB, progressFromAToB );
			}
		}

		return Color.white;
	}

	// ---------------------------------------------------------------------------------------------------------------------------
    protected string Localise(string key)
    {
		#if SUPPORT_LOCALISATION
		string localText = LocalisationFacade.Instance.GetString(key);
		if (localText == "" || localText == null)
		{
			return key;
		}
		return localText;
		#else
		Debug.LogError( "[ScreenBase] Localisation not supported!" );
		return key;
		#endif
    }
 
	// ---------------------------------------------------------------------------------------------------------------------------
	public void NotifyDeviceBackButtonPressed( ref bool isHandled )
	{
		if ( isHandled ) return;

		OnDeviceBackButtonPressed( ref isHandled );

		if ( isHandled ) return;

		if ( _layers != null )
		{
			for ( int i = 0; i < _layers.Count && !isHandled; ++i )
			{
				if ( _layers[i] == null ) continue;
				if ( !_layers[i]._isShowing ) continue;
				
				_layers[i].OnDeviceBackButtonPressed( ref isHandled );
			}
		}
	}

	protected virtual void OnDeviceBackButtonPressed( ref bool isHandled )
	{
	}

	// ---------------------------------------------------------------------------------------------------------------------------

	protected void NotifyLayoutChanged( bool hasContentChanged = false )
	{
		if ( _pOnLayoutChanged != null )
		{
			_pOnLayoutChanged( hasContentChanged );
		}
	}

	#if UNITY_EDITOR
	[ContextMenu( "LayoutChanged" )]
	public void EditorNotifyLayoutChanged()
	{
		NotifyLayoutChanged( hasContentChanged:false );
	}

	[ContextMenu( "ContentChanged" )]
	public void EditorNotifyContentChanged()
	{
		NotifyLayoutChanged( hasContentChanged:true );
	}
	#endif

	// -----------------------------------------------------------------------------------------------
	private void ForceEndTransitionWidgetsAutoTweenIn()
	{
		for(int i=0;i<_tweeners.Length;i++)
		{
			if ( _tweeners[i] == null ) continue;
			if ( !_tweeners[i]._pIsAutoTweenIn ) continue;
			if ( !_tweeners[i]._pIsTweeningIn ) continue;

			_tweeners[i].ForceEndTween();
		}
	}

	private void ForceEndTransitionWidgetsAutoTweenOut()
	{
		for(int i=0;i<_tweeners.Length;i++)
		{
			if ( _tweeners[i] == null ) continue;
			if ( !_tweeners[i]._pIsAutoTweenOut ) continue;
			if ( !_tweeners[i]._pIsTweeningOut ) continue;

			_tweeners[i].ForceEndTween();
		}
	}

	// ---------------------------------------------------------------------------------------------------------------------------
	private void RefreshCanvasSortingOrder()
	{
		if ( _canvas != null )
		{
			_canvas.sortingOrder = _uguiScreenDepth;
		}
	}
}
