using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MovieTextureQuad : MonoBehaviour{
	
    public Vector3[] newVertices = new Vector3[] { 
		new Vector3(-1.0f,1f,1.1f),  new Vector3(1.0f,1f,1.1f), 
		new Vector3(-1.0f,-1f,1.1f),  new Vector3(1.0f,-1f,1.1f)
	};
	
    public Vector2[] newUV = new Vector2[] {
		new Vector2(0,1), new Vector2(1,1),
		new Vector2(0,0), new Vector2(1,0)
	};
	
	public Vector2[] newUV2 = new Vector2[] {
		new Vector2(0,1), new Vector2(1,1),
		new Vector2(0,0), new Vector2(1,0)
	};
	
    public int[] newTriangles = new int[] {
		0,1,2,2,1,3
	};
	
	void Awake(){
		init();
	}
	
    public void init() {
        Mesh mesh = new Mesh(); // make a new mesh
        GetComponent<MeshFilter>().mesh = mesh; // assign it to the filter so it gets rendered
        mesh.vertices = newVertices; // assign the vert positions
        mesh.uv = newUV; // assign the vert uvs
		mesh.uv2 = newUV2; // assign the vert uvs2
        mesh.triangles = newTriangles; // set up the triangles
    }
}

