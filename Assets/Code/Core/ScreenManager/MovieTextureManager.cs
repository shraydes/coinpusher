using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovieTextureManager {
	
	//public static Material movieTextureMaterial = new Material(Resources.Load("Alpha Movie") as Shader);
	
	public static GameObject cameraHolder;
	
	public static int numCameras = 0;
	
	static Dictionary<string,MovieTextureInfo> movieTextures;
	
	public static MovieTextureInfo getMovie(string movieId, Vector2 frameSize, int frames, bool loop){
		if(movieTextures == null)
			movieTextures = new Dictionary<string, MovieTextureInfo>();
		
		if(cameraHolder == null){
			cameraHolder = new GameObject();
			cameraHolder.transform.position = new Vector3(0,1000,0);
			cameraHolder.name = "MovieTexture Camera Holder";
			GameObject.DontDestroyOnLoad(cameraHolder);
		}
		
		if(movieTextures.ContainsKey(movieId))
			return movieTextures[movieId];
		
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_WEBGL && !UNITY_TVOS && !UNITY_SWITCH
		MovieTextureInfo newSheet = new MovieTextureInfo(movieId, frameSize, frames,loop, null);
		movieTextures.Add(movieId,newSheet);
		return newSheet;
#else
		return null;
#endif
	}
	
}

