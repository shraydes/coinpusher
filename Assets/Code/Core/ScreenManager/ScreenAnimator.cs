﻿////////////////////////////////////////////
// 
// ScreenAnimator.cs
//
// Created: 19/06/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_ScreenAnimator
#endif

using UnityEngine;
using AmuzoEngine;

[RequireComponent( typeof(ScreenBase), typeof(Animator) )]
public class ScreenAnimator : MonoBehaviour, IScreenTweener
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ScreenAnimator] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	[SerializeField]
	private bool	_isAutoTweenIn;

	[SerializeField]
	private bool	_isAutoTweenOut;

	[SerializeField]
	private bool	_isAutoTweenOutBlocking;

	private ScreenBase	_screen;

	private Animator	_animator;
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	private void Awake()
	{
		_screen = GetComponent<ScreenBase>();
		_animator = GetComponent<Animator>();
	}

	// ** PUBLIC
	//

	public void ANIM_EVENT_EndTweenIn()
	{
		_screen.EndTweenIn();
	}
	
	public void ANIM_EVENT_EndTweenOut()
	{
		_screen.EndTweenOut();
	}
	
	// ** INTERNAL
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//

	bool IScreenTweener._pIsAutoTweenIn
	{
		get
		{
			return _isAutoTweenIn;
		}
	}

	bool IScreenTweener._pIsAutoTweenOut
	{
		get
		{
			return _isAutoTweenOut;
		}
	}

	float IScreenTweener._pAutoTweenInDuration
	{
		get
		{
			return -1f;
		}
	}

	float IScreenTweener._pAutoTweenOutDuration
	{
		get
		{
			return -1f;
		}
	}

	bool IScreenTweener._pIsAutoTweenOutBlocking
	{
		get
		{
			return _isAutoTweenOutBlocking;
		}
	}

	bool IScreenTweener._pIsTweeningIn
	{
		get
		{
			return false;
		}
	}

	bool IScreenTweener._pIsTweeningOut
	{
		get
		{
			return false;
		}
	}

	void IScreenTweener.BeginAutoTweenIn()
	{
		_animator.SetBool( "SHOW", true );
	}

	void IScreenTweener.BeginAutoTweenOut()
	{
		_animator.SetBool( "SHOW", false );
	}

	void IScreenTweener.ForceEndTween()
	{
	}
}

