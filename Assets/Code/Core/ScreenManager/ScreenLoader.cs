using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScreenLoader : InitialisationObject 
{
	public bool initialiseGameSceneAfterScreenLoad = true;
	public bool dontDestroyOnLoad = true;

	public bool emulateLowMemInEditor = false;

	public List<ScreenLoaderInfo> screensToLoad;
	[HideInInspector]
	public bool showScreens = false;

	private int _currentScreenLoad = 0;
	private int _numScreensToLoad = 0;

	private bool _isLoading = false;

	// -----------------------------------------------------------------------------------------------
	protected override void Awake()
	{
		if(dontDestroyOnLoad)
		{
			DontDestroyOnLoad(gameObject);
		}

		base.Awake();
	}

	public override void startInitialising()
	{
		_currentState = InitialisationState.INITIALISING;
		LoadScreens();
	}

	// -----------------------------------------------------------------------------------------------
	public void LoadScreens()
	{
	/*	if(Facades<ScreenFacade>.Instance == null)
		{
			ScreenFacade newScreenFacade = new ScreenFacade();
			Facades<ScreenFacade>.Register(newScreenFacade);
		}*/

		_currentScreenLoad = 0;
		_numScreensToLoad = screensToLoad.Count;

	//	StartCoroutine(HandleScreenLoading());
		_isLoading = true;
	}

//	// -----------------------------------------------------------------------------------------------
//	private bool IsLowMemory()
//	{
//#if UNITY_EDITOR
//		if(emulateLowMemInEditor)
//		{
//			return true;
//		}
//#endif

//		if(Facades<FidelityFacade>.Instance != null)
//		{
//			if(Facades<FidelityFacade>.Instance.fidelity == Fidelity.Low)
//			{
//				return true;
//			}
//		}

//		return false;
//	}

	// -----------------------------------------------------------------------------------------------
	private void Update()
	{
		if(!_isLoading)
		{
			return;
		}

		if ( _currentScreenLoad == 0 )
		{
			Facades<ScreenFacade>.Instance.BeginRegisteringScreens();
		}

		// step our screen loading over multiple frames so that we have some breathing room while loading
		if(_currentScreenLoad < _numScreensToLoad)
		{
			if(screensToLoad[_currentScreenLoad] != null && screensToLoad[_currentScreenLoad].screenBase != null)
			{
				Facades<ScreenFacade>.Instance.LoadScreenPrefab( screensToLoad[_currentScreenLoad].screenBase );
			}
			_currentScreenLoad++;
		}
		else
		{
			Facades<ScreenFacade>.Instance.EndRegisteringScreens();

			ScreenBase	screen, screenToShow = null;

			for ( int i = 0; i < screensToLoad.Count; ++i )
			{
				screen = Facades<ScreenFacade>.Instance.FindScreen( screensToLoad[i].screenBase.name );

				if ( screensToLoad[i].immediatelyShowOnLoad && screenToShow == null )
				{
					screenToShow = screen;
				}
			}

			if ( screenToShow != null )
			{
				Facades<ScreenFacade>.Instance.ShowScreen( screenToShow );
			}

			_isLoading = false;
			_currentState = InitialisationState.FINISHED;
		}

	}

	// -----------------------------------------------------------------------------------------------
/*	private IEnumerator HandleScreenLoading()
	{
		for(; _currentScreenLoad<_numScreensToLoad; _currentScreenLoad++)
		{
			if(screensToLoad[_currentScreenLoad] == null) continue;

			ScreenBase screen = GameObject.Instantiate(screensToLoad[_currentScreenLoad].screenBase) as ScreenBase;

			screen.gameObject.name = screensToLoad[_currentScreenLoad].screenBase.name;
			screen.gameObject.SetActive( screensToLoad[_currentScreenLoad].immediatelyShowOnLoad );
	
			if( screensToLoad[_currentScreenLoad].immediatelyShowOnLoad )
			{
				Facades<ScreenFacade>.Instance.SetCurrentlyActiveScreen(screen);
				screen.ShowScreen();
			}

			if(parentScreensToThisObject)
			{
				screen.transform.parent = transform;
			}

			// give us a breather, for any screen loading anims that may happen
			yield return null; 
		}

		if(initialiseGameSceneAfterScreenLoad)
		{	
			GameSceneInitialise.InitialiseGameScene();
		}
	}*/

}
