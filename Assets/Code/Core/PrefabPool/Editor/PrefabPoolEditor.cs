﻿////////////////////////////////////////////
// 
// PrefabPoolEditor.cs
//
// Created: 07/11/2014 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using UnityEditor;
using AmuzoEngine;
using Logger = UnityEngine.Debug;

[CanEditMultipleObjects]
[CustomEditor(typeof(PrefabPool))]
public class PrefabPoolEditor : Editor
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[PrefabPoolEditor] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	private PrefabPool	_pool;
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void OnEnable()
	{
		_pool = target as PrefabPool;
	}
	
	// ** PUBLIC
	//
	
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		if ( _pool == null ) return;
		
		if ( !Application.isPlaying && GUILayout.Button( "Create Instances" ) )
		{
			_pool.OfflineDestroyInstances();
			_pool.OfflineCreateInstances();
			EditorUtility.SetDirty( _pool );
		}

		if ( Application.isPlaying && GUILayout.Button( "Report Usage" ) )
		{
			_pool.ReportUsage();
		}
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

