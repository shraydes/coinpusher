﻿////////////////////////////////////////////
// 
// PrefabPool.cs
//
// Created: dd/mm/yyyy ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using AmuzoEngine;
using System.Collections.Generic;
using Logger = UnityEngine.Debug;

public class PrefabPool : MonoBehaviour, IObjectInstantiator
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[PrefabPool] ";
	
	public enum EInstantiateFailedPolicy
	{
		FAIL = 0,
		USE_OLDEST,
		GROW_BY_ONE,
		GROW_BY_ORIG,
		GROW_DOUBLE
	}
	
	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//

	[System.Serializable]
	public class PrefabInfo
	{
		public Object	_prefab;
		public int		_count;
	}

	private class UsageData
	{
		public int	_minFreeCount;

		public int	_growCount;
	}
	
	//
	// MEMBERS 
	//

	public EInitializeType	_initType = EInitializeType.MANUAL;
	
	public PrefabInfo[]	_prefabs;
	
	public string	_instanceName;
	
	public bool	_isShuffleInstances;
	
	public bool	_isCreateOffline;

	public bool	_isEnableDefaultInstanceStateSetting = true;

	public event System.Action<GameObject /*instance*/>	_OnSetInstanceStateAllocated;

	public event System.Action<GameObject /*instance*/>	_OnSetInstanceStateFree;

	public Transform	_instanceContainerOverride;
	
	public EInstantiateFailedPolicy	_onInstantiateFailed;

	public bool	_isGrowOk;

	public bool	_isTrackUsage;
	
	public List<GameObject>	_instances;

	private int	_origInstanceCount;

	private int	_growCloneInstanceIdx;
	
	private bool	_isInitialized;
	
	private ObjectPool<GameObject>	_instancePool;

	private UsageData	_usageData;
	
	//
	// PROPERTIES
	//
	
	private string	_pLogTag	{ get { return "[PrefabPool:" + gameObject.name + "] "; } }

	public bool _pIsInitialised
	{
		get
		{
			return _isInitialized;
		}
	}
	
	public bool	_pCanGrow	{ get { return _onInstantiateFailed == EInstantiateFailedPolicy.GROW_BY_ONE || _onInstantiateFailed == EInstantiateFailedPolicy.GROW_BY_ORIG || _onInstantiateFailed == EInstantiateFailedPolicy.GROW_DOUBLE; } }
	
	public bool	_pCanReuseInstances	{ get { return _onInstantiateFailed == EInstantiateFailedPolicy.USE_OLDEST; } }

	private Transform _pInstanceContainerTransform
	{
		get
		{
			if ( _instanceContainerOverride ) return _instanceContainerOverride;
			return this.gameObject.transform;
		}
	}
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void Awake()
	{
		if ( _initType == EInitializeType.AWAKE )
		{
			Initialize();
		}
	}
	
	void Start()
	{
		if ( _initType == EInitializeType.START )
		{
			Initialize();
		}
	}
	
	// ** PUBLIC
	//
	
	public bool OfflineCreateInstances()
	{
		if ( Application.isPlaying )
		{
			Logger.LogError( _pLogTag + "Offline create not allowed when playing", gameObject );
			return false;
		}
		
		if ( !_isCreateOffline )
		{
			Logger.LogError( _pLogTag + "Offline create not allowed by settings", gameObject );
			return false;
		}
		
		CreateInstances();
		
		return true;
	}
	
	public bool OfflineDestroyInstances()
	{
		if ( Application.isPlaying )
		{
			Logger.LogError( _pLogTag + "Offline destroy not allowed when playing", gameObject );
			return false;
		}
		
		if ( !_isCreateOffline )
		{
			Logger.LogError( _pLogTag + "Offline destroy not allowed by settings", gameObject );
			return false;
		}
		
		DestroyInstances( isDestroyImmediate:true );
		
		return true;
	}

	public void Initialize()
	{
		if ( _isInitialized ) return;
		
		if ( !_isCreateOffline )
		{
			CreateInstances();
		}
		
		PoolInstances();

		if ( _isTrackUsage )
		{
			InitUsageData();
		}
		
		_isInitialized = true;
	}
	
	public void SafeInitialize()
	{
		if ( _initType == EInitializeType.MANUAL )
		{
			Initialize();
		}
	}
	
	public GameObject AllocateFromPool()
	{
		GameObject	newInstance = AllocateInstance();
		
		if ( newInstance == null )
		{
			switch ( _onInstantiateFailed )
			{
			case EInstantiateFailedPolicy.USE_OLDEST:
				newInstance = ReallocateOldestInstance();
				break;
			case EInstantiateFailedPolicy.GROW_BY_ONE:
			case EInstantiateFailedPolicy.GROW_BY_ORIG:
			case EInstantiateFailedPolicy.GROW_DOUBLE:
				newInstance = GrowAndReallocateInstance();
				break;
			}
		}
		
		return newInstance;
	}
	
	public GameObject AllocateFromPool( Vector3 position, Quaternion rotation )
	{
		GameObject	newInstance = AllocateFromPool();
		
		if ( newInstance != null )
		{
			newInstance.transform.position = position;
			newInstance.transform.rotation = rotation;
		}
		
		return newInstance;
	}
	
	public void ReturnToPool( GameObject instance )
	{
		FreeInstance( instance );
	}
	
	public void ReturnAllToPool()
	{
		FreeAllInstances();
	}
	
	public void ReportUsage()
	{
		if ( _usageData == null ) return;
		if ( _instances == null ) return;

		if ( _instances.Count > _origInstanceCount )
		{
			Logger.Log( _pLogTag + "- Has grown " + _usageData._growCount + " times; currSize=" + _instances.Count + ", origSize=" + _origInstanceCount );
		}
		else if ( _usageData._minFreeCount > 0 )
		{
			Logger.Log( _pLogTag + "- Has NOT Used Some Allocations: " + _usageData._minFreeCount );
		}
		else
		{
			Logger.Log( _pLogTag + "- Has Used ALL Allocations, Consider Raising" );
		}
	}

	[System.Obsolete( "Use AllocateFromPool instead" )]
	public GameObject Instantiate()
	{
		return AllocateFromPool();
	}

	[System.Obsolete( "Use AllocateFromPool instead" )]
	public GameObject Instantiate( Vector3 position, Quaternion rotation )
	{
		return AllocateFromPool( position, rotation );
	}

	[System.Obsolete( "Use ReturnToPool instead" )]
	public void Destroy( GameObject instance )
	{
		ReturnToPool( instance );
	}

	[System.Obsolete( "Use ReturnAllToPool instead" )]
	public void DestroyAllInstances()
	{
		ReturnAllToPool();
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private void CreateInstances()
	{
		_instances = new List<GameObject>();

		GameObject	newInstance;
		
		for ( int i = 0; i < _prefabs.Length; ++i )
		{
			if ( _prefabs[i] == null ) continue;
			for ( int j = 0; j < _prefabs[i]._count; ++j )
			{
				if ( _prefabs[i]._prefab == null ) continue;
				newInstance = CreateInstance( _prefabs[i]._prefab );
				if ( newInstance == null )
				{
					Logger.LogWarning( _pLogTag + "Failed to instantiate instance " + i.ToString() + " from prefab '" + _prefabs[i]._prefab.name + "'", gameObject );
					continue;
				}
				_instances.Add( newInstance );
			}
		}
		
		if ( _isShuffleInstances )
		{
			_instances.Shuffle();
		}

		_origInstanceCount = _instances.Count;
		_growCloneInstanceIdx = 0;
	}

	private GameObject CreateInstance( Object prefab )
	{
		GameObject	newInstance = (GameObject)Object.Instantiate( prefab, _pInstanceContainerTransform );

		if ( newInstance == null ) return null;

		if ( _instanceName != null && _instanceName.Length > 0 )
		{
			newInstance.name = _instanceName;
		}

		SetInstanceStateFree( newInstance );

		return newInstance;
	}

	private void DestroyInstances( bool isDestroyImmediate )
	{
		if ( _instances == null ) return;

		if ( isDestroyImmediate )
		{
			for ( int i = 0; i < _instances.Count; ++i )
			{
				GameObject.DestroyImmediate( _instances[i] );
			}
		}
		else
		{
			for ( int i = 0; i < _instances.Count; ++i )
			{
				GameObject.Destroy( _instances[i] );
			}
		}

		_instances = null;
	}
	
	private void PoolInstances()
	{
		if ( _instances == null ) return;
		
		int	nextInstance = 0;
		_instancePool = new ObjectPool<GameObject>( _instances.Count, () => ( _instances[ nextInstance++ ] ), null );
	}
	
	private GameObject AllocateInstance()
	{
		if ( !_isInitialized )
		{
			Logger.LogError( _pLogTag + "Not initialized", gameObject );
			return null;
		}
		
		if ( _instancePool == null ) return null;
		
		return AllocateInstanceCore();
	}

	private GameObject AllocateInstanceCore()
	{
		GameObject	instance = _instancePool.Allocate();
		
		if ( instance == null ) return null;

		OnInstanceAllocated();
		
		SetInstanceStateAllocated( instance );
		
		return instance;
	}
	
	private void FreeInstance( GameObject instance )
	{
		SetInstanceStateFree( instance );
		
		if ( _instancePool == null ) return;
		
		_instancePool.Free( instance );
	}
	
	private void FreeAllInstances()
	{
		if ( _instancePool == null ) return;
		
		for ( var instNode = _instancePool.ActiveList.First; instNode != null; instNode = instNode.Next )
		{
			SetInstanceStateFree( instNode.Value );
		}

		_instancePool.FreeAll();
	}
	
	private GameObject ReallocateOldestInstance()
	{
		if ( _instancePool == null ) return null;
		
		_instancePool.FreeOldest();
		
		return AllocateInstanceCore();
	}
	
	private GameObject GrowAndReallocateInstance()
	{
		int	deltaCount = 0;

		switch ( _onInstantiateFailed )
		{
		case EInstantiateFailedPolicy.GROW_BY_ONE:
			deltaCount = 1;
			break;
		case EInstantiateFailedPolicy.GROW_BY_ORIG:
			deltaCount = _origInstanceCount;
			break;
		case EInstantiateFailedPolicy.GROW_DOUBLE:
			deltaCount = _instances.Count;
			break;
		}

		if ( deltaCount == 0 ) return null;

		for ( int i = 0; i < deltaCount; ++i )
		{
			_instances.Add( CreateInstance( _instances[ _growCloneInstanceIdx ] ) );

			_growCloneInstanceIdx++;
			if ( _growCloneInstanceIdx >= _origInstanceCount )
			{
				_growCloneInstanceIdx = 0;
			}
		}

		if ( _instancePool == null ) return null;
		
		_instancePool.Size += deltaCount;

		OnGrow();
		
		return AllocateInstanceCore();
	}

	private void SetInstanceStateAllocated( GameObject instance )
	{
		if ( _isEnableDefaultInstanceStateSetting )
		{
			instance.SetActive( true );
		}

		if ( _OnSetInstanceStateAllocated != null )
		{
			_OnSetInstanceStateAllocated( instance );
		}
	}

	private void SetInstanceStateFree( GameObject instance )
	{
		if ( _isEnableDefaultInstanceStateSetting )
		{
			instance.SetActive( false );
			#if UNITY_2018_3_OR_NEWER
			instance.transform.SetParent( _pInstanceContainerTransform, worldPositionStays:true );
			#else
			instance.transform.parent = _pInstanceContainerTransform;
			#endif
			instance.transform.localPosition = Vector3.zero;
		}

		if ( _OnSetInstanceStateFree != null )
		{
			_OnSetInstanceStateFree( instance );
		}
	}

	private void InitUsageData()
	{
		_usageData = new UsageData();
		_usageData._minFreeCount = _instancePool.FreeCount;
		_usageData._growCount = 0;
	}

	private void OnInstanceAllocated()
	{
		if ( _usageData != null )
		{
			int	freeCount = _instancePool.FreeCount;

			if ( freeCount < _usageData._minFreeCount )
			{
				_usageData._minFreeCount = freeCount;
			}
		}
	}

	private void OnGrow()
	{
		if ( _usageData != null )
		{
			++_usageData._growCount;

			if ( !_isGrowOk )
			{
				Logger.LogWarning( _pLogTag + "- Has grown " + _usageData._growCount + " times; currSize=" + _instances.Count + ", origSize=" + _origInstanceCount );
			}
		}
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
	
	Object IObjectInstantiator.CreateInstance()
	{
		return AllocateFromPool();
	}
	
	void IObjectInstantiator.DestroyInstance( Object inst )
	{
		ReturnToPool( (GameObject)inst );
	}
}

