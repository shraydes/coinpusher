using UnityEngine;

//CC20141029 - fixed flawed DTKeyedByTypeCollection implementation
public class Facades<T> where T : class
{
//	static DTKeyedByTypeCollection<object> facades = new DTKeyedByTypeCollection<object>();
	private static T	_instance;
	
	public static void Register(T instance)
	{
//        int index = facades.GetIndexForKey<T>();
//		
//        if (index < 0)
//        {
//            facades.Add(instance);
//        }
//        else
//		{
//			if( instance == null )
//			{
//				facades.RemoveAt( index );
//			}
//			else
//			{
//				Debug.LogWarning ("Overwritten pre-existing singleton reference for " + typeof(T).FullName);
//				facades[index] = instance;
//			}
//        }
		if ( instance != null && _instance != null )
		{
			Debug.LogWarning ("Overwritten pre-existing singleton reference for " + typeof(T).FullName);
		}
		_instance = instance;
	}
	
	public static T Instance
	{
		get
		{
//			if(facades.GetIndexForKey<T>() >= 0)
//			{
//            	return facades.Find<T>();
//			}
//			return null;
			return _instance;
		}
	}
}