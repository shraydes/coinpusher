////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// File:	DontDestroyOnLoad.cs
// Date:	31/7/12

using UnityEngine;
using System.Collections;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class:	DontDestroyOnLoad
// Date:	31/7/12
// 
// Notes:	Script to add to any game object created in code (or not) that make it a persistent object. 
//			In other words it stops it from being destroyed when a scene is changed. 

public class DontDestroyOnLoad : MonoBehaviour 
{
	void Awake()
	{
		DontDestroyOnLoad( gameObject );
	}
}
