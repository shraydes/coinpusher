﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// Pusher.cs
//
// Created: 20/05/2019 rchan
// Contributors: 
// 
// Intention: Manages the pushing motion on "Pusher".
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pusher : MonoBehaviour
{

    [SerializeField] Vector3 movementVector = new Vector3(0f, 0f, 2f);
    [SerializeField] float period = 1f;

	

    [Range(0, 1)] [SerializeField] float movementFactor;

    private Vector3 startingPos = new Vector3(0.0f, -1.3f, -0.6f);

	// Start is called before the first frame update
	void Start()
    {
		//startingPos = transform.position;
		//Debug.Log(startingPos);
    }

	// Update is called once per frame
	void Update()
	{
		//Debug.Log(startingPos);
		if(!GameManager._pInstance._levelTransitioning)
		{
			float cycles = Time.time / period; // grows from 0, continously
			const float tau = Mathf.PI * 2; // 6.2 2x pi
			float rawSinWave = Mathf.Sin(cycles * tau);

			movementFactor = rawSinWave / 2f + 0.5f;

			Vector3 offset = movementVector * movementFactor;
			transform.position = startingPos + offset;
		}
	}
}
