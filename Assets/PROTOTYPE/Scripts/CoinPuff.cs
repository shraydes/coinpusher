﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// CoinPuff.cs
//
// Created: 29/05/2019 rchan
// Contributors: 
// 
// Intention: Manages the puff effect and what comes before/after.
//
// Notes: Prefab attached to the puff prefab.
//Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinPuff : PoolableObject
{

	// need to remake into object pool

	private const int MULTIPLIER = 2;

	public TextMesh _puffText;	

	private float _defaultTime = 1.5f;
	private float _timerDisable;
	

	private void Awake()
	{
		if (_puffText == null)  // ?
			return;

		
	}
	// Start is called before the first frame update
	void Start()
    {
	}

	private void OnEnable()
	{
		_timerDisable = 0.0f;

		if (GameManager._pInstance._multiply)
			_puffText.text = "+" + ( GameManager._pInstance._coinValue * MULTIPLIER);
		else
			_puffText.text = "+" + GameManager._pInstance._coinValue;
	}

	// Update is called once per frame
	void Update()
	{
		_timerDisable += Time.deltaTime;
		if (_timerDisable >= _defaultTime)
		{
			ReturnOurselvesToPool();
		}
	}
}
