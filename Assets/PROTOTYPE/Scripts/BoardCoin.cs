﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// BoardCoin.cs
//
// Created: 31/05/2019 rchan
// Contributors: 
// 
// Intention: To identify the coins on the board.
//
// Notes:
//Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardCoin : MonoBehaviour
{
	private Vector3 _coinStartPos;
	private Vector3 _coinHidePos = new Vector3(-15, 0, 0);
	private Quaternion _coinStartRotation;
	private Rigidbody _rb;

	private float _timer;
	private int _timeToFreeze = 1;

	private bool _freeze;
	private bool _deFreeze;

	private void Awake()
	{
		_coinStartRotation = transform.localRotation;
		_coinStartPos = transform.position;
		_rb = GetComponent<Rigidbody>();
	}


	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (GameManager._pInstance._transitionDelaying)
		{
			if(!_rb.isKinematic)
			{
				_rb.isKinematic = true;
				transform.position = _coinHidePos;
			}
		}

        if (_freeze)
		{
			_timer += Time.deltaTime;
			if(_timer > _timeToFreeze)
			{
				_freeze = false;
				_timer = 0;
				_rb.isKinematic = true;
				_rb.useGravity = false;
				_rb.velocity = new Vector3(0, 0, 0);
			}
		}
    }

	public void ResetBoardCoins()
	{
		if (_rb == null)
			return;
		_rb.isKinematic = true;
		Debug.Log("To pos" + _coinStartPos);

		gameObject.SetActive(true);
		_rb.velocity = new Vector3(0, 0, 0);
		transform.rotation = _coinStartRotation;
		transform.position = _coinStartPos;

	}

	public void SolidifyBoardCoins()
	{
		if (_rb == null)
			return;
		Debug.Log("Actual pos " + transform.position);
		_rb.isKinematic = false;
		_rb.useGravity = true;
	}


	public void OnTriggerExit(Collider other)   // 
	{
		if (other.tag == "ExtraEndCoin")
		{
			_freeze = true;
		}
	}

}
