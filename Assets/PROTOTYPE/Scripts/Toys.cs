﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// Toy.cs
//
// Created: 04/06/2019 rchan
// Contributors: 
// 
// Intention: Toy functions.
//
// Notes:
//Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toys : MonoBehaviour
{
	private const string TOY_VALUE_BASE_TEXT = "+ $";
	private Vector3 UI_CAMERA_ADJUST = new Vector3 (0, 1, 8);
	private const float TOY_DISPLAY_ROTATION = 25f;
	private const float TOY_DISPLAY_ADDROTATION = 100f;



	public GameObject _displayPanel;
	public GameObject _toyCamera;
	public Text _toyValueText;
	private float _timer;

	private Vector3 _toyCameraPosition;
	private Vector3 _toyStartPos = new Vector3(0, 0, -3);
	private Quaternion _toyStartRotation;
	private Rigidbody _rb;

	private int _timeToDisplayToy = 5;
	private float _rotation;


	private bool _freeze;
	private bool _deFreeze;
	private bool _displaying;
	private float _maxXpos = 4;

	private void Awake()
	{
		_toyStartRotation = transform.localRotation;
		_rb = GetComponent<Rigidbody>();
	}


	// Start is called before the first frame update
	void Start()
	{
		_toyCameraPosition = _toyCamera.GetComponent<Transform>().transform.position;
	}

	// Update is called once per frame
	void Update()
	{
		//if (ToyCamera._pInstance._displayingToy)
		if (_displaying)
		{
			_rotation += Time.deltaTime * TOY_DISPLAY_ADDROTATION;
			transform.localRotation = Quaternion.Euler(TOY_DISPLAY_ROTATION, _rotation, 0);
			_timer += Time.deltaTime;
			if (_timer > _timeToDisplayToy || ToyCamera._pInstance._forceEndDisplay)
			{
				_displaying = false;
				ToyCamera._pInstance._displayingToy = false;
				ToyCamera._pInstance._forceEndDisplay = false;
				_timer = 0;
				_displayPanel.SetActive(false);
				gameObject.SetActive(false);
			}
		}
	}

	public void ResetToys()
	{
		if (_rb == null)
			return;
		_rb.isKinematic = true;
		gameObject.layer = 0;
		gameObject.SetActive(true);
		transform.rotation = _toyStartRotation;
		transform.position = new Vector3(Random.Range(-_maxXpos, _maxXpos), _toyStartPos.y, _toyStartPos.z);
			
			//_toyStartPos;

		//new Vector3(Random.Range(-_maxXpos, _maxXpos), transform.position.y, transform.position.z), Quaternion.Euler(90, 0, 0));
	}


	public void SolidifyToys()
	{
		if (_rb == null)
			return;
		_rb.isKinematic = false;
		_rb.useGravity = true;
	}


	public void OnTriggerExit(Collider other)   // 
	{
		if (other.tag == "ExtraEndCoin")
		{
			DisplayToy();
		}
	}

	private void DisplayToy()
	{
		if (!ToyCamera._pInstance._displayingToy)
		{
			UIManager._pInstance._goalParticle.Play();
			_rb.isKinematic = true;
			gameObject.layer = 5;	//UI layer, magic?
			gameObject.transform.position = _toyCameraPosition + UI_CAMERA_ADJUST;
			_displaying = true;
			ToyCamera._pInstance._displayingToy = true;

			_toyValueText.text = TOY_VALUE_BASE_TEXT + GameManager._pInstance._currentToyValue;
			_displayPanel.SetActive(true);
			//*GameManager._pInstance._currentPotCoins += 500;*/   /// magic number, need to change to getset for a serializefield in gamemanager!!  //
			//gameObject.SetActive(false);
			//_freeze = true;
		}
		else
		{
			ToyCamera._pInstance.AddToy(gameObject);
		}
	}

	public void ToDisplay(GameObject gameObject)  //refactor possible with above?
	{
		UIManager._pInstance._goalParticle.Play();
		_rb.isKinematic = true;
		gameObject.layer = 5;
		gameObject.transform.position = _toyCameraPosition + UI_CAMERA_ADJUST;
		_displaying = true;
		ToyCamera._pInstance._displayingToy = true;
		_toyValueText.text = TOY_VALUE_BASE_TEXT + GameManager._pInstance._currentToyValue;
		_displayPanel.SetActive(true);
	}
}
