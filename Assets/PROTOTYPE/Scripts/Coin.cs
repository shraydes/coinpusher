﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// Coin.cs
//
// Created: 20/05/2019 rchan
// Contributors: 
// 
// Intention: Handles the Coin object(s).
//
// Notes: Attached to the CoinSpawner gameobject in Main Scene
//Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : PoolableObject
{

    [SerializeField] float _frictionAfterLanding = 0.5f;

	public GameObject _puff;

    private Collider _coll;
    private Rigidbody _rb;

	private float _randomMinTime = 0;
	private float _randomMaxTime = 1f;
	private float _randomTime;
	private float _time;
	// private bool _puffed = false;

	//private PoolableObject _puff;

	// Start is called before the first frame update
	void Awake()
    {
        _coll = gameObject.GetComponent<Collider>();
        if(_coll == null)
            Debug.LogWarning("set up info for new user.");

        _rb = GetComponent<Rigidbody>();

		_randomTime = (Random.Range(_randomMinTime, _randomMaxTime));
	}

	// Update is called once per frame
	void Update()
	{
		if (GameManager._pInstance._transitionDelaying)
		{
			_time += Time.deltaTime;
			if (_time > _randomTime)
			{
				//_puffed = true;
				//EnableCoinPuffs();
				SpawnCoinPuff();
				_time = 0;
				//SpawnCoinPuff();    // puff before requeue

				ReturnOurselvesToPool();
			}
		}
	}

	//public void EnableCoinPuffs() // todo change from instantiate to pooling
	//{
	//	GameManager._pInstance.CoinCountFromTransitioning();        // adds coin value for each coin into bank
	//	Instantiate(_puff, transform.position, Quaternion.identity);
	//}

	public void SpawnCoinPuff( int numberToSpawn = 1 )
	{
		for (int count = 0; count < numberToSpawn; count++)
		{
			PoolableObject newCoinPuff = ObjectPool._pInstance.SpawnFromPool(ObjectPool.POOL_OBJECT_TYPE_COINPUFFS,
			new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);

			GameManager._pInstance.CoinCountFromTransitioning();
			//_puff = newCoinPuff;

		}
	}

	public void OnCollisionEnter(Collision collision)
    {
        if(collision == null)
            return;

        if(collision.gameObject.name == "Pusher" || collision.gameObject.name == "MachineBottom")
        {
			
			if(_coll != null)
            {
                _coll.material.dynamicFriction = _frictionAfterLanding;
            }
            //rb.mass = 100;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.tag == "EndCoin")
        {
            Debug.Log("GOAL COIN");

			ObjectPool._pInstance.ReturnObjectToPool(this);

        }
    }

	


	// for pool?
	void OnEnable()
	{

	}


	void OnDisable()
	{

	}

}
