﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// GoalCoin.cs
//
// Created: 20/05/2019 rchan
// Contributors:  chope
// 
// Intention: Detects objects passing through, namely Coins.
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalCoin : MonoBehaviour
{


	public static GoalCoin _pInstance;

	[Header("Core")]
	public int _goalCount;

	private void Awake()
	{
		if(_pInstance == null)
		{
			_pInstance = this;
		}

		else if(_pInstance != this)
		{
			Destroy(gameObject);
		}
		DontDestroyOnLoad(gameObject);
	}

	private void OnDestroy()
	{
		if(_pInstance == this)
		{
			_pInstance = null;
		}
	}


	// Start is called before the first frame update
	void Start()
    {

    }

	// Update is called once per frame
	void Update()
	{
		//Debug.Log(_goalCount);
		
	}

    public void OnTriggerExit(Collider other)
    {
        //Coin coin = other.gameObject.GetComponent<Coin>();
        //if(coin == null)
        //    return;

		if(other.tag == "Coin")
		{
			if(!GameManager._pInstance._levelTransitioning && !GameManager._pInstance._transitionDelaying)
			{
				_goalCount++;
				GameManager._pInstance.CoinCount();

			}
		}

		if (other.tag == "Toy")
		{
			if (!GameManager._pInstance._levelTransitioning && !GameManager._pInstance._transitionDelaying)
			{
				

				GameManager._pInstance.ToyCount();
				//_confetti.Play();
			}
		}

	}

}
