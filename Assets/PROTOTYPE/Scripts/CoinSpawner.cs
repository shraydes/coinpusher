﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// CoinSpawner.cs
//
// Created: 20/05/2019 rchan
// Contributors: chope
// 
// Intention: Handles the way the coins are spawned/repositioned.
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
	public static CoinSpawner _pInstance;



	[Header("Design")]
	[SerializeField] float _FrenzySpeedPercent = 100f;
	[SerializeField] float _maxXpos = 3;
	[SerializeField] public float _defaultspawnTime = 1.1f;
	[SerializeField] float _coinFrenzyTime = 10f;

	//DEBUG
	public float _savedDefaultSpawnTime;

	public bool _coinFrenzy;

	private float _reTapTime = 0.1f;
	public bool _allowTap;
	private float _tapTimer;
    private float _timerSpawn;
	private float _timerFrenzy;
	public float _saveSpawnTime;
	private Vector3 COIN_DOWNFORCE_ONSPAWN = new Vector3(0,-5,0);


	private void Awake()
	{
		if(_pInstance == null)
			_pInstance = this;

		else if(_pInstance != this)
			Destroy(this);

		DontDestroyOnLoad(gameObject);
	}

	private void OnDestroy()
	{
		if(_pInstance == this)
		{
			_pInstance = null;
		}
	}
	
	void Start()
    {
		_coinFrenzy = false;
		_savedDefaultSpawnTime = _defaultspawnTime;
	}

	public void ResetCoinSpeed()
	{
		_defaultspawnTime = _savedDefaultSpawnTime;
	}

	void Update()
    {

		if (!_allowTap)
		{
			_tapTimer += Time.deltaTime;
			if (_tapTimer > _reTapTime)
			{
				_tapTimer = 0;
				_allowTap = true;
			}
		}

		if (_coinFrenzy)
		{
			UIManager._pInstance.PressedBlueFrenzyButton(UIManager._pInstance._coinFrenzyButton);
			//_defaultspawnTime = _saveSpawnTime / 2;  // todo to be changed to designer float! 
			_timerFrenzy += Time.deltaTime;
			if (_timerFrenzy > _coinFrenzyTime)
			{
				Debug.Log("Resetted spawntime");
				_defaultspawnTime = _saveSpawnTime;
				_timerFrenzy = 0;
				UIManager._pInstance.BlueFrenzyButton(UIManager._pInstance._coinFrenzyButton);
				_coinFrenzy = false;

			}
		}
		
		if (!GameManager._pInstance._levelTransitioning && !GameManager._pInstance._transitionDelaying)
		{
			LevelManager._pInstance._fakePusher.SetActive(false);
			_timerSpawn += Time.deltaTime;
			if(_timerSpawn >= _defaultspawnTime)
			{
				UIManager._pInstance._allowLevelButton = true;
				_timerSpawn = 0.0f;
				SpawnCoin();
				if (_coinFrenzy)
					SpawnCoin(2);	
			}
		}

		if(Input.GetKeyDown(KeyCode.X))   // to reduce clicking = noise
		{
			SpawnCoin(5);
		}
    }

	public void SpawnCoinFromTap()
	{

		if (!GameManager._pInstance._levelTransitioning && !GameManager._pInstance._transitionDelaying)
		{
			if (_allowTap)
			{
				_allowTap = false;
				SpawnCoin();
			}
		}
	}

    public void SpawnCoin( int numberToSpawn = 1 )
    {
        for (int count = 0; count < numberToSpawn; count++)
        {
            PoolableObject newCoin = ObjectPool._pInstance.SpawnFromPool(ObjectPool.POOL_OBJECT_TYPE_COIN, 
				new Vector3(Random.Range(-_maxXpos, _maxXpos), transform.position.y, transform.position.z), Quaternion.Euler(90, 0, 0));

			if (newCoin != null)
			{
				newCoin.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
				newCoin.GetComponent<Collider>().material.dynamicFriction = 0;

				// EXPERIMENTAL

				Rigidbody rb = newCoin.GetComponent<Rigidbody>();

				rb.velocity = COIN_DOWNFORCE_ONSPAWN;
				rb.AddForce(Physics.gravity * (rb.mass * rb.mass));
			}
        }
    }
}
