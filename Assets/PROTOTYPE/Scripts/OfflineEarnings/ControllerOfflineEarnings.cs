﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// ControllerOfflineEarnings.cs
//
// Created: 05/06/2019 chope 
// Contributors: 
// 
// Intention: Handles the offline earning values, times and notifications. 
//
// Notes: 
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
// CLASS ControllerOfflineEarnings
public class ControllerOfflineEarnings : MonoBehaviour
{
	//------------------------------------------------------------------------------------------------------------------
	// CONSTS / DEFINES
#if UNITY_EDITOR
	private const string DEBUG_LOG_PREFIX = "<color=#ffcccc><b>ControllerOfflineEarnings - </b></color>";
#else
	private const string DEBUG_LOG_PREFIX = "ControllerOfflineEarnings - ";
#endif

	private const string SAVE_KEY_LAST_TIME = "OE_LD";
	private const string SAVE_KEY_EARNING_PER_SECOND = "OE_EPS";

	private const float TIME_MIN_TO_SHOW_EARNINGS = 60f; //60.0f * 2.0f; // 5 mins. 
	private const float EARNINGS_MAX = 1000000.0f;		///

	//------------------------------------------------------------------------------------------------------------------
	// SINGLETON

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	[SerializeField] int _offlineEarningdividingFactor = 3;

	private bool _initialised = false;
	private bool _displayWhenPossible = false;
	private float _offlineEarnings;
	
	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES

	//------------------------------------------------------------------------------------------------------------------
	// EVENTS / DELEGATES

	//------------------------------------------------------------------------------------------------------------------
	private void Awake()
	{
		OfflineEarningsPopup._OnRewardClaimed += OnRewardClaimed;
	}

	//------------------------------------------------------------------------------------------------------------------
	private void Start()
	{
		_initialised = true;

		DontDestroyOnLoad( gameObject );

		_displayWhenPossible = CalculateOfflineEarnings();
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private void Update()
	{
		if( _displayWhenPossible )
        {
			//show the popup.
			if( OfflineEarningsPopup._pInstance != null )
			{
				OfflineEarningsPopup._pInstance.ShowPopUp( ( int )_offlineEarnings );
				_displayWhenPossible = false;
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void OnDestroy()
	{
		OfflineEarningsPopup._OnRewardClaimed -= OnRewardClaimed;
	}

	//------------------------------------------------------------------------------------------------------------------
    private void OnApplicationPause( bool pause )
    {
        if( !pause )
        {
			// work out, award and display actual offline earnings. 
			_displayWhenPossible = CalculateOfflineEarnings();
        }
        else
        {
			// work out how long till max offline earnings and schedule push notification. 
			PrepareForSessionEnd();
        }
    }

	//------------------------------------------------------------------------------------------------------------------
    public void OnApplicationQuit()
    {
        PrepareForSessionEnd();
		PlayerPrefs.Save();
    }

	//------------------------------------------------------------------------------------------------------------------
    public void OnRewardClaimed()
    {
		// award the calculated awards.
		if( GameManager._pInstance != null )
			GameManager._pInstance.DeltaCoinAmountToGatheredTotal( ( int )_offlineEarnings );
    }

	//------------------------------------------------------------------------------------------------------------------
	// return true if we should show the dialogue popup. 
	private bool CalculateOfflineEarnings()
	{
		if( !_initialised )
			return false;

		_offlineEarnings = 0;

		Debug.Log( DEBUG_LOG_PREFIX + "Calculating Offline Earnings." );

		if( !PlayerPrefs.HasKey( SAVE_KEY_LAST_TIME ) )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "No Saved last session time, this must be our first run." );
			return false;
		}

		if( GameManager._pInstance == null )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "required data not present, no offline earnings will be calculated." );
			return false;
		}

		float earningsPerSecond = 0.0f;

		// look up the saved earnings per second. 
		if( PlayerPrefs.HasKey( SAVE_KEY_EARNING_PER_SECOND ) )
		{
			earningsPerSecond = PlayerPrefs.GetFloat( SAVE_KEY_EARNING_PER_SECOND );
		}

		if( earningsPerSecond <= 0.0f )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Earnings per second was 0, no rewards to be given." );
			return false;
		}

		// look up the date now and when we last played. 
		DateTime timeNow = DateTime.UtcNow;

		string lastDay = PlayerPrefs.GetString( SAVE_KEY_LAST_TIME );
		DateTime lastTime = Convert.ToDateTime( lastDay );

		TimeSpan span = timeNow - lastTime;

		double seconds = span.TotalSeconds;
		Debug.Log( DEBUG_LOG_PREFIX + "It Has Been " + seconds + " Seconds Since We Have Run" );

		// if it hasnt been long enough to bother here, just return. 
		if( seconds <= TIME_MIN_TO_SHOW_EARNINGS )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "This is not enough time to allow offline earning rewards." );
			return false;
		}

		_offlineEarnings = ( float )( seconds * earningsPerSecond );

		// multiply this by the offline earning upgrade multiplier. 
		_offlineEarnings *= GameManager._pInstance._offlineEarning;

		// cap this with the max capacity. 
		_offlineEarnings = Mathf.Min( _offlineEarnings, GameManager._pInstance._pOfflineCapacity );

		Debug.Log( DEBUG_LOG_PREFIX + "Offline earnings caluculated to be: " + _offlineEarnings );

		return ( _offlineEarnings > 0 );
	}

	//------------------------------------------------------------------------------------------------------------------
	private void PrepareForSessionEnd()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Preparing for session end" );

		// work out the earnings per second at this time. 
		float earningsPerSecond = 0.0f;
		if( GameManager._pInstance != null )
		{
			if( CoinSpawner._pInstance != null )
			{
				// the coin value divided by the time between idle coin drops.  
				earningsPerSecond = GameManager._pInstance._coinValue * GameManager._pInstance._offlineEarning / _offlineEarningdividingFactor;	 
				//  GameManager._pInstance._coinValue / CoinSpawner._pInstance._defaultspawnTime;// / _dividingFactor;
			}
		}

		// save the earnings per second value. 
		Debug.Log( DEBUG_LOG_PREFIX + "Earnings Per Second will be: " + earningsPerSecond );
		PlayerPrefs.SetFloat( SAVE_KEY_EARNING_PER_SECOND, earningsPerSecond );

		// save the time now value. 
		DateTime currentTime = DateTime.UtcNow;
		string currentTimeString = currentTime.ToString();
		Debug.Log( DEBUG_LOG_PREFIX + "Session End Time is: " + currentTimeString );
		PlayerPrefs.SetString( SAVE_KEY_LAST_TIME, currentTimeString );
	}
}
