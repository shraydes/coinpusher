﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// OfflineEarningsPopup.cs
//
// Created: 05/06/2019 chope 
// Contributors: 
// 
// Intention: 
//
// Notes: 
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using UnityEngine;
using UnityEngine.UI;

//----------------------------------------------------------------------------------------------------------------------
// CLASS OfflineEarningsPopup
public class OfflineEarningsPopup : MonoBehaviour
{
	//------------------------------------------------------------------------------------------------------------------
	// CONSTS / DEFINES
	private const string PREFIX_AMOUNT = "$";

	//------------------------------------------------------------------------------------------------------------------
	// SINGLETON
	private static OfflineEarningsPopup _instance;
	public static OfflineEarningsPopup _pInstance
	{
		get => _instance;
	}

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	[SerializeField]
	private Text _txtAmount;
	[SerializeField]
	private GameObject _goPopup;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES

	//------------------------------------------------------------------------------------------------------------------
	// EVENTS / DELEGATES
	public static System.Action _OnRewardClaimed;

	//------------------------------------------------------------------------------------------------------------------
	private void Awake()
	{
		if( _instance == null )
			_instance = this;
	}

	//------------------------------------------------------------------------------------------------------------------
	private void Start()
	{
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private void Update()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	private void OnDestroy()
	{
		if( _instance == this )
			_instance = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void ShowPopUp( int earningsAmount )
	{
		if( _txtAmount != null )
			_txtAmount.text = PREFIX_AMOUNT + earningsAmount.ToString();

		if( _goPopup != null )
			_goPopup.SetActive( true );
	}

	//------------------------------------------------------------------------------------------------------------------
	public void OnBtnClaim()
	{
		// turn off popup
		if( _goPopup != null )
			_goPopup.SetActive( false );

		// tell any subscribers of event. 
		if( _OnRewardClaimed != null )
			_OnRewardClaimed();

		// show effects too??
	}
}
