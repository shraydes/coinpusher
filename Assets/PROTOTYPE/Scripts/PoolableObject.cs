﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableObject : MonoBehaviour
{
	private string _poolKeyName;

	public string _pPoolKeyName
	{
		get
		{
			return _poolKeyName;
		}
		set
		{
			_poolKeyName = value;
		}
	}

	protected void ReturnOurselvesToPool()
	{
		if (ObjectPool._pInstance == null)
		{
			Debug.LogWarning( "trying to return object to pool but cant find it, destroying instead. " );
			Destroy(gameObject);
			return;
		}

		ObjectPool._pInstance.ReturnObjectToPool(this);
	}
}
