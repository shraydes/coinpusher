﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// ObjectPool.cs
//
// Created: 20/05/2019 rchan
// Contributors:  chope
// 
// Intention: Handles the pools/dictionaries in the game.
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public const string POOL_OBJECT_TYPE_COIN = "Coins";
	public const string POOL_OBJECT_TYPE_COINPUFFS = "CoinPuffs";

	public static ObjectPool _pInstance;

	public enum eEmptyAction
	{
		NOTHING,
		INSTANTIATE
	}

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

	public eEmptyAction _emptyAction;
    public List<Pool> pools;
    public Dictionary<string, Queue<PoolableObject>> poolDictionary;
	
    void Awake()
    {
        _pInstance = this;

        poolDictionary = new Dictionary<string, Queue<PoolableObject>>();

        if(pools != null)
        {
            for (int count = 0; count < pools.Count; count++)
            {
				if(pools[count] == null)
					continue;

				Queue<PoolableObject> objectPool = new Queue<PoolableObject>();

                for (int i = 0; i < pools[count].size; i++)
                {
					GameObject obj = Instantiate(pools[count].prefab);
					PoolableObject poolableObj = obj.GetComponent<PoolableObject>();
					
					if(poolableObj == null)
					{
						// debug log to warn user is trying to pool an object with a class that doesnt inherit from PoolableObject. 
						Destroy(obj);
						continue;
					}

					obj.transform.parent = transform;
                    obj.SetActive(false);
                    objectPool.Enqueue(poolableObj);
                }

                poolDictionary.Add(pools[count].tag, objectPool);
            }
        }
    }

    public PoolableObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if(!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag + " doesn't exist.");
            return null;
        }
		
		PoolableObject objectToSpawn = null;

		// if the queue is empty either return or make a new instance. 
		if ( poolDictionary[tag].Count <= 0 )
		{
			switch (_emptyAction)
			{
				case eEmptyAction.NOTHING:
					return null;
				default:
					if (pools != null)
					{
						for (int count = 0; count < pools.Count; count++  )
						{
							if (pools[count] == null)
								continue;

							if( pools[count].tag == tag)
							{
								GameObject obj = Instantiate(pools[count].prefab);
								objectToSpawn = obj.GetComponent<PoolableObject>();
							}
						}
					}

					Debug.Log("Poole instantiation at run time, increase pool size in editor: " + tag);
					break;

			}
		}
		else
		{
			objectToSpawn = poolDictionary[tag].Dequeue();
		}

		objectToSpawn.gameObject.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

		objectToSpawn._pPoolKeyName = tag;

		//IPooledObject pooledObj = objectToSpawn.GetComponent<IPooledObject>();

		//if( pooledObj != null )
		//{
		//    pooledObj.OnObjectSpawn();
		//}

		//poolDictionary[tag].Enqueue(objectToSpawn);


		return objectToSpawn;   
    }

	public void ReturnObjectToPool(PoolableObject objToReturn)
	{
		if(objToReturn == null)
			return;

		if(!poolDictionary.ContainsKey(objToReturn._pPoolKeyName))
		{
			Debug.LogWarning("Pool with tag " + tag + " doesn't exist.");
			return;
		}

		objToReturn.gameObject.SetActive(false);
		poolDictionary[objToReturn._pPoolKeyName].Enqueue(objToReturn);

	}

}