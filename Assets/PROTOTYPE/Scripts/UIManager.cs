﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// UIManager.cs
//
// Created: 20/05/2019 rchan
// Contributors: chope
// 
// Intention: Handling in/out UI data.
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	private const string DOLLAR_SIGN_LEFT = "$";
	private const float COLOR_MULTIPLIER_DEFAULT = 1f;

	public static UIManager _pInstance = null;

	[Header("Core")]
	
	public ParticleSystem _goalParticle;
	public ParticleSystem _claimParticle;
	public ParticleSystem _readyLevelUpParticle;
	public ParticleSystem _levelUpParticle;
	public GameObject _progressionSlider;         //null check all of these?
	public GameObject _levelUpButton;
	public GameObject _claimButtonObject;
	public GameObject _transitionTextObj;
	public GameObject _visualCollider;
	public GameObject _potTextObj;
    public Text _coinText;
	public Text _potText;
    public Text _transitionText;
	public Text _currentLevelText;
	public Text _levelUpPanelText;
	public Button _coinFrenzyButton;
	public Button _claimButton;
	public Button _upgrCVButton;
	public Button _upgrCSButton;
	public Button _upgrOEButton;
	public Button _upgrOCButton;
	public Text _upgrCVBtnTxt;
	public Text _upgrCSBtnTxt;
	public Text _upgrOEBtnTxt;
	public Text _upgrOCBtnTxt;
	public Text _actualCVTxt;
	public Text _actualCSTxt;
	public Text _actualOETxt;
	public Text _actualOCTxt;                   //null check all of these?
	public ColorBlock _grayUpgradeColor;
	public int _totalCoins;
	public bool _allowLevelButton;
	public bool _levelingUp;



	private ColorBlock _greenUpgradeColor;
	private ColorBlock _redUpgradeColor;
	private ColorBlock _yellowButtonColor;
	private ColorBlock _blueButtonColor;
	private ColorBlock _pressedBlueButtonColor;

	private Color32 COLORS_FOR_PRESSEDBLUE_BUTTON = new Color32(46, 117, 181, 0);
	private Color32 COLORS_FOR_BLUE_BUTTON = new Color32(0, 135, 255, 255);
	private Color32 COLORS_FOR_GREEN_UPGRADE = new Color32(34, 250, 16, 255);  // couldnt const!
	private Color32 COLORS_FOR_RED_UPGRADE = new Color32(255, 0, 0, 255);
	private Color32 COLORS_FOR_YELLOW_BUTTON = new Color32(255, 255, 0, 255);
	//changing from Color to Color32 seemed to fix "bleach" problem
	private Color32 COLORS_FOR_FINISHED_UPGRADE = new Color32(240, 240, 240, 240); 

	private Color32 COLORS_FOR_PRESSED = new Color32(255, 255, 162, 255);  


	private void Awake()
    {
        if(_pInstance == null)
        {
            _pInstance = this;
        }

        else if(_pInstance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {

		_allowLevelButton = true;
		_visualCollider.SetActive(false);

		//_allowClaimButton = true;
		_greenUpgradeColor.colorMultiplier = COLOR_MULTIPLIER_DEFAULT;
		_greenUpgradeColor.normalColor = COLORS_FOR_GREEN_UPGRADE;
		_greenUpgradeColor.pressedColor = COLORS_FOR_PRESSED; //30 may COLORS_FOR_GREEN_UPGRADE;
		_greenUpgradeColor.disabledColor = COLORS_FOR_GREEN_UPGRADE;
		_greenUpgradeColor.highlightedColor = COLORS_FOR_GREEN_UPGRADE;

		_redUpgradeColor.colorMultiplier = COLOR_MULTIPLIER_DEFAULT;
		_redUpgradeColor.normalColor = COLORS_FOR_RED_UPGRADE;
		_redUpgradeColor.pressedColor = COLORS_FOR_RED_UPGRADE;
		_redUpgradeColor.disabledColor = COLORS_FOR_RED_UPGRADE;
		_redUpgradeColor.highlightedColor = COLORS_FOR_RED_UPGRADE;     //maybe not neccessary but just incase

		_yellowButtonColor.colorMultiplier = COLOR_MULTIPLIER_DEFAULT;
		_yellowButtonColor.normalColor = COLORS_FOR_YELLOW_BUTTON;
		_yellowButtonColor.pressedColor = COLORS_FOR_FINISHED_UPGRADE;
		_yellowButtonColor.disabledColor = COLORS_FOR_YELLOW_BUTTON;
		_yellowButtonColor.highlightedColor = COLORS_FOR_YELLOW_BUTTON;

		_grayUpgradeColor.colorMultiplier = COLOR_MULTIPLIER_DEFAULT;
		_grayUpgradeColor.normalColor = COLORS_FOR_FINISHED_UPGRADE;
		_grayUpgradeColor.pressedColor = COLORS_FOR_FINISHED_UPGRADE;
		_grayUpgradeColor.disabledColor = COLORS_FOR_FINISHED_UPGRADE;
		_grayUpgradeColor.highlightedColor = COLORS_FOR_FINISHED_UPGRADE;

		_blueButtonColor.colorMultiplier = COLOR_MULTIPLIER_DEFAULT;
		_blueButtonColor.normalColor = COLORS_FOR_BLUE_BUTTON;
		_blueButtonColor.pressedColor = COLORS_FOR_BLUE_BUTTON;
		_blueButtonColor.disabledColor = COLORS_FOR_BLUE_BUTTON;
		_blueButtonColor.highlightedColor = COLORS_FOR_BLUE_BUTTON;

		_pressedBlueButtonColor.colorMultiplier = COLOR_MULTIPLIER_DEFAULT;
		_pressedBlueButtonColor.normalColor = COLORS_FOR_PRESSEDBLUE_BUTTON;
		_pressedBlueButtonColor.pressedColor = COLORS_FOR_PRESSEDBLUE_BUTTON;
		_pressedBlueButtonColor.disabledColor = COLORS_FOR_PRESSEDBLUE_BUTTON;
		_pressedBlueButtonColor.highlightedColor = COLORS_FOR_PRESSEDBLUE_BUTTON;
	}

	// Update is called once per frame
	void Update()
	{
		//Debug.Log("Slider value " + GameManager._pInstance._progressionSlider.value);

		if(!GameManager._pInstance._levelTransitioning && !UIManager._pInstance._levelingUp)
		{
		
			_coinText.text = DOLLAR_SIGN_LEFT + (int)GameManager._pInstance._gatheredTotalCoins;
			//_progressionSlider.SetActive(true);
			if(GameManager._pInstance._progressionSlider.value >= GameManager._pInstance._finishAmount)
			{
				if(_allowLevelButton)
				{
					_levelUpButton.SetActive(true);
					_allowLevelButton = false;
				}
			}
		}

		if(GameManager._pInstance._levelTransitioning)   // on wrong script cause of calculating?
		{
			_potTextObj.SetActive(false);
			_claimButtonObject.SetActive(false);
		}
		else
		{
			_potTextObj.SetActive(true);
			_claimButtonObject.SetActive(true);
		}
    }

	public void GreenUpgradeButton(Button button)
	{
		button.colors = _greenUpgradeColor;
		button.interactable = true;
	}

	public void RedUpgradeButton(Button button)
	{
		button.colors = _redUpgradeColor;
		button.interactable = false;
	}

	public void YellowClaimButton(Button button)
	{
		button.colors = _yellowButtonColor;
		button.interactable = true;
	}

	public void BlueFrenzyButton(Button button)
	{
		button.colors = _blueButtonColor;
		button.interactable = true;
	}

	public void PressedBlueFrenzyButton(Button button)
	{
		button.colors = _pressedBlueButtonColor;
		button.interactable = false;
	}

	public void AdjustButtonColour(Button button, float gatheredCoins, float coinCost)
	{
		if(gatheredCoins < coinCost)
		{
			RedUpgradeButton(button);
		}
		else
		{
			GreenUpgradeButton(button);
		}
	}
}
