﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// BoardCoinsHolder.cs
//
// Created: 04/06/2019 rchan
// Contributors: 
// 
// Intention: To identify the toys on the board.
//
// Notes: 
//Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardToysHolder : MonoBehaviour
{
	public static BoardToysHolder _pInstance;

	public GameObject _UICamera;


	private void Awake()
	{
		if (_pInstance == null)
			_pInstance = this;

		else if (_pInstance != this)
			Destroy(this);

		DontDestroyOnLoad(gameObject);
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	//public void RandomToy()
	//{

	//	Toys[] toyObjects = GetComponentsInChildren<Toys>();

	//	int counter = 0;

	//	for (int i = 0; i < toyObjects.Length; i++)
	//	{
	//		if (counter < GameManager._pInstance._maxToys)
	//		{
	//			int setObjActive = Random.Range(0, 10);
	//			if( setObjActive >= 2)
	//			{
	//				counter++;
	//				toyObjects[i].gameObject.SetActive(true);
	//			}
	//		}
	//	}
	//}

				//	if (toyObjects[i].gameObject.activeSelf)
				//{
				//	counter++;
				//}
				//else
				//{
				//	int setObjActive = (Random.Range(0, 16));   // int for probability change
					
				//	// wtf
				//	if (setObjActive)
				//		counter++;

				//	toyObjects[i].gameObject.SetActive(setObjActive);
				//} 

}
