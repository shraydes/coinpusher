﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyCamera : MonoBehaviour
{
	public static ToyCamera _pInstance;

	public bool _displayingToy;

	public bool _forceEndDisplay;


	public List<GameObject> _nextToyList = new List<GameObject>();



	private void Awake()
	{
		if (_pInstance == null)
			_pInstance = this;

		else if (_pInstance != this)
			Destroy(this);

		DontDestroyOnLoad(gameObject);
	}
		// Start is called before the first frame update
		void Start()
	{
		_displayingToy = false;
		_forceEndDisplay = false;
}

    // Update is called once per frame
    void Update()
    {
		Debug.Log("Debug list count " + _nextToyList.Count);
		if(!_displayingToy)
		{
			if(_nextToyList.Count > 0)
			{
				_nextToyList[0].GetComponent<Toys>().ToDisplay(_nextToyList[0]);
				_nextToyList.Remove(_nextToyList[0]); 
			}
		}
    }

	public void AddToy(GameObject nextToy)
	{
		_nextToyList.Add(nextToy);

	}

	public void OnClickOKButton()
	{
		_forceEndDisplay = true;
	}
}
