﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// BoardCoinsHolder.cs
//
// Created: 31/05/2019 rchan
// Contributors: 
// 
// Intention: To identify the coins on the board.
//
// Notes: 
//Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardCoinsHolder : MonoBehaviour
{
	public static BoardCoinsHolder _pInstance;


	private void Awake()
	{
		if (_pInstance == null)
			_pInstance = this;

		else if (_pInstance != this)
			Destroy(this);

		DontDestroyOnLoad(gameObject);
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
