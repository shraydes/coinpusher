﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// ButtonController.cs
//
// Created: 29/05/2019 rchan
// Contributors: 
// 
// Intention: Handles all the buttons and relays commands further.
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
	public static ButtonController _pInstance;

	public GameManager _gameManager;    // works but is this correct way of doing it?

	public Animator _totalCoinAnimation;

	private void Awake()
	{
		if (_gameManager == null)
		{
			Debug.LogWarning("Attach GameManager to this script!");
			return;
		}

		if (_pInstance == null)
			_pInstance = this;

		else if (_pInstance != this)
			Destroy(this);



		//DontDestroyOnLoad(gameObject);		// needed for upgrade panel?
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		//Debug.Log("test " + _gameManager._gatheredTotalCoins);
	}


	//DEBUG
	public void CallAddCoins()
	{
		GameManager._pInstance._gatheredTotalCoins += 10000;
	}

	//DEBUG
	public void CallDeletePrefs()
	{
		GameManager._pInstance.DeletePlayerPrefs();
	}


	public void CallCoinFrenzy()
	{
		_gameManager.CoinFrenzy();
	}

	public void CallLevelUpButton()
	{
		_gameManager.LevelUpButton();
	}

	public void CallClaimButton()
	{
		_totalCoinAnimation.SetBool("smoothCoin", true);
		//_totalCoinAnimation.Play("totalCoinText");
		_gameManager.ClaimButton();
	}

	public void CallTransitionButton()
	{
		_gameManager.TransitionButton();	
	}

	public void CallMultiplyButton()
	{
		_gameManager.MultiplyReward();
	}


	//called from buttons (upgrade panel)
	public void CallCoinValueUpgrade()
	{
		if (!GameManager._pInstance._coinLerp)
			_gameManager.CoinValueUpgrade();
	}

	public void CallCoinSpeedUpgrade()
	{
		if (!GameManager._pInstance._coinLerp)
			_gameManager.CoinSpeedUpgrade();
	}

	public void CallOfflineEarningUpgrade()
	{
		if (!GameManager._pInstance._coinLerp)
			_gameManager.OfflineEarningUpgrade();
	}

	public void CallOfflineCapacityUpgrade()
	{
		if (!GameManager._pInstance._coinLerp)
			_gameManager.OfflineCapacityUpgrade();
	}

}
