﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// GameManager.cs
//
// Created: 21/05/2019 rchan
// Contributors: 
// 
// Intention: Manages the ingame events, everything related to the game.
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	private const string LV_X_TEXT = "Lv. ";
	private const string DOLLAR_SIGN_RIGHT = "$";
	private const string DOLLAR_SIGN_LEFT = "$";
	private const string X_TEXT = "x";
	private const string PERCENT_TEXT = "%";
	private const string MAX_TEXT = "MAX";
	private const int MAX_TIER = 10;
	private const float MULTIPLIER_TWO = 2f;

	//for resetting prefs, DEBUG
	private const int RESET_VALUE_CV = 1;
	private const int RESET_VALUE_CS = 1;
	private const int RESET_VALUE_OE = 1;
	private const int RESET_VALUE_OC = 1;
	private float RESET_VALUECOST_CV;
	private float RESET_VALUECOST_CS;
	private float RESET_VALUECOST_OE;
	private float RESET_VALUECOST_OC;

	private int RESET_CURRENT_LVL = 1;
	private float RESET_FINISH_AMOUNT;

	private const float MULTIPLIER_OFFLINE_CAPACITY = 1000.0f;
	//DEBUG
	private const string SAVE_KEY_TOY_VALUE = "GM_TOY_VALUE";
	private const string SAVE_KEY_TOTAL_COINS = "GM_TOTAL_COINS";
	private const string SAVE_KEY_CURRENT_PROGRESS = "GM_CURRENT_PROGRESS";
	private const string SAVE_KEY_UPGRADES = "GM_UPGRADED";
	private const string SAVE_KEY_CV_UPGRADE = "GM_CV_UPGRADE";
	private const string SAVE_KEY_CS_UPGRADE = "GM_CS_UPGRADE";
	private const string SAVE_KEY_CS_UPGRADE_SPEED = "GM_CS_UPGRADE_SPEED";
	private const string SAVE_KEY_OE_UPGRADE = "GM_OE_UPGRADE";
	private const string SAVE_KEY_OC_UPGRADE = "GM_OC_UPGRADE";
	private const string SAVE_KEY_CURRENT_LEVEL = "GM_CURRENT_LVL";
	private const string SAVE_KEY_LVL_FINISHAMOUNT = "GM_LVL_FINISHAMOUNT";

	private const string SAVE_KEY_CV_MAX = "GM_CV_MAX";				//not used atm
	private const string SAVE_KEY_CS_MAX = "GM_CS_MAX";
	private const string SAVE_KEY_OE_MAX = "GM_OE_MAX";
	private const string SAVE_KEY_OC_MAX = "GM_OC_MAX";				//

	private const string SAVE_KEY_CV_COST = "GM_CV_COST";
	private const string SAVE_KEY_CS_COST = "GM_CS_COST";
	private const string SAVE_KEY_OE_COST = "GM_OE_COST";
	private const string SAVE_KEY_OC_COST = "GM_OC_COST";
		
	public static GameManager _pInstance;

	[Header("Design")]

	public int _maxToys = 2;  // to be serialized with get int

	[SerializeField][Tooltip("Percent")] int _toySpawnChance = 20;
	[SerializeField] float _toyValue = 50;
	[SerializeField] float _coinValueCost = 150;
	[SerializeField] float _coinSpeedCost = 100;
	[SerializeField] float _offlineEarningCost = 50;
	[SerializeField] float _offlineCapacityCost = 50;
	[SerializeField] float _finishAmountMultiplier = 1.5f;
	[SerializeField] float _upgradeCostMultiplier = 2f;
	[SerializeField][Tooltip("Not percent")] float _upgrCVIncrement = .5f;
	[SerializeField][Tooltip("In percent")] float _upgrCSIncrement = 10;
	[SerializeField][Tooltip("In percent")] float _upgrOEIncrement = 10;
	[SerializeField][Tooltip("In percent")] float _upgrOCIncrement = 10;




	[Header("Core")]
	public float _currentToyValue;
	public List<GameObject> _toys;
    public Slider _progressionSlider;
	public Slider _transparentSlider;
	public GameObject _claimCoinAnim;
	public GameObject _boardCoinsHolder;
	public GameObject _toyHolder;
	public float _coinValue = 1;
	public float _coinSpeed = 1;
	public float _offlineEarning = 1;
	public float _offlineCapacity = 1;
	public float _gatheredTotalCoins;
	public float _potCoins;
	public float _finishAmount = 250;

	public float _currentlevelCoins;
	public float _currentPotCoins;        //30 May
	public float _transitionDelay = 2.5f;
	public bool _transitionDelaying;
	public bool _levelTransitioning;
	public bool _upgrCVMax = false;
	public bool _upgrCSMax = false;
	public bool _upgrOEMax = false;
	public bool _upgrOCMax = false;
	public bool _multiply;

	public bool _progressLerp;
	public bool _coinLerp;
	public bool _allowClaim;


	private int _lowestSliderValue = 1;
	private int _currentLevelID = 1;      // get) change later on
	private float _transitionTimer;
	private int _upgrCVTier = 0;
	private int _upgrCSTier = 0;
	private int _upgrOETier = 0;
	private int _upgrOCTier = 0;

	private float _currentProgressValue;

	private float _coinLerpSpeed = 5f;
	private float _claimTimer = 5;
	private float _timer;
	private float _fillSpeed = 1f;
	
	private float _totalCoinLerpValue;
	private float _approximationValue1 = 1f;
	private float _approximationValue2 = 5f;

	public float _pOfflineCapacity
	{
		get
		{
			// the capacity is the displayed capacity e.g. 1.7f multiplied by a standard multipler to increase number
			// to the 1000s ranges. 
			return _offlineCapacity * MULTIPLIER_OFFLINE_CAPACITY;
		}
	}

	private void Awake()
    {
		if (_pInstance == null)
			_pInstance = this;

		else if (_pInstance != this)
			Destroy(this);

		DontDestroyOnLoad(gameObject);

		if (_progressionSlider == null)
		{
			Debug.LogWarning("Attach Canvas>GameOverlay>Slider to GameManager>ProgressionSlider!");

			return;         // q 4
		}

		if( PlayerPrefs.HasKey( SAVE_KEY_TOTAL_COINS ) )
		{
			_gatheredTotalCoins = (int)PlayerPrefs.GetFloat(SAVE_KEY_TOTAL_COINS);
		}

		//if (PlayerPrefs.HasKey(SAVE_KEY_TOY_VALUE))
		//{
		//	_toyValue = PlayerPrefs.GetFloat(SAVE_KEY_TOTAL_COINS);
		//}



		if (!PlayerPrefs.HasKey(SAVE_KEY_CURRENT_LEVEL))
			PlayerPrefs.SetInt(SAVE_KEY_CURRENT_LEVEL, _currentLevelID);


		if (PlayerPrefs.HasKey(SAVE_KEY_CURRENT_LEVEL))
		{
			_currentLevelID = PlayerPrefs.GetInt(SAVE_KEY_CURRENT_LEVEL);
			if (PlayerPrefs.HasKey(SAVE_KEY_LVL_FINISHAMOUNT))
				_finishAmount = PlayerPrefs.GetFloat(SAVE_KEY_LVL_FINISHAMOUNT);
		}



		if( PlayerPrefs.HasKey ( SAVE_KEY_UPGRADES) )
		{
			if( PlayerPrefs.HasKey (SAVE_KEY_CV_UPGRADE) )
			{
				_upgrCVTier = PlayerPrefs.GetInt(SAVE_KEY_CV_UPGRADE);
				_coinValue += (_upgrCVIncrement * _upgrCVTier);
				_coinValueCost = PlayerPrefs.GetFloat(SAVE_KEY_CV_COST);
				UIManager._pInstance._actualCVTxt.text = (_coinValue * 100) + PERCENT_TEXT;
			}

			if(PlayerPrefs.HasKey(SAVE_KEY_CS_UPGRADE))
			{
				_upgrCSTier = PlayerPrefs.GetInt(SAVE_KEY_CS_UPGRADE);
				CoinSpawner._pInstance._defaultspawnTime = PlayerPrefs.GetFloat(SAVE_KEY_CS_UPGRADE_SPEED);
				_coinSpeed += (_upgrCSIncrement / 100) * _upgrCSTier;
				_coinSpeedCost = PlayerPrefs.GetFloat(SAVE_KEY_CS_COST);
				float convertedText = _coinSpeed;
				//_coinSpeed += (_upgrCSIncrement / 100);
				Debug.Log("Converted text " + convertedText);
			}

			if(PlayerPrefs.HasKey(SAVE_KEY_OE_UPGRADE))
			{
				Debug.Log("Has OE Key");
				_upgrOETier = PlayerPrefs.GetInt(SAVE_KEY_OE_UPGRADE);
				_offlineEarning += (_upgrOEIncrement / 100) * _upgrOETier;
				_offlineEarningCost = PlayerPrefs.GetFloat(SAVE_KEY_OE_COST);
				Debug.Log(_offlineEarning);


			}

			if(PlayerPrefs.HasKey(SAVE_KEY_OC_UPGRADE))
			{
				_upgrOCTier = PlayerPrefs.GetInt(SAVE_KEY_OC_UPGRADE);
				_offlineCapacity += (_upgrOCIncrement / 100) * _upgrOCTier;
				_offlineCapacityCost = PlayerPrefs.GetFloat(SAVE_KEY_OC_COST);
				Debug.Log(_offlineCapacity);
			}
		}
	}

    private void OnDestroy()
    {
        if(_pInstance == this)
        {
            _pInstance = null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
		RESET_FINISH_AMOUNT = _finishAmount;
		//RESET_CURRENT_LVL = _currentLevelID;
		RESET_VALUECOST_CV = _coinValueCost;
		RESET_VALUECOST_CS = _coinSpeedCost;
		RESET_VALUECOST_OE = _offlineEarningCost;
		RESET_VALUECOST_OC = _offlineCapacityCost;
		//DEBUG


		float cvCost = _coinValueCost;
		_progressionSlider.minValue = _lowestSliderValue;
        _progressionSlider.maxValue = _finishAmount;
        _progressionSlider.value = 0;
		_transparentSlider.minValue = _lowestSliderValue;
		_transparentSlider.maxValue = _finishAmount;
		_transparentSlider.value = 0;
		_transitionDelaying = false;
		// haskey continue?
		UIManager._pInstance._currentLevelText.text = LV_X_TEXT + _currentLevelID;
		_currentToyValue = _toyValue * _currentLevelID;
		UIManager._pInstance._actualCVTxt.text = (_coinValue * 100) + PERCENT_TEXT;
		UIManager._pInstance._actualCSTxt.text = X_TEXT + _coinSpeed;
		UIManager._pInstance._actualOETxt.text = X_TEXT + _offlineEarning;
		UIManager._pInstance._actualOCTxt.text = X_TEXT + _offlineCapacity;

		UIManager._pInstance._upgrCVBtnTxt.text = _coinValueCost + DOLLAR_SIGN_RIGHT;			//magic
		UIManager._pInstance._upgrCSBtnTxt.text = _coinSpeedCost + DOLLAR_SIGN_RIGHT;			//magic	
		UIManager._pInstance._upgrOEBtnTxt.text = _offlineEarningCost + DOLLAR_SIGN_RIGHT;	//magic	
		UIManager._pInstance._upgrOCBtnTxt.text = _offlineCapacityCost + DOLLAR_SIGN_RIGHT; //magic

		CheckPlayerPrefs();

		/// should this be in Awake instead?
		/// if( PlayerPrefs.HasKey (SAVE_KEY_CV_UPGRADE) )


	}

    // Update is called once per frame
    void Update()
    {

		if(!_allowClaim )
		{
			UIManager._pInstance.RedUpgradeButton(UIManager._pInstance._claimButton);
			_timer += Time.deltaTime;
			if (_timer > _claimTimer)
			{
				if (_currentPotCoins > 0)
				{
					_claimCoinAnim.SetActive(false);
					UIManager._pInstance.YellowClaimButton(UIManager._pInstance._claimButton);
					_allowClaim = true;
					_timer = 0;
				}
			}
		}

		if( _coinLerp )
		{
			_gatheredTotalCoins += (_totalCoinLerpValue - _gatheredTotalCoins) * _coinLerpSpeed * Time.deltaTime;

				//Mathf.Lerp(_gatheredTotalCoins, _totalCoinLerpValue, _fillSpeed * Time.deltaTime);
			if (_gatheredTotalCoins >= _totalCoinLerpValue - _approximationValue1)		
			{
				_coinLerp = false;
			}

			if( _gatheredTotalCoins >= _totalCoinLerpValue - _approximationValue2)
			{
				ButtonController._pInstance._totalCoinAnimation.SetBool("smoothCoin", false);
			}
		}


		if( _progressLerp )
		{
			_progressionSlider.value = Mathf.Lerp(_progressionSlider.value, _currentProgressValue, _fillSpeed * Time.deltaTime);
			if (_progressionSlider.value == _transparentSlider.value)
			{
				UIManager._pInstance._readyLevelUpParticle.Play();
				_progressLerp = false;
			}
		}


		//Debug.Log("Chain check " + _coinValue + " " + _coinSpeed + " " + _offlineEarning + " " + _offlineCapacity);

		//Debug.Log("Playerpref " + PlayerPrefs.GetInt(SAVE_KEY_CURRENT_LEVEL));
		//Debug.Log("Current ID " + _currentLevelID);
		//Debug.Log("tier " + _upgrOCTier + " OC value " + _offlineCapacity + " OC cost " + _offlineCapacityCost);
		//Debug.Log("nonkey: " + _gatheredTotalCoins);
		//Debug.Log("playerpref " + PlayerPrefs.GetInt(SAVE_KEY_TOTAL_COINS));

		///////////////////////////////////////////////////////////////////////////////////////////////////

		if (_transitionDelaying)
		{
			_transitionTimer += Time.deltaTime;
			if(_transitionTimer >= _transitionDelay)
			{
				_transitionTimer = 0;		// are these considered magic numbers?
				_levelTransitioning = true;
				_transitionDelaying = false;
			}
		}




		if(!_upgrCVMax)
			UIManager._pInstance.AdjustButtonColour(UIManager._pInstance._upgrCVButton, _gatheredTotalCoins, _coinValueCost);

		if(!_upgrCSMax)
			UIManager._pInstance.AdjustButtonColour(UIManager._pInstance._upgrCSButton, _gatheredTotalCoins, _coinSpeedCost);

		if(!_upgrOEMax)
			UIManager._pInstance.AdjustButtonColour(UIManager._pInstance._upgrOEButton, _gatheredTotalCoins, _offlineEarningCost);

		if(!_upgrOCMax)
			UIManager._pInstance.AdjustButtonColour(UIManager._pInstance._upgrOCButton, _gatheredTotalCoins, _offlineCapacityCost)
			;

		// Any better way write this code ?

	}
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//Debug menu
	public void DeletePlayerPrefs()
	{
		Debug.Log("deleted all");
		PlayerPrefs.DeleteAll();
		PlayerPrefs.SetInt(SAVE_KEY_CURRENT_LEVEL, _currentLevelID);
		PlayerPrefs.DeleteKey(SAVE_KEY_CURRENT_LEVEL);
		Debug.Log("playerpref lvl " + PlayerPrefs.GetInt(SAVE_KEY_CURRENT_LEVEL));
		//
		_gatheredTotalCoins = 0;
		_currentLevelID = RESET_CURRENT_LVL;
		UIManager._pInstance._currentLevelText.text = LV_X_TEXT + _currentLevelID;
		_finishAmount = RESET_FINISH_AMOUNT;
		CoinSpawner._pInstance.ResetCoinSpeed();
		_upgrCVMax = false;
		_upgrCSMax = false;
		_upgrOEMax = false;
		_upgrOCMax = false;

		Debug.Log("lvl id " + RESET_CURRENT_LVL);
		//CV
		_upgrCVTier = 0;
		_coinValue = RESET_VALUE_CV;
		_coinValueCost = RESET_VALUECOST_CV;
		UIManager._pInstance._actualCVTxt.text = (_coinValue * 100) + PERCENT_TEXT;
		UIManager._pInstance._upgrCVBtnTxt.text = (int)_coinValueCost + DOLLAR_SIGN_RIGHT;


		//CS
		_upgrCSTier = 0;
		_coinSpeed = RESET_VALUE_CS;
		_coinSpeedCost = RESET_VALUECOST_CS;
		UIManager._pInstance._actualCSTxt.text = X_TEXT + _coinSpeed;
		UIManager._pInstance._upgrCSBtnTxt.text = (int)_coinSpeedCost + DOLLAR_SIGN_RIGHT;

		//OE
		_upgrOETier = 0;
		_offlineEarning = RESET_VALUE_OE;
		_offlineEarningCost = RESET_VALUECOST_OE;
		UIManager._pInstance._actualOETxt.text = X_TEXT + _offlineEarning;
		UIManager._pInstance._upgrOEBtnTxt.text = (int)_offlineEarningCost + DOLLAR_SIGN_RIGHT;

		//OC
		_upgrOCTier = 0;
		_offlineCapacity = RESET_VALUE_OC;
		_offlineCapacityCost = RESET_VALUECOST_OE;
		UIManager._pInstance._actualOCTxt.text = X_TEXT + _offlineCapacity;
		UIManager._pInstance._upgrOCBtnTxt.text = (int)_offlineCapacityCost + DOLLAR_SIGN_RIGHT;

	}
	///////////////////////////////////////////////////////////////////////////////////////////////////
	public void CheckPlayerPrefs()
	{

		if (PlayerPrefs.HasKey(SAVE_KEY_CURRENT_PROGRESS))
		{
			_progressionSlider.value += PlayerPrefs.GetFloat(SAVE_KEY_CURRENT_PROGRESS);
			_transparentSlider.value = _progressionSlider.value;
		}


		if (PlayerPrefs.HasKey(SAVE_KEY_CV_UPGRADE))
			{
			if (PlayerPrefs.GetInt(SAVE_KEY_CV_UPGRADE) == MAX_TIER)   // upgrcvtier didnt work here for some reason
			{
				_upgrCVMax = true;
				Debug.Log("Upgr cv max");
				UIManager._pInstance._upgrCVBtnTxt.text = MAX_TEXT;
				UIManager._pInstance._upgrCVButton.colors = UIManager._pInstance._grayUpgradeColor;
				//UIManager._pInstance._upgrCVButton.interactable = false;
			}
		}

		if (PlayerPrefs.HasKey(SAVE_KEY_CS_UPGRADE))
		{

			UIManager._pInstance._actualCSTxt.text = X_TEXT + _coinSpeed;

			if (PlayerPrefs.GetInt(SAVE_KEY_CS_UPGRADE) == MAX_TIER)
			{
				_upgrCSMax = true;
				Debug.Log("Upgr cs max");
				UIManager._pInstance._upgrCSBtnTxt.text = MAX_TEXT;
				UIManager._pInstance._upgrCSButton.colors = UIManager._pInstance._grayUpgradeColor;
			}
		}

		if (PlayerPrefs.HasKey(SAVE_KEY_OE_UPGRADE))
		{
			UIManager._pInstance._actualOETxt.text = X_TEXT + _offlineEarning;

			if (PlayerPrefs.GetInt(SAVE_KEY_OE_UPGRADE) == MAX_TIER)
			{
				_upgrOEMax = true;
				Debug.Log("Upgr oe max");
				UIManager._pInstance._upgrOEBtnTxt.text = MAX_TEXT;
				UIManager._pInstance._upgrOEButton.colors = UIManager._pInstance._grayUpgradeColor;
			}
		}

		if (PlayerPrefs.HasKey(SAVE_KEY_OC_UPGRADE))
		{
			UIManager._pInstance._actualOCTxt.text = X_TEXT + _offlineCapacity;

			if (PlayerPrefs.GetInt(SAVE_KEY_OC_UPGRADE) == MAX_TIER)
			{
				_upgrOCMax = true;
				Debug.Log("Upgr oc max");
				UIManager._pInstance._upgrOCBtnTxt.text = MAX_TEXT;
				UIManager._pInstance._upgrOCButton.colors = UIManager._pInstance._grayUpgradeColor;
			}
		}

	}

	public void ToyCount()
	{
		_currentToyValue = _toyValue * _currentLevelID;
		_currentlevelCoins += _currentToyValue;      //to be changed to getset value from serialized in gamemanager
		_currentPotCoins += _currentToyValue;          //to be changed to getset value from serialized in gamemanager
		_transparentSlider.value += _currentToyValue;
	}

    public void CoinCount()
    {
		// 04 june  // _currentlevelCoins = GoalCoin._pInstance._goalCount * _coinValue;
		_currentlevelCoins += _coinValue;
		_currentPotCoins += _coinValue;  //30 May
										 //30 may UIManager._pInstance._allowClaimButton = true;


		_transparentSlider.value += _coinValue;
		
		UIManager._pInstance._potText.text = DOLLAR_SIGN_LEFT + (int)_currentPotCoins;
		//_gatheredTotalCoins += _coinValue;  // orig
		UIManager._pInstance._levelUpPanelText.text = DOLLAR_SIGN_RIGHT + " " + (int)_currentPotCoins;

	}

	public void DeltaCoinAmountToGatheredTotal( int amount )
	{
		_gatheredTotalCoins += amount;	
		PlayerPrefs.SetFloat( SAVE_KEY_TOTAL_COINS, _gatheredTotalCoins );
	}

	
	public void CoinCountFromTransitioning()
	{
		if (_multiply)
		{
			DeltaCoinAmountToGatheredTotal( ( int )( _coinValue * MULTIPLIER_TWO ) );

			_currentlevelCoins += _coinValue * MULTIPLIER_TWO;
			UIManager._pInstance._transitionText.text = "You have earned " + (int)(_currentlevelCoins + _currentPotCoins) +
" Coins from this machine! Lets check out another machine!";
		}
		else
		{
			DeltaCoinAmountToGatheredTotal( ( int )_coinValue );

			_currentlevelCoins += _coinValue;
			UIManager._pInstance._transitionText.text = "You have earned " + (int)(_currentlevelCoins + _currentPotCoins) +
" Coins from this machine! Lets check out another machine!";
			
		}

	}

	// boardCoins
	public void ResetAllCoins()
	{
		// ?? //
		BoardCoin[] coinObjects = _boardCoinsHolder.GetComponentsInChildren<BoardCoin>();

		for (int i = 0; i < coinObjects.Length; i++)
		{
			coinObjects[i].ResetBoardCoins();
		}
	}

	public void ResetToys()
	{
		{
			int counter = 0;
			Debug.Log("inside for ++ " + _toys);
			if (counter < _maxToys)
			{

				for (int i = 0; i < _toys.Count; i++)
				{
					if (counter < _maxToys)
					{
						int setObjActive = Random.Range(0, 100);
						if (setObjActive <= _toySpawnChance)
						{
							counter++;
							_toys[i].gameObject.SetActive(true);
							_toys[i].GetComponent<Toys>().ResetToys();
						}
					}
				}
				//if (_toys[i].gameObject.activeSelf)
				//{
				//	counter++;
				//}
				//else
				//{
				//	bool setObjActive = (Random.Range(0, 2) == 0);   // int for probability change + wtf
				//	if (setObjActive)
				//		counter++;


				//	_toys[i].gameObject.SetActive(setObjActive);
				//	_toys[i].GetComponent<Toys>().ResetToys();

				//}
			}
		}
	}

	public void SolidifyCoins()
	{
		BoardCoin[] coinObjects = _boardCoinsHolder.GetComponentsInChildren<BoardCoin>();

		for (int i = 0; i < coinObjects.Length; i++)
		{
			coinObjects[i].SolidifyBoardCoins();
		}
	}

	public void SolidifyToys()
	{
		Toys[] toyObjects = _toyHolder.GetComponentsInChildren<Toys>();

		for (int i = 0; i < toyObjects.Length; i++)
		{
			toyObjects[i].SolidifyToys();
		}
	}

	// function called by LevelUpButton in the scene, should also make LevelUpPanel visible
	public void ClaimButton()
	{
		if (_allowClaim)
		{
			_allowClaim = false;
			_totalCoinLerpValue = _gatheredTotalCoins + _currentPotCoins;
			_coinLerp = true;

			

			UIManager._pInstance._claimParticle.Play();
			float lerpValue = _progressionSlider.value + _currentPotCoins;
			ProgressLerp(lerpValue);
			//DeltaCoinAmountToGatheredTotal((int)_currentPotCoins);
			_currentPotCoins = 0;
			_claimCoinAnim.SetActive(true);
			// test 05 june _progressionSlider.value += _currentPotCoins;           //03june
			PlayerPrefs.SetFloat(SAVE_KEY_CURRENT_PROGRESS, _progressionSlider.value);


			UIManager._pInstance._potText.text = DOLLAR_SIGN_LEFT + (int)_currentPotCoins;

			PlayerPrefs.SetFloat(SAVE_KEY_TOTAL_COINS, _gatheredTotalCoins);

		}
	}


	public void ProgressLerp(float valueToLerp)
	{
		_progressLerp = true;
		_currentProgressValue = valueToLerp;
	}

	public void CoinFrenzy()
	{
		CoinSpawner._pInstance._saveSpawnTime = CoinSpawner._pInstance._defaultspawnTime;
		CoinSpawner._pInstance._defaultspawnTime = CoinSpawner._pInstance._saveSpawnTime / 2;
		CoinSpawner._pInstance._coinFrenzy = true;
		//UIManager._pInstance.BlueFrenzyButton(UIManager._pInstance._coinFrenzyButton);
	}

	public void LevelUpButton()         //30 May
	{
		UIManager._pInstance._levelingUp = true;
	}

	//function called by button ClaimPanel>2xButton in the scene
	public void MultiplyReward()
	{
		_currentPotCoins *= MULTIPLIER_TWO;
		_multiply = true;
		//30may _currentlevelCoins *= MULTIPLIER;
		//Debug.Log("Multiplied = " + _currentlevelCoins);
	}



	//function called by buttons in the ClaimPanel in the scene
	public void TransitionButton()
	{
		_currentProgressValue = 0;
		UIManager._pInstance._levelingUp = false;
		UIManager._pInstance._levelUpParticle.Play();
		//30may Time.timeScale = 1;
		PlayerPrefs.DeleteKey(SAVE_KEY_CURRENT_PROGRESS);
		_transitionDelaying = true;

		DeltaCoinAmountToGatheredTotal( ( int )_currentPotCoins );

		UIManager._pInstance._transitionText.text = "You have earned " + (_currentlevelCoins + _currentPotCoins) +
" Coins from this machine! Lets check out another machine!";
		UIManager._pInstance._transitionTextObj.SetActive(true);
		_currentPotCoins = 0;
		_progressionSlider.value = 0;
		_transparentSlider.value = 0;
		_finishAmount *= _finishAmountMultiplier;
		_progressionSlider.maxValue = _finishAmount;
		_transparentSlider.maxValue = _finishAmount;
		PlayerPrefs.SetFloat(SAVE_KEY_LVL_FINISHAMOUNT, _finishAmount);
		// finishamount LEVEL KEY
		// ddd _currentlevelCoins = 0;
		GoalCoin._pInstance._goalCount = 0;
		//_currentLevelID++;
		IncInt(SAVE_KEY_CURRENT_LEVEL);
		_currentLevelID = PlayerPrefs.GetInt(SAVE_KEY_CURRENT_LEVEL);
		UIManager._pInstance._currentLevelText.text = LV_X_TEXT + _currentLevelID;
		UIManager._pInstance._potText.text = DOLLAR_SIGN_LEFT + "0";		//are these considered magic numbers as well?
	}


	public static void IncInt(string key)
	{
		PlayerPrefs.SetInt(key, PlayerPrefs.GetInt(key) + 1);
	}

	// called from btn CV
	public void CoinValueUpgrade()
	{
		if (_upgrCVTier < MAX_TIER)
		{
			_coinValue += _upgrCVIncrement;
			UIManager._pInstance._actualCVTxt.text = (_coinValue * 100) + PERCENT_TEXT;

			DeltaCoinAmountToGatheredTotal( ( int )-_coinValueCost );

			_coinValueCost *= _upgradeCostMultiplier;
			UIManager._pInstance._upgrCVBtnTxt.text = (int)_coinValueCost + DOLLAR_SIGN_RIGHT;
			IncInt(SAVE_KEY_CV_UPGRADE);
			_upgrCVTier = PlayerPrefs.GetInt(SAVE_KEY_CV_UPGRADE);
			//_upgrCVTier++;
			PlayerPrefs.SetFloat(SAVE_KEY_CV_COST, _coinValueCost);


			if (_upgrCVTier == MAX_TIER)
			{
				IncInt(SAVE_KEY_CV_MAX);
				_upgrCVMax = true;
				Debug.Log("Upgr cv max");
				UIManager._pInstance._upgrCVBtnTxt.text = MAX_TEXT;
				UIManager._pInstance._upgrCVButton.colors = UIManager._pInstance._grayUpgradeColor;
				UIManager._pInstance._upgrCVButton.interactable = false;
			}
		}
		if (!PlayerPrefs.HasKey(SAVE_KEY_UPGRADES))
			IncInt(SAVE_KEY_UPGRADES);
	}

	// called from btn CS
	public void CoinSpeedUpgrade()
	{
		if(_upgrCSTier < MAX_TIER)        
		{
			if ( !CoinSpawner._pInstance._coinFrenzy)
			{
				float converted = CoinSpawner._pInstance._defaultspawnTime - (_upgrCSIncrement / 100);
				float convertedText = _coinSpeed;
				_coinSpeed += (_upgrCSIncrement / 100);

				// change, cant keep using realtime speed for converting into txt the other way
				Debug.Log("converted text: " + convertedText);

				CoinSpawner._pInstance._defaultspawnTime = converted;

				PlayerPrefs.SetFloat(SAVE_KEY_CS_UPGRADE_SPEED, converted);
				UIManager._pInstance._actualCSTxt.text = X_TEXT + (convertedText + (_upgrCSIncrement / 100));

				DeltaCoinAmountToGatheredTotal((int)-_coinSpeedCost);

				_coinSpeedCost *= _upgradeCostMultiplier;
				UIManager._pInstance._upgrCSBtnTxt.text = (int)_coinSpeedCost + DOLLAR_SIGN_RIGHT;
				IncInt(SAVE_KEY_CS_UPGRADE);
				_upgrCSTier = PlayerPrefs.GetInt(SAVE_KEY_CS_UPGRADE);
				PlayerPrefs.SetFloat(SAVE_KEY_CS_COST, _coinSpeedCost);


				if (_upgrCSTier == MAX_TIER)
				{
					_upgrCSMax = true;
					Debug.Log("Upgr cs max");
					UIManager._pInstance._upgrCSBtnTxt.text = MAX_TEXT;
					UIManager._pInstance._upgrCSButton.colors = UIManager._pInstance._grayUpgradeColor;
					UIManager._pInstance._upgrCSButton.interactable = false;
				}
			}
		}
		if (!PlayerPrefs.HasKey(SAVE_KEY_UPGRADES))
			IncInt(SAVE_KEY_UPGRADES);
	}

	// called from btn OE
	public void OfflineEarningUpgrade()
	{
		if(_upgrOETier < MAX_TIER)   
		{
			float converted = _offlineEarning;
			_offlineEarning += (_upgrOEIncrement / 100); // fix n'sta
			UIManager._pInstance._actualOETxt.text = X_TEXT + _offlineEarning;

			DeltaCoinAmountToGatheredTotal( ( int )-_offlineEarningCost );

			_offlineEarningCost *= _upgradeCostMultiplier;
			UIManager._pInstance._upgrOEBtnTxt.text = (int)_offlineEarningCost + DOLLAR_SIGN_RIGHT;
			IncInt(SAVE_KEY_OE_UPGRADE);
			_upgrOETier = PlayerPrefs.GetInt(SAVE_KEY_OE_UPGRADE);
			UIManager._pInstance._actualOETxt.text = X_TEXT + _offlineEarning;
			PlayerPrefs.SetFloat(SAVE_KEY_OE_COST, _offlineEarningCost);

			if (_upgrOETier == MAX_TIER)
			{
				_upgrOEMax = true;
				Debug.Log("Upgr oe max");
				UIManager._pInstance._upgrOEBtnTxt.text = MAX_TEXT;
				UIManager._pInstance._upgrOEButton.colors = UIManager._pInstance._grayUpgradeColor;
				UIManager._pInstance._upgrOEButton.interactable = false;
			}
		}
		if (!PlayerPrefs.HasKey(SAVE_KEY_UPGRADES))
			IncInt(SAVE_KEY_UPGRADES);
	}

	// called from btn OC
	public void OfflineCapacityUpgrade()
	{
		if (_upgrOCTier < MAX_TIER)
		{
			_offlineCapacity += (_upgrOCIncrement / 100); // fix n'sta
			UIManager._pInstance._actualOCTxt.text = X_TEXT + _offlineCapacity;

			DeltaCoinAmountToGatheredTotal( ( int )-_offlineCapacityCost );

			_offlineCapacityCost *= _upgradeCostMultiplier;
			UIManager._pInstance._upgrOCBtnTxt.text = (int)_offlineCapacityCost + DOLLAR_SIGN_RIGHT;
			IncInt(SAVE_KEY_OC_UPGRADE);
			_upgrOCTier = PlayerPrefs.GetInt(SAVE_KEY_OC_UPGRADE);
			UIManager._pInstance._actualOCTxt.text = X_TEXT + _offlineCapacity;
			PlayerPrefs.SetFloat(SAVE_KEY_OC_COST, _offlineCapacityCost);
			

			if (_upgrOCTier == MAX_TIER)
			{
				_upgrOCMax = true;
				Debug.Log("Upgr oc max");
				UIManager._pInstance._upgrOCBtnTxt.text = MAX_TEXT;
				UIManager._pInstance._upgrOCButton.colors = UIManager._pInstance._grayUpgradeColor;
				UIManager._pInstance._upgrOCButton.interactable = false;
			}
		}

		if (!PlayerPrefs.HasKey(SAVE_KEY_UPGRADES))
			IncInt(SAVE_KEY_UPGRADES);
	}

}
