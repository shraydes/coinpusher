﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// LevelManager.cs
//
// Created: 23/05/2019 rchan
// Contributors: chope
// 
// Intention: Manages the transitioning/objects between levels/machines.
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	private const float X_VALUE_FOR_DESPAWN = -15.0f;

	public static LevelManager _pInstance;
	public GameObject _toy;


	[Header("Design")]
	[SerializeField] int _levelSwipeSpeed = 8;

	[Header("Core")]
	//public GameObject _board1;
	//public GameObject _board2;
	//public GameObject _board3;
	public List<GameObject> _boards;
	public GameObject _pusher;
	public GameObject _fakePusher;
	public int _currentLevel = 0;

	private Vector3 _spawnPos = new Vector3(15, 0, 0);
	private Vector3 _levelPos = new Vector3(0, 0, 0);
	private Vector3 _endLevelPos = new Vector3(-15, 0, 0);
	private Vector3 _swipeLeft;
	private Vector3 _fakePusherDefaultPos;

	private bool _spawnedNewBoard = false;

	private int _pNextLevel
	{
		get
		{
			// safetey check the next level index. 
			if(_boards == null)
				return 0;

			if(_currentLevel == (_boards.Count - 1))
				return 0;

			return (_currentLevel + 1);
		}
	}

	private void Awake()
	{
		if(_pInstance == null)
		{
			_pInstance = this;
		}

		else if(_pInstance != this)
		{
			Destroy(gameObject);
		}
		DontDestroyOnLoad(gameObject);

		// do a validity check on boards set in the editor. 
		if(_boards != null)
		{
			for (int count = _boards.Count - 1; count >= 0; count--)
			{
				if(_boards[count] == null)
				{
					Debug.LogWarning("Level Manager Object has invalid boards defined, please fix setup in editor");
					_boards.RemoveAt(count);
				}
			}
		}
	}

	private void OnDestroy()
	{
		if(_pInstance == this)
		{
			_pInstance = null;
		}
	}

	void Start()
	{
		_fakePusherDefaultPos = _fakePusher.transform.position;
		_swipeLeft = new Vector3(-_levelSwipeSpeed * Time.deltaTime, 0, 0);
	}

	void Update()
	{

		Debug.Log("List count " + _boards.Count);
		if(GameManager._pInstance._levelTransitioning)
		{
			
			// with no valid boards or board to change to we are done. 
			if ((_boards == null) || (_boards.Count <= 1))
			{
				GameManager._pInstance._levelTransitioning = false;
				return;
			}

			if(!_spawnedNewBoard)
			{
				GameManager._pInstance.ResetAllCoins();
				GameManager._pInstance.ResetToys();		// random function depending on current level?

				GameManager._pInstance._toyHolder.transform.position = _spawnPos;
				GameManager._pInstance._boardCoinsHolder.transform.position = _spawnPos;
				_boards[_pNextLevel].SetActive(true);
				_boards[_pNextLevel].transform.position = _spawnPos;
				_fakePusher.transform.position = _fakePusherDefaultPos;
				_spawnedNewBoard = true;
			}

			// work out swipe each frame because Time.deltaTime can be different each frame, 
			// this keeps speed constant regardless of frame rate. 
			_swipeLeft = new Vector3(-_levelSwipeSpeed * Time.deltaTime, 0, 0);

			GameObject newBoard = _boards[_pNextLevel];
			newBoard.transform.Translate(_swipeLeft);
			_fakePusher.SetActive(true);
			_fakePusher.transform.Translate(_swipeLeft);

			GameObject oldBoard = _boards[_currentLevel];
			oldBoard.transform.Translate(_swipeLeft);
			_pusher.transform.Translate(_swipeLeft);



			GameManager._pInstance._boardCoinsHolder.transform.Translate(_swipeLeft);

			GameManager._pInstance._toyHolder.transform.Translate(_swipeLeft);

			if(_boards[_currentLevel].transform.position.x <= X_VALUE_FOR_DESPAWN)
			{
				GameManager._pInstance.SolidifyToys();
				GameManager._pInstance.SolidifyCoins();
				GameManager._pInstance._currentlevelCoins = 0;
				GameManager._pInstance._multiply = false;
				_boards[_currentLevel].SetActive(false);
				GameManager._pInstance._levelTransitioning = false;
				//spawn random prize//
				newBoard.transform.position = _levelPos;
				GameManager._pInstance._boardCoinsHolder.transform.position = _levelPos;
				GameManager._pInstance._toyHolder.transform.position = _levelPos;
				_spawnedNewBoard = false;
				UIManager._pInstance._transitionTextObj.SetActive(false);
				UIManager._pInstance._progressionSlider.SetActive(true);

				// return board to its original spawn pos for reuse. 
				_boards[_currentLevel].transform.position = _spawnPos;

				// increment level and reset if needed. 
				_currentLevel++;
				if(_currentLevel >= _boards.Count)
					_currentLevel = 0;

			}
		}
	}
}
