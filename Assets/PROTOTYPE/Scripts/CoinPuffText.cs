﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// CoinPuffText.cs
//
// Created: 29/05/2019 rchan
// Contributors: 
// 
// Intention: Managing the text that comes after the "puff effects" on the coins.
//
// Notes: Prefab attached to the puff prefab.
//Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPuffText : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnDisable()
	{

	}

	void OnEnable()
	{

	}


}
