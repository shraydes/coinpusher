﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// Pins.cs
//
// Created: 20/05/2019 rchan
// Contributors: 
// 
// Intention: Handles the pins.
//
// Notes: Any setup details, e.g. should be on object in Main Scene, OR singlton but still needs null checked.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pins: MonoBehaviour
{
	[Header("Design")]
	[SerializeField] float _noPinsDuration = 3;

	[Header("Core")]
	public GameObject _pins; 
    public bool _pinRetract;

    private float _pinTimer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_pinRetract)
        {
            Debug.Log("pinretract true");
            _pinTimer += Time.deltaTime;
            if(_pinTimer > _noPinsDuration)
            {
                _pinTimer = 0;
                _pins.SetActive(true);
                _pinRetract = false;
            }
        }
    }

    public void PinsRetract()
    {
        _pinTimer = 0;
        _pins.SetActive(false);
        _pinRetract = true;
    }
    
}
