//CC20130404 - Something similar to Unity Transform, but simpler!
using UnityEngine;

public struct Transform2d
{
	static Transform2d				_identity = new Transform2d( Vector2.zero, 0.0f );
	public static Transform2d		Identity	{ get { return _identity; } }
	
	private Vector2		_xaxis;
	private Vector2		_yaxis;
	public Vector2		_position;//public for fast access
	private float		_rotation;
	private bool		_isRightHanded;
	
	private Vector2		_transposedXaxis	{ get { return new Vector2( _xaxis.x, _yaxis.x ); } }
	private Vector2		_transposedYaxis	{ get { return new Vector2( _xaxis.y, _yaxis.y ); } }
	private bool		_setIsRightHanded	{ set { _isRightHanded = value; UpdateAxes(); } }
	
	public Vector2		Position			{ get { return _position; } set { _position = value; } }
	public float		Rotation			{ get { return _rotation; } set { _rotation = value; UpdateAxes(); } }
	public Vector2		Right				{ get { return _isRightHanded ? -_xaxis : _xaxis; } }
	public Vector2		Left				{ get { return _isRightHanded ? _xaxis : -_xaxis; } }
	public Vector2		Up					{ get { return _yaxis; } }
											
	public bool			IsRightHanded		{ get { return _isRightHanded; }	set { _setIsRightHanded = value; } }
	public bool			IsRotated			{ get { return _rotation != 0.0f; }	set { if ( value == false ) Rotation = 0.0f; } }
	
	public Vector2		this[ int i ]
	{
		get
		{
			switch ( i )
			{
			case 0: return _xaxis;
			case 1: return _yaxis;
			}
			return Vector2.zero;
		}
	}
	
	public float		this[ int i, int j ]	{ get { return this[i][j]; } }
	
	public Transform2d( Vector2 position, float rotation ) : this()
	{
		this.Position = position;
		this.Rotation = rotation;
	}

	public void SetIdentity()
	{
		Position = Vector2.zero;
		Rotation = 0.0f;
	}
	
	public Vector2		TransformPoint( Vector2 p )
	{
		return (_rotation == 0.0f && _isRightHanded == false) ? (_position + p) : (_position + p.x * _xaxis + p.y * _yaxis);
	}

	public Vector2		TransformDirection( Vector2 d )
	{
		return (_rotation == 0.0f && _isRightHanded == false) ? d : (d.x * _xaxis + d.y * _yaxis);
	}
	
	public Vector2		InverseTransformPoint( Vector2 p )
	{
		return (_rotation == 0.0f && _isRightHanded == false) ? (p - _position) : (( p.x - _position.x ) * _transposedXaxis + ( p.y - _position.y ) * _transposedYaxis);
	}

	public Vector2		InverseTransformDirection( Vector2 d )
	{
		return (_rotation == 0.0f && _isRightHanded == false) ? d : (d.x * _transposedXaxis + d.y * _transposedYaxis);
	}

	private void		UpdateAxes()
	{
		_xaxis.x = Mathf.Cos( _rotation );
		_xaxis.y = Mathf.Sin( _rotation );

		_yaxis.x = -_xaxis.y;
		_yaxis.y = _xaxis.x;
		
		if ( _isRightHanded )
		{
			_xaxis.x = -_xaxis.x;
			_yaxis.x = -_yaxis.x;
		}
	}
	
	public override string		ToString()
	{
		return string.Format( "[{0},{1}]", Position, Rotation );
	}
}


