using UnityEngine;
using System.Collections;

public class Easing
{
	public enum EaseType
	{
		Linear,
		EaseIn,
		EaseOut,
		EaseInOut,
		EaseInSine,
		EaseOutSine,
		EaseInOutSine,
		EaseInExpo,
		EaseOutExpo,
		EaseInOutExpo,
		EaseInCircle,
		EaseOutCircle,
		EaseInOutCircle,
		EaseInElastic,
		EaseOutElastic,
		EaseInOutElastic,
		EaseInBack,
		EaseOutBack,
		EaseInOutBack,
		EaseInBounce,
		EaseOutBounce,
		EaseInOutBounce
	}
		
	// time should go from 0f to duration ...
	
	public static float Ease(EaseType easing, float time, float duration, float start, float end)
	{
		float t = CalculateTime(easing, time, duration);
		float change = end - start;
		return start + change * t;
	}
	
	public static Vector2 Ease(EaseType easing, float time, float duration, Vector2 start, Vector2 end)
	{
		float t = CalculateTime(easing, time, duration);
		Vector2 change = end - start;
		return start + change * t;
	}
	
	public static Vector3 Ease(EaseType easing, float time, float duration, Vector3 start, Vector3 end)
	{
		float t = CalculateTime(easing, time, duration);
		Vector3 change = end - start;
		return start + change * t;
	}
	
	public static Vector4 Ease(EaseType easing, float time, float duration, Vector4 start, Vector4 end)
	{
		float t = CalculateTime(easing, time, duration);
		Vector4 change = end - start;
		return start + change * t;
	}
	
	public static Color Ease(EaseType easing, float time, float duration, Color start, Color end)
	{
		// decided that this one shouldn't overshoot ...
		return Color.Lerp(start, end, CalculateTime(easing, time, duration));
	}
	
	// this set of functions go to the max value halfway through the cycle, and then returns to the original value
	
	public static float Wave(EaseType easing, float time, float duration, float start, float max, int cycles = 1, FadeType fade = FadeType.None)
	{
		WaveTransformTime(ref time, ref duration, cycles, fade);
		return Ease(easing, time, duration, start, max);
	}
	
	public static Vector2 Wave(EaseType easing, float time, float duration, Vector2 start, Vector2 max, int cycles = 1, FadeType fade = FadeType.None)
	{
		WaveTransformTime(ref time, ref duration, cycles, fade);
		return Ease(easing, time, duration, start, max);
	}
	
	public static Vector3 Wave(EaseType easing, float time, float duration, Vector3 start, Vector3 max, int cycles = 1, FadeType fade = FadeType.None)
	{
		WaveTransformTime(ref time, ref duration, cycles, fade);
		return Ease(easing, time, duration, start, max);
	}
	
	public static Vector4 Wave(EaseType easing, float time, float duration, Vector4 start, Vector4 max, int cycles = 1, FadeType fade = FadeType.None)
	{
		WaveTransformTime(ref time, ref duration, cycles, fade);
		return Ease(easing, time, duration, start, max);
	}
	
	public static Color Wave(EaseType easing, float time, float duration, Color start, Color max, int cycles = 1, FadeType fade = FadeType.None)
	{
		WaveTransformTime(ref time, ref duration, cycles, fade);
		return Ease(easing, time, duration, start, max);
	}
	
	//
	
	public enum FadeType
	{
		None,
		In,
		Out,
		InOut
	}
	
	public static float FadeMultiplier(FadeType fade, float time, float duration)
	{
		switch(fade)
		{
		default:
		case FadeType.None: return 1f;
		case FadeType.In: return time / duration;
		case FadeType.Out: return 1f - (time / duration);
		case FadeType.InOut: 
			time *= 2f;
			if(time < duration) return time / duration;
			return 2f - (time / duration);
		}
	}
	
	static void WaveTransformTime(ref float time, ref float duration, int cycles, FadeType fade)
	{
		float mul = FadeMultiplier(fade, time, duration);
		duration /= cycles;
		float half = duration / 2f;
		while(time >= duration) time -= duration;
		if(time > half) time = duration - time;
		duration = half;
		time *= mul;
	}

	//
	
	private static float CalculateTime(EaseType easing, float time, float duration)
	{
		float p, s;
		
		switch(easing)
		{
		default:
		case EaseType.Linear:
			return time / duration;
			
		case EaseType.EaseIn:
			time /= duration;
			return time * time;
			
		case EaseType.EaseOut:
			time /= duration;
			return -1f * time * (time - 2f);
			
		case EaseType.EaseInOut:
			time /= (duration / 2f);
			if(time < 1f) return 0.5f * time * time;
			return -0.5f * ((time - 1f) * (time - 3f) - 1f);	
			
		case EaseType.EaseInSine:
			return 1f - Mathf.Cos((time / duration) * (Mathf.PI / 2f));
			
		case EaseType.EaseOutSine:
			return Mathf.Sin((time / duration) * (Mathf.PI / 2f));
			
		case EaseType.EaseInOutSine:
			return -0.5f * ( Mathf.Cos(Mathf.PI * (time / duration)) - 1f );
			
		case EaseType.EaseInExpo:
			if(Mathf.Approximately(0f, time)) return 0f;
			return Mathf.Pow(2f, 10f * ((time / duration) - 1f));
			
		case EaseType.EaseOutExpo:
			if(Mathf.Approximately(duration, time)) return 1f;
			return 1f - Mathf.Pow(2f, -10f * (time / duration));
			
		case EaseType.EaseInOutExpo:
			if(Mathf.Approximately(0f, time)) return 0f;
			if(Mathf.Approximately(duration, time)) return 1f;
			time /= (duration / 2f);
			if(time < 1f) return 0.5f * Mathf.Pow(2f, 10 * (time - 1f));
			return 0.5f * ( 2f - Mathf.Pow(2f, -10f * (time - 1f)) );
			
		case EaseType.EaseInCircle:
			time /= duration;
			return -(Mathf.Sqrt(1f - (time * time)) - 1f);
		
		case EaseType.EaseOutCircle:
			time /= duration;
			time -= 1f;
			return Mathf.Sqrt(1f - (time * time));
		
		case EaseType.EaseInOutCircle:
			time /= (duration / 2f);
			if(time < 1f) return -0.5f * (Mathf.Sqrt(1f - (time * time)) - 1f);
			time -= 2f;
			return 0.5f * (Mathf.Sqrt(1f - (time * time)) + 1f);
			
		case EaseType.EaseInElastic:
			if(Mathf.Approximately(time, 0f)) return 0f;  
			time /= duration;
			if(Mathf.Approximately(time, 1f)) return 1f;  
			p = duration * 0.3f;
			s = p / 4f;
			time -= 1f;
			return -( Mathf.Pow(2f,10f*time) * Mathf.Sin((time*duration-s)*(2f*Mathf.PI)/p) );
			
		case EaseType.EaseOutElastic:
			if(Mathf.Approximately(time, 0f)) return 0f;  
			time /= duration;
			if(Mathf.Approximately(time, 1f)) return 1f;  
			p = duration * 0.3f;
			s = p / 4f;
			return Mathf.Pow(2f,-10f*time) * Mathf.Sin( (time*duration-s)*(2f*Mathf.PI)/p ) + 1f;
			
		case EaseType.EaseInOutElastic:
			if(Mathf.Approximately(time, 0f)) return 0f;  
			time /= (duration / 2f);
			if(Mathf.Approximately(time, 2f)) return 1f;  
			p = duration * (0.3f * 1.5f);
			s = p / 4f;
			time -= 1f;
			if (time < 0f) return -0.5f*(Mathf.Pow(2f,10f*time) * Mathf.Sin( (time*duration-s)*(2f*Mathf.PI)/p ));
			return Mathf.Pow(2f,-10f*time) * Mathf.Sin( (time*duration-s)*(2f*Mathf.PI)/p )*0.5f + 1f;
		
		case EaseType.EaseInBack:
			s = 1.70158f;
			time /= duration;
			return time*time*((s+1f)*time - s);
		
		case EaseType.EaseOutBack:
			s = 1.70158f;
			time /= duration;
			time -= 1f;
			return (time*time*((s+1)*time + s) + 1f);
			
		case EaseType.EaseInOutBack:
			s = 1.70158f * 1.525f;
			time /= (duration / 2f);
			if (time < 1f) return 0.5f*(time*time*((s+1f)*time - s));
			time -= 2f;
			return 0.5f*(time*time*((s+1f)*time + s) + 2f);
			
		case EaseType.EaseInBounce:
			return EaseInBounce(time, duration);
		
		case EaseType.EaseOutBounce:
			return EaseOutBounce(time, duration);
		
		case EaseType.EaseInOutBounce:
			if (time < (duration / 2f)) return EaseInBounce (time*2f, duration) * 0.5f;
			return EaseOutBounce (time*2f-duration, duration) * 0.5f + 0.5f;
		}
	}
	
	private static float EaseInBounce(float time, float duration)
	{
		return 1f - EaseOutBounce(duration-time, duration);
	}
	
	private static float EaseOutBounce(float time, float duration)
	{
		time /= duration;
		if (time < (1f/2.75f)) {
			return (7.5625f*time*time);
		} else if (time < (2f/2.75f)) {
			time -= 1.5f/2.75f;
			return (7.5625f*time*time + 0.75f);
		} else if (time < (2.5f/2.75f)) {
			time -= 2.25f/2.75f;
			return (7.5625f*time*time + 0.9375f);
		} else {
			time -= 2.625f/2.75f;
			return (7.5625f*time*time + 0.984375f);
		}
	}
}
