////////////////////////////////////////////
// 
// Rotation3d.cs
//
// Created: 09/11/2018 ccuthbert
// Contributors:
// 
// Intention:
// Stores rotation as quaternion and 
// matrix, for quick access to both.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_Rotation3d
#endif

using UnityEngine;

namespace AmuzoEngine
{
	public interface IRotation3D
	{
		Quaternion	_pQuaternion	{ get; set; }

		Vector3	_pRight	{ get; set; }

		Vector3	_pUp	{ get; set; }

		Vector3	_pForward	{ get; set; }
	}

	public struct Rotation3d
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[Rotation3d] ";

		//
		// NESTED CLASSES / STRUCTS
		//

		//
		// MEMBERS 
		//
	
		private Vector3		_right;

		private Vector3		_up;

		private Vector3		_forward;

		private Quaternion	_quat;
	
		//
		// PROPERTIES
		//
		
		public Vector3 this[ int i ]
		{
			get
			{
				switch ( i )
				{
				case 0: return _right;
				case 1: return _up;
				case 2: return _forward;
				}
				return Vector3.zero;
			}
		}
	
		public float this[ int i, int j ]
		{
			get
			{
				return this[i][j];
			}
		}
	
		public Quaternion _pQuaternion
		{
			get
			{
				return _quat;
			}
			set
			{
				_quat = value;
				UpdateAxes();
			}
		}

		public Vector3 _pRight
		{
			get
			{
				return _right;
			}
			set
			{
				Vector3	forward = Vector3.Cross( value, _pUp );
				_pQuaternion = Quaternion.LookRotation( forward, Vector3.Cross( forward, value ) );
			}
		}

		public Vector3 _pUp
		{
			get
			{
				return _up;
			}
			set
			{
				_pQuaternion = Quaternion.LookRotation( Vector3.Cross( _pRight, value ), value );
			}
		}

		public Vector3 _pForward
		{
			get
			{
				return _forward;
			}
			set
			{
				_pQuaternion = Quaternion.LookRotation( value, _pUp );
			}
		}

		private Vector3 _pTransposedRight
		{
			get
			{
				return new Vector3( _right.x, _up.x, _forward.x );
			}
		}

		private Vector3 _pTransposedUp
		{
			get
			{
				return new Vector3( _right.y, _up.y, _forward.y );
			}
		}

		private Vector3 _pTransposedForward
		{
			get
			{
				return new Vector3( _right.z, _up.z, _forward.z );
			}
		}

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public Rotation3d( Quaternion rotation ) : this()
		{
			_pQuaternion = rotation;
		}

		public Rotation3d( Transform unityTransform ) : this()
		{
			_pQuaternion = unityTransform.rotation;
		}

		public void SetIdentity()
		{
			_pQuaternion = Quaternion.identity;
		}
	
		public Vector3 RotateVector( Vector3 v )
		{
			return v.x * _right + v.y * _up + v.z * _forward;
		}

		public Vector3 InverseRotateVector( Vector3 v )
		{
			return v.x * _pTransposedRight + v.y * _pTransposedUp + v.z * _pTransposedForward;
		}
	
		public override string ToString()
		{
			return string.Format( "[{0},{1},{2}]", _right, _up, _forward );
		}
		
		// ** INTERNAL
		//
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private void UpdateAxes()
		{
			_right.x = 1.0f - 2.0f * _quat.y * _quat.y - 2.0f * _quat.z * _quat.z;
			_right.y = 2.0f * _quat.x * _quat.y + 2.0f * _quat.w * _quat.z;
			_right.z = 2.0f * _quat.x * _quat.z - 2.0f * _quat.w * _quat.y;

			_up.x = 2.0f * _quat.x * _quat.y - 2.0f * _quat.w * _quat.z;
			_up.y = 1.0f - 2.0f * _quat.x * _quat.x - 2.0f * _quat.z * _quat.z;
			_up.z = 2.0f * _quat.y * _quat.z + 2.0f * _quat.w * _quat.x;

			_forward.x = 2.0f * _quat.x * _quat.z + 2.0f * _quat.w * _quat.y;
			_forward.y = 2.0f * _quat.y * _quat.z - 2.0f * _quat.w * _quat.x;
			_forward.z = 1.0f - 2.0f * _quat.x * _quat.x - 2.0f * _quat.y * _quat.y;
		}
	}

	public class UnityTransformRotationWrapper : IRotation3D
	{
		private Transform	_transform;

		public UnityTransformRotationWrapper( Transform t )
		{
			_transform = t;
		}

		Quaternion IRotation3D._pQuaternion
		{
			get
			{
				return _transform.rotation;
			}
			set
			{
				_transform.rotation = value;
			}
		}

		Vector3 IRotation3D._pRight
		{
			get
			{
				return _transform.right;
			}
			set
			{
				Vector3	forward = Vector3.Cross( value, _transform.up );
				_transform.rotation = Quaternion.LookRotation( forward, Vector3.Cross( forward, value ) );
			}
		}

		Vector3 IRotation3D._pUp
		{
			get
			{
				return _transform.up;
			}
			set
			{
				_transform.rotation = Quaternion.LookRotation( Vector3.Cross( _transform.right, value ), value );
			}
		}

		Vector3 IRotation3D._pForward
		{
			get
			{
				return _transform.forward;
			}
			set
			{
				_transform.rotation = Quaternion.LookRotation( value, _transform.up );
			}
		}
	}
}
