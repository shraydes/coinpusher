//CC20130404 - Something similar to Unity Transform, but simpler!
using UnityEngine;
using AmuzoEngine;

public struct SimpleTransform
{
	private Vector3		_position;

	private Rotation3d	_rotation;
	
	//Unity Transform-style access
	public Vector3		position	{ get { return _position; } set { _position = value; } }
	public Quaternion	rotation	{ get { return _rotation._pQuaternion; } set { _rotation._pQuaternion = value; } }
	public Vector3		right		{ get { return _rotation._pRight; } }
	public Vector3		up			{ get { return _rotation._pUp; } }
	public Vector3		forward		{ get { return _rotation._pForward; } }
	
	public Vector3		this[ int i ]
	{
		get
		{
			switch ( i )
			{
			case 0: return _rotation._pRight;
			case 1: return _rotation._pUp;
			case 2: return _rotation._pForward;
			}
			return Vector3.zero;
		}
	}
	
	public float		this[ int i, int j ]	{ get { return this[i][j]; } }

	public SimpleTransform( Vector3 position, Quaternion rotation ) : this()
	{
		this.position = position;
		this.rotation = rotation;
	}

	public SimpleTransform( Transform unityTransform ) : this()
	{
		position = unityTransform.position;
		rotation = unityTransform.rotation;
	}

	public void			SetIdentity()
	{
		position = Vector3.zero;
		rotation = Quaternion.identity;
	}
	
	public Vector3		TransformPoint( Vector3 p )
	{
		return _position + _rotation.RotateVector( p );
	}

	public Vector3		TransformDirection( Vector3 d )
	{
		return _rotation.RotateVector( d );
	}
	
	public Vector3		InverseTransformPoint( Vector3 p )
	{
		return _rotation.InverseRotateVector( p - _position );
	}

	public Vector3		InverseTransformDirection( Vector3 d )
	{
		return _rotation.InverseRotateVector( d );
	}
	
	public static implicit operator SimpleTransform( Transform src )
	{
		return new SimpleTransform( src );
	}
	
	public override string		ToString()
	{
		return string.Format( "[{0},{1},{2},{3}]", _rotation._pRight, _rotation._pUp, _rotation._pForward, _position );
	}
}

