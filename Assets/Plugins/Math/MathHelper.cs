using UnityEngine;
using System.Collections;

public static class MathHelper {

	static MathHelper()
	{
		float dt = 1 / 60f;
		float lambda = 0.5f;
		_halfToPowerGradient = (Mathf.Pow(0.5f, dt / lambda) - 1) / (dt / lambda);
	}


	public const float TAU = Mathf.PI * 2;
	
	private static float _halfToPowerGradient;

	private struct ExponentInfo
	{
		public readonly float	_exp;
		public readonly float	_rec;
		public ExponentInfo( float exp, float rec )
		{
			_exp = exp;
			_rec = rec;
		}
	}

	private static readonly ExponentInfo[] _exponents = new ExponentInfo[] {
		new ExponentInfo( 1f, 1f ),
		new ExponentInfo( 10f, 1f/10f ),
		new ExponentInfo( 100f, 1f/100f ),
		new ExponentInfo( 1000f, 1f/1000f ),
		new ExponentInfo( 10000f, 1f/10000f ),
		new ExponentInfo( 100000f, 1f/100000f ),
		new ExponentInfo( 1000000f, 1f/1000000f ),
		new ExponentInfo( 10000000f, 1f/10000000f ),
		new ExponentInfo( 100000000f, 1f/100000000f ),
		new ExponentInfo( 1000000000f, 1f/1000000000f )
	};

	public static Vector3 RoundVector3ToPrecision(Vector3 v, int decimalPlaces)
	{
		return
			Vector3.right *		RoundToPrecision(v.x, decimalPlaces) +
			Vector3.up *		RoundToPrecision(v.y, decimalPlaces) +
			Vector3.forward *	RoundToPrecision(v.z, decimalPlaces);
	}

	public static float RoundToPrecision(float n, int decimalPlaces)
	{
		ExponentInfo e = _exponents[decimalPlaces];
		return (int)(n * e._exp + Mathf.Sign(n) * 0.5f) * e._rec;
	}

	public static double RoundToPrecision(double n, int decimalPlaces)
	{
		return System.Math.Round( n, decimalPlaces );
	}

	public static bool ApproxEquals( float a, float b, float tolerance )
	{
		return Mathf.Abs( a - b ) <= tolerance;
	}
	
	public static bool ApproxEquals( Vector2 a, Vector2 b, float tolerance )
	{
		return ( a - b ).magnitude <= tolerance;
	}
	
	public static bool ApproxEquals( Vector3 a, Vector3 b, float tolerance )
	{
		return ( a - b ).magnitude <= tolerance;
	}
	
	public static float GetAngle(Vector3 v1, Vector3 v2, Vector3 normal){
		return Mathf.Atan2(
				Vector3.Dot(
					normal, 
					Vector3.Cross(v1, v2)
				),
				Vector3.Dot(v1, v2)
			) * Mathf.Rad2Deg;
	}

	public static float GetAxisAndAngle( Vector3 fromVecA, Vector3 toVecB, out Vector3 axis )
	{
		float	abInv = 1f / Mathf.Sqrt( Vector3.Dot( fromVecA, fromVecA ) * Vector3.Dot( toVecB, toVecB ) );
		float	cosAngle = abInv * Vector3.Dot( fromVecA, toVecB );
		Vector3	sinAngleAxis = abInv * Vector3.Cross( fromVecA, toVecB );
		float	sinAngle = sinAngleAxis.magnitude;

		if ( sinAngle == 0f )
		{
			axis = Vector3.right;
			return 0f;
		}

		axis = (1f / sinAngle) * sinAngleAxis;

		return Mathf.Atan2( sinAngle, cosAngle );
	}
	
	public static float GetArcDistance(	Vector3 originPosition, 
										Quaternion originRotation, 
										Vector3 destinationPosition){
		
		// Get the radius of the circle
		float radius = MathHelper.GetCircleRadius(
			originPosition, 
			originRotation, 
			destinationPosition
		);
		
		if (float.IsInfinity(radius) || float.IsNaN(radius)){
			return Mathf.Infinity;
		}
		
		// Find out the position of the center of the circle
		Vector3 center = MathHelper.GetCircleCenter(originPosition, originRotation, destinationPosition);
		
		// Calculate the arc angle
		float arcAngle = Vector3.Angle(
			originPosition - center,
			destinationPosition - center
		);
		
		// Calculate the arc distance
		return 2f * Mathf.PI * radius * arcAngle / 360f;
	}
	
	public static Vector3 GetCircleCenter(	Vector3 originPosition, 
											Quaternion originRotation, 
											Vector3 destinationPosition){
		
		// Get the radius of the circle
		float radius = MathHelper.GetCircleRadius(
			originPosition, 
			originRotation, 
			destinationPosition
		);
		
		// Check if the center is on the left or the right of the current position
		float sign = Mathf.Sign(
			MathHelper.GetAngle(
				originRotation * Vector3.forward, 
				destinationPosition - originPosition,
				originRotation * Vector3.up
			)
		);

		// Find out the position of the center of the circle
		Vector3 center = 
			originPosition + 
			originRotation * Vector3.left * sign * radius;
		
		return center;
	}
	
	public static float GetCircleRadius(Vector3 originPosition, 
										Quaternion originRotation, 
										Vector3 destinationPosition){
		
		// Calculate the angle between the current orientation and the
		// direction of the line (destinationPosition - originPosition)
		float angle = Mathf.Deg2Rad * Vector3.Angle(
			originRotation * Vector3.forward, 
			destinationPosition - originPosition
		);
		
		return Mathf.Abs(
			Vector3.Distance(originPosition, destinationPosition) *
			Mathf.Cos(angle) /
			Mathf.Sin(angle)
		);
	}
	
	public static float GetCircleRadius(float maxSpeed, float grip, float gravity){
		return Mathf.Abs((maxSpeed * maxSpeed) / (grip * gravity));
	}
	
	public static float GetMaxSpeed(float radius, float grip, float gravity){
		// If the arc has an infinity radius, cap the radius to a high value
		if (float.IsInfinity(radius) || float.IsNaN(radius)){
			radius = 10000f;
		}
		
		return Mathf.Sqrt(Mathf.Abs(radius * grip * gravity));
	}
	
	
	public static float GetMaxSpeed(Vector3		originPosition, 
									Quaternion	originRotation, 
									Vector3		destinationPosition,
									float		destinationSpeed,
									float		grip,
									float		deceleration,
									float		gravity){
		
		float radius = MathHelper.GetCircleRadius(originPosition, originRotation, destinationPosition);
		float distance = MathHelper.GetArcDistance(originPosition, originRotation, destinationPosition);
		
		return Mathf.Max(
			1f
			,
			Mathf.Min(
				MathHelper.GetMaxSpeed(radius, grip, gravity)
				,
				destinationSpeed + distance * deceleration
			)
		);
	}


	//CC20130404 - Given two points P1(x1, y1) and P2(x2, y2) and the x-value of 
	//a third point P3 on the line passing through P1 and P2, calculate the y-value of P3
	public static float	Lerp( float x1, float y1, float x2, float y2, float x )
	{
		return y1 + ( ( y2 - y1 ) / ( x2 - x1 ) ) * ( x - x1 );
	}
	
	//CC20130709 - Like above, but for Vector2.  More flexible than Vector2.Lerp
	public static Vector2	LerpVector2( float x1, Vector2 y1, float x2, Vector2 y2, float x )
	{
		return y1 + ( ( y2 - y1 ) / ( x2 - x1 ) ) * ( x - x1 );
	}
	
	//CC20130709 - Like above, but for Vector3.  More flexible than Vector3.Lerp
	public static Vector3	LerpVector3( float x1, Vector3 y1, float x2, Vector3 y2, float x )
	{
		return y1 + ( ( y2 - y1 ) / ( x2 - x1 ) ) * ( x - x1 );
	}
	
	//CC20130404 - Calculate the next position and velocity of a mass on a spring, given spring/damping coefficients, external force, and time step.
	//Position is extension from rest length.
	//Returns change in energy of system (should always be negative if damping and no external force are applied).
	public static float StepSpring( ref float pos, ref float vel, float spring, float damping, float extForce, float mass, float dt )
	{
		var energy0 = 0.5f * ( mass * vel * vel + spring * pos * pos );
		var	accel = ( extForce - spring * pos - damping * vel ) / mass;

		pos += vel * dt + 0.5f * accel * dt * dt;
		vel += accel * dt;
		
		return 0.5f * ( mass * vel * vel + spring * pos * pos ) - energy0;
	}
	
	//CC20140841 - 'halflife' decay, ie 'value' loses half it's size in 'lambda' time
	public static float LambdaDecay( float value, float lambda, float dt )
	{
		if ( lambda <= 0f ) return value;
		return value / Mathf.Pow( 2f, dt / lambda );
	}
	
	//CC20181219 - 'value' approaches 'target' by halflife decay of difference
	public static float LambdaApproach( float value, float target, float lambda, float dt )
	{
		return target + LambdaDecay( value - target, lambda, dt );
	}
	
	//CC20131011
	public static void CombineTransforms( Vector3 posA, Quaternion rotA, Vector3 posB, Quaternion rotB, out Vector3 posR, out Quaternion rotR )
	{
		posR = posA + rotA * posB;
		rotR = rotA * rotB;
	}
	
	//CC20131011
	public static void InvertTransform( Vector3 pos, Quaternion rot, out Vector3 invPos, out Quaternion invRot )
	{
		invRot = Quaternion.Inverse( rot );
		invPos = invRot * -pos;
	}
	
	//CC20130404 - Return vector produced by 'projecting' a vector v onto an axis.
	//The axis is assumed to be unit length.
	//The resultant vector has no component *not* in the direction of the axis.
	public static Vector3 ProjectVector3( Vector3 v, Vector3 axis )
	{
		return Vector3.Dot( v, axis ) * axis;
	}

	//CC20140731 - Return vector produced by 'projecting' a vector v onto an axis.
	//The axis is assumed to be unit length.
	//The resultant vector has no component *not* in the direction of the axis.
	public static Vector2 ProjectVector2( Vector2 v, Vector2 axis )
	{
		return Vector2.Dot( v, axis ) * axis;
	}
	
	//CC20130404 - Return vector produced by 'clipping' a vector v to an axis.
	//The axis is assumed to be unit length.
	//The resultant vector has no component in the direction of the axis.
	public static Vector3 ClipVector3( Vector3 v, Vector3 axis )
	{
		return v - ProjectVector3( v, axis );
	}
	
	//CC20140731 - Return vector produced by 'clipping' a vector v to an axis.
	//The axis is assumed to be unit length.
	//The resultant vector has no component in the direction of the axis.
	public static Vector2 ClipVector2( Vector2 v, Vector2 axis )
	{
		return v - ProjectVector2( v, axis );
	}
	
	//CC20130404 - Return vector produced by rotating a vector about an arbitrary axis.
	//The axis is assumed to be unit length.
	//Derived from quaternion rotate (p' = qpq*)
	public static Vector3 RotateVector3( Vector3 v, Vector3 axis, float angle )
	{
		var	cosAngle = Mathf.Cos( angle );
		var	sinAngle = Mathf.Sin( angle );
		
		return cosAngle * v + ( 1.0f - cosAngle ) * Vector3.Dot( v, axis ) * axis - sinAngle * Vector3.Cross( v, axis );
	}
	
	//CC20130702
	public static Vector2 RotateVector2( Vector2 v, float angle )
	{
		var	cosAngle = Mathf.Cos( angle );
		var	sinAngle = Mathf.Sin( angle );
		
		return new Vector2( cosAngle * v.x - sinAngle * v.y, sinAngle * v.x + cosAngle * v.y );
	}
	
	//CC20130702 - Assumes original extents are non-negative
	public static Vector2 RotateExtents2( Vector2 e, float angle )
	{
		var	cosAngle = Mathf.Abs( Mathf.Cos( angle ) );
		var	sinAngle = Mathf.Abs( Mathf.Sin( angle ) );
		
		return new Vector2( cosAngle * e.x + sinAngle * e.y, sinAngle * e.x + cosAngle * e.y );
	}
	
	//CC20130605 - Return original-axis-aligned extents after transform.  Always output extents >= input extents
	public static Vector3 TransformExtents( Transform tm, Vector3 extents )
	{
		Vector3		resultExtents;
		Matrix4x4	m = tm.localToWorldMatrix;
		
		resultExtents.x = Mathf.Abs( m.m00 * extents.x ) + Mathf.Abs( m.m01 * extents.y ) + Mathf.Abs( m.m02 * extents.z );
		resultExtents.y = Mathf.Abs( m.m10 * extents.x ) + Mathf.Abs( m.m11 * extents.y ) + Mathf.Abs( m.m12 * extents.z );
		resultExtents.z = Mathf.Abs( m.m20 * extents.x ) + Mathf.Abs( m.m21 * extents.y ) + Mathf.Abs( m.m22 * extents.z );
		
		return resultExtents;
	}

	public static Bounds TransformBounds( Transform tm, Bounds bounds )
	{
		Bounds	resultBounds = bounds;
		resultBounds.extents = TransformExtents( tm, bounds.extents );
		resultBounds.center = tm.TransformPoint( bounds.center );
		return resultBounds;
	}

	//CC20130605 - Return original-axis-aligned extents after transform.  Always output extents >= input extents
	public static Vector3 TransformExtents( Matrix4x4 m, Vector3 extents )
	{
		Vector3		resultExtents;
		
		resultExtents.x = Mathf.Abs( m.m00 * extents.x ) + Mathf.Abs( m.m01 * extents.y ) + Mathf.Abs( m.m02 * extents.z );
		resultExtents.y = Mathf.Abs( m.m10 * extents.x ) + Mathf.Abs( m.m11 * extents.y ) + Mathf.Abs( m.m12 * extents.z );
		resultExtents.z = Mathf.Abs( m.m20 * extents.x ) + Mathf.Abs( m.m21 * extents.y ) + Mathf.Abs( m.m22 * extents.z );
		
		return resultExtents;
	}

	public static Bounds TransformBounds( Matrix4x4 m, Bounds bounds )
	{
		Bounds	resultBounds = bounds;
		resultBounds.extents = TransformExtents( m, bounds.extents );
		resultBounds.center = m.MultiplyPoint3x4( bounds.center );
		return resultBounds;
	}

	//CC20130605 - Return original-axis-aligned extents after transform.  Always output extents >= input extents
	public static Vector3 TransformExtents( SimpleTransform tm, Vector3 extents )
	{
		Vector3		resultExtents;
		
		resultExtents.x = Mathf.Abs( extents.x * tm[0,0] ) + Mathf.Abs( extents.y * tm[1,0] ) + Mathf.Abs( extents.z * tm[2,0] );
		resultExtents.y = Mathf.Abs( extents.x * tm[0,1] ) + Mathf.Abs( extents.y * tm[1,1] ) + Mathf.Abs( extents.z * tm[2,1] );
		resultExtents.z = Mathf.Abs( extents.x * tm[0,2] ) + Mathf.Abs( extents.y * tm[1,2] ) + Mathf.Abs( extents.z * tm[2,2] );
		
		return resultExtents;
	}

	public static Bounds TransformBounds( SimpleTransform tm, Bounds bounds )
	{
		Bounds	resultBounds = bounds;
		resultBounds.extents = TransformExtents( tm, bounds.extents );
		resultBounds.center = tm.TransformPoint( bounds.center );
		return resultBounds;
	}
	
	//CC20130612 - Return original-axis-aligned extents after transform.  Always output extents >= input extents
	public static Vector3 InverseTransformExtents( Transform tm, Vector3 extents )
	{
		Vector3		resultExtents;
		Matrix4x4	m = tm.localToWorldMatrix;
		
		resultExtents.x = Mathf.Abs( m.m00 * extents.x ) + Mathf.Abs( m.m10 * extents.y ) + Mathf.Abs( m.m20 * extents.z );
		resultExtents.y = Mathf.Abs( m.m01 * extents.x ) + Mathf.Abs( m.m11 * extents.y ) + Mathf.Abs( m.m21 * extents.z );
		resultExtents.z = Mathf.Abs( m.m02 * extents.x ) + Mathf.Abs( m.m12 * extents.y ) + Mathf.Abs( m.m22 * extents.z );
		
		return resultExtents;
	}

	public static Bounds InverseTransformBounds( Transform tm, Bounds bounds )
	{
		Bounds	resultBounds = bounds;
		resultBounds.center = tm.InverseTransformPoint( bounds.center );
		resultBounds.extents = InverseTransformExtents( tm, bounds.extents );
		return resultBounds;
	}

	//CC2013005 - Return original-axis-aligned extents after transform.  Always output extents >= input extents
	public static Vector3 InverseTransformExtents( SimpleTransform tm, Vector3 extents )
	{
		Vector3		resultExtents;
		
		resultExtents.x = Mathf.Abs( extents.x * tm[0,0] ) + Mathf.Abs( extents.y * tm[0,1] ) + Mathf.Abs( extents.z * tm[0,2] );
		resultExtents.y = Mathf.Abs( extents.x * tm[1,0] ) + Mathf.Abs( extents.y * tm[1,1] ) + Mathf.Abs( extents.z * tm[1,2] );
		resultExtents.z = Mathf.Abs( extents.x * tm[2,0] ) + Mathf.Abs( extents.y * tm[2,1] ) + Mathf.Abs( extents.z * tm[2,2] );
		
		return resultExtents;
	}

	public static Bounds InverseTransformBounds( SimpleTransform tm, Bounds bounds )
	{
		Bounds	resultBounds = bounds;
		resultBounds.center = tm.InverseTransformPoint( bounds.center );
		resultBounds.extents = InverseTransformExtents( tm, bounds.extents );
		return resultBounds;
	}
	
	public static float	ExtentInDirection( Vector2 extents, Vector2 direction )
	{
		return Mathf.Abs( extents.x * direction.x ) + Mathf.Abs( extents.y * direction.y );
	}

	public static float	ExtentInDirection( Vector3 extents, Vector3 direction )
	{
		return Mathf.Abs( extents.x * direction.x ) + Mathf.Abs( extents.y * direction.y ) + Mathf.Abs( extents.z * direction.z );
	}

	//CC20130516
	public static Plane TransformPlane( Transform tm, Plane plane )
	{
		var	resultPlane = plane;
		
		if ( tm != null )
		{
			var	point = plane.normal * plane.distance;
			
			resultPlane.SetNormalAndPosition( tm.TransformDirection( plane.normal ), tm.TransformPoint( point ) );
		}
		
		return resultPlane;
	}
	
	//CC20130516
	public static Plane InverseTransformPlane( Transform tm, Plane plane )
	{
		var	resultPlane = plane;
		
		if ( tm != null )
		{
			var	point = plane.normal * plane.distance;
			
			resultPlane.SetNormalAndPosition( tm.InverseTransformDirection( plane.normal ), tm.InverseTransformDirection( point ) );
		}
		
		return resultPlane;
	}
	
	//CC20140731
	public static float Vector2Cross( Vector2 a, Vector2 b )
	{
		return a.x * b.y - a.y * b.x;
	}
	
	//CC20140731
	public static Vector2 Vector2Perp( Vector2 a )
	{
		return new Vector2( -a.y, a.x );
	}
	
	//CC20130709
	public static Vector3 GeneratePerpVector3( Vector3 a )
	{
		var	result = Vector3.zero;
		var	mx = Mathf.Abs(a.x);
		var	my = Mathf.Abs(a.y);
		var	mz = Mathf.Abs(a.z);
		
		if ( mx > my )
		{
			if ( mx > mz )
			{
				result = Vector3.Cross( Vector3.forward, a );
			}
			else
			{
				result = Vector3.Cross( Vector3.up, a );
			}
		}
		else
		{
			if ( my > mz )
			{
				result = Vector3.Cross( Vector3.right, a );
			}
			else
			{
				result = Vector3.Cross( Vector3.up, a );
			}
		}
		
		return result.normalized;
	}
	
	public static bool SolveQuadratic( float a, float b, float c, out float r1, out float r2 )
	{
		if ( a != 0.0f )
		{
			var	disc = (b * b) - (4.0f * a * c);
			if ( disc >= 0.0f )
			{
				r1 = (-b - Mathf.Sqrt( disc )) / (2.0f * a);
				r2 = (-b + Mathf.Sqrt( disc )) / (2.0f * a);
				return true;
			}
		}
		r1 = r2 = 0.0f;
		return false;
	}
	
	//CC20130411 - Solves a set of linear simultaneous equations.
	//Equations are specified as matrix of coefficients.
	//Matrix should have N rows and (N+1) columns.
	//Matrix is concatenation of N-by-N square matrix A and N-vector b.
	//If N-vector x is vector of unknowns, then the system of equations that the matrix represents are:
	//(A[0,0] * x[0]) + (A[0,1] * x[1]) + ... + (A[0,N-1] * x[N-1]) = b[0]
	//(A[1,0] * x[0]) + (A[1,1] * x[1]) + ... + (A[1,N-1] * x[N-1]) = b[1]
	// .
	// .
	// .
	//(A[N-1,0] * x[0]) + (A[N-1,1] * x[1]) + ... + (A[N-1,N-1] * x[N-1]) = b[N-1]
	//Uses gaussian elimination to reduce (a copy of) the matrix to reduced row-echelon form.
	public class SimEqSolver
	{
		float[,]	_lm;//local copy of matrix
		
		int			_nr;//number of rows
		int			_nc;//number of columns
		
		int[]		_rm;//row index map (so don't have to physically swap rows)
		int[]		_cm;//column index map (so don't have to physically swap columns)
		int[]		_cmi;//inverse column index map
		
		int			_pr;//pivot row index
		int			_pc;//pivot columns index
		float		_pv;//pivot value
		
		float[]		_sv;//solution vector
	
	
		void Initialise( float[,] m )
		{
			_nr = m.GetLength(0);
			_nc = m.GetLength(1);
			
			//check _nc == _nr + 1
			
			_lm = new float[_nr, _nc];
			
			for ( int r = 0; r < _nr; ++r )
			{
				for ( int c = 0; c < _nc; ++c )
				{
					_lm[r,c] = m[r,c];
				}
			}
			
			_rm = new int[_nr];
			_cm = new int[_nc];
			_cmi = new int[_nc];
			
			for ( int r = 0; r < _nr; ++r )
			{
				_rm[r] = r;
			}
			
			for ( int c = 0; c < _nc; ++c )
			{
				_cm[c] = c;
			}
			
			_pr = -1;
			_pc = -1;
			_pv = 0.0f;
			
			_sv = new float[_nr];
		}
		
		void FindPivot( int sr )//sr = start row index
		{
			_pr = -1;
			_pc = -1;
			_pv = 0.0f;
			
			float	v;
			
			for ( int r = sr; r < _nr; ++r )
			{
				for ( int c = sr; c < _nr; ++c )
				{
					v = _lm[ _rm[r], _cm[c] ];
					
					if ( Mathf.Abs( v ) > Mathf.Abs( _pv ) )
					{
						_pr = r;
						_pc = c;
						_pv = v;
					}
				}
			}
		}
	
		public float[] Solve( float[,] m )
		{
			Initialise( m );
			
			int	r, c, tr, tc, rr;
			float	ppv;
			
			//go down the rows...
			for ( r = 0; r < _nr; ++r )
			{
				FindPivot( r );
				
				if ( _pv == 0.0f )
				{
					return null;
				}
				
				//check valid pivot row/column?
				
				//do row swapping
				if ( _pr != r )
				{
					tr = _rm[r];
					_rm[r] = _rm[_pr];
					_rm[_pr] = tr;
				}
				
				//do column swapping
				if ( _pc != r )
				{
					tc = _cm[r];
					_cm[r] = _cm[_pc];
					_cm[_pc] = tc;
				}
				
				//divide current row by pivot value
				if ( _pv != 0.0f )
				{
					_lm[ _rm[r], _cm[r] ] = 1.0f;
					for ( c = r + 1; c < _nc; ++c )
					{
						_lm[ _rm[r], _cm[c] ] = _lm[ _rm[r], _cm[c] ] / _pv;
					}
				}
				
				//for subsequent rows, subract multiple of current row, to get a zero in pivot column
				for ( rr = r + 1; rr < _nr; ++rr )
				{
					ppv = _lm[ _rm[rr], _cm[r] ];
					if ( ppv != 0.0f )
					{
						_lm[ _rm[rr], _cm[r] ] = 0.0f;
						for ( c = r + 1; c < _nc; ++c )
						{
							_lm[ _rm[rr], _cm[c] ] = _lm[ _rm[rr], _cm[c] ] - ppv * _lm[ _rm[r], _cm[c] ];
						}
					}
				}
			}
			
			//go back up the rows...
			for ( r = _nr - 1; r >= 0; --r )
			{
				//for all rows up to current row, subtract multiple of current row to get zero in column r
				for ( rr = 0; rr < r; ++rr )
				{
					ppv = _lm[ _rm[rr], _cm[r] ];
					if ( ppv != 0.0f )
					{
						_lm[ _rm[rr], _cm[r] ] = 0.0f;
						_lm[ _rm[rr], _cm[_nc - 1] ] = _lm[ _rm[rr], _cm[_nc - 1] ] - ppv * _lm[ _rm[r], _cm[_nc - 1] ];
					}
				}
			}
			
			//build inverse column index map
			for ( c = 0; c < _nc; ++c )
			{
				_cmi[ _cm[c] ] = c;
			}
			
			//read out solution from last column
			for ( r = 0; r < _nr; ++r )
			{
				_sv[r] = _lm[ _rm[ _cmi[r] ], _cm[_nc - 1] ];
			}
			
			return _sv;
		}
	}



	public static float Wrap(float val, float closedMin, float openMax)
	{
		float range = openMax - closedMin;
		float ret = ((val - closedMin) % range) + closedMin;
		if (ret < closedMin) {
			ret += range;
		}
		return ret;
	}

	public static int Wrap(int val, int closedMin, int openMax)
	{
		int range = openMax - closedMin;
		int ret = ((val - closedMin) % range) + closedMin;
		if (ret < closedMin) {
			ret += range;
		}
		return ret;
	}



	public static float Wrap01(float val)
	{
		return Wrap(val, 0, 1);
	}



	public static bool IsInOpenRange(float val, float min, float max)
	{
		return val > min && val < max;
	}



	public static bool IsInClosedRange(float val, float min, float max)
	{
		return val >= min && val <= max;
	}



	public static bool IsBetween01(float val)
	{
		return IsInOpenRange(val, 0, 1);
	}



	public static Vector3 WrapAngles(Vector3 angles)
	{
		return new Vector3(
			Wrap(angles.x, -180, 180),
			Wrap(angles.y, -180, 180),
			Wrap(angles.z, -180, 180)
		);
	}



	public static float Square(float n)
	{
		return n * n;
	}



	public static void EaseTowards(ref float value, float to, float lambda)
	{
		value = EaseTowards(value, to, lambda, Time.deltaTime);
	}



	public static void EaseTowards(ref float value, float to, float lambda, float dt)
	{
		value = EaseTowards(value, to, lambda, dt);
	}

	

	public static float EaseTowards(float value, float to, float lambda)
	{
		return EaseTowards(value, to, lambda, Time.deltaTime);
	}

	

	public static float EaseTowards(float value, float to, float lambda, float dt)
	{
		return value + (to - value) * (1 - HalfToPowerApprox(dt / lambda));
	}



	private static float HalfToPowerApprox(float p)
	{
		return 1 + p * _halfToPowerGradient;
	}



	public static Vector3 TripleProduct(Vector3 a, Vector3 b, Vector3 c)
	{
		return b * Vector3.Dot(a, c) - c * Vector3.Dot(a, b);
	}



	public static Vector3 GetUphillVector(Vector3 groundNormal)
	{
		return (Vector3.up - groundNormal * groundNormal.y).normalized;
	}



	public static Quaternion GetRelativeQuaternion(Quaternion from, Quaternion to)
	{
		return Quaternion.Inverse(from) * to;
	}



	public static bool SameSideOfZero(float a, float b)
	{
		return
			(a < 0 && b < 0) ||
			(a > 0 && b > 0);
	}



	public static bool OppositeSidesOfZero(float a, float b)
	{
		return
			(a < 0 && b > 0) ||
			(a > 0 && b < 0);
	}



	public static Vector3 RandomOnUnitCircle()
	{
		float a = Random.value * 360;

		return new Vector3(Mathf.Sin(a), Mathf.Cos(a));
	}




	public static void CheckNan( this float num, string message = "" )
	{
		if ( float.IsNaN( num ) || float.IsInfinity( num ) )
		{
			Debug.LogError( "NaN: " + message );
		}
	}

	public static void CheckNan( this Vector2 vec, string message = "" )
	{
		vec.x.CheckNan( "vec.x: " + message );
		vec.y.CheckNan( "vec.y: " + message );
	}

	public static void CheckNan( this Vector3 vec, string message = "" )
	{
		vec.x.CheckNan( "vec.x: " + message );
		vec.y.CheckNan( "vec.y: " + message );
		vec.z.CheckNan( "vec.z: " + message );
	}
}
