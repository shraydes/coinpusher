#include "pch.h"
#include <string>

using namespace Windows::System::Profile;
using namespace Windows::UI::ViewManagement;

// extern "C" __declspec(dllexport) void __stdcall AmuzoUwpUtils_GetDeviceFamily( wchar_t* strBuf, int strBufSize )
// {
	// Platform::String^	deviceFamily = AnalyticsInfo::VersionInfo->DeviceFamily;
	// wcsncpy_s( strBuf, strBufSize, deviceFamily->Data, deviceFamily->Length() );
// }

extern "C" __declspec(dllexport) bool __stdcall AmuzoUwpUtils_IsDeviceMobile()
{
	UserInteractionMode interactionMode = UIViewSettings::GetForCurrentView()->UserInteractionMode;
	return interactionMode == UserInteractionMode::Touch;
}

extern "C" __declspec(dllexport) bool __stdcall AmuzoUwpUtils_IsDeviceDesktop()
{
	UserInteractionMode interactionMode = UIViewSettings::GetForCurrentView()->UserInteractionMode;
	return interactionMode == UserInteractionMode::Mouse;
}
