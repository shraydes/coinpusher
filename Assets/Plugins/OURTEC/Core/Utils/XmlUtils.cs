﻿////////////////////////////////////////////
// 
// XmlUtils.cs
//
// Created: 27/02/2017 ccuthbert
// Contributors:
// 
// Intention:
// Xml utility functions refactored from Anvil.
// Depends on System.Xml
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System;
using System.Xml;

namespace AmuzoEngine
{
	public static class XmlUtils
	{
		//------------------------------------------------------------------------------------------------------------------
		public static void SetXmlChild( XmlElement parentElem, string childName, Action<XmlElement> setAction )
		{
			SetXmlChild( parentElem, e => e.Name == childName, () => parentElem.OwnerDocument.CreateElement( childName ), setAction );
		}
	
		//------------------------------------------------------------------------------------------------------------------
		public static void SetXmlChild( XmlElement parentElem, Predicate<XmlElement> findPred, Func<XmlElement> createFunc, Action<XmlElement> setAction )
		{
			XmlElement	childElem = FindXmlChild( parentElem, findPred );

			if ( childElem != null )
			{
				if ( setAction != null )
				{
					setAction( childElem );
				}
			}
			else
			{
				AddXmlChild( parentElem, createFunc, setAction );
			}
		}

		//------------------------------------------------------------------------------------------------------------------
		public static void RemoveXmlChild( XmlElement parentElem, string childName )
		{
			RemoveXmlChild( parentElem, e => e.Name == childName );
		}
	
		//------------------------------------------------------------------------------------------------------------------
		public static void RemoveXmlChild( XmlElement parentElem, Predicate<XmlElement> findPred )
		{
			XmlElement	childElem = FindXmlChild( parentElem, findPred );

			if ( childElem != null )
			{
				parentElem.RemoveChild( childElem );
			}
		}
	
		//------------------------------------------------------------------------------------------------------------------
		public static XmlElement FindXmlChild( XmlElement parentElem, Predicate<XmlElement> match )
		{
			XmlNodeList	childNodes = parentElem.SelectNodes( "child::node()" );
		
			XmlElement	foundElem = null;
		
			if ( childNodes != null )
			{
				int	nodeCount = childNodes.Count;
				XmlNode	childNode;
			
				for ( int i = 0; i < nodeCount; ++i )
				{
					childNode = childNodes[i];
				
					if ( childNode == null )
					{
						continue;
					}
				
					XmlElement	childElem = childNode as XmlElement;
				
					if ( childElem == null )
					{
						continue;
					}
				
					if ( match != null && match( childElem ) == false ) continue;
				
					foundElem = childElem;
				
					break;
				}
			}

			return foundElem;
		}
	
		//------------------------------------------------------------------------------------------------------------------
		public static XmlElement FindXmlElement( XmlElement parentElem, string elementPath )
		{
			if ( string.IsNullOrEmpty( elementPath ) ) return null;

			string[]	pathNodes = elementPath.Split( '/', '\\' );

			XmlElement	currElem = parentElem;

			foreach ( string node in pathNodes )
			{
				currElem = FindXmlChild( currElem, match:e => e.Name.Equals( node, StringComparison.OrdinalIgnoreCase ) );

				if ( currElem == null ) break;
			}

			return currElem;
		}
	
		//------------------------------------------------------------------------------------------------------------------
		public static void AddXmlChild( XmlElement parentElem, string childName, Action<XmlElement> setAction )
		{
			AddXmlChild( parentElem, () => parentElem.OwnerDocument.CreateElement( childName, parentElem.NamespaceURI ), setAction );
		}

		//------------------------------------------------------------------------------------------------------------------
		public static void AddXmlChild( XmlElement parentElem, Func<XmlElement> createFunc, Action<XmlElement> setAction )
		{
			XmlElement	childElem = createFunc();
		
			if ( childElem == null ) throw new Exception( "Failed to create child element" );
		
			parentElem.AppendChild( childElem );
		
			if ( setAction != null )
			{
				setAction( childElem );
			}
		}
	}
}
