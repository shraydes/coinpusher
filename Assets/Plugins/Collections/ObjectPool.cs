//CC20130628
using System.Collections.Generic;

namespace AmuzoEngine
{
public class ObjectPool<T> where T:class
{
	int				_size		{ get { return _numFree + _numActive; } }
	public int		Size		{ get { return _size; }	set { SetSize( value ); } }
	
	LinkedList<T>	_freeList;
	LinkedList<T>	_activeList;
	public LinkedList<T>	ActiveList	{ get { return _activeList; } }
	
	int				_numFree	{ get { return _freeList.Count; } }
	int				_numActive	{ get { return _activeList.Count; } }

	public int	FreeCount	{ get { return _numFree; } }
	public int	ActiveCount	{ get { return _numActive; } }
	
	public delegate T		DInternalAlloc();
	public delegate void	DInternalFree( T obj );
	
	private DInternalAlloc	_internalAlloc;
	private DInternalFree	_internalFree;
	
	public ObjectPool( int size, DInternalAlloc internalAllocFn, DInternalFree internalFreeFn )
	{
		_freeList = new LinkedList<T>();
		_activeList = new LinkedList<T>();
		_internalAlloc = internalAllocFn;
		_internalFree = internalFreeFn;
		EnsureInternalAllocFree();
		
		SetSize( size );
	}
	
	public T Allocate()
	{
		LinkedListNode<T>	objNode = _freeList.First;
		
		if ( objNode != null )
		{
			_freeList.Remove( objNode );
			_activeList.AddLast( objNode );
			return objNode.Value;
		}
		
		return null;
	}
	
	public void Free( T obj )
	{
		if ( obj != null )
		{
			LinkedListNode<T>	objNode	= GetActiveListNode( obj );
			
			if ( objNode != null )
			{
				FreeNode( objNode );
			}
		}
	}
	
	public void FreeOldest( int count = 1 )
	{
		while ( _activeList.First != null && count > 0 )
		{
			FreeNode( _activeList.First );
			--count;
		}
	}
	
	public void FreeSome( System.Predicate<T> wantFreePred )
	{
		if ( wantFreePred == null ) return;

		LinkedListNode<T>	objNode = _activeList.First;
		LinkedListNode<T>	nextNode;

		for ( ; objNode != null; objNode = nextNode )
		{
			nextNode = objNode.Next;
			if ( wantFreePred( objNode.Value ) )
			{
				FreeNode( objNode );
			}
		}
	}
	
	public void FreeAll()
	{
		while ( _activeList.First != null )
		{
			FreeNode( _activeList.First );
		}
	}
	
	protected virtual LinkedListNode<T> InitListNode( T obj )
	{
		return new LinkedListNode<T>( obj );
	}
	
	protected virtual LinkedListNode<T> GetActiveListNode( T obj )
	{
		return _activeList.Find( obj );
	}
	
	private void FreeNode( LinkedListNode<T> objNode )
	{
		_activeList.Remove( objNode );
		_freeList.AddLast( objNode );
	}
	
	private void SetSize( int newSize )
	{
		var	currSize = _size;
		
		if ( newSize > currSize )
		{
			InternalAllocateObjects( newSize - currSize );
		}
		else
		if ( newSize < currSize )
		{
			InternalFreeObjects( currSize - newSize );
		}
	}
	
	private void InternalAllocateObjects( int numObjects )
	{
		while ( numObjects > 0 )
		{
			_freeList.AddLast( InitListNode( _internalAlloc() ) );
			--numObjects;
		}
	}
	
	private void InternalFreeObjects( int numObjects )
	{
		numObjects = InternalFreeObjects( ref _freeList, numObjects );
		if ( numObjects > 0 )
		{
			InternalFreeObjects( ref _activeList, numObjects );
		}
	}
	
	private int InternalFreeObjects( ref LinkedList<T> list, int numObjects )
	{
		while ( numObjects > 0 )
		{
			_internalFree( list.Last.Value );
			list.RemoveLast();
			--numObjects;
		}
		
		return numObjects;
	}
	
	private void EnsureInternalAllocFree()
	{
		if ( _internalAlloc == null )
		{
			_internalAlloc = () => null;
		}
		
		if ( _internalFree == null )
		{
			_internalFree = obj => {};
		}
	}

	public IEnumerable<T> ActiveObjects
	{
		get
		{
			LinkedListNode<T>	objNode = _activeList.First, nextNode;
			
			for ( ; objNode != null; objNode = nextNode )
			{
				nextNode = objNode.Next;
				
				yield return objNode.Value;
			}
		}
	}
}

// Fast-Free support

public interface IObjectPoolFFItem<T>
{
	LinkedListNode<T>	_pObjectPoolListNode	{ get; }
}

public class ObjectPoolFF<T> : ObjectPool<T> where T : class, IObjectPoolFFItem<T>
{
	public ObjectPoolFF( int size, DInternalAlloc internalAllocFn, DInternalFree internalFreeFn ) : base( size, internalAllocFn, internalFreeFn )
	{
	}
	
	protected override LinkedListNode<T> InitListNode( T obj )
	{
		return obj._pObjectPoolListNode;
	}
	
	protected override LinkedListNode<T> GetActiveListNode( T obj )
	{
		return obj._pObjectPoolListNode;
	}
}
}//namespace AmuzoEngine
