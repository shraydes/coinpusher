﻿////////////////////////////////////////////
// 
// ReadonlyListWrapper.cs
//
// Created: 01/08/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !UNITY_2018_3_OR_NEWER && (UNITY_EDITOR || !UNITY_WSA)
#define NO_DOT_NET_READONLY_LIST
#endif

using System;
using System.Collections;
using System.Collections.Generic;

namespace AmuzoEngine
{
#if NO_DOT_NET_READONLY_LIST
	public interface IReadOnlyList<ElementT> : IEnumerable<ElementT>, IEnumerable
	{
		ElementT this[int index]
		{
			get;
		}

		int Count
		{
			get;
		}

		bool Contains( ElementT item );

		void CopyTo( ElementT[] array, int arrayIndex );

		int IndexOf( ElementT item );
	}
#endif

	public class ReadOnlyListWrapperBase<ElementT>
	{
		private IList<ElementT>	_wrappedList;

		private System.Func<IList<ElementT>>	_getWrappedList;

		protected IList<ElementT>	_pWrappedList
		{
			get
			{
				return _wrappedList ?? ( _getWrappedList != null ? _getWrappedList() : null );
			}
		}

		protected ReadOnlyListWrapperBase( IList<ElementT> wrappedList )
		{
			_wrappedList = wrappedList;
		}

		protected ReadOnlyListWrapperBase( System.Func<IList<ElementT>> getWrappedList )
		{
			_getWrappedList = getWrappedList;
		}

		private ReadOnlyListWrapperBase()
		{
		}
	}

	public class ReadOnlyListWrapper<ElementT> : ReadOnlyListWrapperBase<ElementT>, IReadOnlyList<ElementT>, IList<ElementT>
	{
		public ReadOnlyListWrapper( IList<ElementT> wrappedList ) : base( wrappedList )
		{
		}

		public ReadOnlyListWrapper( System.Func<IList<ElementT>> getWrappedList ) : base( getWrappedList )
		{
		}

		public ElementT this[int index]
		{
			get
			{
				return _pWrappedList[index];
			}

			set
			{
				throw new System.NotImplementedException( "Cannot set item in readonly list" );
			}
		}

		public int Count
		{
			get
			{
				return _pWrappedList.Count;
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		public void Add( ElementT item )
		{
			throw new System.NotImplementedException( "Cannot add item in readonly list" );
		}

		public void Clear()
		{
			throw new System.NotImplementedException( "Cannot clear items in readonly list" );
		}

		public bool Contains( ElementT item )
		{
			return _pWrappedList.Contains( item );
		}

		public void CopyTo( ElementT[] array, int arrayIndex )
		{
			_pWrappedList.CopyTo( array, arrayIndex );
		}

		public IEnumerator<ElementT> GetEnumerator()
		{
			return _pWrappedList.GetEnumerator();
		}

		public int IndexOf( ElementT item )
		{
			return _pWrappedList.IndexOf( item );
		}

		public void Insert( int index, ElementT item )
		{
			throw new System.NotImplementedException( "Cannot insert item in readonly list" );
		}

		public bool Remove( ElementT item )
		{
			throw new System.NotImplementedException( "Cannot remove item in readonly list" );
		}

		public void RemoveAt( int index )
		{
			throw new System.NotImplementedException( "Cannot remove item in readonly list" );
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _pWrappedList.GetEnumerator();
		}
	}

	public static class ReadOnlyListExtensions
	{
		public static void ForEach<T>( this IReadOnlyList<T> thisList, System.Action<T> elementAction )
		{
			if ( elementAction == null ) return;
			for ( int i = 0; i < thisList.Count; ++i )
			{
				elementAction( thisList[i] );
			}
		}

		//----------------------------------------------------------------------------------------------------------------
		public static List<T> FindAll<T>( this IReadOnlyList<T> thisList, System.Predicate<T> elementMatch )
		{
			if ( elementMatch == null ) return new List<T>( thisList );
			List<T>	newList = new List<T>();
			for ( int i = 0; i < thisList.Count; ++i )
			{
				if ( elementMatch( thisList[i] ) == true )
				{
					newList.Add( thisList[i] );
				}
			}
			return newList;
		}
	}
}
