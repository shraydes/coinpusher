﻿////////////////////////////////////////////
// 
// CollectionExtensions.cs
//
// Created: 01/08/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if UNITY_2018_3_OR_NEWER || (UNITY_WSA && !UNITY_EDITOR)
#define HAS_DOT_NET_READONLY_LIST
#endif

using System.Collections;
using System.Collections.Generic;

public static class CollectionExtensions
{
	//----------------------------------------------------------------------------------------------------------------
	public static void SafeClear<T>(this List<T> list)
	{
		if(list != null && list.Count > 0)
		{
			list.Clear();
		}
	}
	
	//----------------------------------------------------------------------------------------------------------------
	public static T First<T>(this IList<T> targ)
	{
		return targ.Count == 0 ? default(T) : targ[0];
    }

	//----------------------------------------------------------------------------------------------------------------
	public static T Last<T>(this IList<T> targ)
	{
		return targ.Count == 0 ? default(T) : targ[targ.Count - 1];
    }

	//----------------------------------------------------------------------------------------------------------------
	///<summary>Returns a new array that is a shallow clone of the original with item added to the end</summary>
	public static T[] Push<T>(this T[] targ, T item)
	{
		T[] newList = new T[targ.Length + 1];
		targ.CopyTo(newList, 0);
		newList[newList.Length - 1] = item;
		return newList;
    }

	//----------------------------------------------------------------------------------------------------------------
	public static T GetRandom<T>(this IList<T> list)
	{
		if (list.Count == 0)
		{
			return default( T );
		}

		return list[UnityEngine.Random.Range(0, list.Count)];
	}

	//----------------------------------------------------------------------------------------------------------------
	public static T GetRandom<T>(this IList<T> list, bool doRemove)
	{
		if (list.Count == 0)
		{
			return default( T );
		}

		int index = UnityEngine.Random.Range(0, list.Count);

		T item = list[index];

		if (doRemove)
		{
			list.RemoveAt(index);
		}

		return item;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static T[] Concat<T>(this IList<T> targ, IList<T> other)
	{
		T[] ret = new T[targ.Count + other.Count];
		targ.CopyTo( ret, 0 );
		other.CopyTo(ret, targ.Count);
		return ret;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static T[] Add<T>(this IList<T> targ, params T[] others)
	{
		T[] ret = new T[targ.Count + others.Length];
		targ.CopyTo( ret, 0 );
		others.CopyTo(ret, targ.Count);
		return ret;
	}

	#if HAS_DOT_NET_READONLY_LIST
	//----------------------------------------------------------------------------------------------------------------
	public static int IndexOf<T>(this IReadOnlyList<T> targ, T item)
	{
		for ( int i = 0; i < targ.Count; ++i )
		{
			if ( targ[i].Equals( item ) ) return i;
		}
		return -1;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool Contains<T>(this IReadOnlyList<T> targ, T item)
	{
		return targ.IndexOf( item ) != -1;
	}
	#else
	//----------------------------------------------------------------------------------------------------------------
	public static bool Contains<T>(this IList<T> targ, T item)
	{
		return targ.IndexOf( item ) != -1;
	}
	#endif

	//----------------------------------------------------------------------------------------------------------------
	public static void Remove<T>(this IList<T> targ, IList<T> items)
	{
		foreach (T item in items )
		{
			targ.Remove( item );
		}
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void CloneTo( this IList thisList, IList toList )
	{
		if ( toList == null ) return;

		int	commonCount = System.Math.Min( thisList.Count, toList.Count );

		for ( int i = 0; i < commonCount; ++i )
		{
			toList[i] = thisList[i];
		}

		for ( int i = commonCount; i < thisList.Count; ++i )
		{
			toList.Add( thisList[i] );
		}

		while ( toList.Count > thisList.Count )
		{
			toList.RemoveAt( toList.Count - 1 );
		}
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void CloneTo<T>( this IList<T> thisList, IList<T> toList )
	{
		if ( toList == null ) return;

		int	commonCount = System.Math.Min( thisList.Count, toList.Count );

		for ( int i = 0; i < commonCount; ++i )
		{
			toList[i] = thisList[i];
		}

		for ( int i = commonCount; i < thisList.Count; ++i )
		{
			toList.Add( thisList[i] );
		}

		while ( toList.Count > thisList.Count )
		{
			toList.RemoveAt( toList.Count - 1 );
		}
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void ForEach<T>( this IList<T> thisList, System.Action<T> elementAction )
	{
		if ( elementAction == null ) return;
		for ( int i = 0; i < thisList.Count; ++i )
		{
			elementAction( thisList[i] );
		}
	}

	//----------------------------------------------------------------------------------------------------------------
	public static List<T> FindAll<T>( this IList<T> thisList, System.Predicate<T> elementMatch )
	{
		if ( elementMatch == null ) new List<T>( thisList );
		List<T>	newList = new List<T>();
		for ( int i = 0; i < thisList.Count; ++i )
		{
			if ( elementMatch( thisList[i] ) == true )
			{
				newList.Add( thisList[i] );
			}
		}
		return newList;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void SetCountAtLeast( this IList list, int countAtLeast, object value = null, System.Action<int> onPreAdd = null, System.Action<int> onAdded = null )
	{
		if ( list == null ) throw new System.NullReferenceException( "list is null" );

		while ( list.Count < countAtLeast )
		{
			if ( onPreAdd != null )
			{
				onPreAdd( list.Count );
			}

			list.Add( value );

			if ( onAdded != null )
			{
				onAdded( list.Count - 1 );
			}
		}
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void SetCountAtMost( this IList list, int countAtMost, System.Action<int> onPreRemove = null, System.Action<int> onRemoved = null )
	{
		if ( list == null ) throw new System.NullReferenceException( "list is null" );
		if ( countAtMost < 0 ) throw new System.ArgumentOutOfRangeException( "countAtMost", countAtMost, "expected non-negative value" );

		while ( list.Count > countAtMost )
		{
			if ( onPreRemove != null )
			{
				onPreRemove( list.Count - 1 );
			}

			list.RemoveAt( list.Count - 1 );

			if ( onRemoved != null )
			{
				onRemoved( list.Count );
			}
		}
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void SetCount( this IList list, int count, object defaultValue = null, System.Action<int> onPreAdd = null, System.Action<int> onAdded = null, System.Action<int> onPreRemove = null, System.Action<int> onRemoved = null )
	{
		if ( list == null ) throw new System.NullReferenceException( "list is null" );
		if ( count < 0 ) throw new System.ArgumentOutOfRangeException( "count", count, "expected non-negative value" );

		list.SetCountAtLeast( count, defaultValue, onPreAdd, onAdded );
		list.SetCountAtMost( count, onPreRemove, onRemoved );
	}
}

