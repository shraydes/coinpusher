﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

public static class FileHelper
{
	public static void WriteTextFile( string filePath, string fileData )
	{
#if !UNITY_WEBPLAYER
		var	folderPath = Path.GetDirectoryName( filePath );
		
		if ( folderPath != null && folderPath != "" )
		{
			try
			{
				Directory.CreateDirectory( folderPath );
			}
			catch ( System.Exception )
			{
				Debug.LogWarning( "Failed to create directory: " + folderPath.ToString() );
			}
		}
		
		try
		{
			File.WriteAllText( filePath, fileData );
		}
		catch ( System.Exception )
		{
			Debug.LogWarning( "Failed to write file:" + filePath.ToString() );
		}
#endif
	}

	public static void ForEachFileInDirectory( string dirPath, System.Action<FileInfo> action, System.Predicate<FileInfo> match = null )
	{
		ForEachFileInDirectory( new DirectoryInfo( dirPath ), action, match );
	}

	public static void ForEachFileInDirectory( DirectoryInfo dirInfo, System.Action<FileInfo> action, System.Predicate<FileInfo> match = null )
	{
		if ( dirInfo == null || !dirInfo.Exists ) return;
		ForEachFile( dirInfo.GetFiles(), action, match );
	}

	public static void ForEachFile( FileInfo[] fileInfos, System.Action<FileInfo> action, System.Predicate<FileInfo> match = null )
	{
		foreach ( FileInfo fi in fileInfos )
		{
			if ( match == null || match( fi ) == true )
			{
				action( fi );
			}
		}
	}

	public static void ForEachSubDirectory( string dirPath, System.Action<DirectoryInfo> action, System.Predicate<DirectoryInfo> match = null )
	{
		ForEachSubDirectory( new DirectoryInfo( dirPath ), action, match );
	}

	public static void ForEachSubDirectory( DirectoryInfo dirInfo, System.Action<DirectoryInfo> action, System.Predicate<DirectoryInfo> match = null )
	{
		if ( dirInfo == null || !dirInfo.Exists ) return;
		ForEachDirectory( dirInfo.GetDirectories(), action, match );
	}

	public static void ForEachDirectory( DirectoryInfo[] dirInfos, System.Action<DirectoryInfo> action, System.Predicate<DirectoryInfo> match = null )
	{
		foreach ( DirectoryInfo di in dirInfos )
		{
			if ( match == null || match( di ) == true )
			{
				action( di );
			}
		}
	}


	public static string AddLocalFileUrlScheme( string absolutePath )
	{
		if ( string.IsNullOrEmpty( absolutePath ) ) return "file:///";
		if ( absolutePath.StartsWith( "/" ) ) return "file://" + absolutePath;
		return "file:///" + absolutePath;
	}

	public delegate int	DForEachLineAction( List<string> lines, int lineIdx );
	public static void ForEachLine( string filePath, DForEachLineAction lineAction, string saveFilePath = null )
	{
		List<string>	lines;
		
		try
		{
			lines = new List<string>( File.ReadAllLines( filePath ) );
		}
		catch ( System.Exception )
		{
			Debug.LogError( "Failed to read file: " + filePath );
			throw;
		}

		if ( lineAction != null )
		{
			for ( int i = 0; i < lines.Count; ++i )
			{
				i += lineAction( lines, i );
			}
		}

		if ( !string.IsNullOrEmpty( saveFilePath ) )
		{
			Directory.CreateDirectory( Path.GetDirectoryName( saveFilePath ) );
			File.WriteAllLines( saveFilePath, lines.ToArray() );
		}
	}
}
