﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;

public class JsonMapperLite 
{
	static bool
		DO_DEBUG = false;

    private static bool _loadedAnything = false;
    

	// -------------------------------------------------------------------------------------------
	public static JsonData ToObject( JsonReader reader )
	{
		JsonData root = null;
        _loadedAnything = false;   

		while(reader.Read())
		{
			if(reader.Token == JsonToken.ObjectStart || reader.Token == JsonToken.ArrayStart)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Json checked and started with a " + ( reader.Token == JsonToken.ObjectStart? "Object" : "Array" ) + ".");

				if( reader.Token == JsonToken.ArrayStart )
				{
					parseArray( reader, ref root );
				}
				else
				{
					root = ParseObject( reader, true );
				}

			}
			else if(reader.Token != JsonToken.ObjectEnd)
			{
				Debug.LogError("Unexpected Token when reading JSONData");
				return null;
			}
		}

     //   Debug.LogError("JsonLite Loaded Json with: " + root.Count + " elements");

        if (!_loadedAnything) return null;

		return root;
	}

	// -------------------------------------------------------------------------------------------
	private static JsonData ParseObject( JsonReader reader, bool isRoot = false )
	{
		string propertyName = null;
		JsonData objectData = new JsonData();



		while(reader.Read())
		{
			if(reader.Token == JsonToken.PropertyName)
			{
				propertyName = (string)reader.Value;
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Property name found [" + propertyName + "]");
			}
			else if(reader.Token == JsonToken.ObjectEnd)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Object Ended");
				return objectData;
			}
			else if(reader.Token == JsonToken.ObjectStart)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Object Started");
				JsonData subObject = ParseObject( reader );
				objectData[propertyName] = subObject;

                _loadedAnything = true;
			}
			else if(reader.Token == JsonToken.String)
			{
				objectData[propertyName] = (string)reader.Value;
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: String found [" + (string)reader.Value + "]");
                _loadedAnything = true;
			}
			else if(reader.Token == JsonToken.Boolean)
			{
				objectData[propertyName] = (bool)reader.Value;
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Boolean found [" + (bool)reader.Value + "]");
                _loadedAnything = true;
			}
			else if(reader.Token == JsonToken.Int)
			{
				objectData[propertyName] = (int)reader.Value;
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Int found [" + (int)reader.Value + "]");
                _loadedAnything = true;
			}
			else if(reader.Token == JsonToken.Long)
			{
				objectData[propertyName] = (long)reader.Value;
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Long found [" + (long)reader.Value + "]");
                _loadedAnything = true;
			}
			else if(reader.Token == JsonToken.Double)
			{
				objectData[propertyName] = (double)reader.Value;
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Double found [" + (double)reader.Value + "]");
                _loadedAnything = true;
			}
			else if(reader.Token == JsonToken.ArrayStart)
			{
				JsonData jsonArray = new JsonData();
                _loadedAnything = true;

				parseArray( reader, ref jsonArray );

				objectData[propertyName] = jsonArray;
			}
			else if(reader.Token == JsonToken.Null)
			{
				objectData[propertyName] = "Null";
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: String found [Null]");
				_loadedAnything = true;
			}
			else
			{
				Debug.LogWarning("Unhandled Token: " + reader.Token);
			}
		}

		return objectData;
	}

	static void parseArray( JsonReader reader, ref JsonData arrayRoot)
	{
		if( arrayRoot == null )
		{
			arrayRoot = new JsonData();
		}

		// fudge: make sure we are an array type
		// adding an object then clearing will give us an empty array
		arrayRoot.Add(new JsonData(false));
		arrayRoot.Clear();

		if( DO_DEBUG ) Debug.Log("JsonMapperLite: Array Started");

		while(reader.Read()) 
		{
			if(reader.Token == JsonToken.ArrayEnd)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Array Ended");
				break;
			}
			else if(reader.Token == JsonToken.ObjectStart)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Array Object Started");
				JsonData subObject = ParseObject( reader );
				arrayRoot.Add(subObject);
			}
			else if(reader.Token == JsonToken.String)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Array string found [" + (string)reader.Value + "]");
				arrayRoot.Add(new JsonData((string)reader.Value));
			}
			else if(reader.Token == JsonToken.Boolean)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Array bool found [" + (bool)reader.Value + "]");
				arrayRoot.Add(new JsonData((bool)reader.Value));
			}
			else if(reader.Token == JsonToken.Int)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Array int found [" + (int)reader.Value + "]");
				arrayRoot.Add(new JsonData((int)reader.Value));
			}
			else if(reader.Token == JsonToken.Double)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Array double found [" + (double)reader.Value + "]");
				arrayRoot.Add(new JsonData((double)reader.Value));
			}
			else if(reader.Token == JsonToken.Long)
			{
				if( DO_DEBUG ) Debug.Log("JsonMapperLite: Array long found [" + (long)reader.Value + "]");
				arrayRoot.Add(new JsonData((long)reader.Value));
			}
			else if(reader.Token == JsonToken.ArrayStart)
			{
				JsonData jsonArray = new JsonData();

				parseArray( reader, ref jsonArray );

				arrayRoot.Add( jsonArray );
			}
		}
	}
}
