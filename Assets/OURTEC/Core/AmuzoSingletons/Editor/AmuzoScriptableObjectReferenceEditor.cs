﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AmuzoScriptableObjectReference))]
public class AmuzoScriptableObjectReferenceEditor : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		if( !Application.isPlaying && GUILayout.Button( "Load From Project" ) )
		{
			AmuzoScriptableObjectReference script = serializedObject.targetObject as AmuzoScriptableObjectReference;
			script._scriptableObjects = AmuzoScriptableObjectReference.GetAllScriptableObjects();
		}
	}
}
