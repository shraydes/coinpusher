﻿////////////////////////////////////////////
// 
// AmuzoScriptableObjectReference.cs
//
// Created: 11/12/2014 rbateman
// Contributors: 
// 
// Intention:
// This class is to be the storehouse of all AmuzoScriptableSingleton instance references.
// Under the hood it will be used to facitlitate the AmuzoScriptableSingleton _pInstance property.
//
// Note: 
// The expected usage of the class is for it to be added to the 'Loading' scene on a GameObject 
// and popuated with the AmuzoScriptableSingleton instances and serialised into a prefab.
//
// Remark: 
// No Remarks.
//
////////////////////////////////////////////

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System;
using System.Collections.Generic;
#endif

public class AmuzoScriptableObjectReference : AmuzoMonoSingleton<AmuzoScriptableObjectReference>
{
	// CONSTS / DEFINES / ENUMS

	private static ScriptableObject[] _scriptableObjectsArray;

	// SINGLETON DECLARATION

	// NESTED CLASSES / STRUCTS

	// MEMBERS 

	public ScriptableObject[] _scriptableObjects;

	// PROPERTIES

	// ABSTRACTS / VIRTUALS

	// STATIC FUNCTIONS

	public static T GetScriptableObject<T>() where T : ScriptableObject
	{
		if( _scriptableObjectsArray == null )
		{
			_scriptableObjectsArray = GetAllScriptableObjects();
		}

		if( _scriptableObjectsArray == null )
		{
			return null;
		}

		for( int x = 0, max = _scriptableObjectsArray.Length; x < max; x++ )
		{
			if( !( _scriptableObjectsArray[x] is T ) )
			{
				continue;
			}
			return _scriptableObjectsArray[x] as T;
		}

		return null;
	}

	public static ScriptableObject[] GetAllScriptableObjects()
	{
		ScriptableObject[] returnArray = new ScriptableObject[0];

#if UNITY_EDITOR
		if( !Application.isPlaying )
		{
			string[] allAssets = AssetDatabase.GetAllAssetPaths();
			List<ScriptableObject> scriptableObjects = new List<ScriptableObject>();
			ScriptableObject tempObject = null;

			for( int x = 0, max = allAssets.Length; x < max; x++ )
			{
				if( !allAssets[x].EndsWith( ".asset", StringComparison.OrdinalIgnoreCase ) )
				{
					continue;
				}

				try
				{
					tempObject = AssetDatabase.LoadAssetAtPath( allAssets[x], typeof( ScriptableObject ) ) as ScriptableObject;
				}
				catch { }

				if( tempObject == null )
				{
					continue;
				}

				scriptableObjects.Add( tempObject );
			}

			returnArray = scriptableObjects.ToArray();
		}
#endif

		if( returnArray.Length == 0 && _pInstance != null && _pInstance._scriptableObjects != null )
		{
			returnArray = _pInstance._scriptableObjects;
			_pInstance._scriptableObjects = null;
		}

		return returnArray;
	}

	// FUNCTIONS 

	// ** UNITY

	protected override void OnDestroy()
	{
		base.OnDestroy();

		_scriptableObjectsArray = null;
	}

	// ** PUBLIC

	// ** PROTECTED

	// ** PRIVATE

	// OPERATOR OVERLOADS
}
