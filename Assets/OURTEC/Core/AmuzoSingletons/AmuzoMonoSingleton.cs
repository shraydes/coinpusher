﻿////////////////////////////////////////////
// 
// AmuzoMonoSingleton.cs
//
// Created: 11/12/2014 rbateman
// Contributors: 
// 
// Intention:
// This class is to be the inherited type of all Amuzo MonoBehaviour 
// Singletons. This class uses generics to allow singleton access to be inherited 
// removing the need to duplicate the singleton decleration between all singletons.
//
// This class also allows us to have a single point of reference if any additional 
// mangement code needs to be added later down the line.
//
// Note: 
// The expected usage of the class is in the inherited type for future singletons:
// public class AmuzoSaveDataFacade : AmuzoMonoSignleton<AmuzoSaveDataFacade>
//
// Remark: 
// No Remarks.
//
////////////////////////////////////////////

using UnityEngine;

public class AmuzoMonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
	// CONSTS / DEFINES / ENUMS

	// SINGLETON DECLARATION

	public static bool _pExists
	{
		get
		{
			return _instance != null;
		}
	}

	private static T _instance;
	public static T _pInstance
	{
		get
		{
			// This check is to prevent singletons from being reconstitiuted if the game is being closed.
			if( _hasBeenFinalDestroyed )
			{
				return null;
			}

			if( _instance == null )
			{
				// In case the instance reference hasn't been set up yet, check for it.
				_instance = ( T )FindObjectOfType( typeof( T ) );

				if( _instance == null )
				{
					// If not found, try to resource-load a prefab
					T	prefab = Resources.Load<T>( typeof(T).Name );

					if ( prefab != null )
					{
						_instance = Instantiate( prefab );
					}

					if( _instance == null )
					{
						// No instance is assigned and none exist, so create a new instance.
						GameObject newGameObject = new GameObject( typeof( T ).ToString() );
						_instance = newGameObject.AddComponent<T>();
					}
				}

				AmuzoMonoSingleton<T>	singleton = _instance as AmuzoMonoSingleton<T>;

				if ( singleton != null && !_hasBeenInitialised )
				{
					if ( singleton._isAutoInitialise )
					{
						singleton.Initialise();
					}
					else
					{
						Debug.LogError( typeof( T ).ToString() + " was not initialised before it's instance was accessed!" );
					}
				}
			}

			return _instance;
		}
	}

	// NESTED CLASSES / STRUCTS

	// MEMBERS 

	[SerializeField]
	private bool _dontDestroyOnLoad = true;

	[SerializeField]
	private bool _isAutoInitialise = true;

	[SerializeField]
	private bool _allowBeingDestroyed = false;

	private static bool _hasBeenInitialised = false;

	private static bool _hasBeenFinalDestroyed = false;

	// PROPERTIES

	public static bool	_pIsInitialised
	{
		get
		{
			return _pExists && _hasBeenInitialised;
		}
	}

	// ABSTRACTS / VIRTUALS

	protected void Initialise()
	{
		OnInitialise();

		_hasBeenInitialised = true;
	}

	protected virtual void OnInitialise()
	{
	}

	protected virtual bool OnResolveSingletonConflict( T newInstance )
	{
		return false;
	}

	// STATIC FUNCTIONS

	// FUNCTIONS 

	// ** UNITY

	protected virtual void Awake()
	{
		if( _dontDestroyOnLoad )
		{
			DontDestroyOnLoad( gameObject );
		}

		if( _instance == null && !_hasBeenFinalDestroyed )
		{
			_instance = this as T;

			if ( _isAutoInitialise && !_hasBeenInitialised )
			{
				Initialise();
			}
		}
		else if( _instance != this )
		{
			bool	isResolved = ( _instance as AmuzoMonoSingleton<T> ).OnResolveSingletonConflict( this as T );

			if ( !isResolved )
			{
				Destroy( gameObject );
			}
		}
	}

	protected virtual void OnDestroy()
	{
		if ( _instance == this )
		{
			_instance = null;
			_hasBeenFinalDestroyed = !_allowBeingDestroyed;
			_hasBeenInitialised = false;
		}
	}

	// ** PUBLIC

	// ** PROTECTED

	// ** PRIVATE

	// OPERATOR OVERLOADS
}
