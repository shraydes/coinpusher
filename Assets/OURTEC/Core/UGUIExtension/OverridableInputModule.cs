﻿////////////////////////////////////////////
// 
// OverridableInputModule.cs
//
// Created: 17/01/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_OverridableInputModule
#endif

using UnityEngine;
using UnityEngine.EventSystems;

namespace AmuzoEngine
{
	public class OverridableInputModule : StandaloneInputModule
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[OverridableInputModule] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//

		//
		// MEMBERS 
		//

		//
		// PROPERTIES
		//

		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		protected override void Awake()
		{
			base.Awake();

			m_InputOverride = gameObject.AddComponent<OverridableInput>();
		}

		// ** PUBLIC
		//

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
