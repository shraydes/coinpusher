﻿////////////////////////////////////////////
// 
// OverridableInput.cs
//
// Created: 17/01/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_OverridableInput
#endif

using UnityEngine;
using UnityEngine.EventSystems;

namespace AmuzoEngine
{
	public class OverridableInput : BaseInput
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[OverridableInput] ";

		//
		// SINGLETON DECLARATION
		//

		public static OverridableInput	_pInstance { get; private set; }

		//
		// NESTED CLASSES / STRUCTS
		//

		//
		// MEMBERS 
		//

		//
		// PROPERTIES
		//

		public System.Func<int>	_pGetTouchCountOverride { private get; set; }

		public System.Func<int, Touch>	_pGetTouchOverride { private get; set; }

		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		protected override void Awake()
		{
			base.Awake();

			if ( _pInstance == null )
			{
				_pInstance = this;
			}
		}

		protected override void OnDestroy()
		{
			if ( _pInstance == this )
			{
				_pInstance = null;
			}

			base.OnDestroy();
		}

		// ** PUBLIC
		//

		public override int touchCount
		{
			get
			{
				return _pGetTouchCountOverride != null ? _pGetTouchCountOverride() : base.touchCount;
			}
		}

		public override Touch GetTouch( int index )
		{
			return _pGetTouchOverride != null ? _pGetTouchOverride( index ) : base.GetTouch( index );
		}

		// ** PROTECTED
		//

		// ** PRIVATE
		//
	
		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
