﻿////////////////////////////////////////////
// 
// CameraBlend.cs
//
// Created: 05/06/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_CameraBlend
#endif

using UnityEngine;

namespace AmuzoEngine
{
	public class CameraBlend
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[CameraBlend] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//
	
		private Camera	_blendCamera;

		private float	_duration;

		private Easing.EaseType	_easeType;

		private System.Action	_onComplete;

		private CameraState	_startState;

		private CameraState	_targetState;

		private CameraState	_blendState;

		private float	_clock;

		//
		// PROPERTIES
		//
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		// ** PUBLIC
		//
	
		public CameraBlend( Camera blendCamera, CameraState start, CameraState target, float duration, Easing.EaseType easeType, System.Action onComplete )
		{
			_blendCamera = blendCamera;
			_startState = new CameraState( start );
			_targetState = new CameraState( target );
			_blendState = new CameraState( _startState );
			_duration = duration;
			_easeType = easeType;
			_onComplete = onComplete;

			_clock = 0f;
			_blendState.CopyTo( _blendCamera );
		}

		public void Update( Camera target, float dt )
		{
			_targetState.CopyFrom( target );

			Update( dt );
		}

		public void Update( float dt )
		{
			_clock += dt;

			if ( _clock > _duration )
			{
				_clock = _duration;
			}

			float	t = Easing.Ease( _easeType, _clock, _duration, 0f, 1f );

			_blendState.Lerp( _startState, _targetState, t );
			_blendState.CopyTo( _blendCamera );

			if ( _clock >= _duration )
			{
				if ( _onComplete != null )
				{
					_onComplete();
				}
			}
		}

		// ** INTERNAL
		//
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//
	
		private CameraBlend()
		{
		}

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
