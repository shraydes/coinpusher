﻿using UnityEngine;
using UnityEditor;
using System.Collections;

// https://blogs.unity3d.com/2012/09/07/property-drawers-in-unity-4/

[CustomPropertyDrawer (typeof(FunctionOfX.FunctionOfXDescriptionAttribute))]
public class FunctionOfXDescription : PropertyDrawer
{
	const int descriptionHeight = 30;

	private static SerializedObject _serialisedObject;
	private static SerializedProperty _functionOfXProperty;
	private static SerializedProperty _functionOfXSegmentsProperty;
	private static SerializedProperty _functionOfXSegmentsElementProperty;

	//CC20170526 - commented out to remove "value never used" warning
	//private static SerializedProperty _functionOfXSegmentsElementIsPositiveProperty;
	private static SerializedProperty _functionOfXSegmentsElementMultiplierProperty;
	private static SerializedProperty _functionOfXSegmentsElementPowerIntProperty;
	private static SerializedProperty _functionOfXSegmentsElementDoUsePowerFloatProperty;
	private static SerializedProperty _functionOfXSegmentsElementPowerFloatProperty;

	private static FunctionOfX.FunctionSegment[] _array;

	FunctionOfX.FunctionOfXDescriptionAttribute funcOfXAttribute { get { return ((FunctionOfX.FunctionOfXDescriptionAttribute)attribute); } }

	public override float GetPropertyHeight (SerializedProperty prop,
                                             GUIContent label)
	{
		return descriptionHeight;
    }

	// Here you can define the GUI for your property drawer. Called by Unity.
    public override void OnGUI (Rect position,
                                SerializedProperty prop,
                                GUIContent label)
	{
		// Instead of showing the description property show our nicely printed equation. 
		Rect descriptionPosition = EditorGUI.IndentedRect( position );
		descriptionPosition.height = descriptionHeight;
		DrawDescriptionBox( descriptionPosition, prop );
	}
	

	void DrawDescriptionBox (Rect position, SerializedProperty prop)
	{
		_serialisedObject = prop.serializedObject;

		if( _serialisedObject == null )
		{
			return;
		}
		
		_functionOfXProperty = _serialisedObject.FindProperty( prop.propertyPath.Replace( "._functionDescriptionString", "" ) );

		if( _functionOfXProperty == null )
		{
			_serialisedObject = null;
			return;
		}

		_functionOfXSegmentsProperty = _functionOfXProperty.FindPropertyRelative( "_segments" );
		
		_array = new FunctionOfX.FunctionSegment[ _functionOfXSegmentsProperty.arraySize ];
		
		for( int x = 0; x < _array.Length; x++ )
		{
			_functionOfXSegmentsElementProperty = _functionOfXSegmentsProperty.GetArrayElementAtIndex( x );
			
			// This is horrible, but it seems there is no way to make a SerializedProperty into it's custom class without making that class extend SerializableObject :/

			_array[x] = new FunctionOfX.FunctionSegment();

			//CC20170526 - commented out to remove "value never used" warning
			//_functionOfXSegmentsElementIsPositiveProperty = _functionOfXSegmentsElementProperty.FindPropertyRelative( "_isPositive" );
			_functionOfXSegmentsElementMultiplierProperty = _functionOfXSegmentsElementProperty.FindPropertyRelative( "_multiplier" );
			_functionOfXSegmentsElementPowerIntProperty = _functionOfXSegmentsElementProperty.FindPropertyRelative( "_powerInt" );
			_functionOfXSegmentsElementDoUsePowerFloatProperty = _functionOfXSegmentsElementProperty.FindPropertyRelative( "_doUsePowerFloat" );
			_functionOfXSegmentsElementPowerFloatProperty = _functionOfXSegmentsElementProperty.FindPropertyRelative( "_powerFloat" );

			_array[x]._multiplier = _functionOfXSegmentsElementMultiplierProperty.floatValue;
			_array[x]._powerInt = _functionOfXSegmentsElementPowerIntProperty.intValue;
			_array[x]._doUsePowerFloat = _functionOfXSegmentsElementDoUsePowerFloatProperty.boolValue;
			_array[x]._powerFloat = _functionOfXSegmentsElementPowerFloatProperty.floatValue;
		}

        EditorGUI.HelpBox( position, FunctionOfX.MakeFunctionDescriptionString( _array ), MessageType.Info );

		_array = null;
		_functionOfXProperty = null;
		_serialisedObject = null;

		//CC20170526 - commented out to remove "value never used" warning
		//_functionOfXSegmentsElementIsPositiveProperty = null;
		_functionOfXSegmentsElementMultiplierProperty = null;
		_functionOfXSegmentsElementPowerIntProperty = null;
		_functionOfXSegmentsElementDoUsePowerFloatProperty = null;
		_functionOfXSegmentsElementPowerFloatProperty = null;
	}
}
