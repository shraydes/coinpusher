﻿////////////////////////////////////////////
// 
// ScriptingDefineSymbolsFacade.cs
//
// Created: dd/mm/yyyy ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using UnityEditor;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public static class ScriptingDefineSymbolsFacade
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ScriptingDefineSymbolsFacade] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	private static BuildTargetGroup	_sCurrentGroup = BuildTargetGroup.Unknown;

	private static List<string>	_sSymbols = null;

	private static JobCounter	_sChanges = new JobCounter( OnChangesBegan, OnChangesEnded );
	
	//
	// PROPERTIES
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public static string GetPlayerSettingsSymbols( BuildTargetGroup group )
	{
		return PlayerSettings.GetScriptingDefineSymbolsForGroup( group );
	}

	public static void SetPlayerSettingsSymbols( BuildTargetGroup group, string defines )
	{
		PlayerSettings.SetScriptingDefineSymbolsForGroup( group, defines );
	}

	public static bool BeginChanges( BuildTargetGroup group )
	{
		if ( group == BuildTargetGroup.Unknown )
		{
			LoggerAPI.LogError( LOG_TAG + "Cannot change symbols for group '" + group + "'" );
			return false;
		}

		if ( _sCurrentGroup != BuildTargetGroup.Unknown && _sCurrentGroup != group )
		{
			LoggerAPI.LogError( LOG_TAG + "Cannot change symbols for group '" + group + "', because already changing sybmols for group '" + _sCurrentGroup + "'" );
			return false;
		}

		_sCurrentGroup = group;
		_sChanges.BeginJob();

		return true;
	}

	public static void EndChanges()
	{
		_sChanges.EndJob();
	}

	public static void AddSymbol( string symbol, BuildTargetGroup group )
	{
		if ( BeginChanges( group ) == false ) return;

		if ( _sSymbols != null && !_sSymbols.Contains( symbol ) )
		{
			_sSymbols.Add( symbol );
		}

		EndChanges();
	}
	
	public static void RemoveSymbol( string symbol, BuildTargetGroup group )
	{
		if ( BeginChanges( group ) == false ) return;

		if ( _sSymbols != null && _sSymbols.Contains( symbol ) )
		{
			_sSymbols.Remove( symbol );
		}

		EndChanges();
	}
	
	public static void RemoveSymbols( System.Predicate<string> match, BuildTargetGroup group )
	{
		if ( match == null ) return;
		if ( BeginChanges( group ) == false ) return;

		if ( _sSymbols != null )
		{
			for ( int i = 0; i < _sSymbols.Count; ++i )
			{
				if ( match( _sSymbols[i] ) == true )
				{
					_sSymbols.RemoveAt( i-- );
				}
			}
		}

		EndChanges();
	}
	
	public static void RemoveAllSymbols( BuildTargetGroup group )
	{
		if ( BeginChanges( group ) == false ) return;

		if ( _sSymbols != null )
		{
			_sSymbols.Clear();
		}

		EndChanges();
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static void OnChangesBegan()
	{
		string		symbolsStr = PlayerSettings.GetScriptingDefineSymbolsForGroup( _sCurrentGroup );

//		LoggerAPI.Log( LOG_TAG + "Begin changes: " + symbolsStr );

		string[]	symbolsArr = symbolsStr.Split( ',', ';', ' ' );

		_sSymbols = new List<string>( symbolsArr );
	}
	
	private static void OnChangesEnded()
	{
		string	symbolsStr = string.Join( ";", _sSymbols.ToArray() );

	//	LoggerAPI.Log( LOG_TAG + "End changes: " + symbolsStr );

		PlayerSettings.SetScriptingDefineSymbolsForGroup( _sCurrentGroup, symbolsStr );

		_sSymbols = null;
		_sCurrentGroup = BuildTargetGroup.Unknown;
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

