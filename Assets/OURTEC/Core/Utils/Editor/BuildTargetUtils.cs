﻿////////////////////////////////////////////
// 
// BuildTargetUtils.cs
//
// Created: 15/04/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
#if UNITY_4_6
#define UNITY_4
#endif

#if !UNITY_4
#define SUPPORT_WEBGL
#endif

#if UNITY_5_4_OR_NEWER
#define SUPPORT_TVOS
#endif

#if !UNITY_5_4_OR_NEWER
#define SUPPORT_WEBPLAYER
#endif

#if UNITY_5_6_OR_NEWER
#define SUPPORT_SWITCH
#endif

using UnityEditor;
using LoggerAPI = UnityEngine.Debug;

public static class BuildTargetUtils
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[BuildTargetUtils] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public static BuildTargetGroup GetBuildTargetGroup( BuildTarget buildTarget )
	{
		BuildTargetGroup	group = BuildTargetGroup.Unknown;

		switch( buildTarget )
		{
		case BuildTarget.Android:
			group = BuildTargetGroup.Android;
			break;
#if UNITY_4
		case BuildTarget.iPhone:
			group = BuildTargetGroup.iPhone;
			break;
#else
		case BuildTarget.iOS:
			group = BuildTargetGroup.iOS;
			break;
#endif
#if SUPPORT_WEBPLAYER
		case BuildTarget.WebPlayer:
			group = BuildTargetGroup.WebPlayer;
			break;
#endif
#if SUPPORT_WEBGL
		case BuildTarget.WebGL:
			group = BuildTargetGroup.WebGL;
			break;
#endif
		case BuildTarget.StandaloneWindows:
			group = BuildTargetGroup.Standalone;
			break;
#if UNITY_4
		case BuildTarget.MetroPlayer:
			group = BuildTargetGroup.Metro;
			break;
#else
		case BuildTarget.WSAPlayer:
			group = BuildTargetGroup.WSA;
			break;
#endif
#if SUPPORT_TVOS
		case BuildTarget.tvOS:
			group = BuildTargetGroup.tvOS;
			break;
#endif
#if SUPPORT_SWITCH
		case BuildTarget.Switch:
			group = BuildTargetGroup.Switch;
			break;
#endif
		}

		return group;
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

