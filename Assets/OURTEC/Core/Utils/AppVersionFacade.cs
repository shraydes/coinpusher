﻿////////////////////////////////////////////
// 
// AppVersionFacade.cs
//
// Created: 06/03/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public static class AppVersionFacade
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[AppVersionFacade] ";

	//
	// MEMBERS 
	//

	public static string _pAppVersionString
	{
		get
		{
#if UNITY_WSA && NETFX_CORE
			return GetWindowsAppVersion();
#elif PUMA_DEFINE_ANVIL
			return AnvilBuildInfo._pBundleVersion;
#else
			return "0";
#endif
		}
	}

	public static System.Version _pAppVersion
	{
		get
		{
			return new System.Version( _pAppVersionString );
		}
	}
	
	//
	// PROPERTIES
	//
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
#if UNITY_WSA && NETFX_CORE
    private static string GetWindowsAppVersion()
    {
        Windows.ApplicationModel.Package package = Windows.ApplicationModel.Package.Current;
        Windows.ApplicationModel.PackageId packageId = package.Id;
        Windows.ApplicationModel.PackageVersion version = packageId.Version;
  
        return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
    }
#endif
}

