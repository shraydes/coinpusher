﻿////////////////////////////////////////////
// 
// CoreExtensions.cs
//
// Created: 25/02/2016 ccuthbert
// Contributors:
// 
// Intention:
// Extensions to Unity classes and core Mono
// classes.  No dependencies on other systems, 
// apart from core Amuzo utils.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if UNITY_5_4_OR_NEWER
#define USE_UNITY_WEBREQUEST
#endif
 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using System.Globalization;
using System.Reflection;
using AmuzoPhysics;
#if USE_UNITY_WEBREQUEST
using UnityEngine.Networking;
using WebRequestType = UnityEngine.Networking.UnityWebRequest;
#else
using WebRequestType = UnityEngine.WWW;
#endif

public static partial class Extensions
{
	public static string PrintFormatXML(string xml)
	{
		if(Application.isEditor)
		{
			return xml;
		}
		return xml.Replace("<", "&lt;").Replace(">", "&gt;");
	}
	
	public static void EnumerateDictionary<T1, T2>(Dictionary<T1, T2> dict, System.Action<T1, T2> action)
	{
		foreach(KeyValuePair<T1, T2> keyValue in dict)
		{
			action(keyValue.Key, keyValue.Value);
		}
	}	
	
	public static string StringJoin<T>( this T[] array, string separator )
	{
		if( array == null )
		{
			return "NULL";
		}

		System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();

		for( int x = 0, max = array.Length; x < max; x++ )
		{
			stringBuilder.Append( array[x].ToString() );
			if( x != max - 1 )
			{
				stringBuilder.Append( separator );
			}
		}

		return stringBuilder.ToString();
	}

	public static string StringJoin<T>( this List<T> array, string separator )
	{
		return array.ToArray().StringJoin( separator );
	}

	public static void hardDestroyGameObject( ref GameObject objectToDestroy )
	{
		// Destroy the offending object.
		if( Application.isPlaying )
		{
			GameObject.Destroy( objectToDestroy );
		}
		else
		{
			GameObject.DestroyImmediate( objectToDestroy );
		}
		
		// Then null.
		objectToDestroy = null;
		
		// Re assign the memory.
		objectToDestroy = new GameObject();
		
		// Destroy the new object used to reassign memory.
		if( Application.isPlaying )
		{
			GameObject.Destroy( objectToDestroy );
		}
		else
		{
			GameObject.DestroyImmediate( objectToDestroy );
		}
		
		// Declare the variable null again to make sure.
		objectToDestroy = null;
	}		
	
	
	//-------------------------------------------------------------------------
	public static string GetFullName( this GameObject gob, char separator = '/' )
	{
		return (gob.transform.parent != null) ? gob.transform.parent.gameObject.GetFullName( separator ) + separator + gob.name : separator + gob.name;
	}
	
	//-------------------------------------------------------------------------
	public static string GetFullName( this MonoBehaviour bhv, char separator = '/' )
	{
		return bhv.gameObject.GetFullName( separator );
	}
	
	//-------------------------------------------------------------------------
	public static ComponentT EnsureComponent<ComponentT>( this GameObject obj, System.Action<ComponentT> onNewComponent = null ) where ComponentT : Component
	{
		ComponentT	c = obj.GetComponent<ComponentT>();
		
		if ( c == null )
		{
			c = obj.AddComponent<ComponentT>();
			if ( onNewComponent != null )
			{
				onNewComponent( c );
			}
		}
		
		return c;
	}
	
	//-------------------------------------------------------------------------
	public static ComponentT EnsureComponent<ComponentT>( this MonoBehaviour bhv, System.Action<ComponentT> onNewComponent = null ) where ComponentT : Component
	{
		return bhv.gameObject.EnsureComponent( onNewComponent );
	}
	
	public static void Add<T>(this List<T> list, params T[] elements)
	{
		list.AddRange(elements);
	}

	
	public static Vector3 LocalTransformPoint( this Transform t, Vector3 p )
	{
		return t.localRotation * ( t.localPosition + Vector3.Scale( t.localScale, p ) );
	}
	
	public static Vector3 LocalTransformVector( this Transform t, Vector3 v )
	{
		return t.localRotation * Vector3.Scale( t.localScale, v );
	}

	public static Vector3 LocalTransformDirection( this Transform t, Vector3 d )
	{
		return t.localRotation * d;
	}

	public static Vector2 Divide(this Vector2 targ, Vector2 other)
	{
		targ.x /= other.x;
		targ.y /= other.y;
		 return targ;
	}

	public static Vector2 Multiply(this Vector2 targ, Vector2 other)
	{
		targ.x *= other.x;
		targ.y *= other.y;
		return targ;
	}

	public static Rect GetScreenSpaceRect( this RectTransform rt, bool isZeroBottom = false )
	{
		Vector3[]	worldCorners = new Vector3[4];
		
		rt.GetWorldCorners( worldCorners );

		Vector3 min = Vector3.Min( worldCorners[0], Vector3.Min( worldCorners[1], Vector3.Min( worldCorners[2], worldCorners[3] ) ) );
		Vector3 max = Vector3.Max( worldCorners[0], Vector3.Max( worldCorners[1], Vector3.Max( worldCorners[2], worldCorners[3] ) ) );

		Canvas	canvas = rt.GetComponentInParent<Canvas>();

		if ( canvas != null && canvas.renderMode == RenderMode.ScreenSpaceCamera && canvas.worldCamera != null )
		{
			min = canvas.worldCamera.WorldToScreenPoint( min );
			max = canvas.worldCamera.WorldToScreenPoint( max );
		}

		return new Rect( min.x, min.y, max.x - min.x, max.y - min.y );
	}
	
	public static TComponentToReturn Instantiate<TComponentToReturn>(this UnityEngine.Object o, GameObject original) where TComponentToReturn : Component
	{
		GameObject obj = (GameObject)UnityEngine.Object.Instantiate(original);
		
		TComponentToReturn ret = obj.GetComponent<TComponentToReturn>();
		
		if (!ret) {
			Debug.LogError(string.Format("Object \"{0}\" does not have component \"{1}\" attached", obj, (typeof(TComponentToReturn).ToString())));
			return null;
		}
		
		return ret;
		
	}
	
	
	public static TComponentToReturn Instantiate<TComponentToReturn>(this UnityEngine.Object o, string prefabName) where TComponentToReturn : Component
	{
		GameObject prefab = Resources.Load<GameObject>(prefabName);
		
		if (!prefab) {
			Debug.LogError(string.Format("Couldn't load prefab \"{0}\" from resources", prefabName));
			return null;
		}
		
		return o.Instantiate<TComponentToReturn>(prefab);
	}
	
	
	
	
	public static string ToRGBHash(this Color color)
	{
		int r = (int)(color.r * 255);
		int g = (int)(color.g * 255);
		int b = (int)(color.b * 255);
		return "#"+r.ToString("X2")+g.ToString("X2")+b.ToString("X2");
	}
	
	
	
	public delegate void DInvokedFunction();
	
	//----------------------------------------------------------------------------------------------------------------
	public static void Invoke(this MonoBehaviour mb, DInvokedFunction method, float time)
	{
		mb.Invoke(((DInvokedFunction)method).Method.Name, time);
	}
	
	
	//----------------------------------------------------------------------------------------------------------------
	public static void Invoke_TimeScaleIndependent(this MonoBehaviour mb, DInvokedFunction method, float time)
	{
		mb.StartCoroutine(_Invoke_TimeScaleIndependent(method, time));
	}
	
	private static IEnumerator _Invoke_TimeScaleIndependent(DInvokedFunction method, float time)
	{
		float startTime = Time.realtimeSinceStartup;
		
		while (Time.realtimeSinceStartup < startTime + time) {
			yield return null;
		}
		
		method();
	}
	
	
	//----------------------------------------------------------------------------------------------------------------
	public static void Invoke_AfterFrames(this MonoBehaviour mb, DInvokedFunction method, int frames)
	{
		mb.StartCoroutine(_Invoke_AfterFrames(method, frames));
	}
	
	private static IEnumerator _Invoke_AfterFrames(DInvokedFunction method, int frames)
	{
		int startFrame = Time.frameCount;
		
		while (Time.frameCount < startFrame + frames) {
			yield return null;
		}
		
		method();
	}
	
	//----------------------------------------------------------------------------------------------------------------
	public static void InvokeRepeating(this MonoBehaviour mb, DInvokedFunction method, float time, float repeatRate)
	{
		mb.InvokeRepeating(((DInvokedFunction)method).Method.Name, time, repeatRate);
	}
	
	public static void InvokeRepeating(this MonoBehaviour mb, DInvokedFunction method, float interval)
	{
		mb.InvokeRepeating(method, interval, interval);
	}
	
	//----------------------------------------------------------------------------------------------------------------
	public static void CancelInvoke(this MonoBehaviour mb, DInvokedFunction method)
	{
		mb.CancelInvoke(((DInvokedFunction)method).Method.Name);
	}
	
	//----------------------------------------------------------------------------------------------------------------
	public static WebRequestType HttpPost(string url, Dictionary<string, string> post)
	{
		WWWForm form = new WWWForm();
		foreach(KeyValuePair<String,String> post_arg in post)
		{
			form.AddField(post_arg.Key, post_arg.Value);
		}
		#if USE_UNITY_WEBREQUEST
		UnityWebRequest	newRequest = UnityWebRequest.Post( url, form );
		return newRequest;
		#else
		WWW www = new WWW(url, form);
		return www; 
		#endif
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool IsArrayOrList( this Type targetType )
	{
		if ( typeof( Array ).IsAssignableFrom( targetType ) ) return true;
		if ( typeof( IList ).IsAssignableFrom( targetType ) ) return true;

		return false;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static int GetArrayOrListSize( this Type targetType, object target )
	{
		if ( typeof( Array ).IsAssignableFrom( targetType ) ) return ( ( Array )target ).Length;
		if ( typeof( IList ).IsAssignableFrom( targetType ) ) return ( ( IList )target ).Count;

		return 0;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static object GetArrayOrListElement( this Type targetType, object target, int index )
	{
		if ( typeof( Array ).IsAssignableFrom( targetType ) ) return ( ( Array )target ).GetValue( index );
		if ( typeof( IList ).IsAssignableFrom( targetType ) ) return ( ( IList )target )[ index ];

		return null;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void SetArrayOrListElement( this Type targetType, object target, int index, object value )
	{
		if ( typeof( Array ).IsAssignableFrom( targetType ) ) ( ( Array )target ).SetValue( value, index );
		if ( typeof( IList ).IsAssignableFrom( targetType ) ) ( ( IList )target )[ index ] = value;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool IsArrayOrList( this object target )
	{
		Type	targetType = target.GetType();

		return targetType.IsArrayOrList();
	}

	//----------------------------------------------------------------------------------------------------------------
	public static int GetArrayOrListSize( this object target )
	{
		Type	targetType = target.GetType();

		return targetType.GetArrayOrListSize( target );
	}

	//----------------------------------------------------------------------------------------------------------------
	public static object GetArrayOrListElement( this object target, int index )
	{
		Type	targetType = target.GetType();

		return targetType.GetArrayOrListElement( target, index );
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void SetArrayOrListElement( this object target, int index, object value )
	{
		Type	targetType = target.GetType();

		targetType.SetArrayOrListElement( target, index, value );
	}

	//----------------------------------------------------------------------------------------------------------------
	public static Type GetArrayOrListElementType( this Type arrayListType )
	{
		if ( arrayListType.IsArray )
		{
			return arrayListType.GetElementType();
		}

		if ( typeof( IList ).IsAssignableFrom( arrayListType ) )
		{
			Type	elementType = arrayListType.GetElementType();

			if ( elementType == null )
			{
				Type[]	genericTypes = arrayListType.GetGenericArguments();

				if ( genericTypes.Length > 0 )
				{
					elementType = genericTypes[0];
				}
			}

			return elementType;
		}

		return null;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool HasFieldOrProperty(this object target, string name, System.Predicate<FieldInfo> isMatchField = null, System.Predicate<PropertyInfo> isMatchProperty = null)
	{
		return target.GetType().HasFieldOrProperty( name, isMatchField, isMatchProperty );
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool HasFieldOrProperty(this Type targetType, string name, System.Predicate<FieldInfo> isMatchField = null, System.Predicate<PropertyInfo> isMatchProperty = null)
	{
		// look for field
		FieldInfo fi = GetFieldInfo( targetType, name, 
						BindingFlags.Instance |  
						BindingFlags.NonPublic | 
						BindingFlags.Public );

		if (fi != null && (isMatchField == null || isMatchField( fi ) == true)) {
			return true;
		}

		// look for property
		PropertyInfo pi = GetPropertyInfo( targetType, name, 
						BindingFlags.Instance |  
						BindingFlags.NonPublic | 
						BindingFlags.Public );

		if (pi != null && (isMatchProperty == null || isMatchProperty( pi ) == true)) {
			return true;
		}

		return false;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool HasFieldOrProperty(this object target, string name, Type type = null)
	{
		return target.GetType().HasFieldOrProperty( name, type );
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool HasFieldOrProperty(this Type targetType, string name, Type type = null)
	{
		return targetType.HasFieldOrProperty( name, fi => type == null || fi.FieldType == type, pi => type == null || pi.PropertyType == type );
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void SetMember( System.Type memberType, object memberCurr, System.Action<object> setMember, object value, System.Action<string> onError = null )
	{
		System.Type	valueType = value != null ? value.GetType() : null;

		if ( valueType != null && typeof( Array ).IsAssignableFrom( valueType ) )
		{
			Array	valueArray = (Array)value;

			if ( typeof( Array ).IsAssignableFrom( memberType ) )
			{
				Array	memberArray = (Array)valueArray.Clone();
				setMember( memberArray );
			}
			else if ( typeof( IList ).IsAssignableFrom( memberType ) )
			{
				IList	memberList = (IList)memberCurr;
				memberList.Clear();
				for ( int i = 0; i < valueArray.Length; ++i )
				{
					memberList.Add( valueArray.GetValue(i) );
				}
			}
			else
			{
				if ( onError != null )
				{
					onError( "Member is not array/list!" );
				}
			}
		}
		else if ( valueType != null && typeof( IList ).IsAssignableFrom( valueType ) )
		{
			IList	valueList = (IList)value;

			if ( typeof( Array ).IsAssignableFrom( memberType ) )
			{
				Array memberArray = Array.CreateInstance( memberType.GetArrayOrListElementType(), valueList.Count );
				valueList.CopyTo( memberArray, 0 );
				setMember( memberArray );
			}
			else if ( typeof( IList ).IsAssignableFrom( memberType ) )
			{
				IList	memberList = (IList)memberCurr;
				memberList.Clear();
				for ( int i = 0; i < valueList.Count; ++i )
				{
					memberList.Add( valueList[i] );
				}
			}
			else
			{
				if ( onError != null )
				{
					onError( "Member is not array/list!" );
				}
			}
		}
		else
		{
			setMember( value );
		}
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void SetFieldOrProperty(this object target, string name, object value, bool logErrorIfNotFound = false)
	{
		//System.Type	valueType = value != null ? value.GetType() : null;

		// look for field
		FieldInfo fi = GetFieldInfo( target.GetType(), name, 
						BindingFlags.Instance |  
						BindingFlags.NonPublic | 
						BindingFlags.Public );
		if (fi != null)
		{
			SetMember( fi.FieldType, fi.GetValue( target ), v => fi.SetValue( target, v ), value, err => {
				if (logErrorIfNotFound)
				{
					Debug.LogError( err + ", Field=" + name + ", Target=" + target );
				}
			} );
			return;
		}

		// look for property
		PropertyInfo pi = GetPropertyInfo( target.GetType(), name, 
						BindingFlags.Instance |  
						BindingFlags.NonPublic | 
						BindingFlags.Public );
		if (pi != null)
		{
			SetMember( pi.PropertyType, pi.GetValue( target, null ), v => pi.SetValue( target, v, null ), value, err => {
				if (logErrorIfNotFound)
				{
					Debug.LogError( err + ", Field=" + name + ", Target=" + target );
				}
			} );
			return;
		}

		// couldn't find either
		if (logErrorIfNotFound) {
			Debug.LogError(string.Format("Property \"{0}\" not found on {1}!", name, target));
		}
	}

	//----------------------------------------------------------------------------------------------------------------
	public static object GetFieldOrProperty(this object target, string name, bool logErrorIfNotFound = false)
	{
		// look for field
		FieldInfo fi = GetFieldInfo( target.GetType(), name, 
						BindingFlags.Instance |  
						BindingFlags.NonPublic | 
						BindingFlags.Public );
		if (fi != null) {
			return fi.GetValue(target);
		}

		// look for property
		PropertyInfo pi = GetPropertyInfo( target.GetType(), name, 
						BindingFlags.Instance |  
						BindingFlags.NonPublic | 
						BindingFlags.Public );
		if (pi != null) {
			return pi.GetValue(target, null);
		}

		// couldn't find either
		if (logErrorIfNotFound) {
			Debug.LogError(string.Format("Property \"{0}\" not found on {1}!", name, target));
		}
		return null;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static Type GetFieldOrPropertyType(this object target, string name, bool logErrorIfNotFound = false)
	{
		return target.GetType().GetFieldOrPropertyType( name, logErrorIfNotFound );
	}

	//----------------------------------------------------------------------------------------------------------------
	public static Type GetFieldOrPropertyType(this System.Type targetType, string name, bool logErrorIfNotFound = false)
	{
		// look for field
		FieldInfo fi = GetFieldInfo( targetType, name, 
						BindingFlags.Instance |  
						BindingFlags.NonPublic | 
						BindingFlags.Public );
		if (fi != null) {
			return fi.FieldType;
		}

		// look for property
		PropertyInfo pi = GetPropertyInfo( targetType, name, 
						BindingFlags.Instance |  
						BindingFlags.NonPublic | 
						BindingFlags.Public );
		if (pi != null) {
			return pi.PropertyType;
		}

		// couldn't find either
		if (logErrorIfNotFound) {
			Debug.LogError(string.Format("Property \"{0}\" not found on type {1}!", name, targetType));
		}
		return null;
	}

	//----------------------------------------------------------------------------------------------------------------
	private static FieldInfo GetFieldInfo( System.Type classType, string fieldName, BindingFlags bindingFlags )
	{
		FieldInfo	fieldInfo = null;

		System.Type	baseClassType = classType.BaseType;

		if ( baseClassType != null )
		{
			fieldInfo = GetFieldInfo( baseClassType, fieldName, bindingFlags );

			if ( fieldInfo != null ) return fieldInfo;
		}

		fieldInfo = classType.GetField( fieldName, bindingFlags | BindingFlags.DeclaredOnly );

		return fieldInfo;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static PropertyInfo GetPropertyInfo( System.Type classType, string propertyName, BindingFlags bindingFlags )
	{
		PropertyInfo	propertyInfo = null;

		System.Type	baseClassType = classType.BaseType;

		if ( baseClassType != null )
		{
			propertyInfo = GetPropertyInfo( baseClassType, propertyName, bindingFlags );

			if ( propertyInfo != null ) return propertyInfo;
		}

		propertyInfo = classType.GetProperty( propertyName, bindingFlags | BindingFlags.DeclaredOnly );

		return propertyInfo;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static Rect Translate(this Rect target, float x, float y)
	{
		return new Rect(
			target.xMin + x,
			target.yMin + y,
			target.width,
			target.height
		);
	}

	//----------------------------------------------------------------------------------------------------------------
	public static Rect Translate(this Rect target, Vector2 offset)
	{
		return new Rect(
			target.xMin + offset.x,
			target.yMin + offset.y,
			target.width,
			target.height
		);
	}

	//----------------------------------------------------------------------------------------------------------------
	public static Rect Combine( this Rect thisRect, Rect otherRect )
	{
		float	minX = Mathf.Min( thisRect.xMin, otherRect.xMin );
		float	minY = Mathf.Min( thisRect.yMin, otherRect.yMin );
		float	maxX = Mathf.Max( thisRect.xMax, otherRect.xMax );
		float	maxY = Mathf.Max( thisRect.yMax, otherRect.yMax );

		return new Rect( minX, minY, maxX - minX, maxY - minY );
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool IsPointInFrustrum(this Camera targ, Vector3 point, float normBorder = 0)
	{
		Vector3 viewportPos = targ.WorldToViewportPoint( point );

		return
			viewportPos.x > normBorder &&
			viewportPos.x < 1 - normBorder &&
			viewportPos.y > normBorder &&
			viewportPos.y < 1 - normBorder &&
			viewportPos.z > 0;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool AreBoundsInView(this Camera targ, Bounds bounds)
	{
		return GeometryUtility.TestPlanesAABB(
			GeometryUtility.CalculateFrustumPlanes(targ),
			bounds
		);
	}

	//----------------------------------------------------------------------------------------------------------------
	public static Bounds WorldToScreenBounds( this Camera cam, Bounds bounds )
	{
		Bounds	resultBounds = new Bounds( cam.WorldToScreenPoint( bounds.center ), Vector3.zero );
		bounds.ForEachCorner( c => resultBounds.Encapsulate( cam.WorldToScreenPoint( c ) ) );
		return resultBounds;
	}

	//----------------------------------------------------------------------------------------------------------------
	public static void ForEachCorner( this Bounds bounds, System.Action<Vector3> action )
	{
		Vector3	centre = bounds.center;
		Vector3	extent = bounds.extents;

		action( centre + extent );//1,1,1

		extent.x = -extent.x;
		action( centre + extent );//-1,1,1

		extent.y = -extent.y;
		action( centre + extent );//-1,-1,1

		extent.x = -extent.x;
		action( centre + extent );//1,-1,1

		extent.z = -extent.z;
		action( centre + extent );//1,-1,-1

		extent.y = -extent.y;
		action( centre + extent );//1,1,-1

		extent.x = -extent.x;
		action( centre + extent );//-1,1,-1

		extent.y = -extent.y;
		action( centre + extent );//-1,-1,-1
	}

	//----------------------------------------------------------------------------------------------------------------
	public static float SqrMagnitudeXZ(this Vector3 targ)
	{
		return (targ.x * targ.x) + (targ.z * targ.z);
	}

	//----------------------------------------------------------------------------------------------------------------
	public static float MagnitudeXZ(this Vector3 targ)
	{
		return Mathf.Sqrt((targ.x * targ.x) + (targ.z * targ.z));
	}

	//----------------------------------------------------------------------------------------------------------------
	public static bool Is(this Type target, Type type)
	{
		if (target.BaseType == type) {
			return true;
		}
		if (target.BaseType == null) {
			return false;
		}
		return target.BaseType.Is(type);
	}


	public static Vector3 WrapAngles(this Vector3 v)
	{
		v.x = MathHelper.Wrap(v.x, -180, 180);
		v.y = MathHelper.Wrap(v.y, -180, 180);
		v.z = MathHelper.Wrap(v.z, -180, 180);
		return v;
	}


	/// <summary>
	/// Returns all rigidbodies in the scene that are joined to the target,
	/// either directly, or via a chain of other rigidbodies
	/// </summary>
	public static List<Rigidbody> GetConnectedBodies(this Rigidbody targ, int layerMask = ~0)
	{
		// Find all joints and rigidbodies in current scene
		List<Joint> jointsToCheck;
		List<Rigidbody> rigidbodiesToCheck;
		PhysicsHelper.GetJointsAndTheirRigidbodies(out jointsToCheck, out rigidbodiesToCheck, layerMask);
		
		List<Rigidbody> connectedBodies = new List<Rigidbody>();

		_GetConnectedBodies(
			targ,
			connectedBodies,
			jointsToCheck,
			rigidbodiesToCheck
		);

		connectedBodies.Remove(targ);

		return connectedBodies;
	}



	private static void _GetConnectedBodies(Rigidbody targ, List<Rigidbody> connectedBodies, List<Joint> jointsToCheck, List<Rigidbody> rigidbodiesToCheck)
	{
		for (int i = jointsToCheck.Count - 1; i >= 0; --i)
		{
			// Ensure i is in range (list may have been shortened during recursions)
			while (i >= jointsToCheck.Count) {
				--i;
				if (i < 0) return;
			}

			Joint jointToCheck = jointsToCheck[i];
			Rigidbody rigidbodyToCheck = rigidbodiesToCheck[i];
			
			// Check if joint is a child (down tree)
			if (jointToCheck.connectedBody == targ)
			{
				connectedBodies.Add(rigidbodyToCheck);
				jointsToCheck.Remove(jointToCheck);
				rigidbodiesToCheck.Remove(rigidbodyToCheck);
				_GetConnectedBodies(rigidbodyToCheck, connectedBodies, jointsToCheck, rigidbodiesToCheck);
			}

			// If joint belongs to targ, iterate on its connected body (up tree)
			if (rigidbodyToCheck == targ &&
				jointToCheck.connectedBody != null)
			{
				connectedBodies.Add(jointToCheck.connectedBody);
				jointsToCheck.Remove(jointToCheck);
				rigidbodiesToCheck.Remove(rigidbodyToCheck);
				_GetConnectedBodies(jointToCheck.connectedBody, connectedBodies, jointsToCheck, rigidbodiesToCheck);
			}
		}
	}


	///
	/// <summary> Moves the target, and any bodies connected to it via joints </summary>
	/// <param name="layerMask"> Optimisation: only the bodies connected to targ that are on these layers will be moved along with targ </param>
	/// <returns> A list of all the bodies that were moved </returns>
	/// 
	public static List<Rigidbody> MoveTo(this Rigidbody targ, Vector3 position, Quaternion rotation, int layerMask = ~0)
	{
		List<Rigidbody> connectedBodies = targ.GetConnectedBodies(layerMask);

		Vector3[] relativePositions = new Vector3[connectedBodies.Count];
		Quaternion[] relativeRotations = new Quaternion[connectedBodies.Count];
		
		for (int i = connectedBodies.Count - 1; i >= 0; --i)
		{
			relativePositions[i] = targ.transform.InverseTransformPoint(connectedBodies[i].transform.position);
			relativeRotations[i] = MathHelper.GetRelativeQuaternion(targ.transform.rotation, connectedBodies[i].rotation);
		}

		targ.transform.position = position;
		targ.transform.rotation = rotation;

		for (int i = connectedBodies.Count - 1; i >= 0; --i)
		{
			connectedBodies[i].transform.position = targ.transform.TransformPoint(relativePositions[i]);
			connectedBodies[i].transform.rotation = targ.transform.rotation * relativeRotations[i];
		}

		// Make joints indestructible for a few frames while physics settle
		// HACK: attach coroutine to any gameobject in the scene
		MonoBehaviour mb = GameObject.FindObjectOfType<MonoBehaviour>();
		mb.StartCoroutine(SetJointIndestructible(connectedBodies, 2));

		return connectedBodies;
	}



	private static IEnumerator SetJointIndestructible(List<Rigidbody> bodies, int forFrames)
	{
		Joint[] joints = new Joint[bodies.Count];
		Vector2[] breakForces = new Vector2[joints.Length];

		for (int i = joints.Length - 1; i >= 0; --i)
		{
			joints[i] = bodies[i].GetComponent<Joint>();
			if (joints[i] == null) continue;
			breakForces[i].Set(joints[i].breakForce, joints[i].breakTorque);
			joints[i].breakForce = float.PositiveInfinity;
			joints[i].breakTorque = float.PositiveInfinity;
		}

		while (--forFrames >= 0) {
			yield return new WaitForFixedUpdate();
		}

		for (int i = joints.Length - 1; i >= 0; --i) {
			if (joints[i]== null) continue;
			joints[i].breakForce = breakForces[i].x;
			joints[i].breakTorque = breakForces[i].y;
		}
	}



	public static void SetLayerRecursively(this GameObject targ, int layer)
	{
		Transform[] children = targ.GetComponentsInChildren<Transform>(true);

		if ( layer < 0 || layer > 31 ) Debug.Log( "Layer: " + layer );

		for (int i = children.Length - 1; i >= 0; --i)
		{
			children[i].gameObject.layer = layer;
		}
	}

	public static void SetTagRecursively(this GameObject targ, string tag)
	{
		Transform[] children = targ.GetComponentsInChildren<Transform>(true);

		for (int i = children.Length - 1; i >= 0; --i)
		{
			children[i].gameObject.tag = tag;
		}
	}



	public static void Draw(this GameObject targ, Transform transform, Camera camera = null, int layer = -1)
	{
		Draw(targ, transform.localToWorldMatrix, camera, layer);
	}



	public static void Draw(this GameObject targ, Vector3 position, Quaternion rotation, Vector3 scale, Camera camera = null, int layer = -1)
	{
		Draw(targ, Matrix4x4.TRS(position, rotation, scale), camera, layer);
	}



	public static void Draw(this GameObject targ, Matrix4x4 matrix, Camera camera = null, int layer = -1)
	{
		if (targ == null) {
			throw new NullReferenceException();
		}
		
		// Draw MeshRenderers
		MeshRenderer[] meshRenderers = targ.GetComponentsInChildren<MeshRenderer>();

		for (int meshRendererIndex = meshRenderers.Length - 1; meshRendererIndex >= 0; --meshRendererIndex)
		{
			MeshRenderer mr = meshRenderers[meshRendererIndex];
			MeshFilter mf = mr.GetComponent<MeshFilter>();

			Matrix4x4 meshToTargMatrix = targ.transform.worldToLocalMatrix * mr.transform.localToWorldMatrix;
			
			for (int submeshIndex = mf.sharedMesh.subMeshCount - 1; submeshIndex >= 0; --submeshIndex)
			{
				Graphics.DrawMesh(
					mesh:			mf.sharedMesh,
					matrix:			matrix * meshToTargMatrix,
					material:		mr.sharedMaterials[submeshIndex],
					layer:			layer == -1 ? mr.gameObject.layer : layer,
					camera:			camera,
					submeshIndex:	submeshIndex
				);
			}
		}


		// Draw SkinnedMeshRenderers
		SkinnedMeshRenderer[] skinnedMeshRenderers = targ.GetComponentsInChildren<SkinnedMeshRenderer>();

		for (int skinnedMeshRendererIndex = skinnedMeshRenderers.Length - 1; skinnedMeshRendererIndex >= 0; --skinnedMeshRendererIndex)
		{
			SkinnedMeshRenderer smr = skinnedMeshRenderers[skinnedMeshRendererIndex];

			Matrix4x4 meshToTargMatrix = targ.transform.worldToLocalMatrix * smr.transform.localToWorldMatrix;
			
			for (int submeshIndex = smr.sharedMesh.subMeshCount - 1; submeshIndex >= 0; --submeshIndex)
			{
				Graphics.DrawMesh(
					mesh:			smr.sharedMesh,
					matrix:			matrix * meshToTargMatrix,
					material:		smr.sharedMaterials[submeshIndex],
					layer:			smr.gameObject.layer,
					camera:			null,
					submeshIndex:	submeshIndex
				);
			}
		}
	}



	public static Transform FindChildDeep(this Transform targ, string name)
	{
		Transform child = targ.Find(name);

		if (child)
		{
			return child;
		}
		else
		{
			for (int i = 0; i < targ.childCount; ++i)
			{
				Transform child2 = targ.GetChild(i).FindChildDeep(name);
				if (child2) return child2;
			}
		}

		return null;
	}



	public static Bounds GetLocalMeshBounds(this GameObject targ)
	{
		Bounds b = new Bounds();

		MeshFilter[] meshFilters = targ.GetComponentsInChildren<MeshFilter>();

		for (int i = meshFilters.Length - 1; i >= 0; --i)
		{
			b.Encapsulate(
				TransformBounds(
					targ.transform.worldToLocalMatrix * meshFilters[i].transform.localToWorldMatrix,
					meshFilters[i].sharedMesh.bounds
				)
			);
		}

		return b;
	}



	public static Vector3 TransformExtents( Matrix4x4 m, Vector3 extents )
	{
		Vector3		resultExtents;
		
		resultExtents.x = Mathf.Abs( m.m00 * extents.x ) + Mathf.Abs( m.m01 * extents.y ) + Mathf.Abs( m.m02 * extents.z );
		resultExtents.y = Mathf.Abs( m.m10 * extents.x ) + Mathf.Abs( m.m11 * extents.y ) + Mathf.Abs( m.m12 * extents.z );
		resultExtents.z = Mathf.Abs( m.m20 * extents.x ) + Mathf.Abs( m.m21 * extents.y ) + Mathf.Abs( m.m22 * extents.z );
		
		return resultExtents;
	}



	public static Bounds TransformBounds( Matrix4x4 m, Bounds bounds )
	{
		Bounds	resultBounds = bounds;
		resultBounds.extents = TransformExtents( m, bounds.extents );
		resultBounds.center = m.MultiplyPoint3x4( bounds.center );
		return resultBounds;
	}



	public static bool IsInfrontOf(this Component targ, Component other, float distanceOffset = 0)
	{
		return Vector3.Dot(other.transform.forward, targ.transform.position - other.transform.position) > distanceOffset;
	}



	public static bool IsInView(this Camera targ, Vector3 worldPoint)
	{
		Vector3 viewportPoint = targ.WorldToViewportPoint(worldPoint);

		return
			viewportPoint.z > 0 &&
			viewportPoint.x > 0 &&
			viewportPoint.x < 1 &&
			viewportPoint.y > 0 &&
			viewportPoint.y < 1;
	}

	//Processes list of ints as it they are triangle vertex indices.
	//For each triangle, calls action( triIndex, vertIndex1, vertIndex2, vertIndex3 )
	public static void ForEachTriangle( this IList<int> triangles, System.Action<int, int, int, int> action )
	{
		if( action == null ) return;
		if( triangles == null ) return;
		if( triangles.Count == 0 ) return;

		for( int i = 0; i < triangles.Count - 2; i += 3 )
		{
			action( i / 3, triangles[i], triangles[i + 1], triangles[i + 2] );
		}
	}

	public static int FindTriangle( this IList<int> triangles, System.Func<int, int, int, int, bool> match )
	{
		if( match == null ) return -1;
		if( triangles == null ) return -1;
		if( triangles.Count == 0 ) return -1;

		for( int i = 0; i < triangles.Count - 2; i += 3 )
		{
			if ( match( i / 3, triangles[i], triangles[i + 1], triangles[i + 2] ) == true ) return ( i / 3 );
		}

		return -1;
	}

	//CC20141107
	//Uses Fisher-Yates shuffle algorithm
	public static void Shuffle<T>( this IList<T> list )
	{
		int j;
		T val;
		for( int i = list.Count - 1; i > 0; --i )
		{
			j = UnityEngine.Random.Range( 0, i );
			val = list[j];
			list[j] = list[i];
			list[i] = val;
		}
	}
}
