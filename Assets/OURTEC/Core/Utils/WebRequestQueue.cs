﻿////////////////////////////////////////////
// 
// WebRequestQueue.cs
//
// Created: 29/03/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using LoggerAPI = UnityEngine.Debug;

namespace AmuzoEngine
{

public class WebRequestQueue
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[WebRequestQueue] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	private ActionQueue	_actionQueue;

	private int	_maxConcurrent = 1;

	private System.Func<bool>	_canStartNextRequest = null;

	private bool	_isPaused;

	private BooleanStateRequests	_isPausedRequests;
	
	//
	// PROPERTIES
	//

	public event System.Action<int>	_pOnRequestSent;

	public event System.Action<int>	_pOnResponseReceived;

	public event System.Action	_pOnAllResponsesReceived;

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public WebRequestQueue( string name )
	{
		_actionQueue = new ActionQueue( name );

		_actionQueue._pOnActionStarted += downloadId => {
			if ( _pOnRequestSent != null )
			{
				_pOnRequestSent( downloadId );
			}
		};

		_actionQueue._pOnActionEnded += downloadId => {
			if ( _pOnResponseReceived != null )
			{
				_pOnResponseReceived( downloadId );
			}
		};

		_actionQueue._pOnAllActionsEnded += () => {
			if ( _pOnAllResponsesReceived != null )
			{
				_pOnAllResponsesReceived();
			}
		};

		_canStartNextRequest = () => _maxConcurrent < 0 || _actionQueue._pActiveCount < _maxConcurrent;

		_isPausedRequests = new BooleanStateRequests( newState => _isPaused = newState, () => _isPaused, false );
	}

	public void SetMaxConcurrentDownloads( int maxConcurrent )
	{
		_maxConcurrent = maxConcurrent;
	}

	public int SendRequest( IWebRequest request, object groupId = null )
	{
		return AddRequest( request, ActionQueue.EActionStart.LAST, groupId );
	}

	public int SendRequestNext( IWebRequest request, object groupId = null )
	{
		return AddRequest( request, ActionQueue.EActionStart.NEXT, groupId );
	}

	public int SendRequestNow( IWebRequest request, object groupId = null )
	{
		return AddRequest( request, ActionQueue.EActionStart.NOW, groupId );
	}

	public void CancelRequest( int downloadId )
	{
		_actionQueue.CancelAction( downloadId );
	}

	public void CancelRequests( object groupId = null )
	{
		if ( groupId == null )
		{
			_actionQueue.Reset();
		}
		else
		{
			_actionQueue.CancelActions( groupId );
		}
	}

	public bool IsAllResponsesReceived( object groupId = null )
	{
		return groupId == null ? _actionQueue._pIsAllActionsEnded : _actionQueue.IsAllActionsEnded( groupId );
	}

	public void Update()
	{
		if ( !_isPaused )
		{
			_actionQueue.DoActions( _canStartNextRequest );
		}
	}

	public void PauseRequests( bool wantPaused, object requester )
	{
		_isPausedRequests.RequestState( wantPaused, requester, 0, true );
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private int AddRequest( IWebRequest request, ActionQueue.EActionStart start, object groupId )
	{
		return _actionQueue.AddAction( () => request.Send(), onActionAdded:downloadId => {
			request._pOnResult += () => _actionQueue.OnActionComplete( downloadId );
		}, isAsyncAction:true, name:request.ToString(), groupId:groupId, start:start );
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

}
