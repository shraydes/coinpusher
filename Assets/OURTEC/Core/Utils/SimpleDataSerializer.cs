﻿////////////////////////////////////////////
// 
// SimpleDataSerializer.cs
//
// Created: 05/08/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System.Text;

public class SimpleDataSerializer
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[SimpleDataSerializer] ";

	private const char		DEFAULT_RECORD_SEPARATOR = '#';
	private const char		DEFAULT_PAIR_SEPARATOR = ';';
	private const char		DEFAULT_KEY_VALUE_SEPARATOR = '=';
	private const char		INVALID_SEPARATOR = '\0';

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	private char	_recordSeparator = DEFAULT_RECORD_SEPARATOR;

	private char	_pairSeparator = DEFAULT_PAIR_SEPARATOR;

	private char	_keyValueSeparator = DEFAULT_KEY_VALUE_SEPARATOR;

	private string	_nullValue = null;

	private StringBuilder	_sb = new StringBuilder();
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	private static void SerializeRecords( int recordCount, System.Func<int, int> getPairCount, System.Func<int, int, string> getKey, System.Func<int, int, string> getValue, char recordSeparator, char pairSeparator, char keyValueSeparator, string nullValue, StringBuilder sb )
	{
		sb.Length = 0;

		if ( getPairCount == null ) return;
		if ( getKey == null ) return;
		if ( getValue == null ) return;

		int		pairCount;
		for ( int i = 0; i < recordCount; ++i )
		{
			int	iCap = i;

			if ( i > 0 )
			{
				sb.Append( recordSeparator );
			}

			pairCount = getPairCount( i );

			SerializePairs( pairCount, pairIndex => getKey( iCap, pairIndex ), pairIndex => getValue( iCap, pairIndex ), pairSeparator, keyValueSeparator, nullValue, sb );
		}
	}

	private static void SerializePairs( int pairCount, System.Func<int, string> getKey, System.Func<int, string> getValue, char pairSeparator, char keyValueSeparator, string nullValue, StringBuilder sb )
	{
		if ( getKey == null ) return;
		if ( getValue == null ) return;

		string	key, value;
		bool	hasAddedPair = false;

		for ( int i = 0; i < pairCount; ++i )
		{
			key = getKey( i );

			if ( key == null ) continue;

			value = getValue( i );

			if ( value == null && nullValue == null ) continue;

			if ( hasAddedPair )
			{
				sb.Append( pairSeparator );
			}

			SerializeKeyValue( key, value, keyValueSeparator, nullValue, sb );

			hasAddedPair = true;
		}
	}

	private static void SerializeKeyValue( string key, string value, char keyValueSeparator, string nullValue, StringBuilder sb )
	{
		sb.Append( key );
		sb.Append( keyValueSeparator );

		if ( value == null )
		{
			sb.Append( nullValue );
		}
		else
		{
			sb.Append( value );
		}
	}

	private static void DeserializeRecords( string data, System.Action<int> setRecordCount, System.Action<int, int> setPairCount, System.Action<int, int, string, string> setPair, char recordSeparator, char pairSeparator, char keyValueSeparator, string nullValue )
	{
		string[]	records = data.Split( recordSeparator );

		if ( setRecordCount != null )
		{
			setRecordCount( records.Length );
		}

		for ( int i = 0; i < records.Length; ++i )
		{
			int	iCap = i;
			DeserializePairs( records[i], pairCount => {
				if ( setPairCount != null )
				{
					setPairCount( iCap, pairCount );
				}
			}, ( pairIndex, key, value ) => {
				setPair( iCap, pairIndex, key, value );
			}, pairSeparator, keyValueSeparator, nullValue );
		}
	}

	private static void DeserializePairs( string data, System.Action<int> setPairCount, System.Action<int, string, string> setPair, char pairSeparator, char keyValueSeparator, string nullValue )
	{
		string[]	pairs = data.Split( pairSeparator );

		if ( setPairCount != null )
		{
			setPairCount( pairs.Length );
		}

		for ( int i = 0; i < pairs.Length; ++i )
		{
			int	iCap = i;
			DeserializeKeyValue( pairs[i], ( key, value ) => setPair( iCap, key, value ), keyValueSeparator, nullValue );
		}
	}

	private static void DeserializeKeyValue( string data, System.Action<string, string> setPair, char keyValueSeparator, string nullValue )
	{
		string[] kv = data.Split( keyValueSeparator );
			
		if ( nullValue != null && kv[1] == nullValue )
		{
			setPair( kv[0], null );
		}
		else
		{
			setPair( kv[0], kv[1] );
		}
	}

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public SimpleDataSerializer()
	{
		_recordSeparator = DEFAULT_RECORD_SEPARATOR;
		_pairSeparator = DEFAULT_PAIR_SEPARATOR;
		_keyValueSeparator = DEFAULT_KEY_VALUE_SEPARATOR;
		_nullValue = null;
	}

	public SimpleDataSerializer( string nullValue )
	{
		_recordSeparator = DEFAULT_RECORD_SEPARATOR;
		_pairSeparator = DEFAULT_PAIR_SEPARATOR;
		_keyValueSeparator = DEFAULT_KEY_VALUE_SEPARATOR;
		_nullValue = nullValue;
	}

	public SimpleDataSerializer( char pairSeparator, char keyValueSeparator, string nullValue )
	{
		_recordSeparator = INVALID_SEPARATOR;
		_pairSeparator = pairSeparator;
		_keyValueSeparator = keyValueSeparator;
		_nullValue = nullValue;
	}

	public SimpleDataSerializer( char recordSeparator, char pairSeparator, char keyValueSeparator, string nullValue )
	{
		_recordSeparator = recordSeparator;
		_pairSeparator = pairSeparator;
		_keyValueSeparator = keyValueSeparator;
		_nullValue = nullValue;
	}

	public string SerializeRecords( int recordCount, System.Func<int, int> getPairCount, System.Func<int, int, string> getKey, System.Func<int, int, string> getValue )
	{
		_sb.Length = 0;
		SerializeRecords( recordCount, getPairCount, getKey, getValue, _recordSeparator, _pairSeparator, _keyValueSeparator, _nullValue, _sb );
		return _sb.ToString();
	}

	public string SerializePairs( int pairCount, System.Func<int, string> getKey, System.Func<int, string> getValue )
	{
		_sb.Length = 0;
		SerializePairs( pairCount, getKey, getValue, _pairSeparator, _keyValueSeparator, _nullValue, _sb );
		return _sb.ToString();
	}

	public void DeserializeRecords( string data, System.Action<int> setRecordCount, System.Action<int, int> setPairCount, System.Action<int, int, string, string> setPair )
	{
		DeserializeRecords( data, setRecordCount, setPairCount, setPair, _recordSeparator, _pairSeparator, _keyValueSeparator, _nullValue );
	}
	
	public void DeserializePairs( string data, System.Action<int> setPairCount, System.Action<int, string, string> setPair )
	{
		DeserializePairs( data, setPairCount, setPair, _pairSeparator, _keyValueSeparator, _nullValue );
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

