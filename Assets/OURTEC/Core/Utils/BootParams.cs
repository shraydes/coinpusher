﻿////////////////////////////////////////////
// 
// BootParams.cs
//
// Created: 25/02/2016 ccuthbert
// Contributors:
// 
// Intention:
// Class to provide boot params functionality.
// Functionality was previously in Extensions.cs
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !UNITY_2017_1_OR_NEWER
#define SUPPORT_WEBPLAYER
#endif

#if !UNITY_2018_3_OR_NEWER
#define HAS_UNITY_WWW
#endif

using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

#if HAS_UNITY_WWW
using UnityWebRequestAPI = UnityEngine.WWW;
#else
using UnityWebRequestAPI = UnityEngine.Networking.UnityWebRequest;
#endif

public static class BootParams
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[BootParams] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	public static string GetBootParam(string value, bool doValidate = true)
	{
#if !( UNITY_WEBPLAYER || UNITY_WEBGL )
		return null;
#else
#if UNITY_WEBPLAYER
		int question = Application.srcValue.IndexOf('?');
		
		if(question <= 0)
		{
			return null;
		}
		
		string queryString = Application.srcValue.Substring(question + 1);
#elif UNITY_WEBGL
		if ( !WebGLBootParams._pIsBootParamsReady )
		{
			Debug.LogError( "[GetBootParam] Boot params not ready, cannot get boot param: " + value );
			return null;
		}

		string	queryString = WebGLBootParams._pBootParams;

		if ( queryString == null )
		{
			Debug.LogError( "[GetBootParam] Failed to get boot param: " + value );
			return null;
		}
#endif
		
		Debug.Log ("Parsing querystring " + queryString);
		
		string[] bootParam = queryString.Split('&');
		
		foreach(string keyValuepair in bootParam)
		{
			string[] keyValue = keyValuepair.Split('=');
			
			if(keyValue[0] == value)
			{
				Debug.Log("Found boot param '" + value + "': " + keyValue[1]);
				return doValidate? ValidateURL( keyValue[1] ) : keyValue[1];
			}
		}
		
		return null;
#endif
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//--------------------------------------------------------------------------
#if SUPPORT_WEBPLAYER
	static string PREPEND_FILE_PATH = !Application.isWebPlayer? "file:///" : "";
#else
	static string PREPEND_FILE_PATH = "file:///";
#endif
	static string FULL_DATA_PATH = PREPEND_FILE_PATH + Application.dataPath;
	
	private static string ValidateURL(string url)
	{
		url = UnityWebRequestAPI.UnEscapeURL(url);
		
		if(!url.StartsWith("http"))
		{
			return string.Format("{0}/{1}", FULL_DATA_PATH.Replace("%20"," "), url);
		}
		
		return url;
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

