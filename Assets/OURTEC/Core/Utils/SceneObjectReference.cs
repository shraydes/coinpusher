﻿////////////////////////////////////////////
// 
// SceneObjectReference.cs
//
// Created: 03/01/2017 ccuthbert
// Contributors:
// 
// Intention:
// Attach this script to an object to add 
// a reference to that object into a global 
// list of object references, which can be 
// looked up in code.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System.Collections.Generic;
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public sealed class SceneObjectReference : MonoBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[SceneObjectReference] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//

	//
	// MEMBERS 
	//

	private static Dictionary<string, SceneObjectReference>	_sReferencesDict = new Dictionary<string, SceneObjectReference>();

	[SerializeField]
	private string	_nameOverride;

	[SerializeField]
	private Component	_component;

	private System.Type	_type;

	//
	// PROPERTIES
	//

	private string _pName
	{
		get
		{
			return string.IsNullOrEmpty( _nameOverride ) ? gameObject.name : _nameOverride;
		}
	}

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	public static GameObject FindGameObject( string name )
	{
		if ( string.IsNullOrEmpty( name ) ) return null;
		if ( !_sReferencesDict.ContainsKey( name ) ) return null;

		SceneObjectReference	@ref = _sReferencesDict[ name ];
		
		return @ref != null ? @ref.gameObject : null;
	}

	public static T FindComponent<T>( string name ) where T : Component
	{
		if ( string.IsNullOrEmpty( name ) ) return null;
		if ( !_sReferencesDict.ContainsKey( name ) ) return null;

		SceneObjectReference	@ref = _sReferencesDict[ name ];

		if ( @ref == null ) return null;

		if ( @ref._component != null )
		{
			System.Type	typeT = typeof( T );

			if ( DoesReferenceMatchType( @ref, typeT ) )
			{
				return @ref._component as T;
			}
		}

		return @ref.gameObject.GetComponent<T>();
	}

	private static void RegisterReference( SceneObjectReference @ref )
	{
		string	name = @ref._pName;

		if ( _sReferencesDict.ContainsKey( name ) )
		{
			LoggerAPI.LogError( LOG_TAG + string.Format( "Already registered reference named '{0}'", name ) );
			return;
		}

		_sReferencesDict.Add( name, @ref );
	}

	private static void UnregisterReference( SceneObjectReference @ref )
	{
		string	name = @ref._pName;

		if ( !_sReferencesDict.ContainsKey( name ) )
		{
			LoggerAPI.LogWarning( LOG_TAG + string.Format( "Reference named '{0}' not registered", name ) );
			return;
		}

		_sReferencesDict.Remove( name );
	}

	private static bool DoesReferenceMatchType( SceneObjectReference @ref, System.Type type )
	{
		return type.IsAssignableFrom( @ref._type );
	}

	private static bool DoesReferenceExactlyMatchType( SceneObjectReference @ref, System.Type type )
	{
		return @ref._type == type;
	}

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void Awake()
	{
		Initialize();

		RegisterReference( this );
	}

	void OnDestroy()
	{
		UnregisterReference( this );
	}

	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private void Initialize()
	{
		SetType();
	}

	private void SetType()
	{
		if ( _component != null )
		{
			_type = _component.GetType();
		}
		else
		{
			_type = typeof( GameObject );
		}
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

