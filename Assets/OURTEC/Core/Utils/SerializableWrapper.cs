﻿////////////////////////////////////////////
// 
// SerializableWrapper.cs
//
// Created: 26/01/2018 ccuthbert
// Contributors:
// 
// Intention:
// Wrap a non-serialized (but serializable) 
// class in a ScriptableObject.
// Allows a plain class to be saved as an
// asset, without that class needing to 
// inherit from ScriptableObject.
// This class should be used as a base class
// for a specific sub-class.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_SerializableWrapper
#endif

using UnityEngine;

namespace AmuzoEngine
{
	public abstract class SerializableWrapper<T> : ScriptableObject
	{
		[SerializeField]
		private T	_data;

		public T _pData
		{
			get
			{
				return _data;
			}
		}
	}
}
