﻿////////////////////////////////////////////
// 
// SimpleIntegerIdGenerator.cs
//
// Created: 25/02/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// Refactored out of Utils.cs
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
namespace AmuzoEngine
{
	public class SimpleIntegerIdGenerator
	{
		private int _next;
		public SimpleIntegerIdGenerator()
		{
			Reset();
		}
		public int _pNew
		{
			get
			{
				return _next++;
			}
		}
		public void Reset()
		{
			_next = 0;
		}
	}
}

