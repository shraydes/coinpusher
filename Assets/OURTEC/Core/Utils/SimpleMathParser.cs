﻿////////////////////////////////////////////
// 
// SimpleMathParser.cs
//
// Created: 07/08/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_SimpleMathParser
#endif

using System.Text.RegularExpressions;
using FloatT = System.Single;

namespace AmuzoEngine
{
	public static class SimpleMathParser
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[SimpleMathParser] ";

		private static readonly Regex _sMatchWhitespace = new Regex( @"\s+" );

		[System.Flags]
		public enum EParseOptions
		{
			NONE = 0,
			IMPLICIT_MULTIPLICATION = 1,
			SINGLE_LETTER_VARIABLES = 2,

			ALGEBRA = IMPLICIT_MULTIPLICATION|SINGLE_LETTER_VARIABLES,
			PROGRAM = NONE
		}

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//
	
		//
		// PROPERTIES
		//
	
		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public static bool IsNumber( string str, int start, out int length, out FloatT value )
		{
			length = 0;
			value = default( FloatT );

			int	strLen = str.Length;

			if ( start >= strLen ) return false;

			int		idx = start;
			char	chr = str[ idx ];

			if ( chr == '-' )
			{
				++idx;
			}

			bool	hasDigit = false;
			bool	hasPoint = false;

			for ( ; idx < strLen; ++idx )
			{
				chr = str[ idx ];

				if ( chr >= '0' && chr <= '9' )
				{
					hasDigit = true;
				}
				else if ( chr == '.' && !hasPoint )
				{
					hasPoint = true;
				}
				else
				{
					break;
				}
			}

			if ( !hasDigit ) return false;

			length = idx - start;

			string	numberStr = str.Substring( start, length );

			return FloatT.TryParse( numberStr, out value );
		}

		public static bool IsOperator( string str, int start, bool isExpectingInfixOrPostfix, out int length, out int arity )
		{
			length = 0;
			arity = 0;

			int	strLen = str.Length;

			if ( start >= strLen ) return false;

			int		idx = start;
			char	chr = str[ idx ];

			if ( isExpectingInfixOrPostfix )
			{
				switch ( chr )
				{
				case '+':
				case '-':
				case '*':
				case '/':
				case '^':
				case '%':
					length = 1;
					arity = 0;
					return true;
				}
			}
			else
			{
				switch ( chr )
				{
				case '-':
					length = 1;
					arity = -1;
					return true;
				}
			}

			return false;
		}

		public static bool IsName( string str, int start, out int length, out string name, out bool isFunction, EParseOptions options )
		{
			length = 0;
			name = null;
			isFunction = false;

			int	strLen = str.Length;

			if ( start >= strLen ) return false;

			int		idx = start;
			char	chr = str[ idx ];
			bool	isSingleLetterVarsOnly = ( options & EParseOptions.SINGLE_LETTER_VARIABLES ) != 0;
			bool	isValidChar = chr >= 'a' && chr <= 'z' || chr >= 'A' && chr <= 'Z' || ( chr == '_' && !isSingleLetterVarsOnly );

			if ( !isValidChar ) return false;

			++idx;

			for ( ; idx < strLen; ++idx )
			{
				chr = str[ idx ];
				isValidChar = chr >= 'a' && chr <= 'z' || chr >= 'A' && chr <= 'Z' || chr >= '0' && chr <= '9' || chr == '_';
				if ( !isValidChar ) break;
			}

			length = idx - start;
			isFunction = idx < strLen && str[ idx ] == '(';

			if ( isSingleLetterVarsOnly )//if only allowed single-letter variables...
			{
				if ( length == 1 )
				{
					if ( isFunction && ( options & EParseOptions.IMPLICIT_MULTIPLICATION ) != 0 )//if this is a single-letter followed by an open-bracket and we're allowed implicit multiplication...
					{
						isFunction = false;//...it's not a function; it's a variable followed by implicit multiplication
					}
				}
				else//if it's more than 1 letter...
				{
					if ( !isFunction )//if it's a variable...
					{
						length = 1;//...truncate to single-letter
					}
				}
			}

			name = str.Substring( start, length );

			return true;
		}
	
		public static bool ParseExpression( string expr, System.Action<string, FloatT> onNumber, System.Action<string, int> onOperator, System.Action<string> onOpenParenthesis, System.Action onCloseParenthesis, System.Action<string> onVariable, System.Action onSeparator, System.Action<int> onEnd, EParseOptions parseOptions )
		{
			if ( expr.Contains( " " ) )
			{
				expr = _sMatchWhitespace.Replace( expr, "" );
			}

			FloatT	value;
			int tokenNext = 0;
			int	tokenLength;
			int	opArity;
			string	name;
			bool	isFunction;
			bool	isExpectingNumber = true;

			while ( tokenNext < expr.Length )
			{
				if ( isExpectingNumber )
				{
					if ( IsNumber( expr, tokenNext, out tokenLength, out value ) )
					{
						onNumber( expr.Substring( tokenNext, tokenLength ), value );//TODO: don't do substring again - CC20170807
						tokenNext += tokenLength;
						isExpectingNumber = false;
					}
					else if ( IsName( expr, tokenNext, out tokenLength, out name, out isFunction, parseOptions ) )
					{
						if ( isFunction )
						{
							onOpenParenthesis( name );
							tokenNext += tokenLength + 1;
							isExpectingNumber = true;
						}
						else
						{
							onVariable( name );
							tokenNext += tokenLength;
							isExpectingNumber = false;
						}
					}
					else if ( expr[ tokenNext ] == '(' )
					{
						onOpenParenthesis( null );
						++tokenNext;
						isExpectingNumber = true;
					}
					else if ( IsOperator( expr, tokenNext, false, out tokenLength, out opArity ) )
					{
						onOperator( expr.Substring( tokenNext, tokenLength ), opArity );//TODO: don't do substring again - CC20170807
						tokenNext += tokenLength;
						isExpectingNumber = true;
					}
					else
					{
						onEnd( tokenNext );
						return false;
					}
				}
				else
				{
					if ( IsOperator( expr, tokenNext, true, out tokenLength, out opArity ) )
					{
						onOperator( expr.Substring( tokenNext, tokenLength ), opArity );//TODO: don't do substring again - CC20170807
						tokenNext += tokenLength;
						isExpectingNumber = opArity == 0;
					}
					else if ( expr[ tokenNext ] == ')' )
					{
						onCloseParenthesis();
						++tokenNext;
						isExpectingNumber = false;
					}
					else if ( expr[ tokenNext ] == ',' )
					{
						onSeparator();
						++tokenNext;
						isExpectingNumber = true;
					}
					else if ( ( parseOptions & EParseOptions.IMPLICIT_MULTIPLICATION ) != 0 && ( 
						IsNumber( expr, tokenNext, out tokenLength, out value ) || 
						IsName( expr, tokenNext, out tokenLength, out name, out isFunction, parseOptions ) || 
						expr[ tokenNext ] == '(' ) )
					{
						onOperator( "*", 0 );
						isExpectingNumber = true;
					}
					else
					{
						onEnd( tokenNext );
						return false;
					}
				}
			}

			onEnd( tokenNext );
			return true;
		}
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//
	}
}
