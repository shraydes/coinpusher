﻿////////////////////////////////////////////
// 
// DomainChecker.cs
//
// Created: 27/09/2017 ccuthbert
// Contributors:
// 
// Intention:
// Refactored out from SessionManager.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if UNITY_WEBPLAYER || UNITY_WEBGL
#define ENABLE_DOMAIN_CHECKER
#endif

using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class DomainChecker : InitialisationObject
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[DomainChecker] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	[SerializeField]
	private string[]	_trustedDomains;

	[SerializeField]
	private bool	_allowAllDomains = true;

#if ENABLE_DOMAIN_CHECKER
	private bool	_illegalDomain = false;
#endif

	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	// ** PUBLIC
	//
	
	public override void startInitialising()
	{
#if ENABLE_DOMAIN_CHECKER
		_currentState = InitialisationState.INITIALISING;

		_allowAllDomains = _allowAllDomains || Application.isEditor;

		// if the domain is illegal 

		if( !CheckTrustedDomains() )
		{
			// destroy everything but this:

			GameObject[] gameObjects = GameObject.FindObjectsOfType( typeof( GameObject ) ) as GameObject[];

			foreach( GameObject go in gameObjects )
			{
				if( go != gameObject )
				{
					DestroyImmediate( go );
				}
			}

			// set illegal domain to true so that this behaviour will do nothing

			_illegalDomain = true;

			throw new System.Exception( "ILLEGAL DOMAIN" );
		}
#endif

		_currentState = InitialisationState.FINISHED;
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private bool CheckTrustedDomains()
	{
		if ( Debug.isDebugBuild || Application.isEditor || _allowAllDomains )
		{
			Debug.Log( "Not Checking Trusted Domains" );
			return true;
		}

		string url = Application.absoluteURL.Replace( "https://", "" );
		url = url.Replace( "http://", "" );
		int slash = url.IndexOf( '/' );
		url = url.Substring( 0, slash );

		Debug.Log( "Checking Domain: " + url );

		foreach( string domain in _trustedDomains )
		{
			if( url.EndsWith( domain ) )
			{
				Debug.Log( "Application is running on allowed domain: " + domain );
				return true;
			}
		}

		return false;
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

