﻿////////////////////////////////////////////
// 
// DpadUI.cs
//
// Created: 16/06/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
//#define DEBUG_DpadUI
#endif

using System.Collections.Generic;
using UnityEngine;
using LoggerAPI = UnityEngine.Debug;

namespace AmuzoEngine
{
	public class DpadUI
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const float		CLOSE_ANGLE_MAX_DEFAULT = 10f;

		private const float		ALLOW_ANGLE_MAX_DEFAULT = 60f;

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//

		public interface IScreen
		{
			int	_pElementCount { get; }

			IElement	GetElement( int i );

			IElement	_pInitialFocusElement { get; }

			int		_pDepth { get; }

			bool	_pIsBlocking { get; }

			bool	_pIsScrollingHorizontal { get; }

			bool	_pIsScrollingVertical { get; }

			bool	GetBlockingElementDepth( out int blockingElementDepth );

			void	OnElementGainedFocus( IElement element );

			void	OnElementLostFocus( IElement element );
		}

		public interface IElement
		{
			IScreen	_pScreen { get; }

			Rect	_pRect { get; }

			int		_pDepth { get; }

			bool	_pIsVisible { get; }

			void	RefreshRect();

			void	OnGainedFocus();

			void	OnLostFocus();
		}

		public delegate void DOnFocusElementChanged( IElement oldFocusElement, IElement newFocusElement );

		//
		// MEMBERS 
		//

		private string	_instanceId;

		private List<IScreen>	_activeScreens = new List<IScreen>();

		private List<IElement>	_activeElements = new List<IElement>();

		private IElement	_focusElem = null;

		private int?	_blockingScreenDepth = null;

		private int?	_blockingElementDepth = null;

		//
		// PROPERTIES
		//

		private string _pLogTag
		{
			get
			{
				return "[DpadUI:" + _instanceId + "] ";
			}
		}

		public IElement _pFocusElement
		{
			get
			{
				return _focusElem;
			}
			set
			{
				SetFocusElementInt( value );
			}
		}

		public int _pActiveScreenCount
		{
			get
			{
				return _activeScreens.Count;
			}
		}

		public event DOnFocusElementChanged	_pOnFocusElementChanged;
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public DpadUI( string name )
		{
			_instanceId = name;
		}

		public void ChangeFocus( Vector2 dir )
		{
			if ( _focusElem == null )
			{
				Debug.LogWarning( _pLogTag + "Cannot change focus because no current focus" );
				return;
			}

			IElement	newFocusElement = GetBestLinkedElement( _focusElem, dir, CLOSE_ANGLE_MAX_DEFAULT * UnitConversion.DegToRad, ALLOW_ANGLE_MAX_DEFAULT * UnitConversion.DegToRad );

			if ( newFocusElement != null )
			{
				SetFocusElementInt( newFocusElement );
			}
		}

		public void OnShowScreen( IScreen screen )
		{
			ActivateScreen( screen );

			EnsureValidFocusElement();
		}

		public void OnShowScreenComplete( IScreen screen )
		{
			ActivateElements( screen );

			EnsureValidFocusElement( screen._pInitialFocusElement );
		}

		public void OnExitScreen( IScreen screen )
		{
			DeactivateElements( screen );

			EnsureValidFocusElement();
		}

		public void OnExitScreenComplete( IScreen screen )
		{
			DeactivateScreen( screen );

			EnsureValidFocusElement();
		}

		public void OnScreenLayoutChanged( IScreen screen, bool hasContentChanged = false, IList<IElement> newElements = null )
		{
			if ( hasContentChanged )
			{
				if ( newElements != null )
				{
					for ( int i = 0; i < newElements.Count; ++i )
					{
						ActivateElement( newElements[i] );
					}
				}
				else
				{
					DeactivateElements( screen );
					ActivateElements( screen );
				}
			}

			RefreshElementRects( screen );
			RefreshBlockingDepth();

			EnsureValidFocusElement();
		}

	#if UNITY_EDITOR
		public Texture2D CreateDebugTexture( IScreen screen )
		{
			Debug.Log( _pLogTag + string.Format( "CreateTexture: {0}x{1}", Screen.width, Screen.height ) );

			Texture2D	tex = new Texture2D( Screen.width, Screen.height );

			Rect	rect;

			for ( int i = 0; i < screen._pElementCount; ++i )
			{
				rect = screen.GetElement(i)._pRect;

				Debug.Log( _pLogTag + string.Format( "Rect[{0}]: {1}", i.ToString(), rect.ToString() ) );
				int	width = (int)rect.width;
				int	height = (int)rect.height;
				int	pixelCount = width * height;
				Color[]	colours = new Color[ pixelCount ];
				for ( int j = 0; j < pixelCount; ++j )
				{
					colours[j] = Color.white;
				}
				tex.SetPixels( (int)rect.xMin, (int)rect.yMin, width, height, colours );
			}

			return tex;
		}
	#endif

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private void SetFocusElementInt( IElement newFocusElem )
		{
			if ( _focusElem != newFocusElem )
			{
				IElement	oldFocusElem = _focusElem;

				_focusElem = newFocusElem;

#if DEBUG_DpadUI
				if ( newFocusElem != null )
				{
					LoggerAPI.Log( _pLogTag + "Focus: " + newFocusElem + " @ " + newFocusElem._pRect.ToString() );
				}
				else
				{
					LoggerAPI.Log( _pLogTag + "Focus: null" );
				}
#endif
				if ( oldFocusElem != null )
				{
					oldFocusElem.OnLostFocus();

					if ( oldFocusElem._pScreen != null )
					{
						oldFocusElem._pScreen.OnElementLostFocus( oldFocusElem );
					}
				}

				if ( newFocusElem != null )
				{
					newFocusElem.OnGainedFocus();

					if ( newFocusElem._pScreen != null )
					{
						newFocusElem._pScreen.OnElementGainedFocus( newFocusElem );
					}
				}

				if ( _pOnFocusElementChanged != null )
				{
					_pOnFocusElementChanged( oldFocusElem, newFocusElem );
				}
			}
		}

		private void RefreshElementRects( IScreen screen )
		{
			for ( int i = 0; i < screen._pElementCount; ++i )
			{
				screen.GetElement(i).RefreshRect();
			}
		}

		private void RefreshBlockingDepth()
		{
			_blockingScreenDepth = null;
			_blockingElementDepth = null;

			IScreen	screen;

			for ( int i = 0; i < _activeScreens.Count; ++i )
			{
				screen = _activeScreens[i];

				if ( screen == null )
				{
					Debug.LogError( _pLogTag + "Expected screen to have IScreen" );
					continue;
				}

				if ( screen._pIsBlocking )
				{
					if ( !_blockingScreenDepth.HasValue || screen._pDepth > _blockingScreenDepth.Value )
					{
						_blockingScreenDepth = screen._pDepth;
						_blockingElementDepth = null;
					}
				}
				else
				{
					if ( !_blockingScreenDepth.HasValue || screen._pDepth >= _blockingScreenDepth.Value )
					{
						int	elementDepth;

						if ( screen.GetBlockingElementDepth( out elementDepth ) == true )
						{
							//if there's no blocking screen
							// OR [there is and] this screen has a higher depth
							// OR [this screen has the same depth and] there's no blocking element 
							// OR [there is a blocking element and] this element has a higher depth
							if ( !_blockingScreenDepth.HasValue || screen._pDepth > _blockingScreenDepth.Value || !_blockingElementDepth.HasValue || elementDepth > _blockingElementDepth.Value )
							{
								_blockingScreenDepth = screen._pDepth;
								_blockingElementDepth = elementDepth;
							}
						}
					}
				}
			}
		}

		private void EnsureValidFocusElement( IElement initFocus = null )
		{
			if ( _focusElem != null && _activeElements.Contains( _focusElem ) && IsElementTargetable( _focusElem, null ) ) return;

			IElement	newFocusElem;

			if ( initFocus != null && _activeElements.Contains( initFocus ) && IsElementTargetable( initFocus, null ) )
			{
				newFocusElem = initFocus;
			}
			else
			{
				newFocusElem = _activeElements.Find( e => IsElementTargetable( e, null ) );
			}

			SetFocusElementInt( newFocusElem );
		}

		private bool IsElementTargetable( IElement elem, Rect? elemRect )
		{
			if ( elem == null ) return false;
			if ( !elem._pIsVisible ) return false;

			//check blocked
			if ( _blockingScreenDepth.HasValue && elem._pScreen != null && elem._pScreen._pDepth < _blockingScreenDepth.Value ) return false;
			if ( _blockingElementDepth.HasValue && _blockingScreenDepth.HasValue && elem._pScreen != null && elem._pScreen._pDepth == _blockingScreenDepth.Value && elem._pDepth < _blockingElementDepth.Value ) return false;

			//check on screen
			if ( !elemRect.HasValue )
			{
				elemRect = elem._pRect;
			}

			if ( ( elemRect.Value.xMax < 0f || elemRect.Value.xMin > Screen.width ) && !elem._pScreen._pIsScrollingHorizontal ) return false;
			if ( ( elemRect.Value.yMax < 0f || elemRect.Value.yMin > Screen.height ) && !elem._pScreen._pIsScrollingVertical ) return false;

			return true;
		}

		private IElement GetBestLinkedElement( IElement fromElem, Vector2 inDir, float closeAngleMax, float allowAngleMax )
		{
			IElement	bestElem = null;
			float	bestDist = 0f;
			bool	isBestClose = false;
			IElement	tempElem;
			Vector2	tempDir;
			float	tempDist, tempDot;
			bool	isTempClose;

			inDir.Normalize();

			float	closeDotMin = Mathf.Cos( closeAngleMax );
			float	allowDotMin = Mathf.Cos( allowAngleMax );

			for ( int i = 0; i < _activeElements.Count; ++i )
			{
				tempElem = _activeElements[i];

				if ( tempElem == fromElem ) continue;
				if ( !IsElementTargetable( tempElem, null ) ) continue;

				GetLinkVector( fromElem._pRect, tempElem._pRect, out tempDir, out tempDist );

				tempDot = Vector2.Dot( tempDir, inDir );

				if ( tempDot < allowDotMin ) continue;

				isTempClose = tempDot >= closeDotMin;

				if ( isBestClose && !isTempClose ) continue;

				if ( bestElem == null || isTempClose && !isBestClose || tempDist < bestDist )
				{
					bestElem = tempElem;
					bestDist = tempDist;
					isBestClose = isTempClose;
				}
			}

			return bestElem;
		}

		private static void GetLinkVector( Rect fromRect, Rect toRect, out Vector2 dir, out float dist )
		{
			Vector2	vec = Vector2.zero;

			if ( fromRect.xMax < toRect.xMin )
			{
				vec.x = toRect.xMin - fromRect.xMax;
			}
			else if ( fromRect.xMin > toRect.xMax )
			{
				vec.x = toRect.xMax - fromRect.xMin;
			}

			if ( fromRect.yMax < toRect.yMin )
			{
				vec.y = toRect.yMin - fromRect.yMax;
			}
			else if ( fromRect.yMin > toRect.yMax )
			{
				vec.y = toRect.yMax - fromRect.yMin;
			}

			dist = vec.magnitude;

			if ( dist == 0f )
			{
				dir = toRect.center - fromRect.center;
			}
			else
			{
				dir = ( 1f / dist ) * vec;
			}
		}

		private void ActivateScreen( IScreen screen )
		{
			if ( !_activeScreens.Contains( screen ) )
			{
				_activeScreens.Add( screen );
			}

			RefreshBlockingDepth();
		}

		private void ActivateElements( IScreen screen )
		{
			for ( int i = 0; i < screen._pElementCount; ++i )
			{
				ActivateElement( screen.GetElement(i) );
			}
		}

		private void ActivateElement( IElement element )
		{
			if ( !_activeElements.Contains( element ) )
			{
				_activeElements.Add( element );
			}

			element.RefreshRect();
		}

		private void DeactivateElements( IScreen screen )
		{
			for ( int i = 0; i < _activeElements.Count; ++i )
			{
				if ( _activeElements[i]._pScreen == screen )
				{
					_activeElements.RemoveAt( i-- );
				}
			}
		}

		private void DeactivateScreen( IScreen screen )
		{
			int	idx = _activeScreens.IndexOf( screen );

			if ( idx >= 0 )
			{
				_activeScreens.RemoveAt( idx );
			}

			RefreshBlockingDepth();
		}

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}

