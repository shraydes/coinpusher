﻿////////////////////////////////////////////
// 
// ScrollingPooledListBase.cs
//
// Created: 22/01/2019 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_ScrollingPooledListBase
#endif

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AmuzoEngine
{
	public abstract class ScrollingPooledListBase : MonoBehaviour
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[ScrollingPooledListBase] ";

		private static readonly Vector2	VECTOR2_CENTRE = new Vector2( 0.5f, 0.5f );
		private static readonly Vector2	VECTOR2_BOTTOM_LEFT = new Vector2( 0f, 0f );
		private static readonly Vector2	VECTOR2_LEFT = new Vector2( 0f, 0.5f );
		private static readonly Vector2	VECTOR2_TOP_LEFT = new Vector2( 0f, 1f );
		private static readonly Vector2	VECTOR2_TOP = new Vector2( 0.5f, 1f );
		private static readonly Vector2	VECTOR2_TOP_RIGHT = new Vector2( 1f, 1f );

		private static readonly ActiveElementUI	NULL_ACTIVE_ELEMENT_UI = new ActiveElementUI() { _go = null, _idx = -1 };

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//

		public interface IOwner
		{
			int	_pElementCount	{ get; }

			void	InitialiseElementUI( GameObject elementUI, int elementIdx );
			void	UninitialiseElementUI( GameObject elementUI, int elementIdx );
		}

		private struct ActiveElementUI
		{
			public GameObject	_go;
			public int	_idx;
		}
			
		//
		// MEMBERS 
		//

		[SerializeField]
		private Object	_owner;

		[SerializeField]
		private GameObject	_listElementUIPrefab;

		[SerializeField]
		private int	_listElementUIPoolSize;

		[SerializeField]
		private RectOffset	_listPadding;

		[SerializeField]
		private float	_listElementSpacing;

		[SerializeField]
		private int	_listElementAllocPadding;

		[SerializeField]
		[HideInInspector]
		private RectTransform	_scrollRectTrans;

		[SerializeField]
		[HideInInspector]
		private RectTransform	_contentTrans;

		[SerializeField]
		[HideInInspector]
		private ScrollRect	_scrollRect;
	
		[SerializeField]
		[HideInInspector]
		private PrefabPool	_listElementUIPool;

		private IOwner	_ownerInt;

		private List<ActiveElementUI>	_activeElementUIs;

		private float	_viewportSize;

		private float	_listElementSize;

		private int	_activeElementFirst;

		private int	_activeElementCountTarget;

		//
		// PROPERTIES
		//

		//
		// ABSTRACTS / VIRTUALS
		//

		protected abstract RectTransform.Axis _pScrollAxis	{ get; }

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		private void Awake()
		{
			FindOwner();

			CreateUIStructure();
			CreateListElementPool();

			InitialiseListElementPool();
			InitialiseListElementSize();
			InitialiseActiveElementUIs();
		}

		private void OnEnable()
		{
			RefreshViewportSize();
			RefreshContentSize();
			RefreshActiveFirst();
			RefreshActiveCountTarget();
			RefreshActiveAllocation();
		}

		private void Update()
		{
			RefreshViewportSize();
			RefreshActiveFirst();
		}

		// ** PUBLIC
		//

		public void OnElementCountChanged()
		{
			RefreshContentSize();
			RefreshActiveFirst();
			RefreshActiveAllocation();
		}

		// ** INTERNAL
		//

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private void FindOwner()
		{
			if ( _owner is GameObject )
			{
				_ownerInt = (_owner as GameObject).GetComponent( typeof( IOwner ) ) as IOwner;
			}
			else
			{
				_ownerInt = _owner as IOwner;
			}
		}

		[ContextMenu("Create UI Structure")]
		private void CreateUIStructure()
		{
			if ( _scrollRectTrans == null )
			{
				_scrollRectTrans = new GameObject( "ScrollRect", typeof( RectTransform ) ).transform as RectTransform;
				_scrollRectTrans.SetParent( this.transform );
				_scrollRectTrans.localPosition = Vector3.zero;
				_scrollRectTrans.localRotation = Quaternion.identity;
				_scrollRectTrans.localScale = Vector3.one;
				_scrollRectTrans.pivot = VECTOR2_CENTRE;
				_scrollRectTrans.anchorMin = VECTOR2_BOTTOM_LEFT;
				_scrollRectTrans.offsetMin = Vector2.zero;
				_scrollRectTrans.anchorMax = VECTOR2_TOP_RIGHT;
				_scrollRectTrans.offsetMax = Vector2.zero;
			}

			bool	isVertical = _pScrollAxis == RectTransform.Axis.Vertical;

			if ( _contentTrans == null )
			{
				_contentTrans = new GameObject( "Content", typeof( RectTransform ) ).transform as RectTransform;
				_contentTrans.SetParent( _scrollRectTrans );
				_contentTrans.localPosition = Vector3.zero;
				_contentTrans.localRotation = Quaternion.identity;
				_contentTrans.localScale = Vector3.one;
				_contentTrans.pivot = isVertical ? VECTOR2_TOP : VECTOR2_LEFT;
				_contentTrans.anchorMin = isVertical ? VECTOR2_TOP_LEFT : VECTOR2_BOTTOM_LEFT;
				_contentTrans.offsetMin = Vector2.zero;
				_contentTrans.anchorMax = isVertical ? VECTOR2_TOP_RIGHT : VECTOR2_TOP_LEFT;
				_contentTrans.offsetMax = Vector2.zero;
				_contentTrans.gameObject.AddComponent<Image>().color = Color.clear;
			}

			if ( _scrollRect == null )
			{
				_scrollRect = _scrollRectTrans.gameObject.AddComponent<ScrollRect>();
				_scrollRect.vertical = isVertical;
				_scrollRect.horizontal = !isVertical;
			}

			_scrollRect.viewport = _scrollRectTrans;
			_scrollRect.content = _contentTrans;

			_scrollRect.viewport.gameObject.EnsureComponent<RectMask2D>();
		}

		[ContextMenu("Create List Element Pool")]
		private void CreateListElementPool()
		{
			if ( _listElementUIPool == null )
			{
				_listElementUIPool = new GameObject( "ElementPool" ).AddComponent<PrefabPool>();
				_listElementUIPool.transform.SetParent( this.transform );
				_listElementUIPool.transform.localPosition = Vector3.zero;
				_listElementUIPool.transform.localRotation = Quaternion.identity;
				_listElementUIPool.transform.localScale = Vector3.one;
				_listElementUIPool._initType = EInitializeType.MANUAL;
				_listElementUIPool._prefabs = new PrefabPool.PrefabInfo[] {
					new PrefabPool.PrefabInfo() {
						_prefab = _listElementUIPrefab,
						_count = _listElementUIPoolSize
					}
				};
				_listElementUIPool._instanceName = "Element";
				if ( _contentTrans == null ) throw new System.Exception( "Must create UI structure first" );
				_listElementUIPool._instanceContainerOverride = _contentTrans;
				_listElementUIPool._isEnableDefaultInstanceStateSetting = false;
				_listElementUIPool._OnSetInstanceStateAllocated += ShowElementUI;
				_listElementUIPool._OnSetInstanceStateFree += HideElementUI;
				_listElementUIPool._isShuffleInstances = false;
				_listElementUIPool._isCreateOffline = false;
				_listElementUIPool._onInstantiateFailed = PrefabPool.EInstantiateFailedPolicy.FAIL;
			}
		}

		private void InitialiseListElementPool()
		{
			if ( !_listElementUIPool._pIsInitialised )
			{
				_listElementUIPool.Initialize();
			}
		}

		private void InitialiseActiveElementUIs()
		{
			_activeElementUIs = new List<ActiveElementUI>();
		}

		private void InitialiseListElementSize()
		{
			RectTransform	elementUITrans = _listElementUIPrefab.transform as RectTransform;

			SetListElementSize( _pScrollAxis == RectTransform.Axis.Vertical ? elementUITrans.rect.height : elementUITrans.rect.width );
		}

		private void SetListElementSize( float newSize )
		{
			if ( _listElementSize != newSize )
			{
				_listElementSize = newSize;

				RefreshContentSize();
				RefreshActiveCountTarget();
			}
		}

		private void RefreshViewportSize()
		{
			SetViewportSize( _pScrollAxis == RectTransform.Axis.Vertical ? _scrollRect.viewport.rect.height : _scrollRect.viewport.rect.width );
		}

		private void SetViewportSize( float newSize )
		{
			if ( _viewportSize != newSize )
			{
				_viewportSize = newSize;

				RefreshActiveCountTarget();
			}
		}

		private void RefreshContentSize()
		{
			if ( _ownerInt == null ) return;
			if ( _listElementSize <= 0f ) return;

			float	fElementCount = (float)_ownerInt._pElementCount;
			float	paddingBefore, paddingAfter;

			if ( _pScrollAxis == RectTransform.Axis.Vertical )
			{
				paddingBefore = _listPadding.top;
				paddingAfter = _listPadding.bottom;
			}
			else
			{
				paddingBefore = _listPadding.left;
				paddingAfter = _listPadding.right;
			}

			float	contentSize = paddingBefore + ( _listElementSize + _listElementSpacing ) * fElementCount - _listElementSpacing + paddingAfter;

			_contentTrans.SetSizeWithCurrentAnchors( _pScrollAxis, contentSize );
		}

		private void RefreshActiveFirst()
		{
			float	contentStartPos = _pScrollAxis == RectTransform.Axis.Vertical ? _contentTrans.anchoredPosition.y : -_contentTrans.anchoredPosition.x;
			float	elementStep = _listElementSize + _listElementSpacing;
			int		visibleFirst = (int)(contentStartPos / elementStep);
			int		activeFirst = visibleFirst - _listElementAllocPadding;

			SetActiveFirst( activeFirst );
		}

		private void SetActiveFirst( int newActiveFirst )
		{
			if ( newActiveFirst < 0 ) 
			{
				newActiveFirst = 0;
			}

			int	elementCount = _ownerInt._pElementCount;

			if ( newActiveFirst >= elementCount )
			{
				newActiveFirst = elementCount - 1;
			}

			if ( _activeElementFirst != newActiveFirst )
			{
				_activeElementFirst = newActiveFirst;

				RefreshActiveAllocation();
			}
		}

		private void RefreshActiveCountTarget()
		{
			if ( _listElementSize <= 0f ) return;
			if ( _viewportSize <= 0f  ) return;

			float	elementStep = _listElementSize + _listElementSpacing;
			int		visibleCount = (int)(_viewportSize / elementStep) + 1;
			int		activeCount = visibleCount + (_listElementAllocPadding << 1);

			SetActiveCountTarget( activeCount );
		}

		private void SetActiveCountTarget( int newActiveCount )
		{
			if ( newActiveCount < 0 ) 
			{
				newActiveCount = 0;
			}

			if ( _activeElementCountTarget != newActiveCount )
			{
				_activeElementCountTarget = newActiveCount;

				RefreshActiveAllocation();
			}
		}

		private void RefreshActiveAllocation()
		{
			if ( _ownerInt == null ) return;
			if ( _activeElementUIs == null ) return;

			int	elementCount = _ownerInt._pElementCount;
			int	activeCountMax = elementCount - _activeElementFirst;
			int	activeCount = Mathf.Min( _activeElementCountTarget, activeCountMax );
			int	activeElementLast = _activeElementFirst + activeCount - 1;
			int	activeElementIdx;
			int	activeElementMin = -1;
			int	activeElementMax = -1;

			for ( int i = 0; i < _activeElementUIs.Count; ++i )
			{
				activeElementIdx = _activeElementUIs[i]._idx;

				if ( activeElementIdx < _activeElementFirst || activeElementIdx > activeElementLast )
				{
					DestroyActiveElementUI( i );
				}
				else
				{
					if ( activeElementMin < 0 || activeElementIdx < activeElementMin )
					{
						activeElementMin = activeElementIdx;
					}

					if ( activeElementMax < 0 || activeElementIdx > activeElementMax )
					{
						activeElementMax = activeElementIdx;
					}
				}
			}

			for ( int i = _activeElementFirst; i <= activeElementLast; ++i )
			{
				if ( i < activeElementMin || i > activeElementMax )
				{
					CreateActiveElementUI( i );
				}
			}

			CleanActiveElementUIs();
		}

		private void DestroyAllActiveElementUIs()
		{
			for ( int i = 0; i < _activeElementUIs.Count; ++i )
			{
				DestroyActiveElementUI( i );
			}

			CleanActiveElementUIs();
		}

		private void CreateActiveElementUI( int elementIdx )
		{
			GameObject	elementUI = AllocateElementUI();

			if ( elementUI == null ) return;

			SetActiveElementUITransform( elementUI, elementIdx );

			_ownerInt.InitialiseElementUI( elementUI, elementIdx );

			AddActiveElementUI( elementUI, elementIdx );
		}

		private void DestroyActiveElementUI( int activeIdx )
		{
			GameObject	elementUI = _activeElementUIs[ activeIdx ]._go;

			_ownerInt.UninitialiseElementUI( elementUI, _activeElementUIs[ activeIdx ]._idx );

			RemoveActiveElementUI( activeIdx );

			FreeElementUI( elementUI );
		}

		private void AddActiveElementUI( GameObject elementUI, int elementIdx )
		{
			for ( int i = 0; i < _activeElementUIs.Count; ++i )
			{
				if ( _activeElementUIs[i]._idx < 0 )
				{
					_activeElementUIs[i] = new ActiveElementUI() { _go = elementUI, _idx = elementIdx };
					return;
				}
			}

			_activeElementUIs.Add( new ActiveElementUI() { _go = elementUI, _idx = elementIdx } );
		}

		private void RemoveActiveElementUI( int activeIdx )
		{
			_activeElementUIs[ activeIdx ] = NULL_ACTIVE_ELEMENT_UI;
		}

		private void CleanActiveElementUIs()
		{
			for ( int i = _activeElementUIs.Count - 1; i >= 0; --i )
			{
				if ( _activeElementUIs[i]._idx < 0 )
				{
					_activeElementUIs.RemoveAt(i);
				}
			}
		}

		private void SetActiveElementUITransform( GameObject elementUI, int elementIdx )
		{
			RectTransform	elementTrans = elementUI.transform as RectTransform;

			RectTransform.Edge	parentEdge;

			float	paddingBefore;

			if ( _pScrollAxis == RectTransform.Axis.Vertical )
			{
				elementTrans.anchorMin = VECTOR2_TOP_LEFT;
				elementTrans.offsetMin = new Vector2( _listPadding.left, 0f );
				elementTrans.anchorMax = VECTOR2_TOP_RIGHT;
				elementTrans.offsetMax = new Vector2( _listPadding.right, 0f );

				parentEdge = RectTransform.Edge.Top;
				paddingBefore = _listPadding.top;
			}
			else
			{
				elementTrans.anchorMin = VECTOR2_BOTTOM_LEFT;
				elementTrans.offsetMin = new Vector2( 0f, _listPadding.bottom );
				elementTrans.anchorMax = VECTOR2_TOP_LEFT;
				elementTrans.offsetMax = new Vector2( 0f, _listPadding.top );

				parentEdge = RectTransform.Edge.Left;
				paddingBefore = _listPadding.left;
			}

			float	elementStep = _listElementSize + _listElementSpacing;
			float	offset = paddingBefore + elementStep * (float)elementIdx;

			elementTrans.SetInsetAndSizeFromParentEdge( parentEdge, offset, _listElementSize );
		}

		private GameObject AllocateElementUI()
		{
			return _listElementUIPool.AllocateFromPool();
		}

		private void FreeElementUI( GameObject elementUI )
		{
			_listElementUIPool.ReturnToPool( elementUI );
		}

		private void ShowElementUI( GameObject elementUI )
		{
			var	canvas = elementUI.GetComponent<Canvas>();

			if ( canvas != null )
			{
				canvas.enabled = true;
			}
			else
			{
				elementUI.SetActive( true );
			}
		}

		private void HideElementUI( GameObject elementUI )
		{
			var	canvas = elementUI.GetComponent<Canvas>();

			if ( canvas != null )
			{
				canvas.enabled = false;
			}
			else
			{
				elementUI.SetActive( false );
			}
		}

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
