﻿////////////////////////////////////////////
// 
// HorizontalScrollingPooledList.cs
//
// Created: 22/01/2019 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_HorizontalScrollingPooledList
#endif

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AmuzoEngine
{
	public class HorizontalScrollingPooledList : ScrollingPooledListBase
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[HorizontalScrollingPooledList] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//

		//
		// MEMBERS 
		//

		//
		// PROPERTIES
		//

		protected override RectTransform.Axis _pScrollAxis
		{
			get
			{
				return RectTransform.Axis.Horizontal;
			}
		}

		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		// ** PUBLIC
		//

		// ** INTERNAL
		//

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
