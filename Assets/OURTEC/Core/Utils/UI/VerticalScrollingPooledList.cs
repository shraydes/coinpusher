﻿////////////////////////////////////////////
// 
// VerticalScrollingPooledList.cs
//
// Created: 22/01/2019 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_VerticalScrollingPooledList
#endif

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AmuzoEngine
{
	public class VerticalScrollingPooledList : ScrollingPooledListBase
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[VerticalScrollingPooledList] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//

		//
		// MEMBERS 
		//

		//
		// PROPERTIES
		//

		protected override RectTransform.Axis _pScrollAxis
		{
			get
			{
				return RectTransform.Axis.Vertical;
			}
		}

		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		// ** PUBLIC
		//

		// ** INTERNAL
		//

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
