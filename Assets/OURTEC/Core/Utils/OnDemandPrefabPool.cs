﻿////////////////////////////////////////////
// 
// OnDemandPrefabPool.cs
//
// Created: 25/01/2019 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_OnDemandPrefabPool
#endif

using UnityEngine;

namespace AmuzoEngine
{
	public abstract class OnDemandPrefabPool<PoolType, PrefabType> : PrefabPool where PoolType : MonoBehaviour where PrefabType : MonoBehaviour
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[OnDemandPrefabPool] ";

		private const int	INIT_POOL_SIZE = 16;

		//
		// SINGLETON DECLARATION
		//

		private static PoolType	_sInstance = null;

		public static PoolType _pInstance
		{
			get
			{
				if ( _sInstance == null )
				{
					GameObject	newObject = new GameObject( typeof( PoolType ).Name );

					DontDestroyOnLoad( newObject );

					_sInstance = newObject.AddComponent<PoolType>();
				}

				return _sInstance;
			}
		}

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		//
		// PROPERTIES
		//

		//
		// ABSTRACTS / VIRTUALS
		//

		protected abstract Object	_pPrefab	{ get; }

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		protected virtual void Awake()
		{
			InitialiseBase();
		}

		// ** PUBLIC
		//

		public PrefabType InstantiateTyped()
		{
			return AllocateFromPool().GetComponent<PrefabType>();
		}

		public void Destroy( PrefabType instance )
		{
			ReturnToPool( instance.gameObject );
		}

		// ** INTERNAL
		//

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private void InitialiseBase()
		{
			Object	prefab = _pPrefab;

			if ( prefab == null ) return;

			_initType = EInitializeType.MANUAL;

			_prefabs = new PrefabPool.PrefabInfo[] {
				new PrefabPool.PrefabInfo() {
					_prefab = prefab,
					_count = INIT_POOL_SIZE
				}
			};

			_instanceName = prefab.name + " (Clone)";
			_isShuffleInstances = false;
			_isCreateOffline = false;
			_onInstantiateFailed = PrefabPool.EInstantiateFailedPolicy.GROW_DOUBLE;
			_isGrowOk = true;

			Initialize();
		}

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
