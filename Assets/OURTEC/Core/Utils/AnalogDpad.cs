﻿//#define USE_CROSS_PLATFORM_INPUT
////////////////////////////////////////////
// 
// AnalogDpad.cs
//
// Created: 07/12/2016 ccuthbert
// Contributors:
// 
// Intention:
// Convert analog axis values to digital 
// d-pad events
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class AnalogDpad
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[AnalogDpad] ";

	private const string	HORZ_AXIS_DEFAULT = "Horizontal";
	
	private const string	VERT_AXIS_DEFAULT = "Vertical";

	private const float		PRESS_VALUE_THRESHOLD_DEFAULT = 0.9f;

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

#pragma warning disable 0414
	private string	_horzAxis;

	private string	_vertAxis;

	private float	_pressValueThreshold = PRESS_VALUE_THRESHOLD_DEFAULT;
#pragma warning restore 0414
	
	private bool	_isPressedUp;

	private bool	_isPressedDown;

	private bool	_isPressedLeft;

	private bool	_isPressedRight;

	private bool	_wasPressedUp;

	private bool	_wasPressedDown;

	private bool	_wasPressedLeft;

	private bool	_wasPressedRight;
	
	//
	// PROPERTIES
	//
	
	public bool _pIsPressedUp
	{
		get
		{
			return _isPressedUp;
		}
	}

	public bool _pIsJustPressedUp
	{
		get
		{
			return _isPressedUp && !_wasPressedUp;
		}
	}

	public bool _pIsJustReleasedUp
	{
		get
		{
			return !_isPressedUp && _wasPressedUp;
		}
	}

	public bool _pIsPressedDown
	{
		get
		{
			return _isPressedDown;
		}
	}

	public bool _pIsJustPressedDown
	{
		get
		{
			return _isPressedDown && !_wasPressedDown;
		}
	}

	public bool _pIsJustReleasedDown
	{
		get
		{
			return !_isPressedDown && _wasPressedDown;
		}
	}

	public bool _pIsPressedLeft
	{
		get
		{
			return _isPressedLeft;
		}
	}

	public bool _pIsJustPressedLeft
	{
		get
		{
			return _isPressedLeft && !_wasPressedLeft;
		}
	}

	public bool _pIsJustReleasedLeft
	{
		get
		{
			return !_isPressedLeft && _wasPressedLeft;
		}
	}

	public bool _pIsPressedRight
	{
		get
		{
			return _isPressedRight;
		}
	}

	public bool _pIsJustPressedRight
	{
		get
		{
			return _isPressedRight && !_wasPressedRight;
		}
	}

	public bool _pIsJustReleasedRight
	{
		get
		{
			return !_isPressedRight && _wasPressedRight;
		}
	}

	public event System.Action _pOnJustPressedUp;
	public event System.Action _pOnJustReleasedUp;
	public event System.Action _pOnJustPressedDown;
	public event System.Action _pOnJustReleasedDown;
	public event System.Action _pOnJustPressedLeft;
	public event System.Action _pOnJustReleasedLeft;
	public event System.Action _pOnJustPressedRight;
	public event System.Action _pOnJustReleasedRight;
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public AnalogDpad( string horzAxis = HORZ_AXIS_DEFAULT, string vertAxis = VERT_AXIS_DEFAULT, float pressValueThreshold = PRESS_VALUE_THRESHOLD_DEFAULT )
	{
		_horzAxis = horzAxis;
		_vertAxis = vertAxis;
		_pressValueThreshold = pressValueThreshold;
	}
	
	public void OnUpdate()
	{
		_wasPressedUp = _isPressedUp;
		_wasPressedDown = _isPressedDown;
		_wasPressedLeft = _isPressedLeft;
		_wasPressedRight = _isPressedRight;

#if UNITY_EDITOR
		_isPressedUp = Input.GetKey( KeyCode.UpArrow );
		_isPressedDown = Input.GetKey( KeyCode.DownArrow );
		_isPressedLeft = Input.GetKey( KeyCode.LeftArrow );
		_isPressedRight = Input.GetKey( KeyCode.RightArrow );
#else
#if USE_CROSS_PLATFORM_INPUT
		float	axisHorz = CrossPlatformInputManager.GetAxis( _horzAxis );
		float	axisVert = CrossPlatformInputManager.GetAxis( _vertAxis );
#else
		float	axisHorz = Input.GetAxis( _horzAxis );
		float	axisVert = Input.GetAxis( _vertAxis );
#endif

		_isPressedUp = axisVert >= _pressValueThreshold;
		_isPressedDown = axisVert <= -_pressValueThreshold;
		_isPressedLeft = axisHorz <= -_pressValueThreshold;
		_isPressedRight = axisHorz >= _pressValueThreshold;
#endif
		if ( _pIsJustPressedUp && _pOnJustPressedUp != null )
		{
			_pOnJustPressedUp();
		}

		if ( _pIsJustReleasedUp && _pOnJustReleasedUp != null )
		{
			_pOnJustReleasedUp();
		}

		if ( _pIsJustPressedDown && _pOnJustPressedDown != null )
		{
			_pOnJustPressedDown();
		}

		if ( _pIsJustReleasedDown && _pOnJustReleasedDown != null )
		{
			_pOnJustReleasedDown();
		}

		if ( _pIsJustPressedLeft && _pOnJustPressedLeft != null )
		{
			_pOnJustPressedLeft();
		}

		if ( _pIsJustReleasedLeft && _pOnJustReleasedLeft != null )
		{
			_pOnJustReleasedLeft();
		}

		if ( _pIsJustPressedRight && _pOnJustPressedRight != null )
		{
			_pOnJustPressedRight();
		}

		if ( _pIsJustReleasedRight && _pOnJustReleasedRight != null )
		{
			_pOnJustReleasedRight();
		}
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

