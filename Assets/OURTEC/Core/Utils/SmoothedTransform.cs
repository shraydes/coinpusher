﻿////////////////////////////////////////////
// 
// SmoothedTransform.cs
//
// Created: dd/mm/yyyy ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_SmoothedTransform
#endif

using UnityEngine;
using AmuzoEngine;

public class SmoothedTransform : MonoBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[SmoothedTransform] ";

	public enum ESmoothingType
	{
		IGNORE = 0,
		NO_SMOOTH = 1,
		EXP = 2,
		LAMBDA = 3,
	}

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
	
	[System.Serializable]
	public struct SmoothingSpec
	{
		public ESmoothingType	_type;

		public float	_param;
	}

	//
	// MEMBERS 
	//

	[SerializeField]
	private Transform	_targetTransform;
	
	[SerializeField]
	private EUpdateType	_updateType;

	[SerializeField]
	private SmoothingSpec	_smoothingSpecPosX;

	[SerializeField]
	private SmoothingSpec	_smoothingSpecPosY;

	[SerializeField]
	private SmoothingSpec	_smoothingSpecPosZ;

	[SerializeField]
	private SmoothingSpec	_smoothingSpecRotX;

	[SerializeField]
	private SmoothingSpec	_smoothingSpecRotY;

	[SerializeField]
	private SmoothingSpec	_smoothingSpecRotZ;

	//
	// PROPERTIES
	//

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	private void FixedUpdate()
	{
		if ( _updateType == EUpdateType.FIXED_UPDATE )
		{
			DoSmoothing( Time.fixedDeltaTime );
		}
	}

	private void Update()
	{
		if ( _updateType == EUpdateType.UPDATE )
		{
			DoSmoothing( Time.deltaTime );
		}
	}

	private void LateUpdate()
	{
		if ( _updateType == EUpdateType.LATE_UPDATE )
		{
			DoSmoothing( Time.deltaTime );
		}
	}

	// ** PUBLIC
	//

	public void SetTarget( Transform newTarget )
	{
		if ( _targetTransform != newTarget )
		{
			_targetTransform = newTarget;
		}
	}

	// ** INTERNAL
	//

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static float SmoothLinear( float valueCurrent, float valueTarget, ESmoothingType smoothType, float smoothParam, float dt )
	{
		switch ( smoothType )
		{
		case ESmoothingType.NO_SMOOTH:
			return valueTarget;
		case ESmoothingType.EXP:
			return valueCurrent - smoothParam * ( valueCurrent - valueTarget );
		case ESmoothingType.LAMBDA:
			return MathHelper.LambdaApproach( valueCurrent, valueTarget, smoothParam, dt );
		default:
			return valueCurrent;
		}
	}

	private static float SmoothAngular( float valueCurrent, float valueTarget, ESmoothingType smoothType, float smoothParam, float dt )
	{
		float	rotError = valueCurrent - valueTarget;
		while ( rotError > 180 ) rotError -= 360;
		while ( rotError < -180 ) rotError += 360;

		return valueTarget + SmoothLinear( rotError, 0f, smoothType, smoothParam, dt );
	}

	private void DoSmoothing( float dt )
	{
		Vector3	targetPos = _targetTransform.position;
		Vector3	targetRot = _targetTransform.eulerAngles;
		Vector3	newPos = transform.position;
		Vector3	newRot = transform.eulerAngles;

		newPos.x = SmoothLinear( newPos.x, targetPos.x, _smoothingSpecPosX._type, _smoothingSpecPosX._param, dt );
		newPos.y = SmoothLinear( newPos.y, targetPos.y, _smoothingSpecPosY._type, _smoothingSpecPosY._param, dt );
		newPos.z = SmoothLinear( newPos.z, targetPos.z, _smoothingSpecPosZ._type, _smoothingSpecPosZ._param, dt );
		newRot.x = SmoothAngular( newRot.x, targetRot.x, _smoothingSpecRotX._type, _smoothingSpecRotX._param, dt );
		newRot.y = SmoothAngular( newRot.y, targetRot.y, _smoothingSpecRotY._type, _smoothingSpecRotY._param, dt );
		newRot.z = SmoothAngular( newRot.z, targetRot.z, _smoothingSpecRotZ._type, _smoothingSpecRotZ._param, dt );

		transform.position = newPos;
		transform.eulerAngles = newRot;
	}
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

