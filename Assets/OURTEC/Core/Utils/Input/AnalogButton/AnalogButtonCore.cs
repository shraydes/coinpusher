﻿////////////////////////////////////////////
// 
// AnalogButtonCore.cs
//
// Created: 24/12/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_AnalogButtonCore
#endif

using UnityEngine;
using Params = AmuzoEngine.AnalogButtonParams;

namespace AmuzoEngine
{
	public class AnalogButtonCore
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[AnalogButtonCore] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		private Params	_params;

		private bool	_buttonDown;

		private float	_currTime;
	
		//
		// PROPERTIES
		//

		public float _pValue
		{
			get
			{
				return _buttonDown ? _params._pDownCurve.Evaluate( _currTime ) : _params._pUpCurve.Evaluate( _currTime );
			}
		}
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public AnalogButtonCore()
		{
			_params = new Params();
		}

		public void SetParams( Params newParams )
		{
			_params.CopyFrom( newParams );
		}

		public void SetFullyUp()
		{
			_buttonDown = false;
			_currTime = 0f;
		}

		public void Update( bool buttonDown, float dt )
		{
			_buttonDown = buttonDown;

			if ( _buttonDown )
			{
				if ( _params._pDownTime > 0f )
				{
					_currTime += ( dt / _params._pDownTime );
					if ( _currTime > 1f ) _currTime = 1f;
				}
				else
				{
					_currTime = 1f;
				}
			}
			else
			{
				if ( _params._pUpTime > 0f )
				{
					_currTime -= ( dt / _params._pUpTime );
					if ( _currTime < 0f ) _currTime = 0f;
				}
				else
				{
					_currTime = 0f;
				}
			}
		}
	
		// ** INTERNAL
		//
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//
	
		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
