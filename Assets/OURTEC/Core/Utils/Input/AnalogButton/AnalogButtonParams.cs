﻿////////////////////////////////////////////
// 
// AnalogButtonParams.cs
//
// Created: 24/12/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_AnalogButtonParams
#endif

using UnityEngine;

namespace AmuzoEngine
{
	[System.Serializable]
	public class AnalogButtonParams
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[AnalogButtonParams] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		[SerializeField]
		private float	_downTime;

		[SerializeField]
		private AnimationCurve	_downCurve;
	
		[SerializeField]
		private float	_upTime;

		[SerializeField]
		private AnimationCurve	_upCurve;
	
		//
		// PROPERTIES
		//

		public float _pDownTime
		{
			get
			{
				return _downTime;
			}
			set
			{
				_downTime = value;
			}
		}

		public AnimationCurve _pDownCurve
		{
			get
			{
				return _downCurve;
			}
		}

		public float _pUpTime
		{
			get
			{
				return _upTime;
			}
			set
			{
				_upTime = value;
			}
		}

		public AnimationCurve _pUpCurve
		{
			get
			{
				return _upCurve;
			}
		}
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		// ** PUBLIC
		//

		public AnalogButtonParams()
		{
			_downCurve = new AnimationCurve();
			_upCurve = new AnimationCurve();
		}

		public void CopyFrom( AnalogButtonParams src )
		{
			_downTime = src._downTime;
			_downCurve.keys = src._downCurve.keys;
			_upTime = src._upTime;
			_upCurve.keys = src._upCurve.keys;
		}
	
		// ** INTERNAL
		//
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//
	
		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
