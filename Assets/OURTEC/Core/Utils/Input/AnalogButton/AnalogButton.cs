﻿////////////////////////////////////////////
// 
// AnalogButton.cs
//
// Created: 24/12/2018 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_AnalogButton
#endif

using UnityEngine;
using Params = AmuzoEngine.AnalogButtonParams;

namespace AmuzoEngine
{
	public class AnalogButton : MonoBehaviour
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[AnalogButton] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		[SerializeField]
		private Params	_params;

		[SerializeField]
		private AnalogButton	_oppositeButton;

		private AnalogButtonCore	_core;

		private bool	_buttonDown;

		private bool	_isToggle;

		#if UNITY_EDITOR
		public KeyCode	_editorKey;

		public bool	_updateParams;
		#endif

		//
		// PROPERTIES
		//

		public float _pValue
		{
			get
			{
				return _core._pValue;
			}
		}

		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// FUNCTIONS 
		//

		// ** UNITY
		//

		private void Awake()
		{
			_core = new AnalogButtonCore();
		}

		private void Start()
		{
			_core.SetParams( _params );
		}

		private void Update()
		{
			#if UNITY_EDITOR
			if ( _editorKey != KeyCode.None )
			{
				if ( _isToggle )
				{
					if ( Input.GetKeyDown( _editorKey ) )
					{
						_buttonDown = !_buttonDown;
					}
				}
				else
				{
					_buttonDown = Input.GetKey( _editorKey );
				}
			}

			if ( _updateParams )
			{
				_core.SetParams( _params );
			}
			#endif

			if ( _oppositeButton != null && _oppositeButton._buttonDown && !_buttonDown )
			{
				_core.SetFullyUp();
			}

			_core.Update( _buttonDown, Time.deltaTime );
		}

		// ** PUBLIC
		//

		public void OnPressed()
		{
			if ( _isToggle )
			{
				_buttonDown = !_buttonDown;
			}
			else
			{
				_buttonDown = true;
			}

			_core.Update( _buttonDown, 0f );
		}

		public void OnReleased()
		{
			if ( !_isToggle )
			{
				_buttonDown = false;
				_core.Update( _buttonDown, 0f );
			}
		}

		// ** INTERNAL
		//

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
