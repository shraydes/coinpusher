﻿////////////////////////////////////////////
// 
// JsonExtensions.cs
//
// Created: 15/02/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System;
using System.IO;
using System.Collections; 
using System.Collections.Generic; 
using System.Collections.Specialized;
using UnityEngine;
using LitJson;

public static partial class Extensions
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[JsonExtensions] ";

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	private static JsonWriter _jsonWriter = null;

	private static StringWriter _jsonStringWriter = null;

	private static bool _jsonIsBeginObject = false;
	
	//
	// PROPERTIES
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	public static JsonData JsonAtPath(this JsonData root, string path)
	{
		path = path.Replace(".", "/");
		return JsonAtPath(root, path.Split('/'), 0);
	}
	
	public static JsonData JsonAtPath(JsonData root, string[] nodes, int startIndex)
	{
		if(root == null)
		{
			return null;
		}
		JsonData ret = root;
		for(int i = startIndex; i<nodes.Length; ++i)
		{
			ret = ret.TryGet(nodes[i]);
			if(ret == null)
			{
				return null;
			}
		}
		return ret;
	}
	
	public static JsonData LoadJson(string json)
	{
		if(json == null || json == "") return null;
		JsonReader reader = new JsonReader(json);
		return JsonMapper.ToObject(reader);
	}
	
	public static string PrettyPrint(JsonData data)
	{
		StringWriter sw = new StringWriter();
		JsonWriter writer = new JsonWriter(sw);
		writer.Validate = true;
		writer.PrettyPrint = true;
		data.ToJson(writer);
		return sw.ToString();	
	}
	
	public static string ToDebug(this JsonData data, string title = null)
	{
		StringWriter sw = new StringWriter();
		if(title != null) sw.WriteLine (title + ":");
		DebugJson(data, sw, 0);
		return sw.ToString();
	}
	
	public static JsonWriter BeginJson( bool isBeginObject = true, bool isWantValidate = true )
	{
		if(_jsonStringWriter != null)
		{
			Debug.LogError ("BeginJson: need to EndJson() first!");
			return null;
		}
		_jsonStringWriter = new StringWriter();
		_jsonWriter = new JsonWriter(_jsonStringWriter);
		_jsonWriter.Validate = isWantValidate;
		_jsonIsBeginObject = isBeginObject;
		if ( _jsonIsBeginObject )
		{
			_jsonWriter.WriteObjectStart();
		}
		return _jsonWriter;
	}
	
	public static string EndJson()
	{
		if(_jsonStringWriter == null)
		{
			Debug.LogError ("EndJson: need to BeginJson() first!");
			return null;
		}
		if ( _jsonIsBeginObject )
		{
			_jsonWriter.WriteObjectEnd();
		}
		string str = _jsonStringWriter.ToString();
		_jsonStringWriter = null;
		_jsonWriter = null;
		return str; 
	}
	
	public static void WriteValue(this JsonWriter writer, string propertyName, object obj)
	{
		writer.WritePropertyName(propertyName);
		if(obj is bool)
		{
			writer.Write((bool)obj);
		}
		else if(obj is decimal)
		{
			writer.Write((decimal)obj);
		}
		else if(obj is double)
		{
			writer.Write((double)obj);
		}
		else if(obj is int)
		{
			writer.Write((int)obj);
		}
		else if(obj is long)
		{
			writer.Write((long)obj);
		}
		else if(obj is ulong)
		{
			writer.Write((ulong)obj);
		}
		else
		{
			writer.Write(obj.ToString());
		}
	}
	
	public static void WriteValue(this JsonWriter writer, string propertyName, bool boolean)
	{
		writer.WritePropertyName(propertyName);
		writer.Write(boolean);
	}
	
	public static void WriteValue(this JsonWriter writer, string propertyName, decimal number)
	{
		writer.WritePropertyName(propertyName);
		writer.Write(number);
	}
	
	public static void WriteValue(this JsonWriter writer, string propertyName, double number)
	{
		writer.WritePropertyName(propertyName);
		writer.Write(number);
	}
	
	public static void WriteValue(this JsonWriter writer, string propertyName, int number)
	{
		writer.WritePropertyName(propertyName);
		writer.Write(number);
	}
	
	public static void WriteValue(this JsonWriter writer, string propertyName, long number)
	{
		writer.WritePropertyName(propertyName);
		writer.Write(number);
	}
	
	public static void WriteValue(this JsonWriter writer, string propertyName, ulong number)
	{
		writer.WritePropertyName(propertyName);
		writer.Write(number);
	}
	
	public static void WriteValue(this JsonWriter writer, string propertyName, string str)
	{
		writer.WritePropertyName(propertyName);
		writer.Write(str);
	}

	public static void WriteArray(this JsonWriter writer, string propertyName, int[] array)
	{
		writer.WritePropertyName(propertyName);
		writer.WriteArrayStart();
		for(int i = 0; i < array.Length; ++i)
		{
			int value = array[i];
			writer.Write(value);
		}
		writer.WriteArrayEnd();
	}
	
	public static void WriteArray(this JsonWriter writer, string propertyName, string[] array)
	{
		writer.WritePropertyName(propertyName);
		writer.WriteArrayStart();
		for(int i = 0; i < array.Length; ++i)
		{
			string value = array[i];
			writer.Write(value);
		}
		writer.WriteArrayEnd();
	}
	
	public static void WriteList(this JsonWriter writer, string propertyName, List<int> list)
	{
		writer.WritePropertyName(propertyName);
		writer.WriteArrayStart();
		for(int i = 0; i < list.Count; ++i)
		{
			int value = list[i];
			writer.Write(value);
		}
		writer.WriteArrayEnd();
	}
	
	public static void WriteArray(this JsonWriter writer, string propertyName, bool[] array)
	{
		writer.WritePropertyName(propertyName);
		writer.WriteArrayStart();
		for(int i = 0; i < array.Length; ++i)
		{
			bool value = array[i];
			writer.Write(value);
		}
		writer.WriteArrayEnd();
	}
	
	public static void WriteList(this JsonWriter writer, string propertyName, List<bool> list)
	{
		writer.WritePropertyName(propertyName);
		writer.WriteArrayStart();
		for(int i = 0; i < list.Count; ++i)
		{
			bool value = list[i];
			writer.Write(value);
		}
		writer.WriteArrayEnd();
	}
	
	public static bool WriteJsonData( this JsonWriter writer, JsonData jd, Func<JsonData, string, bool> errorHandler = null )
	{
		Func<string, bool>	handleError = msg => {
			return errorHandler != null && errorHandler( jd, msg );
		};
		
		if ( jd == null ) return handleError( "Null JsonData" );
		
		switch ( jd.GetJsonType() )
		{
		default:
		case JsonType.None:
			return handleError( "Invalid JsonData" );
		case JsonType.Object:
		{
			writer.WriteObjectStart();
			foreach ( DictionaryEntry kv in (IDictionary)jd )
			{
				if ( kv.Key == null || !(kv.Key is String) ) return handleError( "Null or invalid object key" );
				if ( kv.Value == null || !(kv.Value is JsonData) ) return handleError( "Null or invalid object value" );
				writer.WritePropertyName( (string)kv.Key );
				if ( WriteJsonData( writer, (JsonData)kv.Value, errorHandler ) == false ) return false;
			}
			writer.WriteObjectEnd();
		}
			break;
		case JsonType.Array:
		{
			writer.WriteArrayStart();
			for ( int i = 0; i < jd.Count; ++i )
			{
				if ( jd[i] == null ) return handleError( "Null array element" );
				if ( WriteJsonData( writer, jd[i], errorHandler ) == false ) return false;
			}
			writer.WriteArrayEnd();
		}
			break;
		case JsonType.String:
			writer.Write( (string)jd );
			break;
		case JsonType.Int:
			writer.Write( (int)jd );
			break;
		case JsonType.Long:
			writer.Write( (long)jd );
			break;
		case JsonType.Double:
			writer.Write( (double)jd );
			break;
		case JsonType.Boolean:
			writer.Write( (bool)jd );
			break;
		}
		
		return true;
	}
	
	public static string SafeToJson( this JsonData jd, Action<string> errorHandler = null )
	{
		if ( jd == null ) return "";
		
		Action<string>	handleError = msg => {
			if ( errorHandler != null )
			{
				errorHandler( msg );
			}
		};
		
		Func<JsonData, string, bool>	errorHandler2 = ( jd2, msg ) =>
		{
			handleError( msg );
			return false;
		};
		
		JsonWriter	writer = BeginJson( false, false );
		
		if ( writer == null )
		{
			handleError( "Cannot write json" );
			return "";
		}
		
		if ( writer.WriteJsonData( jd, errorHandler2 ) == false )
		{
			EndJson();
			handleError( "Failed to write json" );
			return "";
		}
		
		return EndJson();
	}
	
	public static JsonData TryGet(this JsonData data, string key)
	{
		if(data != null && data.Contains(key))
		{
			return data[key];
		}
		
		return null;
	}
	
	public static JsonData TryGet(this JsonData data, string key, JsonType type)
	{
		JsonData	childData = data.TryGet( key );
		
		if ( childData == null || childData.GetJsonType() != type ) return null;
		
		return childData;
	}
	
	public static string TryGetString(this JsonData data, string key, string defaultValue = "")
	{
		if(data != null && data.Contains(key))
		{
			JsonData val = data[key];
			if(val.GetJsonType() == JsonType.String)
			{
				return (string)val;
			}
		}
      
		return defaultValue;
    }
	
	public static int TryGetInt(this JsonData data, string key, int defaultValue = 0)
	{
		if(data != null && data.Contains(key))
		{
			JsonData val = data[key];
			if(val.GetJsonType() == JsonType.Int)
			{
				return (int)val;
			}
		}
		
		return defaultValue;
    }

	public static double TryGetDouble(this JsonData data, string key, double defaultValue = 0.0)
	{
		if(data != null && data.Contains(key))
		{
			JsonData val = data[key];
			if(val.GetJsonType() == JsonType.Double)
			{
				return (double)val;
			}
		}
      
		return defaultValue;
	}
	
	public static bool TryGetBool(this JsonData data, string key, bool defaultValue = false)
	{
		if(data != null && data.Contains(key))
		{
			JsonData val = data[key];
			if(val.GetJsonType() == JsonType.Boolean)
			{
				return (bool)val;
			}
		}
		
		return defaultValue;
    }
   
    // children of root will be add to this data
	public static void AddChildren(this JsonData data, JsonData root)
	{
		foreach(DictionaryEntry keyValue in (IDictionary)root)
		{
			string key = (string)keyValue.Key;
			data[key] = keyValue.Value as JsonData;
		}
	}	
	
	// enumerate action on children of root
	public static void Enumerate(this JsonData data, System.Action<string, JsonData> action)
	{
		foreach(DictionaryEntry keyValue in (IDictionary)data)
		{
			action((string)keyValue.Key, keyValue.Value as JsonData);
		}
	}	
	
	public static JsonData DeepCopyJson( JsonData src )
	{
		JsonData	dst = null;
		
		if ( src != null )
		{
			JsonType	type = src.GetJsonType();
			
			switch ( type )
			{
			case JsonType.Array:
			{
				dst = new JsonData();
				dst.SetJsonType( JsonType.Array );
				for ( int i = 0; i < src.Count; ++i )
				{
					dst.Add( DeepCopyJson( src[i] ) );
				}
			}
				break;
			case JsonType.Boolean:
			{
				dst = new JsonData( (bool)src );
			}
				break;
			case JsonType.Double:
			{
				dst = new JsonData( (double)src );
			}
				break;
			case JsonType.Int:
			{
				dst = new JsonData( (int)src );
			}
				break;
			case JsonType.Long:
			{
				dst = new JsonData( (long)src );
			}
				break;
			case JsonType.None:
			{
				dst = new JsonData();
			}
				break;
			case JsonType.Object:
			{
				dst = new JsonData();
				dst.SetJsonType( JsonType.Object );
				foreach ( DictionaryEntry kvp in (IDictionary)src )
				{
					dst[ (string)kvp.Key ] = DeepCopyJson( (JsonData)kvp.Value );
				}
			}
				break;
			case JsonType.String:
			{
				dst = new JsonData( (string)src );
			}
				break;
			default:
				//				throw new Exception( "Unexpected JsonType: " + type.ToString() );
				break;
			}
		}
		
		return dst;
	}
	
	public static JsonData MergeJsonObjects( JsonData objA, JsonData objB )
	{
		JsonData	merged = null;
		
		if ( objA != null && objA.IsObject )
		{
			merged = DeepCopyJson( objA );
			
			if ( objB != null && objB.IsObject )
			{
				MergeJsonObjectChildren( merged, objB );
			}
		}
		else if ( objB != null && objB.IsObject )
		{
			merged = DeepCopyJson( objB );
		}
		else
		{
			merged = new JsonData();
			merged.SetJsonType( JsonType.Object );
		}
		
		return merged;
	}
	
	public static bool MergeJsonObjectChildren( JsonData objA, JsonData objB )
	{
		if ( objA == null ) return false;
		if ( !objA.IsObject ) return false;
		if ( objB == null ) return false;
		if ( !objB.IsObject ) return false;
		
		string		key;
		JsonData	childA, childB;
		
		foreach ( DictionaryEntry kvp in (IDictionary)objB )
		{
			key = (string)kvp.Key;
			childB = (JsonData)kvp.Value;
			if ( childB.IsObject && objA.Contains( key ) )
			{
				childA = objA[ key ];
				if ( childA.IsObject )
				{
					//merge object A-child with object B-child (fields of B-child are added to A-child)
					MergeJsonObjectChildren( childA, childB );
				}
				else
				{
					//replace non-object A-child with object B-child
					objA[ key ] = DeepCopyJson( childB );
				}
			}
			else
			{
				//create new A-child as copy of B-child
				objA[ key ] = DeepCopyJson( childB );
			}
		}
		
		return true;
	}
	
	public static void TryRemove( this JsonData thisData, string key )
	{
		if ( thisData == null ) return;
		if ( !thisData.IsObject ) return;
		IDictionary	dict = thisData as IDictionary;
		if ( !dict.Contains( key ) ) return;
		dict.Remove( key );
	}
	
	public static void TryRemove( this JsonData thisData, JsonData childData )
	{
		if ( thisData == null ) return;
		if ( !thisData.IsArray && !thisData.IsObject ) return;
		int	count = thisData.Count;
		int	index = 0;
		for ( ; index < count && thisData[index] != childData; ++index ) {}
		if ( index == count ) return;
		if ( thisData.IsArray )
		{
			IList	list = thisData as IList;
			list.RemoveAt( index );
		}
		else if ( thisData.IsObject )
		{
			IOrderedDictionary	dict = thisData as IOrderedDictionary;
			dict.RemoveAt( index );
		}
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private static void DebugJson(IJsonWrapper obj, StringWriter sw, int indent)
	{
		sw.WriteLine("{0,14}", obj.GetJsonType());
		
		if (obj.IsArray) {
			
			foreach (object elem in (IList) obj)
				DebugJson ((JsonData) elem, sw, indent + 1);
			
			return;
		}
		
		if (obj.IsObject) {
			
			foreach (DictionaryEntry entry in ((IDictionary) obj)) {
				
				DebugJson ((JsonData) entry.Value, sw, indent + 1);
			}
			
			return;
		}
	}
	
}

