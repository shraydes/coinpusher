﻿////////////////////////////////////////////
// 
// SimpleMathExpression.cs
//
// Created: 28/07/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_SimpleMathExpression
#endif

using System.Collections.Generic;
using FloatT = System.Single;
using Math = UnityEngine.Mathf;
using EParseOptions = AmuzoEngine.SimpleMathParser.EParseOptions;

namespace AmuzoEngine
{
	public class SimpleMathExpression
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[SimpleMathExpression] ";

		private const int	OPERATOR_PRECEDENCE_OPEN_BRACKET = 10;

		//
		// SINGLETON DECLARATION
		//

		private static Operator[]	_sOperators = new Operator[] {
			new Operator() { _str = "+", _prec = 1, _left = true },
			new Operator() { _str = "-", _prec = 1, _left = true },
			new Operator() { _str = "*", _prec = 2, _left = true },
			new Operator() { _str = "/", _prec = 2, _left = true },
			new Operator() { _str = "^", _prec = 3, _left = false },
			new Operator() { _str = "~", _prec = 4, _left = false }
		};

		//
		// NESTED CLASSES / STRUCTS
		//

		public interface IValueStack
		{
			void	PushValues( params FloatT[] values );
			bool	PopValues( int popCount, FloatT[] values );
		}

		private class ValueStack : Stack<FloatT>, IValueStack
		{
			public ValueStack( int count ) : base( count )
			{
			}

			public bool PopValues(int popCount, float[] values)
			{
				if ( this.Count < popCount ) return false;
				if ( values.Length < popCount ) return false;

				while ( popCount-- > 0 )
				{
					values[ popCount ] = this.Pop();
				}

				return true;
			}

			public void PushValues(params float[] values)
			{
				for ( int i = 0; i < values.Length; ++i )
				{
					this.Push( values[i] );
				}
			}
		}

		private struct RpnToken
		{
			public string	_str;
			public FloatT?	_value;
		}

		private class Operator
		{
			public string	_str;
			public int		_prec;
			public bool		_left;
		}

		public delegate bool	DNamedTokenHandler( string name, IValueStack valueStack );

		private delegate bool	DGetNextToken( out string token );
			
		//
		// MEMBERS 
		//

		private string	_expr;

		private EParseOptions	_parseOptions;

		private List<RpnToken>	_rpnTokens;

		private string	_cachedOpStr;

		private Operator	_cachedOp;
	
		//
		// PROPERTIES
		//
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//

		public static bool TryEvaluate( string expr, EParseOptions parseOptions, DNamedTokenHandler namedTokenHandler, out FloatT result )
		{
			SimpleMathExpression	inst = new SimpleMathExpression( expr, parseOptions );

			return inst.TryEvaluate( namedTokenHandler, out result );
		}

		public static FloatT Evaluate( string expr, EParseOptions parseOptions, DNamedTokenHandler namedTokenHandler )
		{
			SimpleMathExpression	inst = new SimpleMathExpression( expr, parseOptions );

			return inst.Evaluate( namedTokenHandler );
		}

		public static FloatT EvaluateForX( string expr, FloatT x )
		{
			SimpleMathExpression	inst = new SimpleMathExpression( expr, EParseOptions.ALGEBRA );

			return inst.EvaluateForX( x );
		}

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public SimpleMathExpression( string expr, EParseOptions parseOptions )
		{
			_expr = expr;
			_parseOptions = parseOptions;

			CreateRpn();
		}

		public bool TryEvaluate( DNamedTokenHandler namedTokenHandler, out FloatT result )
		{
			return EvaluateRpn( namedTokenHandler, out result );
		}

		public FloatT Evaluate( DNamedTokenHandler namedTokenHandler )
		{
			FloatT	result;
			if ( TryEvaluate( namedTokenHandler, out result ) == false ) throw new System.Exception( "Failed to evaluate expression: " + _expr );

			return result;
		}

		public float EvaluateForX( FloatT x )
		{
			DNamedTokenHandler	namedTokenHandler = ( name, stack ) => {
				if ( name != "x" && name != "X" ) throw new System.Exception( "Unexpected variable: " + name );
				stack.PushValues( x );
				return true;
			};

			return Evaluate( namedTokenHandler );
		}
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private SimpleMathExpression()
		{
		}

		private void CreateRpn()
		{
			_rpnTokens = new List<RpnToken>();

			Stack<Operator>	opStack = new Stack<Operator>();

			bool	isOk = SimpleMathParser.ParseExpression( _expr, onNumber:( str, val ) => {
				//UnityEngine.Debug.Log( "Number: " + str );
				_rpnTokens.Add( new RpnToken() { _str = str, _value = val } );
			}, onOperator:( opStr, opArity ) => {
				if ( opArity < 0 )
				{
					//UnityEngine.Debug.Log( "Unary op: " + opStr );
					//handle negation
					if ( opStr == "-" ) opStr = "~";
					opStack.Push( new Operator() { _str = opStr, _prec = GetOperatorPrecedence( opStr ), _left = IsOperatorLeftAssociative( opStr ) } );
				}
				else
				{
					//UnityEngine.Debug.Log( "Binary op: " + opStr );
					int	opPrec = GetOperatorPrecedence( opStr );
					while ( opStack.Count > 0 && opStack.Peek()._prec != OPERATOR_PRECEDENCE_OPEN_BRACKET && ( opStack.Peek()._prec >= opPrec || (opStack.Peek()._prec == opPrec && opStack.Peek()._left == true ) ) )
					{
						_rpnTokens.Add( new RpnToken() { _str = opStack.Pop()._str } );
					}
					opStack.Push( new Operator() { _str = opStr, _prec = opPrec, _left = IsOperatorLeftAssociative( opStr ) } );
				}
			}, onOpenParenthesis: funcName => {
				//UnityEngine.Debug.Log( "Open bracket: " + funcName );
				opStack.Push( new Operator() { _str = string.IsNullOrEmpty( funcName ) ? "(" : funcName, _prec = OPERATOR_PRECEDENCE_OPEN_BRACKET, _left = false } );
			}, onCloseParenthesis:() => {
				while ( opStack.Count > 0 && opStack.Peek()._prec != OPERATOR_PRECEDENCE_OPEN_BRACKET )
				{
					_rpnTokens.Add( new RpnToken() { _str = opStack.Pop()._str } );
				}
				string	funcName = opStack.Pop()._str;
				if ( funcName != "(" )
				{
					//UnityEngine.Debug.Log( "Close bracket: " + funcName );
					//handle functions
					_rpnTokens.Add( new RpnToken() { _str = funcName } );
				}
				else
				{
					//UnityEngine.Debug.Log( "Close bracket: " );
				}
			}, onSeparator:() => {
				//UnityEngine.Debug.Log( "Separator" );
				while ( opStack.Count > 0 && opStack.Peek()._prec != OPERATOR_PRECEDENCE_OPEN_BRACKET )
				{
					_rpnTokens.Add( new RpnToken() { _str = opStack.Pop()._str } );
				}
			}, onVariable: varName => {
				//UnityEngine.Debug.Log( "Variable: " + varName );
				//handle variables
				_rpnTokens.Add( new RpnToken() { _str = varName } );
			}, onEnd: endIdx => {
				//UnityEngine.Debug.Log( "End at: " + endIdx + "/" + _expr.Length );
			}, parseOptions:_parseOptions );

			if ( !isOk )
			{
				throw new System.Exception( "Failed to parse expression: " + _expr );
			}

			while ( opStack.Count > 0 )
			{
				_rpnTokens.Add( new RpnToken() { _str = opStack.Pop()._str } );
			}
		}

		private Operator FindOperator( string opStr )
		{
			if ( opStr == _cachedOpStr ) return _cachedOp;

			int	opIdx = System.Array.FindIndex( _sOperators, o => o._str == opStr );

			if ( opIdx < 0 ) return null;

			_cachedOpStr = opStr;
			_cachedOp = _sOperators[ opIdx ];

			return _cachedOp;
		}

		private bool IsOperator( string token )
		{
			return FindOperator( token ) != null;
		}

		private int GetOperatorPrecedence( string op )
		{
			return FindOperator( op )._prec;
		}

		private bool IsOperatorLeftAssociative( string op )
		{
			return FindOperator( op )._left;
		}

		private bool EvaluateRpn( DNamedTokenHandler namedTokenHandler, out FloatT result )
		{
			ValueStack	valueStack = new ValueStack( _rpnTokens.Count );

			FloatT[]	args = new FloatT[2];

			for ( int i = 0; i < _rpnTokens.Count; ++i )
			{
				RpnToken	token = _rpnTokens[i];

				if ( token._value.HasValue )
				{
					valueStack.Push( token._value.Value );
				}
				else
				{
					switch ( token._str )
					{
					case "+":
						valueStack.PopValues( 2, args );
						valueStack.Push( args[0] + args[1] );
						break;
					case "-":
						valueStack.PopValues( 2, args );
						valueStack.Push( args[0] - args[1] );
						break;
					case "*":
						valueStack.PopValues( 2, args );
						valueStack.Push( args[0] * args[1] );
						break;
					case "/":
						valueStack.PopValues( 2, args );
						valueStack.Push( args[0] / args[1] );
						break;
					case "^":
						valueStack.PopValues( 2, args );
						valueStack.Push( Math.Pow( args[0], args[1] ) );
						break;
					case "~":
						args[0] = valueStack.Pop();
						valueStack.Push( -args[0] );
						break;
					case "int":
						args[0] = valueStack.Pop();
						valueStack.Push( (float)(int)args[0] );
						break;
					case "min":
						valueStack.PopValues( 2, args );
						valueStack.Push( Math.Min( args[0], args[1] ) );
						break;
					case "max":
						valueStack.PopValues( 2, args );
						valueStack.Push( Math.Max( args[0], args[1] ) );
						break;
					case "sin":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Sin( args[0] ) );
						break;
					case "cos":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Cos( args[0] ) );
						break;
					case "tan":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Tan( args[0] ) );
						break;
					case "asin":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Asin( args[0] ) );
						break;
					case "acos":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Acos( args[0] ) );
						break;
					case "atan":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Atan( args[0] ) );
						break;
					case "atan2":
						valueStack.PopValues( 2, args );
						valueStack.Push( Math.Atan2( args[0], args[1] ) );
						break;
					case "sqrt":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Sqrt( args[0] ) );
						break;
					case "abs":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Abs( args[0] ) );
						break;
					case "sign":
						args[0] = valueStack.Pop();
						valueStack.Push( Math.Sign( args[0] ) );
						break;
					default:
						if ( namedTokenHandler( token._str, valueStack ) == false )
						{
							throw new System.Exception( "Unhandled token: " + token._str );
						}
						break;
					}
				}
			}

			if ( valueStack.Count == 1 )
			{
				result = valueStack.Pop();

				return true;
			}

			result = default( FloatT );

			return false;
		}

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}

