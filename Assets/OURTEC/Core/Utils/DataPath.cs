﻿////////////////////////////////////////////
// 
// DataPath.cs
//
// Created: 16/01/2019 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_DataPath
#endif

using System.Collections.Generic;
using System.Text;

namespace AmuzoEngine
{
	//Examples below are for data such as:
	//{ car: { wheels: [ {...}, {...}, {...}, { radius: 0.5 } ] } }
	public interface IDataPathReadOnly
	{
		//_pElements the full path split into elements.
		//eg {"car","wheels","3","radius"}
		IList<string>	_pElements	{ get; }
		//_pIndices the full path split into elements, where an 
		//element only has a value if it is an array index.
		//eg {null,null,3,null}
		IList<int?>		_pIndices	{ get; }
		//_pFullPath is the full path to the data.
		//eg "car.wheels[3].radius"
		string			_pFullPath	{ get; }
		//Subpath can be called to create a path containing only 
		//a sequence of elements of the source path
		IDataPathReadOnly	Subpath( int startIndex, int length = -1 );
	}
	
	public class DataPath : IDataPathReadOnly
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[DataPath] ";
			
		//
		// MEMBERS 
		//

		private List<string>	_elements;

		private List<int?>	_indices;

		private StringBuilder	_fullPath;
	
		//
		// PROPERTIES
		//

		public bool _pIsEmpty
		{
			get
			{
				return _elements == null || _elements.Count == 0;
			}
		}

		public string _pFullPath
		{
			get
			{
				return _fullPath.ToString();
			}
		}
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//
	
		public DataPath()
		{
			_elements = new List<string>();
			_indices = new List<int?>();
			_fullPath = new StringBuilder( "" );
		}

		public DataPath( string pathStr )
		{
			ParsePathStr( pathStr );
		}

		public static DataPath operator+( DataPath path, string newElement )
		{
			DataPath	resultPath = new DataPath( path );

			resultPath.Add( newElement );

			return resultPath;
		}

		public static DataPath operator+( DataPath path, int newIndex )
		{
			DataPath	resultPath = new DataPath( path );

			resultPath.Add( newIndex );

			return resultPath;
		}

		public static DataPath operator--( DataPath path )
		{
			DataPath	resultPath = new DataPath( path );

			resultPath.RemoveLast();

			return resultPath;
		}

		// ** INTERNAL
		//
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private DataPath( DataPath srcPath )
		{
			Copy( srcPath );
		}

		private void Copy( DataPath srcPath )
		{
			_elements = new List<string>( srcPath._elements );
			_indices = new List<int?>( srcPath._indices );
			_fullPath = new StringBuilder( srcPath._fullPath.ToString() );
		}

		private void Reset()
		{
			_elements = new List<string>();
			_indices = new List<int?>();
			_fullPath = new StringBuilder( "" );
		}

		private void ParsePathStr( string pathStr )
		{
			Reset();

			string[]	pathStrParts = pathStr.Replace(" ", "").Split( '.', '[' );

			string	part;

			for ( int i = 0; i < pathStrParts.Length; ++i )
			{
				part = pathStrParts[i];

				if ( part.Length > 0 && part[ part.Length - 1 ] == ']' )
				{
					part = part.Substring( 0, part.Length - 1 );
				}

				Add( part );
			}
		}

		private void Add( string elem )
		{
			int	index;

			if ( int.TryParse( elem, out index ) )
			{
				Add( elem, index );
			}
			else
			{
				Add( elem, null );
			}
		}

		private void Add( int index )
		{
			Add( index.ToString(), index );
		}

		private void Add( string elem, int? index )
		{
			_elements.Add( elem );
			_indices.Add( index );

			if ( index.HasValue )
			{
				_fullPath.Append( "[" + index.Value.ToString() + "]" );
			}
			else if ( _fullPath.Length > 0 )
			{
				_fullPath.Append( "." + elem );
			}
			else
			{
				_fullPath.Append( elem );
			}
		}

		private void RemoveLast()
		{
			if ( _elements.Count > 0 )
			{
				_elements.RemoveAt( _elements.Count - 1 );
			}

			if ( _indices.Count > 0 )
			{
				_indices.RemoveAt( _indices.Count - 1 );
			}

			if ( _fullPath.Length > 0 )
			{
				int	removeLength = 1;

				if ( _fullPath[ _fullPath.Length - 1 ] == ']' )
				{
					while ( removeLength < _fullPath.Length ) 
					{
						++removeLength;
						if ( _fullPath[ _fullPath.Length - removeLength ] == '[' ) break;
					}
				}
				else
				{
					while ( removeLength < _fullPath.Length ) 
					{
						++removeLength;
						if ( _fullPath[ _fullPath.Length - removeLength ] == '.' ) break;
					}
				}

				_fullPath.Remove( _fullPath.Length - removeLength, removeLength );
			}
		}
	
		//
		// INTERFACE IMPLEMENTATIONS
		//

		IList<string> IDataPathReadOnly._pElements
		{
			get
			{
				return _elements;
			}
		}

		IList<int?> IDataPathReadOnly._pIndices
		{
			get
			{
				return _indices;
			}
		}

		string IDataPathReadOnly._pFullPath
		{
			get
			{
				return _pFullPath;
			}
		}

		IDataPathReadOnly IDataPathReadOnly.Subpath( int startIndex, int length )
		{
			DataPath	subpath = new DataPath();

			int	endIndex = length < 0 ? _elements.Count : startIndex + length;

			if ( endIndex > _elements.Count )
			{
				endIndex = _elements.Count;
			}

			for ( int i = startIndex; i < endIndex; ++i )
			{
				subpath.Add( _elements[i], _indices[i] );
			}

			return subpath;
		}
	}

	public static class DataPathExtensions
	{
		public static System.Type GetType( this IDataPathReadOnly dataPath, System.Type rootType )
		{
			System.Type	currType = rootType;

			for ( int i = 0; i < dataPath._pElements.Count && currType != null; ++i )
			{
				if ( dataPath._pIndices[i].HasValue )
				{
					if ( currType.IsArrayOrList() )
					{
						currType = currType.GetArrayOrListElementType();
					}
					else
					{
						currType = null;
					}
				}
				else
				{
					if ( currType.HasFieldOrProperty( dataPath._pElements[i], type:null ) )
					{
						currType = currType.GetFieldOrPropertyType( dataPath._pElements[i] );
					}
					else
					{
						currType = null;
					}
				}
			}

			return currType;
		}

		public static object GetValue( this IDataPathReadOnly dataPath, object rootObject )
		{
			object	currObject = rootObject;

			for ( int i = 0; i < dataPath._pElements.Count && currObject != null; ++i )
			{
				if ( dataPath._pIndices[i].HasValue )
				{
					if ( currObject.IsArrayOrList() )
					{
						currObject = currObject.GetArrayOrListElement( dataPath._pIndices[i].Value );
					}
					else
					{
						currObject = null;
					}
				}
				else
				{
					if ( currObject.HasFieldOrProperty( dataPath._pElements[i], type:null ) )
					{
						currObject = currObject.GetFieldOrProperty( dataPath._pElements[i] );
					}
					else
					{
						currObject = null;
					}
				}
			}

			return currObject;
		}

		public static void SetValue( this IDataPathReadOnly dataPath, object rootObject, object newValue )
		{
			object	currObject = rootObject;

			int	lastIdx = dataPath._pElements.Count - 1;

			for ( int i = 0; i < lastIdx && currObject != null; ++i )
			{
				if ( dataPath._pIndices[i].HasValue )
				{
					if ( currObject.IsArrayOrList() )
					{
						currObject = currObject.GetArrayOrListElement( dataPath._pIndices[i].Value );
					}
					else
					{
						currObject = null;
					}
				}
				else
				{
					if ( currObject.HasFieldOrProperty( dataPath._pElements[i], type:null ) )
					{
						currObject = currObject.GetFieldOrProperty( dataPath._pElements[i] );
					}
					else
					{
						currObject = null;
					}
				}
			}

			if ( currObject != null )
			{
				if ( dataPath._pIndices[ lastIdx ].HasValue )
				{
					if ( currObject.IsArrayOrList() )
					{
						currObject.SetArrayOrListElement( dataPath._pIndices[ lastIdx ].Value, newValue );
					}
				}
				else
				{
					if ( currObject.HasFieldOrProperty( dataPath._pElements[ lastIdx ], type:null ) )
					{
						currObject.SetFieldOrProperty( dataPath._pElements[ lastIdx ], newValue );
					}
				}
			}
		}
	}
}
