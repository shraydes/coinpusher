﻿////////////////////////////////////////////
// 
// ResourceLoadedTexture.cs
//
// Created: 05/08/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_ResourceLoadedTexture
#endif

using UnityEngine;

namespace AmuzoEngine
{
	[System.Serializable]
	public class ResourceLoadedTexture
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[ResourceLoadedTexture] ";

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		[SerializeField]
		private string	_resourcePath;

		[SerializeField]
		private Texture	_preLoadedTexture;
	
		//
		// PROPERTIES
		//

		public Texture _pTexture
		{
			get
			{
				if ( _preLoadedTexture != null ) return _preLoadedTexture;
				if ( !string.IsNullOrEmpty( _resourcePath ) ) return Resources.Load<Texture>( _resourcePath );
				return null;
			}
		}

		public bool _pIsValid
		{
			get
			{
				return _preLoadedTexture != null || !string.IsNullOrEmpty( _resourcePath );
			}
		}
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

#if UNITY_EDITOR
		public void EditorPreLoadTexture()
		{
			if ( !string.IsNullOrEmpty( _resourcePath ) )
			{
				_preLoadedTexture = Resources.Load<Texture>( _resourcePath );
				_resourcePath = null;
			}
		}
#endif

		public override string ToString()
		{
			return _preLoadedTexture != null ? _preLoadedTexture.ToString() : _resourcePath;
		}

		// ** PROTECTED
		//

		// ** PRIVATE
		//
	
		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
