﻿////////////////////////////////////////////
// 
// ScalePopper.cs
//
// Created: 10/08/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_ScalePopper
#endif

using UnityEngine;
using AmuzoEngine;

public class ScalePopper : MonoBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[ScalePopper] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	[SerializeField]
	private float	_popScale = 2f;

	[SerializeField]
	private float	_popTime = 0f;

	[SerializeField]
	private float	_unpopTime = 0.2f;

	[SerializeField]
	private Easing.EaseType	_popEasing = Easing.EaseType.EaseInSine;

	[SerializeField]
	private Easing.EaseType	_unpopEasing = Easing.EaseType.EaseInSine;

	private Vector3	_baseScale = Vector3.one;

	private float	_currentScale;

	private float	_popClock;

	private bool	_isPopping;

	public System.Action	_onNextPopEnd;

	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void Awake()
	{
		_baseScale = transform.localScale;
	}

	void Update()
	{
		UpdatePop( Time.deltaTime );
	}

	// ** PUBLIC
	//

	public static void Pop( GameObject obj, float popScale, float popTime, float unpopTime, Easing.EaseType popEasing, Easing.EaseType unpopEasing, System.Action onEndPop )
	{
		ScalePopper	popper = obj.AddComponent<ScalePopper>();

		popper._popScale = popScale;
		popper._popTime = popTime;
		popper._unpopTime = unpopTime;
		popper._popEasing = popEasing;
		popper._unpopEasing = unpopEasing;

		popper.TriggerPop( () => {
			Object.Destroy( popper );
			if ( onEndPop != null )
			{
				onEndPop();
			}
		} );
	}

	public void TriggerPop( System.Action onEnd = null )
	{
		_isPopping = true;
		_popClock = 0f;
		if ( onEnd != null )
		{
			_onNextPopEnd += onEnd;
		}
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private void UpdatePop( float dt )
	{
		if ( _isPopping )
		{
			_popClock += dt;

			if ( _popClock < _popTime )
			{
				_currentScale = Easing.Ease( _popEasing, _popClock, _popTime, 1f, _popScale );
			}
			else
			{
				_currentScale = Easing.Ease(_unpopEasing, _popClock - _popTime, _unpopTime, _popScale, 1f);

				if ( _currentScale <= 1f )
				{
					_isPopping = false;
					if ( _onNextPopEnd != null )
					{
						System.Action	cachedAction = _onNextPopEnd;
						_onNextPopEnd = null;
						cachedAction();
					}
				}
			}

			if ( _currentScale < 1f )
			{
				_currentScale = 1f;
			}

			if ( _currentScale > _popScale )
			{
				_currentScale = _popScale;
			}
		}
		else
		{
			_currentScale = 1f;
		}

		transform.localScale = Vector3.Scale( _baseScale, new Vector3( _currentScale, _currentScale, _currentScale ) );
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

