﻿////////////////////////////////////////////
// 
// CameraUtils.cs
//
// Created: dd/mm/yyyy ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
#if !SUBMISSION
#define DEBUG_CameraUtils
#endif

using UnityEngine;

namespace AmuzoEngine
{
	public static class CameraUtils
	{
		public static bool ViewToWorldPositionOnPlane( Camera camera, Vector2 viewPos, Plane worldPlane, out Vector3 worldPos )
		{
			if ( camera == null )
			{
				worldPos = Vector3.zero;
				return false;
			}

			Ray	ray = camera.ViewportPointToRay( viewPos );

			float	rayDist;

			if ( !worldPlane.Raycast( ray, out rayDist ) )
			{
				worldPos = Vector3.zero;
				return false;
			}

			worldPos = ray.origin + rayDist * ray.direction;
			return true;
		}
	}
}
