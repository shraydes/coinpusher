﻿////////////////////////////////////////////
// 
// GlobalDownloadQueue.cs
//
// Created: 29/03/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class GlobalDownloadQueue : AmuzoMonoSingleton<GlobalDownloadQueue>
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[GlobalDownloadQueue] ";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	[SerializeField]
	private int	_maxConcurrentDownloads = 1;

	private WebRequestQueue	_downloadQueue;

	//
	// PROPERTIES
	//

	public event System.Action<int>	_pOnDownloadStarted;

	public event System.Action<int>	_pOnDownloadEnded;

	public event System.Action	_pOnAllDownloadsEnded;

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	private void Update()
	{
		if ( _downloadQueue != null )
		{
			_downloadQueue.Update();
		}
	}

	// ** PUBLIC
	//

	public int Download( DownloadRequest request, object groupId = null )
	{
		int	downloadId = -1;

		if ( _downloadQueue != null )
		{
			downloadId = _downloadQueue.SendRequest( request, groupId );
		}

		return downloadId;
	}

	public int DownloadNext( DownloadRequest request, object groupId = null )
	{
		int	downloadId = -1;

		if ( _downloadQueue != null )
		{
			downloadId = _downloadQueue.SendRequestNext( request, groupId );
		}

		return downloadId;
	}

	public int DownloadNow( DownloadRequest request, object groupId = null )
	{
		int	downloadId = -1;

		if ( _downloadQueue != null )
		{
			downloadId = _downloadQueue.SendRequestNow( request, groupId );
		}

		return downloadId;
	}

	public void CancelDownload( int downloadId )
	{
		if ( _downloadQueue != null )
		{
			_downloadQueue.CancelRequest( downloadId );
		}
	}

	public void CancelDownloads( object groupId = null )
	{
		if ( _downloadQueue != null )
		{
			_downloadQueue.CancelRequests( groupId );
		}
	}

	public bool IsAllDownloadsEnded( object groupId = null )
	{
		return _downloadQueue == null || _downloadQueue.IsAllResponsesReceived( groupId );
	}

	public void PauseDownloads( bool wantPaused, object requester )
	{
		if ( _downloadQueue != null )
		{
			_downloadQueue.PauseRequests( wantPaused, requester );
		}
	}

	// ** PROTECTED
	//

	protected override void OnInitialise()
	{
		_downloadQueue = new WebRequestQueue( "GlobalDownloadQueue" );

		_downloadQueue._pOnRequestSent += downloadId => {
			if ( _pOnDownloadStarted != null )
			{
				_pOnDownloadStarted( downloadId );
			}
		};

		_downloadQueue._pOnResponseReceived += downloadId => {
			if ( _pOnDownloadEnded != null )
			{
				_pOnDownloadEnded( downloadId );
			}
		};

		_downloadQueue._pOnAllResponsesReceived += () => {
			if ( _pOnAllDownloadsEnded != null )
			{
				_pOnAllDownloadsEnded();
			}
		};

		_downloadQueue.SetMaxConcurrentDownloads( _maxConcurrentDownloads );
	}

	protected override bool OnResolveSingletonConflict( GlobalDownloadQueue newInstance )
	{
		_maxConcurrentDownloads = newInstance._maxConcurrentDownloads;

		Destroy( newInstance.gameObject );

		return true;
	}

	// ** PRIVATE
	//

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

