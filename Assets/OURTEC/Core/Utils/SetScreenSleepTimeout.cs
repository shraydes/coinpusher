﻿////////////////////////////////////////////
// 
// SetScreenSleepTimeout.cs
//
// Created: 28/09/2017 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class SetScreenSleepTimeout : MonoBehaviour
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[SetScreenSleepTimeout] ";

	public enum ESleepTimeout
	{
		NEVER_SLEEP = SleepTimeout.NeverSleep,
		SYSTEM_SETTING = SleepTimeout.SystemSetting
	}

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	[SerializeField]
	private ESleepTimeout	_sleepTimeout = ESleepTimeout.SYSTEM_SETTING;

	//
	// PROPERTIES
	//

	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void Awake()
	{
		Screen.sleepTimeout = (int)_sleepTimeout;
	}

	// ** PUBLIC
	//

	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	//
	// INTERFACE IMPLEMENTATIONS
	//
}

