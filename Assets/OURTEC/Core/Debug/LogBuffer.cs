﻿////////////////////////////////////////////
// 
// LogBuffer.cs
//
// Created: 17/05/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

public class LogBuffer
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[LogBuffer] ";

	private const char		LOG_TYPE_NULL = 'N';

	private const char		LOG_TYPE_MESSAGE = 'M';

	private const char		LOG_TYPE_WARNING = 'W';

	private const char		LOG_TYPE_ERROR = 'E';

	private const string	TIME_STAMP_BEGIN = "@";

	private const string	TIME_STAMP_END = ": ";

	public enum EBufferSize
	{
		ZERO = 0,
		TEST = 5,
		_1K = 10,
		_2K,
		_4K,
		_8K,
		_16K,
		_32K,
		_64K,
		_128K,
		_256K,
		_512K,
		_1M,
		_2M,
		_4M,
		_8M,
		_16M,
		_32M,
		_64M,
		_128M,
		_256M,
		_512M
	}

	//
	// SINGLETON DECLARATION
	//

	public static int GetBufferSizeInChars( EBufferSize bufferSize )
	{
		return 1 << ( int )bufferSize;
	}

	//
	// NESTED CLASSES / STRUCTS
	//

	//
	// MEMBERS 
	//

	private char[]	_buffer;

	private int		_idxModMask;

	private bool	_isShowTimeStamp;

	private int		_firstEntryPos;

	private int		_nextEntryPos;

	private int		_dropCount;

	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//

	public LogBuffer( EBufferSize bufferSize, bool isShowTimeStamp = false )
	{
		int	bufferSizeInChars = GetBufferSizeInChars( bufferSize );

		_buffer = new char[ bufferSizeInChars ];

		_idxModMask = bufferSizeInChars - 1;

		_isShowTimeStamp = isShowTimeStamp;

		Clear();
	}

	public void Clear()
	{
		_firstEntryPos = _nextEntryPos = 0;

		WriteChar( _nextEntryPos, LOG_TYPE_NULL );

		_dropCount = 0;
	}

	public void Log( string message, Object context = null )
	{
		LogInternal( LOG_TYPE_MESSAGE, message );
	}
	
	public void LogWarning( string message, Object context = null )
	{
		LogInternal( LOG_TYPE_WARNING, message );
	}
	
	public void LogError( string message, Object context = null )
	{
		LogInternal( LOG_TYPE_ERROR, message );
	}

	public void DumpToUnityLog( string logPrefix = null, bool isClearLog = true )
	{
		if ( logPrefix == null )
		{
			logPrefix = "";
		}

		ForEachEntry( m => {
			Debug.Log( logPrefix + m );
		}, w => {
			Debug.LogWarning( logPrefix + w );
		}, e => {
			Debug.LogError( logPrefix + e );
		} );

		if ( isClearLog )
		{
			Clear();
		}
	}

	public void ForEachEntry( System.Action<string> onMessage, System.Action<string> onWarning, System.Action<string> onError )
	{
		int pos = _firstEntryPos;
		char	logType;
		string	message;
		bool	isEnd = false;

		while ( !isEnd )
		{
			pos = ReadEntry( pos, out logType, out message );

			switch ( logType )
			{
			case LOG_TYPE_MESSAGE:
				if ( onMessage != null )
				{
					onMessage( message );
				}
				break;
			case LOG_TYPE_WARNING:
				if ( onWarning != null )
				{
					onWarning( message );
				}
				break;
			case LOG_TYPE_ERROR:
				if ( onError != null )
				{
					onError( message );
				}
				break;
			default:
				isEnd = true;
				break;
			}
		}
	}
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//

	private void FixPointers()
	{
		_firstEntryPos = _firstEntryPos & _idxModMask;
		_nextEntryPos = _nextEntryPos & _idxModMask;

		if ( _nextEntryPos < _firstEntryPos )
		{
			_nextEntryPos += _buffer.Length;
		}
	}
	
	private void LogInternal( char logType, string message )
	{
		string	timeStamp = _isShowTimeStamp ? Time.realtimeSinceStartup.ToString() : null;
		int		totalMessageLength = GetTotalLength( message, timeStamp );
		int		totalRequiredLength = totalMessageLength + 4;

		if ( totalRequiredLength > _buffer.Length )
		{
			Debug.LogError( LOG_TAG + "message too long" );
			return;
		}

		EnsureSpace( _nextEntryPos, totalRequiredLength );

		_nextEntryPos = WriteEntry( _nextEntryPos, logType, message, timeStamp, totalMessageLength );

		WriteChar( _nextEntryPos, LOG_TYPE_NULL );

		FixPointers();
	}

	private int GetTotalLength( string message, string timeStamp )
	{
		int	totalLength = message.Length;

		if ( timeStamp != null )
		{
			totalLength += TIME_STAMP_BEGIN.Length + timeStamp.Length + TIME_STAMP_END.Length;
		}

		return totalLength;
	}

	private void EnsureSpace( int pos, int len )
	{
		int	posEnd = pos + len;
		int	lenTotal = posEnd - _firstEntryPos;
		int	lenBuf = _buffer.Length;

		while ( lenTotal > lenBuf )
		{
			++_dropCount;
			_firstEntryPos = ReadEntry( _firstEntryPos );
			lenTotal = posEnd - _firstEntryPos;
		}
	}

	private int WriteEntry( int pos, char logType, string message, string timeStamp, int totalMessageLength )
	{
		pos = WriteChar( pos, logType );

		if ( totalMessageLength < 0 )
		{
			totalMessageLength = GetTotalLength( message, timeStamp );
		}

		pos = WriteInt( pos, totalMessageLength );

		if ( timeStamp != null )
		{
			pos = WriteString( pos, TIME_STAMP_BEGIN );
			pos = WriteString( pos, timeStamp );
			pos = WriteString( pos, TIME_STAMP_END );
		}

		pos = WriteString( pos, message );

		return pos;
	}

	private int WriteChar( int pos, char value )
	{
		_buffer[ pos & _idxModMask ] = value;

		return pos + 1;
	}

	private int WriteInt( int pos, int value )
	{
		_buffer[ pos & _idxModMask ] = (char)(value >> 16);
		_buffer[ (pos + 1) & _idxModMask ] = (char)(value & 0xffff);

		return pos + 2;
	}

	private int WriteString( int pos, string value )
	{
		int	lenStr = value.Length;
		int	lenBuf = _buffer.Length;
		int	lenToEnd = lenBuf - (pos & _idxModMask);

		if ( lenStr > lenToEnd )
		{
			value.CopyTo( 0, _buffer, (pos & _idxModMask), lenToEnd );
			value.CopyTo( lenToEnd, _buffer, 0, lenStr - lenToEnd );
		}
		else
		{
			value.CopyTo( 0, _buffer, (pos & _idxModMask), lenStr );
		}

		return pos + lenStr;
	}

	private int ReadEntry( int pos, out char logType, out string fullMessage )
	{
		pos = ReadChar( pos, out logType );

		if ( logType == LOG_TYPE_MESSAGE || logType == LOG_TYPE_WARNING || logType == LOG_TYPE_ERROR )
		{
			int	totalMessageLength;
			pos = ReadInt( pos, out totalMessageLength );
			pos = ReadString( pos, totalMessageLength, out fullMessage );
		}
		else
		{
			fullMessage = null;
		}

		return pos;
	}

	private int ReadEntry( int pos )
	{
		char	logType;
		pos = ReadChar( pos, out logType );

		if ( logType == LOG_TYPE_MESSAGE || logType == LOG_TYPE_WARNING || logType == LOG_TYPE_ERROR )
		{
			int	totalMessageLength = 0;
			pos = ReadInt( pos, out totalMessageLength );
			pos += totalMessageLength;
		}

		return pos;
	}

	private int ReadChar( int pos, out char value )
	{
		value = _buffer[ pos & _idxModMask ];

		return pos + 1;
	}

	private int ReadInt( int pos, out int value )
	{
		value = (((int)_buffer[ pos & _idxModMask ]) << 16) | (( int )(_buffer[ (pos + 1) & _idxModMask ] ));
		return pos + 2;
	}

	private int ReadString( int pos, int length, out string value )
	{
		int	lenStr = length;
		int	lenBuf = _buffer.Length;
		int	lenToEnd = lenBuf - (pos & _idxModMask);

		if ( lenStr > lenToEnd )
		{
			value = new string( _buffer, pos & _idxModMask, lenToEnd );
			value += new string( _buffer, 0, lenStr - lenToEnd );
		}
		else
		{
			value = new string( _buffer, pos & _idxModMask, lenStr );
		}

		return pos + lenStr;
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

