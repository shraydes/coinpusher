﻿////////////////////////////////////////////
// 
// SimulateButtonPress.cs
//
// Created: 28/06/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

using ButtonType = UnityEngine.UI.Button;
using TouchType = UnityEngine.Touch;

public static class SimulateButtonPress
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[SimulateButtonPress] ";

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	//
	// PROPERTIES
	//
	
	//
	// FUNCTIONS 
	//

	// ** PUBLIC
	//
	
	public static void SimulateButtonPressSuccess( this ButtonType button )
	{
		button.onClick.Invoke();
	}

	//Fake a screen touch over the button.
	//This handles the case where, for example, the button is occluded by full-screen collider.
	public static void SimulateButtonPressAttempt( this ButtonType button )
	{
		if ( OverridableInput._pInstance == null )
		{
			Debug.LogError( LOG_TAG + "Cannot simulate button press without OverridableInput" );
			return;
		}

		RectTransform	rectTrans = button.gameObject.GetComponent<RectTransform>();

		Rect	screenRect = rectTrans.GetScreenSpaceRect( isZeroBottom:true );
		Vector2	buttonPos = screenRect.center;

		TouchType	buttonTouch = new TouchType() { position = buttonPos, fingerId = 1, phase = TouchPhase.Began, tapCount = 0 };

		int	overrideCallCount = 0;

		OverridableInput._pInstance._pGetTouchCountOverride = () => 1;

		OverridableInput._pInstance._pGetTouchOverride = touchIdx => {
			++overrideCallCount;
			if ( overrideCallCount == 2 )
			{
				OverridableInput._pInstance._pGetTouchCountOverride = null;//end override
				OverridableInput._pInstance._pGetTouchOverride = null;//end override
				buttonTouch.phase = TouchPhase.Ended;
			}
			return buttonTouch;
		};
	}

	// ** PROTECTED
	//

	// ** PRIVATE
	//
}

