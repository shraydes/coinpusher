﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;

public class Windows10IconResizer : EditorWindow
{
	private static string[] FILE_NAMES = new string[]
	{
		"Square44x44Logo.scale-100.png",
		"Square44x44Logo.scale-200.png",
		"Square44x44Logo.scale-400.png",

		"Square44x44Logo.targetsize-16.png",
		"Square44x44Logo.targetsize-16_altform-unplated.png",
		"Square44x44Logo.targetsize-24.png",
		"Square44x44Logo.targetsize-24_altform-unplated.png",
		"Square44x44Logo.targetsize-48.png",
		"Square44x44Logo.targetsize-48_altform-unplated.png",
		"Square44x44Logo.targetsize-256.png",
		"Square44x44Logo.targetsize-256_altform-unplated.png",

		"Square71x71Logo.scale-100.png",
		"Square71x71Logo.scale-200.png",
		"Square71x71Logo.scale-400.png",

		"Square150x150Logo.scale-100.png",
		"Square150x150Logo.scale-200.png",
		"Square150x150Logo.scale-400.png",

		"StoreLogo.scale-100.png",
		"StoreLogo.scale-200.png",
		"StoreLogo.scale-400.png"
	};

	private struct IconSize
	{
		public int	_width;
		public int	_height;
		public IconSize( int width, int height )
		{
			_width = width;
			_height = height;
		}
	}

	private static IconSize[] ICON_SIZES = new IconSize[]
	{
		new IconSize( 44, 44 ),
		new IconSize( 88, 88 ),
		new IconSize( 176, 176 ),

		new IconSize( 16, 16 ),
		new IconSize( 16, 16 ),
		new IconSize( 24, 24 ),
		new IconSize( 24, 24 ),
		new IconSize( 48, 48 ),
		new IconSize( 48, 48 ),
		new IconSize( 256, 256 ),
		new IconSize( 256, 256 ),

		new IconSize( 71, 71 ),
		new IconSize( 142, 142 ),
		new IconSize( 284, 284 ),

		new IconSize( 150, 150 ),
		new IconSize( 300, 300 ),
		new IconSize( 600, 600 ),

		new IconSize( 50, 50 ),
		new IconSize( 100, 100 ),
		new IconSize( 200, 200 )
	};

	private Texture2D _sourceFile;
	private Texture2D[] _sourceVariants = new Texture2D[4];


	[MenuItem ("Amuzo Tools/Windows 10/Icon Resizer")]
	public static void  ShowWindow ()
	{
		Windows10IconResizer window = (Windows10IconResizer)EditorWindow.GetWindow(typeof(Windows10IconResizer));
		window.maxSize = new Vector2( 200, 106 );
		window.minSize = new Vector2( 200, 106 );
	}

	void OnGUI ()
	{
		if( GUI.Button( new Rect( 8, 8, 182, 90 ), "Make Icons" ) )
		{
			string path = EditorUtility.OpenFilePanel( "Select Source File", "", "png");

			if( path != null && path.Length != 0 )
			{
				MakeWindows10Icons( path );
			}
		}
	}

	private void MakeWindows10Icons( string sourceFile )
	{
		int assetIndex = sourceFile.LastIndexOf( "/Assets/" );
		string relativeSourceFile = sourceFile;
		if( assetIndex != -1 )
		{
			relativeSourceFile = sourceFile.Substring( assetIndex + 1, sourceFile.Length - ( assetIndex + 1 ) );
		}

		string targetFolder = sourceFile.Substring( 0, sourceFile.LastIndexOf('/') + 1 );
		//CC20170526 - commented out to removed "value never used" warning
		//string relativeTargetFolder = relativeSourceFile.Substring( 0, relativeSourceFile.LastIndexOf('/') + 1 );
		 
		Debug.Log("Output folder [" + targetFolder + "]");

		TextureImporter importer = AssetImporter.GetAtPath( relativeSourceFile ) as TextureImporter;
		if( importer != null )
		{
			importer.textureType = TextureImporterType.Sprite;
			importer.isReadable = true;
			importer.mipmapEnabled = false;
			importer.SaveAndReimport();
		}

		Texture2D sourceIcon = (Texture2D)AssetDatabase.LoadAssetAtPath( relativeSourceFile, typeof(Texture2D) );

		if( sourceIcon == null )
		{
			Debug.LogError("Unable to load Texture2D at file path [" + relativeSourceFile + "]. Unable make icons.");
			return;
		}

		Debug.Log( "Source icon size [" + sourceIcon.width + "/" + sourceIcon.height + "]" );

		if( sourceIcon.width < 600 || sourceIcon.height < 600 )
		{
			Debug.LogError("Source icon must bee at least 600x600. Unable to make icons.");
			return;
		}

		_sourceVariants[0] = ScaleTexture( sourceIcon, 512, 512 );
		_sourceVariants[1] = ScaleTexture( _sourceVariants[0], 256, 256 );
		_sourceVariants[2] = ScaleTexture( _sourceVariants[1], 128, 128 );
		_sourceVariants[3] = ScaleTexture( _sourceVariants[2], 64, 64 );

		if( FILE_NAMES.Length != ICON_SIZES.Length )
		{
			Debug.LogError("Icons file names and icon sizes arrays do not match! Unable to make icons.");
			return;
		}

		string newFileName;
		Texture2D newIcon;
		for( int x = 0; x < FILE_NAMES.Length; x++ )
		{
			newFileName = targetFolder + "/" + FILE_NAMES[x];
			if( ICON_SIZES[x]._width < 64 )
			{
				newIcon = ScaleTexture( _sourceVariants[3], ICON_SIZES[x]._width, ICON_SIZES[x]._height );
			}
			else if( ICON_SIZES[x]._width < 128 )
			{
				newIcon = ScaleTexture( _sourceVariants[2], ICON_SIZES[x]._width, ICON_SIZES[x]._height );
			}
			else if( ICON_SIZES[x]._width < 256 )
			{
				newIcon = ScaleTexture( _sourceVariants[1], ICON_SIZES[x]._width, ICON_SIZES[x]._height );
			}
			else if( ICON_SIZES[x]._width < 512 )
			{
				newIcon = ScaleTexture( _sourceVariants[0], ICON_SIZES[x]._width, ICON_SIZES[x]._height );
			}
			else
			{
				newIcon = ScaleTexture( sourceIcon, ICON_SIZES[x]._width, ICON_SIZES[x]._height );
			}
			
			if( File.Exists( newFileName ) )
			{
				File.Delete( newFileName );
			}
			File.WriteAllBytes( newFileName, newIcon.EncodeToPNG() );
		}
	}

	private Texture2D ScaleTexture( Texture2D source, int targetWidth, int targetHeight )
	{
		Texture2D result = new Texture2D( targetWidth, targetHeight, source.format, source.mipmapCount > 1 );
		Color[] rpixels = result.GetPixels( 0 );
		float incX = ( (float)1 / source.width ) * ( (float)source.width / targetWidth );
		float incY = ( (float)1 / source.height ) * ( (float)source.height / targetHeight );
		for( int px = 0; px < rpixels.Length; px++ )
		{
				rpixels[px] = source.GetPixelBilinear( incX * ( (float)px % targetWidth ),
									incY * ( (float)Mathf.Floor( px / targetWidth ) ) );
		}
		result.SetPixels( rpixels, 0 );
		result.Apply();
		return result;
 }
}
