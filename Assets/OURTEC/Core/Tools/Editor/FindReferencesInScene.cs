﻿using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;



public class FindReferencesInScene : EditorWindow
{

	private Object _object;
	private bool _isObjectPrefab = false;
	//CC20170526 - commented out to remove "value never used" warning
	//private string _objectTypeString;
	private readonly List<Reference> _references = new List<Reference>();
	private static bool _forceUpdateReferences = true;
	private Vector2 _scrollPos;
	


	[MenuItem(AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Find References In Scene")]
	public static void ShowWindow()
	{
		FindReferencesInScene window = GetWindow<FindReferencesInScene>();
		window.Show();
	}



	private FindReferencesInScene()
	{
		titleContent = new GUIContent("Find References In Scene");
		_forceUpdateReferences = true;
	}



	private void OnGUI()
	{
		if (_forceUpdateReferences) {
			UpdateReferences();
			_forceUpdateReferences = false;
		}

		GUILayout.Label("Object:" + (_isObjectPrefab  ? " (prefab)" : ""), EditorStyles.boldLabel);

		EditorGUILayout.BeginHorizontal();
		{
			Object newObject = EditorGUILayout.ObjectField(_object, typeof(Object), true);

			if (newObject != _object &&
				ValidateObject(newObject)
			) {
				SetObject(newObject);
			}

			EditorGUI.BeginDisabledGroup(_object == null);
			{
				if (GUILayout.Button("Update", EditorStyles.miniButton, GUILayout.Width(50)))
				{
					UpdateReferences();
				}
			}
			EditorGUI.EndDisabledGroup();
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();

		Rect r = EditorGUILayout.GetControlRect(false);

		r.width /= 3;
		GUI.Box(r, "");

		GUI.Label(r, new GUIContent("GameObject", "The GameObject in the scene hierarchy"), EditorStyles.boldLabel);
			
		r.x += r.width;
		GUI.Box(r, "");
		GUI.Label(r, new GUIContent("Component", "The Component on the GameObject"), EditorStyles.boldLabel);
			
		r.x += r.width;
		GUI.Box(r, "");
		GUI.Label(r, new GUIContent("Field", "The field in the Component"), EditorStyles.boldLabel);

		if (_object != null)
		{
			_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);

			if (_references.Count > 0)
			{
				foreach (Reference reference in _references)
				{
					r = EditorGUILayout.GetControlRect(false);

					r.width /= 3;

					GUI.Box(r, "");
					EditorGUI.ObjectField(r, reference._gameObject, typeof(GameObject), true);

					r.x += r.width;
					GUI.Box(r, "");
					EditorGUI.LabelField(r, reference._formattedComponentName, EditorStyles.boldLabel);

					r.x += r.width;
					GUI.Box(r, "");
					EditorGUI.LabelField(
						r,
						reference._fieldInfo == null
							? new GUIContent("")
							: new GUIContent(
									(reference._isFieldInInspector ? reference._formattedFieldName : reference._fieldInfo.Name) +
									(reference._index != -1 ? ((reference._isFieldInInspector ? " " : "") + "[" + reference._index + "]") : "") +
									(reference._isFieldInInspector ? "" : " (Not in inspector)"),
								reference._isFieldInInspector ? "Member name: " + reference._fieldInfo.Name : ""
							)
					);
				}
			}
			else
			{
				EditorGUILayout.LabelField("No references in scene");
			}

			EditorGUILayout.EndScrollView();
		}
	}



	private bool ValidateObject(Object obj)
	{
		return true;
		//CC20170526 - commented out to remove "unreachable code" warning
		//if (obj == null) {
		//	return true;
		//}

		//// ensure object is not a component on a non-instance prefab
		//if (PrefabUtility.GetPrefabType(obj) == PrefabType.Prefab &&
		//	!(obj is GameObject)
		//) {
		//	EditorUtility.DisplayDialog("Invalid Object", "Components on non-instance prefabs are not allowed", "OK");
		//	return false;
		//}

		//return true;
	}



	private void SetObject(Object obj)
	{
		_object = obj;

		_isObjectPrefab = _object != null && 
		#if UNITY_2018_3_OR_NEWER
			PrefabUtility.GetPrefabAssetType(_object) != PrefabAssetType.NotAPrefab;
		#else
			PrefabUtility.GetPrefabType(_object) == PrefabType.Prefab;
		#endif

		//CC20170526 - commented out to remove "value never used" warning
		//_objectTypeString = _object == null
		//	? ""
		//	: _object.GetType().ToString().Substring(_object.GetType().ToString().LastIndexOf(".") + 1);

		UpdateReferences();
	}



	private void UpdateReferences()
	{
		_references.Clear();

		if (_object == null) {
			return;
		}

		if (_isObjectPrefab)
		{
			Component component = _object as Component;
			GameObject gameObject = component ? component.gameObject : _object as GameObject;
			Object	prefabParent;

			foreach (GameObject go in GameObject.FindObjectsOfType<GameObject>())
			{
				#if UNITY_2018_3_OR_NEWER
				prefabParent = PrefabUtility.GetCorrespondingObjectFromSource(go);
				#else
				prefabParent = PrefabUtility.GetPrefabParent(go);
				#endif
				if (prefabParent == gameObject)
				{
					_references.Add(new Reference(go));
				}
			}
			return;
		}

		foreach (Component c in GameObject.FindObjectsOfType<Component>())
		{
			foreach (FieldInfo fi in c.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
			{
				if (fi.GetValue(c) is IList)
				{
					IList a = (IList)fi.GetValue(c);

					for (int i = 0; i < a.Count; ++i)
					{
						try
						{
							if (a[i] == (object)_object)
							{
								_references.Add(new Reference(c.gameObject, c, fi, i));
							}
						}
						catch(System.Exception) { }
					}
				}
				else if (fi.GetValue(c) == (object)_object)
				{
					_references.Add(new Reference(c.gameObject, c, fi));
				}
			}
		}
	}



	private static string FormatAsInspectorFieldName(string fieldName)
	{
		fieldName = fieldName.TrimStart('_');

		List<string> words = new List<string>();
		int currWordIndex = 0;

		string word;

		for (int i = 1; i < fieldName.Length; ++i)
		{
			char p = i - 1 >= 0 ? fieldName[i - 1] : ' ';
			char c = fieldName[i];
			char n = i + 1 < fieldName.Length ? fieldName[i + 1] : ' ';

			if (p == '_' || c == '_' || n == '_') continue;
			if (char.IsDigit(c) && (char.IsDigit(p) || (char.IsLetter(p) && char.IsUpper(p)))) continue;
			

			if ((i + 1 < fieldName.Length &&	char.IsUpper(c) && char.IsLower(n) && !char.IsDigit(n)) ||
				(i - 1 >= 0 &&					char.IsUpper(c) && char.IsLower(p) && !(char.IsDigit(c) && i == 1))
			) {
				word = fieldName.Substring(currWordIndex, i - currWordIndex);
				word = word[0].ToString().ToUpper() + word.Substring(1);
				words.Add(word);
				currWordIndex = i;
			}
		}

		if (currWordIndex < fieldName.Length) {
			word = fieldName.Substring(currWordIndex);
			word = word[0].ToString().ToUpper() + word.Substring(1);
			words.Add(word);
		}

		return string.Join(" ", words.ToArray());
	}



	private class Reference
	{
		public readonly GameObject _gameObject;
		public readonly Component _component;
		public readonly FieldInfo _fieldInfo;
		public readonly int _index;
		public readonly bool _isFieldInInspector;
		public readonly string _formattedComponentName;
		public readonly string _formattedFieldName;
		
		public Reference(GameObject gameObject, Component component = null, FieldInfo fieldInfo = null, int index = -1)
		{
			_gameObject = gameObject;
			_component = component;
			_fieldInfo = fieldInfo;
			_index = index;

			if (_component != null) _formattedComponentName = FormatAsInspectorFieldName(_component.GetType().ToString());
			if (_fieldInfo != null) _formattedFieldName = FormatAsInspectorFieldName(_fieldInfo.Name);
			
			_isFieldInInspector =
				_fieldInfo != null &&
				(System.Attribute.GetCustomAttribute(_fieldInfo, typeof(HideInInspector)) == null) &&
				(System.Attribute.GetCustomAttribute(_fieldInfo, typeof(System.NonSerializedAttribute)) == null) &&
				((System.Attribute.GetCustomAttribute(_fieldInfo, typeof(SerializeField)) != null) || _fieldInfo.IsPublic);
				
		}
	}
	
}
