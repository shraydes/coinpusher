﻿////////////////////////////////////////////
// 
// PackagerTool.cs
//
// Created: 03/12/2015 ccuthbert
// Contributors:
// 
// Intention:
// Repackage Unity packages, adding our own 
// metadata.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using System.Text; 
using System.IO;
using UnityEngine;
using UnityEditor;
using Puma;
using LitJson;

public class PackagerTool : ScriptableWizard
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[PackagerTool] ";
	
	private const string	DEFAULT_PACKAGE_DEVELOPER = "Amuzo";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	public TextAsset	_packageInfo;
	
	public string	_packageDeveloper = DEFAULT_PACKAGE_DEVELOPER;
	
	public string	_packageName;
	
	public string	_packageVersion;

	public string	_repackageVersion;

	public string	_packageSymbols;

	[TextArea]
	public string	_changeSummary;
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	[MenuItem( ( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Puma/Packager" ) )]
	static void CreateWizard()
	{
		PackagerTool	wizard = DisplayWizard<PackagerTool>( "Packager", "Make Package", "Read Package Info" );

		if ( wizard != null )
		{
			wizard._packageInfo = Selection.activeObject as TextAsset;

			if ( wizard._packageInfo != null )
			{
				wizard.ReadPackageInfo( () => {
					wizard._packageInfo = null;
				} );
			}
		}
	}
	
	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void OnWizardCreate()
	{
		Puma.PackageInfo	packageInfo;

		bool	canGenerate = CanGenerate( out packageInfo );
		
		if ( !canGenerate )
		{
			Debug.LogError( LOG_TAG + "Can not generate package info" );
			return;
		}

		packageInfo.WritePackageInfoFile();
		packageInfo.ExportPackage();
	}

	void OnWizardOtherButton()
	{
		ReadPackageInfo( () => {
			Debug.LogError( LOG_TAG + "Invalid package info: " + _packageInfo.name );
		} );
	}
	
	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private bool CanGenerate( out Puma.PackageInfo packageInfo )
	{
		packageInfo = null;
		
		if ( string.IsNullOrEmpty( _packageDeveloper ) )
		{
			Debug.LogError( LOG_TAG + "Must specify package developer" );
			return false;
		}
		
		if ( string.IsNullOrEmpty( _packageName ) )
		{
			Debug.LogError( LOG_TAG + "Must specify package name" );
			return false;
		}
		
		if ( string.IsNullOrEmpty( _packageVersion ) )
		{
			Debug.LogError( LOG_TAG + "Must specify package version" );
			return false;
		}

		//if ( string.IsNullOrEmpty( _repackageVersion ) )
		//{
		//	Debug.LogError( LOG_TAG + "Must specify repackage version" );
		//	return false;
		//}

		packageInfo = new Puma.PackageInfo( _packageDeveloper, _packageName, _packageVersion, _repackageVersion, _packageSymbols );
		packageInfo.ReadChangeLogFromExistingInfoFile();
		packageInfo.AddChangeLogEntry( _packageVersion, _repackageVersion, _changeSummary );
		packageInfo.ReadContentsFromSelection();
		
		return packageInfo._pHasContents;
	}

	private void ReadPackageInfo( System.Action onInvalidPackageInfo )
	{
		if ( _packageInfo == null ) return;

		Puma.PackageInfo	packageInfo = new Puma.PackageInfo();

		packageInfo.ReadPackageInfoFile( _packageInfo );

		if ( !packageInfo._pIsValid )
		{
			if ( onInvalidPackageInfo != null )
			{
				onInvalidPackageInfo();
			}

			return;
		}

		packageInfo.SelectFiles();

		_packageDeveloper = packageInfo._pPackageDeveloper;
		_packageName = packageInfo._pPackageName;
		_packageVersion = packageInfo._pPackageVersion;
		_repackageVersion = packageInfo._pRepackageVersion;
		_packageSymbols = packageInfo._pPackageSymbolsString;
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

