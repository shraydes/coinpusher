﻿////////////////////////////////////////////
// 
// PackageItem.cs
//
// Created: 06/04/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
using System.IO;
using System.Text;
using System.Security.Cryptography;
using LoggerAPI = UnityEngine.Debug;

namespace Puma
{

	public class PackageItem : System.IComparable<PackageItem>
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[PackageItem] ";

		private const string	PACKAGE_INFO_FILE_HASH = "*";

		private const string	UNKNOWN_FILE_HASH = "?";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		private string	_filePath;

		private string	_fileHash;

		//
		// PROPERTIES
		//

		public string _pFilePath
		{
			get
			{
				return _filePath;
			}
		}
	
		public string _pFileHash
		{
			get
			{
				return _fileHash;
			}
		}

		public bool _pIsPackageInfoFile
		{
			get
			{
				return _fileHash == PACKAGE_INFO_FILE_HASH;
			}
		}

		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public PackageItem( string filePath, bool isPackageInfoFile )
		{
			_filePath = filePath;

			if ( isPackageInfoFile )
			{
				_fileHash = PACKAGE_INFO_FILE_HASH;
			}
			else
			{
				GenerateFileHash();
			}
		}
	
		public PackageItem( string filePath, string fileHash )
		{
			_filePath = filePath;
			_fileHash = fileHash;
		}
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private void GenerateFileHash()
		{
			if ( string.IsNullOrEmpty( _filePath ) )
			{
				_fileHash = null;
				return;
			}

			_fileHash = GenerateFileHash( _filePath );
		}

		private static string GenerateFileHash( string filePath )
		{
			byte[]	fileBytes = null;
			
			try
			{
				fileBytes = File.ReadAllBytes( filePath );
			}
			catch ( System.IO.IOException )
			{
				//LoggerAPI.LogError( LOG_TAG + "Failed to open '" + filePath + "'" );
				return UNKNOWN_FILE_HASH;
			}

			if ( fileBytes == null ) return null;

			//mimic git file hash algorithm
			byte[]	prefixBytes = Encoding.ASCII.GetBytes( "blob " + fileBytes.Length.ToString() + "\0" );
			byte[]	inputBytes = new byte[ prefixBytes.Length + fileBytes.Length ];

			prefixBytes.CopyTo( inputBytes, 0 );
			fileBytes.CopyTo( inputBytes, prefixBytes.Length );

			SHA1	sha = new SHA1CryptoServiceProvider();

			byte[]	hashBytes = sha.ComputeHash( inputBytes );

			return System.BitConverter.ToString( hashBytes ).Replace( "-", string.Empty ).ToUpper();
		}

		private static int CompareFilePaths( PackageItem a, PackageItem b )
		{
			if ( a == null && b == null ) return 0;
			if ( a == null ) return -1;
			if ( b == null ) return 1;

			if ( string.IsNullOrEmpty( a._pFilePath ) && string.IsNullOrEmpty( b._pFilePath ) ) return 0;
			if ( string.IsNullOrEmpty( a._pFilePath ) ) return -1;
			if ( string.IsNullOrEmpty( b._pFilePath ) ) return 1;

			return string.CompareOrdinal( a._pFilePath, b._pFilePath );
		}
	
		//
		// INTERFACE IMPLEMENTATIONS
		//

		int System.IComparable<PackageItem>.CompareTo( PackageItem other )
		{
			return CompareFilePaths( this, other );
		}
	}

}
