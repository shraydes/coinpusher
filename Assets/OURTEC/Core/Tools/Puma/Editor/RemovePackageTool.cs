﻿////////////////////////////////////////////
// 
// RemovePackageTool.cs
//
// Created: 03/12/2015 ccuthbert
// Contributors:
// 
// Intention:
// Remove package files listed in package 
// info.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using UnityEngine;
using UnityEditor;

public class RemovePackageTool : ScriptableWizard
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[RemovePackageTool] ";
	
	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//
	
	public TextAsset	_packageInfo;
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	[MenuItem( ( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Puma/Remove Package" ) )]
	static void CreateWizard()
	{
		RemovePackageTool	wizard = DisplayWizard<RemovePackageTool>( "Remove Package", "Remove Package", "Select Files" );

		if ( wizard != null )
		{
			wizard._packageInfo = Selection.activeObject as TextAsset;

			if ( wizard._packageInfo != null )
			{
				wizard.SelectFiles( () => {
					wizard._packageInfo = null;
				} );
			}
		}
	}
	
	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void OnWizardCreate()
	{
		if ( _packageInfo == null ) return;

		Puma.PackageManager.RefreshDatabase();
		Puma.PackageManager.RemovePackage( _packageInfo );
	}

	void OnWizardOtherButton()
	{
		SelectFiles( () => {
			Debug.LogError( LOG_TAG + "Invalid package info: " + _packageInfo.name );
		} );
	}
	
	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private void SelectFiles( System.Action onInvalidPackageInfo )
	{
		if ( _packageInfo == null ) return;

		Puma.PackageInfo	packageInfo = new Puma.PackageInfo();

		packageInfo.ReadPackageInfoFile( _packageInfo );

		if ( !packageInfo._pIsValid )
		{
			if ( onInvalidPackageInfo != null )
			{
				onInvalidPackageInfo();
			}
			
			return;
		}

		packageInfo.SelectFiles();
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

