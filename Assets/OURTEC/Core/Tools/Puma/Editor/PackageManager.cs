﻿////////////////////////////////////////////
// 
// PackageManager.cs
//
// Created: 15/01/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Logger = UnityEngine.Debug;

namespace Puma
{
	[InitializeOnLoad]
	public class PackageManager
	{
		//
		// CONSTS / DEFINES / ENUMS
		//

		private const string	LOG_TAG = "[PackageManager] ";

		private const string	PACKAGE_SYMBOL_PREFIX = "PUMA_DEFINE_";

		//
		// SINGLETON DECLARATION
		//

#pragma warning disable 0414
		private static PackageManager	_instance;
#pragma warning restore 0414

		//
		// NESTED CLASSES / STRUCTS
		//
			
		//
		// MEMBERS 
		//

		private static Dictionary<string, Puma.PackageInfo>	_packageInfos;

		private static Dictionary<string, int>	_assetRefCounts;
	
		//
		// PROPERTIES
		//
	
		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//
		
		static PackageManager()
		{
			Initialize();
		}

		private static void Initialize()
		{
			_instance = new PackageManager();
			
			RefreshDatabase();
			ApplyPackageSymbols();
		}

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public static void RefreshDatabase()
		{
			Logger.Log( LOG_TAG + "Refreshing package database" );
			Reset();
			ReadPackageInfoFiles();
			CountAssetRefs();
		}

		public static PackageInfo GetPackageInfo( string packageDeveloper, string packageName )
		{
			if ( string.IsNullOrEmpty( packageDeveloper ) ) return null;
			if ( string.IsNullOrEmpty( packageName ) ) return null;

			string	packageInfoAssetName = "PackageInfo_" + packageDeveloper + "_" + packageName;

			if ( _packageInfos == null ) return null;
			if ( !_packageInfos.ContainsKey( packageInfoAssetName ) ) return null;

			return _packageInfos[ packageInfoAssetName ];
		}

		public static void RemovePackage( TextAsset packageInfoFile )
		{
			if ( packageInfoFile == null ) return;

			string	packageInfoFileName = packageInfoFile.name;

			if ( !_packageInfos.ContainsKey( packageInfoFileName ) ) return;

			PackageInfo	pi = _packageInfos[ packageInfoFileName ];

			_packageInfos.Remove( packageInfoFileName );

			if ( pi == null ) return;

			int		notTrackedCount = 0;
			int		keepCount = 0;
			int		deletedCount = 0;
			int		deleteFailedCount = 0;
			bool	deleteResult;

			pi.ForEachFile( file => {
				if ( file == null ) return;
				string	path = file._pFilePath;
				if ( _assetRefCounts.ContainsKey( path ) )
				{
					int	refCount = _assetRefCounts[ path ];

					if ( refCount > 1 )
					{
						_assetRefCounts[ path ] = refCount - 1;

						++keepCount;
					}
					else
					{
						_assetRefCounts.Remove( path );

						deleteResult = AssetDatabase.DeleteAsset( path );

						if ( deleteResult )
						{
							++deletedCount;
						}
						else
						{
							Logger.LogError( LOG_TAG + "Failed to delete package file: " + path );
							++deleteFailedCount;
						}
					}
				}
				else
				{
					Logger.LogError( LOG_TAG + "Package file not in database: " + path );
					++notTrackedCount;
				}
			} );

			int	totalCount = notTrackedCount + keepCount + deletedCount + deleteFailedCount;

			Logger.Log( LOG_TAG + "Removed " + deletedCount + " of " + totalCount + " files" );

			if ( deletedCount > 0 )
			{
				AssetDatabase.Refresh();
			}
		}

		public static string[] GetPackageSymbols()
		{
			if ( _packageInfos == null ) return new string[] {};

			List<string>	symbols = new List<string>( _packageInfos.Count );

			string[]	packSymbols;

			foreach( PackageInfo pi in _packageInfos.Values )
			{
				if ( pi == null ) continue;
				if ( !pi._pHasPackageSymbols ) continue;

				packSymbols = pi._pPackageSymbols;

				if ( packSymbols == null ) continue;
				if ( packSymbols.Length == 0 ) continue;
				
				for ( int i = 0; i < packSymbols.Length; ++i )
				{
					if ( string.IsNullOrEmpty( packSymbols[i] ) ) continue;
					if ( symbols.Contains( packSymbols[i] ) ) continue;
					symbols.Add( PACKAGE_SYMBOL_PREFIX + packSymbols[i] );
				}
			}

			return symbols.ToArray();
		}

		public static void ApplyPackageSymbols()
		{
			Logger.Log( LOG_TAG + "Applying package symbols" );

			List<string>	packageSymbols = new List<string>( GetPackageSymbols() );

			if ( packageSymbols == null || packageSymbols.Count == 0 ) return;

			BuildTargetGroup targetGroup = BuildTargetUtils.GetBuildTargetGroup( EditorUserBuildSettings.activeBuildTarget );

			if ( ScriptingDefineSymbolsFacade.BeginChanges( targetGroup ) == false )
			{
				Logger.LogError( LOG_TAG + "Failed to apply package symbols" );
				return;
			}

			//remove unrequired package symbols
			ScriptingDefineSymbolsFacade.RemoveSymbols( s => s.StartsWith( PACKAGE_SYMBOL_PREFIX ) && !packageSymbols.Contains( s ), targetGroup );

			//add package symbols
			for ( int i = 0; i < packageSymbols.Count; ++i )
			{
				if ( string.IsNullOrEmpty( packageSymbols[i] ) ) continue;
				ScriptingDefineSymbolsFacade.AddSymbol( packageSymbols[i], targetGroup );
			}

			ScriptingDefineSymbolsFacade.EndChanges();
		}

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private PackageManager()
		{
		}

		private static void Reset()
		{
			_packageInfos = new Dictionary<string, PackageInfo>();
			_assetRefCounts = new Dictionary<string, int>();
		}

		private static void ReadPackageInfoFiles()
		{
			string		packageInfoDirectory = "Assets/PackageInfo";
			string[]	packageInfoFileGuids = AssetDatabase.FindAssets( "PackageInfo_ t:TextAsset", new string[] { packageInfoDirectory } );
			string		path;
			TextAsset	ta;
			PackageInfo	pi;

			for ( int i = 0; i < packageInfoFileGuids.Length; ++i )
			{
				if ( string.IsNullOrEmpty( packageInfoFileGuids[i] ) ) continue;

				path = AssetDatabase.GUIDToAssetPath( packageInfoFileGuids[i] );

				if ( string.IsNullOrEmpty( path ) ) continue;

#if UNITY_4_6 
				ta = (TextAsset)AssetDatabase.LoadAssetAtPath( path, typeof( TextAsset ) );
#else
				ta = AssetDatabase.LoadAssetAtPath<TextAsset>( path );
#endif
				if ( ta == null ) continue;

				pi = new PackageInfo();
				pi.ReadPackageInfoFile( ta );

				if ( !pi._pIsValid ) continue;

				_packageInfos.Add( ta.name, pi );
			}
		}

		private static void CountAssetRefs()
		{
			if ( _packageInfos == null ) return;
			if ( _assetRefCounts == null ) return;

			foreach ( PackageInfo pi in _packageInfos.Values )
			{
				if ( pi == null ) continue;

				pi.ForEachFile( file => {
					if ( file == null ) return;
					string	path = file._pFilePath;
					if ( _assetRefCounts.ContainsKey( path ) )
					{
						_assetRefCounts[ path ]++;
					}
					else
					{
						_assetRefCounts[ path ] = 1;
					}
				} );
			}
		}

		//
		// INTERFACE IMPLEMENTATIONS
		//
	}
}
