﻿////////////////////////////////////////////
// 
// PackageChangesTool.cs
//
// Created: 11/04/2016 ccuthbert
// Contributors:
// 
// Intention:
// Detect and display changes to package files.
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using System.Text; 
using System.IO;
using UnityEngine;
using UnityEditor;
using Puma;
using LitJson;

public class PackageChangesTool : ScriptableWizard
{
	//
	// CONSTS / DEFINES / ENUMS
	//
	
	private const string	LOG_TAG = "[PackageChangesTool] ";
	
	private const string	DEFAULT_PACKAGE_DEVELOPER = "Amuzo";

	//
	// SINGLETON DECLARATION
	//

	//
	// NESTED CLASSES / STRUCTS
	//
			
	//
	// MEMBERS 
	//

	public TextAsset	_packageInfo;

	private Puma.PackageInfo	_packageInfoFromFile;

	private Puma.PackageInfo	_packageInfoFromSelection;
	
	//
	// PROPERTIES
	//
	
	//
	// ABSTRACTS / VIRTUALS
	//

	//
	// STATIC FUNCTIONS
	//

	[MenuItem( ( AmuzoToolsCommon.TOOLS_MENU_PREFIX + "Puma/Package Changes" ) )]
	static void CreateWizard()
	{
		PackageChangesTool	wizard = DisplayWizard<PackageChangesTool>( "Package Changes", "Log Changes" );

		if ( wizard != null )
		{
			wizard._packageInfo = Selection.activeObject as TextAsset;

			if ( wizard._packageInfo != null )
			{
				int	changeCount = 0;
				wizard.DetectChanges( ( path, hashA, hashB ) => {
					++changeCount;
				} );
				Debug.Log( LOG_TAG + "There are " + changeCount + " changes" );
			}
		}
	}
	
	//
	// FUNCTIONS 
	//

	// ** UNITY
	//

	void OnWizardCreate()
	{
		int	changeCount = 0;
		DetectChanges( ( path, hashA, hashB ) => {
			if ( hashB != null )
			{
				Debug.Log( LOG_TAG + "Change: " + path + " (" + hashA + " --> " + hashB + ")" );
			}
			else
			{
				Debug.Log( LOG_TAG + "Delete: " + path + " (" + hashA + ")" );
			}
			++changeCount;
		} );
		Debug.Log( LOG_TAG + "There are " + changeCount + " changes" );
	}

	void OnWizardOtherButton()
	{
	}
	
	// ** PUBLIC
	//
	
	// ** PROTECTED
	//

	// ** PRIVATE
	//
	
	private void DetectChanges( System.Action<string, string, string> onChange )
	{
		if ( _packageInfo == null ) return;

		Puma.PackageInfo	piA = new Puma.PackageInfo();

		piA.ReadPackageInfoFile( _packageInfo );
		piA.DetectChanges( onChange );
	}

	//
	// INTERFACE IMPLEMENTATIONS
	//
}

