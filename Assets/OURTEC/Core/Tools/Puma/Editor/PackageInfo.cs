﻿////////////////////////////////////////////
// 
// PackageInfo.cs
//
// Created: 14/01/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// V2 - adds support for multiple package symbols
// V3 - adds support for file hashes
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;
using LitJson;
using Logger = UnityEngine.Debug;

namespace Puma
{

	public class PackageInfo
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[PackageInfo] ";

		private const int		INFO_VERSION = 3;
	
		private const string	KEY_INFO_VERSION = "infoVersion";
	
		private const string	KEY_PACKAGE_DEVELOPER = "packageDeveloper";
	
		private const string	KEY_PACKAGE_NAME = "packageName";
	
		private const string	KEY_PACKAGE_VERSION = "packageVersion";

		private const string	KEY_REPACKAGE_VERSION = "repackageVersion";
	
		private const string	KEY_PACKAGE_SYMBOLS = "packageSymbols";
	
		private const string	KEY_PACKAGE_CONTENTS = "packageContents";

		private const string	KEY_PACKAGE_ITEM_PATH  = "path";

		private const string	KEY_PACKAGE_ITEM_HASH  = "hash";

		private const string	KEY_CHANGE_LOG = "changeLog";

		private const string	KEY_CHANGE_LOG_ENTRY_SUMMARY = "summary";

		private const string	DEPRECATED_KEY_PACKAGE_SYMBOL = "packageSymbol";
	
		//
		// NESTED CLASSES / STRUCTS
		//

		private class ChangeLogEntry
		{
			public string	_pPackageVersion	{ get; set; }

			public string	_pRepackageVersion	{ get; set; }

			public string	_pChangeSummary		{ get; set; }
		}
			
		//
		// MEMBERS 
		//

		private string	_packageDeveloper;

		private string	_packageName;

		private string	_packageVersion;

		private string	_repackageVersion;

		private string[]	_packageSymbols;

		private List<PackageItem>	_packageContents;

		private List<ChangeLogEntry>	_changeLog;

		private string	_packageInfoDirectory;
	
		private string	_packageInfoPath;

		//
		// PROPERTIES
		//

		public string _pPackageDeveloper
		{
			get
			{
				return _packageDeveloper;
			}
		}

		public string _pPackageName
		{
			get
			{
				return _packageName;
			}
		}

		public string _pPackageVersion
		{
			get
			{
				return _packageVersion;
			}
		}

		public string _pRepackageVersion
		{
			get
			{
				return _repackageVersion;
			}
		}

		private bool _pIsRepackage
		{
			get
			{
				return !string.IsNullOrEmpty( _repackageVersion );
			}
		}

		public string[] _pPackageSymbols
		{
			get
			{
				return _packageSymbols;
			}
		}

		public string _pPackageSymbolsString
		{
			get
			{
				return _packageSymbols != null ? string.Join( ";", _packageSymbols ) : string.Empty;
			}
		}

		public bool _pHasPackageSymbols
		{
			get
			{
				return _packageSymbols.Length > 0;
			}
		}

		public bool	_pHasContents
		{
			get
			{
				return _packageContents != null && _packageContents.Count > 0;
			}
		}

		public bool _pIsValid
		{
			get
			{
				return !string.IsNullOrEmpty( _packageDeveloper ) && !string.IsNullOrEmpty( _packageName ) && !string.IsNullOrEmpty( _packageVersion );
			}
		}

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public PackageInfo()
		{
			Reset();
		}

		public PackageInfo( string packageDeveloper, string packageName, string packageVersion, string repackageVersion, string packageSymbols )
		{
			_packageDeveloper = packageDeveloper;
			_packageName = packageName;
			_packageVersion = packageVersion;
			_repackageVersion = repackageVersion;
			SetPackageSymbols( packageSymbols );

			ResetContents();
			ResetChangeLog();
			RefreshPackageInfoPath();
		}

		public void ReadContentsFromSelection()
		{
			ResetContents();

			Object[] selectedAssets = Selection.GetFiltered( typeof( Object ), SelectionMode.DeepAssets );
		
			if ( selectedAssets == null || selectedAssets.Length == 0 ) return;
		
			_packageContents = new List<PackageItem>( selectedAssets.Length );
		
			string	assetPath;
		
			for ( int i = 0; i < selectedAssets.Length; ++i )
			{
				if ( selectedAssets[i] == null ) continue;
			
				assetPath = AssetDatabase.GetAssetPath( selectedAssets[i].GetInstanceID() );
			
				if ( string.IsNullOrEmpty( assetPath ) ) continue;
				if ( Directory.Exists( assetPath ) ) continue;
			
				_packageContents.Add( new PackageItem( assetPath, IsPackageInfoPath( assetPath ) ) );
			}
		
			_packageContents.Sort();
		}

		public void ReadPackageInfoFile( TextAsset packageInfoFile )
		{
			if ( packageInfoFile == null ) return;

			ReadPackageInfoStr( packageInfoFile.text );
		}

		public void ReadChangeLogFromExistingInfoFile()
		{
			try
			{
				RefreshPackageInfoPath();

				string	oldPackageInfoStr = File.ReadAllText( _packageInfoPath );

				ReadPackageInfoStr( oldPackageInfoStr, ReadChangeLogFromJson );
			}
			catch ( System.Exception )
			{
			}
		}

		public void AddChangeLogEntry( string packageVersion, string repackageVersion, string changeSummary )
		{
			ChangeLogEntry	entry = new ChangeLogEntry();
			entry._pPackageVersion = packageVersion;
			entry._pRepackageVersion = repackageVersion;
			entry._pChangeSummary = changeSummary;

			_changeLog.Add( entry );
		}

		public void WritePackageInfoFile()
		{
			RefreshPackageInfoPath();

			if ( _packageContents.Find( i => i._pFilePath == _packageInfoPath ) == null )
			{
				_packageContents.Add( new PackageItem( _packageInfoPath, true ) );
				_packageContents.Sort();
			}

			string	packageInfoStr = ToJson();
		
			Directory.CreateDirectory( _packageInfoDirectory );
			File.WriteAllText( _packageInfoPath, packageInfoStr );
		
			AssetDatabase.ImportAsset( _packageInfoPath, ImportAssetOptions.ForceUpdate );
		}

		public void ExportPackage()
		{
			string	packageDeveloper = ToPascalCase( _packageDeveloper );
			string	packageName = ToPascalCase( _packageName );
			string	packageFileName;
			
			if ( _pIsRepackage )
			{
				packageFileName = packageDeveloper + "_" + packageName + "_" + _packageVersion + "_Repackaged_" + _repackageVersion;
			}
			else
			{
				packageFileName = packageDeveloper + "_" + packageName + "_" + _packageVersion;
			}
		
			string[]	assetPaths = GetPackageItemPaths();

			AssetDatabase.ExportPackage( assetPaths, packageFileName + ".unitypackage", ExportPackageOptions.Interactive );

			// Export package info file alongside package
			RefreshPackageInfoPath();

			AssetDatabase.CopyAsset( _packageInfoPath, packageFileName + ".txt" );
		}

		public void SelectFiles()
		{
			if ( _packageContents == null ) return;

			List<Object>	assetObjs = new List<Object>();

			Object	assetObj;
			int	contentsCount = _packageContents.Count;

			for ( int i = 0; i < contentsCount; ++i )
			{
				if ( _packageContents[i] == null ) continue;
				if ( string.IsNullOrEmpty( _packageContents[i]._pFilePath ) ) continue;

				assetObj = AssetDatabase.LoadMainAssetAtPath( _packageContents[i]._pFilePath );

				if ( assetObj == null ) continue;

				assetObjs.Add( assetObj );
			}

			Selection.objects = assetObjs.ToArray();
		}

		public void ForEachFile( System.Action<PackageItem> fileAction )
		{
			if ( fileAction == null ) return;
			if ( _packageContents == null ) return;

			int	contentsCount = _packageContents.Count;

			for ( int i = 0; i < contentsCount; ++i )
			{
				if ( _packageContents[i] == null ) continue;

				fileAction( _packageContents[i] );
			}
		}

		public PackageItem FindFile( System.Predicate<PackageItem> matchFile )
		{
			if ( matchFile == null ) return null;
			if ( _packageContents == null ) return null;

			int	contentsCount = _packageContents.Count;

			for ( int i = 0; i < contentsCount; ++i )
			{
				if ( _packageContents[i] == null ) continue;
				if ( matchFile( _packageContents[i] ) == true ) return _packageContents[i];
			}

			return null;
		}

		public void DetectChanges( System.Action<string, string, string> onChange )
		{
			if ( !_pIsValid )
			{
				Debug.LogError( LOG_TAG + "Invalid package info" );
				return;
			}

			SelectFiles();

			Puma.PackageInfo	piB = new Puma.PackageInfo( _pPackageDeveloper, _pPackageName, _pPackageVersion, _pRepackageVersion, _pPackageSymbolsString );

			piB.ReadContentsFromSelection();

			if ( !piB._pIsValid )
			{
				Debug.LogError( LOG_TAG + "Invalid package info" );
				return;
			}

			PackageItem	fileB;

			ForEachFile( fileA =>
			{
				fileB = piB.FindFile( f => f._pFilePath == fileA._pFilePath );
				if ( fileB != null )
				{
					if ( fileA._pFileHash != fileB._pFileHash )
					{
						if ( onChange != null )
						{
							onChange( fileA._pFilePath, fileA._pFileHash, fileB._pFileHash );
						}
					}
				}
				else
				{
					if ( onChange != null )
					{
						onChange( fileA._pFilePath, fileA._pFileHash, null );
					}
				}
			} );
		}
	
		// ** PROTECTED
		//

		// ** PRIVATE
		//

		private void Reset()
		{
			_packageDeveloper = string.Empty;
			_packageName = string.Empty;
			_packageVersion = string.Empty;
			_repackageVersion = string.Empty;
			SetPackageSymbols( string.Empty );

			ResetContents();
			ResetChangeLog();
			RefreshPackageInfoPath();
		}

		private void SetPackageSymbols( string packageSymbols )
		{
			if ( string.IsNullOrEmpty( packageSymbols ) )
			{
				_packageSymbols = new string[] {};
			}
			else
			{
				_packageSymbols = packageSymbols.Split( ';', ',', ' ' );
			}
		}

		private void ResetContents()
		{
			_packageContents = new List<PackageItem>();
		}

		private void ResetChangeLog()
		{
			_changeLog = new List<ChangeLogEntry>();
		}

		private void ValidateChangeLog()
		{
			for ( int i = 0; i < _changeLog.Count; ++i )
			{
				if ( _changeLog[i] == null )
				{
					_changeLog.RemoveAt(i--);
					continue;
				}

				if ( string.IsNullOrEmpty( _changeLog[i]._pChangeSummary ) )
				{
					_changeLog.RemoveAt(i--);
					continue;
				}

				//split multiline logs
				if ( _changeLog[i]._pChangeSummary.Contains( "\n" ) )
				{
					string[]	separateLogs = _changeLog[i]._pChangeSummary.Split( '\n', '\r' );

					for ( int j = 0; j < separateLogs.Length; ++j )
					{
						if ( string.IsNullOrEmpty( separateLogs[j] ) ) continue;
						_changeLog.Insert( i, new ChangeLogEntry() { _pPackageVersion = _changeLog[i]._pPackageVersion, _pRepackageVersion = _changeLog[i]._pRepackageVersion, _pChangeSummary = separateLogs[j] } );
						++i;
					}

					_changeLog.RemoveAt(i--);
					continue;
				}
			}			
		}

		private void RefreshPackageInfoPath()
		{
			string	packageDeveloper = ToPascalCase( _packageDeveloper );
			string	packageName = ToPascalCase( _packageName );
			string	packageInfoFileName = "PackageInfo_" + packageDeveloper + "_" + packageName + ".txt";
			
			_packageInfoDirectory = "Assets/PackageInfo";
			_packageInfoPath = Path.Combine( _packageInfoDirectory, packageInfoFileName ).Replace( "\\", "/" );
		}

		private bool IsPackageInfoPath( string filePath )
		{
			return string.Equals( filePath, _packageInfoPath );
		}

		private string[] GetPackageItemPaths()
		{
			string[]	paths = new string[ _packageContents.Count ];

			for ( int i = 0; i < _packageContents.Count; ++i )
			{
				if ( _packageContents[i] != null )
				{
					paths[i] = _packageContents[i]._pFilePath;
				}

				if ( paths[i] == null )
				{
					paths[i] = string.Empty;
				}
			}

			return paths;
		}

		private string ToJson()
		{
			StringBuilder	sb = new StringBuilder();
			JsonWriter	jw = new JsonWriter( sb );
		
			jw.PrettyPrint = true;
			jw.WriteObjectStart();
		
			jw.WritePropertyName( KEY_INFO_VERSION );
			jw.Write( INFO_VERSION );
		
			jw.WritePropertyName( KEY_PACKAGE_DEVELOPER );
			jw.Write( _packageDeveloper );
		
			jw.WritePropertyName( KEY_PACKAGE_NAME );
			jw.Write( _packageName );
		
			jw.WritePropertyName( KEY_PACKAGE_VERSION );
			jw.Write( _packageVersion );
		
			if ( _pIsRepackage )
			{
				jw.WritePropertyName( KEY_REPACKAGE_VERSION );
				jw.Write( _repackageVersion );
			}
		
			if ( _pHasPackageSymbols )
			{
				jw.WritePropertyName( KEY_PACKAGE_SYMBOLS );
				jw.WriteArrayStart();

				for ( int i = 0; i < _packageSymbols.Length; ++i )
				{
					jw.Write( _packageSymbols[i] );
				}

				jw.WriteArrayEnd();
			}
		
			jw.WritePropertyName( KEY_PACKAGE_CONTENTS );
			jw.WriteArrayStart();
		
			for ( int i = 0; i < _packageContents.Count; ++i )
			{
				if ( _packageContents[i] == null ) continue;
				if ( string.IsNullOrEmpty( _packageContents[i]._pFilePath ) ) continue;

				jw.WriteObjectStart();

				jw.WritePropertyName( KEY_PACKAGE_ITEM_PATH );
				jw.Write( _packageContents[i]._pFilePath );

				jw.WritePropertyName( KEY_PACKAGE_ITEM_HASH );
				jw.Write( _packageContents[i]._pFileHash );

				jw.WriteObjectEnd();
			}
		
			jw.WriteArrayEnd();

			ValidateChangeLog();
		
			jw.WritePropertyName( KEY_CHANGE_LOG );
			jw.WriteArrayStart();
		
			for ( int i = 0; i < _changeLog.Count; ++i )
			{
				if ( _changeLog[i] == null ) continue;
				if ( string.IsNullOrEmpty( _changeLog[i]._pChangeSummary ) ) continue;

				jw.WriteObjectStart();

				if ( !string.IsNullOrEmpty( _changeLog[i]._pPackageVersion ) )
				{
					jw.WritePropertyName( KEY_PACKAGE_VERSION );
					jw.Write( _changeLog[i]._pPackageVersion );
				}
		
				if ( !string.IsNullOrEmpty( _changeLog[i]._pRepackageVersion ) )
				{
					jw.WritePropertyName( KEY_REPACKAGE_VERSION );
					jw.Write( _changeLog[i]._pRepackageVersion );
				}

				jw.WritePropertyName( KEY_CHANGE_LOG_ENTRY_SUMMARY );
				jw.Write( _changeLog[i]._pChangeSummary );

				jw.WriteObjectEnd();
			}
		
			jw.WriteArrayEnd();
		
			jw.WriteObjectEnd();
		
			return sb.ToString();
		}

		private void ReadPackageInfoStr( string packageInfoStr, System.Action<JsonData, int> readFromJson = null )
		{
			JsonReader	jr = new JsonReader( packageInfoStr );
			JsonData	jd;
			
			try
			{
				jd = JsonMapper.ToObject( jr );
			}
			catch ( JsonException )
			{
				return;
			}

			if ( jd == null ) return;
			if ( !jd.IsObject ) return;
			if ( !jd.Contains( KEY_INFO_VERSION ) ) return;

			JsonData	infoVersionData = jd[ KEY_INFO_VERSION ];

			if ( infoVersionData == null ) return;
			if ( !infoVersionData.IsInt ) return;

			int	infoVersion = ( int )infoVersionData;

			if ( readFromJson == null )
			{
				readFromJson = ReadFromJson;
			}

			readFromJson( jd, infoVersion );
		}

		private void ReadFromJson( JsonData infoData, int infoVersion )
		{
			Reset();

			JsonData	jd;

			if ( infoData.Contains( KEY_PACKAGE_DEVELOPER ) )
			{
				jd = infoData[ KEY_PACKAGE_DEVELOPER ];

				if ( jd != null && jd.IsString )
				{
					_packageDeveloper = ( string )jd;
				}
			}

			if ( infoData.Contains( KEY_PACKAGE_NAME ) )
			{
				jd = infoData[ KEY_PACKAGE_NAME ];

				if ( jd != null && jd.IsString )
				{
					_packageName = ( string )jd;
				}
			}

			if ( infoData.Contains( KEY_PACKAGE_VERSION ) )
			{
				jd = infoData[ KEY_PACKAGE_VERSION ];

				if ( jd != null && jd.IsString )
				{
					_packageVersion = ( string )jd;
				}
			}

			if ( infoData.Contains( KEY_REPACKAGE_VERSION ) )
			{
				jd = infoData[ KEY_REPACKAGE_VERSION ];

				if ( jd != null && jd.IsString )
				{
					_repackageVersion = ( string )jd;
				}
			}

			RefreshPackageInfoPath();

			ReadSymbolsFromJson( infoData, infoVersion );
			ReadContentsFromJson( infoData, infoVersion );
			ReadChangeLogFromJson( infoData, infoVersion );
		}

		private void ReadSymbolsFromJson( JsonData infoData, int infoVersion )
		{
			SetPackageSymbols( string.Empty );

			JsonData	jd;

			if ( infoVersion < 2 )
			{
				if ( infoData.Contains( DEPRECATED_KEY_PACKAGE_SYMBOL ) )
				{
					jd = infoData[ DEPRECATED_KEY_PACKAGE_SYMBOL ];

					if ( jd != null && jd.IsString )
					{
						_packageSymbols = new string[] {  ( string )jd };
					}
				}
			}
			else
			{
				if ( infoData.Contains( KEY_PACKAGE_SYMBOLS ) )
				{
					jd = infoData[ KEY_PACKAGE_SYMBOLS ];

					if ( jd != null && jd.IsArray )
					{
						_packageSymbols = new string[ jd.Count ];

						for ( int i = 0; i < jd.Count; ++i )
						{
							if ( jd[i] != null && jd[i].IsString )
							{
								_packageSymbols[i] = ( string )jd[i];
							}
							else
							{
								_packageSymbols[i] = string.Empty;
							}
						}
					}
				}
			}
		}

		private void ReadContentsFromJson( JsonData infoData, int infoVersion )
		{
			ResetContents();

			if ( infoData.Contains( KEY_PACKAGE_CONTENTS ) )
			{
				JsonData	contentsData = infoData[ KEY_PACKAGE_CONTENTS ];

				if ( contentsData != null && contentsData.IsArray )
				{
					_packageContents.Clear();

					int	contentsCount = contentsData.Count;
					JsonData	jd, jd2;
					string	filePath, fileHash;
					bool	isPackageInfoFile;

					for ( int i = 0; i < contentsCount; ++i )
					{
						jd = contentsData[i];

						if ( jd == null ) continue;

						if ( infoVersion < 3 )
						{
							if ( !jd.IsString ) continue;

							filePath = ( string )jd;
							isPackageInfoFile = IsPackageInfoPath( filePath );

							_packageContents.Add( new PackageItem( filePath, isPackageInfoFile ) );
						}
						else
						{
							if ( !jd.IsObject ) continue;

							jd2 = jd.TryGet( KEY_PACKAGE_ITEM_PATH, JsonType.String );

							if ( jd2 == null ) continue;

							filePath = ( string )jd2;

							jd2 = jd.TryGet( KEY_PACKAGE_ITEM_HASH, JsonType.String );

							if( jd2 != null )
							{
								fileHash = ( string )jd2;
							}
							else
							{
								fileHash = null;
							}

							_packageContents.Add( new PackageItem( filePath, fileHash ) );
						}
					}
				}
			}
		}

		private void ReadChangeLogFromJson( JsonData infoData, int infoVersion )
		{
			ResetChangeLog();

			JsonData	jd;

			if ( infoData.Contains( KEY_CHANGE_LOG ) )
			{
				JsonData	changeLogData = infoData[ KEY_CHANGE_LOG ];

				if ( changeLogData != null && changeLogData.IsArray )
				{
					int	entryCount = changeLogData.Count;
					JsonData	entryData;
					string	entryPackageVersion, entryRepackageVersion, entrySummary;
					ChangeLogEntry	entry;

					for ( int i = 0; i < entryCount; ++i )
					{
						entryData = changeLogData[i];

						if ( entryData == null ) continue;
						if ( !entryData.IsObject ) continue;

						entryPackageVersion = null;
						if ( entryData.Contains( KEY_PACKAGE_VERSION ) )
						{
							jd = entryData[ KEY_PACKAGE_VERSION ];
							if ( jd != null && jd.IsString )
							{
								entryPackageVersion = ( string )jd;
							}
						}

						entryRepackageVersion = null;
						if ( entryData.Contains( KEY_REPACKAGE_VERSION ) )
						{
							jd = entryData[ KEY_REPACKAGE_VERSION ];
							if ( jd != null && jd.IsString )
							{
								entryRepackageVersion = ( string )jd;
							}
						}

						entrySummary = null;
						if ( entryData.Contains( KEY_CHANGE_LOG_ENTRY_SUMMARY ) )
						{
							jd = entryData[ KEY_CHANGE_LOG_ENTRY_SUMMARY ];
							if ( jd != null && jd.IsString )
							{
								entrySummary = ( string )jd;
							}
						}

						if ( string.IsNullOrEmpty( entrySummary ) ) continue;

						entry = new ChangeLogEntry();
						entry._pPackageVersion = entryPackageVersion;
						entry._pRepackageVersion = entryRepackageVersion;
						entry._pChangeSummary = entrySummary;

						_changeLog.Add( entry );
					}
				}
			}
		}

		private static string ToPascalCase( string origString )
		{
			if ( string.IsNullOrEmpty( origString ) ) return origString;
			if ( origString.Length == 1 ) return origString.ToUpper();

			string[]	words = origString.Split( new char[] {}, System.StringSplitOptions.RemoveEmptyEntries );

			System.Text.StringBuilder	sb = new StringBuilder();

			foreach ( string word in words )
			{
				sb.Append( word.Substring(0, 1).ToUpper() + word.Substring(1) );
			}

			return sb.ToString();
		}
	}
}

