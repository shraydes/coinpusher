////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// SquadBase.cs
//
// Created: 24/06/2014 chope
// Contributors: 
// 
// Intention:
// Handles Group Knowledge and information sharing between brains. 
//
// Remark: 
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using AmuzoActor;
using GameDefines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
#region SquadMember - CLASS
public class SquadMember
{
	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	internal bool _waitingToKick;
	internal bool _isAttacking;
	internal int _idActorMember;
	internal int _idActorTarget;
	internal float _targetScore;
	internal float _targetingStartTime;
	internal ActorLogicBase _memberActor = null;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	internal bool _pHasTarget
	{
		get
		{
			return ( _idActorTarget != GlobalDefines.INVALID_ID );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	internal SquadMember()
	{
		Reset();
	}

	//------------------------------------------------------------------------------------------------------------------
	internal void Reset()
	{
		_idActorMember = GlobalDefines.INVALID_ID;
		_idActorTarget = GlobalDefines.INVALID_ID;
		_targetScore = 0.0f;
		_targetingStartTime = -1.0f;
		_memberActor = null;
	}
};
#endregion // SquadMember

//----------------------------------------------------------------------------------------------------------------------
#region SquadTarget - CLASS
public class SquadTarget
{
	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	internal int _idActor;
	internal int _numTargeters;
	internal ActorLogicBase _targetActor = null;

	//------------------------------------------------------------------------------------------------------------------
	internal SquadTarget()
	{
		Reset();
	}

	//------------------------------------------------------------------------------------------------------------------
	internal void Reset()
	{
		_idActor = GlobalDefines.INVALID_ID;
		_numTargeters = 0;
		_targetActor = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	internal void IncrementNumTargeters()
	{
		_numTargeters++;
	}

	//------------------------------------------------------------------------------------------------------------------
	internal void DecrementNumTargeters()
	{
		if( _numTargeters > 0 )
			_numTargeters--;
	}
};
#endregion // SquadTarget

//----------------------------------------------------------------------------------------------------------------------
public abstract class SquadBase : MonoBehaviour
{
	//------------------------------------------------------------------------------------------------------------------
	// STATICS / CONSTS / DEFINES
	static readonly protected int _cDefaultCapacity = 15;

	private const float TIME_TARGETING_MIN_DEFAULT = 1.5f;
	private const float SCORE_KICK_THRESHOLD_DEFAULT = 5.0f;

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	public int _maxMembers = -1;
	public int _maxTargetersPerTarget = 15;
	public int _maxAttackersPerTarget = 1;
	public bool _renderDebugInfo = false;

	protected ActorLogicBase _dummyActor = null;
	protected SquadMember _dummyMember = null;
	protected SquadTarget _dummyTarget = null;

	internal Dictionary<int, SquadMember> _members = new Dictionary<int, SquadMember>( _cDefaultCapacity );
	internal Dictionary<int, SquadTarget> _targets = new Dictionary<int, SquadTarget>( _cDefaultCapacity );

	private int _numMembersTotal;
	private int _numMembersIndividual;
	private bool _anyMembersVisiblePart;
	private bool _anyMembersVisibleFull;
	private bool _allMembersVisiblePart;
	private bool _allMembersVisibleFull;
	private Vector3 _dummyVector = new Vector3();
	private List<int> _deleteList = new List<int>( _cDefaultCapacity );
	private List<SquadMember> _membersUnused = new List<SquadMember>( _cDefaultCapacity );
	private List<SquadTarget> _targetsUnused = new List<SquadTarget>( _cDefaultCapacity );

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES

	// excludes child actors attached to memebers. 
	public int _pNumMembers
	{
		get
		{
			return _numMembersIndividual;
		}
	}

	// includes child actors attached to members. 
	public int _pNumMembersTotal
	{
		get
		{
			return _numMembersTotal;
		}
	}

	// the number of threat targets the squad has between them. 
	public int _pNumTargets
	{
		get
		{
			return _targets.Count;
		}
	}

	public bool _pHasMembers
	{
		get
		{
			return ( _pNumMembers != 0 );
		}
	}
	public bool _pAnyMembersVisiblePart
	{
		get
		{
			return _anyMembersVisiblePart;
		}
	}
	public bool _pAnyMembersVisibleFull
	{
		get
		{
			return _anyMembersVisibleFull;
		}
	}
	public bool _pAllMembersVisiblePart
	{
		get
		{
			return _allMembersVisiblePart;
		}
	}
	public bool _pAllMembersVisibleFull
	{
		get
		{
			return _allMembersVisibleFull;
		}
	}

	public bool _pIsFull
	{
		get
		{
			if( _maxMembers < 0 )
				return false;
			return _numMembersIndividual >= _maxMembers;
		}
	}

	// the number of squad members that can target the same threat. 
	public int _pMaxTargetersPerTarget
	{
		get
		{
			// max targeters of less than 0 means infinite. 
			if( _maxTargetersPerTarget < 0 )
			{
				return _numMembersTotal;
			}
			return _maxTargetersPerTarget;
		}
		set
		{
			_maxTargetersPerTarget = value;
		}
	}

	protected virtual float _pScoreKickThreshold
	{
		get
		{
			return SCORE_KICK_THRESHOLD_DEFAULT;
		}
	}

	protected virtual float _pTimeTargetingMin
	{
		get
		{
			return TIME_TARGETING_MIN_DEFAULT;
		}
	}


	//------------------------------------------------------------------------------------------------------------------
	// ABSTRACTS
	public abstract bool CanMemberBeAddedToThisSquad( ActorLogicBase actorMember );
	public abstract bool CanTargetBeAddedToThisSquad( ActorLogicBase actorTarget );
	protected abstract bool IsValidTarget( int targetIdActor );

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void Awake()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void Start()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void OnDestroy()
	{
		_dummyActor = null;
		_dummyMember = null;
		_dummyTarget = null;

		ClearAllTargets();
		ClearAllMembers( true );
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void Update()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void FixedUpdate()
	{
		UpdateMemberVisibility();

		RemoveInvalidSquadTargets();
		RemoveInvalidSquadMembers();
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void AddMember( ActorLogicBase actor )
	{
		// don't add actors that we already have as members or are not allowed in this squad. 
		if( actor == null )
			return;
		if( IsMemberOfSquad( actor._pIdActor ) )
			return;
		if( !CanMemberBeAddedToThisSquad( actor ) )
			return;

		SquadMember newSquadMember = null;
		if( _membersUnused.Count > 0 )
		{
			// if we have already made unused members then use one of them. 
			newSquadMember = _membersUnused[0];
			_membersUnused.RemoveAt( 0 );
		}
		else
		{
			// otherwise make a new member. 
			newSquadMember = new SquadMember();
		}

		if( newSquadMember != null )
		{
			newSquadMember.Reset();
			newSquadMember._idActorMember = actor._pIdActor;
			newSquadMember._memberActor = actor;
			newSquadMember._memberActor._pSquad = this;

			_members.Add( newSquadMember._idActorMember, newSquadMember );

			OnNumMembersChanged( 1 );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void RemoveMember( int idActor, bool freeActorAsWell = false )
	{
		if( !IsMemberOfSquad( idActor ) )
			return;

		_dummyMember = _members[idActor];
		if( _dummyMember != null )
		{
			// if we are targeting anyone remove us from them. 
			if( _dummyMember._pHasTarget )
			{
				RemoveTargeter( idActor );
			}

			_dummyActor = _dummyMember._memberActor;

			// reset it, add it to the unused list and remove from used dictionary. 
			_dummyMember.Reset();
			_membersUnused.Add( _dummyMember );
			_members.Remove( idActor );


			if( _dummyActor != null )
			{
				_dummyActor._pSquad = null;

				if( freeActorAsWell )
				{
					_dummyActor.ReturnActor();
				}
			}

			OnNumMembersChanged( -1 );
		}

		_dummyMember = null;
		_dummyActor = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnSquadMemberDeath( int idActorMember )
	{
		UpdateAttackStatus( idActorMember, false );
		RemoveMember( idActorMember );
	}

	//------------------------------------------------------------------------------------------------------------------
	// nothig at base level but you can override to tell other members. 
	public virtual void OnSquadMemberTakeDamage( int idActorMember, ref float amount )
	{
		// nothing at base level. 		
	}

	//------------------------------------------------------------------------------------------------------------------
	// allows actor to ask thier squad if they can/should target a given target. 
	public virtual bool CanBeTargeted( int targetIdActor, int targeterIdActor = GlobalDefines.INVALID_ID )
	{
		if( !IsValidTarget( targetIdActor ) )
		{
			return false;
		}

		// if we havent got this entity as a target already then we can target it.
		if( !IsTargetOfSquad( targetIdActor ) )
			return true;

		if( targeterIdActor != GlobalDefines.INVALID_ID )
		{
			// if the targeter passed is not a member of this squad then they can not target them. 
			if( !IsMemberOfSquad( targeterIdActor ) )
				return false;

			// if the supplied member id is already targeting this target then they can continue to do so. 
			if( _members[targeterIdActor]._idActorTarget == targetIdActor )
				return true;
		}

		// if we are not already at the max targeters then yes we can target them. 
		if( _targets[targetIdActor]._numTargeters < _pMaxTargetersPerTarget )
		{
			return true;
		}

		return false;
	}

	//------------------------------------------------------------------------------------------------------------------
	// allows actor to ask thier squad if they can/should target a given target. 
	public virtual bool CanBeAttacked( int targetIdActor, int targeterIdActor = GlobalDefines.INVALID_ID )
	{
		if( !IsValidTarget( targetIdActor ) )
			return false;

		// if we havent got this entity as a target already then we cant attack it
		if( !IsTargetOfSquad( targetIdActor ) )
			return false;

		if( targeterIdActor != GlobalDefines.INVALID_ID )
		{
			// if the targeter passed is not a member of this squad then they can not target them. 
			if( !IsMemberOfSquad( targeterIdActor ) )
				return false;

			// the supplied memeber can only attack the target if they are already targeting them. 
			if( _members[targeterIdActor]._idActorTarget != targetIdActor )
				return false;
		}
		
		// find the number of members currently attacking the target. 
		int numAttackers = 0;
		foreach( KeyValuePair<int, SquadMember> currentMember in _members )
		{
			if( currentMember.Value == null )
				continue;
			if( currentMember.Value._memberActor == null )
				continue;
			if( currentMember.Value._idActorTarget != targetIdActor )
				continue;
			if( currentMember.Value._memberActor._pIsDead )
				continue;

			if( currentMember.Value._isAttacking )
				numAttackers++;
		}

		// we have fewer attackers than max, this targeter can attack. 
		if( numAttackers < _maxAttackersPerTarget )
			return true;

		return false;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void UpdateAttackStatus( int targeterIdActor, bool attacking )
	{
		if( !IsMemberOfSquad( targeterIdActor ) )
			return;

		_members[targeterIdActor]._isAttacking = attacking;
	}

	//------------------------------------------------------------------------------------------------------------------
	// inform squad that a member wants to start targeting a specific target threat. 
	// Score is that given from the members brain/knowledge, 
	// the squad can use this to tell them to find someone else if others are already targeting with higher scores.
	// using the kick targeter function.  
	public bool AddTargeter( int idActorTargeter, int idActorTarget, float score = 0.0f )
	{
		if( !IsMemberOfSquad( idActorTargeter ) )
			return false;
		_dummyMember = _members[idActorTargeter];
		if( _dummyMember == null )
			return false;

		// find the actor we are about to target. 
		_dummyActor = null;
		ActorDataBase.FindActor( idActorTarget, ref _dummyActor );
		if( _dummyActor == null )
			return false;

		// make sure we are allowed to target them. 
		if( !CanBeTargeted( idActorTarget ) )
			return false;

		bool bIncrementNumTargets = false;
		if( _dummyMember._pHasTarget && ( _dummyMember._idActorTarget != idActorTarget ) )
		{
			// If we are targeting something which is different to what we are asking to remove us from them. 
			RemoveTargeter( idActorTargeter );
		}

		// so we should be already targeting them or targeting no-one. 
		if( !_dummyMember._pHasTarget || !IsTargetOfSquad( idActorTarget ) )
		{
			AddTarget( _dummyActor );
			bIncrementNumTargets = true;
		}

		if( _targets.ContainsKey( idActorTarget ) )
		{
			_dummyTarget = _targets[idActorTarget];
			if( _dummyTarget != null )
			{
				// update our member's data
				bool newTarget = _dummyMember._idActorTarget != idActorTarget;
				_dummyMember._idActorTarget = idActorTarget;
				_dummyMember._targetScore = score;

				if( newTarget )
				{
					OnTargetersChanged();
				}

				if( bIncrementNumTargets )
				{
					_dummyTarget.IncrementNumTargeters();
					_dummyMember._targetingStartTime = Time.time;
				}
			}
		}

		_dummyMember = null;
		_dummyActor = null;
		_dummyTarget = null;

		return true;
	}

	//------------------------------------------------------------------------------------------------------------------
	// inform squad that a member has stopped targeting a specific target threat
	// idActorTarget = invalid if you are saying they have stopped targeting everything. 
	public void RemoveTargeter( int idActorTargeter, int idActorTarget = GlobalDefines.INVALID_ID )
	{
		if( !IsMemberOfSquad( idActorTargeter ) )
			return;
		_dummyMember = _members[idActorTargeter];
		if( _dummyMember == null )
			return;
		if( !_dummyMember._pHasTarget )
			return;

		// if the target has not been defined then set it to whoever we re targeting. 
		if( idActorTarget == GlobalDefines.INVALID_ID )
		{
			idActorTarget = _dummyMember._idActorTarget;
		}

		// update our squads target list. 
		if( IsTargetOfSquad( idActorTarget ) )
		{
			_dummyTarget = _targets[idActorTarget];

			if( _dummyTarget != null )
			{
				_dummyTarget.DecrementNumTargeters();

				// if this target no longer has any targeters remove it from squad target list
				if( _dummyTarget._numTargeters == 0 )
				{
					_dummyTarget = null;
					RemoveTarget( idActorTarget );
				}
			}
		}

		// update the member data. 
		_dummyMember._idActorTarget = GlobalDefines.INVALID_ID;
		_dummyMember._targetingStartTime = 0.0f;
		_dummyMember._targetScore = 0.0f;
	}

	//------------------------------------------------------------------------------------------------------------------
	public int GetNearestSquadMember( ref Vector3 refPosition, int memberIdToIgnore = GlobalDefines.INVALID_ID )
	{
		int returnInt = GlobalDefines.INVALID_ID;

		if( !_pHasMembers )
		{
			return returnInt;
		}

		float nearestDistSqrd = Mathf.Infinity;

		foreach( KeyValuePair<int, SquadMember> currentMember in _members )
		{
			if( currentMember.Value == null )
				continue;
			if( currentMember.Value._memberActor == null )
				continue;
			if( currentMember.Value._idActorMember == memberIdToIgnore )
				continue;
			if( currentMember.Value._memberActor._pIsChild )
				continue;

			// find the vector from the ref pos to this member. 
			_dummyVector = currentMember.Value._memberActor._pPosition - refPosition;

			float magSqrd = _dummyVector.sqrMagnitude;
			if( magSqrd < nearestDistSqrd )
			{
				nearestDistSqrd = magSqrd;
				returnInt = currentMember.Value._idActorMember;
			}
		}

		return returnInt;
	}

	//------------------------------------------------------------------------------------------------------------------
	public Vector3 GetNearestSquadMemberPosition( 
		ref Vector3 refPosition, int memberIdToIgnore = GlobalDefines.INVALID_ID )
	{
		if( !_pHasMembers )
		{
			return Vector3.zero;
		}

		int nearestMemberIdActor = GetNearestSquadMember( ref refPosition, memberIdToIgnore );
		if( ( nearestMemberIdActor != GlobalDefines.INVALID_ID ) && _members.ContainsKey( nearestMemberIdActor ) )
		{
			if( _members[nearestMemberIdActor]._memberActor != null )
			{
				return _members[nearestMemberIdActor]._memberActor._pPosition;
			}
		}
		return Vector3.zero;
	}

	//------------------------------------------------------------------------------------------------------------------
	// returns the distance as a float, but sets the idActor of the nearest member to nearestMemberIdActor. 
	// use the member to ignore if you want to NOT include the actor requesting he data. 
	public float GetDistanceToNearestSquadMember( ref Vector3 refPosition, ref int nearestMemberIdActor,
		int memberIdToIgnore = GlobalDefines.INVALID_ID )
	{
		float returnFloat = 0.0f;
		if( !_pHasMembers )
		{
			return returnFloat;
		}

		nearestMemberIdActor = GetNearestSquadMember( ref refPosition, memberIdToIgnore );
		if( ( nearestMemberIdActor != GlobalDefines.INVALID_ID ) && _members.ContainsKey( nearestMemberIdActor ) )
		{
			if( _members[nearestMemberIdActor]._memberActor != null )
			{
				returnFloat =
					( _members[nearestMemberIdActor]._memberActor._pPosition - refPosition ).magnitude;
			}
		}
		return returnFloat;
	}

	//------------------------------------------------------------------------------------------------------------------
	// returns the vector as a NON_Normalised Vector3, sets the idActor of the nearest member to nearestMemberIdActor. 
	// use the member to ignore if you want to NOT include tha ctor requesting he data. 
	public Vector3 GetVectorToNearestSquadMember( ref Vector3 refPosition, ref int nearestMemberIdActor,
		int memberIdToIgnore = GlobalDefines.INVALID_ID )
	{
		Vector3 returnVec = Vector3.zero;
		if( !_pHasMembers )
		{
			return returnVec;
		}

		nearestMemberIdActor = GetNearestSquadMember( ref refPosition, memberIdToIgnore );
		if( ( nearestMemberIdActor != GlobalDefines.INVALID_ID ) && _members.ContainsKey( nearestMemberIdActor ) )
		{
			if( _members[nearestMemberIdActor]._memberActor != null )
			{
				returnVec = ( _members[nearestMemberIdActor]._memberActor._pPosition - refPosition );
			}
		}
		return returnVec;
	}

	//------------------------------------------------------------------------------------------------------------------
	// used for cycling through all sqaud members from outside of squad code. 
	public void GetMemberActorByIndex( int index, ref ActorLogicBase actor )
	{
		if( index >= _pNumMembers )
		{
			actor = null;
			return;
		}

		int count = 0;
		foreach( SquadMember current in _members.Values )
		{
			if( ( count == index ) && ( current != null ) )
			{
				actor = current._memberActor;
				return;
			}
			count++;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	// alows an actor give the squad thier knowledge about enemies and using group knowledge tell them which to use. 
	// returns the actor id of the best target chosen. 
	public virtual int IssueBestTargetFromCollection( ref List<Knowledge> knowledgeList, int idActorMember )
	{
		// get our squad member.
		if( !IsMemberOfSquad( idActorMember ) )
			return GlobalDefines.INVALID_ID;

		_dummyMember = _members[idActorMember];
		if( _dummyMember == null )
			return GlobalDefines.INVALID_ID;

		// if the collection is empty just make sure we are not targeting anyone... we dont have a threat. 
		if( ( knowledgeList == null ) || ( knowledgeList.Count == 0 ) )
		{
			RemoveTargeter( idActorMember );
			return GlobalDefines.INVALID_ID;
		}

		// if we think we are targeting an actor that is not in our target list (they died) stop us from targeting them
		if( _dummyMember._pHasTarget && !IsTargetOfSquad( _dummyMember._idActorTarget ) )
		{
			RemoveTargeter( idActorMember );
		}

		// if we have not targeted for the min time already dont change yet. 
		if( _dummyMember._pHasTarget && ( ( Time.time - _dummyMember._targetingStartTime ) < _pTimeTargetingMin ) )
		{
			return _dummyMember._idActorTarget;
		}

		// if we have been kicked from our target do it now. 
		if( _dummyMember._waitingToKick )
		{
			RemoveTargeter( idActorMember );
			_dummyMember._waitingToKick = false;
		}

		int idOfChosenTarget = GlobalDefines.INVALID_ID;
		float scoreOfChosenTarget = 0.0f;

		// go through the sorted list of knowledge and find the first one we can use
		foreach( Knowledge currentKnowledge in knowledgeList )
		{
			if( currentKnowledge == null )
				continue;

			// if they are not already a target of the squad then go ahead and target them. 
			if( !IsTargetOfSquad( currentKnowledge._idActorTarget ) )
			{
				AddTargeter(
					_dummyMember._idActorMember, currentKnowledge._idActorTarget, currentKnowledge._threatValue );
				return currentKnowledge._idActorTarget;
			}

			_dummyTarget = _targets[currentKnowledge._idActorTarget];

			// if we are already targeting this target and no one else is then update the score and return
			if( ( _dummyTarget != null ) && ( _dummyTarget._numTargeters <= 1 ) &&
				( _dummyMember._idActorTarget == currentKnowledge._idActorTarget ) )
			{
				_dummyMember._targetScore = currentKnowledge._threatValue;
				return currentKnowledge._idActorTarget;
			}

			// this is 1st targetable threat (and so the best available threat because the list is ordered), 
			// record details encase we get to the end without finding a better one. 
			if( CanBeTargeted( currentKnowledge._idActorTarget, idActorMember ) &&
				( idOfChosenTarget == GlobalDefines.INVALID_ID ) )
			{
				idOfChosenTarget = currentKnowledge._idActorTarget;
				scoreOfChosenTarget = currentKnowledge._threatValue;
			}

			// so if we get here some other members are stopping us from targeting, find if we are a better targeter
			// NB, no targeter number check, just kick if we have a higher score, they can be readded in their update if need be. 
			if( ShouldWeKickLowPriorityTargeter(
				idActorMember, currentKnowledge._idActorTarget, currentKnowledge._threatValue, true, false ) )
			{
				// we have just kicked someone, now add ourselves in their place. 
				AddTargeter( idActorMember, currentKnowledge._idActorTarget, currentKnowledge._threatValue );
				return currentKnowledge._idActorTarget;
			}
		}

		// we have not found a completely free target, share the best one we found. 
		if( idOfChosenTarget != GlobalDefines.INVALID_ID )
		{
			AddTargeter( _dummyMember._idActorMember, idOfChosenTarget, scoreOfChosenTarget );
			return idOfChosenTarget;
		}
		else
		{
			// if we havent found a target at all just make sure we are not targeting anyone. 
			RemoveTargeter( _dummyMember._idActorMember );
			return GlobalDefines.INVALID_ID;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool IsMemberOfSquad( int idActor )
	{
		if( _members == null )
			return false;
		if( _members.Count == 0 )
			return false;
		return _members.ContainsKey( idActor );
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool IsTargetOfSquad( int idActor )
	{
		if( _targets == null )
			return false;
		if( _targets.Count == 0 )
			return false;
		return _targets.ContainsKey( idActor );
	}

	//------------------------------------------------------------------------------------------------------------------
	public ActorLogicBase GetSquadMemberActor( int idActorMember )
	{
		if( !_members.ContainsKey( idActorMember ) )
			return null;

		return _targets[idActorMember]._targetActor;
	}

	//------------------------------------------------------------------------------------------------------------------
	public ActorLogicBase GetSquadTargetActor( int idActorTarget )
	{
		if( !_targets.ContainsKey( idActorTarget ) )
			return null;

		return _targets[idActorTarget]._targetActor;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void RestoreAllMemberHealth()
	{
		foreach( KeyValuePair<int, SquadMember> currentPair in _members )
		{
			if( currentPair.Value == null )
				continue;
			if( currentPair.Value._memberActor == null )
				continue;

			currentPair.Value._memberActor._pHealth = currentPair.Value._memberActor._pHealthMax;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public void DamageAllMembers( float amount, int type )
	{
		foreach( SquadMember current in _members.Values )
		{
			if( current == null )
				continue;
			if( current._memberActor == null )
				continue;
			if( current._memberActor._pIsChild )
				continue;

			current._memberActor.TakeDamage(
				amount, current._memberActor._pPosition, Vector3.up, GlobalDefines.INVALID_ID, false, type );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public void ClearAllMembers( bool freeAsWell = false )
	{
		List<int> _memberIds = new List<int>( _pNumMembers );
		foreach( KeyValuePair<int, SquadMember> currentPair in _members )
		{
			_memberIds.Add( currentPair.Key );
		}

		foreach( int currentId in _memberIds )
		{
			RemoveMember( currentId, freeAsWell );
		}

		_members.Clear();
	}

	//------------------------------------------------------------------------------------------------------------------
	// Takes non-targeting member id and finds if this member is a more suitable targeter for the Target id supplied 
	// than any of those already targeting the same target. 
	public bool ShouldWeKickLowPriorityTargeter(
		int idActorMember, int idActorTarget, float targetScore, bool bDoIt = true, bool bCheckNumTargeters = true )
	{
		if( !IsMemberOfSquad( idActorMember ) )
			return false;

		// if target can be targeted by new members then there is no need to kick others from thier targeting slots. 
		if( bCheckNumTargeters && CanBeTargeted( idActorTarget ) )
		{
			return false;
		}

		// if this member is already targeting this target then we should definitely not kick other targeters. 
		if( _members[idActorMember]._idActorTarget == idActorTarget )
		{
			return false;
		}

		foreach( KeyValuePair<int, SquadMember> currentPair in _members )
		{
			if( currentPair.Value == null )
				continue;
			if( currentPair.Key == idActorMember )
				continue;

			// if this member is targeting the target
			if( currentPair.Value._idActorTarget == idActorTarget )
			{
				// if the member targeting has a lower score then consider kicking them. 
				if( currentPair.Value._targetScore < targetScore )
				{
					// if the differences in score are enough to warrant a kick do it. 
					if( _pScoreKickThreshold <= ( targetScore - currentPair.Value._targetScore ) )
					{
						if( bDoIt )
						{
							KickLowestPriorityTargeter( idActorTarget );
						}

						return true;
					}
				}
			}
		}

		return false;
	}

	//------------------------------------------------------------------------------------------------------------------
	// removes the targeter of the supplied target that has the lowest score for doing so. 
	public bool KickLowestPriorityTargeter( int idActorTarget )
	{
		int memberIdLowestScore = GlobalDefines.INVALID_ID;
		float scoreLowest = float.MaxValue;

		foreach( KeyValuePair<int, SquadMember> currentPair in _members )
		{
			if( currentPair.Value == null )
				continue;

			// if this member is targeting the target
			if( currentPair.Value._idActorTarget == idActorTarget )
			{
				// if the member targeting has a lower score then consider kicking them. 
				if( currentPair.Value._targetScore < scoreLowest )
				{
					scoreLowest = currentPair.Value._targetScore;
					memberIdLowestScore = currentPair.Value._idActorMember;
				}
			}
		}

		if( memberIdLowestScore != GlobalDefines.INVALID_ID )
		{
			RemoveTargeter( memberIdLowestScore );
			return true;
		}

		return false;
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void OnNumMembersChanged( int change )
	{
		UpdateMemberNumbers();
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void OnNumTargetsChanged()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void OnTargetersChanged()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected void UpdateMemberNumbers()
	{
		_numMembersTotal = _members.Count;
		_numMembersIndividual = 0;
		foreach( KeyValuePair<int, SquadMember> currentPair in _members )
		{
			if( currentPair.Value == null )
				continue;
			if( currentPair.Value._memberActor == null )
				continue;

			// if this is not a child actor then count them as an individual member. 
			if( !currentPair.Value._memberActor._pIsChild )
			{
				_numMembersIndividual++;
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void UpdateMemberVisibility()
	{
		_allMembersVisiblePart = true;
		_allMembersVisibleFull = true;
		_anyMembersVisiblePart = false;
		_anyMembersVisibleFull = false;

		// go through our members and find the data that is referenced from other places. 
		foreach( KeyValuePair<int, SquadMember> currentPair in _members )
		{
			if( currentPair.Value == null )
				continue;
			if( currentPair.Value._memberActor == null )
				continue;
			if( currentPair.Value._memberActor._pIsChild )
				continue;

			_allMembersVisibleFull &= currentPair.Value._memberActor._pIsInCameraViewFull;
			_allMembersVisiblePart &= currentPair.Value._memberActor._pIsInCameraViewPart;

			if( currentPair.Value._memberActor._pIsInCameraViewFull )
			{
				_anyMembersVisiblePart = true;
			}

			if( currentPair.Value._memberActor._pIsInCameraViewPart )
			{
				_anyMembersVisiblePart = true;
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public void AddTarget( ActorLogicBase actor )
	{
		// don't add actors that we already have as targets
		if( actor == null )
			return;
		if( IsTargetOfSquad( actor._pIdActor ) )
			return;
		if( !CanTargetBeAddedToThisSquad( actor ) )
			return;

		SquadTarget newSquadTarget = null;
		if( _targetsUnused.Count > 0 )
		{
			// if we have already made unused members then use one of them. 
			newSquadTarget = _targetsUnused[0];
			_targetsUnused.RemoveAt( 0 );
		}
		else
		{
			// otherwise make a new member. 
			newSquadTarget = new SquadTarget();
		}

		if( newSquadTarget != null )
		{
			newSquadTarget.Reset();
			newSquadTarget._idActor = actor._pIdActor;
			newSquadTarget._targetActor = actor;

			_targets.Add( newSquadTarget._idActor, newSquadTarget );
			OnNumTargetsChanged();
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private void RemoveTarget( int idActor )
	{
		if( !IsTargetOfSquad( idActor ) )
			return;

		_dummyTarget = _targets[idActor];
		if( _dummyTarget != null )
		{
			// reset it, add it to the unused list and remove from used dictionary. 
			_dummyTarget.Reset();
			_targetsUnused.Add( _dummyTarget );
			_targets.Remove( idActor );
		}

		_dummyTarget = null;

		OnNumTargetsChanged();
	}

	//------------------------------------------------------------------------------------------------------------------
	private void RemoveInvalidSquadMembers()
	{
		_deleteList.Clear();

		// go through our members looking for invalid ones
		foreach( KeyValuePair<int, SquadMember> currentMember in _members )
		{
			// if actor cant be found, is currently dead
			if( ( currentMember.Value._memberActor == null ) ||
				( !currentMember.Value._memberActor._pIsAlive ) )
			{
				_deleteList.Add( currentMember.Key );
			}
		}

		// go through the list of those to remove and actually do it. 
		foreach( int toDelete in _deleteList )
		{
			RemoveMember( toDelete );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void RemoveInvalidSquadTargets()
	{
		_deleteList.Clear();

		// go through our targets looking for invalid ones
		foreach( KeyValuePair<int, SquadTarget> currentTarget in _targets )
		{
			// if actor cant be found, is currently dead or has no targeters remove it. 
			if( ( currentTarget.Value._targetActor == null ) ||
				( !currentTarget.Value._targetActor._pIsAlive ) ||
				( currentTarget.Value._numTargeters == 0 ) )
			{
				_deleteList.Add( currentTarget.Key );
			}
		}

		// go through the list of those to remove and actually do it. 
		foreach( int toDelete in _deleteList )
		{
			RemoveTarget( toDelete );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	// return invalid int if not targeting, otherwise the idActor of the target. 
	private int IsMemberTargeting( int idActorMember )
	{
		int returnInt = GlobalDefines.INVALID_ID;

		if( !IsMemberOfSquad( idActorMember ) )
			return returnInt;

		_dummyMember = _members[idActorMember];

		if( _dummyMember != null )
		{
			if( _dummyMember._pHasTarget )
			{
				returnInt = _dummyMember._idActorTarget;
			}
		}

		return returnInt;
	}

	//------------------------------------------------------------------------------------------------------------------
	private bool IsAnyMemberTargetingTarget( int idActorTarget )
	{
		bool returnBool = false;

		if( !IsTargetOfSquad( idActorTarget ) )
			return returnBool;

		_dummyTarget = _targets[idActorTarget];

		if( _dummyTarget != null )
		{
			returnBool = ( _dummyTarget._numTargeters > 0 );
		}

		return false;
	}

	//------------------------------------------------------------------------------------------------------------------
	private void ClearAllTargets()
	{
		foreach( KeyValuePair<int, SquadTarget> currentPair in _targets )
		{
			if( currentPair.Value == null )
				continue;
			currentPair.Value.Reset();
		}
		_targets.Clear();
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	// override for project specific debug rendering. 
	protected virtual void OnDrawGizmos()
	{
		if( !_renderDebugInfo )
			return;


	}

	//--------------------------------------------------------------------------------------------------------------
	protected virtual void OnGUI()
	{
	}
#endif
};

