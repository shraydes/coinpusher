//----------------------------------------------------------------------------------------------------------------------
// BehaveAttackRangedCore.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class BehaveAttackRangedCore : BehaveBase 
{
	//------------------------------------------------------------------------------------------------------------------
	// STATICS / CONSTS / DEFINES
	const float _cFireAngle = 25.0f;

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	Vector3 _dummyDir1;
	Vector3 _dummyDir2;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override int _pBehaviourType{ get { return ( int )eBehaviourCore.ATTACK_RANGED_CORE; } }
	protected override bool _pShouldUpdate
	{ 
		get{ return base._pShouldUpdate && ( _pAllegiance != eAllegiance.NEUTRAL ); } 
	}

	//------------------------------------------------------------------------------------------------------------------
	public BehaveAttackRangedCore()
	: base()
	{
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public override void InitialiseData()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUpdate() 
	{
		if( _pOwnerBrain == null ) return;
		if( _pOwnerBrain._pOwnerActor == null ) return;
		if( _pOwnerBrain._pOwnerActor._pIsPlayer ) return;

		if( _pOwnerBrain._pHasTargetValid )
		{
			// look at our target
			_pOwnerBrain._pOwnerActor._pForward = _pOwnerBrain._pTargetInfo._positionTarget;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnFixedUpdate()
	{
		if( _pOwnerBrain == null ) return;
		if( _pOwnerBrain._pOwnerActor == null ) return;
		if( _pOwnerBrain._pOwnerActor._pIsPlayer ) return;

		if( _pOwnerBrain._pHasTargetValid )
		{
			_dummyDir1 = _pOwnerBrain._pTargetInfo._dirToTarget;
			
			_dummyDir2 = _pOwnerBrain._pOurForward;

			float angle = Vector3.Angle( _dummyDir1, _dummyDir2 );

			// if we are within aim fire at them. 
		    if( angle <= _cFireAngle )
		    {
				bool attemptFire = true;

				if( attemptFire )
				{
					_pOwnerBrain.RequestAttack( 1 );
				}
		    }
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	protected override void OnLateUpdate() 
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnPause()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUnPause()
	{
	}

	#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public override void OnDrawGizmos()
	{
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public override void OnGUI( int width, ref Vector2 textPos )
	{
	}
	#endif
}

