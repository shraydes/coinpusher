using UnityEngine;
using System.Collections.Generic;

public class PathNodeGroup : MonoBehaviour
{
	public List<PathNode> _pathNodes;

	private int _headNodeIndex = 0;

	//------------------------------------------------------------------------------------------------------------------			
	private void Awake()
	{
		if( _pathNodes == null )
		{
			return;
		}

		for( int i = 0; i < _pathNodes.Count; i++ )
		{
			_pathNodes[i].SetPathNodeGroup( this );
			if( _pathNodes[i]._isHeadNode )
			{
				_headNodeIndex = i;
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------			
	public PathNode GetHeadNode()
	{
		return _pathNodes[_headNodeIndex];
	}

	//------------------------------------------------------------------------------------------------------------------				
	public PathNode GetNearestNode( Vector3 pos )
	{
		if( _pathNodes == null || _pathNodes.Count == 0 )
		{
			return null;
		}

		float bestDist = Mathf.Infinity;
		int bestIndex = 0;

		for( int i = 0; i < _pathNodes.Count; i++ )
		{
			Vector3 d = pos - _pathNodes[i]._pWorldPos;
			float mag = d.sqrMagnitude;
			if( mag < bestDist )
			{
				bestDist = mag;
				bestIndex = i;
			}
		}

		return _pathNodes[bestIndex];
	}

	//------------------------------------------------------------------------------------------------------------------	
	public PathNode MakeNode( Vector3 pos, Vector3 forward )
	{
		if( _pathNodes == null )
		{
			_pathNodes = new List<PathNode>();
		}

		PathNode newNode = CreateNodeForProject();
		newNode._position = transform.InverseTransformPoint( pos );
		newNode._forward = forward;
		newNode.SetPathNodeGroup( this );
		newNode._nodeIndex = _pathNodes.Count;

		if( _pathNodes.Count == 0 )
		{
			newNode._isHeadNode = true;
		}

		_pathNodes.Add( newNode );

		return newNode;
	}

	//------------------------------------------------------------------------------------------------------------------	
	public void RemoveNode( int index )
	{
		for( int countNode = 0; countNode < _pathNodes.Count; countNode++ )
		{
			if( countNode == index )
			{
				continue;
			}

			if( _pathNodes[countNode]._nodeLinkData != null )
			{
				for( int countLink = 0; countLink < _pathNodes[countNode]._nodeLinkData.Count; countLink++ )
				{
					// find links to the node that's going to be destroyed and remove them
					if( _pathNodes[countNode]._nodeLinkData[countLink]._linkToNodeId == index )
					{
						_pathNodes[countNode]._nodeLinkData.RemoveAt( countLink );
						countLink--;
					}

					// find links that will need to be retargeted as the list is shortening and indexes are changing
					else if( _pathNodes[countNode]._nodeLinkData[countLink]._linkToNodeId > index )
					{
						_pathNodes[countNode]._nodeLinkData[countLink]._linkToNodeId--;
					}
				}
			}
		}

		_pathNodes.RemoveAt( index );
	}

	//------------------------------------------------------------------------------------------------------------------
	public void SetHeadNode( int node )
	{
		for( int i = 0; i < _pathNodes.Count; i++ )
		{
			_pathNodes[i]._isHeadNode = false;
		}
		_pathNodes[node]._isHeadNode = true;
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool HasHeadNode()
	{
		for( int i = 0; i < _pathNodes.Count; i++ )
		{
			if( _pathNodes[i]._isHeadNode )
			{
				return true;
			}
		}
		return false;
	}

	//------------------------------------------------------------------------------------------------------------------	
	public NodeLink MakeLink( int linkFrom, int linkTo )
	{
		// make sure we dont already have a link to this node. 
		PathNode node = _pathNodes[linkFrom];
		if( node._nodeLinkData != null )
		{
			for( int i = 0; i < node._nodeLinkData.Count; i++ )
			{
				if( node._nodeLinkData[i]._linkToNodeId == linkTo )
				{
					return node._nodeLinkData[i];
				}
			}
		}
		else
		{
			node._nodeLinkData = new List<NodeLink>();
		}

		NodeLink newLink = new NodeLink();
		newLink._linkToNodeId = linkTo;
		newLink._traverseLinkBehaviour = ( int )eBehaviourCore.VCS_TRAVERS_LINK_DEFAULT;

		node._nodeLinkData.Add( newLink );

		return newLink;
	}

	//------------------------------------------------------------------------------------------------------------------	
	protected virtual PathNode CreateNodeForProject()
	{
		return new PathNode();
	}
}
