
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class NodeLink
{
	public NodeLink()
	{
		_linkToNodeId = 0;
		_traverseLinkBehaviour = ( int )eBehaviourCore.VCS_TRAVERS_LINK_DEFAULT;
	}
	public int _linkToNodeId;
	public int _traverseLinkBehaviour;
}

[System.Serializable]
public class PathNode
{
	public bool _isHeadNode = false;
	public bool _foldOut = false;
	public int _atNodeBehaviour = ( int )eBehaviourCore.VCS_AT_NODE_DEFAULT;
	public int _nodeIndex;
	public int[] _nodeLinks;
	public float _stopTimeMin = 0;
	public float _stopTimeMax = 0;
	public Vector3 _position = Vector3.zero;
	public Vector3 _forward = Vector3.zero;
	public List<NodeLink> _nodeLinkData;

	private PathNodeGroup _group;

	//------------------------------------------------------------------------------------------------------------------		
	// PROPERTIES
	public bool _pShouldStopAtNode
	{
		get
		{
			return ( _stopTimeMax != 0.0f );
		}
	}

	public Vector3 _pWorldPos
	{
		get
		{
			if( _group == null )
			{
				return _position;
			}
			return _group.transform.TransformPoint( _position );
		}
	}

	public Vector3 _pWorldForward
	{
		get
		{
			return _forward;
		}
	}

	//------------------------------------------------------------------------------------------------------------------		
	public void SetPathNodeGroup( PathNodeGroup groupRef )
	{
		_group = groupRef;
	}

	//------------------------------------------------------------------------------------------------------------------			
	public int GetLinkCount()
	{
		if( _nodeLinkData == null )
		{
			return 0;
		}

		return _nodeLinkData.Count;
	}

	//------------------------------------------------------------------------------------------------------------------		
	public PathNode GetLinkNode( int i )
	{
		if( i < 0 || i > _nodeLinkData.Count - 1 )
		{
			return null;
		}

		return _group._pathNodes[_nodeLinkData[i]._linkToNodeId];
	}

	//------------------------------------------------------------------------------------------------------------------	
	public PathNode GetNextNode()
	{
		if( _nodeLinkData == null || _nodeLinkData.Count == 0 )
		{
			return null;
		}

		// just pick the first in the list
		return _group._pathNodes[_nodeLinkData[0]._linkToNodeId];
	}

	//------------------------------------------------------------------------------------------------------------------	
	public PathNode GetRandomNextNode()
	{
		return GetRandomNextNode( true, null ); // allow backlinks by default
	}

	//------------------------------------------------------------------------------------------------------------------	
	// if a link if passed in then set it with the link to the node chosen. 
	public PathNode GetRandomNextNode( bool allowBacklinks, PathNode previousNode )
	{
		if( _nodeLinkData == null || _nodeLinkData.Count == 0 )
		{
			return null;
		}

		int randomIndex = Random.Range( 0, _nodeLinkData.Count - 1 );

		// no back linking allowed
		if( !allowBacklinks && previousNode != null )
		{
			// we randomly selected a back link
			if( _group._pathNodes[_nodeLinkData[randomIndex]._linkToNodeId] == previousNode )
			{
				// if this link is the only link then we cant go anywhere else, return null. 
				if( _nodeLinkData.Count == 1 )
				{
					return null;
				}

				// cycle through the links from here until we find a different one. 
				for( int count = 0; count < _nodeLinkData.Count; count++ )
				{
					// wrap the index. 
					randomIndex++;
					if( randomIndex >= _nodeLinkData.Count )
					{
						randomIndex = 0;
					}

					if( ( _nodeLinkData[randomIndex] != null ) &&
						( _group._pathNodes[_nodeLinkData[randomIndex]._linkToNodeId] != null ) )
					{
						return _group._pathNodes[_nodeLinkData[randomIndex]._linkToNodeId];
					}
				}

				// if we get here nothing valid was found
				return null;
			}
		}

		return _group._pathNodes[_nodeLinkData[randomIndex]._linkToNodeId];
	}

	//------------------------------------------------------------------------------------------------------------------	
	public PathNode GetRandomNextNodeWithLink( bool allowBacklinks, PathNode previousNode, out NodeLink link )
	{
		link = null;

		if( _nodeLinkData == null || _nodeLinkData.Count == 0 )
		{
			return null;
		}

		int randomIndex = Random.Range( 0, _nodeLinkData.Count - 1 );

		// no back linking allowed
		if( !allowBacklinks && ( previousNode != null ) )
		{
			// we randomly selected a back link
			if( _group._pathNodes[_nodeLinkData[randomIndex]._linkToNodeId] == previousNode )
			{
				// if this link is the only link then we cant go anywhere else, return null. 
				if( _nodeLinkData.Count == 1 )
				{
					return null;
				}

				// cycle through the links from here until we find a different one. 
				for( int count = 0; count < _nodeLinkData.Count; count++ )
				{
					// wrap the index. 
					randomIndex++;
					if( randomIndex >= _nodeLinkData.Count )
					{
						randomIndex = 0;
					}

					if( ( _nodeLinkData[randomIndex] != null ) &&
						( _group._pathNodes[_nodeLinkData[randomIndex]._linkToNodeId] != null ) )
					{
						link = _nodeLinkData[randomIndex];
						return _group._pathNodes[_nodeLinkData[randomIndex]._linkToNodeId];
					}
				}

				// if we get here nothing valid was found
				return null;
			}
		}

		link = _nodeLinkData[randomIndex];
		
		return _group._pathNodes[_nodeLinkData[randomIndex]._linkToNodeId];
	}

	//------------------------------------------------------------------------------------------------------------------	
	public PathNode GetNodeAndLinkNearestToDirection(
		bool allowBacklinks, PathNode previousNode, Vector3 direction, out NodeLink link )
	{
		link = null;

		if( _nodeLinkData == null || _nodeLinkData.Count == 0 )
		{
			return null;
		}

		int smallestIndex = -1;
		float smallestAngle = Mathf.Infinity;
		Vector3 dirToNode;

		for( int count = 0; count < _nodeLinkData.Count; count++ )
		{
			if( _nodeLinkData[count] == null )
				continue;
			if( _group._pathNodes[_nodeLinkData[count]._linkToNodeId] == null )
				continue;

			// if we do not allow back links and this is where we have just been then continue;
			if( !allowBacklinks && ( previousNode != null ) )
			{
				if( _group._pathNodes[_nodeLinkData[count]._linkToNodeId] == previousNode )
				{
					continue;
				}
			}

			dirToNode = ( _group._pathNodes[_nodeLinkData[count]._linkToNodeId]._pWorldPos - _position ).normalized;

			float angle = Vector3.Angle( direction, dirToNode );
			if( angle < smallestAngle )
			{
				smallestAngle = angle;
				smallestIndex = count;
			}
		}

		if( smallestIndex == -1 )
		{
			return null;
		}

		link = _nodeLinkData[smallestIndex];
		return _group._pathNodes[_nodeLinkData[smallestIndex]._linkToNodeId];
	}

	//------------------------------------------------------------------------------------------------------------------			
	public float GetSqrDist( Vector3 pos )
	{
		return ( _pWorldPos - pos ).sqrMagnitude;
	}
}
