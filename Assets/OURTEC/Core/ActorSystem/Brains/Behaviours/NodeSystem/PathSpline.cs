﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// PathSpline.cs
//
// Created: 09/04/18 chope 
// Contributors: 
// 
// Intention: Allows a spline to be defined used to make custom defined paths for the VCS behaivours. 	
//				Does not make splines to be used in game mode, but splines that can be plotted to allow Waypoints 
//				To be generated for use in game. 
//
// Notes:	Originally created by working through the spline tutorial here:
//			http://catlikecoding.com/unity/tutorials/curves-and-splines/
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
// CLASS PathSpline
[ExecuteInEditMode]
public class PathSpline : MonoBehaviour
{
	//------------------------------------------------------------------------------------------------------------------
	// CONSTS / DEFINES

#if UNITY_EDITOR 
	private const string DEBUG_LOG_PREFIX = "<color=#11FF66><b>Path Spline - </b></color>";
#else
	private const string DEBUG_LOG_PREFIX = "AmuzoLEGOInterface - ";
#endif 

	public enum ePointControlMode
	{
		FREE,
		ALIGNED,
		MIRRORED
	}

	private readonly Vector3[] DEFAULT_START_POINTS = new Vector3[] 
	{
		new Vector3( 1f, 0f, 0f ),
		new Vector3( 2f, 0f, 0f ),
		new Vector3( 3f, 0f, 0f ),
		new Vector3( 4f, 0f, 0f )	
	};

	private const float DIST_CHECK_STEP = 0.01f;
	private const float DIST_RAYCAST = 1000.0f;
	private const float DIST_RAYCAST_OFFSET = 500.0f;
	private const string NODE_GROUP_NAME_PREFIX = "PathNodeGroup_";
	private const string DEFAULT_NODE_GROUP_CLASS_NAME = "PathNodeGroup";

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	public List<Vector3> _points;
	public List<ePointControlMode> _modes;

	[SerializeField]
	private int _traverseNodeBehaveDefault;
	[SerializeField]
	private bool _loop;
	[SerializeField]
	private bool _twoWay;
	[SerializeField]
	private float _nodeSpacing;
	[SerializeField]
	private float _heightOffFloor;
	[SerializeField]
	private string _nodeGroupClassName;
	[SerializeField]
	private LayerMask _groundLayers;
	[SerializeField]
	private PathNodeGroup _nodeGroup;
		
	private int _nodesToPlace;
	private float _length;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public bool _pLoop
	{
		get
		{
			return _loop;
		}
		set
		{
			_loop = value;
			if( value )
			{
				_modes[_modes.Count - 1] = _modes[0];
				SetControlPoint( 0, _points[0] );
			}
		}
	}

	public bool _pTwoWay
	{
		get
		{
			return _twoWay;
		}
		set
		{
			_twoWay = value;
		}
	}

	public int _pControlPointCount
	{
		get
		{
			if( _points == null )
				return 0;

			return _points.Count;
		}
	}

	public int _pCurveCount
	{
		get
		{
			if( _points == null )
				return 0;

			if( _points.Count <= 0 )
				return 0;

			return ( _points.Count - 1 ) / 3;
		}
	}

	public int _pNodeTraverseBehaveDefault
	{
		get
		{
			return _traverseNodeBehaveDefault;
		}
		set
		{
			_traverseNodeBehaveDefault = value;	
		}
	}

	public float _pNodeSpacing
	{
		get
		{
			return _nodeSpacing;
		}
		set
		{
			_nodeSpacing = value;	
		}
	}

	public float _pHeightOffFloor
	{
		get
		{
			return _heightOffFloor;
		}
		set
		{
			_heightOffFloor = value;	
		}
	}

	public string _pNodeGroupClassName
	{
		get
		{
			return _nodeGroupClassName;
		}
		set
		{
			_nodeGroupClassName = value;	
		}
	}

	public PathNodeGroup _pNodeGroup
	{
		get
		{
			return _nodeGroup;
		}
	}

	public LayerMask _pGroundLayers
	{
		get
		{
			return _groundLayers;
		}
		set
		{
			_groundLayers = value;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void Start()
	{
		if( Application.platform != RuntimePlatform.WindowsEditor )
			return;

		if( Application.isPlaying )
			return;
		
		if( _points == null )
		{
			Reset();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public Vector3 GetControlPoint( int index )
	{
		if( _points == null )
			return Vector3.zero;

		if( _points.Count <= index )
			return Vector3.zero;

		return _points[index];
	}

	//------------------------------------------------------------------------------------------------------------------
	public void SetControlPoint( int index, Vector3 point )
	{
		if( ( index % 3 ) == 0 )
		{
			Vector3 delta = point - _points[index];
			if( _loop )
			{
				if( index == 0 )
				{
					_points[1] += delta;
					_points[_points.Count - 2] += delta;
					_points[_points.Count - 1] = point;
				}
				else if( index == _points.Count - 1 )
				{
					_points[0] = point;
					_points[1] += delta;
					_points[index - 1] += delta;
				}
				else
				{
					_points[index - 1] += delta;
					_points[index + 1] += delta;
				}
			}
			else
			{
				if( index > 0 )
				{
					_points[index - 1] += delta;
				}
				if( index + 1 < _points.Count )
				{
					_points[index + 1] += delta;
				}
			}
		}
		_points[index] = point;
		EnforceMode( index );
	}

	//------------------------------------------------------------------------------------------------------------------
	public Vector3 GetPoint( float parametric )
	{
		int index;
		if( parametric >= 1f )
		{
			parametric = 1f;
			index = _points.Count - 4;
		}
		else
		{
			parametric = Mathf.Clamp01( parametric ) * _pCurveCount;
			index = ( int )parametric;
			parametric -= index;
			index *= 3;
		}
		return transform.TransformPoint( PathSplineTools.GetPoint( 
			_points[index], _points[index + 1], _points[index + 2], _points[index + 3], parametric ) );
	}

	//------------------------------------------------------------------------------------------------------------------
	public ePointControlMode GetControlPointMode( int index )
	{
		return _modes[( index + 1 ) / 3];
	}

	//------------------------------------------------------------------------------------------------------------------
	public void SetControlPointMode( int index, ePointControlMode mode )
	{
		int modeIndex = ( index + 1 ) / 3;
		_modes[modeIndex] = mode;
		if( _loop )
		{
			if( modeIndex == 0 )
			{
				_modes[_modes.Count - 1] = mode;
			}
			else if( modeIndex == _modes.Count - 1 )
			{
				_modes[0] = mode;
			}
		}
		EnforceMode( index );
	}

	//------------------------------------------------------------------------------------------------------------------
	public Vector3 GetVelocity( float parametric )
	{
		int i;
		if( parametric >= 1f )
		{
			parametric = 1f;
			i = _points.Count - 4;
		}
		else
		{
			parametric = Mathf.Clamp01( parametric ) * _pCurveCount;
			i = ( int )parametric;
			parametric -= i;
			i *= 3;
		}
		return transform.TransformPoint( PathSplineTools.GetFirstDerivative( 
			_points[i], _points[i + 1], _points[i + 2], _points[i + 3], parametric ) ) - transform.position;
	}

	//------------------------------------------------------------------------------------------------------------------
	public Vector3 GetDirection( float parametric )
	{
		return GetVelocity( parametric ).normalized;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void AddCurve()
	{
		Vector3 pointStart = _points[_points.Count - 1];

		Vector3 pointNew1 = pointStart;
		pointNew1.x += 1f;
		_points.Add( pointNew1 );

		Vector3 pointNew2 = pointNew1;
		pointNew2.x += 1f;
		_points.Add( pointNew2 );

		Vector3 pointNew3 = pointNew2;
		pointNew3.x += 1f;
		_points.Add( pointNew3 );


		ePointControlMode newMode = _modes[_modes.Count - 1];
		_modes.Add( newMode );
		EnforceMode( _points.Count - 4 );

		if( _loop )
		{
			_points[_points.Count - 1] = _points[0];
			_modes[_modes.Count - 1] = _modes[0];
			EnforceMode( 0 );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public void WrapToFloor()
	{
		if( _points == null )
			return;

		Vector3 castPos;
		RaycastHit hitInfo;

		for( int count = 0; count < _points.Count; count++ )
		{
			if( _points[count] == null )
				continue;

			castPos = transform.TransformPoint( _points[count] ) + ( Vector3.up * DIST_RAYCAST_OFFSET );

			if( Physics.Raycast( castPos, Vector3.down, out hitInfo, DIST_RAYCAST, _pGroundLayers ) )
			{
				castPos = hitInfo.point;
				castPos.y += _pHeightOffFloor;
				_points[count] = transform.InverseTransformPoint( castPos );
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public void Reset()
	{
		if( _points == null )
		{
			_points = new List<Vector3>();
		}
		else
		{
			_points.Clear();
		}

		for( int count = 0; count < DEFAULT_START_POINTS.Length; count++ )
		{
			_points.Add( new Vector3(
				DEFAULT_START_POINTS[count].x, DEFAULT_START_POINTS[count].y, DEFAULT_START_POINTS[count].z ) );
		}

		if( _modes == null )
		{
			_modes = new List<ePointControlMode>();
		}
		else
		{
			_modes.Clear();
		}

		_modes.Add( ePointControlMode.FREE );
		_modes.Add( ePointControlMode.FREE );
	}

	//------------------------------------------------------------------------------------------------------------------
	public void PlotNodes()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Request To Plot New Nodes" );

		// Delete any old nodes we may have. 
		DeleteNodes();

		// find out the total length we are dealing with. 
		CalculateTotalLength();
		
		if( _nodesToPlace <= 0 )
			return;

		Debug.Log( DEBUG_LOG_PREFIX + "Nodes will have 2 way links: " + _pTwoWay );
		
		// create the path node group and give it our parent and name
		GameObject newGroupObj = new GameObject( NODE_GROUP_NAME_PREFIX + gameObject.name );

		if( string.IsNullOrEmpty( _nodeGroupClassName ) )
		{
			_nodeGroupClassName = DEFAULT_NODE_GROUP_CLASS_NAME;
		}

		System.Type type = GetType().Assembly.GetType( _nodeGroupClassName, false );
		_nodeGroup = ( PathNodeGroup )newGroupObj.AddComponent( type );

		newGroupObj.transform.position = transform.position;
		newGroupObj.transform.parent = transform.parent;
						
		float parametricStep = 1.0f / ( float )_nodesToPlace;
		int count = 0;
		float parametric = 0.0f;
		PathNode nodeDummy;
		NodeLink linkDummy;

		while( count < _nodesToPlace )
		{
			nodeDummy = _nodeGroup.MakeNode( GetPoint( parametric ), GetDirection( parametric ) );

			if( nodeDummy != null )
			{
				// if this is not the first node places then we should look at links. 
				if( count > 0 )
				{
					// link the previously created node to us. 
					linkDummy = _nodeGroup.MakeLink( count - 1, count );
					if( ( linkDummy != null ) && ( _pNodeTraverseBehaveDefault != ( int )eBehaviourCore.NONE ) )
					{
						linkDummy._traverseLinkBehaviour = _pNodeTraverseBehaveDefault;
					}

					if( _pTwoWay )
					{
						linkDummy = _nodeGroup.MakeLink( count, count - 1 );
						if( ( linkDummy != null ) && ( _pNodeTraverseBehaveDefault != ( int )eBehaviourCore.NONE ) )
						{
							linkDummy._traverseLinkBehaviour = _pNodeTraverseBehaveDefault;
						}
					}

					// this is the last node, link it to the first if this is a looped spline. 
					if( count == ( _nodesToPlace - 1 ) )
					{
						if( _pLoop )
						{
							linkDummy = _nodeGroup.MakeLink( count, 0 );
							if( ( linkDummy != null ) && ( _pNodeTraverseBehaveDefault != ( int )eBehaviourCore.NONE ) )
							{
								linkDummy._traverseLinkBehaviour = _pNodeTraverseBehaveDefault;
							}

							if( _pTwoWay )
							{
								linkDummy = _nodeGroup.MakeLink( 0, count );
								if( ( linkDummy != null ) && 
									( _pNodeTraverseBehaveDefault != ( int )eBehaviourCore.NONE ) )
							{
								linkDummy._traverseLinkBehaviour = _pNodeTraverseBehaveDefault;
							}
							}
						}
					}
				}
			}
			
			count++;
			parametric += parametricStep;
		}

		Debug.Log( DEBUG_LOG_PREFIX + "Node creation complete." );
	}

	//------------------------------------------------------------------------------------------------------------------
	public void DeleteNodes()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Deleting Old Nodes" );

		if( _nodeGroup == null )
			return;

		DestroyImmediate( _nodeGroup.gameObject );
		_nodeGroup = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	// estimates length of spline by stepping along it based on a defined step parametric distance
	public void CalculateTotalLength()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Calculating Current Length of Spline with step size of: " + DIST_CHECK_STEP );

		_length = 0.0f;
		float parametric = DIST_CHECK_STEP;
		Vector3 previousLocationOnSpline = GetPoint( 0.0f );
		Vector3 currentLocationOnSpline = GetPoint( parametric );

		while( parametric <= 1.0f )
		{
			// get the next point along the spline
			currentLocationOnSpline = GetPoint( parametric );

			// find the delta distance
			float deltaDist = ( currentLocationOnSpline - previousLocationOnSpline ).magnitude;

			// add the delta dist to the total length so far
			_length += deltaDist;

			previousLocationOnSpline = currentLocationOnSpline;
			parametric += DIST_CHECK_STEP;
		}

		Debug.Log( DEBUG_LOG_PREFIX + "Spline estimated to be: " + _length.ToString() );

		Debug.Log( DEBUG_LOG_PREFIX + "Node Spacing Set to be: " + _nodeSpacing.ToString() );

		if( _nodeSpacing <= 0.0f )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Node spacing is either 0 or negative, can not create nodes." );
			_nodesToPlace = 0;
		}
		else if( _length < _nodeSpacing )
		{
			Debug.Log( DEBUG_LOG_PREFIX + "Spline is shorter than node spacing, no nodes will be made." );
			_nodesToPlace = 0;
		}
		else
		{
			// find the number of nodes we will be placing and report.
			float nodesPossible = _length / _nodeSpacing;
			_nodesToPlace = ( int )Math.Floor( nodesPossible );

			Debug.Log( DEBUG_LOG_PREFIX + 
				"Spacing and Lenth will allow for " + _nodesToPlace + " nodes. (" + nodesPossible + ")" );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void CalculateTotalNodesToMake()
	{
		Debug.Log( DEBUG_LOG_PREFIX + "Calculating Current Length of Spline with step size of: " + DIST_CHECK_STEP );

		_length = 0.0f;
		float parametric = DIST_CHECK_STEP;
		Vector3 previousLocationOnSpline = GetPoint( 0.0f );
		Vector3 currentLocationOnSpline = GetPoint( parametric );

		while( parametric <= 1.0f )
		{
			// get the next point along the spline
			currentLocationOnSpline = GetPoint( parametric );

			// find the delta distance
			float deltaDist = ( currentLocationOnSpline - previousLocationOnSpline ).magnitude;

			// add the delta dist to the total length so far
			_length += deltaDist;

			previousLocationOnSpline = currentLocationOnSpline;
			parametric += DIST_CHECK_STEP;
		}

		Debug.Log( DEBUG_LOG_PREFIX + "Spline estimated to be: " + _length.ToString() );
	}
	
	//------------------------------------------------------------------------------------------------------------------
	private void EnforceMode( int index )
	{
		int modeIndex = ( index + 1 ) / 3;
		ePointControlMode mode = _modes[modeIndex];
		if( ( mode == ePointControlMode.FREE ) || !_loop && ( modeIndex == 0 || modeIndex == _modes.Count - 1 ) )
		{
			return;
		}

		int middleIndex = modeIndex * 3;
		int fixedIndex, enforcedIndex;
		if( index <= middleIndex )
		{
			fixedIndex = middleIndex - 1;
			if( fixedIndex < 0 )
			{
				fixedIndex = _points.Count - 2;
			}
			enforcedIndex = middleIndex + 1;
			if( enforcedIndex >= _points.Count )
			{
				enforcedIndex = 1;
			}
		}
		else
		{
			fixedIndex = middleIndex + 1;
			if( fixedIndex >= _points.Count )
			{
				fixedIndex = 1;
			}
			enforcedIndex = middleIndex - 1;
			if( enforcedIndex < 0 )
			{
				enforcedIndex = _points.Count - 2;
			}
		}

		Vector3 middle = _points[middleIndex];
		Vector3 enforcedTangent = middle - _points[fixedIndex];
		if( mode == ePointControlMode.ALIGNED )
		{
			enforcedTangent = enforcedTangent.normalized * Vector3.Distance( middle, _points[enforcedIndex] );
		}
		_points[enforcedIndex] = middle + enforcedTangent;
	}

}