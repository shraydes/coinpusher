//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using System.Collections;
using UnityEditor;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
[CustomEditor( typeof( PathNodeGroup ) )]
public class PathNodeGroupEditor : Editor
{
	//------------------------------------------------------------------------------------------------------------------		
	// STATICS / CONSTS / DEFINES
	private const float EDITOR_SCALE = 0.5f;

	//------------------------------------------------------------------------------------------------------------------		
	// FIELDS
	private int _selectedNode = -1;
	private int _interactionNode = -1;
	private Vector2 _editorMousePos;
	private Vector2 _scrollView = Vector2.zero;

	//------------------------------------------------------------------------------------------------------------------		
	public override void OnInspectorGUI()
	{
		PathNodeGroup targ = ( PathNodeGroup )target;

		if( targ == null )
			return;

		if( GUILayout.Button( "Add Node" ) )
		{
			targ.MakeNode( new Vector3( 1, 0, 0 ), Vector3.forward );
		}

		if( targ._pathNodes == null )
		{
			return;
		}

		_scrollView = GUILayout.BeginScrollView( _scrollView );

		for( int count = 0; count < targ._pathNodes.Count; count++ )
		{
			targ._pathNodes[count]._foldOut = 
				EditorGUILayout.Foldout( targ._pathNodes[count]._foldOut,  "[" + count + "]", true );
			
			if( targ._pathNodes[count]._foldOut )
			{	
				GUILayout.BeginHorizontal();

				GUILayout.BeginVertical();
				GUILayout.BeginHorizontal();

				GUILayout.Label( "Head", GUILayout.Width( 40f ) );

				bool headNodeChange = 
					EditorGUILayout.Toggle( targ._pathNodes[count]._isHeadNode, GUILayout.Width( 16f ) );
				if( headNodeChange != targ._pathNodes[count]._isHeadNode )
				{
					targ.SetHeadNode( count );
				}


				EditorGUILayout.LabelField( "Stop Times:", GUILayout.Width( 70f ) );
				targ._pathNodes[count]._stopTimeMin = EditorGUILayout.FloatField( targ._pathNodes[count]._stopTimeMin );
				targ._pathNodes[count]._stopTimeMax = EditorGUILayout.FloatField( targ._pathNodes[count]._stopTimeMax );

				if( GUILayout.Button( "Remove Node" ) )
				{
					targ.RemoveNode( count );
				
					if( targ._pathNodes != null && targ._pathNodes.Count != 0 && !targ.HasHeadNode() )
					{
						targ.SetHeadNode( 0 );
					}
				}

				GUILayout.EndHorizontal();

				GUILayout.BeginHorizontal();

				if( GUILayout.Button( "Go To" ) )
				{
					Vector3 laPos = targ.transform.TransformPoint( targ._pathNodes[count]._position );

					_interactionNode = count;

					if( SceneView.lastActiveSceneView != null )
					{
						SceneView.lastActiveSceneView.LookAt( laPos );
					}
				}

				GUILayout.EndHorizontal();

				GUILayout.EndVertical();

				GUILayout.EndHorizontal();

				GUILayout.BeginHorizontal();
				targ._pathNodes[count]._atNodeBehaviour =
					EditorGUILayout.IntField( "VCS At Node Behaviour", targ._pathNodes[count]._atNodeBehaviour );
				GUILayout.EndHorizontal();

				GUILayout.Space( 10f );

				if( count < targ._pathNodes.Count && targ._pathNodes[count] != null )
				{
					if( targ._pathNodes[count]._nodeLinkData != null )
					{
						for( int countLink = 0; countLink < targ._pathNodes[count]._nodeLinkData.Count; countLink++ )
						{
							GUILayout.BeginHorizontal();
							GUILayout.Label( count + "->" + 
								targ._pathNodes[count]._nodeLinkData[countLink]._linkToNodeId, GUILayout.Width( 70f ) );

							targ._pathNodes[count]._nodeLinkData[countLink]._traverseLinkBehaviour =
								EditorGUILayout.IntField( "VCS Link Behaviour", 
								targ._pathNodes[count]._nodeLinkData[countLink]._traverseLinkBehaviour );

							if( GUILayout.Button( "Remove Link", GUILayout.Width( 100f ) ) )
							{
								targ._pathNodes[count]._nodeLinkData.RemoveAt( countLink );
							}
							GUILayout.EndHorizontal();
						}
					}
				}
			
				// draw project Specific Data
				DrawProjectData( targ );
			}
			DrawLine();
		}

		GUILayout.EndScrollView();

		if( GUI.changed )
		{
			EditorUtility.SetDirty( target );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected void Update()
	{
		if( _selectedNode != -1 )
		{
			Repaint();
		}
	}

	//------------------------------------------------------------------------------------------------------------------		
	protected void OnSceneGUI()
	{
		PathNodeGroup targ = ( PathNodeGroup )target;

		if( targ._pathNodes == null )
			return;
		
		if( Event.current.type == EventType.MouseMove )
		{
			_editorMousePos = Event.current.mousePosition;
		}

		if( Event.current.keyCode == KeyCode.Escape )
		{
			_selectedNode = -1;
		}

		for( int count = 0; count < targ._pathNodes.Count; count++ )
		{
			Vector3 oldPos = targ._pathNodes[count]._position;
			Vector3 oldPosTransformed = targ.transform.TransformPoint( targ._pathNodes[count]._position );

			if( ( _selectedNode != -1 ) || ( _interactionNode == count ) )
			{
				if( _selectedNode == count )
				{
					Handles.color = Color.red;
				}
				else
				{
					Handles.color = Color.white;
				}

				if( Handles.Button( oldPosTransformed, Quaternion.Euler( targ._pathNodes[count]._forward ), 
					EDITOR_SCALE, EDITOR_SCALE, Handles.DotHandleCap ) )
				{
					if( _selectedNode == -1 )
					{
						_selectedNode = count;
					}
					else
					{
						targ.MakeLink( _selectedNode, count );
						_selectedNode = -1;
					}
				}
			}

			if( _interactionNode == count )
			{
				Vector3 posUntransformed = Handles.PositionHandle( 
					oldPosTransformed, Quaternion.LookRotation( targ._pathNodes[count]._forward, Vector3.up ) );
			
				targ._pathNodes[count]._position = targ.transform.InverseTransformPoint( posUntransformed );
				if( oldPos != targ._pathNodes[count]._position )
				{
					EditorUtility.SetDirty( target );
				}
			}
		}

		// draw node links
		for( int countNode = 0; countNode < targ._pathNodes.Count; countNode++ )
		{
			Vector3 oldPosTransformed = targ.transform.TransformPoint( targ._pathNodes[countNode]._position );
			Handles.color = Color.grey;
			// draw node links
			if( targ._pathNodes[countNode]._nodeLinkData != null )
			{
				for( int countLink = 0; countLink < targ._pathNodes[countNode]._nodeLinkData.Count; countLink++ )
				{
					Vector3 linkPos = targ.transform.TransformPoint( 
						targ._pathNodes[targ._pathNodes[countNode]._nodeLinkData[countLink]._linkToNodeId]._position );
					DrawLineArrow( oldPosTransformed, linkPos );
				}
			}

			Handles.Label( oldPosTransformed, countNode.ToString() );
		}

		if( _selectedNode != -1 )
		{
			EditorUtility.SetDirty( target );
			if( SceneView.lastActiveSceneView.camera != null )
			{
				Camera cam = SceneView.lastActiveSceneView.camera;
				Vector3 oldPosTransformed = targ.transform.TransformPoint( targ._pathNodes[_selectedNode]._position );
				Vector3 mousePos3D = cam.ScreenToWorldPoint( 
					new Vector3( _editorMousePos.x, Screen.height - _editorMousePos.y - 25, cam.nearClipPlane ) );

				Handles.DrawLine( oldPosTransformed, mousePos3D );
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------		
	protected virtual void DrawProjectData( PathNodeGroup target )
	{
	}

	//------------------------------------------------------------------------------------------------------------------		
	protected void DrawLine()
	{
		GUILayout.Box( "", new GUILayoutOption[] { GUILayout.ExpandWidth( true ), GUILayout.Height( 1 ) } );
	}

	//------------------------------------------------------------------------------------------------------------------
	private void DrawLineArrow( Vector3 a, Vector3 b )
	{
		Vector3 dir = b - a;
		dir.Normalize();
		
		Handles.DrawLine( a, b );
		Handles.ArrowHandleCap( 0, b - dir * EDITOR_SCALE * 2, 
			Quaternion.LookRotation( b - a ), EDITOR_SCALE * 2, EventType.Repaint );
	}
}
