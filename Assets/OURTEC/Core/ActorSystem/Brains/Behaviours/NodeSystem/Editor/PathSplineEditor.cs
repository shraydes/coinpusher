﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// PathSplineEditor.cs
//
// Created: 09/04/18 chope 
// Contributors: 
// 
// Intention: Interface to the spline plotting tools used by the custom VCS paths. 
//
// Notes:	Originally created by working through the spline tutorial here:
//			http://catlikecoding.com/unity/tutorials/curves-and-splines/
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
// CLASS PathSplineEditor
[CustomEditor( typeof( PathSpline ) )]
public class PathSplineEditor : Editor
{
	//------------------------------------------------------------------------------------------------------------------
	// CONSTS / DEFINES
	private const int STEPS_PER_CURVE = 10;
	private const float DIR_SCALE = 0.5f;
	private const float SIZE_HANDLE = 0.04f;
	private const float SIZE_PICK = 0.06f;

	private readonly Color[] COLOUR_MODES =
	{
		Color.white,
		Color.yellow,
		Color.cyan
	};

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	private PathSpline _spline;
	private Transform _handleTransform;
	private Quaternion _handleRotation;
	private int _selectedIndex = -1;

	//------------------------------------------------------------------------------------------------------------------
	public override void OnInspectorGUI()
	{
		_spline = target as PathSpline;
		EditorGUI.BeginChangeCheck();
		bool loop = EditorGUILayout.Toggle( "Loop", _spline._pLoop );
		if( EditorGUI.EndChangeCheck() )
		{
			Undo.RecordObject( _spline, "Toggle Loop" );
			EditorUtility.SetDirty( _spline );
			_spline._pLoop = loop;
		}

		EditorGUI.BeginChangeCheck();
		_spline._pTwoWay = EditorGUILayout.Toggle( "Two Way Traversal: ", _spline._pTwoWay );
		if( EditorGUI.EndChangeCheck() )
		{
			EditorUtility.SetDirty( _spline );
		}

		if( _selectedIndex >= 0 && _selectedIndex < _spline._pControlPointCount )
		{
			DrawSelectedPointInspector();
		}

		if( GUILayout.Button( "Add Curve" ) )
		{
			Undo.RecordObject( _spline, "Add Curve" );
			_spline.AddCurve();
			EditorUtility.SetDirty( _spline );
		}

		EditorGUI.BeginChangeCheck();
		_spline._pHeightOffFloor = EditorGUILayout.FloatField( "Height Off Floor: ", _spline._pHeightOffFloor );
		if( EditorGUI.EndChangeCheck() )
		{
			EditorUtility.SetDirty( _spline );
		}

		EditorGUI.BeginChangeCheck();
		LayerMask tempMask = EditorGUILayout.MaskField( "Ground Layers",
			 InternalEditorUtility.LayerMaskToConcatenatedLayersMask( _spline._pGroundLayers ), InternalEditorUtility.layers );
		if( EditorGUI.EndChangeCheck() )
		{
			_spline._pGroundLayers = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask( tempMask );
			EditorUtility.SetDirty( _spline );
		}

		if( GUILayout.Button( "Wrap To Floor" ) )
		{
			Undo.RecordObject( _spline, "Wrap To Floor" );
			_spline.WrapToFloor();
			EditorUtility.SetDirty( _spline );
		}

		if( GUILayout.Button( "Reset" ) )
		{
			if( EditorUtility.DisplayDialog( "Confirm Node Rest", "Are you sure?", "Yes", "No" ) )
			{
				Undo.RecordObject( _spline, "Reset" );
				_spline.Reset();
				EditorUtility.SetDirty( _spline );
			}
		}

		EditorGUILayout.Space();
		DrawLine();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.LabelField( "Path Node Creation Tool:" );

		EditorGUI.BeginChangeCheck();
		_spline._pNodeGroupClassName = EditorGUILayout.TextField( "Node Group Class: ", _spline._pNodeGroupClassName );
		_spline._pNodeSpacing = EditorGUILayout.FloatField( "Node Spacing: ", _spline._pNodeSpacing );
		_spline._pNodeTraverseBehaveDefault = 
			EditorGUILayout.IntField( "Default Traverse Behaveiour: ", _spline._pNodeTraverseBehaveDefault );
		if( EditorGUI.EndChangeCheck() )
		{
			EditorUtility.SetDirty( _spline );
		}

		if( GUILayout.Button( "Report Spline Length" ) )
		{
			_spline.CalculateTotalLength();
		}

		if( GUILayout.Button( "Plot Nodes Along Spline" ) )
		{
			_spline.PlotNodes();
			EditorUtility.SetDirty( _spline );
			EditorUtility.SetDirty( _spline._pNodeGroup );
		}

		if( GUILayout.Button( "Delete Nodes" ) )
		{
			_spline.DeleteNodes();
			EditorUtility.SetDirty( _spline );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void DrawSelectedPointInspector()
	{
		GUILayout.Label( "Selected Point" );
		EditorGUI.BeginChangeCheck();
		Vector3 point = EditorGUILayout.Vector3Field( "Position", _spline.GetControlPoint( _selectedIndex ) );
		if( EditorGUI.EndChangeCheck() )
		{
			Undo.RecordObject( _spline, "Move Point" );
			EditorUtility.SetDirty( _spline );
			_spline.SetControlPoint( _selectedIndex, point );
		}

		EditorGUI.BeginChangeCheck();
		PathSpline.ePointControlMode mode = ( PathSpline.ePointControlMode )EditorGUILayout.EnumPopup(
			"Mode", _spline.GetControlPointMode( _selectedIndex ) );

		if( EditorGUI.EndChangeCheck() )
		{
			Undo.RecordObject( _spline, "Change Point Mode" );
			_spline.SetControlPointMode( _selectedIndex, mode );
			EditorUtility.SetDirty( _spline );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void OnSceneGUI()
	{
		_spline = target as PathSpline;
		_handleTransform = _spline.transform;
		_handleRotation = Tools.pivotRotation == PivotRotation.Local ?
			_handleTransform.rotation : Quaternion.identity;

		Vector3 p0 = ShowPoint( 0 );
		for( int i = 1; i < _spline._pControlPointCount; i += 3 )
		{
			Vector3 p1 = ShowPoint( i );
			Vector3 p2 = ShowPoint( i + 1 );
			Vector3 p3 = ShowPoint( i + 2 );

			Handles.color = Color.gray;
			Handles.DrawLine( p0, p1 );
			Handles.DrawLine( p2, p3 );

			Handles.DrawBezier( p0, p3, p1, p2, Color.white, null, 2f );
			p0 = p3;
		}
		ShowDirections();
	}

	//------------------------------------------------------------------------------------------------------------------
	private void ShowDirections()
	{
		Handles.color = Color.green;
		Vector3 point = _spline.GetPoint( 0f );
		Handles.DrawLine( point, point + _spline.GetDirection( 0f ) * DIR_SCALE );
		int steps = STEPS_PER_CURVE * _spline._pCurveCount;
		for( int i = 1; i <= steps; i++ )
		{
			point = _spline.GetPoint( i / ( float )steps );
			Handles.DrawLine( point, point + _spline.GetDirection( i / ( float )steps ) * DIR_SCALE );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private Vector3 ShowPoint( int index )
	{
		Vector3 point = _handleTransform.TransformPoint( _spline.GetControlPoint( index ) );
		float size = HandleUtility.GetHandleSize( point );
		if( index == 0 )
		{
			size *= 2f;
		}
		Handles.color = COLOUR_MODES[( int )_spline.GetControlPointMode( index )];
		if( Handles.Button( point, _handleRotation, size * SIZE_HANDLE, size * SIZE_PICK, Handles.DotCap ) )
		{
			_selectedIndex = index;
			Repaint();
		}
		if( _selectedIndex == index )
		{
			EditorGUI.BeginChangeCheck();
			point = Handles.DoPositionHandle( point, _handleRotation );
			if( EditorGUI.EndChangeCheck() )
			{
				Undo.RecordObject( _spline, "Move Point" );
				EditorUtility.SetDirty( _spline );
				_spline.SetControlPoint( index, _handleTransform.InverseTransformPoint( point ) );
			}
		}
		return point;
	}

	//------------------------------------------------------------------------------------------------------------------		
	private void DrawLine()
	{
		GUILayout.Box( "", new GUILayoutOption[] { GUILayout.ExpandWidth( true ), GUILayout.Height( 1 ) } );
	}
}