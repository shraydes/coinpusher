//----------------------------------------------------------------------------------------------------------------------
// BehaveMoveToPosition.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class BehaveMoveToPosition : BehaveBase 
{
	//------------------------------------------------------------------------------------------------------------------
	//	CONSTS / STATICS / DEFINES		
	enum STATE
	{
		MTP_UNSET,

		MTP_MOVING,
		MTP_FINISHED,

		NUM_MTP_STATES
	}

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	STATE _stateCurrent;
	float _timerBehaviour;
	float _timerState;

	bool _finishOnDamage = false;
	bool _finishOnThreat = false;
	bool _hasMoved = false;
	float _healthOnStart;
	Vector3 _moveToPos = Vector3.zero;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override int _pBehaviourType{ get { return ( int )eBehaviourCore.MOVE_TO_POSIITON; } }
	public bool _pFiniahOnDamage{ set{ _finishOnDamage = value; } }
	public bool _pFiniahOnThreat{ set{ _finishOnThreat = value; } }
	public Vector3 _pMoveToPos{ set{ _moveToPos = value; } }

	//------------------------------------------------------------------------------------------------------------------
	public BehaveMoveToPosition()
	: base()
	{
		_stateCurrent = STATE.MTP_UNSET;
		_timerBehaviour = 0.0f;
		_timerState = 0.0f;
	}	
	
	//------------------------------------------------------------------------------------------------------------------
	public override void InitialiseData()
	{
		if( _pOwnerBrain == null ) return;
		if( _pOwnerBrain._pOwnerActor == null ) return;

		_healthOnStart = _pOwnerBrain._pOwnerActor._pHealth;
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUpdate() 
	{
		// with no owner we should not be updating. 
		if( ( _pOwnerBrain == null ) && ( _stateCurrent != STATE.MTP_UNSET ) )
		{
			SwitchToState( STATE.MTP_UNSET );
			return;
		}

		_timerBehaviour += Time.deltaTime;
		_timerState += Time.deltaTime;

		// check our early out conditions. 
		if( _stateCurrent != STATE.MTP_FINISHED )
		{
			if( _finishOnDamage && ( _pOwnerBrain._pOwnerActor._pHealth != _healthOnStart ) )
			{
				SwitchToState( STATE.MTP_FINISHED );
				return;
			}

			if( _finishOnThreat && _pOwnerBrain._pHasTargetValid )
			{
				SwitchToState( STATE.MTP_FINISHED );
				return;
			}
		}

		switch( _stateCurrent )
		{
			case STATE.MTP_MOVING:
			{
				UpdateMoving();
				break;
			}
			case STATE.MTP_FINISHED:
			{
				break;
			}
			case STATE.MTP_UNSET:
			default:
			{
				UpdateUnset();
				break;
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnFixedUpdate()
	{
	}
	
	//------------------------------------------------------------------------------------------------------------------
	protected override void OnLateUpdate() 
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
		if( _stateCurrent != STATE.MTP_UNSET )
		{
			SwitchToState( STATE.MTP_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnPause()
	{
		if( _stateCurrent != STATE.MTP_UNSET )
		{
			SwitchToState( STATE.MTP_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUnPause()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	void SwitchToState( STATE stateNew )
	{
		// do anything needed to finish or stop current state. 
		switch( _stateCurrent )
		{
			case STATE.MTP_FINISHED:
			case STATE.MTP_MOVING:
			case STATE.MTP_UNSET:
			default:
			{
				break;
			}
		}

		_timerState = 0.0f;
		_stateCurrent = stateNew;

		switch( _stateCurrent )
		{
			case STATE.MTP_MOVING:
			{
				_hasMoved = false;
				CreateMovementTowards();
				break;
			}
			case STATE.MTP_FINISHED:
			{
				_moveToPos = Vector3.zero;
				Finish();
				break;
			}
			case STATE.MTP_UNSET:
			default:
			{
				if( _pOwnerBrain != null )
				{
					_pOwnerBrain.StopAllMovement();
				}
				break;
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateUnset()
	{
		if( _pOwnerBrain == null ) return;
		if( _pOwnerBrain._pOwnerActor == null ) return;
		
		// if a position has not been set then we cant go anywhere. 
		if( _moveToPos == Vector3.zero ) return;
		
		SwitchToState( STATE.MTP_MOVING );
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateMoving()
	{
		// if we have finished the moment and get here we are still not in range, plan again. 
		if( _pOwnerBrain._pIsAtCurrentDestination )
		{
			SwitchToState( STATE.MTP_FINISHED );
			return;
		}

		// keep us up to date on whether or not we have started to move. 
		if( ! _hasMoved )
		{
			_hasMoved = _pOwnerBrain._pOwnerActor._pIsMoving;
		}
		else if( ! _pOwnerBrain._pOwnerActor._pIsMoving )
		{
			// if we have started moving and are no longer then we might be stuck, re-path plan
			SwitchToState( STATE.MTP_MOVING );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void CreateMovementTowards()
	{
		if( _pOwnerBrain == null ) return;
		
		_pOwnerBrain.IssueMoveOrder( _moveToPos );
	}

	#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public override void OnDrawGizmos()
	{
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public override void OnGUI( int width, ref Vector2 textPos )
	{
	}
	#endif
}

