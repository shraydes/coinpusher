//----------------------------------------------------------------------------------------------------------------------
// VCSBehaveBase.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public abstract class VCSBehaveBase : BehaveBase
{
	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	protected BehaveBase _dummyBehaviour;

	VcsInfo _vcsInfo;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override bool _pIsVcsBehaviour
	{
		get
		{
			return true;
		}
	}

	public VcsInfo _pVcsInfo
	{
		set
		{
			_vcsInfo = value;
		}
		protected get
		{
			return _vcsInfo;
		}
	}

	protected PathNode _pPathNode
	{
		get
		{
			if( _pVcsInfo == null )
				return null;
			return _pVcsInfo._pathNodeCurrent;
		}
	}

	protected NodeLink _pPathNodeLink
	{
		get
		{
			if( _pVcsInfo == null )
				return null;
			return _pVcsInfo._nodeLinkCurrent;
		}
	}

	protected override bool _pShouldUpdate
	{
		get
		{
			return base._pShouldUpdate && ( _pPathNode != null );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public VCSBehaveBase()
	: base()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
		_pVcsInfo = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void Finish()
	{
		// finishing any Vcs behaviour means we should finish the Vogon constructor script altogether.
		if( _pOwnerBrain != null )
		{
			_pOwnerBrain.VogonConstructorScriptExit();
		}

		base.Finish();
	}
}



