//----------------------------------------------------------------------------------------------------------------------
// BehaveMoveToTarget.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using AmuzoActor;
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class BehaveMoveToTarget : BehaveBase
{
	//------------------------------------------------------------------------------------------------------------------
	//	CONSTS / DEFINES	
	private const float TIME_BEFORE_PATH_REPLAN = 1.5f;
	private const float RANGE_DEFAULT = 0.5f;
	private const float DISPLACEMENT_TARGET_BEFORE_PATH_REPLAN = 1.0f;

	private enum eState
	{
		MTT_UNSET,

		MTT_STANDING,
		MTT_MOVING_TOWARDS,

		NUM_MTT_STATES
	}

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	private eState _stateCurrent;
	private float _timerBehaviour;
	private float _timerState;
	private float _rangeToAimFor = RANGE_DEFAULT;
	private float _replanDist = DISPLACEMENT_TARGET_BEFORE_PATH_REPLAN;
	private Vector3 _posAtPathPlanTarget = Vector3.zero;
	private ActorLogicBase _targetActor = null;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override int _pBehaviourType
	{
		get
		{
			return ( int )eBehaviourCore.MOVE_TO_TARGET;
		}
	}
	protected override bool _pShouldUpdate
	{
		get
		{
			return base._pShouldUpdate && ( _pAllegiance != eAllegiance.NEUTRAL );
		}
	}

	private bool _pHasTarget
	{
		get
		{
			return ( _targetActor != null );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public BehaveMoveToTarget()
	: base()
	{
		_stateCurrent = eState.MTT_UNSET;
		_timerBehaviour = 0.0f;
		_timerState = 0.0f;
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void InitialiseData()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUpdate()
	{
		// with no owner we should not be updating. 
		if( ( _pOwnerBrain == null ) && ( _stateCurrent != eState.MTT_UNSET ) )
		{
			SwitchToState( eState.MTT_UNSET );
			return;
		}

		// update our target actor if we have one. 
		_targetActor = null;
		if( _pOwnerBrain._pHasTarget )
		{
			_targetActor = _pOwnerBrain._pTargetInfo._actorTarget;
		}

		_timerBehaviour += Time.deltaTime;
		_timerState += Time.deltaTime;

		switch( _stateCurrent )
		{
			case eState.MTT_STANDING:
				{
					UpdateStanding();
					break;
				}
			case eState.MTT_MOVING_TOWARDS:
				{
					UpdateMovingTowards();
					break;
				}
			case eState.MTT_UNSET:
			default:
				{
					UpdateUnset();
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnFixedUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnLateUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
		if( _stateCurrent != eState.MTT_UNSET )
		{
			SwitchToState( eState.MTT_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnPause()
	{
		if( _stateCurrent != eState.MTT_UNSET )
		{
			SwitchToState( eState.MTT_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUnPause()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	void SwitchToState( eState stateNew )
	{
		// do anything needed to finish or stop current state. 
		switch( _stateCurrent )
		{
			case eState.MTT_STANDING:
			case eState.MTT_MOVING_TOWARDS:
			case eState.MTT_UNSET:
			default:
				{
					break;
				}
		}

		_timerState = 0.0f;
		_stateCurrent = stateNew;

		switch( _stateCurrent )
		{
			case eState.MTT_MOVING_TOWARDS:
				{
					CreateMovementTowards();
					break;
				}
			case eState.MTT_STANDING:
			case eState.MTT_UNSET:
			default:
				{
					if( _pOwnerBrain != null )
					{
						_pOwnerBrain.StopAllMovement();
					}
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateUnset()
	{
		if( _pOwnerBrain == null )
			return;
		if( _pOwnerBrain._pOwnerActor == null )
			return;

		SwitchToState( eState.MTT_STANDING );
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateStanding()
	{
		if( !_pHasTarget )
			return;

		if( _pOwnerBrain._pTargetInfo._distToTarget > _rangeToAimFor )
		{
			SwitchToState( eState.MTT_MOVING_TOWARDS );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateMovingTowards()
	{
		// if we no longer have a threat then stand.
		if( !_pHasTarget )
		{
			SwitchToState( eState.MTT_STANDING );
			return;
		}

		// if we are within range then stand. 
		if( _pOwnerBrain._pTargetInfo._distToTarget < ( _rangeToAimFor * GlobalDefines.TWO_THIRDS ) )
		{
			SwitchToState( eState.MTT_STANDING );
			return;
		}

		// if we have finished the moment and get here we are still not in range, plan again. 
		if( !_pOwnerBrain._pIsMoving && ( _timerState >= TIME_BEFORE_PATH_REPLAN ) )
		{
			SwitchToState( eState.MTT_MOVING_TOWARDS );
			return;
		}

		// if the target has moved enough replan. 
		if( ( _targetActor._pPosition - _posAtPathPlanTarget ).magnitude >= _replanDist )
		{
			SwitchToState( eState.MTT_MOVING_TOWARDS );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void CreateMovementTowards()
	{
		if( _pOwnerBrain == null )
			return;
		if( !_pHasTarget )
			return;

		_pOwnerBrain.IssueMoveOrder( _targetActor._pPosition );

		_posAtPathPlanTarget = _targetActor._pPosition;
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public override void OnDrawGizmos()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void OnGUI( int width, ref Vector2 textPos )
	{
	}
#endif
}

