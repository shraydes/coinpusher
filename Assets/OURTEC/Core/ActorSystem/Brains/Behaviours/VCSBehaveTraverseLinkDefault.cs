//----------------------------------------------------------------------------------------------------------------------
// VCSBehaveTraverseLinkDefault.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class VCSBehaveTraverseLinkDefault : VCSBehaveBase
{
	//------------------------------------------------------------------------------------------------------------------
	//	CONSTS / STATICS / DEFINES		
	enum STATE
	{
		VCS_TLD_UNSET,

		VCS_TLD_MOVING,
		VCS_TLD_ARRIVEATNODE,
		VCS_TLD_FINISHED,

		NUM_VCS_TLD_STATES
	}

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	STATE _stateCurrent;
	float _timerBehaviour;
	float _timerState;

	bool _hasMoved = false;
	Vector3 _moveToPos;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override int _pBehaviourType
	{
		get
		{
			return ( int )eBehaviourCore.VCS_TRAVERS_LINK_DEFAULT;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public VCSBehaveTraverseLinkDefault()
	: base()
	{
		_stateCurrent = STATE.VCS_TLD_UNSET;
		_timerBehaviour = 0.0f;
		_timerState = 0.0f;
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void InitialiseData()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUpdate()
	{
		// with no owner we should not be updating. 
		if( ( _pOwnerBrain == null ) && ( _stateCurrent != STATE.VCS_TLD_UNSET ) )
		{
			SwitchToState( STATE.VCS_TLD_UNSET );
			return;
		}

		_timerBehaviour += Time.deltaTime;
		_timerState += Time.deltaTime;

		switch( _stateCurrent )
		{
			case STATE.VCS_TLD_MOVING:
				{
					UpdateMoving();
					break;
				}
			case STATE.VCS_TLD_ARRIVEATNODE:
				{
					break;
				}
			case STATE.VCS_TLD_FINISHED:
				{
					break;
				}
			case STATE.VCS_TLD_UNSET:
			default:
				{
					UpdateUnset();
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnFixedUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnLateUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
		base.OnShutdown();

		if( _stateCurrent != STATE.VCS_TLD_UNSET )
		{
			SwitchToState( STATE.VCS_TLD_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnPause()
	{
		if( _stateCurrent != STATE.VCS_TLD_UNSET )
		{
			SwitchToState( STATE.VCS_TLD_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUnPause()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	void SwitchToState( STATE stateNew )
	{
		// do anything needed to finish or stop current state. 
		switch( _stateCurrent )
		{
			case STATE.VCS_TLD_FINISHED:
			case STATE.VCS_TLD_ARRIVEATNODE:
			case STATE.VCS_TLD_MOVING:
			case STATE.VCS_TLD_UNSET:
			default:
				{
					break;
				}
		}

		_timerState = 0.0f;
		_stateCurrent = stateNew;

		switch( _stateCurrent )
		{
			case STATE.VCS_TLD_MOVING:
				{
					_hasMoved = false;
					CreateMovementTowards();
					break;
				}
			case STATE.VCS_TLD_ARRIVEATNODE:
				{
					FinishLinkTraverse();
					break;
				}
			case STATE.VCS_TLD_FINISHED:
				{
					Finish();
					break;
				}
			case STATE.VCS_TLD_UNSET:
			default:
				{
					_moveToPos = Vector3.zero;
					if( _pOwnerBrain != null )
					{
						_pOwnerBrain.StopAllMovement();
					}
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateUnset()
	{
		if( _pOwnerBrain == null )
			return;
		if( _pOwnerBrain._pOwnerActor == null )
			return;

		if( _pPathNode != null )
		{
			_moveToPos = _pPathNode._pWorldPos;
		}

		// if a position has not been set then we cant go anywhere. 
		if( _moveToPos == Vector3.zero )
			return;

		SwitchToState( STATE.VCS_TLD_MOVING );
		return;
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateMoving()
	{
		// if we have finished the moment and get here we are still not in range, plan again. 
		if( _pOwnerBrain._pIsAtCurrentDestination )
		{
			SwitchToState( STATE.VCS_TLD_ARRIVEATNODE );
			return;
		}

		// keep us up to date on whether or not we have started to move. 
		if( !_hasMoved )
		{
			_hasMoved = _pOwnerBrain._pOwnerActor._pIsMoving;
		}
		else if( !_pOwnerBrain._pOwnerActor._pIsMoving )
		{
			// if we have started moving and are no longer then we might be stuck, re-path plan
			SwitchToState( STATE.VCS_TLD_MOVING );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void CreateMovementTowards()
	{
		if( _pOwnerBrain == null )
			return;
		_pOwnerBrain.IssueMoveOrder( _moveToPos );
	}

	//------------------------------------------------------------------------------------------------------------------
	void FinishLinkTraverse()
	{
		// no node set... bail. 
		if( ( _pPathNode == null ) || ( _pVcsInfo == null ) )
		{
			SwitchToState( STATE.VCS_TLD_FINISHED );
			return;
		}

		// clear the current Link set in the info, we are done with it. 
		_pVcsInfo._nodeLinkCurrent = null;

		// if we should not stop here then just carry onto the next node!
		if( !_pPathNode._pShouldStopAtNode )
		{
			SkipToNextNode();
			return;
		}

		// push the at node behaviour, replacing us while we are at it. 
		if( !_pOwnerBrain.VogonConstructorScriptPushBehaviour( _pVcsInfo._pathNodeCurrent._atNodeBehaviour, true ) )
		{
			// behaviour failed. exit to be save. 
			SwitchToState( STATE.VCS_TLD_FINISHED );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void SkipToNextNode()
	{
		// no node set or no links from this node means we dont know where to go, finish Vcs. 
		if( ( _pPathNode == null ) || ( _pPathNode._nodeLinkData == null ) || ( _pPathNode._nodeLinkData.Count == 0 ) )
		{
			SwitchToState( STATE.VCS_TLD_FINISHED );
			return;
		}

		// find the next node to go to and link to take, not allowing back links. 
		_pVcsInfo._pathNodeDummy = _pVcsInfo._pathNodeCurrent.GetRandomNextNodeWithLink(
			false, _pVcsInfo._pathNodePrevious, out _pVcsInfo._nodeLinkCurrent );

		// if we did not find one then we are finished. 
		if( ( _pVcsInfo._pathNodeDummy == null ) || ( _pVcsInfo._nodeLinkCurrent == null ) )
		{
			SwitchToState( STATE.VCS_TLD_FINISHED );
			return;
		}

		// update the previous and current nodes. 
		_pVcsInfo._pathNodePrevious = _pVcsInfo._pathNodeCurrent;
		_pVcsInfo._pathNodeCurrent = _pVcsInfo._pathNodeDummy;
		_pVcsInfo._pathNodeDummy = null;

		// no need for new behaviour, just start us over. 
		SwitchToState( STATE.VCS_TLD_UNSET );
		return;
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public override void OnDrawGizmos()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void OnGUI( int width, ref Vector2 textPos )
	{
		GUI.Box( new Rect( ( int )textPos.x, ( int )textPos.y, width, GlobalDefines.DEBUG_TEXT_HIEGHT ),
			eBehaviourCore.VCS_TRAVERS_LINK_DEFAULT.ToString() );
		textPos.y += GlobalDefines.DEBUG_TEXT_HIEGHT;
		
		GUI.Box( new Rect( ( int )textPos.x, ( int )textPos.y, width, GlobalDefines.DEBUG_TEXT_HIEGHT ),
			_stateCurrent.ToString() );
		textPos.y += GlobalDefines.DEBUG_TEXT_HIEGHT;
	}
#endif
}

