//----------------------------------------------------------------------------------------------------------------------
// BehavePool.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//----------------------------------------------------------------------------------------------------------------------
// class to make sure behaves are never destroyed during gameplay, which allows us to re-use them where possible.  
public class BehavePool
{
	//------------------------------------------------------------------------------------------------------------------
	// SINGLTON
	static BehavePool _instance;
	static public BehavePool _pInstance
	{
		get
		{
			if( _instance == null )
			{
				_instance = new BehavePool();
			}
			return _instance;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	#region CLASS - BehaveData
	internal class BehaveData
	{
		bool _inUse = false;
		internal BehaveBase _behaveInstance = null;

		~BehaveData()
		{
			ResetAll();
		}

		internal void ResetAll()
		{
			_inUse = false;
			_behaveInstance = null;
		}

		internal void StartUse( ref BrainActorBase ownerBrain )
		{
			_inUse = true;

			if( _behaveInstance != null )
			{
				_behaveInstance._pOwnerBrain = ownerBrain;
			}
		}

		internal void StopUse()
		{
			_inUse = false;

			if( _behaveInstance != null )
			{
				_behaveInstance.Shutdown();
			}
		}

		internal bool IsInUse()
		{
			return ( _inUse );
		}
	}
	#endregion // BehaveData - CLASS 

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	private BehaveData _tempData = null;
	private List<BehaveData> _tempList = null;
	private Dictionary<int, List<BehaveData>> _behaves = new Dictionary<int, List<BehaveData>>();

	//------------------------------------------------------------------------------------------------------------------
	private BehavePool()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	~BehavePool()
	{
		ClearBehavePool();
		_behaves = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void GetBehaviour( int behaviour, ref BehaveBase behave, BrainActorBase ownerBrain )
	{
		if( behave != null )
		{
			behave = null;
		}

		if( behaviour == ( int )eBehaviourCore.USE_DEFAULT )
			return;

		// if this is the first attempt at getting one of theses behaviours then make the new list for them. 
		if( !_behaves.ContainsKey( behaviour ) )
		{
			_behaves.Add( behaviour, new List<BehaveData>() );
		}

		// if we already have a free behave of this type use it and return
		if( !FindExistingBehaviour( behaviour ) )
		{
			_tempData = new BehaveData();
			ownerBrain.CreateBehaviour( behaviour, ref _tempData._behaveInstance );

			if( _tempData._behaveInstance == null )
			{
				// no behave script then it unsafe to use, bark and destroy everything we have just created. 
				Debug.LogWarning( "BehavePool::GetBehaviour - SETUP ISSUE behave failed to be created" );
				_tempData = null;
			}
			else
			{
				// add the new behave and its data to the pool dictionary. 
				_behaves[behaviour].Add( _tempData );
			}
		}

		// if we new have a behave, prep it for use and return the behaviour object. 
		if( _tempData != null )
		{
			behave = _tempData._behaveInstance;
			_tempData.StartUse( ref ownerBrain );
		}

		_tempData = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void FreeBehaviour( int behaviour, int idBehave )
	{
		if( !_behaves.ContainsKey( behaviour ) )
			return;

		_tempList = _behaves[behaviour];
		foreach( BehaveData currentData in _tempList )
		{
			// if its not in use then its not what we are looking for. 
			if( currentData == null )
				continue;
			if( !currentData.IsInUse() )
				continue;

			// if this behave id is correct this is a behave to free. 
			if( ( currentData._behaveInstance != null ) && ( currentData._behaveInstance._pIdBehave == idBehave ) )
			{
				currentData.StopUse();
			}
		}

		_tempList = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void ClearBehavePool()
	{
		foreach( KeyValuePair<int, List<BehaveData>> currentPair in _behaves )
		{
			if( currentPair.Value == null )
				continue;

			foreach( BehaveData currentData in currentPair.Value )
			{
				if( currentData == null )
					continue;

				currentData.ResetAll();
			}

			currentPair.Value.Clear();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private bool FindExistingBehaviour( int behaviour )
	{
		if( !_behaves.ContainsKey( behaviour ) )
			return false;

		_tempList = _behaves[behaviour];
		foreach( BehaveData currentData in _tempList )
		{
			// if this behave is already in use or cant be found then continue;
			if( currentData == null )
				continue;
			if( currentData.IsInUse() )
				continue;

			// this is an unused behave of the correct type. Use it. 
			if( currentData._behaveInstance != null )
			{
				_tempData = currentData;
				return true;
			}
		}

		// free up the temp list. 
		_tempList = null;
		return false;
	}
}


