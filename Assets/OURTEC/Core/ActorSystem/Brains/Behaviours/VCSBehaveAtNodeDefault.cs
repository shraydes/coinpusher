//----------------------------------------------------------------------------------------------------------------------
// VCSBehaveAtNodeDefault.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class VCSBehaveAtNodeDefault : VCSBehaveBase
{
	//------------------------------------------------------------------------------------------------------------------
	//	CONSTS / STATICS / DEFINES		
	enum STATE
	{
		VCS_AND_UNSET,

		VCS_AND_STANDING,
		VCS_AND_LEAVENODE,
		VCS_AND_FINISHED,

		NUM_VCS_AND_STATES
	}

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	STATE _stateCurrent;
	float _timerBehaviour;
	float _timerState;

	float _timeAtNode;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override int _pBehaviourType
	{
		get
		{
			return ( int )eBehaviourCore.VCS_AT_NODE_DEFAULT;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public VCSBehaveAtNodeDefault()
	: base()
	{
		_stateCurrent = STATE.VCS_AND_UNSET;
		_timerBehaviour = 0.0f;
		_timerState = 0.0f;
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void InitialiseData()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUpdate()
	{
		// with no owner we should not be updating. 
		if( ( _pOwnerBrain == null ) && ( _stateCurrent != STATE.VCS_AND_UNSET ) )
		{
			SwitchToState( STATE.VCS_AND_UNSET );
			return;
		}

		_timerBehaviour += Time.deltaTime;
		_timerState += Time.deltaTime;

		switch( _stateCurrent )
		{
			case STATE.VCS_AND_STANDING:
				{
					UpdateStanding();
					break;
				}
			case STATE.VCS_AND_LEAVENODE:
				{
					break;
				}
			case STATE.VCS_AND_FINISHED:
				{
					break;
				}
			case STATE.VCS_AND_UNSET:
			default:
				{
					UpdateUnset();
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnFixedUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnLateUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
		base.OnShutdown();

		if( _stateCurrent != STATE.VCS_AND_UNSET )
		{
			SwitchToState( STATE.VCS_AND_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnPause()
	{
		if( _stateCurrent != STATE.VCS_AND_UNSET )
		{
			SwitchToState( STATE.VCS_AND_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUnPause()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	void SwitchToState( STATE stateNew )
	{
		// do anything needed to finish or stop current state. 
		switch( _stateCurrent )
		{
			case STATE.VCS_AND_FINISHED:
			case STATE.VCS_AND_LEAVENODE:
			case STATE.VCS_AND_STANDING:
			case STATE.VCS_AND_UNSET:
			default:
				{
					break;
				}
		}

		_timerState = 0.0f;
		_stateCurrent = stateNew;

		switch( _stateCurrent )
		{
			case STATE.VCS_AND_STANDING:
				{
					// find how long we should stay here. 
					_timeAtNode =
						( _pPathNode != null ) ? Random.Range( _pPathNode._stopTimeMin, _pPathNode._stopTimeMax ) : 0.0f;
					break;
				}
			case STATE.VCS_AND_LEAVENODE:
				{
					LeaveNode();
					break;
				}
			case STATE.VCS_AND_FINISHED:
				{
					Finish();
					break;
				}
			case STATE.VCS_AND_UNSET:
			default:
				{
					if( _pOwnerBrain != null )
					{
						_pOwnerBrain.StopAllMovement();
					}
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateUnset()
	{
		if( _pOwnerBrain == null )
			return;
		if( _pOwnerBrain._pOwnerActor == null )
			return;

		// if we didnt have a node set then finish the behaviour altogether. 
		if( _pPathNode == null )
		{
			SwitchToState( STATE.VCS_AND_FINISHED );
			return;
		}

		SwitchToState( STATE.VCS_AND_STANDING );
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateStanding()
	{
		// if we have been here long enough then leave the node. 
		if( _timerState >= _timeAtNode )
		{
			SwitchToState( STATE.VCS_AND_LEAVENODE );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void LeaveNode()
	{
		// no node set or no links from this node means we dont know where to go, finish Vcs. 
		if( ( _pPathNode == null ) || ( _pPathNode._nodeLinkData == null ) || ( _pPathNode._nodeLinkData.Count == 0 ) )
		{
			SwitchToState( STATE.VCS_AND_FINISHED );
			return;
		}

		// find the next node to go to and link to take, not allowing back links. 
		_pVcsInfo._pathNodeDummy = _pVcsInfo._pathNodeCurrent.GetRandomNextNodeWithLink(
			false, _pVcsInfo._pathNodePrevious, out _pVcsInfo._nodeLinkCurrent );

		// if we did not find one then we are finished. 
		if( ( _pVcsInfo._pathNodeDummy == null ) || ( _pVcsInfo._nodeLinkCurrent == null ) )
		{
			SwitchToState( STATE.VCS_AND_FINISHED );
			return;
		}

		// update the previous and current nodes. 
		_pVcsInfo._pathNodePrevious = _pVcsInfo._pathNodeCurrent;
		_pVcsInfo._pathNodeCurrent = _pVcsInfo._pathNodeDummy;
		_pVcsInfo._pathNodeDummy = null;

		// push the link behaviour, replacing us while we are at it. 
		if( !_pOwnerBrain.VogonConstructorScriptPushBehaviour( _pVcsInfo._nodeLinkCurrent._traverseLinkBehaviour, true ) )
		{
			// behaviour failed. exit to be save. 
			SwitchToState( STATE.VCS_AND_FINISHED );
			return;
		}
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public override void OnDrawGizmos()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void OnGUI( int width, ref Vector2 textPos )
	{
	}
#endif
}


