//----------------------------------------------------------------------------------------------------------------------
// BehaveAttackRanged.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using AmuzoActor;
using GameDefines;
using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public abstract class BehaveBase
{
	//------------------------------------------------------------------------------------------------------------------
	// STATICS / CONSTS / DEFINES
	static int _nextIdBehave = 0;
	static List<BehaveBase> _behaves = new List<BehaveBase>();

	//------------------------------------------------------------------------------------------------------------------
	// MEMBER FIELDS
	protected Vector3 _ourPosCurrent;
	protected Vector3 _ourPosLast;
	protected Vector3 _ourForward;
	protected JsonData _dummyData = null;
	protected bool _paused;

	int _idBehave;

	eBType _type = eBType.NUM_BTYPE;
	BrainActorBase _ownerBrain = null;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public static List<BehaveBase> _pListBehaves
	{
		get
		{
			return _behaves;
		}
	}

	public int _pIdBehave
	{
		get
		{
			return _idBehave;
		}
	}
	public eBType _pBType
	{
		get
		{
			return _type;
		}
		set
		{
			_type = value;
		}
	}
	public virtual bool _pIsVcsBehaviour
	{
		get
		{
			return false;
		}
	}
	public virtual bool _pShouldPauseOnBuildPointActive
	{
		get
		{
			return true;
		}
	}

	public BrainActorBase _pOwnerBrain
	{
		get
		{
			return _ownerBrain;
		}
		set
		{
			_ownerBrain = value;
		}
	}

	protected eAllegiance _pAllegiance
	{
		get
		{
			if( _ownerBrain == null )
				return eAllegiance.NEUTRAL;

			return _ownerBrain._pAllegiance;
		}
	}

	protected bool _pHasOwner
	{
		get
		{
			return ( ( _ownerBrain != null ) && ( _ownerBrain._pOwnerActor != null ) );
		}
	}

	protected ActorLogicBase _pOwnerActor
	{
		get
		{
			return _pHasOwner ? _ownerBrain._pOwnerActor : null;
		}
	}

	protected virtual bool _pShouldUpdate
	{
		get
		{
			return !_paused && _pHasOwner && _ownerBrain._pOwnerActor._pIsAlive;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	// ABSTRACTS
	public abstract int _pBehaviourType
	{
		get;
	}

	public abstract void InitialiseData();

#if UNITY_EDITOR
	public abstract void OnDrawGizmos();
	public abstract void OnGUI( int width, ref Vector2 textPos );
#endif

	protected abstract void OnUpdate();
	protected abstract void OnFixedUpdate();
	protected abstract void OnLateUpdate();
	protected abstract void OnShutdown();
	protected abstract void OnPause();
	protected abstract void OnUnPause();

	//------------------------------------------------------------------------------------------------------------------
	public BehaveBase()
	{
		// add us to the static list of all actors for fast reference. 
		if( !_behaves.Contains( this ) )
		{
			_behaves.Add( this );
			_idBehave = _nextIdBehave++;
		}

		_paused = false;
	}

	//------------------------------------------------------------------------------------------------------------------
	~BehaveBase()
	{
		// remove from behaviours list. 
		_behaves.Remove( this );
	}

	//------------------------------------------------------------------------------------------------------------------
	public void Update()
	{
		if( !_pShouldUpdate )
			return;

		_ourPosLast = _ourPosCurrent;
		_ourPosCurrent = _ownerBrain._pOurPos;
		_ourForward = _ownerBrain._pOurForward;

		OnUpdate();
	}

	//------------------------------------------------------------------------------------------------------------------
	public void FixedUpdate()
	{
		if( !_pShouldUpdate )
			return;

		OnFixedUpdate();
	}

	//------------------------------------------------------------------------------------------------------------------
	public void LateUpdate()
	{
		if( !_pShouldUpdate )
			return;

		OnLateUpdate();
	}

	//------------------------------------------------------------------------------------------------------------------
	public void Shutdown()
	{
		OnShutdown();
		_paused = false;
		_ownerBrain = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void Pause()
	{
		_paused = true;
		OnPause();
	}

	//------------------------------------------------------------------------------------------------------------------
	public void UnPause()
	{
		_paused = false;
		OnUnPause();
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void Finish()
	{
		if( _pOwnerBrain != null )
		{
			_pOwnerBrain.PopBehaviour( _pBehaviourType, _type );
		}
	}
}

