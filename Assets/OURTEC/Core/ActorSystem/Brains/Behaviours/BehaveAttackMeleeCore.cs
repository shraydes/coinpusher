//----------------------------------------------------------------------------------------------------------------------
// BehaveAttackMeleeCore.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class BehaveAttackMeleeCore : BehaveBase 
{
	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	protected int _numMeleeAttacks = 0;
	protected float _rangeMelee = 5.0f;
	protected float _timeOfLastRequest = 0.0f;

	int _specialAttackIndex;
	float _movingAttackDelay = 3.0f;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override int _pBehaviourType{ get { return ( int )eBehaviourCore.ATTACK_MELEE_CORE; } }
	protected override bool _pShouldUpdate
	{ 
		get{ return base._pShouldUpdate && _pHasMeleeAttacks; } 
	}

	protected bool _pHasMeleeAttacks{ get{ return _numMeleeAttacks > 0 ; } }
	protected bool _pHasSpecialAttack{ get{ return _specialAttackIndex != GlobalDefines.INVALID_ID; } }

	//------------------------------------------------------------------------------------------------------------------
	public BehaveAttackMeleeCore()
	: base()
	{
		_numMeleeAttacks = 0;
		_specialAttackIndex = GlobalDefines.INVALID_ID;
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public override void InitialiseData()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUpdate() 
	{
		if( !_pHasMeleeAttacks ) return;
		if( _pOwnerBrain._pOwnerActor == null ) return;

		// if we dont have ranged attacks keep us looking at the right place. 
		if( !_pOwnerBrain._pHasAttacksRanged )
		{
			if( _pOwnerBrain._pHasTargetValid )
			{
				// look at our target
				_pOwnerBrain._pOwnerActor._pForward = 
					( _pOwnerBrain._pTargetInfo._positionTarget - _pOwnerBrain._pOurPos ).normalized;
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnFixedUpdate()
	{
		if( !_pHasMeleeAttacks ) return;

		if( _pOwnerBrain._pHasTargetValid )
		{			
			if( _pOwnerBrain._pTargetInfo._distToTarget <= _rangeMelee )
			{
				// if we are close enough and within aim then requests melee attack. 
				if( ( GlobalDefines.IsApproximately( _pOwnerBrain._pTargetInfo._dirToTarget.x, _pOwnerBrain._pOurForward.x, 0.15f ) ) &&
				    ( GlobalDefines.IsApproximately( _pOwnerBrain._pTargetInfo._dirToTarget.z, _pOwnerBrain._pOurForward.z, 0.15f ) ) )
				{
					bool valid = true;
			
					// if we are not at our destination only request an attack every so often to keep us moving. 
					if( !_pOwnerBrain._pIsAtCurrentDestination )
					{
						if( ( Time.time - _timeOfLastRequest ) < _movingAttackDelay )
						{
							valid = false;
						}
					}

					if( valid )
					{
						_timeOfLastRequest = Time.time;
						_pOwnerBrain.RequestAttack( Random.Range( 1, _numMeleeAttacks ) );
					}
				}
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnLateUpdate() 
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnPause()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUnPause()
	{
	}

	#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public override void OnDrawGizmos()
	{
	}
	
	//------------------------------------------------------------------------------------------------------------------
	public override void OnGUI( int width, ref Vector2 textPos )
	{
	}
	#endif
}


