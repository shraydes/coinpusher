//----------------------------------------------------------------------------------------------------------------------
// BehaveMoveWithinRange.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class BehaveMoveWithinRange : BehaveBase
{
	//------------------------------------------------------------------------------------------------------------------
	//	CONSTS / STATICS / DEFINES
	const float _cRangeMinDefault = 10.0f;
	const float _cRangeMaxDefault = 30.0f;

	const float _cTimeBeforePathRePlan = 1.5f;

	enum STATE
	{
		MWR_UNSET,

		MWR_STANDING,
		MWR_MOVING_TOWARDS,
		MWR_MOVING_AWAY,

		NUM_MWR_STATES
	}

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	STATE _stateCurrent;
	float _timerBehaviour;
	float _timerState;
	float _rangeMin = _cRangeMinDefault;
	float _rangeMax = _cRangeMaxDefault;
	float _rangeMid = _cRangeMinDefault + ( ( _cRangeMaxDefault - _cRangeMinDefault ) * 0.5f );

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override int _pBehaviourType
	{
		get
		{
			return ( int )eBehaviourCore.MOVE_WITHIN_RANGE;
		}
	}
	protected override bool _pShouldUpdate
	{
		get
		{
			return base._pShouldUpdate && ( _pAllegiance != eAllegiance.NEUTRAL );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public BehaveMoveWithinRange()
	: base()
	{
		_stateCurrent = STATE.MWR_UNSET;
		_timerBehaviour = 0.0f;
		_timerState = 0.0f;
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void InitialiseData()
	{
		// look up the ranges. 

		// middle of the ranges. (the ideal position. 
		_rangeMid = _rangeMin + ( ( _rangeMax - _rangeMin ) * 0.5f );
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUpdate()
	{
		// with no owner we should not be updating. 
		if( ( _pOwnerBrain == null ) && ( _stateCurrent != STATE.MWR_UNSET ) )
		{
			SwitchToState( STATE.MWR_UNSET );
			return;
		}

		_timerBehaviour += Time.deltaTime;
		_timerState += Time.deltaTime;

		switch( _stateCurrent )
		{
			case STATE.MWR_STANDING:
				{
					UpdateStanding();
					break;
				}
			case STATE.MWR_MOVING_TOWARDS:
				{
					UpdateMovingTowards();
					break;
				}
			case STATE.MWR_MOVING_AWAY:
				{
					UpdateMovingAway();
					break;
				}
			case STATE.MWR_UNSET:
			default:
				{
					UpdateUnset();
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnFixedUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnLateUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
		if( _stateCurrent != STATE.MWR_UNSET )
		{
			SwitchToState( STATE.MWR_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnPause()
	{
		if( _stateCurrent != STATE.MWR_UNSET )
		{
			SwitchToState( STATE.MWR_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUnPause()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	void SwitchToState( STATE stateNew )
	{
		// do anything needed to finish or stop current state. 
		switch( _stateCurrent )
		{
			case STATE.MWR_STANDING:
			case STATE.MWR_MOVING_TOWARDS:
			case STATE.MWR_MOVING_AWAY:
			case STATE.MWR_UNSET:
			default:
				{
					break;
				}
		}

		_timerState = 0.0f;
		_stateCurrent = stateNew;

		switch( _stateCurrent )
		{
			case STATE.MWR_MOVING_TOWARDS:
				{
					CreateMovementTowards();
					break;
				}
			case STATE.MWR_MOVING_AWAY:
				{
					CreateMovementAway();
					break;
				}
			case STATE.MWR_STANDING:
			case STATE.MWR_UNSET:
			default:
				{
					if( _pOwnerBrain != null )
					{
						_pOwnerBrain.StopAllMovement();
					}
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateUnset()
	{
		if( _pOwnerBrain == null )
			return;
		if( _pOwnerBrain._pOwnerActor == null )
			return;

		SwitchToState( STATE.MWR_STANDING );
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateStanding()
	{
		if( !_pOwnerBrain._pHasTarget )
			return;

		if( _pOwnerBrain._pTargetInfo._distToTarget > _rangeMax )
		{
			SwitchToState( STATE.MWR_MOVING_TOWARDS );
			return;
		}

		if( _pOwnerBrain._pTargetInfo._distToTarget < _rangeMin )
		{
			SwitchToState( STATE.MWR_MOVING_AWAY );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateMovingTowards()
	{
		// if we no longer have a threat then stand.
		if( !_pOwnerBrain._pHasTarget )
		{
			SwitchToState( STATE.MWR_STANDING );
			return;
		}

		// if our threat is now too close move away. 
		if( _pOwnerBrain._pTargetInfo._distToTarget < _rangeMin )
		{
			SwitchToState( STATE.MWR_MOVING_AWAY );
			return;
		}

		// if we are within range then stand. 
		if( _pOwnerBrain._pTargetInfo._distToTarget < _rangeMid )
		{
			SwitchToState( STATE.MWR_STANDING );
			return;
		}

		// if we have finished the moment and get here we are still not in range, plan again. 
		if( !_pOwnerBrain._pIsMoving && ( _timerState >= _cTimeBeforePathRePlan ) )
		{
			SwitchToState( STATE.MWR_MOVING_TOWARDS );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateMovingAway()
	{
		// if we no longer have a threat then stand.
		if( !_pOwnerBrain._pHasTarget )
		{
			SwitchToState( STATE.MWR_STANDING );
			return;
		}

		// if our threat is now too far move closer. 
		if( _pOwnerBrain._pTargetInfo._distToTarget > _rangeMax )
		{
			SwitchToState( STATE.MWR_MOVING_TOWARDS );
			return;
		}

		// if we are within range then stand. 
		if( _pOwnerBrain._pTargetInfo._distToTarget > _rangeMid )
		{
			SwitchToState( STATE.MWR_STANDING );
			return;
		}

		// if we have finished the moment and get here we are still not in range, plan again. 
		if( !_pOwnerBrain._pIsMoving && ( _timerState >= _cTimeBeforePathRePlan ) )
		{
			SwitchToState( STATE.MWR_MOVING_AWAY );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void CreateMovementTowards()
	{
		if( _pOwnerBrain == null )
			return;
		if( !_pOwnerBrain._pHasTarget )
			return;

		_pOwnerBrain.IssueMoveOrder( _pOwnerBrain._pTargetInfo._positionFloor );
	}

	//------------------------------------------------------------------------------------------------------------------
	void CreateMovementAway()
	{
		if( _pOwnerBrain == null )
			return;
		if( !_pOwnerBrain._pHasTarget )
			return;

		_pOwnerBrain.IssueMoveOrder(
			_pOwnerBrain._pTargetInfo._positionFloor + ( -_pOwnerBrain._pTargetInfo._dirToTarget * _rangeMax ) );
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public override void OnDrawGizmos()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void OnGUI( int width, ref Vector2 textPos )
	{
	}
#endif
}

