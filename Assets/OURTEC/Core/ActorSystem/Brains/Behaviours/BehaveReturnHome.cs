//----------------------------------------------------------------------------------------------------------------------
// BehaveReturnHome.cs
// 14/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class BehaveReturnHome : BehaveBase
{
	//------------------------------------------------------------------------------------------------------------------
	//	CONSTS / STATICS / DEFINES		
	enum STATE
	{
		RH_UNSET,

		RH_MOVING,
		RH_FINISHED,

		NUM_RH_STATES
	}

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	STATE _stateCurrent;
	float _timerBehaviour;
	float _timerState;

	bool _destroyOnArrival = false;
	bool _hasMoved = false;
	Vector3 _homePos = Vector3.zero;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public override int _pBehaviourType
	{
		get
		{
			return ( int )eBehaviourCore.RETURN_HOME;
		}
	}
	public bool _pDestroyOnArrival
	{
		set
		{
			_destroyOnArrival = value;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public BehaveReturnHome()
	: base()
	{
		_stateCurrent = STATE.RH_UNSET;
		_timerBehaviour = 0.0f;
		_timerState = 0.0f;
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void InitialiseData()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUpdate()
	{
		// with no owner we should not be updating. 
		if( ( _pOwnerBrain == null ) && ( _stateCurrent != STATE.RH_UNSET ) )
		{
			SwitchToState( STATE.RH_UNSET );
			return;
		}

		_timerBehaviour += Time.deltaTime;
		_timerState += Time.deltaTime;

		switch( _stateCurrent )
		{
			case STATE.RH_MOVING:
				{
					UpdateMoving();
					break;
				}
			case STATE.RH_FINISHED:
				{
					break;
				}
			case STATE.RH_UNSET:
			default:
				{
					UpdateUnset();
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnFixedUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnLateUpdate()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnShutdown()
	{
		if( _stateCurrent != STATE.RH_UNSET )
		{
			SwitchToState( STATE.RH_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnPause()
	{
		if( _stateCurrent != STATE.RH_UNSET )
		{
			SwitchToState( STATE.RH_UNSET );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected override void OnUnPause()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	void SwitchToState( STATE stateNew )
	{
		// do anything needed to finish or stop current state. 
		switch( _stateCurrent )
		{
			case STATE.RH_FINISHED:
			case STATE.RH_MOVING:
			case STATE.RH_UNSET:
			default:
				{
					break;
				}
		}

		_timerState = 0.0f;
		_stateCurrent = stateNew;

		switch( _stateCurrent )
		{
			case STATE.RH_MOVING:
				{
					_hasMoved = false;
					CreateMovementTowards();
					break;
				}
			case STATE.RH_FINISHED:
				{
					// if we should despawn then do so otherwise just finishe the behaviour. 
					if( _destroyOnArrival && ( _pOwnerBrain != null ) && ( _pOwnerBrain._pOwnerActor != null ) )
					{
						_pOwnerBrain._pOwnerActor.ReturnActor();
					}
					else
					{
						Finish();
						if( _pOwnerBrain != null )
						{
							// now that we are here revert back to default move behaviour. 
							_pOwnerBrain.PushBehaviour( ( int )eBehaviourCore.USE_DEFAULT, eBType.MOVE );
						}
					}
					break;
				}
			case STATE.RH_UNSET:
			default:
				{
					_homePos = Vector3.zero;
					if( _pOwnerBrain != null )
					{
						_pOwnerBrain.StopAllMovement();
					}
					break;
				}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateUnset()
	{
		if( _pOwnerBrain == null )
			return;
		if( _pOwnerBrain._pOwnerActor == null )
			return;

		_homePos = _pOwnerBrain._pHomePosition;

		// if a position has not been set then we cant go anywhere. 
		if( _homePos == Vector3.zero )
			return;

		SwitchToState( STATE.RH_MOVING );
	}

	//------------------------------------------------------------------------------------------------------------------
	void UpdateMoving()
	{
		// if we have finished the moment and get here we are still not in range, plan again. 
		if( _pOwnerBrain._pIsAtCurrentDestination )
		{
			SwitchToState( STATE.RH_FINISHED );
			return;
		}

		// keep us up to date on whether or not we have started to move. 
		if( !_hasMoved )
		{
			_hasMoved = _pOwnerBrain._pOwnerActor._pIsMoving;
		}
		else if( !_pOwnerBrain._pOwnerActor._pIsMoving )
		{
			// if we have started moving and are no longer then we might be stuck, re-path plan
			SwitchToState( STATE.RH_MOVING );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void CreateMovementTowards()
	{
		if( _pOwnerBrain == null )
			return;

		_pOwnerBrain.IssueMoveOrder( _homePos );
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public override void OnDrawGizmos()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	public override void OnGUI( int width, ref Vector2 textPos )
	{
	}
#endif
}

