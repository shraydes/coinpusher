//----------------------------------------------------------------------------------------------------------------------
// BrainActorBase.cs
// 13/5/14

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using AmuzoActor;
using DataBlueprints;
using GameDefines;
using KnowledgeSystem;
using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eBehaviourCore
{
	// order is important, only add to end. 

	NONE = 0,                           // 0
	USE_DEFAULT,                        // acts as invalid behaviour as well. 
	NO_CHANGE,                          // used by triggers. 
	ATTACK_RANGED_CORE,
	MOVE_WITHIN_RANGE,
	MOVE_TO_TARGET,                     // 5
	ATTACK_MELEE_CORE,
	MOVE_TO_POSIITON,
	VCS_AT_NODE_DEFAULT,
	VCS_TRAVERS_LINK_DEFAULT,
	VCS_TRAVERS_LINK_WAIT_ON_THREAT,    // 10
	RETURN_HOME,

	NUM_BEHAVIOURS
}

public enum eBType
{
	MOVE,
	ATTACK,
	MISC,
	ROOT,

	NUM_BTYPE
}

public enum eAllegiance
{
	PLAYER,
	ENEMY,
	NEUTRAL
}

//----------------------------------------------------------------------------------------------------------------------
public struct TargetInfo
{
	public int _knowledgeProjectData;
	public bool _fullyOnScreen;
	public bool _dummyTarget;
	public ActorLogicBase _actorTarget;
	public float _speed;
	public float _distToTarget;
	public float _timeTargetAssigned;
	public Vector3 _positionFloor;
	public Vector3 _positionTarget;
	public Vector3 _forward;
	public Vector3 _moveDir;
	public Vector3 _dirToTarget;
	public Vector3 _dirToTargetFlat;

	public void Reset()
	{
		_knowledgeProjectData = 0;
		_fullyOnScreen = false;
		_dummyTarget = false;
		_actorTarget = null;
		_speed = 0.0f;
		_distToTarget = 0.0f;
		_timeTargetAssigned = 0.0f;
		_positionFloor = Vector3.zero;
		_positionTarget = Vector3.zero;
		_forward = Vector3.zero;
		_moveDir = Vector3.zero;
		_dirToTarget = Vector3.zero;
		_dirToTargetFlat = Vector3.zero;
	}
}

//----------------------------------------------------------------------------------------------------------------------
public class VcsInfo
{
	public bool _isOnVcs;
	public NodeLink _nodeLinkCurrent;
	public PathNode _pathNodeDummy;
	public PathNode _pathNodeCurrent;
	public PathNode _pathNodePrevious;

	public VcsInfo()
	{
		Reset();
	}

	public void Reset()
	{
		_isOnVcs = false;
		_nodeLinkCurrent = null;
		_pathNodeDummy = null;
		_pathNodeCurrent = null;
		_pathNodePrevious = null;
	}
}

//----------------------------------------------------------------------------------------------------------------------
public class BrainActorBase
{
	//------------------------------------------------------------------------------------------------------------------
	// CONSTS / DEFINES		
	private const string DEBUG_LOG_PREFIX = "BrainActorBase - ";

	private const string BPPARAM_ARRIVAL_RANGE = "RangeArrival";
	private const string BPPARAM_KNOWLEDGE_SYS = "KnowledgeSystem";
	private const string BPPARAM_BEHAVE_DEFAULT_ROOT = "Behave_Root";
	private const string BPPARAM_BEHAVE_DEFAULT_MOVE = "Behave_Move";
	private const string BPPARAM_BEHAVE_DEFAULT_ATTACK = "Behave_Attack";
	private const string BPPARAM_BEHAVE_DEFAULT_MISC = "Behave_Misc";

	//------------------------------------------------------------------------------------------------------------------
	// MEMBER FIELDS
	protected int _idActorBestTarget;
	protected int _attackRequested;
	protected int[] _defaultBehaviours;
	protected bool _shouldUpdate;
	protected float _arrivalRange;
	protected Vector3 _ourPos;
	protected Vector3 _ourTargetPos;
	protected Vector3 _ourForward;
	protected TargetInfo _targetInfo;
	protected DataBlueprint _blueprint;
	protected ActorLogicBase _ownerActor;
	protected KnowledgeSystemBase _knowledgeSystem;

	private bool _hadTarget;
	private bool _isAtDestination;
	private string _knowledgeSysClassName;
	private Vector3 _homePosition;
	private Vector3 _destinationPos;
	private VcsInfo _vcsInfo;
	private BehaveBase _dummyBehaviour;
	private eAllegiance _allegiance = eAllegiance.NEUTRAL;
	private List<BehaveBase> _dummyListBehave;
	private List<BehaveBase>[] _behaviour;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public ActorLogicBase _pOwnerActor
	{
		get
		{
			return _ownerActor;
		}
	}

	public float _pArrivalRange
	{
		get
		{
			return _arrivalRange;
		}
		set
		{
			_arrivalRange = value;
		}
	}

	public Vector3 _pOurPos
	{
		get
		{
			return _ourPos;
		}
	}

	public Vector3 _pOurTargetPos
	{
		get
		{
			return _ourTargetPos;
		}
	}

	public Vector3 _pOurForward
	{
		get
		{
			return _ourForward;
		}
	}

	public Vector3 _pDestinationPos
	{
		get
		{
			return _destinationPos;
		}
		set
		{
			_destinationPos = value;
		}
	}

	public VcsInfo _pVcsInfo
	{
		get
		{
			return _vcsInfo;
		}
	}

	public TargetInfo _pTargetInfo
	{
		get
		{
			return _targetInfo;
		}
	}

	public virtual bool _pIsAtCurrentDestination
	{
		get
		{
			return _isAtDestination;
		}
	}

	public virtual bool _pHasAttacksRanged
	{
		get
		{
			return false;
		}
	}

	public virtual bool _pHasAttacksMelee
	{
		get
		{
			return false;
		}
	}

	public virtual float _pSpeedNormalised
	{
		get
		{
			// at base level make it binary, we cant know how projects will define max speed. 
			if( _ownerActor == null )
				return 0.0f;
			return ( _ownerActor._pSpeed > 0.0f ) ? 1.0f : 0.0f;
		}
	}

	public Vector3 _pHomePosition
	{
		get
		{
			return _homePosition;
		}
		set
		{
			_homePosition = value;
		}
	}

	public bool _pHasTarget
	{
		get
		{
			return ( ( _idActorBestTarget != GlobalDefines.INVALID_ID ) && ( _targetInfo._actorTarget != null ) );
		}
	}

	public float _pTimeSinceTargetAssigned
	{
		get
		{
			if( !_pHasTarget )
				return 0.0f;

			return Time.time - _targetInfo._timeTargetAssigned;
		}
	}

	public virtual bool _pHasTargetValid
	{
		get
		{
			bool retVal = _pHasTarget && ( _pOwnerActor != null );

			// inherited brains may want to check if they are in the camera view or not, all project dependant. 

			return retVal;
		}
	}

	public bool _pIsMoving
	{
		get
		{
			if( _pOwnerActor == null )
				return false;
			return _pOwnerActor._pIsMoving;
		}
	}

	public int _pIdActorBestTarget
	{
		get
		{
			return _idActorBestTarget;
		}
		set
		{
			_idActorBestTarget = value;
			if( _targetInfo._actorTarget != null )
			{
				// if this is a new target then clear the target info. 
				if( _targetInfo._actorTarget._pIdActor != value )
				{
					_targetInfo.Reset();
					_targetInfo._timeTargetAssigned = Time.time;
					_hadTarget = false;
					UpdateTarget();
				}
			}
		}
	}

	public BehaveBase _pMoveBehaviour
	{
		get
		{
			if( _behaviour == null )
				return null;
			if( _behaviour[( int )eBType.MOVE] == null )
				return null;
			if( _behaviour[( int )eBType.MOVE].Count == 0 )
				return null;
			return _behaviour[( int )eBType.MOVE][0];
		}
	}

	public virtual eAllegiance _pAllegiance
	{
		get
		{
			return _allegiance;
		}
		set
		{
			_allegiance = value;
		}
	}

	public KnowledgeSystemBase _pKnowledgeSys
	{
		get
		{
			return _knowledgeSystem;
		}
	}

	protected bool _pAttackRequested
	{
		get
		{
			return _attackRequested != 0;
		}
	}

	protected virtual bool _pShouldUpdate
	{
		get
		{
			if( _ownerActor == null )
				return false;

#if PUMA_DEFINE_MULTIPLAYER
			if( !NetworkConnectionManager._pIsHost )
				return false;
#endif
			return _shouldUpdate;
		}
		set
		{
			_shouldUpdate = value;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public BrainActorBase( ActorLogicBase actorOwner, DataBlueprint bp )
	{
		_ownerActor = actorOwner;

		_pShouldUpdate = false;

		_targetInfo = new TargetInfo();
		_targetInfo.Reset();

		_blueprint = bp;
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void Initialise()
	{
		StopAllMovement();

		// make sure all the behaviour structures exist and start off empty. 
		InitialiseBehaviours();

		// read all of our blueprint data
		PopulateDataFromBlurprint();

		// setup our knowledge. 
		CreateAndInitialiseKnowledge();
		
		// get the starting behaivours ready to go. 
		RevertToDefaultBehaviours();

		_pShouldUpdate = true;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void Kill()
	{
		if( _knowledgeSystem != null )
		{
			_knowledgeSystem.Shutdown();
		}

		_dummyBehaviour = null;
		_dummyListBehave = null;

		RemoveBehaviour();

		_targetInfo.Reset();
	}

	//------------------------------------------------------------------------------------------------------------------
	public void ShutDown()
	{
		_pShouldUpdate = false;
		RemoveBehaviour();
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void PopulateDataFromBlurprint()
	{
		if( _blueprint == null )
			return;

		float arrivalRange = 0.0f;
		if( _blueprint.TryGetFloat( BPPARAM_ARRIVAL_RANGE, out arrivalRange ) )
		{
			_arrivalRange = arrivalRange;
		}
			
		// look up default behaivours. 
		string behaveName = "";
		if( _blueprint.TryGetString( BPPARAM_BEHAVE_DEFAULT_ROOT, out behaveName ) )
		{
			_defaultBehaviours[( int )eBType.ROOT] = GetBehaviourEnumFromNameString( 
				( string.IsNullOrEmpty( behaveName ) ? eBehaviourCore.NONE.ToString() : behaveName ) );
		}
		if( _blueprint.TryGetString( BPPARAM_BEHAVE_DEFAULT_MOVE, out behaveName ) )
		{
			_defaultBehaviours[( int )eBType.MOVE] = GetBehaviourEnumFromNameString( 
				( string.IsNullOrEmpty( behaveName ) ? eBehaviourCore.NONE.ToString() : behaveName ) );
		}
		if( _blueprint.TryGetString( BPPARAM_BEHAVE_DEFAULT_ATTACK, out behaveName ) )
		{
			_defaultBehaviours[( int )eBType.ATTACK] = GetBehaviourEnumFromNameString( 
				( string.IsNullOrEmpty( behaveName ) ? eBehaviourCore.NONE.ToString() : behaveName ) );
		}
		if( _blueprint.TryGetString( BPPARAM_BEHAVE_DEFAULT_MISC, out behaveName ) )
		{
			_defaultBehaviours[( int )eBType.MISC] = GetBehaviourEnumFromNameString( 
				( string.IsNullOrEmpty( behaveName ) ? eBehaviourCore.NONE.ToString() : behaveName ) );
		}

		string knowledgeSysClassName = "";
		if( _blueprint.TryGetString( BPPARAM_KNOWLEDGE_SYS, out knowledgeSysClassName ) )
		{
			if( !string.IsNullOrEmpty( knowledgeSysClassName ) )
				_knowledgeSysClassName = knowledgeSysClassName;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void CreateAndInitialiseKnowledge()
	{
		// base actor level so just create a base Knowlege system. 
		if( !string.IsNullOrEmpty( _knowledgeSysClassName ) && ( _knowledgeSystem == null ) )
		{
			System.Type type = GetType().Assembly.GetType( _knowledgeSysClassName, false );

			if( type == null )
			{
				Debug.LogWarning( DEBUG_LOG_PREFIX +
					"CreateAndInitialiseKnowledge - knowledge sys class type can not be found: " +
					_knowledgeSystem );
				return;
			}

			_knowledgeSystem = ( KnowledgeSystemBase )System.Activator.CreateInstance( type );
			_knowledgeSystem.Initialise( _pOwnerActor, this );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void Update()
	{
		if( _pOwnerActor == null )
			return;
		if( !_pShouldUpdate )
			return;

		if( _knowledgeSystem != null )
		{
			_knowledgeSystem.Update();
		}

		_isAtDestination = ( ( _destinationPos - _ourPos ).sqrMagnitude <= ( _arrivalRange * _arrivalRange ) );
		
		// update default look direction before behaviours, they can change the Direction if they want to. 
		UpdateDefaultLookDirection();

		UpdateTarget();

		// update behaviours
		if( _behaviour != null )
		{
			for( int count = 0; count < _behaviour.Length; count++ )
			{
				if( _behaviour[count] == null )
					continue;
				if( _behaviour[count].Count == 0 )
					continue;
				if( _behaviour[count][0] == null )
					continue;
				_behaviour[count][0].Update();
			}
		}

		UpdateAttacks();
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void FixedUpdate()
	{
		if( !_pShouldUpdate )
			return;

		// update our position and direction for our behaviours to use. 
		if( _ownerActor != null )
		{
			_ourTargetPos = _ownerActor._pTargetPos;
			_ourPos = _ownerActor._pPosition;
			_ourForward = _ownerActor._pForward;
		}
		UpdateTargetInfo();

		// fixed update behaviours
		if( _behaviour != null )
		{
			for( int count = 0; count < _behaviour.Length; count++ )
			{
				if( _behaviour[count] == null )
					continue;
				if( _behaviour[count].Count == 0 )
					continue;
				if( _behaviour[count][0] == null )
					continue;
				_behaviour[count][0].FixedUpdate();
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void LateUpdate()
	{
		if( !_pShouldUpdate )
			return;

		// late update behaviours
		if( _behaviour != null )
		{
			for( int count = 0; count < _behaviour.Length; count++ )
			{
				if( _behaviour[count] == null )
					continue;
				if( _behaviour[count].Count == 0 )
					continue;
				if( _behaviour[count][0] == null )
					continue;
				_behaviour[count][0].LateUpdate();
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnOwnerDeath()
	{
		if( _knowledgeSystem != null )
		{
			_knowledgeSystem.ClearAllKnowledge();
		}
		
		_pShouldUpdate = false;

		StopAllMovement();
		RemoveBehaviour();
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual float ScoreKnowledgeBrainSpecific( ref Knowledge knowledge )
	{
		// nothing specific at base level. 
		return 0.0f;
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void RequestAttack( int attackId )
	{
		_attackRequested = attackId;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void StopAllMovement( bool force = false )
	{
		if( _ownerActor == null )
			return;
		if( !_ownerActor._pIsAlive )
			return;

		_ownerActor.StopAllMovement();

		_destinationPos = _ownerActor._pPosition;
		_isAtDestination = true;
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual bool IssueMoveOrder( Vector3 dest, float speedNorm = 1.0f )
	{
		if( _ownerActor == null )
			return false;
		if( !_ownerActor._pIsAlive )
			return false;

		_destinationPos = dest;

		_isAtDestination = ( ( _destinationPos - _ourPos ).sqrMagnitude <= ( _arrivalRange * _arrivalRange ) );

		return _ownerActor.OnBrainGivenDestination( dest, speedNorm );
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool VogonConstructorScriptEnter( PathNodeGroup pathNodeGroup )
	{
		if( _pOwnerActor == null )
			return false;
		if( pathNodeGroup == null )
			return false;

		if( _vcsInfo == null )
		{
			_vcsInfo = new VcsInfo();
			_vcsInfo.Reset();
		}

		_vcsInfo._pathNodePrevious = null;
		_vcsInfo._pathNodeCurrent = pathNodeGroup.GetHeadNode();

		if( _vcsInfo._pathNodeCurrent == null )
			return false;
		if( _vcsInfo._pathNodeCurrent._atNodeBehaviour == ( int )eBehaviourCore.NONE )
			return false;

		// move us to the position of the head node. 
		_pOwnerActor._pPosition = _vcsInfo._pathNodeCurrent._pWorldPos;

		// push the behaviour of this node to start us off (dont replace if this is the first Vcs behaviour. 
		return VogonConstructorScriptPushBehaviour( ( int )( _vcsInfo._pathNodeCurrent._atNodeBehaviour ), false );
	}

	//------------------------------------------------------------------------------------------------------------------
	public void VogonConstructorScriptExit()
	{
		if( _vcsInfo != null )
		{
			// set out exit node as our new home. 
			if( _vcsInfo._pathNodeCurrent != null )
			{
				_pHomePosition = _vcsInfo._pathNodeCurrent._pWorldPos;
			}

			_vcsInfo.Reset();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool VogonConstructorScriptPushBehaviour( int vcsBehave, bool replace )
	{
		BehavePool._pInstance.GetBehaviour( vcsBehave, ref _dummyBehaviour, this );

		if( _dummyBehaviour == null )
			return false;

		if( _dummyBehaviour._pIsVcsBehaviour )
		{
			// pass in the Vcs info for the behaviours to read and update. 
			( ( VCSBehaveBase )_dummyBehaviour )._pVcsInfo = _vcsInfo;
		}
		else
		{
			Debug.LogWarning( DEBUG_LOG_PREFIX + "VogonConstructorScriptPushBehaviour - was are entering a VCScript " +
				"but are popping a non Vcs behaviour this should not happen, Behaviour: " +
				_dummyBehaviour._pBehaviourType.ToString() );
		}

		// push Vcs behaviours to our movement list. 
		PushBehaviour( _dummyBehaviour, eBType.MOVE, replace );
		return true;
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual int GetBehaviourEnumFromNameString( string name )
	{
		switch( name )
		{
			case "ATTACK_RANGED_CORE":
				return ( int )eBehaviourCore.ATTACK_RANGED_CORE;
			case "MOVE_WITHIN_RANGE":
				return ( int )eBehaviourCore.MOVE_WITHIN_RANGE;
			case "MOVE_TO_TARGET":
				return ( int )eBehaviourCore.MOVE_TO_TARGET;
			case "ATTACK_MELEE_CORE":
				return ( int )eBehaviourCore.ATTACK_MELEE_CORE;
			case "MOVE_TO_POSIITON":
				return ( int )eBehaviourCore.MOVE_TO_POSIITON;
			case "VCS_AT_NODE_DEFAULT":
				return ( int )eBehaviourCore.VCS_AT_NODE_DEFAULT;
			case "VCS_TRAVERS_LINK_DEFAULT":
				return ( int )eBehaviourCore.VCS_TRAVERS_LINK_DEFAULT;
			case "VCS_TRAVERS_LINK_WAIT_ON_THREAT":
				return ( int )eBehaviourCore.VCS_TRAVERS_LINK_WAIT_ON_THREAT;
			case "RETURN_HOME":
				return ( int )eBehaviourCore.RETURN_HOME;
			case "NONE":
				return ( int )eBehaviourCore.NONE;
			default:
				Debug.LogWarning( DEBUG_LOG_PREFIX + "GetBehaviourEnumFromNameString - enum entry not found: " + name );
				return ( int )eBehaviourCore.NONE;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void CreateBehaviour( int behaviour, ref BehaveBase newBehaviour )
	{
		// create the correct behave script according to type. 
		switch( behaviour )
		{
			case ( int )eBehaviourCore.ATTACK_RANGED_CORE:
				newBehaviour = new BehaveAttackRangedCore();
				break;
			case ( int )eBehaviourCore.MOVE_WITHIN_RANGE:
				newBehaviour = new BehaveMoveWithinRange();
				break;
			case ( int )eBehaviourCore.MOVE_TO_TARGET:
				newBehaviour = new BehaveMoveToTarget();
				break;
			case ( int )eBehaviourCore.ATTACK_MELEE_CORE:
				newBehaviour = new BehaveAttackMeleeCore();
				break;
			case ( int )eBehaviourCore.MOVE_TO_POSIITON:
				newBehaviour = new BehaveMoveToPosition();
				break;
			case ( int )eBehaviourCore.VCS_AT_NODE_DEFAULT:
				newBehaviour = new VCSBehaveAtNodeDefault();
				break;
			case ( int )eBehaviourCore.VCS_TRAVERS_LINK_DEFAULT:
				newBehaviour = new VCSBehaveTraverseLinkDefault();
				break;
			case ( int )eBehaviourCore.VCS_TRAVERS_LINK_WAIT_ON_THREAT:
				newBehaviour = new VCSBehaveTraverseLinkWaitOnThreat();
				break;
			case ( int )eBehaviourCore.RETURN_HOME:
				newBehaviour = new BehaveReturnHome();
				break;
			default:
				Debug.LogError( DEBUG_LOG_PREFIX + "CreateBehaviour - ERROR, unknown Behaviour type: " +
					behaviour.ToString() );
				break;
			}
	}

	//------------------------------------------------------------------------------------------------------------------
	// type defines which list to add it to, 
	// replace defines if we should replace the current behaviour on that list or pop on top of it. 
	public BehaveBase PushBehaviour( BehaveBase behaviour, eBType type, bool replace = false )
	{
		if( behaviour == null )
			return null;

		if( type == eBType.NUM_BTYPE )
		{
			Debug.LogWarning( DEBUG_LOG_PREFIX + "PushBehaviour - not type was specified so we dont know which list " +
				"to add the new behaviour to, nothing will be done. behaviour: " + behaviour.ToString() );
			return null;
		}

		if( replace && ( _behaviour[( int )type].Count > 0 ) )
		{
			// if we should replace the current get rid of the current front one if there is one. 
			PopBehaviour( _behaviour[( int )type][0]._pBehaviourType, type );
		}

		// pop any behaviours that are the same as the one we are about to add. 
		PopBehaviour( behaviour._pBehaviourType );

		// if we have removed a behaviour or not pause the current top behaviour if there is one. 
		if( _behaviour[( int )type].Count > 0 )
		{
			_behaviour[( int )type][0].Pause();
		}

		behaviour._pOwnerBrain = this;
		_behaviour[( int )type].Insert( 0, behaviour );
		behaviour._pBType = type;
		behaviour.InitialiseData();

		return behaviour;
	}

	//------------------------------------------------------------------------------------------------------------------
	// type defines which list to add it to, 
	// 'replace' defines if we should replace the current behaviour on that list or pop on top of it. 
	public BehaveBase PushBehaviour( int behaviour, eBType type, bool replace = false )
	{
		_dummyBehaviour = null;

		if( type == eBType.NUM_BTYPE )
		{
			Debug.LogWarning( DEBUG_LOG_PREFIX + "PushBehaviour - not type was specified so we dont know which list " +
				"to add the new behaviour to, nothing will be done. behaviour: " + behaviour.ToString() );
			return null;
		}

		if( _behaviour == null )
			return null;

		// if we have been asked to set no behaviour in this list then remove anything we have and return. 
		if( behaviour == ( int )eBehaviourCore.NONE )
		{
			RemoveBehaviour( type );
			return null;
		}

		// if we have been asked to revert to Default then do so and return. 
		if( behaviour == ( int )eBehaviourCore.USE_DEFAULT )
		{
			RevertToDefaultBehaviours( type );
			return null;
		}

		// if this behaviour is running on any of the lists already dont add it again. 
		int typeCount = -1;
		foreach( List<BehaveBase> currentList in _behaviour )
		{
			typeCount++;
			if( currentList == null )
				continue;
			if( currentList.Count <= 0 )
				continue;
			if( currentList[0]._pBehaviourType == behaviour )
			{
				Debug.Log( DEBUG_LOG_PREFIX + "PushBehaviour - " +
					"Behaviour not added because we already have one of that kind running. Behaviour Requested: " +
					behaviour.ToString() + ", Type Requested: " + type.ToString() + ", Type Already Running: " +
					( ( eBType )typeCount ).ToString() );
				return null;
				;
			}
		}

		// if we should replace the current get rid of the current front one if there is one. 
		if( replace && ( _behaviour[( int )type].Count > 0 ) )
		{
			PopBehaviour( _behaviour[( int )type][0]._pBehaviourType, type );
		}

		// if we have removed a behaviour or not pause the current top behaviour if there is one. 
		if( _behaviour[( int )type].Count > 0 )
		{
			_behaviour[( int )type][0].Pause();
		}

		// get an instance of the new behaviour we want to push. 
		BehavePool._pInstance.GetBehaviour( ( int )behaviour, ref _dummyBehaviour, this );

		// if one was successfully initialise it and PUSH IT!
		if( _dummyBehaviour != null )
		{
			_dummyBehaviour._pOwnerBrain = this;
			_dummyBehaviour.InitialiseData();
			_dummyBehaviour._pBType = type;
			_behaviour[( int )type].Insert( 0, _dummyBehaviour );
		}

		_dummyListBehave = null;

		return _dummyBehaviour;
	}

	//------------------------------------------------------------------------------------------------------------------
	// type defines which list to add it to, if NUM_BTYPE then it means any lists that have it. 
	public void PopBehaviour( int behaviour, eBType type = eBType.NUM_BTYPE )
	{
		if( _behaviour == null )
			return;

		if( type == eBType.NUM_BTYPE )
		{
			// no type defined look for any list with this behaviour. 
			foreach( List<BehaveBase> currentList in _behaviour )
			{
				if( currentList == null )
					continue;
				if( currentList.Count <= 0 )
					continue;
				if( currentList[0]._pBehaviourType == behaviour )
				{
					_dummyListBehave = currentList;
					break;
				}
			}
		}
		else
		{
			// specific type defined saftey check it. 
			if( _behaviour[( int )type] == null )
				return;
			if( _behaviour[( int )type].Count == 0 )
				return;
			if( _behaviour[( int )type][0]._pBehaviourType != behaviour )
				return;
			_dummyListBehave = _behaviour[( int )type];
		}

		// nothing found, return;
		if( _dummyListBehave == null )
			return;

		// get the first behaviour which will be the one we are looking for. 
		_dummyBehaviour = _dummyListBehave[0];
		if( _dummyBehaviour == null )
		{
			_dummyListBehave = null;
			return;
		}
		_dummyBehaviour.Shutdown();

		// remove it from our list. 
		_dummyListBehave.RemoveAt( 0 );

		// give it back to the pool
		BehavePool._pInstance.FreeBehaviour( ( int )behaviour, _dummyBehaviour._pIdBehave );

		// dont forget to unpause the behaviour behind if there is one. 
		if( _dummyListBehave.Count > 0 )
		{
			_dummyListBehave[0].UnPause();
		}

		_dummyListBehave = null;
		_dummyBehaviour = null;
	}

	//------------------------------------------------------------------------------------------------------------------
	// if type = NUM_BTYPE then revert everything. otherwise just revert the specific type specified. 
	public void RevertToDefaultBehaviours( eBType type = eBType.NUM_BTYPE )
	{
		if( _behaviour == null )
			return;

		for( int count = 0; count < ( int )eBType.NUM_BTYPE; count++ )
		{
			if( _behaviour[count] == null )
				continue;
			
			if( ( type == eBType.NUM_BTYPE ) || ( type == ( eBType )count ) )
			{
				// if we have behaviours in this list see if we should remove them. 
				if( _behaviour[count].Count > 0 )
				{
					// if we are already using our defualt then there is noting to do here. 
					if( _behaviour[count][0]._pBehaviourType == _defaultBehaviours[count] )
						continue;

					RemoveBehaviour( ( eBType )count );
				}

				// if the default behaviour is none then there is nothing else to do. 
				if( _defaultBehaviours[count] == ( int )eBehaviourCore.NONE )
					continue;
				PushBehaviour( _defaultBehaviours[count], ( eBType )count );
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	// if type = NUM_BTYPE then remove everything. otherwise just remove the specific type specified. 
	public void RemoveBehaviour( eBType type = eBType.NUM_BTYPE )
	{
		if( _behaviour == null )
			return;

		for( int count1 = 0; count1 < ( int )eBType.NUM_BTYPE; count1++ )
		{
			if( _behaviour[count1] == null )
				continue;
			if( _behaviour[count1].Count == 0 )
				continue;

			if( ( type == eBType.NUM_BTYPE ) || ( type == ( eBType )count1 ) )
			{
				for( int count2 = _behaviour[count1].Count - 1; count2 >= 0; count2-- )
				{
					if( _behaviour[count1][count2] == null )
						continue;
					_dummyBehaviour = _behaviour[count1][count2];
					_dummyBehaviour.Shutdown();
					_behaviour[count1].RemoveAt( count2 );
					BehavePool._pInstance.FreeBehaviour(
						( int )( _dummyBehaviour._pBehaviourType ), _dummyBehaviour._pIdBehave );
				}

				_dummyBehaviour = null;
				_behaviour[count1].Clear();
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public int GetCurrentBehaviourIdFromType( eBType type )
	{
		if( _behaviour == null )
			return ( int )eBehaviourCore.NONE;
		if( _behaviour.Length <= 0 )
			return ( int )eBehaviourCore.NONE;

		for( int count1 = 0; count1 < ( int )eBType.NUM_BTYPE; count1++ )
		{
			if( ( eBType )count1 != type )
				continue;
			if( _behaviour[count1] == null )
				continue;
			if( _behaviour[count1].Count == 0 )
				continue;
			if( _behaviour[count1][0] == null )
				continue;
			return ( int )_behaviour[count1][0]._pBehaviourType;
		}

		return ( int )eBehaviourCore.NONE;
	}

	//------------------------------------------------------------------------------------------------------------------
	public BehaveBase GetCurrentBehaviourOfType( eBType type )
	{
		if( _behaviour == null )
			return null;
		if( _behaviour.Length <= 0 )
			return null;

		for( int count1 = 0; count1 < ( int )eBType.NUM_BTYPE; count1++ )
		{
			if( ( eBType )count1 != type )
				continue;

			if( _behaviour[count1] == null )
				return null;

			if( _behaviour[count1].Count == 0 )
				return null;

			if( _behaviour[count1][0] == null )
				return null;

			return _behaviour[count1][0];
		}

		return null;
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void InitialiseBehaviours()
	{
		if( _behaviour == null )
		{
		// initialise the behaviour lists. 
			_behaviour = new List<BehaveBase>[( int )eBType.NUM_BTYPE];

			for( int count = 0; count < ( int )eBType.NUM_BTYPE; count++ )
			{
				_behaviour[count] = new List<BehaveBase>();
			}
		}
		else
		{
			// if they already exists clear them. 
			for( int count = 0; count < ( int )eBType.NUM_BTYPE; count++ )
			{
				_behaviour[count].Clear();
			}
		}

		// initialise the default behaviours to no behaviour.  
		_defaultBehaviours = new int[( int )eBType.NUM_BTYPE];
		for( int count = 0; count < ( int )eBType.NUM_BTYPE; count++ )
		{
			_defaultBehaviours[count] = ( int )eBehaviourCore.NONE;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void UpdateDefaultLookDirection()
	{
		if( !_pShouldUpdate )
			return;

		if( _pOwnerActor._pIsMoving )
		{
			// look where we are going. 
			_pOwnerActor._pForward = _ownerActor._pVelocity.normalized;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void UpdateAttacks()
	{
		// attacks are project specific, so there is no way to know what to do here. 

		_attackRequested = 0;
	}

	//------------------------------------------------------------------------------------------------------------------
	private void UpdateTarget()
	{
		if( !_pShouldUpdate )
			return;

		if( _idActorBestTarget != GlobalDefines.INVALID_ID )
		{
			// if we have an id set but the target itself is not valid then clear the target id. 
			ActorDataBase.FindActor( _idActorBestTarget, ref _targetInfo._actorTarget );
			if( ( _targetInfo._actorTarget == null ) || ( !_targetInfo._actorTarget._pIsAlive ) )
			{
				_pIdActorBestTarget = GlobalDefines.INVALID_ID;
			}
		}

		// if we just got a target then do an immediate update of its info. 
		if( !_hadTarget && _pHasTarget )
		{
			UpdateTargetInfo();
		}

		_hadTarget = _pHasTarget;
	}

	//------------------------------------------------------------------------------------------------------------------
	// only need to do this on physics updates. 
	private void UpdateTargetInfo()
	{
		if( !_pShouldUpdate )
			return;
		if( !_pHasTarget )
			return;
		if( _ownerActor == null )
			return;

		_targetInfo._fullyOnScreen = _targetInfo._actorTarget._pIsInCameraViewFull;
		_targetInfo._dummyTarget =
			ActorDummyTarget.DoesActorIdBelongToDummyTarget( _targetInfo._actorTarget._pIdActor );
		_targetInfo._moveDir = _targetInfo._actorTarget._pVelocity;
		_targetInfo._speed = _targetInfo._moveDir.magnitude;
		_targetInfo._moveDir.Normalize();

		_targetInfo._positionFloor = _targetInfo._actorTarget._pPosition;
		_targetInfo._positionTarget = _targetInfo._actorTarget._pTargetPos;
		_targetInfo._forward = _targetInfo._actorTarget._pForward;

		_targetInfo._dirToTarget = _targetInfo._positionTarget - _ownerActor._pLookOrigin;
		_targetInfo._dirToTargetFlat = _targetInfo._dirToTarget;
		_targetInfo._distToTarget = _targetInfo._dirToTarget.magnitude;
		_targetInfo._dirToTarget.Normalize();
		_targetInfo._dirToTargetFlat.y = 0.0f;
		_targetInfo._dirToTargetFlat.Normalize();

		if( _knowledgeSystem != null )
		{	
			Knowledge knowledge = _knowledgeSystem.GetKnowledgeFromTargetId( _targetInfo._actorTarget._pIdActor );
			_targetInfo._knowledgeProjectData = ( knowledge == null ) ? 0 : knowledge._projectData;
		}
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnDrawGizmos()
	{
		if( _ownerActor == null )
			return;

		if( _ownerActor._renderDebugInfoBehaviours )
		{
			if( _behaviour != null )
			{
				for( int count1 = 0; count1 < ( int )eBType.NUM_BTYPE; count1++ )
				{
					if( _behaviour[count1] == null )
						continue;
					if( _behaviour[count1].Count == 0 )
						continue;
					if( _behaviour[count1][0] == null )
						continue;
					_behaviour[count1][0].OnDrawGizmos();
				}
			}
		}

		if( _ownerActor._renderDebugInfoKnowledge )
		{
			if( _knowledgeSystem != null )
			{
				_knowledgeSystem.OnDrawGizmos();
			}
		} 
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnGUI()
	{
		if( _ownerActor == null )
			return;

		if( _ownerActor._renderDebugInfoBehaviours )
		{
			if( _behaviour != null )
			{
				if( _ownerActor == null )
					return;
				Vector2 textPos = new Vector2( 10.0f, Screen.height * 0.5f );
				int width = ( int )( Screen.width * 0.2f );

				GUI.Box( new Rect( ( int )textPos.x, ( int )textPos.y, width, GlobalDefines.DEBUG_TEXT_HIEGHT ),
					"BEHAVIOUR DATA: " );
				textPos.y += GlobalDefines.DEBUG_TEXT_HIEGHT;

				float startYPos = textPos.y;

				for( int count1 = 0; count1 < ( int )eBType.NUM_BTYPE; count1++ )
				{
					if( _behaviour[count1] == null )
						continue;
					if( _behaviour[count1].Count == 0 )
						continue;
					if( _behaviour[count1][0] == null )
						continue;

					GUI.Box( new Rect( ( int )textPos.x, ( int )textPos.y, width, GlobalDefines.DEBUG_TEXT_HIEGHT ),
						( ( eBType )( count1 ) ).ToString() + ":" );
					textPos.y += GlobalDefines.DEBUG_TEXT_HIEGHT;

					_behaviour[count1][0].OnGUI( width, ref textPos );

					textPos.y = startYPos;
					textPos.x += ( float )width;
				}
			}
		}

		if( _ownerActor._renderDebugInfoKnowledge )
		{
			if( _knowledgeSystem != null )
			{
				_knowledgeSystem.OnGUI();
			}
		} 
	}
#endif
}

