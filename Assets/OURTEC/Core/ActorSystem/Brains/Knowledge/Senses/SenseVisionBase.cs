//----------------------------------------------------------------------------------------------------------------------
// SenseVisionBase.cs
// 15/1/13

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class SenseVisionBase
{
	// never needed this implementation. However, this would go through the actors finding a list that is within a
	// vision frustrum of the owner, and periodically do a ray cast to see if they have a LOS. 
}