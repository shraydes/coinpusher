//----------------------------------------------------------------------------------------------------------------------
// Knowledge.cs
// 15/1/13

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using GameDefines;
using KnowledgeSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
public class Knowledge : IComparable<Knowledge>, IEquatable<Knowledge>, IComparable
{
	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	public int _idKnowledge;
	public int _idActorOwner;		// who has this knowledge
	public int _idActorTarget;		// who is this knowledge of. 
	public int _projectData;		// anything the project wants to use it as. 
	public bool _hasUpdated;
	public bool _isDummyTarget;
	public bool _isPlayer;
	public float _age;
	public float _threatValue;
	public float _distanceSqrd;
	public float _lifeTime;
	public SENSE _senseType;
	public Vector3 _position;
	public Vector3 _direction;		// normalised

	//------------------------------------------------------------------------------------------------------------------
	public Knowledge( int idKnowledge, int idActorOwner )
	{
		_idKnowledge = idKnowledge;
		_idActorOwner = idActorOwner;
		Reset();
	}

	//------------------------------------------------------------------------------------------------------------------
	public void Reset()
	{
		_idActorTarget = GlobalDefines.INVALID_ID;
		_projectData = 0;
		_hasUpdated = false;
		_isDummyTarget = false;
		_isPlayer = false;
		_age = 0.0f;
		_threatValue = 0.0f;
		_distanceSqrd = Mathf.Infinity;
		_lifeTime = KnowledgeSystemBase.INFINITE;
		_senseType = SENSE.INVALID;
		_position = Vector3.zero;
		_direction = Vector3.zero;
	}

	//------------------------------------------------------------------------------------------------------------------
	// IEquatable generic implementation. 
	public override bool Equals( object other )
	{
		if( other == null ) return false;
		if( !( other is Knowledge ) ) return false;
		return Equals( ( Knowledge )other );
	}

	//------------------------------------------------------------------------------------------------------------------
	// IEquatable<> implementation. 
	public bool Equals( Knowledge other )
	{
		if( other == null ) return false;
		return ( _idKnowledge == other._idKnowledge );
	}

	//------------------------------------------------------------------------------------------------------------------
	public override int GetHashCode()
	{
		return _idKnowledge.GetHashCode();
	}

	//------------------------------------------------------------------------------------------------------------------
	// IComparable<> implementation
	// +ve if this after other | 0 is equal | -ve if this before other
	public int CompareTo( Knowledge other )
	{
		if( Equals( other ) ) return 0;

		// first order according to threat value. if ours is bigger we come before, return -ve number. 
		if( _threatValue != other._threatValue )
		{
			return ( _threatValue > other._threatValue ) ? -1 : 1;
		}

		// then order according to sense type, if ours is smaller (earlier in the enum) return -ve number. 
		if( _senseType != other._senseType )
		{
			return ( int )( _senseType - other._senseType );
		}

		// then order according to distance. 
		if( _distanceSqrd != other._distanceSqrd )
		{
			return ( _distanceSqrd < other._distanceSqrd ) ? -1 : 1;
		}

		// as a last resort just use the hash of the unique id. 
		return ( GetHashCode() <= other.GetHashCode() ) ? -1 : 1;
	}

	//------------------------------------------------------------------------------------------------------------------
	// IComparable Generic Implementation.
	// +ve if this after other | 0 is equal | -ve if this before other
	public int CompareTo( object other )
	{
		if( ! ( other is Knowledge ) )
		{
			Debug.LogError( "Knowledge::CompareTo - other is not knowledge, result will be inaccurate. other type:" + 
				other.GetType().ToString() );
			return -1;
		}
		return CompareTo( ( Knowledge )other );
	}

	//------------------------------------------------------------------------------------------------------------------
	public static bool operator < ( Knowledge k1, Knowledge k2 )
	{
		return k1.CompareTo( k2 ) < 0;
	}

	//------------------------------------------------------------------------------------------------------------------
	public static bool operator > ( Knowledge k1, Knowledge k2 )
	{
		return k1.CompareTo( k2 ) > 0;
	}

	//------------------------------------------------------------------------------------------------------------------
	public static bool operator <= ( Knowledge k1, Knowledge k2 )
	{
		return k1.CompareTo( k2 ) <= 0;
	}

	//------------------------------------------------------------------------------------------------------------------
	public static bool operator >= ( Knowledge k1, Knowledge k2 )
	{
		return k1.CompareTo( k2 ) >= 0;
	}

	//------------------------------------------------------------------------------------------------------------------
	public static bool operator == ( Knowledge k1, Knowledge k2 )
	{
		// If both are null, or both are same instance, return true.
		if( System.Object.ReferenceEquals( k1, k2 ) ) return true;

		// If one is null, but not both, return false.
		if( ( ( object )k1 == null ) || ( ( object )k2 == null ) ) return false;

		return k1.Equals( k2 );
	}

	//------------------------------------------------------------------------------------------------------------------
	public static bool operator != ( Knowledge k1, Knowledge k2 )
	{
		return !( k1 == k2 );
	}
}