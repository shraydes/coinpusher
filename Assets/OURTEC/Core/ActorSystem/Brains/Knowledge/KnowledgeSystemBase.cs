////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// KnowledgeSystemProject.cs
//
// Created: 15/1/2013 chope
// Contributors: 
// 
// Intention:
// Base functionality for all knowledge system types. 
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using AmuzoActor;
using GameDefines;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
namespace KnowledgeSystem
{
	public enum SENSE       // ordered according to general importance/priority. 
	{
		TOUCH,
		VISION,
		HEARING,

		INVALID
	}

	//------------------------------------------------------------------------------------------------------------------
	// CLASS KnowledgeSystemBase
	public abstract class KnowledgeSystemBase
	{
		//--------------------------------------------------------------------------------------------------------------
		// CONSTS / DEFINES		
		private const string DEBUG_LOG_PREFIX = "KnowledgeSystemBase - ";
		private const float DEBUG_SPHERE_RADIUS = 0.4f;

		public const float INFINITE = -1.0f;

		private const int MEM_POOL_SIZE_INITIAL = 10;
		private const float ITERATION_TIME_FIND_KNOWLEDGE = 1.5f;
		private const float ITERATION_TIME_UPDATE_KNOWLEDGE = 0.65f;
		private const float RANGE_SIGHT_DEFAULT = INFINITE;
		private const float TIME_MEMORY_DEFAULT = INFINITE;

		private const float SCORE_START_DEFAULT = 300.0f;
		private const float SCORE_MULTIPLIER_HEIGHT_DEFAULT = 1.2f;

		//--------------------------------------------------------------------------------------------------------------
		// FIELDS
		private static int _nextKnowledgeId = 0;
		
		protected float _sightRangeSqrd = RANGE_SIGHT_DEFAULT;
		protected float _memoryTime = TIME_MEMORY_DEFAULT;
		protected float _timeUpdateKnowledgePeriod = ITERATION_TIME_UPDATE_KNOWLEDGE;
		protected float _timerUpdateKnowledge;
		protected float _timeFindKnowledgePeriod = ITERATION_TIME_FIND_KNOWLEDGE;
		protected float _timerFindKnowledge;
		protected Vector3 _dummyVector;
		protected ActorLogicBase _dummyActor = null;
		protected Knowledge _dummyKnowledge = null;
		protected ActorLogicBase _actorOwner;
		protected BrainActorBase _brainOwner;
		protected List<Knowledge> _knowledge;
		protected ActorDummyTarget _dummyTarget = null;

		private List<Knowledge> _knowledgeUnused;

		//--------------------------------------------------------------------------------------------------------------
		// PROPERTIES
		public bool _pHasKnowledge
		{
			get
			{
				return ( _knowledge != null ) && ( _knowledge.Count > 0 );
			}
		}

		public List<Knowledge> _pKnowledgeList
		{
			get
			{
				return _knowledge;
			}
		}

		protected virtual float _pScoreStarting
		{
			get
			{
				return SCORE_START_DEFAULT;
			}
		}

		protected virtual float _pScoreMultiplierHeight
		{
			get
			{
				return SCORE_MULTIPLIER_HEIGHT_DEFAULT;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// ABSTRACTS
		protected abstract bool ValidateKnowledgeProject( Knowledge knowledge );

		//--------------------------------------------------------------------------------------------------------------
		static protected int GetNewKnowledgeId()
		{
			return _nextKnowledgeId++;
		}

		//--------------------------------------------------------------------------------------------------------------
		public virtual void Initialise( ActorLogicBase actorOwner, BrainActorBase brainOwner )
		{
			_actorOwner = actorOwner;
			if( _actorOwner == null )
				return;

			_brainOwner = brainOwner;
			if( _brainOwner == null )
				return;

			// start us off with a random time till new knowlege to stagger the updates if lots created at once. 
			_timerFindKnowledge = Random.Range( 0.0f, _timeFindKnowledgePeriod );
			_timerUpdateKnowledge = Random.Range( 0.0f, _timeUpdateKnowledgePeriod );

			_knowledge = new List<Knowledge>( MEM_POOL_SIZE_INITIAL );
			_knowledgeUnused = new List<Knowledge>( MEM_POOL_SIZE_INITIAL );

			// allocate all of the knowledge now too, get it out of the way. 
			for( int count = 0; count < MEM_POOL_SIZE_INITIAL; count++ )
			{
				_dummyKnowledge = new Knowledge( KnowledgeSystemBase.GetNewKnowledgeId(), _actorOwner._pIdActor );
				_knowledgeUnused.Add( _dummyKnowledge );
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		public virtual void Update()
		{
			if( _actorOwner == null )
				return;
			if( !_actorOwner._pIsAlive )
				return;
			if( _brainOwner == null )
				return;

			_timerFindKnowledge -= Time.deltaTime;
			_timerUpdateKnowledge -= Time.deltaTime;

			if( _timerFindKnowledge <= 0.0f )
			{
				_timerFindKnowledge = _timeFindKnowledgePeriod;
				FindNewKnowledge();
			}

			UpdateExistingKnowledge();

			IssueBestThreat();
		}

		//--------------------------------------------------------------------------------------------------------------
		public Knowledge GetKnowledgeFromTargetId( int idActorTarget )
		{
			if( _knowledge == null )
				return null;

			for( int count = 0; count < _knowledge.Count; count++ )
			{
				if( _knowledge[count] == null )
					continue;

				if( _knowledge[count]._idActorTarget == idActorTarget )
					return _knowledge[count];
			}

			return null;
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void IssueBestThreat()
		{
			if( _brainOwner == null )
				return;

			if( ( _actorOwner != null ) && ( _actorOwner._pSquad != null ) )
			{
				// if we have a squad give them what we know and let them tell us who to target
				_brainOwner._pIdActorBestTarget = _brainOwner._pOwnerActor._pSquad.IssueBestTargetFromCollection(
					ref _knowledge, _actorOwner._pIdActor );
			}
			else if( ( _knowledge == null ) || ( _knowledge.Count == 0 ) )
			{
				_brainOwner._pIdActorBestTarget = GlobalDefines.INVALID_ID;
			}
			else
			{
				// otherwise our knowledge is already ordered to our individual scoring, take the top one. 
				_brainOwner._pIdActorBestTarget = _knowledge[0]._idActorTarget;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void FindNewKnowledge()
		{
			if( _actorOwner == null )
				return;
			if( _brainOwner == null )
				return;

			for( int count = 0; count < ActorDataBase._pActorIds.Length; count++ )
			{
				_dummyActor = ActorDataBase._pActorDict[ActorDataBase._pActorIds[count]];
				if( _dummyActor == null )
					continue;
				if( !_dummyActor._pIsAlive )
					continue;
				if( !_dummyActor._pIsTargetable )
					continue;
				if( _dummyActor._pIdActor == _actorOwner._pIdActor )
					continue;
				if( _dummyActor._pBrain == null )
					continue;
				if( _dummyActor._pBrain._pAllegiance == eAllegiance.NEUTRAL )
					continue;
				if( _dummyActor._pBrain._pAllegiance == _brainOwner._pAllegiance )
					continue;
				if( _dummyActor._pIsChild )
					continue;

				bool isDummyTarget = ( _dummyActor.GetType() == typeof( ActorDummyTarget ) );

				// NOTE, this is where the sences should be checked if we are not just going by our onscreen status. 

				// players must only be attacked by those onscreen
				if( _dummyActor._pIsPlayer )
				{
					// if they are not on the screen then ignore them.
					if( !_dummyActor._pIsInCameraViewPart )
						continue;

					// if we are not in the screen then ignore them. 
					if( !_actorOwner._pIsInCameraViewPart )
						continue;
				}

				_dummyVector = _dummyActor._pTargetPos - _actorOwner._pTargetPos;
				float distanceSqrd = _dummyVector.sqrMagnitude;

				// check the dummy target effective range. 
				if( isDummyTarget )
				{
					if( distanceSqrd > ( ( ( ActorDummyTarget )_dummyActor )._pEffectiveRange ) *
						( ( ActorDummyTarget )_dummyActor )._pEffectiveRange )
						continue;
				}

				// if they are too far away ignore them. 
				if( ( _sightRangeSqrd != INFINITE ) && ( distanceSqrd > _sightRangeSqrd ) )
					continue;

				// check to see if we already have knowledge about them. 
				if( GetExistingKnowledge( _dummyActor._pIdActor, ref _dummyKnowledge ) )
				{
					// if so just reset the age. 
					_dummyKnowledge._age = 0.0f;
					continue;
				}

				// otherwise we need to add new knowledge for them.
				GetOrCreateNewKnowledge( ref _dummyKnowledge );
				if( _dummyKnowledge == null )
					continue;

				// set up the data that is not updated in the normal knowledge update. 
				_dummyKnowledge._senseType = SENSE.VISION;
				_dummyKnowledge._idActorTarget = _dummyActor._pIdActor;
				_dummyKnowledge._lifeTime = _memoryTime;
				_dummyKnowledge._isDummyTarget = isDummyTarget;
				_dummyKnowledge._distanceSqrd = distanceSqrd;
				_dummyKnowledge._isPlayer = _dummyActor._pIsPlayer;
				_dummyKnowledge._position = _dummyActor._pPosition;
				_dummyKnowledge._direction = ( _dummyActor._pPosition - _actorOwner._pPosition ).normalized;

				ScoreKnowledge( ref _dummyKnowledge );

				AddKnowledge( ref _dummyKnowledge );
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void UpdateExistingKnowledge()
		{
			int numKnowledges = _knowledge.Count;
			if( numKnowledges == 0 )
				return;

			bool updateData = ( _timerUpdateKnowledge <= 0.0f );
			if( updateData )
			{
				_timerUpdateKnowledge = _timeUpdateKnowledgePeriod;
			}

			for( int count = numKnowledges - 1; count >= 0; count-- )
			{
				_dummyKnowledge = _knowledge[count];

				_dummyKnowledge._hasUpdated = true;

				// update the age of the knowledge
				_dummyKnowledge._age += Time.deltaTime;

				// if we cant find the target actor remove it from the knowledge. 
				if( !ActorDataBase.FindActor( _dummyKnowledge._idActorTarget, ref _dummyActor ) )
				{
					RemoveKnowledge( count );
					continue;
				}

				// if we found it but its no longer valid remove it. 
				if( ( _dummyActor == null ) || !_dummyActor._pIsAlive || !_dummyActor._pIsTargetable )
				{
					RemoveKnowledge( count );
					continue;
				}

				// if the allegiance of the threat has changed to be one we are not worries about remove the knowledge. 
				if( ( _dummyActor._pAllegiance == eAllegiance.NEUTRAL ) || 
					( _dummyActor._pAllegiance == _brainOwner._pAllegiance ) )
				{
					RemoveKnowledge( count );
					continue;
				}

				// if its too old remove it. 
				if( ( _dummyKnowledge._lifeTime != INFINITE ) &&
					( _dummyKnowledge._age > _dummyKnowledge._lifeTime ) )
				{
					RemoveKnowledge( count );
					continue;
				}

				// project side validation. 
				if( !ValidateKnowledgeProject( _dummyKnowledge ) )
				{
					RemoveKnowledge( count );
					continue;
				}

				if( updateData )
				{
					// update the data about it if its still valid
					_dummyKnowledge._position = _dummyActor._pPosition;
					_dummyKnowledge._direction = _dummyActor._pPosition - _actorOwner._pPosition;
					_dummyKnowledge._distanceSqrd = _knowledge[count]._direction.sqrMagnitude;
					_dummyKnowledge._direction.Normalize();
					_dummyKnowledge._isPlayer = _dummyActor._pIsPlayer;

					// check the dummy target effective range. 
					if( _dummyKnowledge._isDummyTarget )
					{
						if( _dummyKnowledge._distanceSqrd > ( ( ( ActorDummyTarget )_dummyActor )._pEffectiveRange ) *
							( ( ActorDummyTarget )_dummyActor )._pEffectiveRange )
						{
							RemoveKnowledge( count );
							continue;
						}
					}

					ScoreKnowledge( ref _dummyKnowledge );
				}
			}

			//now sort the knowledge into threat value order. 
			_knowledge.Sort();
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void ScoreKnowledge( ref Knowledge knowledge )
		{
			float touchScore = 15.0f;

			knowledge._threatValue = _pScoreStarting - Mathf.Sqrt( knowledge._distanceSqrd );

			if( knowledge._senseType == SENSE.TOUCH )
			{
				knowledge._threatValue += touchScore;
			}

			// take score away the further they are from our height. (prioritise those on your tier). 
			knowledge._threatValue -= ( Mathf.Abs( knowledge._direction.y ) * _pScoreMultiplierHeight );

			// if this is a dummy target add on the value for its priority. 
			if( ActorDummyTarget.FindActorDummyTarget( knowledge._idActorTarget, ref _dummyTarget ) )
			{
				knowledge._threatValue += ( float )_dummyTarget._pPriorityScore;
				_dummyTarget = null;
			}

			knowledge._threatValue += _brainOwner.ScoreKnowledgeBrainSpecific( ref knowledge );
		}

		//--------------------------------------------------------------------------------------------------------------
		public virtual void Shutdown()
		{
			// clear out and de-allocate the knowledge list 
			int numKnowledges = _knowledge.Count;
			for( int count = 0; count < numKnowledges; count++ )
			{
				if( _knowledge[count] == null )
					continue;
				_knowledge[count].Reset();
			}
			_knowledge.Clear();
			_knowledge = null;

			// clear and de-allocate the unused knowledge list. 
			numKnowledges = _knowledgeUnused.Count;
			for( int count = 0; count < numKnowledges; count++ )
			{
				if( _knowledgeUnused[count] == null )
					continue;
				_knowledgeUnused[count].Reset();
			}
			_knowledgeUnused.Clear();
			_knowledgeUnused = null;
		}

		//--------------------------------------------------------------------------------------------------------------
		public virtual void ClearAllKnowledge()
		{
			if( _knowledge == null )
				return;

			while( _knowledge.Count > 0 )
			{
				_dummyKnowledge = _knowledge[0];
				RemoveKnowledge( ref _dummyKnowledge );
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected bool GetExistingKnowledge( int idActorTarget, ref Knowledge knowledge )
		{
			foreach( Knowledge currentKnowledge in _knowledge )
			{
				if( currentKnowledge == null )
					continue;
				if( currentKnowledge._idActorTarget == idActorTarget )
				{
					knowledge = currentKnowledge;
					return true;
				}
			}
			return false;
		}

		//--------------------------------------------------------------------------------------------------------------
		// funciton returns a knowledge in the _knowledgeUnused list. if this is not one it adds one. 
		protected void GetOrCreateNewKnowledge( ref Knowledge newKnowledge )
		{
			if( _actorOwner == null )
				return;

			if( _knowledgeUnused.Count > 0 )
			{
				newKnowledge = _knowledgeUnused[0];
				return;
			}

			newKnowledge = new Knowledge( GetNewKnowledgeId(), _actorOwner._pIdActor );
			_knowledgeUnused.Add( newKnowledge );
			return;
		}

		//--------------------------------------------------------------------------------------------------------------
		protected void AddKnowledge( ref Knowledge knowledge )
		{
			_knowledgeUnused.Remove( knowledge );
			_knowledge.Add( knowledge );
		}

		//--------------------------------------------------------------------------------------------------------------
		protected void RemoveKnowledge( ref Knowledge knowledge )
		{
			knowledge.Reset();
			_knowledgeUnused.Add( knowledge );
			_knowledge.Remove( knowledge );
		}

		//--------------------------------------------------------------------------------------------------------------
		protected void RemoveKnowledge( int listIndex )
		{
			if( listIndex < _knowledge.Count )
			{
				_knowledge[listIndex].Reset();
				_knowledgeUnused.Add( _knowledge[listIndex] );
				_knowledge.RemoveAt( listIndex );
			}
		}

#if UNITY_EDITOR
		//--------------------------------------------------------------------------------------------------------------
		public virtual void OnDrawGizmos()
		{
			if( _knowledge == null )
				return;

			for( int count = 0; count < _knowledge.Count; count++ )
			{
				if( _knowledge[count] == null )
					continue;

				Gizmos.color = ( _knowledge[count]._idActorTarget == _brainOwner._pIdActorBestTarget ) ?
					Color.red : Color.magenta;

				_dummyVector = _knowledge[count]._position;

				if( _dummyVector == Vector3.zero )
				{
					int z;
					z = 0;
				}

				Gizmos.DrawSphere( _knowledge[count]._position, DEBUG_SPHERE_RADIUS );
				Gizmos.DrawLine( _knowledge[count]._position, _brainOwner._pOurPos );

				_dummyVector.y += 1.0f;
				UnityEditor.Handles.color = Color.gray;
				UnityEditor.Handles.Label( _dummyVector, _knowledge[count]._threatValue.ToString() );
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		public virtual void OnGUI()
		{
		}
#endif
	}
}

