﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// ActorMessagesBase.cs
//
// Created: 16/11/17 chope 
// Contributors: 
// 
// Intention: Mp actor content, base functionality. 
//
// Notes: Should be a catch all interface that handles everything even if the multiplayer package is not in the project. 
//			If project does not have Multiplayer support the Messages class should not be needed apart from this base.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace AmuzoActor
{
	//------------------------------------------------------------------------------------------------------------------
	// INCLUDES
	using GameDefines;
	using UnityEngine;

#if PUMA_DEFINE_MULTIPLAYER
	using Photon;

	//------------------------------------------------------------------------------------------------------------------
	// CLASS ActorMessagesBase
	public class ActorMessagesBase : PunBehaviourAmuzo
#else
	public class ActorMessagesBase : MonoBehaviour
#endif
	{
		//--------------------------------------------------------------------------------------------------------------
		// FIELDS
		private ActorDataBase _actorData;
		private ActorVisualBase _actorVis;
		private ActorLogicBase _actorLogic;

		//--------------------------------------------------------------------------------------------------------------
		// PROPERTIES
		public string _pNickName
		{
			get
			{
				if( GlobalDefines._pProjectHasMpSupport )
				{
#if PUMA_DEFINE_MULTIPLAYER
					return photonView.owner.NickName;
#endif
				}

				return GlobalDefines.EMPTY_STRING;
			}
		}

		public bool _pIsLocalActor
		{
			get
			{
				if( GlobalDefines._pProjectHasMpSupport )
				{
#if PUMA_DEFINE_MULTIPLAYER
					return photonView.isMine;
#endif
				}

				// no MP support means everyone is local
				return true;
			}
		}

		public bool _pIsOnHostMachine
		{
			get
			{
				if( GlobalDefines._pProjectHasMpSupport )
				{
#if PUMA_DEFINE_MULTIPLAYER
					return PhotonNetwork.isMasterClient;
#endif
				}

				// no MP support means everyone is local
				return true;
			}
		}

		public virtual bool _pIsSceneOwnedObject
		{
			get
			{
#if PUMA_DEFINE_MULTIPLAYER
				return photonView.isSceneView;
#else
				return false;
#endif
			}
		}

		public int _pIdActor
		{
			get
			{
				if( _actorLogic == null )
					return GlobalDefines.INVALID_ID;

				return _actorLogic._pIdActor;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
#if PUMA_DEFINE_MULTIPLAYER
		protected override void Awake()
		{
			base.Awake();
#else
		protected virtual void Awake()
		{
#endif
			_actorVis = GetComponent<ActorVisualBase>();
			_actorData = GetComponent<ActorDataBase>();
			_actorLogic = GetComponent<ActorLogicBase>();
		}

#if PUMA_DEFINE_MULTIPLAYER
		//------------------------------------------------------------------------------------------------------------------
		protected override void SendUpdateMessage( PhotonStream stream, PhotonMessageInfo info )
		{
			// 1 send flags
			stream.SendNext( ( _actorData != null ) ? ( int )_actorData._pFlagsBase : 0 );
		}																	
#endif

#if PUMA_DEFINE_MULTIPLAYER
		//------------------------------------------------------------------------------------------------------------------
		protected override void ReadUpdateMessage( PhotonStream stream, PhotonMessageInfo info )
		{
			// 1 read flags. 
			int flags = ( int )stream.ReceiveNext();
			if( _actorData != null )
			{
				_actorData._pFlagsBase = ( eFlagsActorBase )flags;

				// keep our alive flag on the logic up to date on remote instances. 
				if( _actorLogic != null )
				{
					_actorLogic._pIsAlive = _actorData.CheckFlag( eFlagsActorBase.IS_ALIVE );
					_actorLogic._pIsInUse = _actorData.CheckFlag( eFlagsActorBase.IN_USE );
				}
			}
		}
#endif

		//--------------------------------------------------------------------------------------------------------------
		public void RequestMove( uint hashId, bool abortCurrentIfPoss = true, bool queueOtherwise = true )
		{
#if PUMA_DEFINE_MULTIPLAYER
			MessageSend_AllClients_PlayMove( hashId, abortCurrentIfPoss, queueOtherwise );
#else
			PlayMove( hashId, abortCurrentIfPoss, queueOtherwise );
#endif
		}
		
		//--------------------------------------------------------------------------------------------------------------
		private void PlayMove( uint hashId, bool abortCurrentIfPoss, bool queueOtherwise )
		{
			if( _actorVis != null )
			{
				if( _actorVis._pMovementSystem != null )
				{
					_actorVis._pMovementSystem.PlayMove( hashId, abortCurrentIfPoss, queueOtherwise );
				}
			}
		}

#if PUMA_DEFINE_MULTIPLAYER
		//--------------------------------------------------------------------------------------------------------------
		// MESSAGES
		// Naming convention:
		// 1. Message send or Reveive
		// 2. On send, Who its sent to. On reveive, who it was from
		// 3. funtion of the message. 

		//-------------------------------------------------------------------------------------------------------------- 
		public virtual void MessageSend_ActorAll_TakeDamage( float amount, int damagerActorId, int damageType )
		{
			if( photonView != null )
			{
				if( _pIsSceneOwnedObject )
				{	
					photonView.RPC( "MessageReceive_ActorSceneObjectdAny_TakeDamage", PhotonTargets.All,
						new object[] { amount, damagerActorId, damageType } );
				}
				else
				{
					photonView.RPC( "MessageReceive_ActorPlayerOwnedAny_TakeDamage", PhotonTargets.All,
						new object[] { amount, damagerActorId, damageType } );
				}
			}
		}

		[PunRPC]
		public virtual void MessageReceive_ActorSceneObjectdAny_TakeDamage( 
			float amount, int damagerActorId, int damageType )
		{ 
			if( _actorLogic != null )
			{
				// fire damage effects on all machines. 
				if( _actorVis != null )
					_actorVis.TakeDamage( amount, _actorLogic._pTargetPos, 
						_actorLogic._pTargetRot.eulerAngles, damageType, damagerActorId );

				// only handle the loss of life on the master client machine. 
				if( _pIsOnHostMachine )
					_actorLogic.TakeDamage( amount, _actorLogic._pTargetPos, 
						_actorLogic._pTargetRot.eulerAngles, damagerActorId, false, damageType );
			}
		}

		[PunRPC]
		public virtual void MessageReceive_ActorPlayerOwnedAny_TakeDamage( 
			float amount, int damagerActorId, int damageType )
		{
			if( _actorLogic != null )
			{
				// only handle the loss of life on the master client machine. 
				if( _pIsLocalActor )
					_actorLogic.TakeDamage( amount, _actorLogic._pTargetPos, 
						_actorLogic._pTargetRot.eulerAngles, damagerActorId, false, damageType );

				// but fire the effects on all machines. 
				if( _actorVis != null )
					_actorVis.TakeDamage( amount, _actorLogic._pTargetPos, 
						_actorLogic._pTargetRot.eulerAngles, damageType, damagerActorId );
			}
		}

		//-------------------------------------------------------------------------------------------------------------- 
		public virtual void MessageSend_ActorAll_OnDeath( int killerActorId )
		{
			if( photonView != null )
			{
				if( _pIsSceneOwnedObject )
				{	
					photonView.RPC( "MessageReceive_ActorSceneObjectdAny_OnDeath", PhotonTargets.AllBufferedViaServer,
						new object[] { killerActorId } );
				}
				else
				{
					photonView.RPC( "MessageReceive_ActorPlayerOwnedAny_OnDeath", PhotonTargets.AllBufferedViaServer,
						new object[] { killerActorId } );
				}
			}
		}

		[PunRPC]
		public virtual void MessageReceive_ActorSceneObjectdAny_OnDeath( int killerActorId )
		{ 
			if( _actorLogic != null )
			{
				// only handle the loss of life on the master client machine. 
				if( _pIsOnHostMachine )
					_actorLogic.OnDeath( killerId: killerActorId );

				// but fire the effects on all machines. 
				if( _actorVis != null )
					_actorVis.OnDeath( killerId: killerActorId );
			}
		}

		[PunRPC]
		public virtual void MessageReceive_ActorPlayerOwnedAny_OnDeath(  int killerActorId  )
		{
			if( _actorLogic != null )
			{
				// only handle the loss of life on the master client machine. 
				if( _pIsLocalActor )
					_actorLogic.OnDeath( killerId: killerActorId );

				// but fire the effects on all machines. 
				if( _actorVis != null )
					_actorVis.OnDeath( killerId: killerActorId );
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		public void MessageSend_AllClients_PlayMove( uint hashId, bool abortCurrentIfPoss, bool queueOtherwise )
		{
			if( photonView != null )
			{
				photonView.RPC( "MessageReceive_ClientHost_PlayMove", PhotonTargets.AllViaServer,
					new object[] { ( int )hashId, abortCurrentIfPoss, queueOtherwise } );
			}
		}

		[PunRPC]
		public void MessageReceive_ClientHost_PlayMove( int hashId, bool abortCurrentIfPoss, bool queueOtherwise )
		{
			PlayMove( ( uint )hashId, abortCurrentIfPoss, queueOtherwise );
		}
#endif
	}
}
