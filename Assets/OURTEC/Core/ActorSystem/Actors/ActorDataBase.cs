﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// ActorDataBase.cs
//
// Created: 02/11/17 chope 
// Contributors: 
// 
// Intention: An actor is an entity with the concept of movement and/or health
//				ActorData is the single scritp ont he actor that inherits form the blueprint behaviour
//				So acts as a one stop shop for all data, references by all the other actor components. 
//				ALSO, this is the static lookup location for any actor component, based on actor ID. 
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace AmuzoActor
{
	//------------------------------------------------------------------------------------------------------------------
	// INCLUDES
	using DataBlueprints;
	using GameDefines;
	using System;
	using System.Collections.Generic;
	using UnityEngine;

	[Flags]
	public enum eFlagsActorBase
	{
		IN_USE = 1 << 0,
		IS_ALIVE = 1 << 1
	}

	//------------------------------------------------------------------------------------------------------------------
	// CLASS ActorDataBase
	public class ActorDataBase : DataBlueprintBehaviour
	{
		//--------------------------------------------------------------------------------------------------------------
		// CONSTS / DEFINES		
		protected const float HEALTH_MAX_DEFAULT = 100.0f;

		//--------------------------------------------------------------------------------------------------------------
		// FIELDS
		private static int _nextIdActor = 0;
		private static bool _actorListDirty = false;
		private static int[] _actorsIds = null;
		private static Dictionary<int, ActorLogicBase> _actorsDict = new Dictionary<int, ActorLogicBase>();
		private static ActorLogicBase _dummyActor;

		#pragma warning disable 0649
		protected float _healthMaxBase;
		protected string _moveSystType;
		#pragma warning restore 0649

		protected int _idActor = GlobalDefines.INVALID_ID;
		protected ActorLogicBase _actorLogicBase;
		protected ActorMessagesBase _actorMsgBase;

		private eFlagsActorBase _flags = 0;

		//--------------------------------------------------------------------------------------------------------------
		// PROPERTIES
		public static int[] _pActorIds
		{
			get
			{
				if( _actorListDirty || ( _actorsIds == null ) )
				{
					_actorListDirty = false;
					_actorsIds = new int[_actorsDict.Count];
					_actorsDict.Keys.CopyTo( _actorsIds, 0 );
				}
				return _actorsIds;
			}
		}

		public static Dictionary<int, ActorLogicBase> _pActorDict
		{
			get
			{
				return _actorsDict;
			}
		}

		public eFlagsActorBase _pFlagsBase
		{
			get
			{
				return _flags;
			}
			set
			{
				_flags = value;
			}
		}

		public int _pIdActor
		{
			get
			{
				return _idActor;
			}
		}

		public virtual float _pHealthMaxBase
		{
			get
			{
				return _healthMaxBase;
			}
		}

		public string _pMoveSystClassName
		{
			get
			{
				return _moveSystType;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// for actors created in game time.... not placed in editor time. 
		public static ActorLogicBase CreateActor( string prefabName, Vector3 position, DataBlueprint bp = null )
		{
			return CreateActor( prefabName, position, Quaternion.identity, bp );
		}

		//--------------------------------------------------------------------------------------------------------------
		// for actors created in game time.... not placed in editor time. 
		// at base level this instantiates prefabs, projects should make version of this taht uses pool systems. 
		public static ActorLogicBase CreateActor( 
			string prefabName, Vector3 position, Quaternion rotation, DataBlueprint bp = null )
		{
			if( string.IsNullOrEmpty( prefabName ) )
				return null;

			ActorLogicBase newActor = null;
			GameObject newObj = Instantiate( Resources.Load( prefabName ) ) as GameObject;

			if( newObj != null )
			{
				newActor = newObj.GetComponent<ActorLogicBase>();

				if( newActor == null )
				{
					// if it doesnt have an actor script then its not a valid object, get rid of it. 
					Destroy( newObj );
				}
				else
				{
					newActor._pPosition = position;
					newActor._pForward = rotation * Vector3.forward;

					// if a specific Bp has been passed, assign it. 
					if( bp != null )
					{
						newActor._pDataBlueprint = bp;
					}

					newActor.gameObject.SetActive( true );
				}
			}

			return newActor;
		}

		//--------------------------------------------------------------------------------------------------------------
		public static void DestroyActor( int idActor )
		{
			if( FindActor( idActor, ref _dummyActor ) )
			{
#if PUMA_DEFINE_MULTIPLAYER
				if( _dummyActor._pActorMessages._pIsSceneOwnedObject )
				{
					if( _dummyActor._pIsOnHostMachine )
						PhotonNetwork.Destroy( _dummyActor._pActorMessages.photonView );
				}
				else
				{
					if( _dummyActor._pIsLocalActor )
						PhotonNetwork.Destroy( _dummyActor._pActorMessages.photonView );
				}
#else
				Destroy( _dummyActor.gameObject );
#endif
			}

			_dummyActor = null;
		}

		//--------------------------------------------------------------------------------------------------------------
		// get a reference to an actor with a given UID. 
		public static bool FindActor( int idActor, ref ActorLogicBase foundActor )
		{
			foundActor = null;

			if( _pActorDict == null )
				return false;

			if( _pActorDict.Count == 0 )
				return false;

			if( !_pActorDict.ContainsKey( idActor ) )
				return false;

			foundActor = _pActorDict[idActor];
			return true;
		}

		//--------------------------------------------------------------------------------------------------------------
		// get a reference to an actor with a given UID. 
		public static bool ActorExists( int idActor )
		{
			if( _pActorDict == null )
				return false;

			if( _pActorDict.Count == 0 )
				return false;

			return _pActorDict.ContainsKey( idActor );
		}

		//--------------------------------------------------------------------------------------------------------------
		protected override void Awake()
		{
			base.Awake();

			_actorLogicBase = GetComponent<ActorLogicBase>();
			_actorMsgBase = GetComponent<ActorMessagesBase>();

#if PUMA_DEFINE_MULTIPLAYER
			if( NetworkConnectionManager._pProjectHasMpSupport && ( _actorMsgBase != null ) )
			{
				// if this is a networked game use the network id as the actor id. 
				_idActor = _actorMsgBase._pIdNetwork;
			}
			else if( _idActor == GlobalDefines.INVALID_ID )
			{
				_idActor = _nextIdActor++;
			}

			_actorsDict.Add( _idActor, _actorLogicBase );
			_actorListDirty = true;
#else
			// add us to the static dictionary of all actors for fast reference. 
			if( _idActor == GlobalDefines.INVALID_ID )
			{
				_idActor = _nextIdActor++;
				_actorsDict.Add( _idActor, _actorLogicBase );
				_actorListDirty = true;
			}
#endif
		}

		//--------------------------------------------------------------------------------------------------------------
		protected override void Start()
		{
			base.Start();  
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void OnDestroy()
		{
			// remove from actors list. 
			_actorsDict.Remove( _idActor );
			_idActor = GlobalDefines.INVALID_ID;
			_actorListDirty = true;
		}

		//------------------------------------------------------------------------------------------------------------------
		public bool CheckFlag( eFlagsActorBase flag )
		{
			return ( ( _flags & flag ) != 0 );
		}

		//------------------------------------------------------------------------------------------------------------------
		public void SetFlag( eFlagsActorBase flag, bool set )
		{
			if( CheckFlag( flag ) != set )
			{
				ToggleFlag( flag );
			}
		}

		//------------------------------------------------------------------------------------------------------------------
		private void ToggleFlag( eFlagsActorBase flag )
		{
			_flags ^= flag;
		}
	}
}