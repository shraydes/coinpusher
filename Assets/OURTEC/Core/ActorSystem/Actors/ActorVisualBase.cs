////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// ActorVisualBase.cs
//
// Created: 02/11/17 chope 
// Contributors: 
// 
// Intention: An actor is an entity with the concept of movement and/or health
//				ActorVisual handles operations that need to be carried out on the actor on all local AND remote clients
//				Visual elements, animations. 
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace AmuzoActor
{
	//------------------------------------------------------------------------------------------------------------------
	// INCLUDES
	using GameDefines;
	using UnityEngine;

	//------------------------------------------------------------------------------------------------------------------
	// CLASS ActorVisualBase
	public class ActorVisualBase : MonoBehaviour
	{
		//--------------------------------------------------------------------------------------------------------------
		// CONSTS / DEFINES		
		private const string DEBUG_LOG_PREFIX = "ActorVisualBase - ";

		//--------------------------------------------------------------------------------------------------------------
		// FIELDS
		public bool _renderDebugInfoMoves = false;
		public string[] _prefabsOnDeath;

		protected bool _firstUpdate;
		protected bool _isInCameraViewPart = false;
		protected bool _isInCameraViewFull = false;
		protected float _speedCurrent;
		protected Vector3 _actorLastForward;
		protected Vector3 _actorLastPos;
		protected Vector3 _actorVelocity;
		protected Vector3 _actorMoveDir;
		protected Vector3 _dummyVec1;
		protected Vector3 _dummyVec2;
		protected ActorDataBase _actorData;
		protected ActorLogicBase _actorLogic;
		protected ActorMessagesBase _actorMsgs;
		protected MovementSystemBase _movementSystem = null;

		//--------------------------------------------------------------------------------------------------------------
		// PROPERTIES
		public int _pIdActor
		{
			get
			{
				return _actorLogic._pIdActor;
			}
		}

		public virtual bool _pIsMoving
		{
			get
			{
				return !GlobalDefines.IsApproximately( _actorVelocity, Vector3.zero, 0.00001f );
			}
		}

		public virtual float _pSpeed
		{
			get
			{
				return _speedCurrent;
			}
		}

		public virtual Vector3 _pVelocity
		{
			get
			{
				return _actorVelocity;
			}
		}

		public virtual Vector3 _pDirMove
		{
			get
			{
				return _actorMoveDir;
			}
		}

		public bool _pIsInCameraViewPart
		{
			get
			{
				return _isInCameraViewPart;
			}
		}

		public bool _pIsInCameraViewFull
		{
			get
			{
				return _isInCameraViewFull;
			}
		}

		public virtual Vector3 _pLookOrigin
		{
			get
			{
				return _actorLogic._pLookOrigin;
			}
		}

		public virtual Vector3 _pPosition
		{
			get
			{
				return transform.position;
			}
			set
			{
				transform.position = value;
			}
		}

		public virtual Vector3 _pForward
		{
			get
			{
				return transform.forward;
			}
			set
			{
				transform.forward = value;
			}
		}

		public virtual Vector3 _pRight
		{
			get
			{
				return transform.right;
			}
			set
			{
				transform.right = value;
			}
		}

		public virtual Vector3 _pUp
		{
			get
			{
				return transform.up;
			}
			set
			{
				transform.up = value;
			}
		}

		// position of a transform attached to the actor that others use to aim/look at, usually in the 'chest' or 'face'
		public Vector3 _pTargetPos
		{
			get
			{
				return _actorLogic._pTargetPos;
			}
		}

		// rotation of the target transform. 
		public Quaternion _pTargetRot
		{
			get
			{
				return _actorLogic._pTargetRot;
			}
		}

		// the target transfom
		public Transform _pTargetTrans
		{
			get
			{
				return _actorLogic._pTargetTrans;
			}
		}

		public bool _pIsLocalActor
		{
			get
			{
				if( _actorMsgs != null )
				{
					return _actorMsgs._pIsLocalActor;
				}
				return true;
			}
		}

		public bool _pIsOnHostMachine
		{
			get
			{
				if( _actorMsgs != null )
				{
					return _actorMsgs._pIsOnHostMachine;
				}

				// no MP means we are master. 
				return true;
			}
		}
		
		public ActorLogicBase _pActorLogic
		{
			get
			{
				return _actorLogic;
			}
		}

		public MovementSystemBase _pMovementSystem
		{
			get
			{
				return _movementSystem;
			}
		}

		public virtual bool _pIsAlive
		{
			get
			{
				if( _actorData != null )
					return _actorData.CheckFlag( eFlagsActorBase.IS_ALIVE );
				return true;
			}
		}

		protected bool _pHasHandledFirstNetworkUpdate
		{
			get
			{
#if PUMA_DEFINE_MULTIPLAYER
				if( _actorMsgs != null )
				{
					return _actorMsgs._pHasHandledFirstNetworkUpdate;
				}
#endif
				return true;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// DELEGATES / EVENTS

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void Awake()
		{
			_actorLogic = GetComponent<ActorLogicBase>();
			_actorData = GetComponent<ActorDataBase>();
			_actorMsgs = GetComponent<ActorMessagesBase>();

			// create our movement system if we want to. 
			CreateMovementSystem();

			if( _movementSystem != null )
				_movementSystem._pAnimatorOwner = GetComponentInChildren<Animator>();
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void Start()
		{
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void OnDestroy()
		{
			// shut down the movement system
			if( _movementSystem != null )
			{
				_movementSystem.ShutDown();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// for non-physics related updates, called every frame. 
		protected virtual void Update()
		{
			if( _firstUpdate )
			{
				_actorLastForward = _pForward;
				_actorLastPos = _pPosition;

				_firstUpdate = false;
			}

			UpdateInCameraView();

			if( _movementSystem != null )
			{
				_movementSystem.Update();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// called every fixed frame rate frame, used for physics and rigid body updates. 
		protected virtual void FixedUpdate()
		{
			if( _movementSystem != null )
			{
				_movementSystem.FixedUpdate();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void LateUpdate()
		{
			// work out the current speed/velocity here for others to see next update. 
			_actorVelocity = _pPosition - _actorLastPos;
			_actorVelocity *= ( 1.0f / Time.deltaTime );
			_speedCurrent = _actorVelocity.magnitude;
			_actorMoveDir = _actorVelocity.normalized;

			_actorLastPos = _pPosition;
			_actorLastForward = _pForward;

			if( _movementSystem != null )
			{
				_movementSystem.LateUpdate();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// can be called at any time, not just at startup. e.g. after being brought out of a pool for use more than once. 
		public virtual void InitialiseActorVisual()
		{
			_firstUpdate = true;

			if( _movementSystem != null )
			{
				_movementSystem.Initialise();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// Handle any Visual effects of taking damage
		public virtual void TakeDamage( float amount, Vector3 pos, Vector3 normal, 
			int damageType = ( int )eDamageTypeCore.DEFAULT, int damagerId = GlobalDefines.INVALID_ID )
		{
		}

		//--------------------------------------------------------------------------------------------------------------
		// death is not the same as destroy, we sit in the actor list waiting to be re-used. 
		public virtual void OnDeath( bool quiet = false, int damageType = 0, int killerId = GlobalDefines.INVALID_ID )
		{
			if( _movementSystem != null )
			{
				_movementSystem.OnOwnerDeath();
			}

			// fire off any death effect we may have. 
			if( !quiet && ( _prefabsOnDeath != null ) && ( _prefabsOnDeath.Length > 0 ) )
			{
				for( int count = 0; count < _prefabsOnDeath.Length; count++ )
				{
					if( !string.IsNullOrEmpty( _prefabsOnDeath[count] ) )
					{
						Instantiate( Resources.Load( _prefabsOnDeath[count] ), _pPosition, Quaternion.identity );
					}
				}
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		public virtual void ReturnActor()
		{
			if( _movementSystem != null )
			{
				_movementSystem.ShutDown();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// up to projects how they want to do this; expensively or cleverly depending on their camera setups. 
		protected virtual void UpdateInCameraView()
		{
		}

		//--------------------------------------------------------------------------------------------------------------
		// can be overridden, but at base level read the movesys type from the blueprint which is the preferred method. 
		protected virtual void CreateMovementSystemForActorTypeClass()
		{
			_movementSystem = null;

			// _moveSystType should have been populated by the Blueprint using reflection. 
			if( string.IsNullOrEmpty( _actorData._pMoveSystClassName ) )
				return;

			System.Type type = GetType().Assembly.GetType( _actorData._pMoveSystClassName, false );

			if( type == null )
			{
				Debug.LogWarning( DEBUG_LOG_PREFIX +
					"CreateMovementSystemForActorTypeClass - move sys class type can not be found: " +
					_actorData._pMoveSystClassName );
				return;
			}

			_movementSystem = ( MovementSystemBase )System.Activator.CreateInstance( type, new object[] { this } );
		}

		//--------------------------------------------------------------------------------------------------------------
		private void CreateMovementSystem()
		{
			if( _movementSystem == null )
			{
				// actor type specific movement system creation. 
				CreateMovementSystemForActorTypeClass();
			}
		}

#if UNITY_EDITOR
		//--------------------------------------------------------------------------------------------------------------
		protected virtual void OnDrawGizmos()
		{
			if( _renderDebugInfoMoves && ( _movementSystem != null ) )
			{
				_movementSystem.OnDrawGizmos();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void OnGUI()
		{
			if( _renderDebugInfoMoves && ( _movementSystem != null ) )
			{
				_movementSystem.OnGUI();
			}
		}
#endif
	}
}