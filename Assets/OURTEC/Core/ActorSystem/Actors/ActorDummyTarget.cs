////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// ActorDummyTarget.cs
//
// Created: 02/11/17 chope 
// Contributors: 
// 
// Intention: Acts as a target/threat for knowledge system when you want entities to attack something thats not an actor
//				e.g. buildings, static objects, fireing into distance. 
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace AmuzoActor
{
	//------------------------------------------------------------------------------------------------------------------
	// INCLUDES
	using GameDefines;
	using UnityEngine;
	using System.Collections;
	using System.Collections.Generic;

	//------------------------------------------------------------------------------------------------------------------
	// effects the threat score of the dummy target. 
	public enum ePriority
	{
		LOW = -200,
		MEDIUM = 0,
		HIGH = 200
	}

	//------------------------------------------------------------------------------------------------------------------
	public class ActorDummyTarget : ActorLogicBase
	{
		public float _pEffectiveRange
		{
			get
			{
				return 0.0f;
			}
		}

		public int _pPriorityScore
		{
			get
			{
				return ( int )0;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		static public bool FindActorDummyTarget( int idActor, ref ActorDummyTarget foundDummyTarget )
		{
			return false;
		}

		//--------------------------------------------------------------------------------------------------------------
		static public bool DoesActorIdBelongToDummyTarget( int idActor )
		{
			return false;
		}

	/*
		//--------------------------------------------------------------------------------------------------------------
		// STATICS / CONSTS / DEFINES
		static int _nextIdDummyTarget = 0;
		static List<ActorDummyTarget> _dummyTargets = new List<ActorDummyTarget>();

		//--------------------------------------------------------------------------------------------------------------
		// FIELDS
		int _idDummyTarget;
		[SerializeField]
		float _effectiveRange = 100.0f;
		[SerializeField]
		ePriority _priority = ePriority.MEDIUM;

		//--------------------------------------------------------------------------------------------------------------
		// PROPERTIES
		public static List<ActorDummyTarget> _pListDummyTargets
		{
			get
			{
				return _dummyTargets;
			}
		}

		public int _pIdDummyTarget
		{
			get
			{
				return _idDummyTarget;
			}
		}

		public int _pPriorityScore
		{
			get
			{
				return ( int )_priority;
			}
		}
		
		public float _pEffectiveRange
		{
			get
			{
				return _effectiveRange;
			}
		}

		public ePriority _pPriority
		{
			get
			{
				return _priority;
			}
		}

		public override bool _pIsAlive
		{
			get
			{
				return gameObject.activeInHierarchy;
			}
		}

		public override bool _pIsTargetable
		{
			get
			{
				return true;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		static public bool FindActorDummyTarget( int idActor, ref ActorDummyTarget foundDummyTarget )
		{
			foreach( ActorDummyTarget currentDummyTarget in _pListDummyTargets )
			{
				if( currentDummyTarget == null )
					continue;
				if( currentDummyTarget._pIdActor == idActor )
				{
					foundDummyTarget = currentDummyTarget;
					return true;
				}
			}
			return false;
		}

		//--------------------------------------------------------------------------------------------------------------
		static public bool DoesActorIdBelongToDummyTarget( int idActor )
		{
			foreach( ActorDummyTarget currentDummyTarget in _pListDummyTargets )
			{
				if( currentDummyTarget == null )
					continue;
				if( currentDummyTarget._pIdActor == idActor )
				{
					return true;
				}
			}
			return false;
		}

		//--------------------------------------------------------------------------------------------------------------
		protected override void Awake()
		{
			base.Awake();

			// add us to the static list of all dummy targets for fast reference. 
			if( !_dummyTargets.Contains( this ) )
			{
				_dummyTargets.Add( this );
				_idDummyTarget = _nextIdDummyTarget++;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected override void OnDestroy()
		{
			base.OnDestroy();

			// remove from actor ship list. 
			_dummyTargets.Remove( this );
		}

		//--------------------------------------------------------------------------------------------------------------
		// called every fixed frame rate frame, used for physics and rigid body updates. 
		protected override void FixedUpdate()
		{
		}

		//--------------------------------------------------------------------------------------------------------------
		protected override void CreateBrainForActorTypeClass()
		{
			_brain = null;
		}
		*/
	}
}

