////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// ActorLogicBase.cs
//
// Created: 02/11/17 chope 
// Contributors: 
// 
// Intention: An actor is an entity with the concept of movement and/or health
//				ActorLogic handles operations that are required to only be carried out on the local client entity. 
//				As well as Data that may be needed by any host managers. 
//				Health, knowledge, input, movement, squads, brains. 
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace AmuzoActor
{
	//------------------------------------------------------------------------------------------------------------------
	// INCLUDES
	using DataBlueprints;
	using GameDefines;
	using System.Collections.Generic;
	using UnityEngine;

	//------------------------------------------------------------------------------------------------------------------
	// CLASS ActorLogicBase
	[RequireComponent( typeof( ActorVisualBase ) )]
	[RequireComponent( typeof( ActorMessagesBase ) )]
	[RequireComponent( typeof( ActorDataBase ) )]
	public class ActorLogicBase : MonoBehaviour
	{
		//--------------------------------------------------------------------------------------------------------------
		// CONSTS / DEFINES		
		protected const string BPPARAM_BRAIN_BLUEPRINT = "BrainBlueprint";
		protected const string BPPARAM_BRAIN_CLASSNAME = "BrainClassName";

		private const float DEBUG_SPHERE_RADIUS = 0.25f;

		private const string DEBUG_LOG_PREFIX = "ActorLogicBase - ";

		//--------------------------------------------------------------------------------------------------------------
		// FIELDS
		public Transform _targetMarker = null;
		public Transform _lookOrigin = null;
		public SquadBase _initialSquad = null;
		public bool _renderDebugInfoActor = false;
		public bool _renderDebugInfoBrain = false;
		public bool _renderDebugInfoBehaviours = false;
		public bool _renderDebugInfoKnowledge = false;

		[SerializeField]
		protected bool _isPlayer = false;
		protected bool _alive = true;
		protected bool _inUse = true;
		protected bool _isChildActor = false;
		protected bool _firstUpdate;
		protected float _actorSpeed;
		protected float _timeAlive;
		protected Vector3 _actorLastForward;
		protected Vector3 _actorLastPos;
		protected Vector3 _actorVelocity;
		protected Vector3 _dummyVec1;
		protected Vector3 _dummyVec2;
		protected SquadBase _squad = null;
		protected BrainActorBase _brain = null;
		protected List<ActorLogicBase> _childrenActors = null;

		protected ActorDataBase _actorData;
		protected ActorVisualBase _actorVis;
		protected ActorMessagesBase _actorMsgs;

		protected float _healthCurrent;

		//--------------------------------------------------------------------------------------------------------------
		// PROPERTIES
		public int _pIdActor
		{
			get
			{
				return _actorData._pIdActor;
			}
		}

		public bool _pIsChild
		{
			get
			{
				return _isChildActor;
			}
		}

		public bool _pIsInUse
		{
			get
			{
				return _inUse;
			}
			set
			{
				_inUse = value;

				// if this is the local instance of the actor set the flag as well, 
				// if this is a MP game it will propogate to remote instances on the next network update. 				
				if( _pIsLocalActor )
					if( _actorData != null )
						_actorData.SetFlag( eFlagsActorBase.IN_USE, _inUse );
			}
		}

		public virtual bool _pIsAlive
		{
			get
			{
				return _alive && gameObject.activeInHierarchy;
			}
			set
			{
				_alive = value;

				// if this is the local instance of the actor set the flag as well, 
				// if this is a MP game it will propogate to remote instances on the next network update. 				
				if( _pIsLocalActor )
					if( _actorData != null )
						_actorData.SetFlag( eFlagsActorBase.IS_ALIVE, _alive );

				if( !_alive )
					_timeAlive = 0.0f;
			}
		}		

		public bool _pIsInCameraViewPart
		{
			get
			{
				return _actorVis._pIsInCameraViewPart;
			}
		}

		public bool _pIsInCameraViewFull
		{
			get
			{
				return _actorVis._pIsInCameraViewFull;
			}
		}

		public bool _pHasChildrenActors
		{
			get
			{
				return ( ( _childrenActors != null ) && ( _childrenActors.Count > 0 ) );
			}
		}

		public virtual bool _pIsPlayer
		{
			get
			{
				return _isPlayer;
			}
			set
			{
				_isPlayer = value;
			}
		}

		public virtual bool _pIsMoving
		{
			get
			{
				return !GlobalDefines.IsApproximately( _pVelocity, Vector3.zero, 0.00001f );
			}
		}

		public virtual bool _pIsAttacking
		{
			get
			{
				// at base default level monitor the movement system for attack status/state. 
				if( _actorVis == null )
					return false;

				if( _actorVis._pMovementSystem == null )
					return false;

				return _actorVis._pMovementSystem._pIsAttacking;
			}
		}

		public virtual bool _pHasMovedThisFrame
		{
			get
			{
				return ( _actorLastPos != _pPosition );
			}
		}

		public virtual bool _pIsDead
		{
			get
			{
				return !_pIsAlive;
			}
		}

		public virtual bool _pIsTargetable
		{
			get
			{
				return false;
			}
		}

		public float _pHealthNormalised
		{
			get
			{
				return _pHealth / _pHealthMax;
			}
		}

		public  virtual float _pHealth
		{
			get
			{
				return _healthCurrent;
			}
			set
			{
				_healthCurrent = ( value < 0.0f ) ? 0.0f : value;
				if( _healthCurrent > _pHealthMax )
				{
					_healthCurrent = _pHealthMax;
				}
			}
		}

		public virtual float _pHealthMax
		{
			get
			{
				return _actorData._pHealthMaxBase;
			}
		}

		public virtual float _pSpeed
		{
			get
			{
				return _actorSpeed;
			}
		}

		public virtual Vector3 _pVelocity
		{
			get
			{
				return _actorVelocity;
			}
		}

		public SquadBase _pSquad
		{
			get
			{
				return _squad;
			}
			set
			{
				// make sure we are not in a squad already. 
				if( _squad != null )
				{
					if( _squad == value )
						return;
					_squad.RemoveMember( _pIdActor );
				}

				_squad = value;
			}
		}

		public bool _pIsMemberOfSquad
		{
			get
			{
				return _squad != null;
			}
		}

		public BrainActorBase _pBrain
		{
			get
			{
				return _brain;
			}
		}

		public MovementSystemBase _pMovementSystem
		{
			get
			{
				if( _actorVis == null )
					return null;

				return _actorVis._pMovementSystem;
			}
		}

		// a taransform attached to actor that defines its origin of view raycasts, usually where the 'eyes' would be. 
		public virtual Vector3 _pLookOrigin
		{
			get
			{
				if( _lookOrigin != null )
				{
					return _lookOrigin.position;
				}
				return _pTargetPos;
			}
		}

		public virtual Vector3 _pPosition
		{
			get
			{
				return transform.position;
			}
			set
			{
				transform.position = value;
			}
		}

		public virtual Vector3 _pPositionLocal
		{
			get
			{
				return transform.localPosition;
			}
			set
			{
				transform.localPosition = value;
			}
		}

		public virtual Quaternion _pRotation
		{
			get
			{
				return transform.rotation;
			}
			set
			{
				transform.rotation = value;
			}
		}

		public virtual Quaternion _pRotationLocal
		{
			get
			{
				return transform.localRotation;
			}
			set
			{
				transform.localRotation = value;
			}
		}

		public virtual Vector3 _pForward
		{
			get
			{
				return transform.forward;
			}
			set
			{
				transform.forward = value;
			}
		}

		public virtual Vector3 _pRight
		{
			get
			{
				return transform.right;
			}
			set
			{
				transform.right = value;
			}
		}

		public virtual Vector3 _pUp
		{
			get
			{
				return transform.up;
			}
			set
			{
				transform.up = value;
			}
		}

		// position of a transform attached to the actor that others use to aim/look at, usually in the 'chest' or 'face'
		public Vector3 _pTargetPos
		{
			get
			{
				// defined transform, or root position. 
				if( _targetMarker != null )
				{
					return _targetMarker.position;
				}
				return _pPosition;
			}
		}

		// rotation of our target transform. 
		public Quaternion _pTargetRot
		{
			get
			{
				if( _targetMarker != null )
				{
					return _targetMarker.rotation;
				}
				return transform.rotation;
			}
		}

		// our target transform. 
		public Transform _pTargetTrans
		{
			get
			{
				if( _targetMarker != null )
				{
					return _targetMarker.transform;
				}
				return transform;
			}
		}

		// does this actor currently have a target from thier knowledge
		public bool _pHasTarget
		{
			get
			{
				// interface to brain. 
				if( _brain == null )
					return false;
				return ( _brain._pHasTarget );
			}
		}

		// at base level this is the same as _pHasTarget, projects can overide to include current visability etc. 
		public bool _pHasTargetValid
		{
			get
			{
				// interface to brain. 
				if( _brain == null )
					return false;
				return ( _brain._pHasTargetValid );
			}
		}

		// used to decipher friend from foe in knowlege system. 
		public virtual eAllegiance _pAllegiance
		{
			get
			{
				if( _pBrain == null )
					return eAllegiance.NEUTRAL;
				return _pBrain._pAllegiance;
			}
		}

		public virtual bool _pShouldUpdateLogic
		{
			get
			{
				// typically if we are the local actor our logic updates, if not we shouldonly update our visuals
				return _pIsLocalActor;
			}
		}

		public bool _pIsLocalActor
		{
			get
			{
				if( _actorMsgs != null )
				{
					return _actorMsgs._pIsLocalActor;
				}

				// no MP means we are local, everything is. 
				return true;
			}
		}

		public bool _pIsOnHostMachine
		{
			get
			{
				if( _actorMsgs != null )
				{
					return _actorMsgs._pIsOnHostMachine;
				}

				// no MP means we are master. 
				return true;
			}
		}

		public DataBlueprint _pDataBlueprint
		{
			set
			{
				if( _actorData == null )
					return;

				_actorData._pBlueprint = value;
			}
		}

		public ActorMessagesBase _pActorMessages
		{
			get
			{
				return _actorMsgs;
			}
		}

		public float _pTimeAlive
		{
			get
			{
				return _timeAlive;
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void Awake()
		{
			_actorVis = GetComponent<ActorVisualBase>();
			_actorData = GetComponent<ActorDataBase>();
			_actorMsgs = GetComponent<ActorMessagesBase>();
						
			if( _actorData != null )
			{
				_actorData.InitialiseBlueprintBehaviour();
			}

			// set up our brain that handles targets and engaging them. 
			CreateBrain();
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void Start()
		{
			//InitialiseActor();
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void OnEnable()
        {
            InitialiseActor();
        }

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void OnDestroy()
		{
			// kill the brain
			if( _brain != null )
			{
				_brain.Kill();
			}
		}	
		
		//--------------------------------------------------------------------------------------------------------------
		// for non-physics related updates, called every frame. 
		protected virtual void Update()
		{
			if( !_pShouldUpdateLogic )
				return;

			if( !_pIsAlive )
				return;

			if( _pIsInUse )
				_timeAlive += Time.deltaTime;

			if( _firstUpdate )
			{
				_actorLastForward = _pForward;
				_actorLastPos = _pPosition;

				_firstUpdate = false;
			}

			if( _brain != null )
			{
				_brain.Update();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// called every fixed frame rate frame, used for physics and rigid body updates. 
		protected virtual void FixedUpdate()
		{
			if( !_pShouldUpdateLogic )
				return;
			
			if( !_pIsAlive )
				return;

			if( _brain != null )
			{
				_brain.FixedUpdate();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void LateUpdate()
		{
			if( !_pShouldUpdateLogic )
				return;
			
			if( !_pIsAlive )
				return;

			FindReferenceVelocityAndSpeed();

			if( _brain != null )
			{
				_brain.LateUpdate();
			}

			_actorLastPos = _pPosition;
			_actorLastForward = _pForward;
		}	

		//--------------------------------------------------------------------------------------------------------------
		// can be called at any time, not just at startup. e.g. after being brought out of a pool for use more than once. 
		public virtual void InitialiseActor()
		{
			_pIsInUse = true;
			_pIsAlive = true;
			_firstUpdate = true;

			// SK Movements stuck on after reset, so forcing them to stop during initialisation
			if ( _pMovementSystem != null )
				_pMovementSystem.ForceStopAllMoves();

			// reset life to max
			_pHealth = _pHealthMax;

			if( _brain != null )
				_brain.Initialise();

			if( _actorVis != null )
				_actorVis.InitialiseActorVisual();

			// if we have any child actors already initialise them. 
			if( _childrenActors != null )
			{
				if( _childrenActors.Count > 0 )
				{
					for( int count = 0; count < _childrenActors.Count; count++ )
					{
						if( _childrenActors[count] == null )
							continue;
						_childrenActors[count].gameObject.SetActive( true );
					}
				}
			}

			// add us to any squads we were assigned to in tools. 
			if( _initialSquad != null )
			{
				_initialSquad.AddMember( this );
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// After an actor is finished with call this (not death but completely finished)
		public void ReturnActor()
		{
			_pIsInUse = false;

			if( _pBrain != null )
			{
				_pBrain.RevertToDefaultBehaviours();
			}

			if( _actorVis != null )
			{
				_actorVis.ReturnActor();
			}

			if( _childrenActors != null )
			{
				if( _childrenActors.Count > 0 )
				{
					for( int count = 0; count < _childrenActors.Count; count++ )
					{
						if( _childrenActors[count] == null )
							continue;
						_childrenActors[count].ReturnActor();
					}
				}
			}
			
			DisposeOfActorObject();
		}

		//--------------------------------------------------------------------------------------------------------------	
		// at base level we actually destroy the object, projects should override and return object to pool system. 
		protected virtual void DisposeOfActorObject()
		{
			ActorDataBase.DestroyActor( _pIdActor );
		}

		//--------------------------------------------------------------------------------------------------------------	
		// if we are using spawn controllers that keep count of active spawnees, use this to tell them we are dead. 
		public virtual void NotifySpawnControllerOfDeathOrDespawn()
		{
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void FindReferenceVelocityAndSpeed()
		{
			// work out the current speed/velocity here for others to see next update. 
			_actorVelocity = _pPosition - _actorLastPos;
			_actorVelocity *= ( 1.0f / Time.deltaTime );
			_actorSpeed = _actorVelocity.magnitude;
		}

		//--------------------------------------------------------------------------------------------------------------
		// death is not the same as destroy, we sit in the actor list waiting to be re-used. 
		// when you have finished any death effects call ReturnActor()
		public virtual void OnDeath( bool quiet = false, int damageType = 0, int killerId = GlobalDefines.INVALID_ID )
		{
			if( !_pIsAlive )
				return;

			_pIsAlive = false;

#if !PUMA_DEFINE_MULTIPLAYER
			// in single player games tell our visuals to handle death directly, 
			// in MP games they'll get the call from a message. 
			_actorVis.OnDeath( quiet, damageType, killerId );
#endif

			if( _pSquad != null )
			{
				_pSquad.OnSquadMemberDeath( _pIdActor );
			}

			if( _brain != null )
			{
				_brain.OnOwnerDeath();
			}

			NotifySpawnControllerOfDeathOrDespawn();
		}

		//--------------------------------------------------------------------------------------------------------------
		// the entry point for damage, use quiet if you want to just remove health, 
		// otherwise it'll use the pos and normal for creation of damage pfx project side. 
		public virtual void TakeDamage( float amount, Vector3 pos,
			Vector3 normal, int damagerActorId = GlobalDefines.INVALID_ID, bool quiet = false,
			int damageType = ( int )eDamageTypeCore.DEFAULT )
		{
			if( _pIsDead )
				return;

#if !PUMA_DEFINE_MULTIPLAYER
			// for non multiplayer titles tell the visuals to handle any effects directly. 
			_actorVis.TakeDamage( amount, pos, normal, damageType, damagerActorId );
#endif

			float amountToDeduct = amount;

			if( _squad != null )
			{
				_squad.OnSquadMemberTakeDamage( _pIdActor, ref amountToDeduct );
			}

			// negative base health implies we do not worry about health. 
			if( _pHealthMax < 0.0f )
				return;

			// deduct the health. 
			_pHealth -= amountToDeduct;

			// if this has killed us handle it and return;
			if( _pHealth <= 0.0f )
			{
#if PUMA_DEFINE_MULTIPLAYER
				// in multiplayer games send the OnDeathMessage. 
				_actorMsgs.MessageSend_ActorAll_OnDeath( damagerActorId );
#else
				// in single player games call the on death directly. 
				OnDeath( quiet, damageType, damagerActorId );
#endif
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// when you attach an actor to this actor this is called to inform us. 
		public virtual void RegisterChildActor( ActorLogicBase child )
		{
			if( child == null )
				return;

			if( _childrenActors == null )
			{
				_childrenActors = new List<ActorLogicBase>();
			}

			if( !_childrenActors.Contains( child ) )
			{
				_childrenActors.Add( child );
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// used for intantanious movement, otherwise you get physics issues. 
		public virtual void Teleport( Vector3 position, Vector3 direction )
		{
			_pPosition = position;
			_pForward = direction;

			// typically projects will want to inform child actors to reset look orientation or similar
		}

		//--------------------------------------------------------------------------------------------------------------
		// at base level issue a move order to where we are. 
		public virtual void StopAllMovement()
		{
			if( _pBrain != null )
			{
				_pBrain.IssueMoveOrder( _pPosition );
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		// nothing to do at base level, how we move is project dependent.
		public virtual bool OnBrainGivenDestination( Vector3 position, float speedNorm = 1.0f )
		{
			return true;
		}

		//--------------------------------------------------------------------------------------------------------------
		// can be overridden, but at base level it reads the brain type from the blueprint which is the preferred method. 
		protected virtual void CreateBrainForActorTypeClass()
		{
			_brain = null;

			if( _actorData._pBlueprint == null )
				return;

			DataBlueprint brainBlueprint = null;

			// if no blueprint param is defined we dont want to have a brain. 
			if( !_actorData._pBlueprint.TryGetBlueprint( BPPARAM_BRAIN_BLUEPRINT, out brainBlueprint ) )
				return;

			if( brainBlueprint == null )
				return;

			// so we have foung the brain blurpint, look for the class name inside of it. 
			string brainClassName = GlobalDefines.INVALID_STRING;

			if( !brainBlueprint.TryGetString( BPPARAM_BRAIN_CLASSNAME, out brainClassName ) )
			{
				Debug.Log( DEBUG_LOG_PREFIX + "CreateBrainForActorTypeClass - Brain BP found but no " +
					"class name defined in it, this actor will have no brain. Brain BP name:" + brainBlueprint._pName );
				return;
			}

			System.Type type = GetType().Assembly.GetType( brainClassName, false );

			if( type == null )
			{
				Debug.LogWarning( DEBUG_LOG_PREFIX +
					"CreateBrainForActorTypeClass - brain class type can not be found: " + brainClassName );
				return;
			}

			_brain = ( BrainActorBase )System.Activator.CreateInstance( type, new object[] { this, brainBlueprint } );

		}

		//--------------------------------------------------------------------------------------------------------------
		private void CreateBrain()
		{
			if( _brain == null )
			{
				// actor type specific brain creation. 
				CreateBrainForActorTypeClass();
			}
		}

#if UNITY_EDITOR
		//--------------------------------------------------------------------------------------------------------------
		protected virtual void OnDrawGizmos()
		{
			if( _brain != null )
			{
				_brain.OnDrawGizmos();
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		protected virtual void OnGUI()
		{
			if( _brain != null )
			{
				_brain.OnGUI();
			}
		}
#endif

	}
}
