﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// MoveBase.cs
//
// Created: 15/9/2014 chope
// Contributors: 
// 
// Intention:
// base functionality for all moves. 
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using AmuzoActor;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
// CLASS MoveBase
public abstract class MoveBase
{
	//------------------------------------------------------------------------------------------------------------------
	// CONSTS / DEFINES		
	private const string DEBUG_LOG_PREFIX = "MoveBase - ";

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	protected bool _firstUpdatePlaying = true;
	protected float _timeSinceStart;
	protected ActorLogicBase _actorOwnerLogic;
	protected ActorVisualBase _actorOwnerVis;
	protected List<MoveBase> _movesNext;
	protected List<MoveBase> _movesPrevious;

	private bool _isPlaying = false;
	private bool _hasBeenAborted = false;
	private MovementSystemBase _movementSystem = null;

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public bool _pIsPlaying
	{
		get
		{
			return _isPlaying;
		}
	}

	public bool _pShouldStart
	{
		get
		{
			return ShouldStart();
		}
	}

	public bool _pHasBeenAborted
	{
		get
		{
			return _hasBeenAborted;
		}
	}

	public bool _pHasMovesNext
	{
		get
		{
			return ( ( _movesNext != null ) && ( _movesNext.Count > 0 ) );
		}
	}

	public float _pTimeSinceStart
	{
		get
		{
			return _timeSinceStart;
		}
	}

	public virtual bool _pAllowsMovement
	{
		get
		{
			return false;
		}
	}

	public virtual bool _pAllowsOrientation
	{
		get
		{
			return false;
		}
	}

	public virtual bool _pAllowsInput
	{
		get
		{
			return false;
		}
	}

	public virtual bool _pHasCompleted
	{
		get
		{
			return false;
		}
	}

	public virtual bool _pCanAbort
	{
		get
		{
			return false;
		}
	}

	public virtual bool _pIsAnAttack
	{
		get
		{
			return false;
		}
	}

	public virtual bool _pIsAJump
	{
		get
		{
			return false;
		}
	}

	public List<MoveBase> _pMovesNext
	{
		get
		{
			return _movesNext;
		}
	}

	public MoveBase _pMoveNext
	{
		set
		{
			if( value == null )
				return;

			if( _movesNext == null )
			{
				_movesNext = new List<MoveBase>();
			}

			if( _movesNext.Contains( value ) )
				return;

			_movesNext.Add( value );
			value._pMovePrevious = this;
		}
	}

	protected virtual bool _pSystemsCheckFull
	{
		get
		{
			return ( _pHasOwnerValid && _pHasMoveSysValid && _pHasAnimatorValid );
		}
	}

	protected virtual bool _pHasMoveSysValid
	{
		get
		{
			return ( _movementSystem != null );
		}
	}

	protected virtual bool _pHasAnimatorValid
	{
		get
		{
			return ( _pHasMoveSysValid && ( _pMoveSys._pAnimatorOwner != null ) );
		}
	}

	protected virtual bool _pHasOwnerValid
	{
		get
		{
			return ( ( _actorOwnerLogic != null ) && ( _actorOwnerVis != null ) && ( _actorOwnerLogic._pIsAlive ) );
		}
	}

	protected MovementSystemBase _pMoveSys
	{
		get
		{
			return _movementSystem;
		}
	}

	protected Animator _pAnimator
	{
		get
		{
			if( !_pHasMoveSysValid || !_pHasAnimatorValid )
				return null;
			return _pMoveSys._pAnimatorOwner;
		}
	}

	private MoveBase _pMovePrevious
	{
		set
		{
			if( value == null )
				return;

			if( _movesPrevious == null )
			{
				_movesPrevious = new List<MoveBase>();
			}

			if( _movesPrevious.Contains( value ) )
				return;

			_movesPrevious.Add( value );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	// ABSTRACTS
	public abstract int _pPriority
	{
		get;
	}

	public abstract uint _pHashId
	{
		get;
	}

	//------------------------------------------------------------------------------------------------------------------
	public MoveBase( MovementSystemBase movementSystem )
	{
		_movementSystem = movementSystem;

		if( _movementSystem != null )
		{
			_actorOwnerLogic = _movementSystem._pActorLogic;
			_actorOwnerVis = _movementSystem._pActorVisual;
		}
	}
	
	//------------------------------------------------------------------------------------------------------------------
	// this can stop the start by returning false
	public virtual bool Start( params object[] parameters )
	{
		_timeSinceStart = 0.0f;
		_isPlaying = true;
		_firstUpdatePlaying = true;
		_hasBeenAborted = false;

		return true;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void Update()
	{
		if( _isPlaying )
		{
			UpdatePlaying();
			if( _firstUpdatePlaying )
			{
				_firstUpdatePlaying = false;
			}
		}
		else
		{
			UpdateWaitingToStart();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public void FixedUpdate()
	{
		if( _isPlaying )
		{
			FixedUpdatePlaying();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public void LateUpdate()
	{
		if( _isPlaying )
		{
			LateUpdatePlaying();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void Finish()
	{
		OnFinishAndAbort();
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void Abort()
	{
		if( !_pCanAbort )
			return;

		_hasBeenAborted = true;

		OnFinishAndAbort();
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void UpdatePlaying()
	{
		_timeSinceStart += Time.deltaTime;
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void FixedUpdatePlaying()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void LateUpdatePlaying()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	// useful for chained moves with branches that need to monitor data to know which should start. 
	protected virtual void UpdateWaitingToStart()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	// at base level we start if we have chained previous moves that are active and have finished. 
	protected virtual bool ShouldStart()
	{
		if( ( _movesPrevious != null ) && ( _movesPrevious.Count > 0 ) )
		{
			for( int count = 0; count < _movesPrevious.Count; count++ )
			{
				if( _movesPrevious[count] == null )
					continue;
				if( !_movesPrevious[count]._pIsPlaying )
					continue;

				// if the move before was aborted we should not continue down the chain of moves. 
				if( _movesPrevious[count]._hasBeenAborted )
					continue;

				if( _movesPrevious[count]._pHasCompleted )
					return true;
			}
		}
		return false;
	}

	//------------------------------------------------------------------------------------------------------------------
	protected virtual void OnFinishAndAbort()
	{
		_isPlaying = false;
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnDrawGizmos()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnGUI( Vector2 textPos, float width, GUIStyle guiStyle )
	{
	}
#endif
}
