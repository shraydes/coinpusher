﻿////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// MovementSystemBase.cs
//
// Created: 16/9/2014 chope
// Contributors: 
// 
// Intention:
// System to create, run, monitor and generally handle the moves for each character. 
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using AmuzoActor;
using GameDefines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
// CLASS MovementSystemBase
public abstract class MovementSystemBase
{
	//------------------------------------------------------------------------------------------------------------------
	// CONSTS / DEFINES		
	private const string DEBUG_LOG_PREFIX = "MovementSystemBase - ";

	//------------------------------------------------------------------------------------------------------------------
	// FIELDS
	protected MoveBase _moveActive = null;
	protected Animator _animatorOwner = null;
	protected ActorLogicBase _actorOwnerLogic = null;
	protected ActorVisualBase _actorOwnerVisual = null;
	protected List<MoveData> _moveQueue = new List<MoveData>();
	protected Dictionary<uint, MoveBase> _moveChains = new Dictionary<uint, MoveBase>();
	
	private GUIStyle _guiStyle = new GUIStyle();

	//------------------------------------------------------------------------------------------------------------------
	// PROPERTIES
	public bool _pIsPlayingAnyMove
	{
		get
		{
			return ( _moveActive != null );
		}
	}

	public bool _pHasAnimatorValid
	{
		get
		{
			return ( _animatorOwner != null );
		}
	}

	public uint _pCurrentMoveHash
	{
		get
		{
			return _pIsPlayingAnyMove ? _moveActive._pHashId : GlobalDefines.INVALID_UID;
		}
	}

	public MoveBase _pCurrentMove
	{
		get
		{
			return _moveActive;
		}
	}

	public ActorLogicBase _pActorLogic
	{
		get
		{
			return _actorOwnerLogic;
		}
	}

	public ActorVisualBase _pActorVisual
	{
		get
		{
			return _actorOwnerVisual;
		}
	}

	public Animator _pAnimatorOwner
	{
		get
		{
			return _animatorOwner;
		}
		set
		{
			_animatorOwner = value;
		}
	}

	public virtual bool _pMoveControllingMovement
	{
		get
		{
			if( !_pIsPlayingAnyMove )
				return false;

			return !_moveActive._pAllowsMovement;
		}
	}

	public virtual bool _pMoveControllingOrientation
	{
		get
		{
			if( !_pIsPlayingAnyMove )
				return false;

			return !_moveActive._pAllowsOrientation;
		}
	}

	public virtual bool _pMoveControllingInput
	{
		get
		{
			if( !_pIsPlayingAnyMove )
				return false;

			return !_moveActive._pAllowsInput;
		}
	}

	public virtual bool _pIsAttacking
	{
		get
		{
			if( !_pIsPlayingAnyMove )
				return false;

			return _pCurrentMove._pIsAnAttack;
		}
	}

	public virtual bool _pIsJumping
	{
		get
		{
			if( !_pIsPlayingAnyMove )
				return false;

			return _pCurrentMove._pIsAJump;
		}
	}

	protected bool _pHasQueuedMoves
	{
		get
		{
			return ( ( _moveQueue != null ) && ( _moveQueue.Count > 0 ) );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	// ABSTRACTS
	protected abstract int GetMovePriority( uint hashId );
	protected abstract void SetUpMoveChains();
	protected abstract string GetMoveNameFromHash( uint hashId );

	//------------------------------------------------------------------------------------------------------------------
	public MovementSystemBase( ActorVisualBase ownerVis )
	{
		_actorOwnerVisual = ownerVis;
		_actorOwnerLogic = ownerVis.GetComponent<ActorLogicBase>();
		SetUpMoveChains();
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void Initialise()
	{
	}

	//------------------------------------------------------------------------------------------------------------------
	// return false if we were unable to play or queue the move. 	
	public virtual bool PlayMove( uint hashId, 
		bool abortCurrentIfPoss = true, bool queueOtherwise = true, params object[] parameters )
	{
		if( _pIsPlayingAnyMove )
		{
			// if we cant abort the current move see if we should queue
			if( !abortCurrentIfPoss )
			{
				// queue if requested or just return. 
				if( queueOtherwise )
				{
					QueueMove( hashId );
					return true;
				}
				return false;
			}

			// if we abort current if possible, and it is possible
			if( abortCurrentIfPoss && _moveActive._pCanAbort )
			{
				// abort move. 
				if( AbortCurrentMove() )
				{
					return StartMoveChain( hashId, parameters );
				}

				// something went wrong, queue otherwise if requested. 
				if( queueOtherwise )
				{
					QueueMove( hashId );
					return true;
				}

				// move not played or queued. 
				return false;
			}

			// if we abort current if possible, and it is NOT possible. 
			if( abortCurrentIfPoss && !_moveActive._pCanAbort )
			{
				// if we are not willing to queue, then return now. 
				if( !queueOtherwise )
					return false;

				// otherwise queue it. 
				QueueMove( hashId );
				return true;
			}
		}

		// no other move is playing, just play the new one and be done. 
		return StartMoveChain( hashId, parameters );
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void Update()
	{
		if( _actorOwnerLogic == null )
			return;

		// if we are not playing a move and have queued moves then get to them. 
		if( !_pIsPlayingAnyMove && _pHasQueuedMoves )
		{
			FindAndStartQueuedMove();
			return;
		}

		if( !_pIsPlayingAnyMove )
		{
			// if we are not playing a move then see if any of our 1st link moves should start. 
			UpdateChainStarts();
			CheckShouldStarts();
		}
		else
		{
			// otherwise we are playing a move chain, update it. 
			UpdateCurrentChain();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void FixedUpdate()
	{
		if( _actorOwnerLogic == null )
			return;

		// only fixed update any moves currently playing. 
		if( _pIsPlayingAnyMove )
		{
			_moveActive.FixedUpdate();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void LateUpdate()
	{
		if( _actorOwnerLogic == null )
			return;

		// only late update any moves currently playing. 
		if( _pIsPlayingAnyMove )
		{
			_moveActive.LateUpdate();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool IsPlayingMove( uint moveHashId )
	{
		if( !_pIsPlayingAnyMove )
			return false;
		return ( _moveActive._pHashId == moveHashId );
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool AbortCurrentMove()
	{
		if( _moveActive == null )
			return true;
		if( !_moveActive._pCanAbort )
			return false;

		_moveActive.Abort();
		_moveActive = null;
		return true;
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnOwnerDeath()
	{
		ForceStopAllMoves();
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void ShutDown()
	{
		ForceStopAllMoves();
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool IsInAnimatorState( int stateHash, int layerIndex )
	{
		if( _animatorOwner == null )
			return false;
		
		if( _animatorOwner.layerCount <= layerIndex )
			return false;

		return ( _animatorOwner.GetCurrentAnimatorStateInfo( layerIndex ).fullPathHash == stateHash );
	}

	//------------------------------------------------------------------------------------------------------------------
	public float GetCurrentStateNormalisedTime( int layerIndex )
	{
		if( _animatorOwner == null )
			return 0.0f;
		
		if( _animatorOwner.layerCount <= layerIndex )
			return 0.0f;

		return _animatorOwner.GetCurrentAnimatorStateInfo( layerIndex ).normalizedTime;
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool IsBlendingIntoState( int stateHash, int layerIndex = -1 )
	{
		if( _animatorOwner == null )
			return false;
		
		if( _animatorOwner.layerCount <= layerIndex )
			return false;

		// if we are not blending at all then its definitely no. 
		if( !_animatorOwner.IsInTransition( layerIndex ) )
			return false;

		return ( _animatorOwner.GetNextAnimatorStateInfo( layerIndex ).fullPathHash == stateHash );
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool IsBlendingOutOfState( int stateHash, int layerIndex )
	{
		if( _animatorOwner == null )
			return false;
		
		if( _animatorOwner.layerCount <= layerIndex )
			return false;

		// if we are not blending at all then its definitely no. 
		if( !_animatorOwner.IsInTransition( layerIndex ) )
			return false;

		return ( _animatorOwner.GetCurrentAnimatorStateInfo( layerIndex ).fullPathHash == stateHash );
	}

	//------------------------------------------------------------------------------------------------------------------
	public bool IsBlendingIntoOrInAnimatorState( int stateHash, int layerIndex )
	{
		if( _animatorOwner == null )
			return false;

		if( _animatorOwner.layerCount <= layerIndex )
			return false;
		
		// if we are not blending and the state we are in is the requested one return true;
		if( !_animatorOwner.IsInTransition( layerIndex ) && 
			( _animatorOwner.GetCurrentAnimatorStateInfo( layerIndex ).fullPathHash == stateHash ) )
			return true;

		// if we are blending and our next state is the requested then return true. 
		if( _animatorOwner.IsInTransition( layerIndex ) &&
			( _animatorOwner.GetNextAnimatorStateInfo( layerIndex ).fullPathHash == stateHash ) )
			return true;

		return false;
	}

	//------------------------------------------------------------------------------------------------------------------
	public void ForceStopAllMoves()
	{
		if( _pHasQueuedMoves )
		{
			_moveQueue.Clear();
		}

		if( _pIsPlayingAnyMove )
		{
			_moveActive.Finish();
			_moveActive = null;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private bool StartMoveChain( uint hashId, params object[] paremters )
	{
		if( _moveChains == null )
			return false;

		if( !_moveChains.ContainsKey( hashId ) )
		{
			Debug.LogWarning( DEBUG_LOG_PREFIX + "StartMove - Move requested not present in dictionary. Move: " +
				GetMoveNameFromHash( hashId ) );
			return false;
		}

		if( _moveChains[hashId] == null )
		{
			Debug.LogWarning( DEBUG_LOG_PREFIX + "StartMove - Move requested has no chain antry. Move: " +
				GetMoveNameFromHash( hashId ) );
			_moveChains.Remove( hashId );
			return false;
		}

		// grab it, start it and let it handle everything from here. 
		if( _moveChains[hashId].Start( paremters ) )
		{
			// only set it as the active move it it did indeed start. 
			_moveActive = _moveChains[hashId];
			return true;
		}

		return false;
	}

	//------------------------------------------------------------------------------------------------------------------
	private void FindAndStartQueuedMove()
	{
		if( !_pHasQueuedMoves )
			return;

		int highestPriority = int.MinValue;
		int index = GlobalDefines.INVALID_INDEX;

		// go through the moved queued looking at their priority. 
		for( int count = 0; count < _moveQueue.Count; count++ )
		{
			int priority = GetMovePriority( _moveQueue[count]._moveHash );
			if( priority > highestPriority )
			{
				highestPriority = priority;
				index = count;
			}
		}

		// if we found a move to start then remove from queue and start it. 
		if( index != GlobalDefines.INVALID_INDEX )
		{
			MoveData moveToStart = _moveQueue[index];
			_moveQueue.RemoveAt( index );
			StartMoveChain( moveToStart._moveHash, moveToStart._params );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void QueueMove( uint hashId, params object[] parameters )
	{
		if( _moveQueue == null )
			return;

		for( int count = 0; count < _moveQueue.Count; count++ )
		{
			if( _moveQueue[count] == null )
				continue;

			// if we already have this move in the queue then just update the parameters. 
			if( _moveQueue[count]._moveHash == hashId )
			{
				_moveQueue[count]._params = parameters;
				return;
			}
		}

		MoveData newData = new MoveData();
		newData._moveHash = hashId;
		newData._params = parameters;

		// add move to the end of the queue. 
		_moveQueue.Insert( _moveQueue.Count, newData );
	}

	//------------------------------------------------------------------------------------------------------------------
	private void CheckShouldStarts()
	{
		if( _actorOwnerLogic == null )
			return;
		if( _pIsPlayingAnyMove )
			return;
		if( _moveActive != null )
			return;
		if( _moveChains == null )
			return;
		if( _moveChains.Count == 0 )
			return;

		int priorityOfPotentialMove = int.MinValue;
		uint hashIdToStart = GlobalDefines.INVALID_UID;

		// iterate through the starting moves and find the highest priority with a should start flag. 
		foreach( MoveBase firstMoveInChain in _moveChains.Values )
		{
			if( firstMoveInChain == null )
				continue;
			if( firstMoveInChain._pShouldStart )
			{
				if( firstMoveInChain._pPriority > priorityOfPotentialMove )
				{
					priorityOfPotentialMove = firstMoveInChain._pPriority;
					hashIdToStart = firstMoveInChain._pHashId;
				}
			}
		}

		// we found a move that should start... start it. 
		if( hashIdToStart != GlobalDefines.INVALID_UID )
		{
			StartMoveChain( hashIdToStart );
			return;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	private void UpdateChainStarts()
	{
		if( _actorOwnerLogic == null )
			return;
		if( _pIsPlayingAnyMove )
			return;
		if( _moveActive != null )
			return;
		if( _moveChains == null )
			return;
		if( _moveChains.Count == 0 )
			return;

		foreach( MoveBase firstMoveInChain in _moveChains.Values )
		{
			if( firstMoveInChain == null )
				continue;
			firstMoveInChain.Update();
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	// updates the current active move and any of the next moves to come in the chain. 
	private void UpdateCurrentChain()
	{
		if( _actorOwnerLogic == null )
			return;
		if( !_pIsPlayingAnyMove )
			return;
		if( _moveActive == null )
			return;

		// update the current move. 
		_moveActive.Update();

		if( _moveActive == null )
			return;

		// if the move does not have a next moves then just check if it is finished and return
		// or if its been aborted we do not consider next moves. 
		if( !_moveActive._pHasMovesNext || _moveActive._pHasBeenAborted )
		{
			if( _moveActive._pHasCompleted || _moveActive._pHasBeenAborted )
			{
				_moveActive.Finish();
				_moveActive = null;
			}
			return;
		}

		int indexToStart = GlobalDefines.INVALID_INDEX;
		int priorityOfNextMove = int.MinValue;

		// otherwise update our next moves, and look to see if we should start any of them. 
		for( int count = 0; count < _moveActive._pMovesNext.Count; count++ )
		{
			if( _moveActive._pMovesNext[count] == null )
				continue;
			_moveActive._pMovesNext[count].Update();

			if( _moveActive._pMovesNext[count]._pShouldStart )
			{
				if( _moveActive._pMovesNext[count]._pPriority > priorityOfNextMove )
				{
					priorityOfNextMove = _moveActive._pMovesNext[count]._pPriority;
					indexToStart = count;
				}
			}
		}

		// we have found a move that should start, finish current and start next. 
		if( ( indexToStart != GlobalDefines.INVALID_INDEX ) &&
			( _moveActive._pMovesNext != null ) && ( indexToStart < _moveActive._pMovesNext.Count ) )
		{
			_moveActive.Finish();
			_moveActive = _moveActive._pMovesNext[indexToStart];

			if( _moveActive != null )
			{
				_moveActive.Start();
				return;
			}
		}

		// if our move has finished and we have not found a move to start then just finish the chain. 
		if( _moveActive._pHasCompleted && ( indexToStart == GlobalDefines.INVALID_INDEX ) )
		{
			_moveActive.Finish();
			_moveActive = null;
			return;
		}
	}

#if UNITY_EDITOR
	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnDrawGizmos()
	{
		if( !_pIsPlayingAnyMove )
			return;

		_pCurrentMove.OnDrawGizmos();
	}

	//------------------------------------------------------------------------------------------------------------------
	public virtual void OnGUI()
	{
		Vector2 textPos = new Vector2( Screen.width * 0.7f, Screen.height * 0.1f );
		int width = ( int )( Screen.width * 0.8f );
		_guiStyle.fontSize = 12;
		_guiStyle.clipping = TextClipping.Clip;
		_guiStyle.normal.textColor = Color.yellow;

		GUI.Box( new Rect( ( int )textPos.x, ( int )textPos.y, width, GlobalDefines.DEBUG_TEXT_HIEGHT ),
			"MOVEMENT SYSTEM DATA: ", _guiStyle );
		textPos.y += GlobalDefines.DEBUG_TEXT_HIEGHT;

		if( !_pIsPlayingAnyMove )
		{
			GUI.Box( new Rect( ( int )textPos.x, ( int )textPos.y, width, GlobalDefines.DEBUG_TEXT_HIEGHT ), 
				"NONE", _guiStyle );
			textPos.y += GlobalDefines.DEBUG_TEXT_HIEGHT;
		}
		else
		{
			if( _pCurrentMove != null )
			{
				GUI.Box( new Rect( ( int )textPos.x, ( int )textPos.y, width, GlobalDefines.DEBUG_TEXT_HIEGHT ),
					_pCurrentMove.GetType().ToString(), _guiStyle );
				textPos.y += GlobalDefines.DEBUG_TEXT_HIEGHT;

				_pCurrentMove.OnGUI( textPos, width, _guiStyle );
			}
		}
	}
#endif
}

public class MoveData
{
	public uint _moveHash;
	public object[] _params;
}
