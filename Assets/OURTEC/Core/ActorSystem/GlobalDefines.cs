//----------------------------------------------------------------------------------------------------------------------
// GlobalDefines.cs
// 23/04/14
// 
// Studio Standard defines and basic lookups used for all projects. 
// Individual Projects Can define their 'partial' implementations and preserve the namespace and class name. 

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

//----------------------------------------------------------------------------------------------------------------------
namespace GameDefines
{
	public enum eDamageTypeCore
	{
		DEFAULT = 0,
		FIRE,
		BULLET,
		IMPACT,
		PROJECTILE,

		NUM_DAMAGE_TYPES_CORE // 5 
	}

	public enum eSpawnObjectType
	{
		NULL = 0,
		
		SOUND_EFFECT_AT_POS
	}
	
	//------------------------------------------------------------------------------------------------------------------
	static public partial class GlobalDefines
	{
		//--------------------------------------------------------------------------------------------------------------
		// CONSTS / DEFINES
		public const string DEFAULT_LANGUAGE = "EN";
		public const string EMPTY_STRING = "";
		public const string INVALID_STRING = "INVALID";
		public const string UNITY_PREFAB_SUFFIX = "(Clone)";
		public static readonly Vector2 INVALID_VECTOR_2 = new Vector2( 9.666f, 9.666f ); 
		public static readonly Vector3 INVALID_VECTOR_3 = new Vector3( 9.666f, 9.666f, 9.666f );
		
		public const int INVALID_INT = 9898989;
		public const int INVALID_ID = -1;
		public const int INVALID_INDEX = -1;		
		public const int DEBUG_TEXT_HIEGHT = 25;
		public const uint INVALID_UID = 9898989;
		public const float INVALID_FLOAT = 98989898.98f;
		public const float QUARTER = 0.25f;
		public const float HALF = 0.5f;
		public const float THREE_QUARTERS = 0.75f;
		public const float THIRD = 0.333333333f;
		public const float TWO_THIRDS = 0.666666666f;
		public const float SECONDS_MILI_TO_WHOLE = 0.001f;
				
		//--------------------------------------------------------------------------------------------------------------
		// STATICS
		public static bool _killSwitchRaised = false;
		public static bool _minVersionWrong = false;
		private static bool _lowMemGameplay = false; // different to fidelity, used for game setups, not quality settings. 
		private static string _urlPrivacy = INVALID_STRING;
		private static string _urlCookies = INVALID_STRING;
		private static string _exitAppMessage = INVALID_STRING;
		
		//--------------------------------------------------------------------------------------------------------------
		// PROPERTIES
		public static bool _pUseLowMemGameplay{ set{ _lowMemGameplay = value; } get{ return _lowMemGameplay; } }
		
		public static bool _pProjectHasMpSupport
		{
			get
			{
#if !PUMA_DEFINE_MULTIPLAYER
				return false;
#else
				return NetworkConnectionManager._pProjectHasMpSupport;
#endif
			}
		}

		//--------------------------------------------------------------------------------------------------------------
		public static string RemoveSpaces( string stringWithSpacesMaybe )
		{
			return stringWithSpacesMaybe.Replace( " ", string.Empty );
		}

		//--------------------------------------------------------------------------------------------------------------
		public static bool IsApproximately( float a, float b, float tolerance = 0.01f )
		{
			return Mathf.Abs( a - b ) < tolerance;
		}
		
		//--------------------------------------------------------------------------------------------------------------
		public static bool IsApproximately( Vector3 a, Vector3 b, float tolerance = 0.01f )
		{
			return ( ( Mathf.Abs( a.x - b.x ) < tolerance ) &&
					( Mathf.Abs( a.y - b.y ) < tolerance ) &&
					( Mathf.Abs( a.z - b.z ) < tolerance ) );
		}

		//--------------------------------------------------------------------------------------------------------------
		public static bool IsValidString( string toCheck )
		{
			return !( toCheck == INVALID_STRING );
		}

		//--------------------------------------------------------------------------------------------------------------
		public static bool IsValidInt( int toCheck )
		{
			return !( toCheck == INVALID_INT );
		}

		//--------------------------------------------------------------------------------------------------------------
		public static bool IsValidId( int toCheck )
		{
			return !( toCheck == INVALID_ID );
		}

		//--------------------------------------------------------------------------------------------------------------
		public static bool IsValidIndex( int toCheck )
		{
			return !( toCheck == INVALID_INDEX );
		}

		//--------------------------------------------------------------------------------------------------------------
		public static bool IsValidFloat( float toCheck )
		{
			return !( toCheck == INVALID_FLOAT );
		}

		//--------------------------------------------------------------------------------------------------------------
		public static T ParseEnum<T>( string value )
		{
			try
			{
				return ( T )Enum.Parse( typeof( T ), value, true );
			}
			catch//( Exception ex )
			{
				//Debug.LogError( "GlobalDefines::ParseEnum - value passed is not a valid enum type. Value: " + value + 
				//	", Enum: " + typeof( T ).ToString() + ", Exeption: " + ex.ToString() );
				return default( T );
			}
		}
	}
}
