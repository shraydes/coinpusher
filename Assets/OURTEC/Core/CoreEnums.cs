﻿////////////////////////////////////////////
// 
// CoreEnums.cs
//
// Created: 14/03/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
 
namespace AmuzoEngine
{
	public enum ETargetPlatform
	{
		NULL = 0,

		STANDALONE,
		ANDROID,
		IOS,
		WEBPLAYER,
		WEBGL,
		WSA,
		TVOS,
		SWITCH
	}

	public static class TargetPlatform
	{
#if UNITY_STANDALONE
		public const ETargetPlatform	CURRENT = ETargetPlatform.STANDALONE;
#elif UNITY_ANDROID
		public const ETargetPlatform	CURRENT = ETargetPlatform.ANDROID;
#elif UNITY_IOS || UNITY_IPHONE
		public const ETargetPlatform	CURRENT = ETargetPlatform.IOS;
#elif UNITY_WEBPLAYER
		public const ETargetPlatform	CURRENT = ETargetPlatform.WEBPLAYER;
#elif UNITY_WEBGL
		public const ETargetPlatform	CURRENT = ETargetPlatform.WEBGL;
#elif UNITY_WSA
		public const ETargetPlatform	CURRENT = ETargetPlatform.WSA;
#elif UNITY_TVOS
		public const ETargetPlatform	CURRENT = ETargetPlatform.TVOS;
#elif UNITY_SWITCH
		public const ETargetPlatform	CURRENT = ETargetPlatform.SWITCH;
#else
		public const ETargetPlatform	CURRENT = ETargetPlatform.NULL;
#endif
	}

	public enum EInitializeType
	{
		AWAKE = 0,
		START,
		MANUAL,
		INIT_FACADE
	}

	public enum EUpdateType
	{
		UPDATE = 0,
		FIXED_UPDATE = 1,
		LATE_UPDATE = 2,
		MANUAL = 3
	}
}	
