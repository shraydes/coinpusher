﻿#if PUMA_DEFINE_INTERNET_REACHABILITY_VERIFIER
#define HAS_INTERNET_REACHABILITY_VERIFIER
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// InternetCheckFacade.cs
//
// Created: 22/12/15 alivy
// Contributors: 
// 
// Intention: the go-to place for Internet checking. 
//
// Notes:
// it is a gatekeeper between all other systems and Internet connectivity checking.
// want to replace an internet check system? update this file, not the entire project.
// Never put project specific code in here, that belongs elsewhere. Don't hack!
// also keep this file light, don't put actual implementation in here either
// this should be a somewhat lightweight wrapper
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------
// INCLUDES
using System.Collections;
using UnityEngine;

//----------------------------------------------------------------------------------------------------------------------
// CLASS InternetCheckFacade
public class InternetCheckFacade : MonoBehaviour, InitialisationProxy.ITarget
{
	private InitialisationState	_initState;

	//------------------------------------------------------------------------------------------------------------------
	// if your Internet verifier needs initialisation or initial checks, they should be flagged here
	public static bool _pIsReadyForInternetChecks
	{
		get
		{
			return true;
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	// primary lookup to see if you're online
	public static bool _pIsOnline
	{
		get
		{
#if HAS_INTERNET_REACHABILITY_VERIFIER
			if( InternetReachabilityVerifier.Instance == null )
				return false;

			//Debug.Log( "InternetCheckFacade _pIsOnline = " +
			//	InternetReachabilityVerifier.Instance._pHasInternet );

			return InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified;
#else
			return false;
#endif
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	void Awake()
	{
		DontDestroyOnLoad( gameObject );
	}

	//------------------------------------------------------------------------------------------------------------------
	// for systems that require a delayed response
	public static void RequestInternetCheck( System.Action<bool> resultCallback )
	{
		// our current implementation doesn't need this, so return immediately
		if( resultCallback != null )
		{
			resultCallback( _pIsOnline );
		}
	}

	//------------------------------------------------------------------------------------------------------------------
	public static void DoRecheckInternetFromFlow()
	{
		// with the current InternetReachability system, we do not need to poll / recheck internet 
		// ( it does it under the hood) but this method still exists in case we still want this functionality 
		// with a future system that requires manual prodding.
	}

	private bool _pHasInternetCheckResult
	{
		get
		{
#if HAS_INTERNET_REACHABILITY_VERIFIER
			if( InternetReachabilityVerifier.Instance == null )
				return true;

			InternetReachabilityVerifier.Status	currStatus = InternetReachabilityVerifier.Instance.status;

			return currStatus == InternetReachabilityVerifier.Status.NetVerified || currStatus == InternetReachabilityVerifier.Status.Offline;
#else
			return true;
#endif
		}
	}

	InitialisationState InitialisationProxy.ITarget._pInitialisationState
	{
		get
		{
			return _initState;
		}

		set
		{
			_initState = value;
		}
	}

	void InitialisationProxy.ITarget.OnStartInitialising()
	{
		if ( _pHasInternetCheckResult )
		{
			_initState = InitialisationState.FINISHED;
		}
		else
		{
			_initState = InitialisationState.INITIALISING;
		}
	}

	InitialisationState InitialisationProxy.ITarget.OnUpdateInitialising()
	{
		if ( _pHasInternetCheckResult )
		{
			_initState = InitialisationState.FINISHED;
		}

		return _initState;
	}
}
