﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(InternetCheckFacade))]
public class InternetCheckFacadeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Debug Test Internet"))
        {
            Debug.Log("Internet check: " + InternetCheckFacade._pIsOnline);
        }
    }
}
