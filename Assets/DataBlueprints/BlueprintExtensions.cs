﻿////////////////////////////////////////////
// 
// BlueprintExtensions.cs
//
// Created: 18/05/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////

using System.Collections.Generic;
using UnityEngine;
using AmuzoEngine;
using LoggerAPI = UnityEngine.Debug;

namespace DataBlueprints
{
	public static class BlueprintExtensions
	{
		public static bool HasAllPropertiesOfBlueprint( this object target, DataBlueprint blueprint )
		{
			// ensure blueprint is not null
			if( blueprint == null )
			{
				Debug.LogError( "Blueprint is null!" );
				return false;
			}

			// check all of the blueprint's properties
			foreach( DataBlueprintProperty property in blueprint.GetAllProperties() )
			{
				if( !target.HasDataBlueprintProperty( property ) )
				{
					return false;
				}
			}

			return true;
		}



		public static void SetValuesFromBlueprint( this object target, DataBlueprint blueprint )
		{
			// ensure blueprint is not null
			if( blueprint == null )
			{
				Debug.LogError( "Blueprint is null!" );
				return;
			}

			// set all of the blueprint's properties
			foreach( DataBlueprintProperty property in blueprint.GetAllProperties() )
			{
				if( target.HasDataBlueprintProperty( property ) )
				{
					target.SetFieldOrProperty( property._objectMemberName, property._pIsArray ? property._pValues : property._pValue );
				}
			}
		}



		public static DataBlueprintProperty[] GetPropertiesThatMatchBlueprint( this object target, DataBlueprint blueprint )
		{
			List<DataBlueprintProperty> ret = new List<DataBlueprintProperty>();

			if( blueprint == null )
			{
				return ret.ToArray();
			}

			foreach( DataBlueprintProperty property in blueprint.GetAllProperties() )
			{
				if( target.HasDataBlueprintProperty( property ) )
				{
					ret.Add( property );
				}
			}

			return ret.ToArray();
		}



		public static bool HasPropertiesThatMatchBlueprint( this object target, DataBlueprint blueprint )
		{
			return target.GetPropertiesThatMatchBlueprint( blueprint ).Length > 0;
		}



		public static bool HasDataBlueprintProperty( this object target, DataBlueprintProperty property, bool ignoreType = false )
		{
			return
				target.HasFieldOrProperty(
					property._objectMemberName,
					fi => ignoreType || property.DoesMatchSystemType( fi.FieldType ),
					pi => ignoreType || property.DoesMatchSystemType( pi.PropertyType ) );
		}
	}
}
