﻿//#define COPY_OLD_DATA

using LitJson;
using System;
using UnityEngine;
using AmuzoEngine;
using System.Collections.Generic;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Verbose = NoDebugLogger;


namespace DataBlueprints
{



	[Serializable]
	public class DataBlueprintProperty
	{

		public enum ValueType
		{
			String = JsonType.String,
			Int = JsonType.Int,
			Float = JsonType.Double,
			Bool = JsonType.Boolean,
			Prefab,
			Texture,
			AudioClip,
			Blueprint
		}

		private static Dictionary<ValueType, Type> _valueTypeToSystemType = new Dictionary<ValueType, Type>
		{
			{ ValueType.String,     typeof(string) },
			{ ValueType.Int,        typeof(int) },
			{ ValueType.Float,      typeof(float) },
			{ ValueType.Bool,       typeof(bool) },
			{ ValueType.Prefab,     typeof(GameObject) },
			{ ValueType.Texture,    typeof(Texture2D) },
			{ ValueType.AudioClip,  typeof(AudioClip) },
			{ ValueType.Blueprint,  typeof(string) }
		};

		private class CachedObjectList : IList
		{
			private DataBlueprintProperty	_property;

			private List<object>	_listImpl;
#if UNITY_EDITOR
			private JobCounter	_changes = null;
			private bool		_isInternalChanges = false;
#endif
			public CachedObjectList( DataBlueprintProperty property )
			{
				_property = property;
				_listImpl = new List<object>();
#if UNITY_EDITOR
				_changes = new JobCounter( OnBeginChanges, OnEndChanges );
#endif
			}

			private CachedObjectList()
			{
			}

			public void InitialiseValues()
			{
				_property.GetValues( _listImpl, true );
			}

#if UNITY_EDITOR
			public bool BeginChanges( bool isInternal )
			{
				if ( _changes._pHasJobs && _isInternalChanges != isInternal )
				{
					Debug.LogError( "[DataBlueprintPropery] Values are locked!" );
					return false;
				}
				_isInternalChanges = isInternal;
				_changes.BeginJob();
				return true;
			}

			public void EndChanges()
			{
				_changes.EndJob();
			}

			private void OnBeginChanges()
			{
				_property.OnBeginCachedValuesChanges();
			}

			private void OnEndChanges()
			{
				_isInternalChanges = false;
				_property.OnEndCachedValuesChanges();
			}
#endif

			object IList.this[int index]
			{
				get
				{
					return _listImpl[ index ];
				}

				set
				{
#if UNITY_EDITOR
					BeginChanges( _isInternalChanges );
					_listImpl[ index ] = value;
					EndChanges();
#endif
				}
			}

			int ICollection.Count
			{
				get
				{
					return _listImpl.Count;
				}
			}

			bool IList.IsFixedSize
			{
				get
				{
					return (_listImpl as IList).IsFixedSize;
				}
			}

			bool IList.IsReadOnly
			{
				get
				{
					return (_listImpl as IList).IsReadOnly;
				}
			}

			bool ICollection.IsSynchronized
			{
				get
				{
					return (_listImpl as ICollection).IsSynchronized;
				}
			}

			object ICollection.SyncRoot
			{
				get
				{
					return (_listImpl as ICollection).SyncRoot;
				}
			}

			int IList.Add( object value )
			{
#if UNITY_EDITOR
				BeginChanges( _isInternalChanges );
				int	result = (_listImpl as IList).Add( value );
				EndChanges();
				return result;
#else
				return -1;
#endif
			}

			void IList.Clear()
			{
#if UNITY_EDITOR
				BeginChanges( _isInternalChanges );
				_listImpl.Clear();
				EndChanges();
#endif
			}

			bool IList.Contains( object value )
			{
				return _listImpl.Contains( value );
			}

			void ICollection.CopyTo( Array array, int index )
			{
				(_listImpl as ICollection).CopyTo( array, index );
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return _listImpl.GetEnumerator();
			}

			int IList.IndexOf( object value )
			{
				return _listImpl.IndexOf( value );
			}

			void IList.Insert( int index, object value )
			{
#if UNITY_EDITOR
				BeginChanges( _isInternalChanges );
				_listImpl.Insert( index, value );
				EndChanges();
#endif
			}

			void IList.Remove( object value )
			{
#if UNITY_EDITOR
				BeginChanges( _isInternalChanges );
				_listImpl.Remove( value );
				EndChanges();
#endif
			}

			void IList.RemoveAt( int index )
			{
#if UNITY_EDITOR
				BeginChanges( _isInternalChanges );
				_listImpl.RemoveAt( index );
				EndChanges();
#endif
			}
		}

		public interface IBlueprint
		{
			IDataBlueprintList _pBlueprintList { get; }
		}

		private delegate object DValueTransform( object valIn );

		private const string KEY_NAME = "name";
		private const string KEY_DESCRIPTION = "description";
		private const string KEY_MEMBER_NAME = "objectMemberName";
		private const string KEY_VALUE = "value";
		private const string KEY_TYPE = "type";

		private static DValueTransform	_valueTransformNull = v => v;
		private static DValueTransform	_valueTransformToNull = v => null;
		private static DValueTransform	_valueTransformToBool = v => ((int)v) != 0;
		private static DValueTransform	_valueTransformToUnityObject = v => Resources.Load( (string)v );
#if UNITY_EDITOR
		private static DValueTransform	_valueTransformFromBool = v => v != null ? ((bool)v ? 1 : 0) : 0;
		private static DValueTransform	_valueTransformFromUnityObject = v => v != null ? GetResourcePath( (UnityEngine.Object)v, null ) : "";
		private static DValueTransform	_valueTransformFromUnityObjectWithWarning = v => v != null ? GetResourcePath( (UnityEngine.Object)v, ShowNotResourceDialog ) : "";
#endif

        private IBlueprint   _blueprint;


		public string _name;
		public string _description;
		public string _objectMemberName;

		[SerializeField]
        private ValueType _valueType = ValueType.String;
		public ValueType _pValueType
		{
			get
			{
				return _valueType;
			}
#if UNITY_EDITOR
			set
			{
				SetValueType( value );
			}
#endif
		}

        [SerializeField]
        private bool _isArray;
		public bool	_pIsArray
		{
			get
			{
				return _isArray;
			}
#if UNITY_EDITOR
			set
			{
				_changes.BeginJob();
				_isArray = value;
				_changes.EndJob();
			}
#endif
		}

		[SerializeField]
		private List<string> _valuesString = new List<string>();

		[SerializeField]
		private List<float> _valuesFloat = new List<float>();

		[SerializeField]
		private List<int> _valuesInt = new List<int>();

#if UNITY_EDITOR
		private bool	_isExpandedInEditor = false;

		private List<bool>	_isValueExpandedInEditor = new List<bool>();
#endif

#if COPY_OLD_DATA
		[SerializeField]
		private string _valueString = "";

		[SerializeField]
		private float _valueFloat = 0.0f;

		[SerializeField]
		private int _valueInt = 0;

		[SerializeField]
		private bool _valueBool = false;

		[SerializeField]
		private GameObject _valuePrefab = null;

		[SerializeField]
		private Texture2D _valueTexture = null;

		[SerializeField]
		private AudioClip _valueAudioClip = null;

		[SerializeField]
		private string _valueBlueprintName = null;
#endif

		private CachedObjectList	_cachedValues = null;

        private bool	_isRefreshingCachedValues;

#if UNITY_EDITOR
        private bool	_isRefreshingFromCachedValues;

		private JobCounter	_changes = null;

		public DataBlueprintProperty( DataBlueprint blueprint, string name, string description, string objectMemberName, ValueType type, bool isArray, object value ) : 
			this( blueprint, name, description, objectMemberName, type, isArray, value, true )
		{
		}

		private DataBlueprintProperty( DataBlueprint blueprint, string name, string description, string objectMemberName, ValueType type, bool isArray, object value, bool isTransformValue ) :
			this( blueprint, name, description, objectMemberName, isArray )
		{
			_changes.BeginJob();

			SetValueType( type );

			if ( _isArray )
			{
				SetValues( value as IList, false, isTransformValue );
			}
			else
			{
				SetValue( 0, value, false, isTransformValue );
			}

			_changes.EndJob();
		}

		public DataBlueprintProperty( DataBlueprint blueprint, string name, string description, string objectMemberName, bool isArray )
		{
            _blueprint = blueprint;
			_name = name;
			_description = description;
			_objectMemberName = objectMemberName;
			_isArray = isArray;

			EnsureValidListSize();

			_cachedValues = new CachedObjectList( this );

			_changes = new JobCounter( OnBeginChanges, OnEndChanges );
		}

		public DataBlueprintProperty Clone( DataBlueprint blueprint )
		{
			IList	valueList = GetValueList( _valueType );

			return
				new DataBlueprintProperty(
                    blueprint,
					_name,
					_description,
					_objectMemberName,
					_valueType,
					_isArray,
					_isArray ? valueList : valueList.Count > 0 ? valueList[0] : GetDefaultValue( _valueType, false ),
					isTransformValue:false
				);
		}
#endif

		public DataBlueprintProperty()
		{
			_cachedValues = new CachedObjectList( this );
#if UNITY_EDITOR
			_changes = new JobCounter( OnBeginChanges, OnEndChanges );
#endif
		}

#if UNITY_EDITOR
		public DataBlueprintProperty( DataBlueprint blueprint, JsonData source ) : this( 
            blueprint,
			( string )source[KEY_NAME],
			( string )source[KEY_DESCRIPTION],
			( string )source[KEY_MEMBER_NAME], 
			( ValueType )Enum.Parse( typeof( ValueType ), ( string )source[KEY_TYPE] ),
            source[KEY_VALUE].IsArray,
			JsonDataToSystemType( source[KEY_VALUE] ),
			isTransformValue:false
        )
		{
		}

		private static object JsonDataToSystemType( JsonData jd )
		{
			if ( jd == null ) return null;

			switch ( jd.GetJsonType() )
			{
			case JsonType.Array:
				{
					int	count = jd.Count;
					object[]	objArray = new object[ count ];
					for ( int i = 0; i < count; ++i )
					{
						objArray[i] = JsonDataToSystemType( jd[i] );
					}
					return objArray;
				}
			case JsonType.Boolean:
				return (bool)jd;
			case JsonType.Double:
				return (float)(double)jd;
			case JsonType.Int:
				return (int)jd;
			case JsonType.Long:
				return (long)jd;
			case JsonType.None:
				return null;
			case JsonType.Object:
				throw new System.InvalidOperationException( "Cannot convert JsonType.Object to system type" );
			case JsonType.String:
				return (string)jd;
			default:
				return null;
			}
		}

		public bool _pIsArrayExpandedInEditor
		{
			get
			{
				if ( !_isArray ) throw new System.NotSupportedException();
				return _isExpandedInEditor;
			}
			set
			{
				if ( !_isArray ) throw new System.NotSupportedException();
				_isExpandedInEditor = value;
			}
		}

		public bool _pIsValueExpandedInEditor
		{
			get
			{
				if ( _isArray ) throw new System.NotSupportedException( "Use _pAreValuesExpandedInEditor instead" );
				return _isValueExpandedInEditor[0];
			}
			set
			{
				if ( _isArray ) throw new System.NotSupportedException( "Use _pAreValuesExpandedInEditor instead" );
				_isValueExpandedInEditor[0] = value;
			}
		}

		public IList<bool> _pAreValuesExpandedInEditor
		{
			get
			{
				if ( !_isArray ) throw new System.NotSupportedException( "Use _pIsValueExpandedInEditor instead" );
				return _isValueExpandedInEditor;
			}
		}

		public void Dispose()
		{
			ClearValueList();
		}
#endif

#if COPY_OLD_DATA
		void CopyOldData()
		{
			_valuesString.Clear();
			_valuesFloat.Clear();
			_valuesInt.Clear();

			switch( _valueType )
			{
			case ValueType.String:
				_valuesString.Add( _valueString );
				OnValueAdded(0);
				break;
			case ValueType.Float:
				_valuesFloat.Add( _valueFloat );
				OnValueAdded(0);
				break;
			case ValueType.Int:
				_valuesInt.Add( _valueInt );
				OnValueAdded(0);
				break;
			case ValueType.Bool:
				_valuesInt.Add( _valueBool ? 1 : 0 );
				OnValueAdded(0);
				break;
			case ValueType.Prefab:
				_valuesString.Add( (string)_valueTransformFromUnityObject( _valuePrefab ) );
				OnValueAdded(0);
                break;
			case ValueType.Texture:
				_valuesString.Add( (string)_valueTransformFromUnityObject( _valueTexture ) );
				OnValueAdded(0);
                break;
			case ValueType.AudioClip:
				_valuesString.Add( (string)_valueTransformFromUnityObject( _valueAudioClip ) );
				OnValueAdded(0);
                break;
			case ValueType.Blueprint:
				_valuesString.Add( _valueBlueprintName );
				OnValueAdded(0);
                break;
			}

			BlueprintManager.SetDirty();
		}
#endif

		public void OnBlueprintManagerInitialise( DataBlueprint blueprint )
		{
			Initialize( blueprint );
		}

		private void Initialize( DataBlueprint blueprint )
		{
			Verbose.Log( "[Blueprint] Initialize property: " + _name );

            _blueprint = blueprint;

#if COPY_OLD_DATA
		    CopyOldData();
#endif
			InitialiseCachedValues();

#if UNITY_EDITOR
			SyncValueExpandedInEditorList();
#endif
		}

#if UNITY_EDITOR
		public void OnBlueprintListChanged()
		{
			RefreshCachedValues();
		}
#endif

		public Type _pSystemType
		{
			get
			{
				return _valueTypeToSystemType[_valueType];
			}
		}


		public bool DoesMatchSystemType( System.Type systemType )
		{
			if ( _isArray )
			{
				System.Type	elementType = systemType.GetArrayOrListElementType();

				if ( elementType == _pSystemType ) return true;
			}
			else
			{
				if ( systemType == _pSystemType ) return true;
			}
			return false;
		}


		public object _pValue
		{
			get
			{
				if ( _isArray )
				{
					Debug.LogError( "[DataBlueprintPropery] Using _pValue when property is array.  Use _pValues instead" );
					return null;
				}
				return GetValue( 0, true );
			}

#if UNITY_EDITOR
			set
			{
				if ( _isArray )
				{
					Debug.LogError( "[DataBlueprintPropery] Using _pValue when property is array.  Use _pValues instead" );
					return;
				}
				SetValue( 0, value, false, true );
			}
#endif
		}


		public IList _pValues
		{
			get
			{
				if ( !_isArray )
				{
					Debug.LogError( "[DataBlueprintPropery] Using _pValues when property is not array.  Use _pValue instead" );
					return null;
				}
				return _cachedValues;
			}
		}


		private IList GetValueList( ValueType valueType )
		{
			switch ( valueType )
			{
			case ValueType.Bool:
			case ValueType.Int:
				return _valuesInt;
			case ValueType.Float:
				return _valuesFloat;
			case ValueType.String:
			case ValueType.Prefab:
			case ValueType.Texture:
			case ValueType.AudioClip:
			case ValueType.Blueprint:
				return _valuesString;
			}
			Debug.LogError( "[Blueprint] No value list for type: " + valueType );
			return null;
		}

		private static object GetDefaultValue( ValueType valueType, bool isTransformValue )
		{
			DValueTransform	valueTransform = isTransformValue ? GetValueTransformTo( valueType, null ) : _valueTransformNull;

			object	value = null;

			switch ( valueType )
			{
			case ValueType.Bool:
			case ValueType.Int:
				value = 0;
				break;
			case ValueType.Float:
				value = 0f;
				break;
			case ValueType.String:
			case ValueType.Prefab:
			case ValueType.Texture:
			case ValueType.AudioClip:
			case ValueType.Blueprint:
				value = "";
				break;
			}

			return valueTransform( value );
		}

#if UNITY_EDITOR
		private static DValueTransform GetValueTransformFrom( ValueType valueType )
		{
			switch ( valueType )
			{
			case ValueType.Bool:
				return _valueTransformFromBool;
			case ValueType.Prefab:
			case ValueType.Texture:
			case ValueType.AudioClip:
				return _valueTransformFromUnityObject;
			}
			return _valueTransformNull;
		}
#endif

		private static DValueTransform GetValueTransformTo( ValueType valueType, DataBlueprintProperty property )
		{
			switch ( valueType )
			{
			case ValueType.Bool:
				return _valueTransformToBool;
			case ValueType.Prefab:
			case ValueType.Texture:
			case ValueType.AudioClip:
				return _valueTransformToUnityObject;
			}
			return _valueTransformNull;
		}

		private object GetValue( int index, bool isTransformValue )
		{
			IList	valueList = GetValueList( _valueType );

			if ( valueList == null )
			{
				Debug.LogError( "[Blueprints] No value list for type: " + _valueType );
				return GetDefaultValue( _valueType, isTransformValue );
			}

			DValueTransform	valueTransform = isTransformValue ? GetValueTransformTo( _valueType, this ) : _valueTransformNull;

			return valueTransform( valueList[ index ] );
		}

		private void GetValues( IList dstValues, bool isTransformValue )
		{
			IList	srcValues = GetValueList( _valueType );

			int	countMin = Mathf.Min( srcValues.Count, dstValues.Count );

			for ( int i = 0; i < countMin; ++i )
			{
				dstValues[i] = GetValue( i, isTransformValue );
			}

			for ( int i = countMin; i < srcValues.Count; ++i )
			{
				dstValues.Add( GetValue( i, isTransformValue ) );
			}

			while ( dstValues.Count > srcValues.Count )
			{
				dstValues.RemoveAt( dstValues.Count - 1 );
			}
		}

        private object TransformValueToBlueprint( object value )
        {
			return _blueprint._pBlueprintList.GetBlueprint( (string)value );
        }

		private void InitialiseCachedValues()
		{
			_cachedValues.InitialiseValues();

			Verbose.Log( "[Blueprints] Initialised cached values: '" + _name + "', " + ( _cachedValues as IList ).Count + " cached values" );
		}

#if UNITY_EDITOR

		private void SetValueType( ValueType newValueType )
		{
			if ( _valueType == newValueType ) return;

			_changes.BeginJob();

			ClearValueList();

			_valueType = newValueType;

			ClearValueList();

			_changes.EndJob();
		}

		private void SetValueTypeFromValue( object value )
		{
			if ( value == null ) return;

			if ( value is string )
			{
				SetValueType( ValueType.String );
			}
			else if ( value is float )
			{
				SetValueType( ValueType.Float );
			}
			else if ( value is int )
			{
				SetValueType( ValueType.Int );
			}
			else if ( value is bool )
			{
				SetValueType( ValueType.Bool );
			}
			else if ( value is GameObject )
			{
				SetValueType( ValueType.Prefab );
			}
			else if ( value is Texture2D )
			{
				SetValueType( ValueType.Texture );
			}
			else if ( value is AudioClip )
			{
				SetValueType( ValueType.AudioClip );
			}
			else if ( value is DataBlueprint )
			{
				SetValueType( ValueType.Blueprint );
			}
			else
			{
				throw new Exception( "Unhandled object type: " + value.GetType() );
			}
		}

		private void SetValue( int index, object value, bool isSetValueType, bool isTransformValue )
		{
			_changes.BeginJob();

			if ( isSetValueType )
			{
				SetValueTypeFromValue( value );
			}

			SetValueListSizeMin( index + 1 );

			IList	valueList = GetValueList( _valueType );

			DValueTransform	valueTransform = isTransformValue ? GetValueTransformFrom( _valueType ) : _valueTransformNull;

			value = valueTransform( value );

			if ( value != null )
			{
				valueList[ index ] = value;
			}

			_changes.EndJob();
		}

		private void SetValues( IList srcValues, bool isSetValueType, bool isTransformValue )
		{
			_changes.BeginJob();

			for ( int i = 0; i < srcValues.Count; ++i )
			{
				SetValue( i, srcValues[i], isSetValueType, isTransformValue );
			}

			SetValueListSizeMax( srcValues.Count );

			_changes.EndJob();
		}

		private void OnBeginChanges()
		{
			IList	valueList = GetValueList( _valueType );

			Verbose.Log( "[Blueprints] Begin prop changes: '" + _name + "', " + valueList.Count + " of " + _valueType );
		}

		private void OnEndChanges()
		{
			EnsureValidListSize();

			IList	valueList = GetValueList( _valueType );

			Verbose.Log( "[Blueprints] End prop changes: '" + _name + "', " + valueList.Count + " of " + _valueType );

			if ( !_isRefreshingFromCachedValues )
			{
				RefreshCachedValues();
			}
		}

		private void OnBeginCachedValuesChanges()
		{
			Verbose.Log( "[Blueprints] Begin cached values changes: '" + _name + "', " + ( _cachedValues as IList ).Count + " values" );
		}

		private void OnEndCachedValuesChanges()
		{
			Verbose.Log( "[Blueprints] End cached values changes: '" + _name + "', " + ( _cachedValues as IList ).Count + " values" );

			if ( !_isRefreshingCachedValues )
			{
				RefreshFromCachedValues();
			}
		}

		private void RefreshCachedValues()
		{
			if ( _isRefreshingCachedValues )
			{
				Debug.LogError( "[Blueprints] Already refreshing cached values" );
				return;
			}

			_isRefreshingCachedValues = true;

			Verbose.Log( "[Blueprints] Begin refresh cached values: '" + _name + "', " + ( _cachedValues as IList ).Count + " cached values" );

			_cachedValues.BeginChanges( isInternal:true );

			GetValues( _cachedValues, true );

			_cachedValues.EndChanges();

			Verbose.Log( "[Blueprints] End refresh cached values: '" + _name + "', " + ( _cachedValues as IList ).Count + " cached values" );

			_isRefreshingCachedValues = false;
		}

		private void RefreshFromCachedValues()
		{
			if ( _isRefreshingFromCachedValues )
			{
				Debug.LogError( "[Blueprints] Already refreshing from cached values" );
				return;
			}

			_isRefreshingFromCachedValues = true;

			Verbose.Log( "[Blueprints] Refresh from cached values: '" + _name + "', " + ( _cachedValues as IList ).Count + " cached values" );

			SetValues( _cachedValues, false, true );

			//ensure cached values are valid representations of internal values
			RefreshCachedValues();

			_isRefreshingFromCachedValues = false;
		}

		private void EnsureValidListSize()
		{
			if ( !_isArray )
			{
				SetValueListSize( 1 );
			}
		}

		private void ClearValueList()
		{
			SetValueListSize( 0 );
		}

		private void SetValueListSize( int newSize )
		{
			if ( newSize < 0 )
			{
				Debug.LogError( "[Blueprints] Invalid list size: " + newSize );
				return;
			}

			SetValueListSizeMin( newSize );
			SetValueListSizeMax( newSize );
		}

		private void SetValueListSizeMin( int sizeMin )
		{
			IList	valueList = GetValueList( _valueType );
			object	defaultValue = GetDefaultValue( _valueType, false );

			valueList.SetCountAtLeast( sizeMin, defaultValue, onAdded:OnValueAdded );
		}

		private void SetValueListSizeMax( int sizeMax )
		{
			if ( sizeMax < 0 )
			{
				Debug.LogError( "[Blueprints] Invalid list max size: " + sizeMax );
				return;
			}

			IList	valueList = GetValueList( _valueType );

			valueList.SetCountAtMost( sizeMax, onPreRemove:OnRemoveValue, onRemoved:OnValueRemoved );
		}

		private void OnValueAdded( int index )
		{
			SyncValueExpandedInEditorList();
		}

		private void OnRemoveValue( int index )
		{
		}

		private void OnValueRemoved( int index )
		{
			SyncValueExpandedInEditorList();
		}


		// --------------------------------------------- EDITOR STUFF ---------------------------------------------

		private void SyncValueExpandedInEditorList()
		{
			IList	valueList = GetValueList( _valueType );

			if ( valueList == null ) return;

			int	valueCount = valueList.Count;

			_isValueExpandedInEditor.SetCount( valueCount, defaultValue:false );
		}

		private const string RESOURCES_SUBSTRING = "/resources/";

		private static string GetResourcePath( UnityEngine.Object obj, System.Action onNotResource = null )
		{
			string path = AssetDatabase.GetAssetPath( obj );
			int indexOfResources = path.ToLower().IndexOf( RESOURCES_SUBSTRING );

			if( indexOfResources == -1 )
			{
				if ( onNotResource != null )
				{
					onNotResource();
				}

				return null;
			}

			// extract resources path
			int startIndex = indexOfResources + RESOURCES_SUBSTRING.Length;
			int endIndex = path.LastIndexOf( '.' );
			int length = endIndex - startIndex;

			return path.Substring( startIndex, length );
		}

		private static void ShowNotResourceDialog()
		{
			EditorUtility.DisplayDialog(
				"Warning",
				"Must be in a resources folder",
				"OK"
			);
		}

		public JsonData _pJson
		{
			get
			{
				JsonData ret = new JsonData();

				ret[KEY_NAME] = _name;
				ret[KEY_DESCRIPTION] = _description;
				ret[KEY_MEMBER_NAME] = _objectMemberName;
				ret[KEY_TYPE] = _valueType.ToString();

				IList	valueList = GetValueList( _valueType );
				object	value;

				if ( _isArray )
				{
					JsonData	arrayData = new JsonData();

					arrayData.SetJsonType( JsonType.Array );

					for ( int i = 0; i < valueList.Count; ++i )
					{
						arrayData.Add( valueList[i] ?? "" );
					}

					ret[KEY_VALUE] = arrayData;
				}
				else
				{
					value = valueList.Count > 0 ? valueList[0] : null;
					ret[KEY_VALUE] = new JsonData( value ?? "" );
				}

				return ret;
			}
		}
#endif

	}

}
