﻿//#define COPY_OLD_DATA

using UnityEngine;
using AmuzoEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.Text.RegularExpressions;

#if UNITY_EDITOR
using UnityEditor;
using System.Reflection;
#endif


namespace DataBlueprints
{

	public class BlueprintManager : AmuzoScriptableSingleton<BlueprintManager>
#if UNITY_EDITOR
		, DataBlueprintList.IListener
#endif
	{

		private const string JSON_ASSET_RESOURCE_NAME = "DataBlueprints";
		private const string DEFAULT_JSON_ASSET_RESOURCE_DIR = "Assets/DataBlueprints/Resources";
		private const string NEW_BLUEPRINT_NAME = "New Blueprint";

		public static bool _setDirtyOnInitialise = false;

		[SerializeField]
		private DataBlueprintList _blueprintList = new DataBlueprintList();

#if COPY_OLD_DATA
		[SerializeField]
		private List<DataBlueprint> _list;
#endif

		public static DataBlueprint GetBlueprint( string name )
		{
			return _pBlueprintsReadOnly.GetBlueprint( name );
		}


#if UNITY_EDITOR
		public static void BeginChanges( System.Action onEnd = null )
		{
			_pBlueprintsWritable.BeginChanges( onEnd );
		}


		public static void EndChanges()
		{
			_pBlueprintsWritable.EndChanges();
		}


		public static void DeleteBlueprint( string blueprintName, bool deleteChildren = false )
		{
			DeleteBlueprint( GetBlueprint( blueprintName ), deleteChildren );
		}


		public static void DeleteBlueprint( DataBlueprint blueprint, bool deleteChildren = false )
		{
			_pBlueprintsWritable.DeleteBlueprint( blueprint, deleteChildren );
		}


		public static void DeleteAllBlueprints()
		{
			_pBlueprintsWritable.DeleteAllBlueprints();
		}


		public static void ReplaceBlueprint( DataBlueprint oldBlueprint, DataBlueprint newBlueprint )
		{
			_pBlueprintsWritable.ReplaceBlueprint( oldBlueprint, newBlueprint );
		}


		public static DataBlueprint AddBlueprint( string name = null, string parent = null )
		{
			DataBlueprint	newBlueprint = NewBlueprint( name, parent );
			_pBlueprintsWritable.AddBlueprint( newBlueprint );
			return newBlueprint;
		}


		public static DataBlueprint NewBlueprint( DataBlueprint source )
		{
			return _pBlueprintsWritable.NewBlueprint( source );
		}


		public static DataBlueprint NewBlueprint( string name = null, string parent = null )
		{
			return _pBlueprintsWritable.NewBlueprint( name ?? _pNextNewBlueprintName, parent );
		}


		public static void CopyBlueprint( DataBlueprint source )
		{
			_pBlueprintsWritable.CopyBlueprint( source );
		}


		public static DataBlueprintList	_pImportList { get; private set; }

		public static bool _pIsImporting
		{
			get
			{
				return _pImportList != null;
			}
		}

		public static void BeginImport( JsonData importData )
		{
			if ( _pIsImporting )
			{
				Debug.LogError( "[BlueprintManager] Already importing" );
				return;
			}

			_pImportList = new DataBlueprintList();
			_pImportList.ImportBlueprints( importData );

			//IReadOnlyList<DataBlueprint>[]	nameCheckLists = new IReadOnlyList<DataBlueprint>[] { _pBlueprintsReadOnly, _pImportList };

			//_pImportList.ResolveNameConflicts( _pBlueprintsReadOnly, ( importBp, mainBp ) => {
			//	string	newName = GetNextDuplicatedBlueprintName( importBp._pName, nameCheckLists );
			//	importBp.SetName( newName );
			//} );
		}

		public static void EndImport()
		{
			if ( !_pIsImporting ) return;

			_pImportList.DeleteAllBlueprints();
			_pImportList = null;
		}


		public static JsonData ToJsonData()
		{
			JsonData json = new JsonData();

			// get json object
			_pBlueprintsReadOnly.ForEach( blueprint => json[blueprint._pName] = blueprint._pJson );

			return json;
		}


		public static new void SetDirty()
		{
			if ( _pExists )
			{
				EditorUtility.SetDirty( _pInstance );
			}
			else
			{
				_setDirtyOnInitialise = true;
			}
		}


		private static Regex	_sDuplicateBlueprintNameRegex = new Regex( @"(?<root>.*\S)\s*\((?<num>[0-9]+)\)\s*" );

		private static void ParseDuplicateBlueprintName( string name, out string root, out int index )
		{
			Match	match = _sDuplicateBlueprintNameRegex.Match( name );

			if ( match.Success )
			{
				root = match.Groups[ "root" ].Value;
				index = int.Parse( match.Groups[ "num" ].Value );
			}
			else
			{
				root = name;
				index = 0;
			}
		}

		public static string GetNextDuplicatedBlueprintName( string name, IReadOnlyList<DataBlueprint>[] checkLists )
		{
			string	nameRoot;
			int		nameIndex;

			ParseDuplicateBlueprintName( name, out nameRoot, out nameIndex );

			string	checkRoot;
			int		checkIndex;
			int		maxIndex = 0;

			for ( int i = 0; i < checkLists.Length; ++i )
			{
				for ( int j = 0; j < checkLists[i].Count; ++j )
				{
					ParseDuplicateBlueprintName( checkLists[i][j]._pName, out checkRoot, out checkIndex );

					if ( checkRoot.Equals( nameRoot ) )
					{
						if ( checkIndex > maxIndex )
						{
							maxIndex = checkIndex;
						}
					}
				}
			}

			return string.Format( "{0} ({1})", nameRoot, maxIndex + 1 );
		}

		// ----------------------------------------- AmuzoScriptableSingleton --------------------------------------

		protected override void Initialise()
		{
			base.Initialise();

			if ( _setDirtyOnInitialise )
			{
				SetDirty();
				_setDirtyOnInitialise = false;
			}
		}
#endif//UNITY_EDITOR

		void OnEnable()
		{
#if COPY_OLD_DATA
			CopyOldData();
#endif

			if ( _blueprintList != null )
			{
#if UNITY_EDITOR
				_blueprintList._pListener = this;
#endif
				_blueprintList.OnBlueprintManagerInitialise();
			}
		}

		// ----------------------------------------------- PROPERTIES -----------------------------------------------

#if UNITY_EDITOR
		private static DataBlueprintList _pBlueprintsWritable
		{
			get
			{
				EnsureListListener();
				return _pInstance._blueprintList;
			}
		}
#endif

		public static IDataBlueprintList _pBlueprintsReadOnly
		{
			get
			{
				return _pInstance._blueprintList;
			}
		}


#if UNITY_EDITOR
		public static string _pNextNewBlueprintName
		{
			get
			{
				int highestNumber = -1;

				foreach ( DataBlueprint blueprint in _pBlueprintsReadOnly )
				{
					if ( blueprint._pName.StartsWith( NEW_BLUEPRINT_NAME ) )
					{
						string remainder = blueprint._pName.Substring( NEW_BLUEPRINT_NAME.Length ).Trim();

						if ( remainder.Length == 0 )
						{
							highestNumber = Mathf.Max( 1, highestNumber );
						}
						else
						{
							int num;
							if ( int.TryParse( remainder, out num ) )
							{
								highestNumber = Mathf.Max( num, highestNumber );
							}
						}
					}
				}

				return NEW_BLUEPRINT_NAME + ( highestNumber >= 0 ? " " + ( highestNumber + 1 ) : "" );
			}
		}

		private static void EnsureListListener()
		{
			_pInstance._blueprintList._pListener = _pInstance;
		}

		void DataBlueprintList.IListener.OnChanged()
		{
			SetDirty();
		}
#endif

#if COPY_OLD_DATA
		private void CopyOldData()
		{
			_blueprintList = new DataBlueprintList();

			for ( int i = 0; i < _list.Count; ++i )
			{
				if ( _list[i] != null )
				{
					_blueprintList.RawAdd( _list[i], "copy old data" );
				}
			}

			SetDirty();
		}
#endif
	}


}
