﻿////////////////////////////////////////////
// 
// DataBlueprintList.cs
//
// Created: 04/08/2016 ccuthbert
// Contributors:
// 
// Intention:
// 
//
// Note: 
// 
//
// Remark: <symbol>
// Single line comment in the code for
// items that correspond to this remark
// like so: // ?<symbol>
//
////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AmuzoEngine;
using LitJson;
using LoggerAPI = UnityEngine.Debug;

namespace DataBlueprints
{
	public interface IDataBlueprintListReadOnly : IReadOnlyList<DataBlueprint>
	{
		DataBlueprint	GetBlueprint( string name );
	}

	public interface IDataBlueprintList : IDataBlueprintListReadOnly
	{
#if UNITY_EDITOR
		DataBlueprint	NewBlueprint( string name, string parent = null );

		void	DeleteBlueprint( DataBlueprint blueprint, bool deleteChildren = false );
			
		void	OnBlueprintNameChanged( string oldName, string newName );
#endif//UNITY_EDITOR
	}

	[System.Serializable]
	public class DataBlueprintList : IDataBlueprintList
	{
		//
		// CONSTS / DEFINES / ENUMS
		//
	
		private const string	LOG_TAG = "[DataBlueprintList] ";

		//
		// SINGLETON DECLARATION
		//

		//
		// NESTED CLASSES / STRUCTS
		//

#if UNITY_EDITOR
		public interface IListener
		{
			void	OnChanged();
		}
#endif
			
		//
		// MEMBERS 
		//

		[SerializeField]
		private List<DataBlueprint> _listInternal = null;

#if UNITY_EDITOR
		private JobCounter	_changes = null;
#endif

		//
		// PROPERTIES
		//

		public DataBlueprint this[int index]
		{
			get
			{
				if ( _listInternal == null ) return null;
				return _listInternal[index];
			}

#if UNITY_EDITOR
			set
			{
				if ( _listInternal == null ) return;
				BeginChanges();
				_listInternal[index] = value;
				EndChanges();
			}
#endif
		}

#if UNITY_EDITOR
		public IListener _pListener { get; set; }
#endif

		//
		// ABSTRACTS / VIRTUALS
		//

		//
		// STATIC FUNCTIONS
		//

		//
		// FUNCTIONS 
		//

		// ** PUBLIC
		//

		public DataBlueprintList()
		{
			_listInternal = new List<DataBlueprint>();
#if UNITY_EDITOR
			_changes = new JobCounter( onEnd:() => {
				OnBlueprintListChanged();
				if ( _pListener != null ) _pListener.OnChanged();
			} );
#endif
		}

#if UNITY_EDITOR
		public void Add( DataBlueprint item )
		{
			if ( _listInternal == null ) return;
			BeginChanges();
			_listInternal.Add( item );
			EndChanges();
		}

		public void RawAdd( DataBlueprint item, string reason )
		{
			if ( string.IsNullOrEmpty( reason ) ) throw new System.Exception( "Must provide reason for raw add" );
			if ( _listInternal == null ) return;
			_listInternal.Add( item );
		}

		public void Clear()
		{
			if ( _listInternal == null ) return;
			BeginChanges();
			for ( int i  = 0; i < _listInternal.Count; ++i )
			{
				_listInternal[i].Dispose();
				_listInternal[i] = null;
			}
			_listInternal.Clear();
			EndChanges();
		}
#endif

		public bool Contains( DataBlueprint item )
		{
			if ( _listInternal == null ) return false;
			return _listInternal.Contains( item );
		}

		public bool Contains( string name )
		{
			return IndexOf( name ) >= 0;
		}

		public void CopyTo( DataBlueprint[] array, int arrayIndex )
		{
			if ( _listInternal == null ) return;
			_listInternal.CopyTo( array, arrayIndex );
		}

		public IEnumerator<DataBlueprint> GetEnumerator()
		{
			if ( _listInternal == null ) return null;
			return _listInternal.GetEnumerator();
		}

		public int IndexOf( DataBlueprint item )
		{
			if ( _listInternal == null ) return -1;
			return _listInternal.IndexOf( item );
		}

		public int IndexOf( System.Predicate<DataBlueprint> match )
		{
			if ( _listInternal == null ) return -1;
			if ( match == null ) return -1;
			for ( int i = 0; i < _listInternal.Count; ++i )
			{
				 if ( match( _listInternal[i] ) ) return i;
			}
			return -1;
		}

		public int IndexOf( string name )
		{
			return IndexOf( bp => bp._pName == name );
		}

#if UNITY_EDITOR
		public void Insert( int index, DataBlueprint item )
		{
			if ( _listInternal == null ) return;
			BeginChanges();
			_listInternal.Insert( index, item );
			EndChanges();
		}

		public bool Remove( DataBlueprint item )
		{
			if ( _listInternal == null ) return false;
			BeginChanges();
			bool	result = _listInternal.Remove( item );
			EndChanges();
			return result;
		}

		public void RemoveAt( int index )
		{
			if ( _listInternal == null ) return;
			BeginChanges();
			_listInternal.RemoveAt( index );
			EndChanges();
		}
#endif

		public DataBlueprint GetBlueprint( string name )
		{
			if ( string.IsNullOrEmpty( name ) ) return null;
			int	index = IndexOf( name );
			return index >= 0 ? this[ index ] : null;
		}

#if UNITY_EDITOR
		public void DeleteBlueprint( string blueprintName, bool deleteChildren = false )
		{
			DeleteBlueprint( GetBlueprint( blueprintName ), deleteChildren );
		}

		public void DeleteBlueprint( DataBlueprint blueprint, bool deleteChildren = false )
		{
			if ( blueprint == null ) return;

			if ( !Contains( blueprint ) )
			{
				Debug.LogError( string.Format( "Can't delete blueprint \"{0}\" - does not exist", blueprint._pName ) );
				return;
			}

			BeginChanges();

			blueprint.Dispose();

			Remove( blueprint );

			// children
			foreach ( DataBlueprint child in blueprint.GetChildren() )
			{
				child._parentBlueprintName = null;

				if ( deleteChildren )
				{
					DeleteBlueprint( child, true );
				}
			}

			EndChanges();
		}

		public void DeleteAllBlueprints()
		{
			BeginChanges();

			Clear();

			EndChanges();
		}

		public void ReplaceBlueprint( DataBlueprint oldBlueprint, DataBlueprint newBlueprint )
		{
			if ( oldBlueprint == null ) throw new System.ArgumentNullException( "oldBlueprint" );
			if ( newBlueprint == null ) throw new System.ArgumentNullException( "newBlueprintSource" );
			if ( oldBlueprint._pName != newBlueprint._pName ) throw new System.ArgumentException( "oldBlueprint and newBlueprintSource must have same name" );
			if ( oldBlueprint._pBlueprintList != this ) throw new System.ArgumentException( "oldBlueprint must have correct list" );
			if ( newBlueprint._pBlueprintList != this ) throw new System.ArgumentException( "newBlueprint must have correct list" );

			int	index = IndexOf( oldBlueprint );

			if ( index < 0 ) return;

			BeginChanges();

			oldBlueprint.Dispose();

			RemoveAt( index );

			AddBlueprint( newBlueprint, index );

			EndChanges();
		}

		public DataBlueprint NewBlueprint( DataBlueprint source )
		{
			return new DataBlueprint( this, source );
		}

		public DataBlueprint NewBlueprint( string name, string parent = null )
		{
			return new DataBlueprint( this, name, parent );
		}

		public void CopyBlueprint( DataBlueprint source )
		{
			DataBlueprint	newBlueprint = NewBlueprint( source );
			AddBlueprint( newBlueprint );
		}

		public void AddBlueprint( DataBlueprint blueprint, int atIndex = -1 )
		{
			if ( atIndex == -1 )
			{
				Add( blueprint );
			}
			else
			{
				Insert( atIndex, blueprint );
			}
		}


		public void ImportBlueprints( JsonData importData )
		{
			BeginChanges();

			foreach ( DictionaryEntry kvp in (IDictionary)importData )
			{
				AddBlueprint( new DataBlueprint( this, (string)kvp.Key, (JsonData)kvp.Value ) );
			}

			EndChanges();
		}

		public delegate void	DNameConflictResolver( DataBlueprint thisBlueprint, DataBlueprint otherBlueprint );

		public void ResolveNameConflicts( IReadOnlyList<DataBlueprint> otherList, DNameConflictResolver resolver )
		{
			if ( resolver == null ) return;

			this.ForEach( bp => {
				otherList.ForEach( otherBp => {
					if ( bp._pName == otherBp._pName )
					{
						resolver( bp, otherBp );
					}
				} );
			} );
		}
#endif//UNITY_EDITOR

		public void OnBlueprintManagerInitialise()
		{
			this.ForEach( bp => bp.OnBlueprintManagerInitialise( this ) );
		}

#if UNITY_EDITOR
		public void OnBlueprintNameChanged( string oldName, string newName )
		{
			this.ForEach( bp => bp.OnBlueprintNameChanged( oldName, newName ) );
		}

		public void OnBlueprintListChanged()
		{
			this.ForEach( bp => bp.OnBlueprintListChanged() );
		}

		public void BeginChanges( System.Action onEnd = null )
		{
			_changes.BeginJob( onEnd );
		}

		public void EndChanges()
		{
			_changes.EndJob();
		}
#endif

		// ** PROTECTED
		//

		// ** PRIVATE
		//

		//
		// INTERFACE IMPLEMENTATIONS
		//

		IEnumerator IEnumerable.GetEnumerator()
		{
			if ( _listInternal == null ) return null;
			return _listInternal.GetEnumerator();
		}

		public int Count
		{
			get
			{
				if ( _listInternal == null ) return 0;
				return _listInternal.Count;
			}
		}

#if UNITY_EDITOR
		public bool IsReadOnly
		{
			get
			{
				if ( _listInternal == null ) return false;
				return ( (IList<DataBlueprint>)_listInternal ).IsReadOnly;
			}
		}
#endif

	}
}
