﻿using GameDefines;
using LitJson;
using UnityEngine;
using AmuzoEngine;
using System;
using System.Collections.Generic;


namespace DataBlueprints
{

	public enum EDeletePropertyType
	{
		UntilOverride,
		MoveToChildren,
		All
	}


	[Serializable]
	public class DataBlueprint : DataBlueprintProperty.IBlueprint
	{

		private const string KEY_PARENT_BLUEPRINT = "parentBlueprint";
		private const string KEY_PROPERTIES = "properties";
		private const string NONE = "[None]";
		private const string NEW_PROPERTY_NAME = "Property";

		private IDataBlueprintList   _blueprintList;
		public IDataBlueprintList _pBlueprintList
		{
			get
			{
				return _blueprintList;
			}
		}

		[SerializeField]
		private string	_name;
		public string _pName
		{
			get
			{
				return _name;
			}
		}

		public string _parentBlueprintName;

		[SerializeField]
		private List<DataBlueprintProperty> _properties = new List<DataBlueprintProperty>();

		private ReadOnlyListWrapper<DataBlueprintProperty>	_propertiesReadOnly = null;
		public IReadOnlyList<DataBlueprintProperty>	_pPropertiesReadOnly
		{
			get
			{
				if ( _propertiesReadOnly == null )
				{
					_propertiesReadOnly = new ReadOnlyListWrapper<DataBlueprintProperty>( () => _properties );
				}
				return _propertiesReadOnly;
			}
		}

#if UNITY_EDITOR
		public bool _pIsExpandedInTreeView { get; set; }
		public bool _pHasRenderedProperties { get; set; }
#endif

		

		public DataBlueprint()
		{
			_properties = new List<DataBlueprintProperty>();
		}


#if UNITY_EDITOR
		public void Dispose()
		{
			ClearProperties();
		}

		public DataBlueprint( IDataBlueprintList blueprintList, DataBlueprint source )
		{
			_blueprintList = blueprintList;
			_name = source._name;
			
			CopyBlueprint( source );
		}

		public DataBlueprint( IDataBlueprintList blueprintList, string name, string parent )
		{
			_blueprintList = blueprintList;
			_name = name;
			_parentBlueprintName = parent;
		}

		public DataBlueprint( IDataBlueprintList blueprintList, string name, JsonData source )
		{
			_blueprintList = blueprintList;
			_name = name;

			if( source.Contains( KEY_PARENT_BLUEPRINT ) )
			{
				_parentBlueprintName = ( string )source[KEY_PARENT_BLUEPRINT];
			}
			else
			{
				_parentBlueprintName = null;
			}

			_properties = new List<DataBlueprintProperty>();

			for( int i = 0; i < source[KEY_PROPERTIES].Count; ++i )
			{
				_properties.Add( new DataBlueprintProperty( this, source[KEY_PROPERTIES][i] ) );
			}
		}
#endif//UNITY_EDITOR



		/// <summary>
		/// Includes owned and inherited properties, i.e. the full list of properties associated with this blueprint
		/// </summary>
		public DataBlueprintProperty[] GetAllProperties( bool includeInheritedDuplicates = false )
		{
			List<DataBlueprintProperty> ret = new List<DataBlueprintProperty>();

			DataBlueprint	parent = GetParent();

			// parent blueprint
			if( parent != null )
			{
				// for each inherited property, add to the return array if we don't have property with same name
				foreach( DataBlueprintProperty inheritedPropery in parent.GetAllProperties( includeInheritedDuplicates ) )
				{
					if( includeInheritedDuplicates || GetProperty( inheritedPropery._name ) == null )
					{
						ret.Add( inheritedPropery );
					}
				}
			}

			ret.AddRange( _properties );

			return ret.ToArray();
		}



		public DataBlueprintProperty GetProperty( string name, bool includeAncestors = false )
		{
			// owned properties
			foreach( DataBlueprintProperty property in _properties )
			{
				if( property._name == name )
				{
					return property;
				}
			}

			DataBlueprint	parent = GetParent();

			// ancestors' properties
			if( includeAncestors && parent != null )
			{
				return parent.GetProperty( name, true );
			}

			return null;
		}



		public DataBlueprintProperty GetPropertyByObjectMemberName( string objectMemberName, bool includeAncestors = false )
		{
			if( string.IsNullOrEmpty( objectMemberName ) )
			{
				return null;
			}

			// owned properties
			foreach( DataBlueprintProperty property in _properties )
			{
				if( property._objectMemberName == objectMemberName )
				{
					return property;
				}
			}

			DataBlueprint	parent = GetParent();

			// ancestors' properties
			if( includeAncestors && parent != null )
			{
				return parent.GetPropertyByObjectMemberName( objectMemberName, true );
			}

			return null;
		}



		public DataBlueprint GetRootPropertyOwner( string propertyName )
		{
			DataBlueprint rootOwner = this;
			DataBlueprint tmp = this;

			do
			{
				if( tmp.GetProperty( propertyName ) != null )
				{
					rootOwner = tmp;
				}
				tmp = tmp.GetParent();
			}
			while( tmp != null );

			return rootOwner;
		}



		public bool HasBlueprintAsAncestor( DataBlueprint blueprint )
		{
			DataBlueprint	parent = GetParent();
			return
				blueprint == this
					? true
					: parent != null
						? parent.HasBlueprintAsAncestor( blueprint )
						: false;
		}



#if UNITY_EDITOR
		public DataBlueprintProperty NewProperty()
		{
			DataBlueprintProperty	p = new DataBlueprintProperty( this, GetNextNewPropertyName(), "", "", false );
			_properties.Add( p );
			return p;
		}

		public DataBlueprintProperty NewProperty( string name, string description, string objectMemberName, DataBlueprintProperty.ValueType type, bool isArray, object value )
		{
			DataBlueprintProperty	p = new DataBlueprintProperty( this, name, description, objectMemberName, type, isArray, value );
			_properties.Add( p );
			return p;
		}

		public void DeleteProperty( DataBlueprintProperty property, EDeletePropertyType type )
		{
			if( property == null )
			{
				return;
			}

			DeleteProperty( property._name, type );
		}



		public void DeleteProperty( string name, EDeletePropertyType type )
		{
			DataBlueprintProperty property = GetProperty( name );

			if( property != null )
			{
				_properties.Remove( property );
			}

			switch( type )
			{
				case EDeletePropertyType.MoveToChildren:
					if( property == null )
					{
						break;
					}
					foreach( DataBlueprint child in GetChildren() )
					{
						if( child.GetProperty( name ) == null )
						{
							child._properties.Add( property.Clone( child ) );
						}
					}
					break;

				case EDeletePropertyType.All:
					foreach( DataBlueprint child in GetChildren() )
					{
						child.DeleteProperty( name, EDeletePropertyType.All );
					}
					break;
			}

			if( property != null )
			{
				property.Dispose();
			}
		}


		public static string NameOrNone( DataBlueprint blueprint )
		{
			return ( blueprint != null ? blueprint._pName : NONE );
		}

#endif


		public DataBlueprint GetParent()
		{
			return _blueprintList.GetBlueprint( _parentBlueprintName );
		}


#if UNITY_EDITOR
		public void SetParent( DataBlueprint parent )
		{
			_parentBlueprintName = ( parent != null ? parent._pName : null );
		}


		public DataBlueprint[] GetChildren()
		{
			List<DataBlueprint> ret = new List<DataBlueprint>();

			foreach( DataBlueprint blueprint in _blueprintList )
			{
				if( blueprint._parentBlueprintName == _pName )
				{
					ret.Add( blueprint );
				}
			}

			return ret.ToArray();
		}



		public bool HasChildren( )
		{
			return GetChildren().Length > 0;
		}


		public DataBlueprint GetNextSibling()
		{
			DataBlueprint	parent = GetParent();

			if( parent == null )
			{
				return null;
			}

			DataBlueprint[]	siblings = parent.GetChildren();

			bool foundThis = false;

			foreach( DataBlueprint sibling in siblings )
			{
				if( sibling == this )
				{
					foundThis = true;
				}
				else if( foundThis )
				{
					return sibling;
				}
			}

			return null;
		}


		public string GetNextNewPropertyName()
		{
			int highestNumber = -1;

			foreach( DataBlueprintProperty property in GetAllProperties( false ) )
			{
				if( property._name.StartsWith( NEW_PROPERTY_NAME ) )
				{
					string remainder = property._name.Substring( NEW_PROPERTY_NAME.Length ).Trim();

					if( remainder.Length == 0 )
					{
						highestNumber = Mathf.Max( 1, highestNumber );
					}
					else
					{
						int num;
						if( int.TryParse( remainder, out num ) )
						{
							highestNumber = Mathf.Max( num, highestNumber );
						}
					}
				}
			}

			return NEW_PROPERTY_NAME + ( highestNumber >= 0 ? ( highestNumber + 1 ).ToString() : "" );
		}


		public int GetInheritanceLevel()
		{
			DataBlueprint	parent = GetParent();
			return parent != null ? parent.GetInheritanceLevel() + 1 : 0;
		}


		public string GetInheritancePath()
		{
			DataBlueprint	parent = GetParent();
			return ( !string.IsNullOrEmpty( _parentBlueprintName ) ? parent.GetInheritancePath() + "/" : "" ) + _pName;
		}



		public string GetPropertiesInfo()
		{
			if( _properties.Count == 0 )
			{
				return "Contains no properties";
			}

			string ret = "Properties:";

			foreach( DataBlueprintProperty property in _properties )
			{
				ret += string.Format( "\n• {0}: {1}", property._name, property._pIsArray ? property._pValues : property._pValue );
			}

			return ret;
		}



		public void UpdateChildrensOverridingProperties( IList<DataBlueprintProperty> rootProperties = null )
		{
			if( rootProperties == null )
			{
				rootProperties = _properties;
			}

			foreach( DataBlueprint child in GetChildren() )
			{
				foreach( DataBlueprintProperty rootProperty in rootProperties )
				{
					DataBlueprintProperty overridingProperty = child.GetProperty( rootProperty._name );

					if( overridingProperty != null )
					{
						overridingProperty._objectMemberName = rootProperty._objectMemberName;
						overridingProperty._description = rootProperty._description;
						overridingProperty._pValueType = rootProperty._pValueType;
						overridingProperty._pIsArray = rootProperty._pIsArray;
					}
				}

				child.UpdateChildrensOverridingProperties( rootProperties );
			}
		}



		/// <summary>
		/// Finds properties in children whose _name is currName and sets it to newName.
		/// Should be called when changing _name of a property of this blueprint.
		/// </summary>
		public void SetNameOfPropertyInChildren( string currName, string newName )
		{
			foreach( DataBlueprint child in GetChildren() )
			{
				DataBlueprintProperty overridingProperty = child.GetProperty( currName );

				if( overridingProperty != null )
				{
					overridingProperty._name = newName;
				}
			}
		}


		public void CopyBlueprint( DataBlueprint source )
		{
			_parentBlueprintName = source._parentBlueprintName;

			CopyProperties( source, true );
		}

		private void ClearProperties()
		{
			if ( _properties != null )
			{
				_properties.ForEach( p => p.Dispose() );
				_properties.Clear();
			}
		}

		/// <summary>
		/// For each property in 'from', copies its value if this blueprint has the property,
		/// otherwise creates the property on this blueprint
		/// </summary>
		public void CopyProperties( DataBlueprint from, bool isClearProperties = false )
		{
			if ( isClearProperties )
			{
				ClearProperties();
			}

			foreach( DataBlueprintProperty property in from._properties )
			{
				DataBlueprintProperty existingProperty = GetProperty( property._name );

				// copy value
				if( existingProperty != null )
				{
					existingProperty._pValueType = property._pValueType;
					existingProperty._pIsArray = property._pIsArray;

					if ( property._pIsArray )
					{
						property._pValues.CloneTo( existingProperty._pValues );
					}
					else
					{
						existingProperty._pValue = property._pValue;
					}
				}
				// add property
				else
				{
					_properties.Add( property.Clone( this ) );
				}
			}
		}



		/// <summary>
		/// Also updates _parentBlueprintName of child blueprints
		/// </summary>
		public void SetName( string newName )
		{
			if ( _name == newName ) return;

			string	oldName = _name;

			_name = newName;

			_blueprintList.OnBlueprintNameChanged( oldName, newName );
		}

		public void OnBlueprintNameChanged( string oldName, string newName )
		{
			if ( _parentBlueprintName == oldName )
			{
				_parentBlueprintName = newName;
			}
		}

		public void OnBlueprintListChanged()
		{
			if ( _properties != null )
			{
				_properties.ForEach( p => p.OnBlueprintListChanged() );
			}
		}
#endif




#if UNITY_EDITOR

		public bool HasCollapsedAncestor( bool isStart = true )
		{
			if( !_pIsExpandedInTreeView && !isStart )
			{
				return true;
			}

			DataBlueprint parent = GetParent();

			if( parent == null )
			{
				return false;
			}

			return parent.HasCollapsedAncestor( false );
		}
#endif




		// ------------------------------------- PROPERTY ACCESSORS: Non-TryGet -------------------------------------

		public int GetInt( string property )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.Int ) )
			{
				return ( int )prop._pValue;
			}

			return GlobalDefines.INVALID_INT;
		}



		public float GetFloat( string property )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.Float ) )
			{
				return ( float )prop._pValue;
			}

			return GlobalDefines.INVALID_FLOAT;
		}



		public string GetString( string property )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.String ) )
			{
				return ( string )prop._pValue;
			}

			return GlobalDefines.INVALID_STRING;
		}



		public bool GetBool( string property )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.Bool ) )
			{
				return ( bool )prop._pValue;
			}

			return false;
		}

		public DataBlueprint GetBlueprint( string property )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.Blueprint ) )
			{
				return BlueprintManager.GetBlueprint( ( string )prop._pValue );
			}

			return null;
		}



		// -------------------------------------- PROPERTY ACCESSORS: TryGet --------------------------------------

		/// <summary>
		/// Returns true if the property exists, otherwise false
		/// </summary>
		public bool TryGetInt( string property, out int value )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.Int ) )
			{
				value = ( int )prop._pValue;
				return true;
			}

			value = GlobalDefines.INVALID_INT;
			return false;
		}



		/// <summary>
		/// Returns true if the property exists, otherwise false
		/// </summary>
		public bool TryGetFloat( string property, out float value )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.Float ) )
			{
				value = ( float )prop._pValue;
				return true;
			}

			value = GlobalDefines.INVALID_FLOAT;
			return false;
		}



		/// <summary>
		/// Returns true if the property exists, otherwise false
		/// </summary>
		public bool TryGetString( string property, out string value )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.String ) )
			{
				value = ( string )prop._pValue;
				return true;
			}

			value = GlobalDefines.INVALID_STRING;
			return false;
		}



		/// <summary>
		/// Returns true if the property exists, otherwise false
		/// </summary>
		public bool TryGetBool( string property, out bool value )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.Bool ) )
			{
				value = ( bool )prop._pValue;
				return true;
			}

			value = false;
			return false;
		}


		/// <summary>
		/// Returns true if the property exists, otherwise false
		/// </summary>
		public bool TryGetBlueprint( string property, out DataBlueprint value )
		{
			DataBlueprintProperty prop = GetProperty( property, true );

			if( ( prop != null ) && ( prop._pValueType == DataBlueprintProperty.ValueType.Blueprint ) )
			{
				value = BlueprintManager.GetBlueprint( ( string )prop._pValue );
				return true;
			}

			value = null;
			return false;
		}


		public void OnBlueprintManagerInitialise( DataBlueprintList blueprintList )
		{
            _blueprintList = blueprintList;

			if ( _properties != null )
			{
				for ( int i = 0; i < _properties.Count; ++i )
				{
					if ( _properties[i] != null )
					{
						_properties[i].OnBlueprintManagerInitialise( this );
					}
				}
			}
		}

		// ---------------------------------------- PROPERTIES ----------------------------------------
#if UNITY_EDITOR
		public JsonData _pJson
		{
			get
			{
				JsonData ret = new JsonData();

				ret[KEY_PARENT_BLUEPRINT] = _parentBlueprintName;
				ret[KEY_PROPERTIES] = new JsonData();
				ret[KEY_PROPERTIES].SetJsonType( JsonType.Array );

				foreach( DataBlueprintProperty property in _properties )
				{
					ret[KEY_PROPERTIES].Add( property._pJson );
				}

				return ret;
			}
		}
#endif

	}

}
