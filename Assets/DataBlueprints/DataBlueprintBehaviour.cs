﻿using AmuzoEngine;
using UnityEngine;


namespace DataBlueprints
{

	/// <summary>
	/// Base class for MonoBehaviours that should pull values from a blueprint.
	/// </summary>
	public abstract class DataBlueprintBehaviour : MonoBehaviour
	{

		[HideInInspector]
		public EInitializeType _initialisationType = EInitializeType.MANUAL;

		public bool _pHasBlueprintBehaviourInitialised { get; private set; }

		[HideInInspector]
		[SerializeField]
		private string _blueprintName;



		protected virtual void Awake()
		{
			if( _initialisationType == EInitializeType.AWAKE )
			{
				InitialiseBlueprintBehaviour();
			}
		}



		protected virtual void Start()
		{
			if( _initialisationType == EInitializeType.START )
			{
				InitialiseBlueprintBehaviour();
			}
		}



		public virtual void InitialiseBlueprintBehaviour()
		{
			if( _pBlueprint != null )
			{
				this.SetValuesFromBlueprint( _pBlueprint );

				_pHasBlueprintBehaviourInitialised = true;
			}
		}





		// ------------------------------------------- PROPERTIES -------------------------------------------

		public string _pBlueprintName
		{
			get
			{
				return _blueprintName;
			}

			set
			{
				DataBlueprint tempBlueprint = BlueprintManager.GetBlueprint( value );

				// validate blueprint if not null
				if( !string.IsNullOrEmpty( value ) && tempBlueprint == null )
				{
					Debug.LogError( string.Format( "Blueprint \"{0}\" not found!", value ) );
					return;
				}

				_blueprintName = value;

				if ( Application.isPlaying )
				{
					InitialiseBlueprintBehaviour();
				}
			}
		}



		public DataBlueprint _pBlueprint
		{
			get
			{
				return BlueprintManager.GetBlueprint( _blueprintName );
			}

			set
			{
				_pBlueprintName = value != null ? value._pName : "";
			}
		}


	}

}
