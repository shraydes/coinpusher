﻿using System;
using DataBlueprints;
using UnityEngine;


public class DataBlueprintsTestRig : MonoBehaviour
{

	private const float UI_SCALE = 3;
	private const float LINE_HEIGHT = 30 * UI_SCALE;
	private const float COLUMN_WIDTH_1 = 80 * UI_SCALE;
	private const float COLUMN_WIDTH_2 = 100 * UI_SCALE;

	public GameObject _dbb;

	private int _numObjects = 10;
	private int _numTestIterations = 1;
	private int _lastTestTime = 0;


	void Awake()
	{
		CreateObjects( _numObjects );
	}



	void OnGUI()
	{
		// number of objects
		GUI.Label( new Rect( 0, LINE_HEIGHT * 0, COLUMN_WIDTH_1, LINE_HEIGHT ), "Objects:" );
		string newNumObjectsString = GUI.TextField( new Rect( COLUMN_WIDTH_1, LINE_HEIGHT * 0, COLUMN_WIDTH_2, LINE_HEIGHT ), _numObjects.ToString() );
		int newNumObjects;
		if( int.TryParse( newNumObjectsString, out newNumObjects ) && newNumObjects != _numObjects )
		{
			_numObjects = newNumObjects;
			CreateObjects( _numObjects );
		}

		// number of iterations
		GUI.Label( new Rect( 0, LINE_HEIGHT * 1, COLUMN_WIDTH_1, LINE_HEIGHT ), "Iterations:" );
		string newNumIterationsString = GUI.TextField( new Rect( COLUMN_WIDTH_1, LINE_HEIGHT * 1, COLUMN_WIDTH_2, LINE_HEIGHT ), _numTestIterations.ToString() );
		int newNumIterations;
		if( int.TryParse( newNumIterationsString, out newNumIterations ) && newNumIterations != _numTestIterations )
		{
			_numTestIterations = newNumIterations;
		}

		// test button
		if( GUI.Button( new Rect( 0, LINE_HEIGHT * 2, COLUMN_WIDTH_1 + COLUMN_WIDTH_2, LINE_HEIGHT ), "Test" ) )
		{
			_lastTestTime = RunTest( _numTestIterations );
		}

		// test time
		GUI.Label( new Rect( 0, LINE_HEIGHT * 3, COLUMN_WIDTH_1, LINE_HEIGHT ), _lastTestTime.ToString() + "ms" );
	}



	void CreateObjects( int numToCreate )
	{
		// remove existing
		foreach( TestDataBlueprintBehaviour child in GetComponentsInChildren<TestDataBlueprintBehaviour>() )
		{
			Destroy( child.gameObject );
		}

		// create new
		for( int i = 0; i < numToCreate; ++i )
		{
			TestDataBlueprintBehaviour obj = ( ( GameObject )Instantiate( _dbb ) ).GetComponent<TestDataBlueprintBehaviour>();
			obj.transform.parent = transform;
		}
	}



	int RunTest( int numIterations )
	{
		DateTime t1 = DateTime.Now;

		TestDataBlueprintBehaviour[] objs = GetComponentsInChildren<TestDataBlueprintBehaviour>();

		for( int i = 0; i < numIterations; ++i )
		{
			for( int j = objs.Length - 1; j >= 0; --j )
			{
				objs[j].SetValuesFromBlueprint( objs[j]._pBlueprint );
				objs[j].SetValues( objs[j]._pBlueprint );
			}
		}

		return ( int )( ( DateTime.Now - t1 ).Ticks / 10000 );
	}

}
