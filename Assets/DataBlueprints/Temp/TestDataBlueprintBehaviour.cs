﻿using DataBlueprints;
using UnityEngine;
using System.Collections.Generic;


public class TestDataBlueprintBehaviour : DataBlueprintBehaviour
{

	public float _float1;
	public float _float2;
	public int _int1;
	public string _string1;
	public bool _bool1;
	public GameObject _prefab1;
	public Texture2D _texture1;
	public AudioClip _audio1;
	public DataBlueprint _blueprint1;

	public float[] _floatArray1;
	public List<float> _floatList1;
	public int[] _intArray1;
	public List<string> _stringList1;
	public bool[] _boolArray1;
	public List<GameObject> _prefabList1;
	public Texture2D[] _textureArray1;
	public List<AudioClip> _audioList1;
	public DataBlueprint[] _blueprintArray1;


	new void Awake()
	{
		Debug.LogError( _pBlueprint.GetInt( "asd" ) );
		Debug.LogError( _pBlueprint.GetFloat( "Int" ) );
		Debug.LogError( _pBlueprint.GetInt( "Int" ) );
		Debug.LogError( _pBlueprint.GetFloat( "Float" ) );
		Debug.LogError( _pBlueprint.GetString( "Float" ) );
		Debug.LogError( _pBlueprint.GetString( "String" ) );
		Debug.LogError( _pBlueprint.GetBool( "Bool" ) );

		string s;
		if( _pBlueprint.TryGetString( "String", out s ) )
		{
			Debug.LogError( s );
		}
		else
		{
			Debug.LogError( "none" );
		}
	}


	public void SetValues( DataBlueprint blueprint )
	{
		foreach( DataBlueprintProperty property in blueprint.GetAllProperties() )
		{
			switch( property._name )
			{
				case "Float1":
					_float1 = ( float )property._pValue;
					break;
				case "_float2":
					_float2 = ( float )property._pValue;
					break;
				case "_int1":
					_int1 = ( int )property._pValue;
					break;
				case "_string1":
					_string1 = ( string )property._pValue;
					break;
				case "_bool1":
					_bool1 = ( bool )property._pValue;
					break;
			}
		}
	}
}
