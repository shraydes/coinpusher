
{
	"infoVersion" : 3,
	"packageDeveloper" : "Amuzo",
	"packageName" : "PlayerPrefsFacade",
	"packageVersion" : "1.1.0",
	"packageSymbols" : [
		"PLAYER_PREFS_FACADE"
	],
	"packageContents" : [
		{
			"path" : "Assets/Code/Systems/PlayerPrefsFacade/Editor/PlayerPrefsFacadeEditor.cs",
			"hash" : "876250DCC33D53810FBB9939C428EFA84580D641"
		},
		{
			"path" : "Assets/Code/Systems/PlayerPrefsFacade/PlayerPrefsFacade.cs",
			"hash" : "EB67C1FF090E87655026D28ABFE2D52873A52B7D"
		},
		{
			"path" : "Assets/PackageInfo/PackageInfo_Amuzo_PlayerPrefsFacade.txt",
			"hash" : "*"
		}
	],
	"changeLog" : [
		{
			"packageVersion" : "1.0.0",
			"summary" : "Init"
		},
		{
			"packageVersion" : "1.1.0",
			"summary" : "Update debug menu."
		}
	]
}